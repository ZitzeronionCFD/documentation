---
title: fixedNormalSlip
copyright:
- Copyright (C) 2017-2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-fixedNormalSlip
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-velocity
---

<%= page_toc %>

## Description

The `fixedNormalSlip` is a vector/tensor boundary condition that
sets the patch-normal component to of the field to the patch-normal component
of a user specified field. The tangential component is treated as `slip`,
i.e. copied from the internal field.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    fixedNormalSlip;
        fixedValue              <vectorField> or <tensorField>;

        // Optional entries
        writeValue              <bool>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "fixedNormalSlipFvPatchField.H", "fixedNormalSlip" %>
<% assert :string_exists, "fixedNormalSlipFvPatchField.C", "fixedValue" %>
<% assert :string_exists, "fixedNormalSlipFvPatchField.C", "writeValue" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `fixedNormalSlip`               | word    | yes      | -
`fixedValue`   | User-defined patch-normal field  | vectorField/tensorField    | yes      | -
`writeValue`   | Write patch values (eg, ParaView)          | bool    | no       | false


The inherited entries are elaborated in:

- [Foam::transformFvPatchField](doxy_id://Foam::transformFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/potentialFreeSurfaceDyMFoam/oscillatingBox" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fixedNormalSlip" %>

API:

- [Foam::fixedNormalSlipFvPatchField](doxy_id://Foam::fixedNormalSlipFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->