---
title: translatingWallVelocity
copyright:
- Copyright (C) 2017-2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
- velocity
menu_id: boundary-conditions-translatingWallVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-velocity
---

<%= page_toc %>

## Description

The `translatingWallVelocity` is a velocity boundary condition for translational
motion on walls that sets the velocity parallel to the wall to a given value,
e.g. to approximate a moving ground plane. The patch normal velocity component
is set to zero.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            translatingWallVelocity;
        U               <Function1<vector>>;

        // Inherited entries
        ...
    }

<% assert :string_exists,
   "translatingWallVelocityFvPatchVectorField.H", "translatingWallVelocity" %>
<% assert :string_exists, "translatingWallVelocityFvPatchVectorField.C", "U" %>

where:

Property              | Description                                | Type    | Required | Default
----------------------|--------------------------------------------|---------|----------|---------
`type`                | Type name: `translatingWallVelocity`       | word    | yes      | -
`U`                   | Translational velocity                     | Function1\<vector\>  | yes | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/driftFluxFoam/RAS/tank3D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/translatingWallVelocity" %>

API:

- [Foam::translatingWallVelocityFvPatchVectorField](doxy_id://Foam::translatingWallVelocityFvPatchVectorField)

<%= history "1.7.0" %>

<!----------------------------------------------------------------------------->
