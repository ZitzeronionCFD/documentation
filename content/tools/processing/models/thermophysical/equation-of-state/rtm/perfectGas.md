---
title: Perfect gas
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- equation of state
- perfect gas
menu_id: equationOfState-perfectGas
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-equationOfState
group: equation-of-state-models
---

<%= page_toc %>

Density, $$\rho$$ \[kg/m$$^3$$\]

$$
\rho = \frac{p}{R T}
$$

Entropy, $$ S $$ \[J/kg/K \]

$$
S = -R \ln \left( \frac{p}{p_{\std}} \right)
$$

Compressibility, $$ \psi $$ \[s$$^2$$/m$$^2$$\]

$$
\psi = \frac{1}{R T}

$$

## Usage

The `perfectGas` equation of state is specified as:

~~~
equationOfState
{
    // empty - no input needed
}
~~~

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/equationOfState/perfectGas" %>
