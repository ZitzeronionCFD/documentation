---
title: Corrections
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- correction
menu_id: numerics-fvoptions-corrections
license: CC-BY-NC-ND-4.0
menu_parent: numerics-fvoptions
---

<%= page_toc %>

## Momentum {#momentum}

<%= insert_models "fvoptions-corrections-momentum" %>

## Energy {#energy}

<%= insert_models "fvoptions-corrections-energy" %>
