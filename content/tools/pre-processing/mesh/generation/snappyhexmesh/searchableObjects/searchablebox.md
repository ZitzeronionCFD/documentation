---
title: searchableBox
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: searchablebox
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh-geometry
group: searchableobjects
---

<%= page_toc %>

A cuboid given by two points on its diagonal.

## Usage

    box
    {
        type            searchableBox;
        min             (1.5 1 -0.5);
        max             (3.5 2 0.5);
    }

<% assert :string_exists, "searchableBox.H", "searchableBox" %>
<% assert :string_exists, "searchableBox.C", "min" %>
<% assert :string_exists, "searchableBox.C", "max" %>

Where:

Property     | Description               | Required
-------------|---------------------------|----------
`min`        | Box minimum point         |      yes
`max`        | Box maximum point         |      yes
