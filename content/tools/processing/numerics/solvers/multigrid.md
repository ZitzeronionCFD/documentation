---
title: Multigrid solvers
short_title: Multigrid
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- multigrid
- solvers
menu_id: linearequationsolvers-multigrid
license: CC-BY-NC-ND-4.0
menu_parent: numerics-linearequationsolvers
---

<%= page_toc %>

## Options {#options}

<%= insert_models "solvers-multigrid" %>
