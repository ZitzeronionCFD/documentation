---
title: Pressure-velocity algorithms
copyright:
- Copyright (C) 2021 OpenCFD Ltd.
tags:
- algorithm
menu_id: pressure-velocity
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

\underConstruction

The background to the [SIMPLE](ref_id://pv-simple),
[PISO](ref_id://pv-piso) and
[PIMPLE](ref_id://pv-pimple) pressure-velocity algorithms can be
demonstrated using the incompressible, inviscid flow equations, comprising the
<a name="eqn-solver-momentum"></a>**momentum equation**:

$$
    \ddt{\u} + \div (\u \otimes \u) = - \grad p,
$$

and **continuity equation**  <a name="eqn-solver-continuity"></a>

$$
    \div \u = 0.
$$

Discretising the momentum equation leads to a set of algebraic equations of the
form:

$$
    M[\u] = - \grad p
$$

where the matrix $$M[\u]$$ comprises the diagonal and off-diagonal
contributions using the <a name="eqn-solver-matrix-composition"></a>
decomposition:

$$
    M[\u] = A\u - \vec{H}
$$

The <a name="eqn-solver-discretised-momentum"></a>
**discretised momentum equation** therefore becomes:

$$
    A\u - \vec{H} = - \grad p
$$

which on re-arranging leads to the <a name="eqn-solver-velocity-corrector"></a>
**velocity correction equation**:

$$
    \u = \frac{\vec{H}}{A} - \frac{1}{A} \grad p.
$$

The volumetric **flux corrector equation** is then derived by interpolating
$$ \u $$ to the faces and dotting the result with the face area vectors,
$$ \vec{S}_f $$: <a name="eqn-solver-flux-corrector"></a>

$$
    \phi
  = \u_f \dprod \vec{S}_f
  = \left( \frac{\vec{H}}{A} \right)_f \dprod \vec{S}_f
  - \left( \frac{1}{A} \right)_f \vec{S}_f \dprod \snGrad p
$$

Discretisation of the continuity equation yields the
<a name="eqn-solver-discretised-continuity"></a>constraint:

$$
    \div \phi = 0.
$$

Substituting the flux equation leads to the
<a name="eqn-solver-pressure"></a>**pressure equation**:

$$
    \div \left[ \left( \frac{1}{A} \right)_f \grad p \right]
  = \div \left( \frac{\vec{H}}{A} \right)_f.
$$
