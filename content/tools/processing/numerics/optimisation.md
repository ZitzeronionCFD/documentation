---
title: Optimisation
copyright:
- Copyright (C) 2019-2021 OpenCFD Ltd.
tags:
- optimisation
menu_id: numerics-optimisation
license: CC-BY-NC-ND-4.0
menu_parent: numerics
publish: false
---

<%= page_toc %>

## Overview {#sec-optimisation-overview}

## Methods {#sec-optimisation-methods}

- <%= link_to_menuid "adjoint" %>

## Further information {#sec-optimisation-further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/optimisation" %>
