---
title: nutWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-nutWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `nutWallFunction` is an abstract base class that
hosts $$y^+$$ calculation methods and common functions for
`nut` wall-function boundary conditions.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Optional entries
        U               <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "nutWallFunctionFvPatchScalarField.H", "nutWallFunction" %>
<% assert :string_exists, "nutWallFunctionFvPatchScalarField.C", "U" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`U`            | Name of velocity field                     | word    | no       | U

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- wallFunctionCoefficients.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/nutWallFunctions/nutWallFunction" %>

API:

- [Foam::nutWallFunctionFvPatchScalarField](doxy_id://Foam::nutWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
