---
title: add
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- add
- field
- function object
- post-processing
menu_id: function-objects-field-add
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `add` function object sums a given list of (at least two or more) fields
and outputs the result into a new field, where the fields possess the same
sizes and dimensions:

    fieldResult = field1 + field2 + ... + fieldN

### Operands

Operand      | Type                          | Location
:---         |:---                           |:---
input        | `{vol,surface}<Type>Field(s)` | `$FOAM_CASE/<time>/<inpField>s`
output file  | -                             | -
output field | `{vol,surface}<Type>Field`    | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `add` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    add1
    {
        // Mandatory entries    (unmodifiable)
        type            add;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entries    (runtime modifiable)
        fields          (<field1> <field2> ... <fieldN>);

        // Optional (inherited) entries
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "add.H", "add" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "add",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldsExpression.C", "fields" %>
<% assert :string_exists, "fieldsExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: add                      | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 fields       | Names of the operand fields         | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldsExpression](ref_id://function-objects-field-fieldsexpression)

Example by using the `postProcess` utility:

    postProcess -func "add(<field1>, <field2>, ..., <fieldN>)"

## Stored properties {#stored-properties}

### Fields

The `add` function object result field is stored on the mesh database,
using the default name:

    add(<field1>,<field2>,...,<fieldN>)

This can be overridden by using the `result` entry.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/twoPhaseEulerFoam/laminar/bubbleColumn" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= "$FOAM_SRC/functionObjects/field/add" %>

API:

- [Foam::functionObjects::add](doxy_id://Foam::functionObjects::add)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
