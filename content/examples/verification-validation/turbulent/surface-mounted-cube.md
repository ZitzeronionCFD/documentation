---
title: Surface mounted cube
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- channel
- example
- turbulence
- verification
- validation
menu_id: verification-validation-turbulent-surface-mounted-cube
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-turbulent
group: verification-validation-turbulent
---

<%= page_toc %>

## Overview {#sec-overview}

- Solver: [pimpleFoam](ref_id://pimplefoam)
- Experimental case described by Martinuzzi and Tropea <%= cite "martinuzzi_flow_1993" %>
- Reynolds number based on the cube height of $$ Re_h $$ 40000
- Turbulence is modelled using the [SA-IDDES](ref_id://des-spalartallmarasiddes) model
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/surfaceMountedCube" %>

![Case description](boundary-conditions-small.png)

## Mesh {#sec-mesh}

- 3D structured mesh created using [blockMesh](ref_id://blockmesh)

## Boundary conditions {#sec-boundary-conditions}

A precursor `boundaryFoam` calculation is used to set the inlet flow conditions
onto which turbulent content is superimposed using the `turbulentDFSEMInlet`
velocity condition.

### Common fields {#sec-boundary-conditions-turbulence-common}

- $$ U $$ 1 m/s
- $$ h $$ 1 m
- $$ \nu $$ 2.5e-5 m2/s

Velocity: `U`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | `turbulentDFSEMInlet`   |   |
| Outlet  | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Walls   | [noSlip](ref_id://boundary-conditions-noSlip)       |              |

Pressure: `p`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Outlet  | [fixedValue](ref_id://boundary-conditions-fixedValue)   | 0 Pa (static)           |
| Walls   | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |

### Turbulence fields {#sec-boundary-conditions-turbulence}

Turbulence viscosity: `nut`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | `calculated`   |              |
| Outlet  | `calculated`   |              |
| Walls   | `nutUSpaldingWallFunction` |  |

#### Spalart-Allmaras

Modified viscosity: `nuTilda`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [mappedFixedValue](ref_id://boundary-conditions-fixedValue)   | |
| Outlet  | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Walls   | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |

## Results {#sec-results}

The following images show examples of the instantaneous- and mean-velocity
predictions

| ![Velocity](U-LIC-1000-400x400.png) | ![Velocity](U-LIC-1000-plan-400x400.png) |
| ![Mean velocity](UMean-LIC-1000-400x400.png) | ![Mean velocity](UMean-LIC-1000-plan-400x400.png) |
{: .noBorder}

Profiles at a series of x/h positions are presented in the following images,
where the predictions are compared against the experimental data of
Martinuzzi and Tropea <%= cite "martinuzzi_flow_1993" %>

| ![x/h = -1](U-x-by-hm10-400x400.png) | ![x/h = -0.5](U-x-by-hm05-400x400.png) |
| ![x/h = 1](U-x-by-h10-400x400.png) | ![x/h = 1.5](U-x-by-h15-400x400.png) |
| ![x/h = 2.5](U-x-by-h25-400x400.png) | ![x/h = 4](U-x-by-h40-400x400.png) |
{: .noBorder}
