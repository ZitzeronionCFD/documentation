function loadTheme() {
  // Theme - persistent using local storage

  // Theme - re-instate theme on page reload
  if (localStorage.getItem('theme') === 'dark') {
    document.documentElement.setAttribute('theme', 'dark');
    let toggleCheck = document.querySelector('#toggle-theme');
    toggleCheck.checked = true;
  }
}

function loadMenu() {
  // Menu state - use session storage

  // If shown.bs.collapse add the unique id to session storage
  $('.collapse').on('shown.bs.collapse', function () {
    sessionStorage.setItem('coll_' + this.id, true);
  });
  // If hidden.bs.collapse remove the unique id from session storage
  $('.collapse').on('hidden.bs.collapse', function () {
    sessionStorage.removeItem('coll_' + this.id);
  });
  // If the key exists and is set to true, show the collapsed, otherwise hide
  $('.collapse').each(function () {
    if (sessionStorage.getItem('coll_' + this.id) == 'true') {
      $(this).collapse('show');
    } else {
      $(this).collapse('hide');
    }
  });

  const scroll = sessionStorage.getItem('menu-scroll-y');
  if (scroll) {
    $('#sidebar').scrollTop(scroll);
  }
}

function loadState() {
  loadTheme();
  loadMenu();
}

$(window).on('load', function() {
  loadState();
});

// Theme - toggle dark mode
$('#theme-toggle-btn').on('click', function() {
  console.log("Switching theme");
  if (localStorage.getItem('theme') === 'dark') {
    localStorage.removeItem('theme')
    document.documentElement.removeAttribute('theme');
  } else {
    localStorage.setItem('theme', 'dark')
    document.documentElement.setAttribute('theme', 'dark');
  }
});

window.addEventListener("pagehide", (event) => {
  sessionStorage.setItem('menu-scroll-y', $('#sidebar').scrollTop());
}, false);

let btnTop = document.getElementById("btnTop");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    btnTop.style.display = "block";
  } else {
    btnTop.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
