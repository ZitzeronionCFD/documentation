---
title: Linear eddy viscosity models
short_title: Linear EVM
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- eddy viscosity model
- linear
- RAS
- turbulence
menu_id: ras-linearevm
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-ras
---

<%= page_toc %>

Linear eddy viscosity turbulence model selections include:

<%= insert_models "turbulence-ras-evm" %>

## Background {#background}

Under the Boussinesq hypothesis <%= cite "boussinesq_essai_1877" %>, the deviatoric
anisotropic stress is considered proportional to the traceless mean rate of
strain:

$$
  - \rho \tensor{R}_\mathit{dev}
  =
  - \rho \av{\u' \otimes \u'} + \frac{2}{3} \rho k \tensor{I}
  =
    \mu_t \left[ 2\, \tensor{S} - \left( \frac{2}{3} \div \u \right) \tensor{I} \right]
$$

where $$ \tensor{S} $$ is the symmetric tensor

$$
    \tensor{S} = \frac{1}{2}\left( \grad \av{\u} + \grad \left( \av{\u} \right)^T \right)
$$

leading to:

$$
  - \rho \tensor{R}_\mathit{dev}
  =
    \mu_t \left( \grad \av{\u} + \grad \left( \av{\u} \right)^T \right)
  + \mu_t \left( \frac{2}{3} \div \u \right)\tensor{I}
$$

where $$ \mu_t $$ is the dynamic *eddy viscosity*.  The momentum equation
therefore becomes:

$$
    \ddt{\rho \av{\u}}
  + \div \left( \rho \av{\u} \otimes \av{\u} \right)
  =
    \vec{g}
  - \grad \av{p}'
  + \div \left( \mu_\mathit{eff} \grad \av{\u} \right)
  + \div \left[ \mu_\mathit{eff} \, \mathrm{dev2}\left(\left(\grad \av{\u}\right)^T \right) \right]
$$

where $$ \mu_\mathit{eff} $$ is the *effective* dynamic eddy viscosity:

$$
    \mu_\mathit{eff} = \mu + \mu_t
$$

i.e. the sum of the laminar and turbulent contributions.
