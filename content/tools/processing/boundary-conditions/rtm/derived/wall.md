---
title: Wall conditions
short_title: Wall
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-wall
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

## Boundary mesh type {#boundary-mesh-type}

Wall conditions are specified using the `wall` type entry in the
`$FOAM_CASE/constant/polyMesh/boundary` file:

    patchName
    {
        type            wall;
        ...
    }

## Velocity {#velocity}

Available wall conditions include

<%= insert_models "bcs-wall-velocity", "bcs-wall-moving-mesh-velocity" %>

## Pressure {#pressure}

- see [zeroGradient](ref_id://boundary-conditions-zeroGradient)

## Temperature {#temperature}

- adiabatic: see [zero gradient](ref_id://boundary-conditions-zeroGradient)
- fixed temperature: see [fixed value](ref_id://boundary-conditions-fixedValue)
- fixed heat flux: see
  [fixed gradient](ref_id://boundary-conditions-fixedGradient)

## Turbulence {#turbulence}

See turbulence wall modelling

- See [Reynolds Averaged Simulation](ref_id://turbulence-ras#wall-modelling)
- See [Detached Eddy Simulation](ref_id://turbulence-des#wall-modelling)
- See [Large Eddy Simulation](ref_id://turbulence-les#wall-modelling)

## Further information {#further-information}

Related:

- See [wall distance](ref_id://schemes-walldistance)

Source code:

- [Foam::wallPolyPatch](doxy_id://Foam::wallPolyPatch)
- [Foam::wallFvPatch](doxy_id://Foam::wallFvPatch)

See also

- [grpWallBoundaryConditions](doxy_id://grpWallBoundaryConditions)
