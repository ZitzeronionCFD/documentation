---
title: movingWallVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-movingWallVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-velocity
---

<%= page_toc %>

## Description

The `movingWallVelocity` is a velocity boundary condition that provides
a condition for cases with moving walls.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    movingWallVelocity;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "movingWallVelocityFvPatchVectorField.H", "movingWallVelocity" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `movingWallVelocity`            | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/propeller" %>
- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/overBuoyantPimpleDyMFoam/movingBox" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/movingWallVelocity" %>

API:

- [Foam::movingWallVelocityFvPatchVectorField](doxy_id://Foam::movingWallVelocityFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->