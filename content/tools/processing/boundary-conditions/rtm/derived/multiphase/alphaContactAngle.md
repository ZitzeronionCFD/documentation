---
title: alphaContactAngle
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-alphaContactAngle
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-multiphase
group: bcs-multiphase
---

<%= page_toc %>

## Description

The `alphaContactAngle` is a boundary condition that provides
contact-angle boundary condition for multi-phase interface-capturing
simulations. It is used in conjunction with `multiphaseSystem`.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                      alphaContactAngle;
        thetaProperties
        (
            (<phase1> <phase2>) <scalar1> <scalar2> <scalar3> <scalar4>
            (<phase3> <phase2>) <scalar1> <scalar2> <scalar3> <scalar4>
            ...
        );

        // Inherited entries
        ...
    }

<% assert :string_exists, "alphaContactAngleFvPatchScalarField.H", "alphaContactAngle" %>
<% assert :string_exists, "alphaContactAngleFvPatchScalarField.C", "thetaProperties" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `alphaContactAngle`             | word    | yes      | -
`thetaProperties`      | Contact-angle properties                   | dict    | yes      | -
\<scalar1\>            | Equilibrium contact angle                  | scalar  | yes      |-
\<scalar2\>            | Dynamic contact angle velocity scale       | scalar  | yes      |-
\<scalar3\>            | Limiting advancing contact angle           | scalar  | yes      |-
\<scalar4\>            | Limiting receding contact angle            | scalar  | yes      |-

The inherited entries are elaborated in:

- zeroGradientFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/multiphaseInterFoam/laminar/damBreak4phase/" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/phaseSystemModels/reactingEuler/multiphaseSystem/derivedFvPatchFields/alphaContactAngle" %>

API:

- [Foam::reactingMultiphaseEuler::alphaContactAngleFvPatchScalarField](doxy_id://Foam::reactingMultiphaseEuler::alphaContactAngleFvPatchScalarField)

<%= history "v2012" %>

<!----------------------------------------------------------------------------->
