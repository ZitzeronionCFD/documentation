---
title: exprField
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- expression
- function object
- post-processing
menu_id: function-objects-field-exprField
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

The `exprField` function object creates or modifies a field based on an
[expression](ref_id://expression-syntax).

## Usage {#usage}

The `exprField` function object is specified using:

    exprField1
    {
        type            exprField;
        libs            (fieldFunctionObjects);
        ...
        field           <field-name>;

        expression      <expr>; // new and modify actions only

        // Optional entries
        action          <action-type>;
        readFields      <bool>;

        fieldMask       <mask-expr>; // modify action only


        autowrite       <bool>; // default: false;
        store           <bool>; // default: true

        dimensions      <dimensions>;
    }

<% assert :string_exists, "fvExpressionField.H", "exprField" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "fvExpressionField",
   "libutilityFunctionObjects" %>
<% assert :string_exists, "fvExpressionField.C", "field" %>
<% assert :string_exists, "fvExpressionField.C", "expression" %>
<% assert :string_exists, "fvExpressionField.C", "action" %>
<% assert :string_exists, "fvExpressionField.C", "readFields" %>
<% assert :string_exists, "fvExpressionField.C", "fieldMask" %>
<% assert :string_exists, "fvExpressionField.C", "autowrite" %>
<% assert :string_exists, "fvExpressionField.C", "dimensions" %>

Actions

- `none`: no-op
- `new`: create a new field - **default**
- `modify`: modify an existing field

<% assert :string_exists, "fvExpressionField.C", "none" %>
<% assert :string_exists, "fvExpressionField.C", "new" %>
<% assert :string_exists, "fvExpressionField.C", "modify" %>

## Examples

Create a new `pTotal` field based on an expressions for

$$
p_t = p + \frac{1}{2} \rho | \u |^2
$$

~~~
exprField1
{
    type            exprField;
    libs            (fieldFunctionObjects);
    field           pTotal;
    expression      "p + 0.5*(rho*magSqr(U))";
    dimensions      [ Pa ];
}
~~~

Modify the existing `pTotal` field by setting the static pressure using the `p`
field in the region defined by

$$
|\vec{x}| < 0.05
$$

and

$$
x_y > 0
$$

~~~
exprField
{
    type            exprField;
    libs            (fieldFunctionObjects);
    field           pTotal;
    action          modify;

    // Static pressure only in these regions
    fieldMask       "(mag(pos()) < 0.05) && (pos().y() > 0)";
    expression      "p";
}
~~~

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/expressions" %>

API:

- [Foam::functionObjects::fvExpressionField](doxy_id://Foam::functionObjects::fvExpressionField)

Related:

- [expressions](ref_id://expression-syntax)