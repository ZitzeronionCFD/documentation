---
title: Diagonal preconditioner
short_title: Diagonal
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- diagonal
- preconditioner
- solvers
menu_id: cg-preconditioner-diagonal
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-cg
group: solvers-cg-preconditioner
---

<%= page_toc %>

## Properties {#properties}

- Symmetric and asymmetric matrices
- Parallel consistent

## Usage {#usage}

    preconditioner  diagonal;

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/LduMatrix/Preconditioners/DiagonalPreconditioner" %>

API:

- [Foam::diagonalPreconditioner](doxy_id://Foam::diagonalPreconditioner)
