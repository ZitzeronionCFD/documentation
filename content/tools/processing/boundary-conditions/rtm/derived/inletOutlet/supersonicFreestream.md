---
title: supersonicFreestream
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-supersonicFreestream
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `supersonicFreestream` is a boundary condition that provides a supersonic
free-stream condition.

- supersonic inflow is assumed to occur according to the Prandtl-Meyer
  expansion process.
- subsonic outflow is applied via a zero-gradient condition from inside
  the domain.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            supersonicFreestream;
        UInf            <vector>;
        pInf            <scalar>;
        TInf            <scalar>;
        gamma           <scalar>;

        // Optional entries
        T               <word>;
        p               <word>;
        psi             <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "supersonicFreestreamFvPatchVectorField.H", "supersonicFreestream" %>
<% assert :string_exists, "supersonicFreestreamFvPatchVectorField.C", "UInf" %>
<% assert :string_exists, "supersonicFreestreamFvPatchVectorField.C", "pInf" %>
<% assert :string_exists, "supersonicFreestreamFvPatchVectorField.C", "TInf" %>
<% assert :string_exists, "supersonicFreestreamFvPatchVectorField.C", "gamma" %>
<% assert :string_exists, "supersonicFreestreamFvPatchVectorField.C", "T" %>
<% assert :string_exists, "supersonicFreestreamFvPatchVectorField.C", "p" %>
<% assert :string_exists, "supersonicFreestreamFvPatchVectorField.C", "psi" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `supersonicFreestream`          | word    | yes      | -
`UInf`         | Freestream velocity                        | vector  | yes      | -
`pInf`         | Freestream pressure                        | vector  | yes      | -
`TInf`         | Freestream temperature                     | vector  | yes      | -
`gamma`        | Heat capacity ratio                        | scalar  | yes      | -
`T`            | Name of temperature field                  | word    | no       | T
`p`            | Name of pressure field                     | word    | no       | p
`psi`          | Name of compressibility field              | word    | no       | thermo:psi

The inherited entries are elaborated in:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/sonicFoam/RAS/nacaAirfoil" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/supersonicFreestream" %>

API:

- [Foam::supersonicFreestreamFvPatchVectorField](doxy_id://Foam::supersonicFreestreamFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
