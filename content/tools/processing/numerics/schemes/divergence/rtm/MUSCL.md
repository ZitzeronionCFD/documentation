---
title: MUSCL divergence scheme
short_title: MUSCL
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-muscl
license: CC-BY-NC-ND-4.0
group: schemes-divergence-nvdtvd
---

<%= page_toc %>

## Properties {#properties}

- <B>M</B>onotone <B>U</B>pstream-centered <B>S</B>chemes for
  <B>C</B>onservation <B>L</B>aws
- Second order
- Unbounded

## Normalised Variable Diagram {#nvd}

![MUSCL](../MUSCL.png)

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss MUSCL;
    }

<% assert :string_exists, "MUSCL.H", "MUSCL" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/limitedSchemes/MUSCL" %>

API:

- [Foam::MUSCLLimiter](doxy_id://Foam::MUSCLLimiter)
