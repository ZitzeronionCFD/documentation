---
title: mappedFixedInternalValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-mappedFixedInternalValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `mappedFixedInternalValue` is a generic boundary condition that maps the
boundary and internal values of a neighbour patch field to the boundary and
internal values of the local patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            mappedPatch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    mappedFixedInternalValue;

        // Inherited entries
        ...
    }

<% assert :string_exists, "mappedFixedInternalValueFvPatchField.H", "mappedFixedInternalValue" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `mappedFixedInternalValue`      | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::mappedFixedValueFvPatchField](doxy_id://Foam::mappedFixedValueFvPatchField)
<!-- end of the list -->

- This boundary condition can only be applied to patches that are of
  the `mappedPolyPatch` type.
  {: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/mappedFixedInternalValue" %>

API:

- [Foam::mappedFixedInternalValueFvPatchField](doxy_id://Foam::mappedFixedInternalValueFvPatchField)

<%= history "2.1.0" %>

<!----------------------------------------------------------------------------->
