---
title: kaqRWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-kaqRWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-optimisation
group: bcs-optimisation
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            kaqRWallFunction;
        solverName      <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "kaqRWallFunctionFvPatchScalarField.H", "kaqRWallFunction" %>
<% assert :string_exists, "kaqRWallFunctionFvPatchScalarField.C", "solverName" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `kaqRWallFunction`              | word    | yes      | -
`solverName`   | Name of operand solver                     | word    | yes      | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- adjointBoundaryConditions.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/adjointOptimisationFoam/shapeOptimisation/sbend/turbulent/kOmegaSST/opt" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/optimisation/adjointOptimisation/adjoint/turbulenceModels/incompressibleAdjoint/adjointRAS/derivedFvPatchFields/kaqRWallFunction" %>

API:

- [Foam::kaqRWallFunctionFvPatchScalarField](doxy_id://Foam::kaqRWallFunctionFvPatchScalarField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
