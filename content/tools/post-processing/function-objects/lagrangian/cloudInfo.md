---
title: Cloud information
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- cloud
- function object
- post-processing
menu_id: function-objects-lagrangian-cloudinformation
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-lagrangian
group: function-objects-lagrangian
---

<%= page_toc %>

The `cloudInfo` function object generates statistics for a list of clouds,
including:

- number of parcels
- total mass of parcels
- maximum diameter
- Arithmetic mean diameter, $$D_{10}$$
- Sauter mean diameter, $$D_{32}$$

## Usage {#usage}

The `cloudInfo` function object is specified using:

    cloudInfo1
    {
        type        cloudInfo;
        libs        ("liblagrangianFunctionObjects.so");

        clouds      (kinematicCloud1  thermoCloud1);
    }

<% assert :string_exists, "cloudInfo.H", "cloudInfo" %>
<% assert :class_library_exists,
   "functionObjects/lagrangian/Make/files",
   "cloudInfo",
   "liblagrangianFunctionObjects" %>
<% assert :string_exists, "cloudInfo.C", "clouds" %>

## Sample output {#sample-output}

    cloudInfo cloudInfo1 write:
        number of parcels : 1234
        mass in system    : 1.234
        maximum diameter  : 1.0
        D10 diameter      : 0.5
        D32 diameter      : 0.6

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/lagrangian/cloudInfo" %>

API:

- [Foam::functionObjects::cloudInfo](doxy_id://Foam::functionObjects::cloudInfo)
