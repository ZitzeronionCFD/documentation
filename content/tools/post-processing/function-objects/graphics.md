---
title: Graphics
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- function object
- graphics
- post-processing
menu_id: function-objects-graphics
license: CC-BY-NC-ND-4.0
menu_parent: post-processing-function-objects
---

<%= page_toc %>

Options:

<%= insert_models "function-objects-graphics" %>

Related:

- [Source documentation](doxy_id://grpGraphicsFunctionObjects)
