---
title: Mesh-wave wall distance (default)
short_title: Mesh-wave
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- wall
- mesh-wave
menu_id: schemes-walldistance-meshwave
license: CC-BY-NC-ND-4.0
#menu_parent: schemes-walldistance
group: schemes-wall-distance
---

<%= page_toc %>

## Properties {#properties}

Fast topological mesh-wave method for calculating the distance to nearest
patch for all cells and boundary.

For regular/un-distorted meshes this method is accurate but for skewed,
non-orthogonal meshes it is approximate with the error increasing with the
degree of mesh distortion.  The distance from the near-wall cells to the
boundary may optionally be corrected.

An example of `meshWave` output in terms of the wall distance
can be seen as follows:

![Wall distance field by meshWave](../meshWave.png)

## Usage {#usage}

The scheme is specified using:

    wallDist
    {
        method          meshWave;

        // Optionally correct distance from near-wall cells to the boundary
        correctWalls    true;
    }

## Further information {#further-information}

Source code

- [Foam::patchDistMethods::meshWave](doxy_id://Foam::patchDistMethods::meshWave)

<!----------------------------------------------------------------------------->
