---
title: scalarTransportFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- basic
- incompressible
- solvers
menu_id: scalartransportfoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-basic
group: solvers-basic
solver_models:
- transient
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-basic" %>
- transient
- incompressible

## Equations {#equations}

Evolves a transport equation for the scalar $$ T $$

$$
    \ddt{T} + \div (\u T) - \div (D_T \grad T ) = S_{T}
$$

## Input requirements {#input-requirements}

Mandatory fields:

- `U`: velocity \[m/s\]
- `T`: scalar \[-\]

### Physical models {#input-requirements-physical-models}

- Transport `$FOAM_CASE/constant/transportProperties` requirements:

      // Diffusion coefficient \[m2/s\]
      DT              4e-05;

<% assert :string_exists, "scalarTransportFoam/createFields.H", "transportProperties" %>
<% assert :string_exists, "scalarTransportFoam/createFields.H", "DT" %>

### Solution controls {#input-requirements-solution-controls}

[$FOAM_CASE/system/fvSchemes](ref_id://fvschemes) requirements:

- [time schemes](ref_id://schemes-time): `ddt(T)`
- [gradient schemes](ref_id://schemes-gradient): `grad(T)`
- [divergence schemes](ref_id://schemes-divergence): `div(phi,T)`
- [laplacian schemes](ref_id://schemes-laplacian): `laplacian(DT,T)`

<% assert :text_exists, "scalarTransportFoam.C", "fvm::ddt" %>
<% assert :text_exists, "scalarTransportFoam.C", "fvm::div" %>
<% assert :text_exists, "scalarTransportFoam.C", "fvm::laplacian" %>

[$FOAM_CASE/system/fvSolution](ref_id://fvsolution) requirements:

- [solver entry](ref_id://numerics-linearequationsolvers#options)  for `T`

<% assert :string_exists, "scalarTransportFoam/createFields.H", "T" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/basic/scalarTransportFoam" %>

See also

- <%= link_to_menuid "icofoam" %>
- <%= link_to_menuid "pisofoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/basic/scalarTransportFoam" %>
