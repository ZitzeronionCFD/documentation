---
title: fixedJumpAMI
copyright:
- Copyright (C) 2016-2023 OpenCFD Ltd.
tags:
- boundary conditions
- coupled
menu_id: boundary-conditions-fixedJumpAMI
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-coupled
group: bcs-coupled-jump
---

<%= page_toc %>

## Description

The `fixedJumpAMI` is a general coupled boundary condition to provide a jump
condition across non-conformal cyclic patch pairs (employing `AMI`). The jump is
specified as a fixed value field, applied as an offset to the 'owner' patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedJumpAMI;
        jump            <Field<Type>>;

        // Inherited entries
        patchType       <word>;
        ...
    }

<% assert :string_exists, "fixedJumpAMIFvPatchField.H", "jump" %>

where:

Property       | Description                   | Type    | Required | Default
---------------|-------------------------------|---------|----------|---------
`type`         | Type name: `fixedJumpAMI`     | word    | yes      | -
`jump`         | Jump field                    | Field\<Type\> | yes  | -

The inherited entries are elaborated in:

- [Foam::jumpCyclicAMIFvPatchField](doxy_id://Foam::jumpCyclicAMIFvPatchField)
<!-- end of the list -->

- The underlying `patchType` should be set to `cyclicAMI`.
{: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fixedJumpAMI" %>

API:

- [Foam::fixedJumpAMIFvPatchField](doxy_id://Foam::fixedJumpAMIFvPatchField)

<%= history "2.2.0" %>

<!----------------------------------------------------------------------------->
