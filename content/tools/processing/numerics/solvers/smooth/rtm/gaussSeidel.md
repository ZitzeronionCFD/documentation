---
title: Gauss Seidel Smoother
short_title: Gauss Seidel
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- solvers
- Gauss Seidel
menu_id: smooth-gauss-seidel
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-smooth
group: solvers-smooth-smoothers
---

<%= page_toc %>

## Properties {#properties}

- Gauss Seidel smoother
- Symmetric or asymmetric matrices

## Usage {#usage}

    smoother        GaussSeidel;

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/smoothers/GaussSeidel" %>

API:

- [Foam::GaussSeidelSmoother](doxy_id://Foam::GaussSeidelSmoother)
