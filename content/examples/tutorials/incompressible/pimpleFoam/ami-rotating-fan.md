---
title: Rotating mesh
copyright:
- Copyright (C) 2019-2022 OpenCFD Ltd.
- Copyright (C) 2019-2022 Jozsef Nagy
tags:
- AMI
- example
- incompressible
- tutorial
menu_id: ami-rotating-fan
license: CC-BY-NC-ND-4.0
menu_parent: examples-tutorials
group: tutorials
authors:
- OpenCFD Ltd.
- Jozsef Nagy
---

<%= page_toc %>

## Overview {#overview}

- Solver: [pimpleFoam](ref_id://pimplefoam)
- Goals: Learn how to set up
  - [geometry](#geometry)
  - [mesh](#mesh)
  - case
- [Simulation](#simulation) with rotating mesh
  - example: rotating fan in a room

Case files can be found here:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/rotatingFanInRoom" %>

## Geometry {#geometry}

The geometry for this tutorial consists of

- room with
  - door
  - window (outlet)
- desk
- rotating fan
- cylinder defining
  - rotating cells
  - the interpolation surfaces between rotating and non-rotating cells

![Geometry](tutorial-pimplefoam-ami-rotating-fan-geometry-600.png)

## Mesh {#mesh}

The mesh is created with [snappyHexMesh](ref_id://snappyhexmesh) using
STL surfaces located in the `constant/triSurface` directory.

    surfaceFeatureExtract

The process begins by extracting features from the STL files (dictionary can
be found in _system/surfaceFeatureExtractDict_).

    blockMesh

This command creates an initial block structured mesh with our starting mesh
resolution (dictionary can be found in _system/blockMeshDict_).

    snappyHexMesh -overwrite

This command refines the initial mesh at the STL surfaces as well as the
extruded features (dictionary can be found in _system/snappyHexMeshDict_).
Layer addition is not utilized in this tutorial for simplicity purposes.
Usual `snappyHexMesh` settings are utilized for a coarse mesh here. One important
entry can be found in the `refinementSurfaces` sub-dictionary:

    AMI
    {
        level     (2 2);
        faceType  boundary;
        cellZone  rotatingZone;
        faceZone  rotatingZone;
        cellZoneInside inside;
    }

This defines a cellZone called _rotatingZone_, which will later define the
rotating cells. Additionally we define a boundary, which will be later used to
define the interpolation faces between rotating and non-rotating regions.

    renumberMesh -overwrite

This commands restructures the mesh for better calculation performance.

    createPatch -overwrite

This command converts the boundary _AMI_ to an arbitrary mesh interface (hence
AMI), where during the simulation the interpolation of fields between rotating
and non-rotating cells will take place (dictionary can be found in
_system/createPatchDict_).

After these steps you can visualize your mesh in ParaView:

![Mesh](tutorial-pimplefoam-ami-rotating-fan-mesh-600.png)

- Use higher refinement on AMI and fan for better mesh resolution.
- Use refinement region defined by AMI for uniform mesh resolution within
  rotating zone.
- This will however increase your calculation time.
{: .note}

Here you can see the rotating and non-rotating regions of the mesh:

![Close-up around moving part](tutorial-pimplefoam-ami-rotating-fan-mesh-close-up-600.png)

## Simulation {#simulation}

Now we move from the folder _mesh_ to the folder _case_.

### Boundary conditions {#boundary-conditions}

- Boundaries in the folder _0_ are defined the following way:
  - door: inlet
  - outlet (window): outlet
  - room: wall
  - desk: wall
  - fan: moving wall

- velocity **U**
  - door: [fixedValue](ref_id://boundary-conditions-fixedValue) with -0.1 m/s
  - outlet (window): [pressureInletOutletVelocity](ref_id://boundary-conditions-pressure-inlet-outlet-velocity)
  - room: [noSlip](ref_id://boundary-conditions-noSlip)
  - desk: [noSlip](ref_id://boundary-conditions-noSlip)
  - fan: [movingWallVelocity](ref_id://boundary-conditions-movingWallVelocity)

- kinematic pressure **p**
  - door: `fixedFluxPressure`
  - outlet (window): [fixedValue](ref_id://boundary-conditions-fixedValue) with atmospheric pressure
    at 0 Pa
  - room: `fixedFluxPressure`
  - desk: `fixedFluxPressure`
  - fan: `fixedFluxPressure`

- turbulent kinetic energy **k**
  - door: `turbulentIntensityKineticEnergyInlet` with 5% turbulence intensity
  - outlet (window): [zeroGradient](ref_id://boundary-conditions-zeroGradient)
    (alternative: [inletOutlet](ref_id://boundary-conditions-inletOutlet)
  - room: `kqRWallFunction`
  - desk: `kqRWallFunction`
  - fan: `kqRWallFunction`

- turbulent dissipation rate **omega**
  - door: `turbulentMixingLengthFrequencyInlet` with 1.2m mixing length
  - outlet (window): [zeroGradient](ref_id://boundary-conditions-zeroGradient)
    (alternative: [inletOutlet](ref_id://boundary-conditions-inletOutlet))
  - room: `omegaWallFunction`
  - desk: `omegaWallFunction`
  - fan: `omegaWallFunction`

- turbulent kinematic viscosity **nut**
  - door: [zeroGradient](ref_id://boundary-conditions-zeroGradient)
  - outlet (window): [zeroGradient](ref_id://boundary-conditions-zeroGradient)
    (alternative: [inletOutlet](ref_id://boundary-conditions-inletOutlet))
  - room: `nutkWallFunction`
  - desk: `nutkWallFunction`
  - fan: `nutkWallFunction`

### Definition of mesh movement {#mesh-movement}

- Dictionary **dynamicMeshDict** can be found in constant.
- Important entries:
  - `cellZone    rotatingZone;` - This was defined in _snappyHexMeshDict_ to
    define the rotating cells.
  - `solidBodyMotionFunction rotatingMotion;` - rotating mesh movement
  - `origin      (-3 2 2.6);` - origin of the axis of rotation of cells (can
    be anywhere on the rotation axis of the fan)
  - `axis        (0 0 1);` - direction of the rotation axis (here: z-direction)
  - `omega       10;` - angular velocity of rotation in rad/s=
    (corresponds to ~95.5 rpm)

- For a realistic fan movement use `omega 20;`
{: .note}

### Additional dictionaries in constant {#constant}

- **g** - gravitational acceleration
- **transportProperties** - definition of kinematic viscosity
- **turbulenceProperties** - definition of turbulence model (here: [kOmegaSST](ref_id://ras-komegasst))

### Simulation settings {#system}

- **controlDict** - runtime settings
  - endTime: 1 s  (approx. 0.64 s per revolution)
  - writeInterval: 0.1 s
  - maxCo: 1
- **decomposeParDict** - parallel setting
- **fvSchemes** - various discretisation schemes
- **fvSolution** - numeric settings (matrix solvers, tolerances, correctors)

### Running the simulation {#running-the-simulation}

The simulation employs the _pimpleFoam_ application, in parallel.

    decomposePar

With this command you divide your mesh and your fields onto multiple processors.

    mpirun -np 4 pimpleFoam -parallel > log.pimpleFoam &

With this command you start your simulation on four cores. If you want to
use a different number of cores you have to change the number (here: 4) to
you choice. Also don't forget to change the settings in
_system/decomposeParDict_!

## Results {#results}

Here you see the flow after _t_ = 0.64 s.

Velocity magnitude and vectors on a y-normal plane:

![Velocity magnitude and vectors](tutorial-pimplefoam-ami-rotating-fan-result4-600.png)

Close-up of velocity magnitude and vectors on a y-normal plane:

![Close-up velocity magnitude and vectors](tutorial-pimplefoam-ami-rotating-fan-result5-600.png)

- For a more developed flow pattern run the simulation until 10-20s.
{: .note}
