---
title: Outlet conditions
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-outlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

## Boundary mesh type {#boundary-mesh-type}

Outlet conditions are specified using the `patch` type entry in the
`$FOAM_CASE/constant/polyMesh/boundary` file:

    <patchName>
    {
        type            patch;
        ...
    }

## General conditions {#general}

Available outlet conditions include

<%= insert_models "bcs-outlet-general" %>

## Velocity conditions {#velocity}

- blocked return flow: see [inlet outlet](ref_id://boundary-conditions-inletOutlet)
- outflow: see [zeroGradient](ref_id://boundary-conditions-zeroGradient).  Note:
  unstable for reverse flow
<%= insert_models "bcs-outlet-velocity" %>

## Pressure conditions {#pressure}

- static pressure: see [fixedValue](ref_id://boundary-conditions-fixedGradient)
<%= insert_models "bcs-outlet-pressure" %>

## Temperature conditions {#temperature}

<%= insert_models "bcs-outlet-temperature" %>

## Turbulence conditions {#turbulence}

- blocked return flow: see [inlet outlet](ref_id://boundary-conditions-inletOutlet)
- outflow: see [zeroGradient](ref_id://boundary-conditions-zeroGradient).  Note:
  unstable for reverse flow

## Further information {#further-information}

Source code:

- [Foam::polyPatch](doxy_id://Foam::polyPatch)
- [Foam::fvPatch](doxy_id://Foam::fvPatch)

See also

- [grpOutletBoundaryConditions](doxy_id://grpOutletBoundaryConditions)
