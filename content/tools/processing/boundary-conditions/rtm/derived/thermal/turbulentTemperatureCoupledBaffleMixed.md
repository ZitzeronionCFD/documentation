---
title: turbulentTemperatureCoupledBaffleMixed
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-turbulentTemperatureCoupledBaffleMixed
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `turbulentTemperatureCoupledBaffleMixed` is a boundary condition that provides
a mixed boundary condition for temperature, to be used for heat-transfer
on back-to-back baffles. Optional thin thermal layer resistances can be
specified through `thicknessLayers` and `kappaLayers` entries.

The condition specifies gradient and temperature such that the equations
are the same on both sides:
- refGradient = zero gradient
- refValue = neighbour value
- mixFraction = nbrKDelta / (nbrKDelta + myKDelta())

where `KDelta` is heat-transfer coefficient K * deltaCoeffs.

The thermal conductivity `kappa` can either be retrieved from various
possible sources, as detailed in the class `temperatureCoupledBase`.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            compressible::turbulentTemperatureCoupledBaffleMixed;
        Tnbr            <word>;

        // Optional entries
        thicknessLayer  <PatchFunction1<scalar>>;
        kappaLayer      <PatchFunction1<scalar>>;
        thicknessLayers <scalarField>;

        // Conditional entries

            // if 'thicknessLayers' entry is set
            kappaLayers  <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "turbulentTemperatureCoupledBaffleMixedFvPatchScalarField.H", "turbulentTemperatureCoupledBaffleMixed" %>
<% assert :string_exists, "turbulentTemperatureCoupledBaffleMixedFvPatchScalarField.C", "Tnbr" %>
<% assert :string_exists, "turbulentTemperatureCoupledBaffleMixedFvPatchScalarField.C", "thicknessLayer" %>
<% assert :string_exists, "turbulentTemperatureCoupledBaffleMixedFvPatchScalarField.C", "kappaLayer" %>
<% assert :string_exists, "turbulentTemperatureCoupledBaffleMixedFvPatchScalarField.C", "thicknessLayers" %>
<% assert :string_exists, "turbulentTemperatureCoupledBaffleMixedFvPatchScalarField.C", "kappaLayers" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `turbulentTemperatureCoupledBaffleMixed` | word    | yes      | -
`Tnbr`                 | Name of field on the neighbour region      | word    | yes      | -
`thicknessLayer`       | Thickness of a single layer                | scalarList | no    | -
`thicknessLayers`      | Thickness of multiple layers               | PatchFunction1\<scalar\> | no | -
`kappaLayer`           | Conductivity of a single layer             | scalarList | no    | -
`kappaLayers`          | Conductivity of multiple layers            | PatchFunction1\<scalar\> | no | -

The inherited entries are elaborated in:

- mappedPatchFieldBase.H
- mixedFvPatchFields.H
- temperatureCoupledBase.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/turbulentTemperatureCoupledBaffleMixed" %>

API:

- [Foam::compressible::turbulentTemperatureCoupledBaffleMixedFvPatchScalarField](doxy_id://Foam::compressible::turbulentTemperatureCoupledBaffleMixedFvPatchScalarField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
