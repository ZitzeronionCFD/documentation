---
title: Mid-point divergence scheme
short_title: Mid-point
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-midpoint
license: CC-BY-NC-ND-4.0
group: schemes-divergence-nvdtvd
---

<%= page_toc %>

## Properties {#properties}

- First/second order
- Unbounded
- Equivalent to [linear](../linear) for isotropic meshes
- Face flux computed using
  $$ \phi_f = 0.5 \left( \phi_c + \phi_d \right) $$

## Normalised Variable Diagram  {#nvd}

![linear](../linear.png)

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss midPoint;
    }

<% assert :string_exists, "midPoint.H", "midPoint" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/schemes/midPoint" %>

API:

- [Foam::midPoint](doxy_id://Foam::midPoint)
