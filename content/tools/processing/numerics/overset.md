---
title: Overset
copyright:
- Copyright (C) 2019-2021 OpenCFD Ltd.
tags:
- overset
menu_id: numerics-overset
license: CC-BY-NC-ND-4.0
menu_parent: numerics
---

<%= page_toc %>

## Overview {#sec-overset-overview}

The overset framework is a generic implementation of *overset* (also referred to
as *Chimera*) meshes, for both static and dynamic cases. Cell-to-cell mappings
between multiple, disconnected mesh regions are employed to generate a
composite domain. This permits complex mesh motions and interactions
without the penalties associated with deforming meshes, for single- and
multiphase flows.

<%= vimeo 223792058, 600, 445 %>

## How does it work? {#sec-overset-how-does-it-work}

Consider the following 2-D case from the tutorials:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/overPimpleDyMFoam/twoSimpleRotors" %>

<%= vimeo 223790883, 600, 300 %>

The mesh comprises three regions; the background mesh:

![Background mesh](overset-twoSimpleRotors-background-mesh-small.png)

and two rotors:

![Rotor 1](overset-twoSimpleRotors-rotor1-mesh-small.png)

![Rotor 2](overset-twoSimpleRotors-rotor2-mesh-small.png)

combined to give:

![Background mesh](overset-twoSimpleRotors-all-meshes-small.png)

Various concepts are used:

- *Donor cells* : cells that provide values
- *Acceptor cells* : these are the cells whose value gets set from interpolation
- *Hole cells* : unreachable/inactive cells
- *Mesh zone* : indicator field to denote potentially overlapping mesh regions
- *Overset patches* : patch type next to cells that are to be interpolated

The mapping determines which parts of the mesh are:

- solved, *calculated*;
- interpolated from solution cells, *interpolated*; and
- not used, *holes*.

For the `twoSimpleRotors` case, the background mesh yields:

![Cell types](overset-twoSimpleRotors-background-mesh-cell-types-small.png)

where the colours correspond to:

- blue: calculated
- white: interpolated
- red: blocked (holes)

## Usage {#sec-overset-usage}

The case must have at least one patch of type `overset`; ideally this is the
first patch.  As a [constraint](ref_id://boundary-conditions-constraint)
type, it will automatically assume a boundary condition type `overset` when
including the default set-up file in the `boundaryField` section of the field
files:

    #includeEtc "caseDicts/setConstraintTypes"

<% assert :file_exists, "caseDicts/setConstraintTypes" %>

Any cells on the outside of the mesh that require interpolation should be on
this patch.  Set the mesh type to `dynamicOversetFvMesh` in the
`dynamicMeshDict` file (located in the [constant](ref_id://case-structure)
directory)

    dynamicFvMesh       dynamicOversetFvMesh;

    solver              displacementLaplacian;

    displacementLaplacianCoeffs
    {
        diffusivity     uniform 1;
    }

<% assert :string_exists, "dynamicOversetFvMesh.H", "dynamicOversetFvMesh" %>
<% assert :string_exists, "displacementLaplacianFvMotionSolver.H", "displacementLaplacian" %>
<% assert :string_exists, "displacementLaplacianFvMotionSolver.C", "diffusivity" %>

Generate a `0/zoneID` `volScalarField` indicating the mesh zone

not to be confused with `cellZones`
{: .note}

This field can usually be generated using standard OpenFOAM
tools, e.g. [topoSet](ref_id://topoSet), *setFields*. It should start at `0`
and be consecutively numbered. There is no limit to the number of zones.

In the <%= link_to_menuid "fvschemes" %> dictionary select the
relevant overset interpolation method and which additional variables should be
included.  By default, all solved-for variables and some solver-specific fields
use overset interpolation.

    oversetInterpolation
    {
        method          inverseDistance;
    }

<% assert :string_exists, "oversetFvPatchField.C", "oversetInterpolation" %>

If necessary adapt the wall distance calculation to a continuous method,
e.g. `Poisson`, `advectionDiffusion`. The default `meshWave` method does not walk
through the overset interpolation.

Update the solver to an asymmetric variant in the
[fvSolution](ref_id://fvsolution) dictionary (the matrix resulting
from interpolation is asymmetric). All asymmetric solvers except from
[GAMG](ref_id://multigrid-gamg) are supported. In practice a good
choice is the [smooth](ref_id://linearequationsolvers-smooth) solver with
[symGaussSeidel](ref_id://smooth-symGaussSeidel)
smoother for transport equations (U, k, etc.) and
[PBiCGStab](ref_id://cg-pbicgstab) with
[DILU](ref_id://cg-preconditioner-dilu) preconditioner for elliptic
equations (p, yPsi).

## Further information {#sec-overset-further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/overset" %>
- <%= repo_link2 "$FOAM_SOLVERS/basic/laplacianFoam/overLaplacianDyMFoam" %>
- <%= repo_link2 "$FOAM_SOLVERS/incompressible/simpleFoam/overSimpleFoam" %>
- <%= repo_link2 "$FOAM_SOLVERS/incompressible/pimpleFoam/overPimpleDyMFoam" %>
- <%= repo_link2 "$FOAM_SOLVERS/multiphase/interFoam/overInterDyMFoam" %>
- <%= repo_link2 "$FOAM_SOLVERS/compressible/rhoPimpleFoam/overRhoPimpleDyMFoam" %>
