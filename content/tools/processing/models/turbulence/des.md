---
title: Detached Eddy Simulation (DES)
short_title: DES
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- DES
- turbulence
menu_id: turbulence-des
license: CC-BY-NC-ND-4.0
menu_parent: models-turbulence
---

<%= page_toc %>

Detached Eddy Simulation relates to a hybrid [RAS](../ras)-[LES](../les)
approach to turbulence modelling, aiming to alleviate the costly near-wall
meshing requirements imposed by LES.

## Options {#options}

<%= insert_models "turbulence-des" %>

## Usage {#usage}

DES is implemented as a subset of [LES](../les), selected by setting the
`simulationType` entry

    simulationType  LES;

    LES
    {
        \\ Model input parameters
    }

<% assert :string_exists, "TurbulenceModel.C", "simulationType" %>
<% assert :string_exists, "LESModel.C", "LES" %>

Suitable for:

- 3-D cases, not appropriate for reduced-dimension cases
- transient, not appropriate for steady state

## Initialisation {#initialisation}

Typically initialised from a precursor calculation, e.g. employing
[RAS](../ras) modelling.

## Wall modelling {#wall-modelling}

- <%= link_to_menuid "ras-wallfunctions" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/DES" %>

Related:

- [Source documentation](doxy_id://grpDESTurbulence)
