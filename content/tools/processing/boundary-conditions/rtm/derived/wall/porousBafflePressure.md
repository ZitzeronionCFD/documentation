---
title: porousBafflePressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-porousBafflePressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `porousBafflePressure` is boundary condition that provides
a jump condition, using the `cyclic` condition as a base.

The porous baffle introduces a pressure jump defined by:

$$
    \Delta p = -(D \mu U + 0.5 I \rho |U|^2 )L
$$

where:

Property | Description
---------|--------------------------------------------
$$p$$    | Pressure \[Pa\]
$$\rho$$ | Density \[kg/m^3\]
$$\mu$$  | Laminar viscosity \[Pa s\]
$$D$$    | Darcy coefficient
$$I$$    | Inertial coefficient
$$L$$    | Porous media length in the flow direction

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            porousBafflePressure;
        D               <Function1<scalar>>;
        I               <Function1<scalar>>;
        length          <scalar>;

        // Optional entries
        phi             <word>;
        rho             <word>;
        uniformJump     <bool>;

        // Inherited entries
        patchType       cyclic;
        jump            uniform 0;
        ...
    }

<% assert :string_exists, "porousBafflePressureFvPatchField.H", "porousBafflePressure" %>
<% assert :string_exists, "porousBafflePressureFvPatchField.C", "D" %>
<% assert :string_exists, "porousBafflePressureFvPatchField.C", "I" %>
<% assert :string_exists, "porousBafflePressureFvPatchField.C", "length" %>
<% assert :string_exists, "porousBafflePressureFvPatchField.C", "phi" %>
<% assert :string_exists, "porousBafflePressureFvPatchField.C", "rho" %>
<% assert :string_exists, "porousBafflePressureFvPatchField.C", "uniformJump" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `porousBafflePressure`          | word    | yes      | -
`D`            | Darcy coefficient                          | Function1\<scalar\> | yes  | -
`I`            | Inertial coefficient                       | Function1\<scalar\> | yes  | -
`length`       | Porous media length in the flow direction  | scalar  | yes      | -
`uniformJump`  | Flag to apply a uniform pressure drop on the patch based | bool | no   | false
`phi`          | Name of flux field                         | word    | no       | phi
`rho`          | Name of density field                      | word    | no       | rho
`patchType`    | Underlying patch type should be `cyclic`   | word    | yes      | -
`jump`         | Jump value                                 | scalarField | yes  | -

The inherited entries are elaborated in:

- fixedJumpFvPatchField.H
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/RAS/damBreakPorousBaffle" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/porousBafflePressure" %>

API:

- [Foam::porousBafflePressureFvPatchField](doxy_id://Foam::porousBafflePressureFvPatchField)

<%= history "2.2.2" %>

<!----------------------------------------------------------------------------->
