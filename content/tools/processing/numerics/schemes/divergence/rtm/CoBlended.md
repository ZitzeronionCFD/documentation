---
title: Courant number blended divergence scheme
short_title: Co blended
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-coblended
license: CC-BY-NC-ND-4.0
group: schemes-divergence-general
---

<%= page_toc %>

## Properties {#properties}

- Blends two schemes based on the local face-based Courant number

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss CoBlended <co1> <scheme1> <co2> <scheme2>;
    }

<% assert :string_exists, "CoBlended.H", "CoBlended" %>

Where

- Co < `co1` : `scheme1`
- Co > `co2` : `scheme2`
- `co1` < Co < `co2` : linear blend between `<scheme1>` and `<scheme2>`

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/schemes/CoBlended" %>

API:

- [Foam::CoBlended](doxy_id://Foam::CoBlended)
