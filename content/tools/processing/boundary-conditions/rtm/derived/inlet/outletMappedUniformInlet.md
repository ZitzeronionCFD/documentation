---
title: outletMappedUniformInlet
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-outletMappedUniformInlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-general
---

<%= page_toc %>

## Description

The `outletMappedUniformInlet` is a generic fixed-value inlet condition that
- averages patch fields of specified "outlet" patches and uniformly applies
the averaged value over a specified inlet patch.
  - optionally, the averaged value can be scaled and/or offset by a specified value,
    and/or mapped by a specified time delay.

The governing equation of the boundary condition is:

$$
  \phi_{inlet} =
    \sum_i \left( f_i \phi_{{outlet}_i} + \phi_{{offset}_i} \right)
$$

where:

Property             | Type
---------------------|-------
$$\phi_{inlet}$$     | Spatially-uniform patch-field value at an inlet patch
$$\phi_{outlet}$$    | Averaged patch-field value at an outlet patch
$$f$$                | User-defined fraction value
$$\phi_{offset}$$    | User-defined offset value
$$i$$                | Outlet-patch index

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            outletMappedUniformInlet;

        outlets
        {
            <outletName.1>
            {
                fraction    <Function1<scalar>>;
                offset      <Function1<Type>>;
                timeDelay   <Function1<scalar>>;
            }
            <outletName.2>
            {
                fraction    <Function1<scalar>>;
                offset      <Function1<Type>>;
                timeDelay   <Function1<scalar>>;
            }
            ...
        }

        // Optional entries
        uniformValue    <PatchFunction1<Type>>;
        phi             phi;

        // Inherited entries
        ...
    }

<% assert :string_exists, "outletMappedUniformInletFvPatchField.H", "outletMappedUniformInlet" %>
<% assert :string_exists, "outletMappedUniformInletFvPatchField.C", "outlets" %>
<% assert :string_exists, "outletMappedUniformInletFvPatchField.C", "fraction" %>
<% assert :string_exists, "outletMappedUniformInletFvPatchField.C", "offset" %>
<% assert :string_exists, "outletMappedUniformInletFvPatchField.C", "timeDelay" %>
<% assert :string_exists, "outletMappedUniformInletFvPatchField.C", "uniformValue" %>
<% assert :string_exists, "outletMappedUniformInletFvPatchField.C", "phi" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `outletMappedUniformInlet`      | word    | yes      | -
`outlets`              | Dictionary name: outlets                   | dict    | yes      | -
`fraction`             | Fraction value                             | Function1\<scalar\> | no   | 1
`offset`               | Offset value                               | Function1\<Type\>   | no   | Zero
`timeDelay`            | Time delay                                 | Function1\<scalar\> | no   | 0
`uniformValue`         | Base inlet patch field                     | PatchFunction1\<Type\> | no | Zero
`phi`                  | Name of operand flux field                 | word    | no   | phi

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::PatchFunction1](doxy_id://Foam::PatchFunction1)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

- Any negative input of `timeDelay` entry is forced to be
  zero without emitting any warnings.
  {: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/reactingParcelFoam/airRecirculationRoom" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/outletMappedUniformInlet" %>

API:

- [Foam::outletMappedUniformInletFvPatchField](doxy_id://Foam::outletMappedUniformInletFvPatchField)

<%= history "2.0.0" %>

<!----------------------------------------------------------------------------->
