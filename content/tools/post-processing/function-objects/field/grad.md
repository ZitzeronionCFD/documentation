---
title: grad
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- gradient
- post-processing
menu_id: function-objects-field-grad
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `grad` function object computes
the gradient of an input field.

The operation is limited to scalar and vector volume or surface fields, and
the output is a volume vector or tensor field.

### Operands

Operand      | Type                                | Location
:---         |:---                                 |:---
input        | `{vol,surface}{Scalar,Vector}Field` | `$FOAM_CASE/<time>/<inpField>`
output file  | -                                   | -
output field | `vol{Vector,Tensor}Field`  | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `grad` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    grad1
    {
        // Mandatory entries (unmodifiable)
        type            grad;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entries (runtime modifiable)
        field           <field>;

        // Optional (inherited) entries
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "grad/grad.H", "grad" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "grad",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: grad                     | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 field        | Name of the operand field           | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func "grad(<field>)"

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/grad" %>

API:

- [Foam::functionObjects::grad](doxy_id://Foam::functionObjects::grad)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
