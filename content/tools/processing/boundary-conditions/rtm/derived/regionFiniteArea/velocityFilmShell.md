---
title: velocityFilmShell
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-velocityFilmShell
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-regionFa
group: bcs-regionFa
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            velocityFilmShell;

        // Optional entries
        zeroWallVelocity  <bool>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "velocityFilmShellFvPatchVectorField.H", "velocityFilmShell" %>
<% assert :string_exists, "velocityFilmShellFvPatchVectorField.C", "zeroWallVelocity" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `velocityFilmShell`             | word    | yes      | -
`zeroWallVelocity` | Flag to enable zero wall velocity      | bool    | no       | true

The inherited entries are elaborated in:

- mixedFvPatchFields.H
- liquidFilmBase.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/kinematicParcelFoam/pitzDailyWithSprinklers" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/regionFaModels/derivedFvPatchFields/filmShell" %>

API:

- [Foam::velocityFilmShellFvPatchVectorField](doxy_id://Foam::velocityFilmShellFvPatchVectorField)

<%= history "v2112" %>

<!----------------------------------------------------------------------------->
