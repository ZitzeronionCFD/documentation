---
title: Peng-Robinson gas
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- equation of state
menu_id: equationOfState-PengRobinsonGas
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-equationOfState
group: equation-of-state-models
---

<%= page_toc %>

The Peng-Robinson <%= cite "peng_new_1976" %> equation of state for gases.

Density, $$ \rho $$ \[kg/m$$^3$$\]

$$
\rho = \frac{p}{Z R T}
$$

Here, $$ Z $$ is computed from the cubic equation:

$$
Z^3 - (1 - B)Z^2 + (A - 2B - 3B^2)Z - (AB - B^2 - B^3) = 0
$$

Where:

$$
A = \frac{\alpha a p}{R^2 T^2}; B = \frac{bp}{RT}.
$$

with:

- $$ a = 0.45724 \frac{R^2 T_c^2}{p_c} $$
- $$ b = 0.07780 \frac{R T_c}{p_c} $$
- $$ \alpha = \left(1 + \kappa (1 - T_r^{0.5}) \right)^2 $$
- $$ \kappa = 0.37464 + 1.564226 \omega - 0.26992 \omega^2 $$
- $$ T_r = \frac{T}{T_c} $$

Entropy, $$ S $$ \[J/kg/K \]

$$
S = ...
$$

Compressibility, $$ \psi $$ \[s$$^2$$/m$$^2$$\]

$$
\psi = \frac{1}{Z R T}
$$

## Usage

The `PengRobinsonGas` equation of state is specified as:

~~~
equationOfState
{
    Tc          <scalar>;
    Vc          <scalar>;
    Pc          <scalar>;
    omega       <scalar>;
}
~~~

Where

- `Tc` : Critical temperature \[K\]
- `Vc` : Critical volume \[m$$^3$$/kmol\]
- `Pc` : Critical pressure \[Pa\]
- `omega` : Accentric factor \[-\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/equationOfState/PengRobinsonGas" %>
