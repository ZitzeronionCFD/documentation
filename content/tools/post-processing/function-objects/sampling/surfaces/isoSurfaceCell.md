---
title: isoSurfaceCell
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- post-processing
- sampling
- surface
menu_id: surfaces-isoSurfaceCell
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-sampling-surfaces
group: sampling-surfaces
---

<%= page_toc %>

Creates iso-surfaces according to constant field values on a mesh.

## Usage {#usage}

Basic operation of the `isoSurfaceCell` surface comprises:

    surfaces1
    {
        type            surfaces;
        libs            (sampling);

        surfaces
        {
            mySurface1
            {
                type        isoSurfaceCell;
                isoField        <field-name>;
                isoValues       (<field-values>);

                // Optional entries
                average         false;
                triangulate     false;
                simpleSubMesh   false;

                regularise      ;
                snap            true;
                mergeTol        ;
                bounds          ;
            }
        }
    }

<% assert :string_exists, "sampledSurfaces.H", "surfaces" %>
<% assert :class_library_exists, "sampling/Make/files", "sampledSurfaces", "libsampling" %>
<% assert :string_exists, "isoSurfaceCell.H", "isoSurfaceCell" %>

## Sample output {#sample-output}

## Further information {#further-information}

See also

- [isoSurface](../isoSurfaceCell)

Source code:

- <%= repo_link2 "$FOAM_SRC/sampling/surface/isoSurface/isoSurfaceCell.H" %>

API

- [Foam::isoSurfaceCell](doxy_id://Foam::isoSurfaceCell)

Example usage:

- <%= repo_link2("$FOAM_TUTORIALS/verificationAndValidation/multiphase/interIsoFoam/porousDamBreak") %>
