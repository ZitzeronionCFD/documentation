---
title: Linear-upwind divergence scheme
short_title: Linear upwind
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-linearupwind
license: CC-BY-NC-ND-4.0
group: schemes-divergence-nvdtvd
---

<%= page_toc %>

## Properties {#properties}

- Employs [upwind](../upwind) interpolation weights,
  with an explicit correction based on the local cell gradient
- Second order
- Unbounded
- As shown by *Warming and beam* <%= cite "warming_upwind_1976" %>

## Normalised Variable Diagram {#nvd}

![linearUpwind](../linearUpwind.png)

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss linearUpwind grad(U);
    }

<% assert :string_exists, "surfaceInterpolation/schemes/linearUpwind/linearUpwind.H", "linearUpwind" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/schemes/linearUpwind" %>

API:

- [Foam::linearUpwind](doxy_id://Foam::linearUpwind)
