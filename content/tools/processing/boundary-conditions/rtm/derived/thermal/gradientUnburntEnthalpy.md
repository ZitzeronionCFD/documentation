---
title: gradientUnburntEnthalpy
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-gradientUnburntEnthalpy
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `gradientUnburntEnthalpy` is a boundary condition that provides
a gradient boundary condition for unburnt enthalpy.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            gradientUnburntEnthalpy;

        // Inherited entries
        ...
    }

<% assert :string_exists, "gradientUnburntEnthalpyFvPatchScalarField.H", "gradientUnburntEnthalpy" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `gradientUnburntEnthalpy`       | word    | yes      | -

The inherited entries are elaborated in:

- fixedGradientFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/reactionThermo/derivedFvPatchFields/gradientUnburntEnthalpy" %>

API:

- [Foam::gradientUnburntEnthalpyFvPatchScalarField](doxy_id://Foam::gradientUnburntEnthalpyFvPatchScalarField)

<%= history "1.6" %>

<!----------------------------------------------------------------------------->
