---
title: electrostaticDeposition
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-electrostaticDeposition
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `electrostaticDeposition` is a boundary condition to
calculate electric potential (`V`) on a given boundary
based on film thickness (`h`) and film resistance (`R`) fields
which are updated based on a given patch-normal current density
field (`jn`), Coulombic efficiency and film resistivity.

The governing equations are as follows:

$$
    j_n = - \sigma \nabla^\perp_p V = - \sigma (\vec{n}\cdot(\nabla V)_p)
$$

$$
    \frac{dh}{dt} = C_{eff} (j_n - j_{min})
$$

$$
    \frac{dR}{dt} = \rho \frac{dh}{dt} = \rho C_{eff} (j_n - j_{min})
$$

$$
    V_{film}^n = V_{film}^o + j_n R_\Delta
$$

$$
    V_{body} = j_n R_{body}
$$

$$
    V_p^n = V_i + V_{body} + V_{film}^n
$$

where:

Property             | Description
---------------------|-------
$$j_n$$              | Patch-normal current density        \[A/m^2\]
$$V_p^n$$            | Electric potential on film-fluid interface \[volt = kg m^2/(A s^3)\]
$$V_p^o$$            | Previous time-step electric potential on the interface \[volt\]
$$V_{film}$$         | Electric potential due to film resistance           \[volt\]
$$V_{body}$$         | Electric potential due to body resistance           \[volt\]
$$V_i$$              | Initial electric potential                          \[volt\]
$$R_\Delta$$         | Film resistance (finite increment)  \[ohm m^2 = kg m^4/(A^2 s^3)\]
$$R_{body}$$         | Body resistance                    \[ohm m^2 = kg m^4/(A^2 s^3)\]
$$\rho$$             | Isotropic film resistivity          \[ohm m = kg m^3/(A^2 s^3)\]
$$h$$                | Film thickness                      \[m\]
$$C_{eff}$$          | Volumetric Coulombic efficiency     \[m^3/(A s)\]
$$j_{min}$$          | Minimum current density for deposition onset  \[A/m^2\]
$$\sigma$$           | Isotropic conductivity of mixture   \[S/m = A^2 s^3/(kg m^3)\]
$$\vec{n}$$          | Patch-normal unit vector            \[-\]

## Usage {#usage}

The condition requires entries in both the `boundary` and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    electrostaticDeposition;
        h                       <scalarField>;
        CoulombicEfficiency     <PatchFunction1>;
        resistivity             <PatchFunction1>;

        // Conditional mandatory entries

            // Option-1: single-phase
            sigma       <scalar>;

            // Option-2: multiphase
            phases
            {
                alpha.air
                {
                    sigma       <scalar>;
                }
                alpha.water
                {
                    sigma       <scalar>;
                }
                alpha.mercury
                {
                    sigma       <scalar>;
                }
                ...
            }

        // Optional entries
        jMin                    <scalar>;
        qMin                    <scalar>;
        Rbody                   <scalar>;
        Vi                      <scalar>;
        Vanode                  <scalar>;
        qCumulative             <scalarField>;

        // Inherited entries
        ...
    }

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `electrostaticDeposition`       | word    | yes      | -
`h`                    | Film thickness                             | scalarField | yes  | -
`CoulombicEfficiency`  | Coulombic efficiency                       | PatchFunction1\<scalar\> | yes  | -
`resistivity`          | Isotropic film resistivity                 | PatchFunction1\<scalar\> | yes  | -
`sigma`                | Isotropic electrical conductivity of phase | scalar  | yes      | -
`jMin`                 | Minimum current density for deposition onset | scalar | no      | 0
`qMin`                 | Minimum accumulative specific charge for deposition onset | scalar | no | 0
`Rbody`                | Resistance due to main body and/or pretreatment layers    | scalar | no | 0
`Vi`                   | Initial electric potential                 | scalar  | no       | 0
`Vanode`               | Anode electric potential                   | scalar  | no       | GREAT
`qCumulative`          | Accumulative specific charge \[A s/m^2\]   | scalarField | no   | 0

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::PatchFunction1](doxy_id://Foam::PatchFunction1)
<!-- end of the list -->

- Depletion or abrasion of material due to negative current is not allowed.
- When accumulative specific charge (`qCumulative`) is less than minimum
accumulative specific charge (`qMin`), no deposition occurs.
- Boundary-condition updates are not allowed during outer corrections
to prevent spurious accumulation of film thickness.
- `resistivity`, `jMin`, `qMin` and `Rbody` are always non-negative.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/RAS/electrostaticDeposition" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/electrostaticDeposition" %>

API:

- [Foam::electrostaticDepositionFvPatchScalarField](doxy_id://Foam::electrostaticDepositionFvPatchScalarField)

<%= history "v2112" %>

<!----------------------------------------------------------------------------->
