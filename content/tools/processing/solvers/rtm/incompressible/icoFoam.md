---
title: icoFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- solvers
- incompressible
menu_id: icofoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-incompressible
group: solvers-incompressible
solver_models:
- transient
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-incompressible" %>
- transient
- incompressible

## Equations {#equations}

The solver uses the [PISO](ref_id://pv-piso) algorithm to solve the continuity
equation:

$$
    \div \u = 0
$$

and momentum equation:

$$
    \ddt{\u}
  + \div ( \u \otimes \u )
  - \div ( \nu \grad \u )
 =
  - \grad p
$$

Where:

$$\u$$
: Velocity

$$p$$
: Kinematic pressure

## Input requirements {#input-requirements}

Mandatory fields:

- p: <%= link_to_menuid "algorithm-kinematic-pressure" %> \[m2/s2\]
- U: velocity \[m/s\]

### Physical models {#input-requirements-physical-models}

- None

### Solution controls {#input-requirements-solution-controls}

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

## Further information {#input-requirements-further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/incompressible/icoFoam" %>

See also

- <%= link_to_menuid "pimplefoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/icoFoam" %>
