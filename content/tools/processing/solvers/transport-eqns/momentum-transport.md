---
title: Momentum transport equation
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
menu_id: momentum-transport
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

The momentum transport equation is as follows:

$$
    \frac{\partial \rho \u}{\partial t}
  + \div (\u \u)
  + f_{MRF}(\rho, \u)
  + \div \vec{R}
    =
  - \grad p_{rgh}
  - (\vec{g} \cdot \vec{h} - (\vec{gh})_{\ref}) \grad \rho
  + \vec{S}_{\rho, \u}
$$

where:

Property  | Description
----------|-------------
$$t$$     | Time
$$\rho$$  | Density
$$\u$$    | Velocity
$$f_{MRF}$$ | Multiple reference frame function
$$\vec{R}$$ | Stress tensor
$$p_{rgh}$$ | Pressure excluding the hydrostatic contribution
$$\vec{h}$$  | Spatial coordinates
$$\vec{g}$$     | Gravitational acceleration
$$\vec{gh}_{ref}$$ | Reference hydrostatic pressure
$$\vec{S}$$ | Source term through `fvOption`


The pressure-term transformation involving the
hydrostatic contribution is explained as follows:

- <%= link_to_menuid "algorithm-p-rgh" %>


<!----------------------------------------------------------------------------->
