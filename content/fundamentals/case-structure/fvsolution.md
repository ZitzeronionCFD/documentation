---
title: fvSolution
copyright:
- Copyright (C) 2021 OpenCFD Ltd.
tags:
- navigation
- system
- fvSolution
menu_id: fvsolution
license: CC-BY-NC-ND-4.0
menu_parent: case-structure
---

<%= page_toc %>

<%= repo_file_snippet "icoFoam/cavity/cavity/system/fvSolution" %>
