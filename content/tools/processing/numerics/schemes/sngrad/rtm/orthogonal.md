---
title: Orthogonal surface-normal gradient scheme
short_title: Orthogonal
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- gradient
menu_id: schemes-sngrad-orthogonal
license: CC-BY-NC-ND-4.0
menu_parent: schemes-sngrad
group: schemes-sngrad
---

<%= page_toc %>

The vector $$ \vec{d} $$ joining two cell centres is parallel to the
normal direction of the inter-connecting face.

$$
    \snGrad Q = \frac{Q_P - Q_N}{| \vec{d} |}
$$

![Schematic](../schemes-sngrad-orthogonal.png)

## Properties {#properties}

- Calculated using a central difference scheme using the mesh delta coefficients
- Any non-orthogonality is ignored

## Usage {#usage}

The scheme is specified using:

    snGradSchemes
    {
        default         none;
        snGrad(Q)       orthogonal;
    }

## Further information {#further-information}

Source code

- [Foam::fv::orthogonalSnGrad](doxy_id://Foam::fv::orthogonalSnGrad)
