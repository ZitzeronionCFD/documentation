---
title: reference
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- reference
menu_id: function-objects-field-reference
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `reference` function object
computes a field whose values are offset to a reference
value obtained by sampling the field at a user-specified location.

The field values are calculated using:

$$
    \text{ref}_c = s(f_{c}(t) - f_p + f_{off})
$$

where:

$$\text{ref}_c$$
: Field values at cell

$$s$$
: Optional scale factor (default = 1)

$$f_{c}(t)$$
: Current field values at cell at this time

$$f_p$$
: Field value at position

$$f_{off}$$
: Offset field value (default = 0)

### Operands

Operand      | Type                       | Location
:---         |:---                        |:---
input        | `{vol,surface}<Type>Field` | `$FOAM_CASE/<time>/<inpField>`
output file  | -                          | -
output field | `{vol,surface}<Type>Field` | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `reference` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    reference1
    {
        // Mandatory entries (unmodifiable)
        type            reference;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entries (runtime modifiable)
        field           <inpField>;

        // Optional entries (runtime modifiable)
        position    (0 0 0);
        scale       1.0;
        offset      0.0;
        interpolationScheme    cell;

        // Optional (inherited) entries
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "reference.H", "reference" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files", "reference", "libfieldFunctionObjects" %>
<% assert :string_exists, "reference.C", "position" %>
<% assert :string_exists, "reference.C", "scale" %>
<% assert :string_exists, "reference.C", "offset" %>
<% assert :string_exists, "reference.C", "interpolationScheme" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: reference                | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 field        | Name of the operand field           | word | yes      | -
 position     | Position to sample                  | vector | no | \<not used\>
 scale        | Scale value                         | scalar | no     | 1.0
 offset       | Offset value                        | scalar | no     | 0.0
 interpolationScheme  | Sampling scheme             | word   | no     | cell

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func "reference(<field>)"

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/reference" %>

API:

- [Foam::functionObjects::reference](doxy_id://Foam::functionObjects::reference)

<%= history "v1806" %>

<!----------------------------------------------------------------------------->
