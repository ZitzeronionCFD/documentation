---
title: Preconditioned conjugate gradient (PCG)
short_title: PCG
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- conjugate gradient
- solvers
- PCG
menu_id: cg-pcg
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-cg
group: solvers-cg
---

<%= page_toc %>

## Properties {#properties}

- symmetric matrices
- run-time selectable [preconditioner](<%= ref_id "linearequationsolvers-cg", "preconditioners" %>)
- good parallel scaling

## Usage {#usage}

    solver          PCG;
    preconditioner  <conditioner>;
    relTol          <relative tolerance>;
    tolerance       <tolerance>;

## Details {#details}

### Operation {#details-operation}

### Computation cost {#details-computational-cost}

Start-up:

- 1 matrix-vector multiply
- 1 parallel reduction

Per iteration

- 1 matrix-vector multiply
- 3 parallel reductions
- and preconditioner - applied 1x

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/solvers/PCG" %>

API:

- [Foam::PCG](doxy_id://Foam::PCG)
