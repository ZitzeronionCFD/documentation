---
title: Williams-Landel-Ferry
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- transport
menu_id: transport-logPolynomial
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-transport
group: transport-models
---

<%= page_toc %>

Transport model based on the work of Williams et. al.
<%= cite "williams_temperature_1955" %>.

Dynamic viscosity, $$ \mu $$ \[Pa.s\]

$$
\mu = \mu_0 \exp \left( - \frac{C_1 ( T - T_r )}{C_2 + T - T_r} \right)
$$

Thermal conductivity, $$ \kappa $$ \[W/mK\]

$$
\kappa = \frac{c_p \mu}{Pr}
$$

Thermal diffusivity, $$ \alpha_h $$ \[kg/ms\]

$$
\alpha_h = \frac{\mu}{Pr}
$$

## Usage

The `WLF` transport model is defined using the entries:

~~~
transport
{
    mu0             <scalar>;
    Tr              <scalar>;
    C1              <scalar>;
    C2              <scalar>;
    Pr              <scalar>;
}
~~~

Where

- `mu0` : Dynamic viscosity at the reference temperature \[Pa.s\]
- `Tr` : Reference temperature \[K\]
- `C1` : Model coefficient \[\]
- `C2` : Model coefficient \[K\]
- `Pr` : Prandtl number

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/transport/WLF" %>
