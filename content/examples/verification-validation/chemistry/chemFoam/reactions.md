---
title: Reactions
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- chemistry
- example
- reactions
- validation
- verification
menu_id: verification-validation-chemfoam-reactions
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-chemistry
group: verification-validation-chemistry
---

<%= page_toc %>

## Overview {#sec-overview}

- Solver: [chemFoam](ref_id://chemfoam)

## Results {#sec-results}

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/chemFoam/gri" %>

  ![GRI](gri-400x400.png)

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/chemFoam/h2" %>

  ![H2](h2-400x400.png)

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/chemFoam/ic8h18" %>

  ![C8H18](ic8h18-400x400.png)

<!-- - <%= repo_link2 "$FOAM_TUTORIALS/combustion/chemFoam/ic8h18_TDAC" %> -->

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/chemFoam/nc7h16" %>

  ![C7H16](nc7h16-400x400.png)
