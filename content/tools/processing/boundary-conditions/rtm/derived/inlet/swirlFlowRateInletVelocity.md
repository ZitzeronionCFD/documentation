---
title: swirlFlowRateInletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-swirlFlowRateInletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `swirlFlowRateInletVelocity` is a velocity boundary condition
that provides a volumetric- or mass-flow normal vector
boundary condition by its magnitude as an integral over its area with a
swirl component determined by the angular speed, given in revolutions per
minute (RPM).

The basis of the patch (volumetric or mass) is determined by the
dimensions of the flux, `phi`. The current density is used to correct the
velocity when applying the mass basis.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type           swirlFlowRateInletVelocity;
        flowRate       <Function1<scalar>>;
        rpm            <Function1<scalar>>;

        // Optional entries
        phi            <word>;
        rho            <word>;
        origin         <vector>;
        axis           <vector>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "swirlFlowRateInletVelocityFvPatchVectorField.H", "swirlFlowRateInletVelocity" %>
<% assert :string_exists, "swirlFlowRateInletVelocityFvPatchVectorField.C", "flowRate" %>
<% assert :string_exists, "swirlFlowRateInletVelocityFvPatchVectorField.C", "rpm" %>
<% assert :string_exists, "swirlFlowRateInletVelocityFvPatchVectorField.C", "phi" %>
<% assert :string_exists, "swirlFlowRateInletVelocityFvPatchVectorField.C", "rho" %>
<% assert :string_exists, "swirlFlowRateInletVelocityFvPatchVectorField.C", "origin" %>
<% assert :string_exists, "swirlFlowRateInletVelocityFvPatchVectorField.C", "axis" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `swirlFlowRateInletVelocity`    | word    | yes      | -
`flowRate`             | Flow rate profile                          | Function1\<scalar\> | yes | -
`rpm`                  | Rotational speed profile                   | Function1\<scalar\> | yes | -
`phi`                  | Name of the flux field                     | word    | no       | phi
`rho`                  | Name of the density field                  | word    | no       | rho
`origin`               | Origin of rotation                         | vector  | no       | Zero
`axis`                 | Axis of rotation                           | vector  | no       | Zero

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

- The value is positive into the domain.
  {: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/swirlFlowRateInletVelocity" %>

API:

- [Foam::swirlFlowRateInletVelocityFvPatchVectorField](doxy_id://Foam::swirlFlowRateInletVelocityFvPatchVectorField)

<%= history "1.7.0" %>

<!----------------------------------------------------------------------------->
