---
title: searchableSurfaceWithGaps
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: searchablesurfacewithgaps
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh-geometry
group: searchableobjects
---

<%= page_toc %>

A _searchableSurface_ that employs multiple slightly perturbed underlying
surfaces to help prevent the mesh from leaking though small gaps.

## Usage

The surface must be defined in the
<%= link_to_menuid "snappyhexmesh-geometry" %> dictionary, to which a
perturbation is applied to identify (and close) gaps.

    sphere.stl
    {
        type            triSurfaceMesh;
    }

    sphere
    {
        type            searchableSurfaceWithGaps;

        // Underlying surface
        surface         sphere.stl;

        // Perturbation distance
        gap             1e-3;
    }
