---
title: gradientEnergy
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-gradientEnergy
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `gradientEnergy` is a boundary condition that provides
a gradient condition for internal energy.

The gradient is calculated using:

$$
    \nabla(e_p) = \nabla_\perp C_p(p, T) + \frac{e_p - e_c}{\Delta}
$$

where:

Property         | Description
-----------------|--------------
$$e_p$$          | Energy at patch faces \[J\]
$$e_c$$          | Energy at patch internal cells \[J\]
$$p$$            | Pressure \[bar\]
$$T$$            | Temperature \[K\]
$$C_p$$          | Specific heat \[J/kg/K\]
$$\Delta$$       | Distance between patch face and internal cell centres \[m\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            gradientEnergy;

        // Inherited entries
        ...
    }

<% assert :string_exists, "gradientEnergyFvPatchScalarField.H", "gradientEnergy" %>

where:

Property | Description                  | Type    | Required | Default
---------|------------------------------|---------|----------|---------
`type`   | Type name: `gradientEnergy` ` | word    | yes      | -

The inherited entries are elaborated in:

- fixedGradientFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/laminar/contaminatedDroplet2D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/basic/derivedFvPatchFields/gradientEnergy" %>

API:

- [Foam::gradientEnergyFvPatchScalarField](doxy_id://Foam::gradientEnergyFvPatchScalarField)

<%= history "2.1.1" %>

<!----------------------------------------------------------------------------->
