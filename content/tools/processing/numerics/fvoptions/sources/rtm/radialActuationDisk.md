---
title: Radial actuation disk
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- disk
- fvOption
- momentum
- source
menu_id: fvoption-radialacuationdisk
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-momentum
---

<%= page_toc %>

## Properties {#properties}

- Variant of the <%= link_to_menuid "fvoption-acuationdisk" %> source that
  includes radial thrust

## Usage {#usage}

The option is specified using:

    disk1
    {
        type            radialActuationDiskSource;
        selectionMode   cellSet;
        cellSet         radialActuationDisk1;
        fields          (U);

        diskDir         (-1 0 0);   // disk direction
        Cp              0.1;        // power coefficient
        Ct              0.5;        // thrust coefficient
        diskArea        5.0;        // disk area
        coeffs          (0.1 0.5 0.01); // radial distribution coefficients
        upstreamPoint   (0 0 0);    // upstream point
   }

<% assert :string_exists, "radialActuationDiskSource.H", "radialActuationDiskSource" %>
<% assert :string_exists, "radialActuationDiskSource.H", "coeffs" %>

## Further information {#further-information}

Tutorials

- *none*

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/derived/radialActuationDiskSource" %>

See also

- <%= link_to_menuid "fvoption-acuationdisk" %>
