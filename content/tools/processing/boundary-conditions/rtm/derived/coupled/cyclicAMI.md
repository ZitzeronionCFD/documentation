---
title: Cyclic Arbitrary Mesh Interface (AMI)
short_title: Cyclic AMI
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
- coupled
menu_id: boundary-conditions-cyclicami
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-coupled
group: bcs-coupled
---

<%= page_toc %>

## Properties {#properties}

- Coupling condition between a pair of patches that share the same outer bounds,
  but whose inner construction may be dissimilar.

## Usage {#usage}

The condition requires entries in both the `boundary` and field files. Mesh

`boundary` file:

    <patchName>
    {
        type            cyclicAMI;
        neighbourPatch  <coupled patch name>;
        transform       <transform type>;
        ...
    }

<% assert :string_exists, "cyclicAMIPolyPatch.H", "cyclicAMI" %>
<% assert :string_exists, "cyclicAMIPolyPatch.C", "neighbourPatch" %>
<% assert :string_exists, "coupledPolyPatch.C", "transform" %>

Field file:

    <patchName>
    {
        type            cyclicAMI;
    }

<% assert :string_exists, "cyclicAMIPolyPatch.H", "cyclicAMI" %>

The `transform` describes the operation required to map the `neighbour` patch
on to the `owner` patch.  Options include:

- `noOrdering`: no mapping defined, i.e. the operation is determined by the
  patch
- `coincidentFullMatch`: no transform defined and checks that the patch faces
  are matched
- `rotational`: rotational about an axis of rotation
- `translational`: translational

<% assert :string_exists, "coupledPolyPatch.C", "noOrdering" %>
<% assert :string_exists, "coupledPolyPatch.C", "coincidentFullMatch" %>
<% assert :string_exists, "coupledPolyPatch.C", "rotational" %>
<% assert :string_exists, "coupledPolyPatch.C", "translational" %>

## Further information {#further-information}

Related:

- [Cyclic condition](../cyclic)

Source code:

- [Foam::cyclicAMIPolyPatch](doxy_id://Foam::cyclicAMIPolyPatch)
- [Foam::cyclicAMIFvPatch](doxy_id://Foam::cyclicAMIFvPatch)
- [Foam::cyclicAMIFvPatchField](doxy_id://Foam::cyclicAMIFvPatchField)

Example usage:

- pimpleFoam `mixerVesselAMI2D` tutorial:
  <%= repo_link2(
    "$FOAM_TUTORIALS/incompressible/pimpleFoam/laminar/mixerVesselAMI2D") %>
- pimpleFoam `propeller` tutorial:
  <%= repo_link2("$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/propeller") %>
- compressibleInterFoam `sphereDrop` tutorial:
  <%= repo_link2(
    "$FOAM_TUTORIALS/multiphase/compressibleInterDyMFoam/laminar/sphereDrop") %>

\ofAssert patchTypeExists incompressible/pimpleFoam/laminar/mixerVesselAMI2D cyclicAMI
\ofAssert patchTypeExists incompressible/pimpleFoam/RAS/propeller cyclicAMI
\ofAssert patchTypeExists multiphase/compressibleInterDyMFoam/laminar/sphereDrop cyclicAMI
