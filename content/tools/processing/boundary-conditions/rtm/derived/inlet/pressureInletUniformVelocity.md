---
title: pressureInletUniformVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-pressureInletUniformVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `pressureInletUniformVelocity` is a velocity boundary condition that
calculates uniform inflow velocity by averaging the flux over the patch,
and then applying it in the direction normal to the patch faces.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                pressureInletUniformVelocity;

        // Inherited entries
        ...
    }

<% assert :string_exists, "pressureInletUniformVelocityFvPatchVectorField.H", "pressureInletUniformVelocity" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `pressureInletUniformVelocity` | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::pressureInletVelocityFvPatchVectorField](doxy_id://Foam::pressureInletVelocityFvPatchVectorField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/pressureInletUniformVelocity" %>

API:

- [Foam::pressureInletUniformVelocityFvPatchVectorField](doxy_id://Foam::pressureInletUniformVelocityFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
