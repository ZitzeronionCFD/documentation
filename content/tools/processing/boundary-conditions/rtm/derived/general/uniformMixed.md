---
title: uniformMixed
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-uniformMixed
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `uniformMixed` is a mixed-type boundary condition that
mix a uniform fixed value and a niform patch-normal
gradient condition. The term "uniform" is a legacy name since the
prescribed values were previously spatially uniform across that patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                uniformMixed;

        // Optional entries
        uniformValue        <PatchFunction1<Type>>;
        uniformGradient     <PatchFunction1<Type>>;

        // Conditional entries

            // if both 'uniformValue' and 'uniformGradient' entries are set
            uniformValueFraction    <PatchFunction1<Type>>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "uniformMixedFvPatchField.H", "uniformMixed" %>
<% assert :string_exists, "uniformMixedFvPatchField.C", "uniformValue" %>
<% assert :string_exists, "uniformMixedFvPatchField.C", "uniformGradient" %>
<% assert :string_exists, "uniformMixedFvPatchField.C", "uniformValueFraction" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `uniformMixed` | word    | yes      | -
`uniformValue`     | Uniform fixed values      | PatchFunction1\<Type\> | no | -
`uniformGradient`  | Uniform gradient values   | PatchFunction1\<Type\> | no | -
`uniformValueFraction` | Fraction values       | PatchFunction1\<Type\> | conditional | -

The inherited entries are elaborated in:

- mixedFvPatchField.H
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/RAS/TJunctionAverage" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/uniformMixed" %>

API:

- [Foam::uniformMixedFvPatchField](doxy_id://Foam::uniformMixedFvPatchField)

<%= history "v2306" %>

<!----------------------------------------------------------------------------->
