---
title: Boussinesq
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- Boussinesq
- regular
- waves
menu_id: waves-regular-boussinesq
license: CC-BY-NC-ND-4.0
menu_parent: models-waves
group: wave-solitary
---

<%= page_toc %>

## Properties {#properties}

- Solitary wave

## Model equations {#model-equations}

The wave height is modelled by the equation:

$$
    \eta = H \left[ \mathrm{sech} \sqrt{\frac{3}{4} \frac{H}{h} \frac{x - c t}{h}}\right]^2
$$

Where:

$$H$$
: wave height

$$h$$
: water depth

$$c$$
: wave celerity

$$t$$
: time

## Default model coefficients {#default-coeffs}

## Usage {#usage}

Inlet patch example

    <patch>
    {
        alpha           alpha.water;
        waveModel       Boussinesq;
        nPaddle         1;
        waveHeight      0.3;
        waveAngle       0.0;
        activeAbsorption yes;
        wavePeriod      0.0;
    }

## Further information {#further-information}

Source code:

- [Foam::waveModels::Boussinesq](doxy_id://Foam::waveModels::Boussinesq)

References:

- ...

Tutorials:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/solitary" %>
