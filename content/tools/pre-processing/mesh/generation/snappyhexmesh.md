---
title: snappyHexMesh
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry
- snappyHexMesh
- meshing
menu_id: snappyhexmesh
license: CC-BY-NC-ND-4.0
menu_parent: mesh-generation
---

<%= page_toc %>

## Overview {#overview}

`snappyHexMesh` is a fully parallel, split hex, mesh generator that
guarantees a minimum mesh quality.  Controlled using OpenFOAM
dictionaries, it is particularly well suited to batch driven operation.

Key features:

- starts from any pure hex mesh (structured or unstructured)
- reads geometry in triangulated formats, e.g. in `stl`, `obj`, `vtk`
- no limit on the number of input surfaces
- can use simple analytically-defined geometry, e.g. box, sphere, cone
- generates prismatic layers
- scales well when meshing in parallel
- can work with *dirty surfaces*, i.e. non-watertight surfaces

[Quick reference](quick-reference)

## Meshing process {#meshing-process}

The overall meshing process is summarised by the figure below:

![snappyHexMesh overview](figures/snappyHexMesh-overview-small.png)

This includes:

- creation of the background mesh using the <%= link_to_menuid "blockmesh" %> utility
  (or any other hexahedral mesh generator)
- extraction of features on the surfaces with `surfaceFeatureExtract`
  utility
- setting up the `snappyHexMeshDict` input dictionary
- running `snappyHexMesh` in serial or parallel

Evolution of the snappy mesh for the `iglooWithFridges` tutorial is shown below:

<%= vimeo 205239146, 600, 400 %>

## Configuration {#configuration}

Meshing controls are set in the `snappyHexMeshDict` located in the case
`system` directory.  This has five main sections, described by the following:

- [Geometry](geometry): specification of the input surfaces
- [Castellation](castellation): starting from any pure hex mesh, refine and
  optionally load balance when running in parallel.  The refinement is specified
  both according to surfaces, volumes and gaps
- [Snapping](snapping): guaranteed mesh quality whilst morphing to geometric
  surfaces and features
- [Layers](layers): prismatic layers are inserted by shrinking an existing
  mesh and creating an infill, subject to the same mesh quality constraints
- [Mesh quality](meshquality): mesh quality settings enforced during the
  [snapping](snapping) and [layer addition](layers) phases
- [Global](global)

## Usage {#usage}

The command `snappyHexMesh` executes the meshing process.  Adding the `-help`
option, i.e.

    snappyHexMesh -help

returns the list of available options:

<%= help_util "snappyHexMesh" %>

By default, each of `snappyHexMesh`'s phases, i.e castellation, snapping and
layer addition will write a complete mesh in time folders.  This behaviour can
be suppressed by using the option

    snappyHexMesh `-overwrite`
