---
title: Buoyant cavity
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- buoyancy
- cavity
- example
- verification
- validation
menu_id: verification-validation-heat-transfer-buoyant-cavtiy
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-heat-transfer
group: verification-validation-heat-transfer
---

<%= page_toc %>

## Overview {#sec-overview}

- Solver: [buoyantSimpleFoam](ref_id://buoyantSimpleFoam)
- Investigation into natural convection in a heat cavity
- Experimental case described by Betts and Bokhari
  <%= cite "betts_experiments_2000" %>
- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantSimpleFoam/buoyantCavity" %>

The case is described in the following figure

![Case description](boundary-conditions-small.png)

A temperature difference of 19.6K is maintained between the hot and cold;
the remaining patches are treated as adiabatic.

## Mesh {#sec-mesh}

- 3D structured mesh created using [blockMesh](ref_id://blockmesh)

## Results {#sec-results}

Results are presented for a selection of y/H locations

### Velocity distributions {#sec-results-velocity}

| ![](U1-400x400.png) | ![](U3-400x400.png) |
| ![](U4-400x400.png) | ![](U5-400x400.png) |
| ![](U6-400x400.png) | ![](U7-400x400.png) |
| ![](U9-400x400.png) | |
{: .noBorder}

### Temperature distributions {#sec-results-temperature}

| ![](T1-400x400.png) | ![](T3-400x400.png) |
| ![](T4-400x400.png) | ![](T5-400x400.png) |
| ![](T6-400x400.png) | ![](T7-400x400.png) |
| ![](T9-400x400.png) | |
{: .noBorder}
