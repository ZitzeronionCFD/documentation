---
title: fluxSummary
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- flux
- function object
- post-processing
menu_id: function-objects-field-fluxsummary
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `fluxSummary` function object computes
the volumetric- or mass-flux information across selections of face zones.

Statistics include:

- positive flux:

$$ \phi_{+} = \sum \phi_{>0} $$

- negative flux:

$$ \phi_{-} = \sum \phi_{<0} $$

- net flux:

$$ \phi_{\mathrm{net}} = \sum \phi $$

- absolute flux:

$$ \phi_{\mathrm{abs}} = \sum |{\phi}| $$

### Operands

Operand       | Type       | Location
:---          |:---        |:---
 input        | -          | -
 output file  | dat        | `$FOAM_CASE/postProcessing/<FO>/<time>/<faceN>`
 output field | -          | -

## Usage {#usage}

Example of the `fluxSummary` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    fluxSummary1
    {
        // Mandatory entries (unmodifiable)
        type            fluxSummary;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        mode            cellZoneAndDirection;
        cellZoneAndDirection
        (
            (porosity (1 0 0))
        );

        // Optional entries  (runtime modifiable)
        phi             phi;
        scaleFactor     1.0;
        tolerance       0.8;

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "fluxSummary.H", "fluxSummary" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "fluxSummary",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fluxSummary.H", "mode" %>
<% assert :string_exists, "fluxSummary.H", "cellZoneAndDirection" %>
<% assert :string_exists, "fluxSummary.H", "phi" %>
<% assert :string_exists, "fluxSummary.H", "scaleFactor" %>
<% assert :string_exists, "fluxSummary.H", "tolerance" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: fluxSummary              | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 mode         | Mode to generate faces to test      | word |  yes     | -
 phi          | Name of surface flux field          | word |  no      | phi
 scaleFactor  | Factor to scale results             | scalar | no     | 1.0
 tolerance    | Tolerance for reference direction   | scalar | no     | 0.8

Options for the `mode` entry:

    faceZone
    faceZoneAndDirection | faceZone with prescribed positive direction
    cellZoneAndDirection | cellZone with prescribed positive direction
    surface
    surfaceAndDirection

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Usage by the `postProcess` utility is not available.

### Notes on entries

- For surface and direction, `phi=U` can be used for determining the fluxes.
- In case of `mode=cellZoneAndDirection`, faces bounding the `cellZone`
  are extracted and processed.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/fluxSummary" %>

API:

- [Foam::functionObjects::fluxSummary](doxy_id://Foam::functionObjects::fluxSummary)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
