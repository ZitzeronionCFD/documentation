---
title: Turbulence
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- turbulence
menu_id: models-turbulence
license: CC-BY-NC-ND-4.0
menu_parent: models
---

<%= page_toc %>

OpenFOAM includes support for the following types of turbulence modelling:

- [Reynolds Averaged Simulation (RAS)](ras),
- [Detached Eddy Simulation (DES)](des), and
- [Large Eddy Simulation (LES)](les).

## Usage {#usage}

Turbulence models are specified in the
`$FOAM_CASE/constant/turbulenceProperties`
file, taking the form, e.g. when specifying the [RAS](ras)
[kOmegaSST](ref_id://ras-komegasst) model:

    simulationType      RAS;

    RAS
    {
        RASModel                kOmegaSST;

        // On/off switch
        turbulence              on

        // Optionally write the model coefficients at run-time
        printCoeffs             no;

        // Optionally override default model coefficients
        kOmegaSSTCoeffs
        {
            ...
        }
    }

<% assert :string_exists, "TurbulenceModel.C", "simulationType" %>
<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "RASModel.C", "turbulence" %>
<% assert :string_exists, "RASModel.C", "printCoeffs" %>

Default coefficients are available for all models, based on their reference
literature.  Optionally users may override the default values by specifying
a `<model>Coeffs` sub-dictionary.  Coefficient names can be found by observing
the solver output when setting the `printCoeffs` to `yes`.

## Mesh requirements {#mesh-requirements}

Effective use of turbulence models requires close attention to their
respective meshing requirements, particularly in near-wall regions.

![Near wall velocity profile](turbulence-wall-profile-2000-small.png)

DNS data from Lee and Moser <%= cite "lee_direct_2015" %>

RAS

- high Reynolds number: first cell height should be in the region of
  30 \< $$ y^{+} $$ \< 200.  Note that the upper limit is imposed by the
  location of the outer layer, which depends on the Reynolds number
- low Reynolds number: mesh required to resolve the viscous sub-layer,
  typically using 10-20 layers

LES

- mesh required to resolve the viscous sub-layer
- requires high order schemes to adequately resolve the high-energy containing
  eddies
- preferably isotropic mesh

## Numerical settings {#numerical-settings}

Turbulence generation is driven by the velocity gradient.  Errors arising from
the [gradient calculation](ref_id://schemes-gradient), e.g.
due to poor quality meshes, can lead to spurious turbulence predictions and
solver instability. This effect can be partly compensated by the application of
[limited schemes](<%= ref_id "schemes-gradient", "options-gradient-limiters" %>).

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels" %>
- [Source documentation](doxy_id://grpTurbulence)
