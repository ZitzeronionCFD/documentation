# frozen_string_literal: true

# Menu creation tools
module Menu
  # Menu item
  class MenuItem
    attr_reader :id, :name, :path, :children

    def initialize(item)
      @id = item[:menu_id]
      # puts item.attributes

      # use file name if file is under a rtm directory
      if item[:filename].include? '/rtm/'
        @name = File.basename(item[:filename], ".*")
      else
        @name = item[:short_title] || item[:title]
      end
      identifier_str = item.identifier.to_s
      @path = "#{identifier_str.chomp(File.extname(identifier_str))}/"
      @children = []
      # puts "MenuItem id:#{@id} path:#{@path}"
    end

    def sort_items(items)
      if items.find { |item| item[:menu_weight] }
        items.sort! do |a, b|
          (a[:menu_weight] || 20) <=> (b[:menu_weight] || 20)
        end
      else
        items.sort! { |a, b| a[:title] <=> b[:title] }
      end
      items
    end

    def attach_children(items, html, level)
      c = sort_items(items.select { |item| item[:menu_parent] == @id })
      c.each do |item|
        node = MenuItem.new(item)
        node.attach_children(items, html, level)
        @children << node
      end
    end

    def show(lead)
      puts "#{lead} #{@name}"
      @children.each { |c| c.show("#{lead}-") }
    end
  end

  # Menu list
  class MenuList
    attr_reader :tree
    attr_accessor :encoded

    def initialize(items, root_id, stem, encode: true)
      @tree = []
      @encoded = +''
      @stem = stem
      root = items.find { |item| item[:menu_id] == root_id }
      if root
        @tree << MenuItem.new(root)
        @tree[0].attach_children(items, html, 1)
      else
        puts "Warning: no '#{root_id}' menu_id found"
      end

      return unless encode

      html = +"\n<ul class=\"list-unstyled components\">"
      level = 1
      @tree.each { |leaf| encode_children(leaf, html, true, level) }
      html << "\n</ul>"
      @encoded = html
    end

    def show
      @tree.each { |item| item.show('-') }
    end

    def encode_children(leaf, out, first, level)
      if first
        out << "\n<li class=\"active level-#{level}\">"
        out << "\n<a href=\"#{@stem}/\">#{leaf.name}</a>"
      elsif !leaf.children.empty?
        out << "\n<li class=\"level-#{level}\">"
        out << "\n<a data-toggle=\"collapse\" "
        out << "href=\"\##{leaf.id}\" "
        out << 'aria-expanded="false" '
        out << "class=\"dropdown-toggle\">#{leaf.name || 'NO TITLE'}</a>"
        out << "<ul id=\"#{leaf.id}\" "
        out << "class=\"collapse list-unstyled level-#{level}\">"
        level += 1
        out << "\n<li class=\"level-#{level}\">"
        out << "<a href=\"#{leaf.path}\">Overview</a>"
      else
        out << "\n<li class=\"level-#{level}\">"
        out << "<a href=\"#{leaf.path}\">#{leaf.name}</a>"
      end
      out << "</li>"

      leaf.children.each { |child| encode_children(child, out, false, level) }

      out << "\n</ul>\n</li>" unless first || leaf.children.empty?
    end

    def html
      @encoded
    end
  end
end
