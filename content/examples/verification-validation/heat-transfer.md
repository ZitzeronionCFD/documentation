---
title: Heat transfer
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- example
- heat transfer
- verification
- validation
menu_id: verification-validation-heat-transfer
license: CC-BY-NC-ND-4.0
menu_parent: examples-verification-validation
---

<%= page_toc %>

## Heat transfer {#heat-transfer}

<%= insert_models "verification-validation-heat-transfer" %>
