---
title: k-ε
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-kepsilon
license: CC-BY-NC-ND-4.0
menu_parent: ras-linearevm
group: turbulence-ras-evm
---

<%= page_toc %>

## Properties {#properties}

- Two transport-equation linear-eddy-viscosity turbulence closure model:
  - Turbulent kinetic energy, $$k$$,
  - Turbulent kinetic energy dissipation rate, $$\epsilon$$.
- Based on:
  - Standard model: *Launder and Spalding (1974)* <%= cite "launder_numerical_1974" %>,
  - Rapid Distortion Theory compression term: *El Tahry (1983)*
  <%= cite "el_tahry_k-epsilon_1983" %>.
- Extensively used with known performance,
- Over-prediction of turbulent kinetic energy at stagnation points,
- Requires near-wall treatment.

## Model equations {#model-equations}

The **turbulent kinetic energy** equation, $$k$$
\[Eq. 2.2-2, <%= cite "launder_numerical_1974" %>\]:

$$
      \Ddt{\rho k}
    =
      \div \left( \rho D_k \grad k \right)
    + P
    - \rho \epsilon
$$

Where:

$$k$$
: Turbulent kinetic energy \[$$\text{m}^2 \text{s}^{-2} $$\]

$$D_k$$
: Effective diffusivity for $$k$$ \[-\]

$$P$$
: Turbulent kinetic energy production rate \[$$\text{m}^2 \text{s}^{-3}$$\]

$$\epsilon$$
: Turbulent kinetic energy dissipation rate \[$$\text{m}^2 \text{s}^{-3}$$\]

The **turbulent kinetic energy dissipation rate** equation,
$$\epsilon$$ \[Eq. 2.2-1, <%= cite "launder_numerical_1974" %>\]:

$$
      \Ddt{\rho \epsilon}
    =
      \div \left( \rho D_{\epsilon} \grad \epsilon \right)
    + \frac{C_1 \epsilon}{k} \left( P + C_3 \frac{2}{3} k \div \u \right)
    - C_2 \rho \frac{\epsilon^2}{k}
$$

Where:

$$D_\epsilon$$
: Effective diffusivity for $$\epsilon$$ \[-\]

$$C_1$$
: Model coefficient \[-\]

$$C_2$$
: Model coefficient \[-\]

The **turbulent viscosity** equation, $$\nu_t$$
\[Eq. 2.2-3, <%= cite "launder_numerical_1974" %>\]:

$$
    \nu_t = C_{\mu} \frac{k^2}{\epsilon}
$$

Where:

$$C_{\mu}$$
: Model coefficient for the turbulent viscosity \[-\]

$$\nu_t$$
: Turbulent viscosity \[$$\text{m}^2 \text{s}^{-1}$$\]

## OpenFOAM implementation

### Equations {#openfoam-model-equations}

The **turbulent kinetic energy dissipation rate**, $$\epsilon$$:

$$
      \ddt{\alpha \rho \epsilon}
    + \div \left( \alpha \rho \u \epsilon \right)
    - \laplacian \left( \alpha \rho D_\epsilon \epsilon \right)
    =
      C_1 \alpha \rho G \frac{\epsilon}{k}
    - \left(
        \left( \frac{2}{3} C_1 - C_{3,RDT} \right) \alpha \rho \div \u \epsilon
      \right)
    - \left(
              C_2 \alpha \rho \frac{\epsilon}{k} \epsilon
      \right)
    + S_\epsilon
    + S_{\text{fvOptions}}
$$

Where:

$$\alpha$$
: Phase fraction of the given phase \[-\]

$$\rho$$
: Density of the fluid \[$$\text{kg} \text{m}^{-3}$$\]

$$G$$
: Turbulent kinetic energy production rate due to the anisotropic part of the Reynolds-stress tensor \[$$\text{m}^2 \text{s}^{-3}$$\]

$$D_\epsilon$$
: Effective diffusivity for $$\epsilon$$ \[-\]

$$C_1$$
: Model coefficient \[$$s$$\]

$$C_2$$
: Model coefficient \[-\]

$$C_{3,RDT}$$
: Rapid-distortion theory compression term coefficient \[-\]

$$S_\epsilon$$
: Internal source term for $$\epsilon$$

$$S_{\text{fvOptions}}$$
: Source terms introduced by `fvOptions` dictionary for $$\epsilon$$

The **turbulent kinetic energy** equation, $$k$$:

$$
      \ddt{\alpha \rho k}
    + \div \left( \alpha \rho \u k \right)
    - \laplacian \left( \alpha \rho D_k k \right)
    =
      \alpha \rho G
    - \left( \frac{2}{3} \alpha \rho \div \u k \right)
    - \left( \alpha \rho \frac{\epsilon}{k} k \right)
    + S_k
    + S_{\text{fvOptions}}
$$

Where:

$$S_k$$
: Internal source term for $$k$$

$$S_{\text{fvOptions}}$$
: Source terms introduced by `fvOptions` dictionary for $$k$$

Note that:

- buoyancy contributions are *not* included,
- the coefficient $$C_3$$ is not the same as $$C_{3,RDT}$$.

## Default model coefficients {#default-coeffs}

The model coefficients are \[Table 2.1, <%= cite "launder_numerical_1974" %>;<%= cite "el_tahry_k-epsilon_1983" %>\]:

$$
    C_\mu = 0.09; \quad C_1 = 1.44; \quad C_2 = 1.92; \quad C_{3, RDT} = 0.0; \quad \sigma_k = 1.0; \quad \sigma_\epsilon = 1.3
$$

## Initial conditions {#initialisation}

For isotropic turbulence, the turbulent kinetic energy can be estimated by:

$$
    k = \frac{3}{2} \left( I \mag{\u_{\mathit{ref}}} \right)^{2}
$$

Where:

$$I$$
: Turbulence intensity \[%\]

$$\u_{\mathit{ref}}$$
: A reference flow speed \[$$\text{m} \text{s}^{-1}$$\]

For isotropic turbulence, the turbulence dissipation rate can be estimated by:

$$
    \epsilon = \frac{C_{\mu}^{0.75}k^{1.5}}{L}
$$

Where:

$$C_{\mu}$$
: A model constant equal to 0.09 by default \[-\]

$$L$$
: A reference length scale \[$$\text{m}$$\]

## Boundary conditions {#bcs}

Inlet:

- <%= link_to_menuid "boundary-conditions-fixedValue" %>
- turbulentMixingLengthDissipationRateInlet

Outlet:

- [Zero gradient](ref_id://boundary-conditions-zeroGradient)
- [Constrained outlet](ref_id://boundary-conditions-inletOutlet)

Walls:

- kLowReWallFunction
- kqRWallFunction
- epsilonWallFunction

## Usage {#usage}

The model can be enabled by using **constant/turbulenceProperties**
dictionary:

    RAS
    {
        // Mandatory entries
        RASModel        kEpsilon;

        // Optional entries
        turbulence      on;
        printCoeffs     on;

        // Optional model coefficients
        Cmu             0.09;
        C1              1.44;
        C2              1.92;
        C3              0.0;
        sigmak          1.0;
        sigmaEps        1.3;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "turbulenceModels/RAS/kEpsilon/kEpsilon.H", "kEpsilon" %>

## Further information {#further-information}

Source code:

- [Foam::RASModels::kEpsilon](doxy_id://Foam::RASModels::kEpsilon)

References:

- Standard model: *Launder and Spalding (1974)* <%= cite "launder_numerical_1974" %>
- Rapid Distortion Theory compression term: *El Tahry (1983)*
  <%= cite "el_tahry_k-epsilon_1983" %>
