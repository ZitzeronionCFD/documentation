---
title: Specie
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- transport
menu_id: thermophysical-specie
license: CC-BY-NC-ND-4.0
menu_parent: models-thermophysical
---

<%= page_toc %>

## Options {#options}

The specie model represents the lowest-level thermophysical entity, responsible
for:

- the specie name,
- molecular weight, $$ W $$ \[kg/kmol\],
- number of mols, $$ n $$ \[mol\], and
- gas constant $$ R_g $$ \[J/kg/K\].

## Usage {#usage}

~~~
specie
{
    molWeight       <molecular-weight>

    // Optional; default = 1
    massFraction    <fraction>
}
~~~

Note that the gas constant is a derived quantity, calculated as:

$$
R_g = \frac{R}{W}
$$

Where $$ R $$ is the universal gas constant with the value 8.314 \[J/K/mol\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/specie" %>
