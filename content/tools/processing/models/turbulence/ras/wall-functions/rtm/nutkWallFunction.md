---
title: Turbulence viscosity based on turbulence kinetic energy
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
- wall function
menu_id: wallfunctions-nutk
license: CC-BY-NC-ND-4.0
menu_parent: ras-wallfunctions
group: turbulence-wall-functions
---

<%= page_toc %>

## Properties {#properties}

- Uses turbulence kinetic energy, $$ k $$
- Employs a switching behaviour for the turbulence viscosity, $$ \nu_t $$

## Usage {#usage}

The wall function is specified using:

    wallPatch
    {
        type            nutkWallFunction;
        value           XXX;
    }

## Further information {#further-information}

Source code:

- [Foam::nutkWallFunctionFvPatchScalarField](doxy_id://Foam::nutkWallFunctionFvPatchScalarField)
