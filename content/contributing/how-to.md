---
title: How to contribute
copyright:
- Copyright (C) 2022-2023 OpenCFD Ltd.
tags:
- contribute
- documentation
menu_id: contributing-how-to
license: CC-BY-NC-ND-4.0
menu_parent: contributing
---

This guide provides details on how to add to this documentation system. For
details on setting up your environment and building the content, please see the
[README](/README.md).

This documentation system uses the [kramdown](https://kramdown.gettalong.org)
flavour of markdown, with custom `ruby` extensions. These aim to provide
additional markdown-like tools, supplemented by ruby functions for more complex
operations.

## Syntax

### Metadata

Each file contains a [YAML](https://yaml.org/) metadata section, enclosed by
`---` boundaries. For example:

~~~
---
title: Contributing
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- tag1
- tag2
- tagN
authors:
- author1
- author2
- authorN
menu_id: contributing
menu_parent: home
menu_weight: 10000
license: CC-BY-NC-ND-4.0
---
~~~

The `tags` entry creates the list of tags seen at the top of the page that link
to the [tags](/tags) page, e.g.

- [tag1](#)
- [tag2](#)
- [tagN](#)
{: .tags}

The `menu_xxx` entries are used to create and access the menu tree:

- `menu_id` : the identifier for the page;
- `menu_parent` : the `menu_id` for the page parent; and
- `menu_weight` : the menu weight to change the position of the page in the
  menu tree; by default, all pages are assigned a weight of 20.

### Links

Standard markdown links can be used, e.g. to create a link to openfoam.com:

    [link to openfoam](https://www.openfoam.com)

Links to documentation pages can use relative or global (paths starting from
`/`) paths, e.g.

    [link to page](path_to_target)

or using the target page `menu_id`. The latter is typically easier for
maintenance since the link does not need to be updated if the page is moved. For
example, to create a link to this page that has a `menu_id` of `contributing`,
the `ref_id://` prefix can be used:

    [link text](ref_id://contributing)

Links to external doxygen API pages use the `doxy_id://` prefix, e.g. to link to
the `Foam::polyMesh` class:

    [link text](doxy_id://Foam::polyMesh)

Using the standard markdown link syntax is useful to provide a familiar style;
however, using the embedded `ruby` functions can remove the burden to maintain
link text. The `link_to_menuid`  function will create a link that automatically
uses the target page title for the link text, e.g. to create a link to this
page:

    <%= link_to_menuid "contributing" %>

Links to source code on Gitlab, e.g. to the `polyMesh` directory can be
written:

    <%= repo_link2 "$FOAM_SRC/OpenFOAM/meshes/polyMesh/" %>

### Tables

From the [kramdown guide](https://kramdown.gettalong.org/syntax.html#math-blocks)

> Note that LaTeX code that uses the pipe symbol | in inline math statements
> may lead to a line being recognized as a table line. This problem can be
> avoided by using the \vert command instead of |!

### HTML Classes

~~~
{: .my_class}
~~~

### Notes

Making a note:

~~~
This is a note
{: .note}
~~~

This is a note
{: .note}
