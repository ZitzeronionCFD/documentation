---
title: Fixed temperature
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- constraint
- temperature
menu_id: fvoption-fixedtemperature
menu_parent: numerics-fvoptions-constraints
license: CC-BY-NC-ND-4.0
group: fvoptions-constraints-energy
---

<%= page_toc %>

This constraint will fix the temperature in the energy equation in the mesh
region specified, regardless of incoming flow temperature or local flow
behaviour.

## Properties {#properties}

Downstream of the constraint the temperature field will continue to develop
normally. Please note this will only affect the temperature field. Other
thermodynamic fields, e.g. density, may still show sensitivity to the local
pressure (if applicable). Note that this constraint will cause a
non-conservation of energy. No information will be added to the log file other
than the identification of the constraint at the start of the simulation.

## Usage {#usage}

    fixedTemperature
    {
        type            fixedTemperatureConstraint;
        active          yes;

        selectionMode   cellZone;
        cellZone        porosity;

        mode            uniform;      // uniform or lookup

        // For uniform option
        temperature     constant 500; // fixed temperature with time \[K\]

        // For lookup option
        // T            <Tname>;      // optional temperature field name
    }

<% assert :string_exists,
   "fixedTemperatureConstraint.H", "fixedTemperatureConstraint" %>
<% assert :string_exists, "fixedTemperatureConstraint.C", "mode" %>
<% assert :string_exists, "fixedTemperatureConstraint.C", "temperature" %>
<% assert :string_exists, "fixedTemperatureConstraint.C", "T" %>
<% assert :string_exists, "cellSetOption.C", "cellZone" %>

## Further information {#further-information}

Tutorials

- <%= repo_link2(
  "$FOAM_TUTORIALS/lagrangian/coalChemistryFoam/simplifiedSiwek") %>
- <%= repo_link2(
  "$FOAM_TUTORIALS/compressible/rhoSimpleFoam/angledDuctExplicitFixedCoeff") %>
- <%= repo_link2(
  "$FOAM_TUTORIALS/compressible/rhoPorousSimpleFoam/angledDuct") %>

Source code

- <%= repo_link2(
  "$FOAM_SRC/fvOptions/constraints/derived/fixedTemperatureConstraint") %>
