---
title: totalPressure
copyright:
- Copyright (C) 2017-2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-totalPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-outlet-pressure
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `totalPressure` is a boundary condition that sets the static pressure at
the patch $$ p_p $$ based on a specification of the total pressure, $$ p_0 $$.
The mode of operation is determined via the input entries and the dimensions
of the convective flux, phi ($$ \phi $$).

Four variants are possible:

### Incompressible subsonic

Patch pressure described by subtracting the dynamic pressure from the total
pressure, where the flux has dimensions of $$ m^3/s $$.

$$
    p_p = p_0 - \frac{1}{2}\mag{\u}^2
$$

where:

Property | Description
---------|---------------------------------------------
$$p_p$$  | Incompressible pressure at patch \[m^2/s^2\]
$$p_0$$  | Incompressible total pressure \[m^2/s^2\]
$$\u$$   | Velocity

### Compressible subsonic

Patch pressure described by subtracting the dynamic pressure from the total
pressure, where the flux has dimensions of $$ kg/s $$.

$$
    p_p = p_0 - \frac{1}{2}\rho\mag{\u}^2
$$

where:

Property  | Description
----------|--------------------------
$$p_p$$   | Pressure at patch \[Pa\]
$$p_0$$   | Total pressure \[Pa\]
$$\rho$$  | Density \[kg/m^3\]
$$\u$$    | Velocity

### Compressible transonic ($$\gamma = 1$$)

Ratio of specific heats, $$ \gamma = C_p/C_v $$ equal to 1.

$$
    p_p = \frac{p_0}{1 + \frac{1}{2} \psi \mag{\u}^2}
$$

where:

Property             | Description
---------------------|-------
$$p_p$$              | Pressure at patch \[Pa\]
$$p_0$$              | Total pressure \[Pa\]
$$\psi$$             | Compressibility \[m^2/s^2\]
$$\u$$               | Velocity

### Compressible supersonic ($$\gamma > 1$$)

Ratio of specific heats, $$ \gamma = C_p/C_v $$ greater than 1.

$$
    p_p = \frac{p_0}{\left(1 + \frac{\gamma - 1}{2} \psi \mag{\u}^2\right)^{\frac{\gamma}{\gamma - 1}}}
$$

Property             | Description
---------------------|-------
$$p_p$$              | Pressure at patch \[Pa\]
$$p_0$$              | Total pressure \[Pa\]
$$\psi$$             | Compressibility \[m^2/s^2\]
$$G$$                | Coefficient given by $$\frac{\gamma}{1-\gamma}$$
$$\u$$               | Velocity

The modes of operation are set by the dimensions of the pressure field
to which this boundary condition is applied, the `psi` entry and the value
of `gamma`:

Mode                 | Dimensions | psi | gamma
---------------------|------------|-----|------
Incompressible subsonic | p/rho   | -   | -
Compressible subsonic | p         | none | -
Compressible transonic  | p       | psi   | 1
Compressible supersonic | p       | psi   | > 1

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            totalPressure;

        // Conditional entries

            // Incompressible subsonic
            rho             none;
            p0              uniform 0;

            // Compressible subsonic
            p0              uniform 100000;

            // Compressible transonic
            psi             psi;
            p0              uniform 100000;

            // Compressible supersonic
            psi             psi;
            gamma           1.4;
            p0              uniform 100000;

        // Optional entries
        U               <word>;
        phi             <word>;
        rho             <word>;
        psi             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists,
   "totalPressureFvPatchScalarField.H", "totalPressure" %>
<% assert :string_exists, "totalPressureFvPatchScalarField.C", "rho" %>
<% assert :string_exists, "totalPressureFvPatchScalarField.C", "p0" %>
<% assert :string_exists, "totalPressureFvPatchScalarField.C", "U" %>
<% assert :string_exists, "totalPressureFvPatchScalarField.C", "phi" %>
<% assert :string_exists, "totalPressureFvPatchScalarField.C", "psi" %>
<% assert :string_exists, "totalPressureFvPatchScalarField.C", "gamma" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `totalPressure`                 | word    | yes      | -
`p0`           | Total pressure field                       | scalarField  | yes | -
`U`            | Name of velocity field                     | word    | no       | U
`phi`          | Name of flux field                         | word    | no       | phi
`rho`          | Name of density field                      | word    | no       | rho
`psi`          | Name of compressibility field              | word    | no       | none
`gamma`        | Heat capacity ratio                        | scalar  | conditional | -

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/windAroundBuildings" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/supersonicFreestream" %>

API:

- [Foam::totalPressureFvPatchScalarField](doxy_id://Foam::totalPressureFvPatchScalarField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
