---
title: flowRateInletVelocity
copyright:
- Copyright (C) 2017-2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-flowRateInletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `flowRateInletVelocity` is a velocity boundary condition that either
corrects the extrapolated velocity or creates a uniform velocity field normal
to the patch adjusted to match the specified flow rate.
The condition is a wrapper around the [fixedValue](ref_id://boundary-conditions-fixedValue)
condition.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                flowRateInletVelocity;

        // Conditional entries

            // Option-1
            volumetricFlowRate  <Function1<scalar>>;

            // Option-2
            massFlowRate        <Function1<scalar>>;

        // Optional entries
        rho                 <word>;
        rhoInlet            <scalar>;
        extrapolateProfile  <bool>;

        // Inherited entries
        ...
    }

<% assert :string_exists,"flowRateInletVelocityFvPatchVectorField.H", "flowRateInletVelocity" %>
<% assert :string_exists, "flowRateInletVelocityFvPatchVectorField.C", "volumetricFlowRate" %>
<% assert :string_exists, "flowRateInletVelocityFvPatchVectorField.C", "massFlowRate" %>
<% assert :string_exists, "flowRateInletVelocityFvPatchVectorField.C", "rho" %>
<% assert :string_exists, "flowRateInletVelocityFvPatchVectorField.C", "rhoInlet" %>
<% assert :string_exists, "flowRateInletVelocityFvPatchVectorField.C", "extrapolateProfile" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `flowRateInletVelocity`         | word    | yes      | -
`volumetricFlowRate`   | Volumetric flow rate                       | Function1\<scalar\>  | choice   | -
`massFlowRate`         | Mass flow rate                             | Function1\<scalar\>  | choice   | -
`rho`                  | Name of density field                      | word    | no       | rho
`rhoInlet`             | Density initialisation value               | scalar  | no       | -VGREAT
`extrapolateProfile`   | Flag to extrapolate the velocity profile from the interior | bool | no     | false

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

- For a mass-based flux input:
  - The flow rate should be provided in \[kg/s\].
  - If `rho` is `none` the flow rate is in \[m^3/s\].
  - Otherwise `rho` should correspond to the name of the density field
  - If the density field cannot be found in the database, the user must
    specify the inlet density using the `rhoInlet` entry.
- For a volumetric-based flux:
  - The flow rate is in \[m^3/s\].
{: .note}

A uniform plug flow is set by default.  To set the profile according to the
downstream cells:

    extrapolateProfile  yes;

<% assert :string_exists, "flowRateInletVelocityFvPatchVectorField.C", "extrapolateProfile" %>

To convert from a mass flow rate to a volumetric flow rate:

    rho                 rho;

<% assert :string_exists, "flowRateInletVelocityFvPatchVectorField.C", "rho" %>

To convert from a mass flow rate to a volumetric flow rate for incompressible
flows:

    rhoInlet            1.0;

<% assert :string_exists,
   "flowRateInletVelocityFvPatchVectorField.C", "rhoInlet" %>

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/laminar/helmholtzResonance" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/porousSimpleFoam/angledDuct" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/flowRateInletVelocity" %>

API:

- [Foam::flowRateInletVelocityFvPatchVectorField](doxy_id://Foam::flowRateInletVelocityFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
