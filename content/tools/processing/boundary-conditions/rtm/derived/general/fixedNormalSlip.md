---
title: fixedNormalSlip
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-fixedNormalSlip
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `fixedNormalSlip` is a generic boundary condition that sets the patch-normal
component to the field to a user specified field. The tangential component is
treated as `slip`.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    fixedNormalSlip;
        fixedValue              <Type>;

        // Optional entries
        writeValue              <bool>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "fixedNormalSlipFvPatchField.H", "fixedNormalSlip" %>
<% assert :string_exists, "fixedNormalSlipFvPatchField.C", "fixedValue" %>
<% assert :string_exists, "fixedNormalSlipFvPatchField.C", "writeValue" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `fixedNormalSlip`               | word    | yes      | -
`fixedValue`           | User-defined value for the normal component | Type   | yes      | -
`writeValue`           | Output patch values (eg, ParaView)         | bool    | no       | false

The inherited entries are elaborated in:

- [Foam::transformFvPatchField](doxy_id://Foam::transformFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/mesh/moveDynamicMesh/SnakeRiverCanyon" %>
- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/potentialFreeSurfaceDyMFoam/oscillatingBox" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fixedNormalSlip" %>

API:

- [Foam::fixedNormalSlipFvPatchField](doxy_id://Foam::fixedNormalSlipFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
