---
title: JohnsonJacksonParticleTheta
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-JohnsonJacksonParticleTheta
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-multiphase
group: bcs-multiphase
---

<%= page_toc %>

## Description

The `JohnsonJacksonParticleTheta` is a boundary condition that provides
Robin condition for the particulate granular temperature.

#### References

    Reuge, N., Cadoret, L., Coufort-Saudejaud, C.,
    Pannala, S., Syamlal, M., & Caussat, B. (2008).
    Multifluid Eulerian modeling of dense gas–solids fluidized bed
    hydrodynamics: influence of the dissipation parameters.
    Chemical Engineering Science, 63(22), 5540-5551.

    Johnson, P. C., & Jackson, R. (1987).
    Frictional–collisional constitutive relations for granular materials,
    with application to plane shearing.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            JohnsonJacksonParticleTheta;
        specularityCoefficient  <dimensionedScalar>;
        restitutionCoefficient  <dimensionedScalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "JohnsonJacksonParticleThetaFvPatchScalarField.H", "JohnsonJacksonParticleTheta" %>
<% assert :string_exists, "JohnsonJacksonParticleThetaFvPatchScalarField.C", "specularityCoefficient" %>
<% assert :string_exists, "JohnsonJacksonParticleThetaFvPatchScalarField.C", "restitutionCoefficient" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `JohnsonJacksonParticleTheta`   | word    | yes      | -
`specularityCoefficient` | Specularity coefficient \[-\]            | dimensionedScalar  | yes     | -
`restitutionCoefficient` | Restitution coefficient \[-\]            | dimensionedScalar  | yes     | -

The inherited entries are elaborated in:

- mixedFvPatchFields.H
<!-- end of the list -->

- The specularity coefficient has to be within (0, 1).
- The restitution coefficient has to be within (0, 1).
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/reactingTwoPhaseEulerFoam/RAS/LBend" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/phaseSystemModels/twoPhaseEuler/phaseCompressibleTurbulenceModels/kineticTheoryModels/derivedFvPatchFields/JohnsonJacksonParticleTheta" %>

API:

- [Foam::JohnsonJacksonParticleThetaFvPatchScalarField](doxy_id://Foam::JohnsonJacksonParticleThetaFvPatchScalarField)

<%= history "v2012" %>

<!----------------------------------------------------------------------------->
