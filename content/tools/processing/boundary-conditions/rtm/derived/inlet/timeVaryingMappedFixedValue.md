---
title: timeVaryingMappedFixedValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-timeVaryingMappedFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `timeVaryingMappedFixedValue` is a boundary condition
that interpolates the values from a set of supplied points in space and time.

Supplied data should be specified in `constant/boundaryData/\<patchname\>/`
- `points`             : pointField of locations
- `\<time\>/\<field\>` : field of values at time `\<time\>`

The default mode of operation (`mapMethod` = `planar`) is to project
the points onto a plane (constructed from the first three points) and
construct a 2D triangulation and finds for the face centres the triangle it
is in and the weights to the 3 vertices.

The optional mapMethod nearest will avoid all projection and triangulation
and just use the value at the nearest vertex.

Values are interpolated linearly between times.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            timeVaryingMappedFixedValue;
        uniformValue    <PatchFunction1Types::MappedFile<Type>>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "timeVaryingMappedFixedValueFvPatchField.H", "timeVaryingMappedFixedValue" %>
<% assert :string_exists, "timeVaryingMappedFixedValueFvPatchField.C", "uniformValue" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `timeVaryingMappedFixedValue`   | word    | yes      | -
`uniformValue`         | Data           | PatchFunction1Types::MappedFile\<Type\>  | yes | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::PatchFunction1Types::MappedFile](doxy_id://Foam::PatchFunction1Types::MappedFile)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/pitzDailyExptInlet" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/timeVaryingMappedFixedValue" %>

API:

- [Foam::timeVaryingMappedFixedValueFvPatchField](doxy_id://Foam::timeVaryingMappedFixedValueFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
