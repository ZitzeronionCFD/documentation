---
title: greyDiffusiveViewFactor
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-greyDiffusiveViewFactor
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `greyDiffusiveViewFactor` is a boundary condition that
provides a grey-diffuse condition for radiative heat flux, `qr`,
for use with the view factor model.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            greyDiffusiveViewFactor;
        qro             <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "greyDiffusiveViewFactorFixedValueFvPatchScalarField.H", "greyDiffusiveViewFactor" %>
<% assert :string_exists, "greyDiffusiveViewFactorFixedValueFvPatchScalarField.C", "qro" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `greyDiffusiveViewFactor`       | word    | yes      | -
`qro`                  | External radiative heat flux               | scalarField | yes  | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/radiation/derivedFvPatchFields/greyDiffusiveViewFactor" %>

API:

- [Foam::radiation::greyDiffusiveViewFactorFixedValueFvPatchScalarField](doxy_id://Foam::radiation::greyDiffusiveViewFactorFixedValueFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
