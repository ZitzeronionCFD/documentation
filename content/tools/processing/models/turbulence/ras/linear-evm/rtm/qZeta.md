---
title: q-ζ
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-qzeta
license: CC-BY-NC-ND-4.0
menu_parent: ras-linearevm
group: turbulence-ras-evm
---

<%= page_toc %>

## Properties {#properties}

- Incompressible only
- Two equation model

## Model equations {#model-equations}

## Default model coefficients {#default-coeffs}

## Usage {#usage}

The model is specified using:

    RAS
    {
        turbulence      on;
        RASModel        qZeta;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "qZeta.H", "qZeta" %>

## Further information {#further-information}

Source code:

- [Foam::incompressible::RASModels::qZeta](doxy_id://Foam::incompressible::RASModels::qZeta)

References:

- Standard model: *Gibson and Dafa'Alla* <%= cite "gibson_two-equation_1995" %>
- OpenFOAM variant: *Dafa'Alla et al.* <%= cite "dafaalla_calculation_1996" %>
