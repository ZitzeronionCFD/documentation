---
title: cloud
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- points
- post-processing
- sampling
- sets
menu_id: sampling-sets-cloud
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-sampling-sets
group: sampling-sets
---

<%= page_toc %>

The `cloud` set samples field values at list of point locations.

## Usage {#usage}

Basic operation of the `cloud` set comprises:

    sets1
    {
        type            sets;
        libs            (sampling);

        mySet1
        {
            type            cloud;
            axis            <axis>;
            points          (<points>);
        }
    }

<% assert :string_exists, "sampledSets.H", "sets" %>
<% assert :class_library_exists, "sampling/Make/files", "sampledSets", "libsampling" %>
<% assert :string_exists, "cloudSet.H", "cloud" %>
<% assert :string_exists, "cloudSet.C", "points" %>

Points to be sampled are listed by the `points` entry, e.g. to sample the
locations (0 0 0), (1, 0, 0) and (2, 0, 0):

    sets1
    {
        type            sets;
        libs            (sampling);

        mySet1
        {
            type            cloud;
            axis            xyz; // write x, y, z co-ordinates
            points
            (
                (0 0 0)
                (1 0 0)
                (2 0 0)
            );
        }
    }

## Further information {#further-information}

Related:

- [Foam::patchCloudSet](doxy_id://Foam::patchCloudSet)

Source code:

- <%= repo_link2 "$FOAM_SRC/sampling/sampledSet/cloud" %>

API:

- [Foam::cloudSet](doxy_id://Foam::cloudSet)

Example usage:

- simpleFoam tutorials:
  - <%= repo_link2(
  "$FOAM_TUTORIALS/incompressible/simpleFoam/backwardFacingStep2D") %>
  - <%= repo_link2(
  "$FOAM_TUTORIALS/incompressible/simpleFoam/simpleCar") %>
