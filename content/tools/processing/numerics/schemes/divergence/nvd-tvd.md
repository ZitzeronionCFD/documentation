---
title: NVD and TVD schemes
short_title: NVD and TVD
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- NVD
- TVD
menu_id: schemes-divergence-nvdtvd
license: CC-BY-NC-ND-4.0
menu_parent: schemes-divergence
---

<%= page_toc %>

Many of the convection schemes in OpenFOAM are based on the Normalised
Variable (NV) <%= cite "leonard_simple_1988" %> and Total Variation Diminishing (TVD)
<%= cite "harten_high_1983" %> methods.  These provide a set tools to characterise
scheme properties such as boundedness (1-D).

| ![NVD](../nvd.png) | ![TVD](../tvd.png) |
{: .noBorder}

## Properties {#properties}

- Offer a blend between a low order scheme and a higher order scheme based on
  the calculation of a limiter
- Boundedness of NVD/TVD schemes is only guaranteed for 1-D cases
- Boundedness can be improved in 2-D and 3-D cases by limiting the
  [gradient](ref_id://schemes-gradient)

## Limiter calculation  {#limiter-calculation}

For arbitrary unstructured meshes it is not straightforward to identify the
upstream and downstream locations reliably.  The limiters are evaluated using a
compact stencil as described in Jasak et al. <%= cite "jasak_high_1999" %>, where:

TVD: Limiter $$\Psi(r)$$ defined as a function of $$r$$
$$
    r = 2 \frac{\vec{d} \dprod \left( \grad{\phi} \right)_P}{\vec{d} \dprod \left(\grad{\phi} \right)_f} - 1
      = 2 \frac{\vec{d} \dprod \left( \grad{\phi} \right)_P}{\phi_N - \phi_P} - 1
$$
NVD: Limiter $$\widetilde{\phi_f}$$ defined as a function of $$\widetilde{\phi_c}$$
$$
    \widetilde{\phi_c} = 1 - 0.5 \frac{\vec{d} \dprod \left( \grad{\phi} \right)_f}{\vec{d} \dprod \left( \grad{\phi}\right)_P }
                       = 1 - 0.5 \frac{\phi_N - \phi_P}{\vec{d} \dprod \left(\grad{\phi}\right)_P}
$$
Where the gradient at cell P $$ \left( \grad{\phi} \right)_P $$ is calculated
using the user-selected gradient scheme, and the vector $$ \vec{d} $$
$$
    \vec{d} = \vec{C}_{N} - \vec{C}_{P}
$$

## Further information  {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/limitedSchemes/LimitedScheme/NVDTVD.H" %>
- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/limitedSchemes/LimitedScheme" %>

API:

- [Foam::NVDTVD](doxy_id://Foam::NVDTVD)
- [Foam::LimitedScheme](doxy_id://Foam::LimitedScheme)
