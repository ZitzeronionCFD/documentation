---
title: Turbulent mixing length frequency
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
- turbulence
menu_id: boundary-conditions-turbulent-mixing-length-frequency
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-turbulence-omega
---

<%= page_toc %>

## Properties {#properties}

- based on the [inlet-outlet](ref_id://boundary-conditions-inletOutlet)
  condition
- sets the turbulent specific dissipation rate based on the patch turbulence
  kinetic energy and user-supplied mixing length

$$
    \omega_p = \frac{k^{0.5}}{C_{\mu}^{0.25} L}
$$

- in the event of reverse flow, a
  [zeroGradient](ref_id://boundary-conditions-zeroGradient) condition is
  applied, i.e.

$$
    \frac{d \omega}{d x} = 0
$$

## Usage {#usage}

The condition is specified in the field file using:

    <patchName>
    {
        type            turbulentMixingLengthFrequencyInlet;
        mixingLength    1;
        value           <field value>;

        // Optional entries
        omega           omega;
        phi             phi;
    }

<% assert :string_exists,
   "turbulentMixingLengthFrequencyInletFvPatchScalarField.H",
   "turbulentMixingLengthFrequencyInlet" %>
<% assert :string_exists,
   "turbulentMixingLengthFrequencyInletFvPatchScalarField.C", "mixingLength" %>
<% assert :string_exists, "inletOutlet/inletOutletFvPatchField.C", "phi" %>

## Further information {#further-information}

Source code:

- [Foam::turbulentMixingLengthDissipationRateInletFvPatchScalarField](doxy_id://Foam::turbulentMixingLengthDissipationRateInletFvPatchScalarField)
