---
title: Incompressible perfect gas
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- equation of state
- incompressible
- perfect gas
menu_id: equationOfState-incompressiblePerfectGas
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-equationOfState
group: equation-of-state-models
---

<%= page_toc %>

Density, $$ \rho $$ \[kg/m$$^3$$\]

$$
\rho = \frac{p_{\ref}}{R T}
$$

Entropy, $$ S $$ \[J/kg/K \]

$$
S = 0
$$

Compressibility, $$ \psi $$ \[s$$^2$$/m$$^2$$\]

$$
\psi = 0
$$

## Usage

The `incompressiblePerfectGas` equation of state is specified as:

~~~
equationOfState
{
    pRef        <scalar>;
}
~~~

Where

- `pRef` : Reference pressure \[Pa\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/equationOfState/incompressiblePerfectGas" %>
