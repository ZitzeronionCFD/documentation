---
title: pressure
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- dynamic
- field
- function object
- post-processing
- pressure
- static
- total
menu_id: function-objects-field-pressure
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `pressure` function object
provides several methods to convert an input pressure
field into derived forms, including:

- Static pressure, $$p$$:

$$
p = \rho \, p_k
$$

- Total pressure, $$p_t$$:

$$
p_t = p_{\ref} + p + 0.5 \rho |\u|^2
$$

- Pressure coefficient, static or total, $$C_p$$:

$$
C_p = \frac{p}{0.5 \rho_{\infty} \mag{\u_{\infty}}^2}
$$

- Isentropic pressure, $$p_i$$:

$$
p_i = p \left[1 + \frac{\gamma-1}{2} M^2 \right]^{\frac{\gamma}{\gamma-1}}
$$

where:

$$\rho$$
: Density \[kg/m3\]

$$\u$$
: Velocity \[m/s\]

$$\rho_{\inf}$$
: Freestream density \[kg/m3\]

$$p_{\inf}$$
: Freestream pressure \[Pa\]

$$\u_{\inf}$$
: Freestream velocity \[m/s\]

$$p_k$$
: Kinematic pressure (p/rho) \[m2/s2\]

$$p_s$$
: Static pressure \[Pa\]

$$p_0$$
: Total pressure \[Pa\]

$$p_{\ref}$$
: Reference pressure level \[Pa\]

$$p_i$$
: Total isentropic pressure

$$C_p$$
: Pressure coefficient

$$C_{p_0}$$
: Total pressure coefficient

$$\gamma$$
: Specific heat ratio

For incompressible cases, the kinematic pressure (pressure divided by density)
can be converted to pressure via a user-specified reference density.

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `volScalarField`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `volScalarField`     | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `pressure` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    pressure1
    {
        // Mandatory entries (unmodifiable)
        type            pressure;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        mode            <option>;

        // Optional entries (runtime modifiable)
        p               <pName>;
        U               <UName>;
        rho             <rhoName>;
        rhoInf          1.0; // enabled if rho=rhoInf
        pRef            0.0;
        hydroStaticMode none;
        g               (0 -9.81 0); // enabled if hydroStaticMode != none
        hRef            0.0;         // enabled if hydroStaticMode != none
        pInf            0.0;
        UInf            (1 0 0);
    }

<% assert :string_exists, "pressure.H", "pressure" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "pressure",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "pressure.C", "mode" %>
<% assert :string_exists, "pressure.C", "p" %>
<% assert :string_exists, "pressure.C", "U" %>
<% assert :string_exists, "pressure.C", "rho" %>
<% assert :string_exists, "pressure.C", "rhoInf" %>
<% assert :string_exists, "pressure.C", "pRef" %>
<% assert :string_exists, "pressure.C", "hydroStaticMode" %>
<% assert :string_exists, "pressure.C", "g" %>
<% assert :string_exists, "pressure.C", "hRef" %>
<% assert :string_exists, "pressure.C", "pInf" %>
<% assert :string_exists, "pressure.C", "UInf" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: pressure                 | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 mode         | Calculation mode (see below)        | word |  yes     | -
 p            | Name of the pressure field          | word |  no      | p
 U            | Name of the velocity field          | word |  no      | U
 rho          | Name of the density field           | word |  no      | rho
 rhoInf | Freestream density for coefficient calculation | scalar | conditional| -
 pRef         | Reference pressure for total pressure    | scalar | no | 0
 hydrostaticMode | Hydrostatic contributions (see below) | word | no   | none
 g            | Gravity vector (see below)               | vector | no | -
 hRef         | Reference height (see below)             | scalar | no | -
 pInf | Freestream pressure for coefficient calculation  | scalar | no | -
 UInf | Freestream velocity for coefficient calculation  | vector | no | -

Options for the `mode` entry:

    static      | static pressure
    total       | total pressure
    isentropic  | isentropic pressure
    staticCoeff | static pressure coefficient
    totalCoeff  | total pressure coefficient

<% assert :string_exists, "pressure.C", "static" %>
<% assert :string_exists, "pressure.C", "total" %>
<% assert :string_exists, "pressure.C", "isentropic" %>
<% assert :string_exists, "pressure.C", "staticCoeff" %>
<% assert :string_exists, "pressure.C", "totalCoeff" %>

The `hydrostaticMode` entry provides handling for the term
$$\rho (\vec{g} \cdot \vec{h})$$ where options include

    none     | not included
    add      | add the term, e.g. to convert from p_rgh to p
    subtract | subtract the term, e.g. to convert from p to p_rgh

If the `hydrostaticMode` is active, values are also required for
gravity, `g`, and reference height, `hRef`.  By default these will be
retrieved from the database.  When these values are not available
the user must provide them, e.g.

    g           (0 -9.81 0);
    hRef        0;

Default behaviour assumes that the case is compressible.  For incompressible
cases, i.e. solved using the kinematic pressure:

$$
p_k = \frac{p}{\rho} \qquad [\mathrm{m}^2 \mathrm{s}^{-2}]
$$

The `rho` entry can be used to set the freestream density:

    rho         rhoInf;
    rhoInf      100000;

<% assert :string_exists, "pressure.C", "rho" %>
<% assert :string_exists, "pressure.C", "rhoInf" %>

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Usage by the `postProcess` utility is not available.

## Stored properties  {#stored-properties}

### Fields

The derived pressure is stored on the mesh database, using the default name:

- static pressure: `static(p)`
- static pressure coefficient: `static(p)_coeff`
- total pressure: `total(p)`
- total pressure coefficient: `total(p)_coeff`

This can be overridden using the `result` entry.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/pressure" %>

API:

- [Foam::functionObjects::pressure](doxy_id://Foam::functionObjects::pressure)

<%= history "2.2.0", [{"v1612+"=>"pressure"}, {"2.2.0"=> "pressureTools"}] %>

<!----------------------------------------------------------------------------->
