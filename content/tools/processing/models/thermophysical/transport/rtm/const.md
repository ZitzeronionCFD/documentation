---
title: Constant
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- transport
menu_id: transport-const
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-transport
group: transport-models
---

<%= page_toc %>

Dynamic viscosity, $$ \mu £$ \[Pa.s\]

$$
\mu = \mu_0
$$

Thermal conductivity, $$ \kappa $$ \[W/mK\]

$$
\kappa = \frac{c_p \mu}{Pr}
$$

Thermal diffusivity, $$ \alpha_h $$ \[kg/ms\]

$$
\alpha_h = \frac{\mu}{Pr}
$$

## Usage

The `const` transport model is defined using the entries:

~~~
transport
{
    mu              <scalar>;
    Pr              <scalar>;
}
~~~

Where

- `mu` : dynamic viscosity \[Pa.s\]
- `Pr` : Prandtl number \[-\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/transport/const" %>
