---
title: Transport
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- transport
menu_id: thermophysical-transport
license: CC-BY-NC-ND-4.0
menu_parent: models-thermophysical
---

<%= page_toc %>

The transport model is responsible for evaluating:

- Dynamic viscosity, $$ \mu $$ \[Pa.s\]
- Thermal conductivity, $$ \kappa $$ \[W/mK\]
- Thermal diffusivity, $$ \alpha_h $$ \[kg/ms\]

## Options {#options}

Equation of state model selections include:

<%= insert_models "transport-models" %>

## Usage {#usage}

The transport type is specified in the `thermoType` dictionary:

~~~
thermoType
{
    transport       <model>;
    <...>
}
~~~

## Further information {#further-information}

Source code

- [grpSpecieEquationOfState](doxy_id://grpSpecieEquationOfState)
