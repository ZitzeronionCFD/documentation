---
title: ensightWrite
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- EnSight
- function object
- post-processing
menu_id: function-objects-utilities-ensightWrite
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-utilities
group: function-objects-utilities
---

<%= page_toc %>

## Overview {#overview}

The `ensightWrite` function object writes OpenFOAM fields in EnSight format.

## Usage {#usage}

Example of function object specification:

    ensightWrite1
    {
        // Mandatory entries
        type            ensightWrite;
        libs            (utilityFunctionObjects);
        fields          (<wordRes>);

        // Optional entries
        blockFields     (<wordRes>);
        boundary        <bool>;
        internal        <bool>;
        nodeValues      <bool>;

        format          <word>;
        width           <label>;
        directory       <fileName>;
        overwrite       <bool>;
        consecutive     <bool>;

        faceZones       (<wordRes>);
        patches         (<wordRes>);
        excludePatches  (<wordRes>);

        selection
        {
            <selection-1>
            {
                action          <action-type>;
                source          <source-type>;
                <source-entries>
            }
            <selection-2>
            {
                action          <action-type>;
                source          <source-type>;
                <source-entries>
            }
            ...
            <selection-3>
            {
                action          <action-type>;
                source          <source-type>;
                <source-entries>
            }
        }

        // Inherited entries
        ...
    }

<% assert :string_exists, "ensightWrite.H", "ensightWrite" %>
<% assert :class_library_exists,
   "functionObjects/utilities/Make/files",
   "ensightWrite",
   "libutilityFunctionObjects" %>
<% assert :string_exists, "ensightWrite.C", "fields" %>


Property  | Description                 | Type      | Required | Default
----------|-----------------------------|-----------|----------|--------
`type`    | Type name: `ensightWrite`   | word      | yes      | -
`libs`    | Library name: fieldFunctionObjects | word | yes    | -
`fields`  | Names of operand fields     | wordRes   | yes      | -
`blockFields` | Excluded fields from output | wordRes | no     | -
`boundary` | Convert boundary fields    | bool      | no       | true
`internal` | Convert internal fields    | bool      | no       | true
`nodeValues` | Write values at nodes    | bool      | no       | false
`format`   | ascii or binary format     | word      | no       | same as simulation
`width`    | Mask width for \c data/XXXX | label    | no       | 8
`directory` | The output directory name  | word     | no       | postProcessing/NAME
`overwrite` | Remove existing directory  | bool     | no       | false
`consecutive` | Consecutive output numbering  | bool | no      | false
`faceZones` | Names of operand face zones | wordRes | no       | -
`patches`   | Names of operand patches   | wordRes  | no       | -
`excludePatches` | Names of excluded patches | wordRes | no    | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)


## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/motorBike" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/utilities/ensightWrite" %>

API:

- [Foam::functionObjects::ensightWrite](doxy_id://Foam::functionObjects::ensightWrite)

<%= history "v1612+" %>


<!----------------------------------------------------------------------------->
