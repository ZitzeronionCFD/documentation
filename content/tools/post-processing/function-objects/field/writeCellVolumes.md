---
title: writeCellVolumes
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- cell
- volume
menu_id: function-objects-field-writecellvolumes
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `writeCellVolumes` function object writes the cell-volumes `volScalarField`.

### Operands

Operand       | Type             | Location
:---          |:---              |:---
input         | -                | -
output file   | -                | -
output field  | `volScalarField` | `$FOAM_CASE/<time>/V`

## Usage {#usage}

Example of the `writeCellVolumes` function object by using `functions`
sub-dictionary in `system/controlDict` file:

    writeCellVolumes1
    {
        // Mandatory entries (unmodifiable)
        type            writeCellVolumes;
        libs            (fieldFunctionObjects);

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists,
   "writeCellVolumes/writeCellVolumes.H", "writeCellVolumes" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "writeCellVolumes",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: writeCellVolumes         | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Example by using the `postProcess` utility:

    postProcess -func writeCellVolumes

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/writeCellVolumes" %>

API:

- [Foam::functionObjects::writeCellVolumes](doxy_id://Foam::functionObjects::writeCellVolumes)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->