---
title: timeVaryingMassSorption
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-timeVaryingMassSorption
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-multiphase
group: bcs-multiphase
---

<%= page_toc %>

## Description

The `timeVaryingMassSorption` is a boundary condition that provides
a first order fixed-value condition for a given scalar field to model
time-dependent adsorption-desoprtion processes to be used with the
`interfaceOxideRate` mass model

$$
    \frac{d c}{d t} =
        k_{abs} w (c_{int} - c_{p_{w}}) + k_{des} (c_{p_{w}} - c_{int})
$$

$$
    w = \max(1 - c_{p_{w}}/max, 0)
$$

where:

Property         | Description
-----------------|---------------------------------
$$c_{int}$$      | Concentration at cell
$$c_{p_{w}}$$    | Concentration at wall
$$k_{abs}$$      | Adsorption rate constant \[1/s\]
$$k_{des}$$      | Desorption rate constant \[1/s\]
$$w$$            | Weight function
$$max$$          | Max concentration at wall

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        type        timeVaryingMassSorption;
        kbas        <scalar>;
        max         <scalar>;

        // Optional entries
        kdes        <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "timeVaryingMassSorptionFvPatchScalarField.H", "timeVaryingMassSorption" %>
<% assert :string_exists, "timeVaryingMassSorptionFvPatchScalarField.C", "kbas" %>
<% assert :string_exists, "timeVaryingMassSorptionFvPatchScalarField.C", "max" %>
<% assert :string_exists, "timeVaryingMassSorptionFvPatchScalarField.C", "kdes" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`     | Type name: `timeVaryingAdsorption` | word   | yes    | -
`kbas`     | Adsorption rate constant         | scalar | yes    | -
`max`      | Maximum concentation at wall     | scalar | yes    | -
`kdes`     | Desorption rate constant         | scalar | no     | 0

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/icoReactingMultiphaseInterFoam/oxideFormation" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/phaseSystemModels/multiphaseInter/phasesSystem/derivedFvPatchFields/timeVaryingMassSorption" %>

API:

- [Foam::timeVaryingMassSorptionFvPatchScalarField](doxy_id://Foam::timeVaryingMassSorptionFvPatchScalarField)

<%= history "v2106" %>

<!----------------------------------------------------------------------------->
