---
title: volFieldValue
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- volFieldValue
menu_id: function-objects-field-volfieldvalue
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#sec-fos-field-volFieldValue-description}

The `volFieldValue` function object provides options to manipulate
volume field data into derived forms, e.g. to report summations, averages and
extrema.

Operations:

- `none` : no operation
- `sum` : summation
- `sumMag` : sum of magnitude
- `average`: average
- `weightedAverage`: weighted average
- `volAverage`: volume average
- `weightedVolAverage`: weighted volume average
- `volIntegrate`: volume integral
- `min`: minimum
- `max`: maximum
- `CoV`: coefficient of variation

<% assert :string_exists, "volFieldValue.C", "none" %>
<% assert :string_exists, "volFieldValue.C", "sum" %>
<% assert :string_exists, "volFieldValue.C", "sumMag" %>
<% assert :string_exists, "volFieldValue.C", "average" %>
<% assert :string_exists, "volFieldValue.C", "weightedAverage" %>
<% assert :string_exists, "volFieldValue.C", "volAverage" %>
<% assert :string_exists, "volFieldValue.C", "weightedVolAverage" %>
<% assert :string_exists, "volFieldValue.C", "volIntegrate" %>
<% assert :string_exists, "volFieldValue.C", "min" %>
<% assert :string_exists, "volFieldValue.C", "max" %>
<% assert :string_exists, "volFieldValue.C", "CoV" %>

## Usage {#sec-fos-field-volFieldValue-usage}

Example of the `volFieldValue` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    volFieldValue1
    {
        // Mandatory entries (unmodifiable)
        type            volFieldValue;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        fields          (<field1> <field2> ... <fieldN>);
        operation       <operationType>;
        regionType      <volRegion>;

        // Optional entries (runtime modifiable)
        postOperation   none;
        weightField     alpha1;

        // Optional (inherited) entries
        writeFields     false;
        scalingFactor   1.0;
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "volFieldValue.H", "volFieldValue" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "volFieldValue",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldValue.C", "fields" %>
<% assert :string_exists, "fieldValue.C", "writeFields" %>
<% assert :string_exists, "fieldValue.C", "scaleFactor" %>
<% assert :string_exists, "volRegion.C", "regionType" %>
<% assert :string_exists, "volFieldValue.C", "operation" %>
<% assert :string_exists, "fieldValue.C", "writeFields" %>
<% assert :string_exists, "fieldValue.C", "scaleFactor" %>
<% assert :string_exists, "surfaceFieldValue.C", "regionType" %>
<% assert :string_exists, "surfaceFieldValue.C", "operation" %>
<% assert :string_exists, "surfaceFieldValue.C", "weightField" %>
<% assert :string_exists, "surfaceFieldValue.C", "writeArea" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: surfaceFieldValue        | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 fields       | Names of operand fields             | wordList | yes  | -
 regionType   | Face regionType: see below          | word |  yes     | -
 name         | Name for regionType                 | word |  yes     | -
 operation    | Operation type: see below           | word |  yes     | -
 postOperation | Post-operation type: see below     | word |  no      | none
 weightField  | Name of field to apply weighting    | word |  no      | none

The inherited entries are elaborated in:

- [fieldValue](ref_id://function-objects-field-fieldvalue)

Options for the `regionType` entry:

    cellZone     | requires a 'name' entry to specify the cellZone
    all          | all cells

Options for the `operation` entry:

    none                 | No operation
    min                  | Minimum
    max                  | Maximum
    sum                  | Sum
    sumMag               | Sum of component magnitudes
    average              | Ensemble average
    volAverage           | Volume weighted average
    volIntegrate         | Volume integral
    CoV                  | Coefficient of variation: standard deviation/mean
    weightedSum          | Weighted sum
    weightedAverage      | Weighted average
    weightedVolAverage   | Weighted volume average
    weightedVolIntegrate | Weighted volume integral

Options for the `postOperation` entry:

    none          | No additional operation after calculation
    mag           | Component-wise `mag()` after normal operation
    sqrt          | Component-wise `sqrt()` after normal operation

Usage by the `postProcess` utility is not available.

## Stored properties  {#stored-properties}

### Reduced data

The following data are stored by the function object, and can be used by other
function objects:

- `<operation-type>(<region-name>, <field-name>)` : result

## Further information {#sec-fos-field-volFieldValue-further-information}

Tutorial:

- <%= repo_link2(
  "$FOAM_TUTORIALS/compressible/overRhoPimpleDyMFoam/twoSimpleRotors") %>
- <%= repo_link2(
  "$FOAM_TUTORIALS/multiphase/icoReactingMultiphaseInterFoam/poolEvaporation") %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/fieldValues/volFieldValue" %>

API:

- [Foam::functionObjects::fieldValues::volFieldValue](doxy_id://Foam::functionObjects::fieldValues::volFieldValue)

Related:

- [surfaceFieldValue](ref_id://function-objects-field-surfacefieldValue)
  function object
- [fieldValueDelta](ref_id://function-objects-field-fieldvaluedelta) function
  object

<%= history "1.7.0", [{"v1612+" => "volFieldValue"}, {"1.7.0" => "cellSource"}] %>

<!----------------------------------------------------------------------------->
