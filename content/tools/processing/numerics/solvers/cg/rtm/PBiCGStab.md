---
title: Stabilised preconditioned bi-conjugate gradient (PBiCGStab)
short_title: PBiCG
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- conjugate gradient
- solvers
- PBiCGStab
menu_id: cg-pbicgstab
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-cg
group: solvers-cg
---

<%= page_toc %>

## Properties {#properties}

- symmetric and asymmetric matrices
- run-time selectable [preconditioner](<%= ref_id "linearequationsolvers-cg", "preconditioners" %>)
- good parallel scaling

## Usage {#usage}

    solver          PBiCGStab;
    preconditioner  <conditioner>;
    relTol          <relative tolerance>;
    tolerance       <absolute tolerance>;

## Details {#details}

### Operation {#details-operation}

### Computation cost {#details-computational-cost}

Start-up

- 1 matrix-vector multiply
- 1 parallel reduction

Per iteration

- 2 matrix-vector multiplies
- 6 parallel reductions
- and preconditioner - applied 2x

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/solvers/PBiCGStab" %>

API:

- [Foam::PBiCGStab](doxy_id://Foam::PBiCGStab)

References:

- *Van der Vorst* <%= cite "van_der_vorst_bi-cgstab:_1992" %>
- *Barrett et al.* <%= cite "barrett_templates_1994" %>
