---
title: Forces
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- forces
- function object
- post-processing
menu_id: function-objects-forces
license: CC-BY-NC-ND-4.0
menu_parent: post-processing-function-objects
---

<%= page_toc %>

Options:

<%= insert_models "function-objects-forces" %>

Related:

- [Source documentation](doxy_id://grpForcesFunctionObjects)
