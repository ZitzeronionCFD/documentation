---
title: sampledMeshedSurface
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- post-processing
- sampling
- surface
menu_id: surfaces-sampledMeshedSurface
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-sampling-surfaces
group: sampling-surfaces
---

<%= page_toc %>

Creates a cutting plane through a mesh.

## Usage {#usage}

Basic operation of the `sampledMeshedSurface` surface comprises:

    surfaces1
    {
        type            surfaces;
        libs            (sampling);

        surfaces
        {
            mySurface1
            {
                type            sampledMeshedSurface;
                source          <source-type>;
                surface         <mySurface-file>;

                // Optional entries

                defaultValue
                {
                    <field1-name>       <field1-value>;
                    ...
                    <field2-name>       <fieldN-value>;
                    <fieldN-name>       <fieldN-value>;
                }

                keepIds         <bool>;

                maxDistance     <scalar>;

                patches         (<patch-names>);
            }
        }
    }

<% assert :string_exists, "sampledSurfaces.H", "surfaces" %>
<% assert :class_library_exists, "sampling/Make/files", "sampledSurfaces", "libsampling" %>
<% assert :string_exists, "sampledMeshedSurface.H", "sampledMeshedSurface" %>
<% assert :string_exists, "sampledMeshedSurface.C", "source" %>
<% assert :string_exists, "sampledMeshedSurface.C", "defaultValue" %>
<% assert :string_exists, "sampledMeshedSurface.C", "keepIds" %>
<% assert :string_exists, "sampledMeshedSurface.C", "maxDistance" %>
<% assert :string_exists, "sampledMeshedSurface.C", "patches" %>

Source types

- `cells`
- `insideCells`
- `boundaryCells`

<% assert :string_exists, "sampledMeshedSurface.C", "cells" %>
<% assert :string_exists, "sampledMeshedSurface.C", "insideCells" %>
<% assert :string_exists, "sampledMeshedSurface.C", "boundaryCells" %>

## Sample output {#sample-output}

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/sampling/sampledSurface/sampledMeshedSurface" %>

API:

- [Foam::sampledMeshedSurface](doxy_id://Foam::sampledMeshedSurface)

Example usage:

- <%= repo_link2("$FOAM_TUTORIALS/compressible/rhoSimpleFoam/squareBend") %>
- <%= repo_link2("$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/propeller") %>

<%= history "1.6", [{"v1912" => "sampledTriSurfaceMesh"}] %>
