---
title: searchableRotatedBox
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: searchablerotatedbox
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh-geometry
group: searchableobjects
---

<%= page_toc %>

![rotated box example](../images/rotatedBox-small.png)

A cuboid defined via minimum and maximum co-ordinates, rotated about its
centre according to a local co-ordinate system.
The rotation is defined by a combination of vectors (e1/e2),(e2/e3) or (e3/e1)

## Usage

    boxRotated
    {
        type            searchableRotatedBox;
        span            (5 4 3);
        origin          (0 0 0);
        e1              (1 0.5 0);
        e3              (0 0.5 1);
    }

<% assert :string_exists, "searchableRotatedBox.H", "searchableRotatedBox" %>
<% assert :string_exists, "searchableRotatedBox.C", "span" %>
<% assert :string_exists, "searchableRotatedBox.C", "origin" %>
<% assert :string_exists, "searchableRotatedBox.C", "e1" %>
<% assert :string_exists, "searchableRotatedBox.C", "e3" %>
