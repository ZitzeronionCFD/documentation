---
title: Snapping
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry
- snappyHexMesh

menu_id: snappyhexmesh-snapping
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh
---

<%= page_toc %>

The *snapping* phase attempts to adapt the castellated mesh to conform to the
input [geometry](../geometry). These controls are located in the `snapControls`
sub-dictionary.

Snapping involves projecting and morphing the mesh to, e.g. the surfaces and
features, in an iterative process.  If the adaptations invalidate the
[mesh quality](../meshquality) criteria the step is undone and replayed using
modified parameters.  The sequence ensures that the resulting mesh achieves a
minimum quality, at the expense of full geometry conformation.

The good practice is to start with the default values and adjust them only
after inspecting the mesh.

The parameters need to be set for snapping onto the surfaces and features
separately.

The algorithm first smooth the mesh on the patches and in the volume.
Then, in the morphing stage displace the vertexes towards the surface.
Only those vertexes are attracted to the surface which are located in the region
defined by the local edge length multiplied by the [tolerance](#tolerance)
entry from the `snapControls` dictionary (by default set to 2).

If the mesh quality is violated, vertices are moved back in a given fraction of
the step (keyword [errorReduction](../meshquality/#keywords-errorReduction) in
the `snappyHexMeshDict.meshQualityControls` dictionary). After mesh quality
check the next attempt to move the point towards the geometry takes place. This
process is repeated until the maximum number of iterations is reached. Each step
is reversible.

After the successful snapping or running out of iterations mesh is cleaned from
unnecessary non-orthogonal planar faces.

    snapControls
    {
        nSmoothPatch    3;
        nSmoothInternal $nSmoothPatch;
        tolerance       2.0;
        nSolveIter      30;
        nRelaxIter      5;

        // Feature snapping

            nFeatureSnapIter 10;
            implicitFeatureSnap false;
            explicitFeatureSnap true;
            multiRegionFeatureSnap false;
    }

<% assert :string_exists, "snapParameters.C", "nSmoothPatch" %>
<% assert :string_exists, "snapParameters.C", "nSmoothInternal" %>
<% assert :string_exists, "snapParameters.C", "tolerance" %>
<% assert :string_exists, "snapParameters.C", "nSolveIter" %>
<% assert :string_exists, "snapParameters.C", "nRelaxIter" %>
<% assert :string_exists, "snapParameters.C", "nFeatureSnapIter" %>
<% assert :string_exists, "snapParameters.C", "implicitFeatureSnap" %>
<% assert :string_exists, "snapParameters.C", "explicitFeatureSnap" %>
<% assert :string_exists, "snapParameters.C", "multiRegionFeatureSnap" %>

## Snapping on features {#keywords-snapping-on-features}

The mesh is snapped to the features after the surface snapping step.
While during the surface snapping the points are displaced in the normal
direction to the surface, displacement to the edge is more complicated. Therefore
more iterations are typically needed.

Features may be specified explicitly using `surfaceFeatureExtract` utility
which creates the *eMesh* file (located in `constant/triSurface` directory)
with the feature line specification.
Other option is to use implicit feature definition. In such case `snappyHexMesh`
does not read *eMesh* file and defines the sharp edges itself.
Implicit algorithm behaves correctly on simple meshes
without sharp corners and baffles.

![comparing explicit (left) and implicit (right) feature definition](../figures/compareFeatureDefinitions-small.png)

Figure shows the different result with explicit (left) and implicit (right) feature
snapping. Picture shows mesh snapped on the baffle patches representing the
break disks. Implicit snapping does not capture properly smooth edges.

A Practical example of implicit feature snapping is in the tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantBoussinesqSimpleFoam/iglooWithFridges" %>

## Keywords {#keywords}

---

### nSmoothPatch \[label\] {#keywords-nSmoothPatch}

Number of smoothing iterations along the surface

---

### nSmoothInternal \[label\] {#keywords-nSmoothInternal}

Number of iterations for internal smoothing to reduce non-orthogonality
at the face of refinement (effectively making the faces non-planar).
Default value is zero.

![effect of internal smoothing](../figures/meshing-with_point_smoother-small.png)

---

### tolerance \[scalar\] {#keywords-tolerance}

Multiplied by local cell-edge length specifies region along the surface
within which the points are attracted by the surface
<% assert :string_exists, "snapParameters.C", "tolerance" %>

---

### nSolveIter \[label\] {#keywords-nSolveIter}

Number of mesh displacement iterations

<% assert :string_exists, "snapParameters.C", "nSolveIter" %>

---

### nRelaxIter \[label\] {#keywords-nRelaxIter}

Number of relaxation iterations during the snapping. If the mesh does not conform
the geometry and all the iterations are spend, user may try to increase the number
of iterations.
<% assert :string_exists, "snapParameters.C", "nRelaxIter" %>

---

### nFeatureSnapIter \[label\] {#keywords-nFeatureSnapIter}

Number of relaxation iterations used for snapping onto the features.
If not specified, feature snapping will be disabled.
<% assert :string_exists, "snapParameters.C", "nFeatureSnapIter" %>

---

### implicitFeatureSnap \[boolean\] {#keywords-implicitFeatureSnap}

Switch turning on the implicit feature specification.
Default is `false`.
<% assert :string_exists, "snapParameters.C", "implicitFeatureSnap" %>

---

### explicitFeatureSnap \[boolean\] {#keywords-explicitFeatureSnap}

Snap mesh onto the feature lines defined in *eMesh* file.  Default is `true`.
<% assert :string_exists, "snapParameters.C", "explicitFeatureSnap" %>

---

### multiRegionFeatureSnap \[boolean\] {#keywords-multiRegionFeatureSnap}

When using [explicitFeatureSnap](#keywords-explicitFeatureSnap) and this switch is
on, features between multiple surfaces will be captured. This is useful for multi-region
meshing where the internal mesh must conform the region geometrical boundaries.
Default = false.
<% assert :string_exists, "snapParameters.C", "multiRegionFeatureSnap" %>
<% assert :string_exists, "snapParameters.C", "explicitFeatureSnap" %>

---

### nFaceSplitInterval \[label\] {#keywords-nFaceSplitInterval}

Existing cell edges are aligned with feature lines by default.  Unsuccessful
alignment will result in concavity which consequently
will lead to non-orthogonality on the extruded mesh at the layer adding phase.
Where the mesh quality fails the extrusion is disabled. Resulting in no prismatic
layer coverage in the area. To create an edge on the volumetric mesh aligned
favourably with the features `nFaceSplitInterval` parameter may be used.
The settings is typically followed by the `layerTerminationAngle` and `detectExtrusionIsland`
parameters in `addLayerControls` dictionary.

Sets the interval of iterations. Avoid using the splitting from
the first iteration by setting the parameter to one half of the
[nFeatureSnapIter](#keywords-nFeatureSnapIter) parameter
Default = -1 (disabled).
<% assert :string_exists, "snapParameters.C", "nFaceSplitInterval" %>
<% assert :string_exists, "snapParameters.C", "nFeatureSnapIter" %>

---

### detectBaffles \[boolean\] {#keywords-detectBaffles}

Explicitly turn on/off detecting baffle edges.  Default is `true`.
<% assert :string_exists, "snapParameters.C", "detectBaffles" %>

---

### releasePoints \[boolean\] {#keywords-releasePoints}

Releasing attraction close to a feature to allow more freedom
for displacement. Default is false.
<% assert :string_exists, "snapParameters.C", "detectBaffles" %>

---

### stringFeatures \[boolean\] {#keywords-stringFeatures}

Walk along the the feature edges to identify connected feature edges.
Default is `true`.
<% assert :string_exists, "snapParameters.C", "releasePoints" %>

---

### avoidDiagonal \[boolean\] {#keywords-avoidDiagonal}

Avoid attraction of points across the diagonal of the face
which would result in a collapsed face.  Default is `false`.
<% assert :string_exists, "snapParameters.C", "avoidDiagonal" %>

---

### concaveAngle \[scalar\] {#keywords-concaveAngle}

Angle at which the face is too concave and will be split.  Default is 45 deg.
<% assert :string_exists, "snapParameters.C", "concaveAngle" %>

---

### minAreaRatio \[scalar\] {#keywords-minAreaRatio}

Do not split a face if its area ratio is smaller than the given value.
Default is 0.3.
<% assert :string_exists, "snapParameters.C", "minAreaRatio" %>

---

### detectNearSurfacesSnap \[boolean\] {#keywords-detectNearSurfacesSnap}

Disable snapping to opposite near surfaces, default `true`.

---

### baffleFeaturePoints {#keywords-baffleFeaturePoints}

Optionally construct baffle features (110 deg hard-coded angle) and snap them
in addition to feature edge snapping

---

### strictRegionSnap {#keywords-strictRegionSnap}

Attract points only to the surface they originate from. This can improve
snapping of intersecting surfaces.  Default is `false`.

---
