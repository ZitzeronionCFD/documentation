---
title: Backward facing step
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- example
- turbulence
- verification
- validation
menu_id: verification-validation-turbulent-backward-facing-step
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-turbulent
group: verification-validation-turbulent
---

<%= page_toc %>

## Overview {#sec-overview}

- Solver: [simpleFoam](ref_id://simplefoam)
- Experimental case described by Driver and Seegmiller
  <%= cite "driver_features_1985" %>
- CFD case based on https://turbmodels.larc.nasa.gov/backstep_val.html
- Reynolds number based on the step height of $$ Re_h $$ 36000
- Reattachment at 6.26+-0.1 x/h
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/backwardFacingStep2D" %>

![Case description](boundary-conditions-small.png)

## Mesh {#sec-mesh}

- 2D structured mesh created using [blockMesh](ref_id://blockmesh)

![Mesh](mesh-600.png)

![Close-up around the step](mesh-close-up-600.png)

## Boundary conditions {#sec-boundary-conditions}

- $$ U $$ 44.2 m/s
- $$ h $$ 1.27 cm

### Common fields {#sec-boundary-conditions-turbulence-common}

Velocity: `U`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [fixedValue](ref_id://boundary-conditions-fixedValue)   | 44.2 m/s in x   |
| Outlet  | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Walls   | [noSlip](ref_id://boundary-conditions-noSlip)       |              |

Pressure: `p`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Outlet  | [fixedValue](ref_id://boundary-conditions-fixedValue)   | 0 Pa (static)           |
| Walls   | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |

### Turbulence fields {#sec-boundary-conditions-turbulence}

- turbulence model: [kOmegaSST](ref_id://ras-komegasst)

Turbulence viscosity: `nut`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | `calculated`   |              |
| Outlet  | `calculated`   |              |
| Walls   | `nutUSpaldingWallFunction` |   |

#### kOmega SST

Turbulence kinetic energy: `k`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [fixedValue](ref_id://boundary-conditions-fixedValue)   |  based on 0.061% intensity            |
| Outlet  | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Walls   | `kqRWallFunction` |           |

Turbulence specific dissipation rate: `omega`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [fixedValue](ref_id://boundary-conditions-fixedValue)   |  based on nut/nu = 0.009            |
| Outlet  | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Walls   | `omegaWallFunction` |         |

## Results {#sec-results}

Predictions are compared against the measurements by Driver and Seegmiller
<%= cite "driver_features_1985" %> below

| ![Skin friction](Reh36000-cf-400x400.png) | ![Pressure coefficient - lower](Reh36000-cp-low-400x400.png) |
| ![Streamwise velocity](Reh36000-Ux-upstream-400x400.png) | ![Shear stress](Reh36000-Rxy-upstream-400x400.png) |
| ![Streamwise velocity](Reh36000-Ux-400x400.png) | ![Shear stress](Reh36000-Rxy-400x400.png) |
{: .noBorder}
