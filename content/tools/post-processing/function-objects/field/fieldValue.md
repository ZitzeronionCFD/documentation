---
title: fieldValue
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
menu_id: function-objects-field-fieldvalue
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `fieldValue` class is an intermediate class for
handling field value-based function objects.

The class is not an executable function object itself, but a provider for
common entries to its derived function objects.

## Usage {#usage}

The optional-inherited entries provided by `fieldValue` to the derived
function objects are as follows:

    <userDefinedSubDictName1>
    {
        // Mandatory and other optional entries
        ...

        // Mandatory (inherited) entries (runtime modifiable)
        fields            (<field1> <field2> ... <fieldN>);

        // Optional (inherited) entries (runtime modifiable)
        writeFields       false;
        scaleFactor       1.0;
    }

<% assert :string_exists, "fieldValue.H", "fieldValue" %>
<% assert :string_exists, "fieldValue.C", "fields" %>
<% assert :string_exists, "fieldValue.C", "writeFields" %>
<% assert :string_exists, "fieldValue.C", "scalingFactor" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: AMIWeights               | word | yes      | -
 fields       | Names of operand fields             | wordList |  yes | -
 writeFields  | Flag to output field values         | bool     |  no  | false
 scaleFactor  | Scaling factor                      | scalar   |  no  | 1.0

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/fieldValues/fieldValue/" %>

API:

- [Foam::functionObjects::fieldValue](doxy_id://Foam::functionObjects::fieldValue)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
