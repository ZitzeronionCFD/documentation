---
title: Stream function
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- regular
- stream function
- waves
menu_id: waves-regular-streamfunction
license: CC-BY-NC-ND-4.0
menu_parent: models-waves
group: wave-regular
---

<%= page_toc %>

## Properties {#properties}

- Regular waves

## Model equations {#model-equations}

The wave height is modelled by the equation:

$$
    \eta = h \sum\limits_{j=1}^N E_j \cos \left( j (k x - \omega t + \phi) \right)
$$

Where:

$$h$$
: water depth

$$E_j$$
: Fourrier coefficients of the computed free surface

$$k$$
: wave number

$$\omega$$
: angular frequency

$$\phi$$
: phase shift

$$t$$
: time

## Default model coefficients {#default-coeffs}

## Usage {#usage}

Inlet patch example

    <patch>
    {
        alpha           alpha.water;
        waveModel       streamFunction;
        nPaddle         1;
        waveHeight      0.1517;
        waveAngle       0.0;
        rampTime        6.034;
        activeAbsorption yes;
        wavePeriod      3.017;
        uMean           2.0825;
        waveLength      6.2832;

        Bjs
        (
            8.6669014e-002
            2.4849799e-002
            7.7446850e-003
            2.3355420e-003
            6.4497731e-004
            1.5205114e-004
            2.5433769e-005
           -2.2045436e-007
           -2.8711504e-006
           -1.2287334e-006
        );

        Ejs
        (
            5.6009609e-002
            3.1638171e-002
            1.5375952e-002
            7.1743178e-003
            3.3737077e-003
            1.6324880e-003
            8.2331980e-004
            4.4403497e-004
            2.7580059e-004
            2.2810557e-004
        );
    }

## Further information {#further-information}

Source code:

- [Foam::waveModels::streamFunction](doxy_id://Foam::waveModels::streamFunction)

References:

- ...

Tutorials:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/streamFunction" %>
