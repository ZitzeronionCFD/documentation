---
title: noSlip
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-noSlip
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-velocity
---

<%= page_toc %>

## Description

The `noSlip` is a velocity boundary condition that fixes the velocity
to zero at walls.

$$
    \vec{u}_p = 0
$$

where:

Property             | Type
---------------------|-------
$$\vec{u}_p$$        | Velocity at the boundary

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            noSlip;

        // Inherited entries
        ...
    }

<% assert :string_exists, "noSlipFvPatchVectorField.H", "noSlip" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `noSlip`                        | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/basic/potentialFoam/pitzDaily" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/noSlip" %>

API:

- [Foam::noSlipFvPatchVectorField](doxy_id://Foam::noSlipFvPatchVectorField)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
