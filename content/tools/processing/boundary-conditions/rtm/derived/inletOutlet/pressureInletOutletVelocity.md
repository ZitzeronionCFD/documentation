---
title: pressureInletOutletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-pressureInletOutletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `pressureInletOutletVelocity` is a velocity inlet/outlet boundary
condition that applies a zero-gradient condition for outflow (as defined by
the flux); and obtains velocity from the flux with specified inlet direction.

The tangential patch velocity can be optionally specified.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            pressureInletOutletVelocity;

        // Optional entries
        tangentialVelocity  <vectorField>;
        phi             <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "pressureInletOutletVelocityFvPatchVectorField.H", "pressureInletOutletVelocity" %>
<% assert :string_exists, "pressureInletOutletVelocityFvPatchVectorField.C", "tangentialVelocity" %>
<% assert :string_exists, "pressureInletOutletVelocityFvPatchVectorField.C", "phi" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `pressureInletOutletVelocity`   | word    | yes      | -
`tangentialVelocity`  | Tangential velocity field           | vectorField | no   | Zero
`phi`          | Name of flux field                         | word    | no       | phi

The inherited entries are elaborated in:

- [Foam::directionMixedFvPatchField](doxy_id://Foam::directionMixedFvPatchField)
<!-- end of the list -->

- Sign conventions:
  - positive flux (out of domain): apply zero-gradient condition
  - negative flux (into of domain): derive from the flux with specified
    direction
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/TJunction" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/pressureInletOutletVelocity" %>

API:

- [Foam::pressureInletOutletVelocityFvPatchVectorField](doxy_id://Foam::pressureInletOutletVelocityFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
