---
title: fixedInternalValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-fixedInternalValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `fixedInternalValue` is a general boundary condition to
provide a mechanism to set boundary (cell) values directly into a matrix,
i.e. to set a constraint condition.  Default behaviour is to act as a zero
gradient condition.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    fixedInternalValue;

        // Inherited entries
        ...
    }

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `fixedInternalValue`            | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::zeroGradientFvPatchField](doxy_id://Foam::zeroGradientFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fixedInternalValueFvPatchField" %>

API:

- [Foam::fixedInternalValueFvPatchField](doxy_id://Foam::fixedInternalValueFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
