---
title: Multiphase boundary conditions
short_title: Multiphase
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-multiphase
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

Available multiphase boundary conditions include:

<%= insert_models "bcs-multiphase" %>
