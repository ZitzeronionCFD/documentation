MathJax = {
  tex: {
    macros: {
      f: '{\\scriptscriptstyle{f}}',
      RR: '{\\bf R}',

      eff: '{\\mathrm{eff}}',
      ref: '{\\mathrm{ref}}',
      std: '{\\mathrm{std}}',

      cprod: '{\\times}',
      dprod: '\\,{\\scriptscriptstyle \\stackrel{\\bullet}{{}}}\\,',
      ddprod: '\\,{\\scriptscriptstyle \\stackrel{\\bullet}{\\bullet}}\\,',
      //  dprod: '\\,{\\stackrel{\\cdot}{{}}}\\,',
      //  ddprod: '\\,{\\stackrel{\\cdot}{\\cdot}}\\,',

      grad: '{\\nabla }',
      div: '{\\nabla \\dprod}',
      laplacian: '{\\nabla^{2}}',
      snGrad: '{\\nabla_{\\f}^{\\scriptscriptstyle{\\perp}}}',

      // 1-parameter
      vec: ['{\\bf #1}',1],
      tensor: ['\\mathrm{\\bf #1}',1],
      mat: ['{#1}', 1],
      old: ['#1^{o}', 1],
      oldold: ['#1^{oo}', 1],
      bold: ['{\\bf #1}', 1],
      av: ['{\\overline{#1}}', 1],
      mag: ['{\\left| #1 \\right|}', 1],
      ddt: ['{\\frac{\\partial}{\\partial t}\\left(#1\\right)}', 1],
      Ddt: ['{\\frac{D}{D t}\\left(#1\\right)}', 1],

      // Derived
      u: '{\\vec{u}}'
    }
  }
}
