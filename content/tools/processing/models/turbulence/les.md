---
title: Large Eddy Simulation (LES)
short_title: LES
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- LES
- turbulence
menu_id: turbulence-les
license: CC-BY-NC-ND-4.0
menu_parent: models-turbulence
---

<%= page_toc %>

## Options {#options}

<%= insert_models "turbulence-les" %>

## Usage {#usage}

LES is selected by setting the `simulationType` entry

    simulationType  LES;

    LES
    {
        \\ Model input parameters
    }

<% assert :string_exists, "TurbulenceModel.C", "simulationType" %>
<% assert :string_exists, "LESModel.C", "LES" %>

Suitable for:

- 3-D cases, not appropriate for reduced-dimension cases
- transient, not appropriate for steady state

## Initialisation {#initialisation}

Typically initialised from a precursor calculation, e.g. employing
[RAS](../ras) modelling.

## LES Deltas

<%= insert_models "turbulence-les-delta" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/LES" %>

Related:

- [Source documentation](doxy_id://grpLESTurbulence)
