---
title: rhoCentralFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- compressible
- heat transfer
- solvers
menu_id: rhocentralfoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-compressible
group: solvers-compressible
solver_models:
- compressible
- heat-transfer
- transient
- turbulence
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-compressible" %>
- transient
- compressible - density based
- heat transfer
- shock capturing
- <%= link_to_menuid "models-turbulence" %>
- <%= link_to_menuid "models-thermophysical" %>
- <%= link_to_menuid "fvoptions" %>

## Equations {#equations}

- based on the central-upwind schemes of Kurganov and Tadmor

## Input requirements {#input-requirements}

Mandatory fields:

- p: pressure \[pa\]
- U: velocity \[m/s\]
- T: temperature \[K\]

### Physical models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence):
  `constant/turbulenceProperties`
- [heat transfer](ref_id://models-thermophysical#usage):
  `constant/thermophysicalModels`
- [finite volume options](ref_id://numerics-fvoptions#usage):
  `constant/fvOptions` (optional)

### Solution controls {#input-requirements-solution-controls}

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/compressible/rhoCentralFoam" %>

See also

- <%= link_to_menuid "rhopimplefoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoCentralFoam" %>
