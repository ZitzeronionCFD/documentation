---
title: pressureDirectedInletOutletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-pressureDirectedInletOutletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `pressureDirectedInletOutletVelocity` is a velocity inlet/outlet boundary
condition that applies zero-gradient condition for outflow (as defined by the
flux); and obtains the velocity from the flux with the specified inlet direction
for inflow.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            pressureDirectedInletOutletVelocity;
        inletDirection  <vectorField>;

        // Optional entries
        phi             <word>;
        rho             <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "pressureDirectedInletOutletVelocityFvPatchVectorField.H", "pressureDirectedInletOutletVelocity" %>
<% assert :string_exists, "pressureDirectedInletOutletVelocityFvPatchVectorField.C", "inletDirection" %>
<% assert :string_exists, "pressureDirectedInletOutletVelocityFvPatchVectorField.C", "phi" %>
<% assert :string_exists, "pressureDirectedInletOutletVelocityFvPatchVectorField.C", "rho" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `pressureDirectedInletOutletVelocity`| word    | yes      | -
`inletDirection` | Inlet direction field                    | vectorField    | yes      | -
`phi`          | Name of flux field                         | word    | no       | phi
`rho`          | Name of density field                      | word    | no       | rho

The inherited entries are elaborated in:

- [Foam::fvPatchField](doxy_id://Foam::fvPatchField)
- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
<!-- end of the list -->

- Sign conventions:
  - positive flux (out of domain): apply zero-gradient condition
  - negative flux (into of domain): derive from the flux with specified
    direction
{: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/pressureDirectedInletOutletVelocity" %>

API:

- [Foam::pressureDirectedInletOutletVelocityFvPatchVectorField](doxy_id://Foam::pressureDirectedInletOutletVelocityFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
