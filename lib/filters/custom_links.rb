# frozen_string_literal: true

# Replaces custom links by erb calls
class CustomLinksFilter < Nanoc::Filter
  identifier :custom_links

  # (ref_id://my_id#anchor) => (<%= ref_id("my_id", "anchor"))
  # (doxy_id://my_id) => (<%= doxy_link("my_id"))
  # (repo_id:my_id) => (<%= repo_url("my_id") %>)
  # (https://my_url) => (https://my_url){:target="_blank"}
  def run(content, _params)
    ops = '{:target="_blank" rel="nofollow noreferrer noopener"}'
    content
      .gsub(%r{ref_id://([\w\-]+)#([\w\-]+)}, '<%= ref_id("\1", "\2") %>')
      .gsub(%r{ref_id://([\w\-]+)}, '<%= ref_id("\1") %>')
      .gsub(%r{\( *doxy_id://([:\w\-\.]+) *\)}, "(<%= doxy_url(\"\\1\") %>)#{ops}")
      .gsub(%r{\( *repo_id://(.*) *\)}, "(<%= repo_url(\"\\1\") %>)#{ops}")
      .gsub(%r{\( *repo_base://(.*) *\)}, "(<%= repo_base_url(\"\\1\") %>)#{ops}")
      .gsub(%r{\( *https://(.*?) *\)}, "(https://\\1)#{ops}")
  end
end
