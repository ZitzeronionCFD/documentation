---
title: Run-time post-processing
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- graphics
- function object
- post-processing
menu_id: function-objects-graphics-runtimepostprocessing
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-graphics
group: function-objects-graphics
---

<%= page_toc %>

The `runTimePostProcessing` function object generates images by assembling
and compositing scene objects, typically produced via function objects.

- Generate a static image, or a sequence of images
- Render points, lines and surfaces
- Optional scalar bar
- Perspective and parallel camera modes
- Various colour maps

This object has a dependency on the [VTK](http://www.vtk.org) libraries
{: .note}

## Usage {#usage}

The `runTimePostProcessing` function object is specified using multiple
sub-dictionaries that describe the desired output, camera and scene object
settings:

    runTimePostPro1
    {
        type            runTimePostProcessing;
        libs            ("librunTimePostProcessing.so");

        // Image properties
        output
        {
            ...
        }

        // Visualisation properties
        camera
        {
            ...
        }

        // Default colours
        colours
        {
            ...
        }


        // Scene objects

        lines
        {
            ...
        }

        surfaces
        {
            ...
        }

        text
        {
            ...
        }
    }

<% assert :string_exists, "runTimePostProcessing.H", "runTimePostProcessing" %>
<% assert :class_library_exists, "functionObjects/graphics/runTimePostProcessing/CMakeLists-Project.txt", "runTimePostProcessing", "runTimePostProcessing" %>
<% assert :string_exists, "runTimePostProcessing.C", "output" %>
<% assert :string_exists, "scene.C", "camera" %>
<% assert :string_exists, "scene.C", "colours" %>
<% assert :string_exists, "runTimePostProcessing.C", "lines" %>
<% assert :string_exists, "runTimePostProcessing.C", "surfaces" %>
<% assert :string_exists, "runTimePostProcessing.C", "text" %>

### output

The `output` sub-dictionary describes properties of the output `png` file

    output
    {
        name            image;
        width           2000;
        height          1200;
    }

<% assert :string_exists, "runTimePostProcessing.C", "output" %>
<% assert :string_exists, "runTimePostProcessing.C", "name" %>
<% assert :string_exists, "runTimePostProcessing.C", "width" %>
<% assert :string_exists, "runTimePostProcessing.C", "height" %>

### camera

Basic operation of a static `camera` comprises:

    camera
    {
        // Parallel projection flag
        parallelProjection  no;

        // Camera geometry
        focalPoint      (1.2 1.1 0.2);
        up              (0 0 1);
        position        (3.6 5.1 -1.3);


        // Optional properties

        // Restrict the view to a clip box
        clipBox         (-0.2 -0.2 0)(1.65 0.2 1.25);
    }

<% assert :string_exists, "scene.C", "parallelProjection" %>
<% assert :string_exists, "scene.C", "focalPoint" %>
<% assert :string_exists, "scene.C", "up" %>
<% assert :string_exists, "scene.C", "position" %>
<% assert :string_exists, "scene.C", "clipBox" %>

Optionally, the camera can move.  Each of the camera geometry controls is a
Foam::Function1 type, and can be set to vary as a function of scene time using
e.g. tabulated, polynomial, sinusoidal profiles etc.  The scene time is
defined as a linear function between zero and one.

An example using tabulated entries is given by:

    camera
    {
        // Parallel projection flag
        parallelProjection  no;

        // Camera geometry
        focalPoint      (1.2 1.1 0.2);
        up              (0 0 1);
        position        (3.6 5.1 -1.3);


        // Optional properties

        // If camera is moving, optionally provide start and end times
        // Note: specified in scene time in range 0-1
        startPosition    0.2;
        endPosition      0.75;

        // Total number of frames to generate
        nFrameTotal      20;
    }

<% assert :string_exists, "scene.C", "parallelProjection" %>
<% assert :string_exists, "scene.C", "focalPoint" %>
<% assert :string_exists, "scene.C", "up" %>
<% assert :string_exists, "scene.C", "position" %>
<% assert :string_exists, "scene.C", "clipBox" %>
<% assert :string_exists, "scene.C", "startPosition" %>
<% assert :string_exists, "scene.C", "endPosition" %>
<% assert :string_exists, "scene.C", "nFrameTotal" %>

### colours

The `colours` sub-dictionary defines the set of default colours to employ
for the background, and scene objects when the `colourBy` entry is set to
`colour`.

    colours
    {
        background      (1 1 1);

        text            (0 0 0);
        edge            (1 0 0);
        surface         (0.5 0.5 0.5);
        line            (1 0 0);


        // Optional

        // Set a second background colour to produce a graded background
        background2     (0 0 1);
    }

<% assert :string_exists, "scene.C", "background" %>
<% assert :string_exists, "runTimePostProcessing/text.C", "text" %>
<% assert :string_exists, "runTimePostProcessing/surface.C", "edge" %>
<% assert :string_exists, "runTimePostProcessing/surface.C", "surface" %>
<% assert :string_exists, "runTimePostProcessing/pathline.C", "line" %>
<% assert :string_exists, "scene.C", "background2" %>

### Scene objects

Scene objects provide the main focus of the image, e.g. lines, surfaces and
text.  All objects have a `visible` property to optionally hide the object.

### lines

The `lines` sub-dictionary describes line-based data, e.g. the output from
stream lines.  Valid line types include:

- `functionObjectLine`: line based data produced by a function object

      lines
      {
          streamline
          {
              type            functionObjectLine;
              functionObject  streamLines;
              representation  tube;
              visible         yes;
              tubeRadius      0.01;
              colourBy        field;
              field           U;
              range           (0 20);
              opacity         1;
          }
      }

## Sample output {#sample-output}

- Portable Network Graphic (PNG) images

## Further information {#further-information}

Related:

- [Point sampling](ref_id://function-objects-sampling-sets)
- [Surface sampling](ref_id://function-objects-sampling-surfaces)

API:

- [Foam::functionObjects::runTimePostPro::runTimePostProcessing](doxy_id://Foam::functionObjects::runTimePostPro::runTimePostProcessing)

Example usage:

- [pisoFoam](ref_id://pisofoam) `motorBike` tutorial:
  <%= repo_link2(
  "$FOAM_TUTORIALS/incompressible/pisoFoam/LES/motorBike/motorBike") %>

\ofAssert functionObjectUseExists incompressible/pisoFoam/LES/motorBike/motorBike runTimePostProcessing

<%= history "v3.0+" %>
