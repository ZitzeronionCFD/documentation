---
title: Face-limited gradient scheme
short_title: Face-limited
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- limited
- gradient
menu_id: schemes-gradient-facelimited
license: CC-BY-NC-ND-4.0
#menu_parent: schemes-gradient
group: schemes-gradient-limiter
---

<%= page_toc %>

## Properties {#properties}

## Usage {#usage}

The scheme is specified using:

    gradSchemes
    {
        default         none;
        grad(U)         faceLimited <scheme> <coefficient>;
    }

The coefficient should be specified in the range zero to one, where values of

## Further information {#further-information}

Source code

- [Foam::fv::faceLimitedGrad](doxy_id://Foam::fv::faceLimitedGrad)

Mult-directional form

- [faceMDLimited](ref_id://schemes-gradient-facemdlimited)
