---
title: Lien-Leschziner
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-lienleschziner
license: CC-BY-NC-ND-4.0
menu_parent: ras-linearevm
group: turbulence-ras-evm
---

<%= page_toc %>

## Properties {#properties}

- Incompressible only
- Two-equation model

## Model equations {#model-equations}

The turbulence kinetic energy equation is given by:

$$
    \Ddt{k}
  =
    \div \left( D_k \grad k \right)
  + G
  - \epsilon
$$

and the turbulence dissipation rate equation by:

$$
    \Ddt{\epsilon}
  =
    \div \left( D_\epsilon \grad \epsilon \right)
  + C_{\epsilon 1} G \epsilon/k
  - C_{\epsilon 2} f_2 \epsilon^2/k
  + E
$$

## Default model coefficients {#default-coeffs}

## Usage {#usage}

The model is specified using:

    RAS
    {
        turbulence      on;
        RASModel        LienLeschziner;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "LienLeschziner.H", "LienLeschziner" %>

## Further information {#further-information}

Source code:

- [Foam::incompressible::RASModels::LienLeschziner](doxy_id://Foam::incompressible::RASModels::LienLeschziner)

References:

- Standard model: *Lien and Leschziner* <%= cite "lien_pressure-velocity_1993" %>
