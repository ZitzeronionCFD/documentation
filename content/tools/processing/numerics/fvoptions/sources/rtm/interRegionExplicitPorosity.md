---
title: Inter-region explicit porosity
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- momentum
- multi-region
- porosity
- source
menu_id: fvoption-interregionexplicitporosity
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-momentum-multi-region
---

<%= page_toc %>

## Properties {#properties}

- Inter-region variant of the <%= link_to_menuid "fvoption-explicitporosity" %>

## Usage {#usage}

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/chtMultiRegionSimpleFoam/heatExchanger" %>

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/interRegion/interRegionExplicitPorositySource" %>
