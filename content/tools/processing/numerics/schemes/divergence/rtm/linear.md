---
title: Linear divergence scheme
short_title: Linear
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-linear
license: CC-BY-NC-ND-4.0
group: schemes-divergence-nvdtvd
---

<%= page_toc %>

## Properties {#properties}

- Second order
- Unbounded
- Good choice for LES calculations due to low dissipation
- For isotropic meshes $$ \phi_f = 0.5 \left( \phi_c + \phi_d \right) $$

## Normalised Variable Diagram {#nvd}

![linear](../linear.png)

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss linear;
    }

<% assert :string_exists, "schemes/linear/linear.H", "linear" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/schemes/linear" %>

API:

- [Foam::linear](doxy_id://Foam::linear)
