---
title: fixedPressureCompressibleDensity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-fixedPressureCompressibleDensity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-pressure
---

<%= page_toc %>

## Description

The `fixedPressureCompressibleDensity` is a pressure boundary condition that
calculates a (liquid) compressible density as a function of pressure and
fluid properties.

The governing equations are as follows:

$$
    \rho = \rho_{l,sat} + \psi_l*(p - p_{sat})
$$

where:

Property             | Description
---------------------|-------
$$\rho$$             | Density                          \[kg/m^3\]
$$\rho_{l,sat}$$     | Saturation liquid density        \[kg/m^3\]
$$\psi_l$$           | Liquid compressibility           \[-\]
$$p$$                | Pressure                         \[Pa\]
$$p_{sat}$$          | Saturation pressure              \[Pa\]

The variables $$\rho_{l,sat}, p_{sat}$$ and $$\psi_l$$ are retrieved from the
`thermodynamicProperties` dictionary.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedPressureCompressibleDensity;

        // Optional entries
        p               <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "fixedPressureCompressibleDensityFvPatchScalarField.H", "fixedPressureCompressibleDensity" %>
<% assert :string_exists, "fixedPressureCompressibleDensityFvPatchScalarField.C", "p" %>

where:

Property  | Description                                | Type    | Required | Default
----------|--------------------------------------------|---------|----------|---------
`type`    | Type name: `fixedPressureCompressibleDensity` | word | yes      | -
`p`       | Name of pressure field                     | word    | no       | p

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fixedPressureCompressibleDensity" %>

API:

- [Foam::fixedPressureCompressibleDensityFvPatchScalarField](doxy_id://Foam::fixedPressureCompressibleDensityFvPatchScalarField)

<%= history "1.6" %>

<!----------------------------------------------------------------------------->
