---
title: Buoyancy energy
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- buoyancy
- energy
- fvOption
- source
menu_id: fvoption-buoyancyenergy
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-energy
---

<%= page_toc %>

## Properties {#properties}

Adds the buoyancy energy

$$
    S = \rho (\vec{g} \dprod \vec{U})
$$

## Usage {#usage}

The option is specified using:

    buoyancy1
    {
        type            buoyancyEnergy;
        selectionMode   all;
    }

<% assert :string_exists, "buoyancyEnergy.H", "buoyancyEnergy" %>

## Further information {#further-information}

Tutorials

- *none*

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/derived/buoyancyEnergy" %>
