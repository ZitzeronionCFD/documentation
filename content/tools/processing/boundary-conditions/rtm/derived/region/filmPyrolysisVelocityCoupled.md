---
title: filmPyrolysisVelocityCoupled
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-filmPyrolysisVelocityCoupled
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-region
group: bcs-region
---

<%= page_toc %>

## Description

The `filmPyrolysisVelocityCoupled` is boundary condition
designed to be used in conjunction with surface
film and pyrolysis modelling.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            filmPyrolysisVelocityCoupled;

        // Optional entries
        filmRegion      <word>;
        pyrolysisRegion  <word>;
        phi             <word>;
        rho             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "filmPyrolysisVelocityCoupledFvPatchVectorField.H", "filmPyrolysisVelocityCoupled" %>
<% assert :string_exists, "filmPyrolysisVelocityCoupledFvPatchVectorField.C", "pyrolysisRegion" %>
<% assert :string_exists, "filmPyrolysisVelocityCoupledFvPatchVectorField.C", "filmRegion" %>
<% assert :string_exists, "filmPyrolysisVelocityCoupledFvPatchVectorField.C", "phi" %>
<% assert :string_exists, "filmPyrolysisVelocityCoupledFvPatchVectorField.C", "rho" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `filmPyrolysisVelocityCoupled`  | word  | yes      | -
`pyrolysisRegion` | Name of pyrolysis region                | word    | no       | pyrolysisProperties
`filmRegion`   | Name of film region                        | word    | no       | surfaceFilmProperties
`phi`          | Name of flux region                        | word    | no       | phi
`rho`          | Name of density region                     | word    | no       | rho

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/regionModels/regionCoupling/derivedFvPatchFields/filmPyrolysisVelocityCoupled" %>

API:

- [Foam::filmPyrolysisVelocityCoupledFvPatchVectorField](doxy_id://Foam::filmPyrolysisVelocityCoupledFvPatchVectorField)

<%= history "2.0.0" %>

<!----------------------------------------------------------------------------->
