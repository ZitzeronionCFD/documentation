---
title: Perfect fluid
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- equation of state
- perfect fluid
menu_id: equationOfState-perfectFluid
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-equationOfState
group: equation-of-state-models
---

<%= page_toc %>

Density, $$\rho$$ \[kg/m$$^3$$\]

$$
\rho = \rho_0 + \frac{p}{R T}
$$

Entropy, $$ S $$ \[J/kg/K \]

$$
S = -R \ln \left( \frac{p}{p_{\std}} \right)
$$

Compressibility, $$ \psi $$ \[s$$^2$$/m$$^2$$\]

$$
\psi = \frac{1}{R T}
$$

## Usage

The `perfectFluid` equation of state is specified as:

~~~
equationOfState
{
    R           <scalar>;
    rho0        <scalar>;
}
~~~

Where

- `R` : Fluid constant \[J/kg/K\]
- `rho0` : Reference density \[kg/m$$^3$$\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/equationOfState/perfectFluid" %>
