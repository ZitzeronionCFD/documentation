---
title: writeDictionary
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- dictionary
- function object
- post-processing
menu_id: function-objects-utilities-writedictionary
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-utilities
group: function-objects-utilities
---

<%= page_toc %>

The `writeDictionary` function object writes dictionaries on start-up, and
if changed.

## Usage {#usage}

The `writeDictionary` function object is specified using:

    writeDictionary1
    {
        type        writeDictionary;
        libs        ("libutilityFunctionObjects.so");
        dictNames   (fvSchemes fvSolution)
    }

<% assert :string_exists, "writeDictionary.H", "writeDictionary" %>
<% assert :class_library_exists,
   "functionObjects/utilities/Make/files",
   "writeDictionary",
   "libutilityFunctionObjects" %>

## Sample output {#sample-output}

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/utilities/writeDictionary" %>

API:

- [Foam::functionObjects::writeDictionary](doxy_id://Foam::functionObjects::writeDictionary)
