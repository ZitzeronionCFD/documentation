---
title: Turbulent intensity kinetic energy
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
- turbulence
menu_id: boundary-conditions-turbulent-intensity-kinetic-energy
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-turbulence-k
---

<%= page_toc %>

## Properties {#properties}

- based on the [inlet-outlet](ref_id://boundary-conditions-inletOutlet)
  condition
- sets the turbulent kinetic energy based on the patch velocity and
  user-supplied turbulence intensity

$$
    k_p = 1.5 \left( I \mag{\u} \right)^2
$$

- in the event of reverse flow, a
  [zeroGradient](ref_id://boundary-conditions-zeroGradient) condition is
  applied, i.e.

$$
    \frac{d k}{d x} = 0
$$

## Usage {#usage}

The condition is specified in the field file using:

    <patchName>
    {
        type        turbulentIntensityKineticEnergyInlet;
        intensity   0.05;
        value       <field value>;

        // Optional entries
        U           U;
        phi         phi;
    }

<% assert :string_exists,
   "turbulentIntensityKineticEnergyInletFvPatchScalarField.H",
   "turbulentIntensityKineticEnergyInlet" %>
<% assert :string_exists,
   "turbulentIntensityKineticEnergyInletFvPatchScalarField.C", "intensity" %>
<% assert :string_exists,
   "turbulentIntensityKineticEnergyInletFvPatchScalarField.C", "U" %>
<% assert :string_exists, "inletOutlet/inletOutletFvPatchField.C", "phi" %>

## Further information {#further-information}

Source code:

- [Foam::turbulentIntensityKineticEnergyInletFvPatchScalarField](doxy_id://Foam::turbulentIntensityKineticEnergyInletFvPatchScalarField)
