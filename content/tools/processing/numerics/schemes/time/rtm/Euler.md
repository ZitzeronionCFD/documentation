---
title: Euler implicit time scheme
short_title: Euler implicit
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- time
menu_id: schemes-time-eulerimplicit
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes-time
group: schemes-time
---

<%= page_toc %>

## Properties {#properties}

- Implicit
- First order
- Transient

$$
    \ddt{\phi} = \frac{\phi - \old{\phi}}{\Delta t}
$$

## Usage {#usage}

The scheme is specified using:

    ddtSchemes
    {
        default         Euler;
        ddt(phi)        Euler;
    }

## Further information {#further-information}

Source code

- [Foam::fv::EulerDdtScheme](doxy_id://Foam::fv::EulerDdtScheme)
