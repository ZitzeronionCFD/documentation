---
title: atmNutWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-atmNutWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

The `atmNutWallFunction` is a boundary condition that provides
a wall constraint on the turbulent viscosity (i.e. `nut`) based on the
turbulent kinetic energy (i.e. `k`) and velocity (i.e. `U`) for atmospheric
boundary layer modelling.

The governing equation of the boundary condition:

$$
    \tau_w = {U^*_u} {U^*_k}
$$

with:

$$
    {U^*_u} = \frac{\kappa U_w}{ln(z_p / z_0)}
$$

$$
    {U^*_k} = C_{\mu}^{1/4} \sqrt{k}
$$

where:

Property    | Description
------------|--------------------------------------------
$$\tau_w$$  | Wall shear stress
$$U^*_u$$   | Local friction velocity based on near-ground velocity
$$U^*_k$$   | Local friction velocity based on near-ground k
$$\kappa$$  | von Karman constant
$$U_w$$     | Near-ground velocity
$$z_p$$     | Vertical coordinate
$$z_0$$     | Surface roughness length \[m\]
$$C_mu$$    | Empirical model constant
$$k$$       | Turbulent kinetic energy

#### References

- Theoretical expressions (tags:RH, SBJM, SM):

    Richards, P. J., & Hoxey, R. P. (1993).
    Appropriate boundary conditions for computational wind
    engineering models using the k-ε turbulence model.
    In Computational Wind Engineering 1 (pp. 145-153).
    DOI:10.1016/B978-0-444-81688-7.50018-8

    Sørensen, N. N., Bechmann, A., Johansen, J., Myllerup, L.,
    Botha, P., Vinther, S., & Nielsen, B. S. (2007).
    Identification of severe wind conditions using
    a Reynolds Averaged Navier-Stokes solver.
    In Journal of Physics: Conference
    series (Vol. 75, No. 1, p. 012053).
    DOI:10.1088/1742-6596/75/1/012053

    Sumner, J., & Masson, C. (2012).
    k−ε simulations of the neutral atmospheric boundary layer:
    analysis and correction of discretization errors on practical grids.
    International journal for numerical
    methods in fluids, 70(6), 724-741.
    DOI:10.1002/fld.2709

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            atmNutWallFunction;
        z0Min           <scalar>;
        z0              <PatchFunction1<scalar>>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "atmNutWallFunctionFvPatchScalarField.H", "atmNutWallFunction" %>
<% assert :string_exists, "atmNutWallFunctionFvPatchScalarField.C", "z0Min" %>
<% assert :string_exists, "atmNutWallFunctionFvPatchScalarField.C", "z0" %>

where:

Property | Description                            | Type    | Required | Default
---------|----------------------------------------|---------|----------|---------
`type`   | Type name: `atmNutWallFunction`        | word    | yes      | -
`z0Min`  | Minimum surface roughness length \[m\] | scalar  | yes      | -
`z0`     | Surface roughness length \[m\]         | PatchFunction1\<scalar\> | yes | -

The inherited entries are elaborated in:

- nutkWallFunctionFvPatchScalarField.H
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/wallFunctions/atmNutWallFunction" %>

API:

- [Foam::atmNutWallFunctionFvPatchScalarField](doxy_id://Foam::atmNutWallFunctionFvPatchScalarField)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
