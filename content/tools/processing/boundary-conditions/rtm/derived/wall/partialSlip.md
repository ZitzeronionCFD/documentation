---
title: partialSlip
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-partialSlip
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-velocity
---

<%= page_toc %>

## Description

The `partialSlip` is a velocity boundary condition that determines
the amount of slip is controlled by a user-supplied field.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            partialSlip;
        valueFraction   <Field>;

        // Optional entries
        refValue        <Field>;
        writeValue      <bool>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "partialSlipFvPatchField.H", "partialSlip" %>
<% assert :string_exists, "partialSlipFvPatchField.C", "valueFraction" %>
<% assert :string_exists, "partialSlipFvPatchField.C", "writeValue" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `partialSlip`                   | word    | yes      | -
`valueFraction` | Fraction of refValue used for boundary \[0-1\] | Field | yes   | -
`refValue`     | Reference value at zero slip               | Field   | no     | Zero
`writeValue`   | Flag to output patch values (e.g. for ParaView) | bool | no     | false

The inherited entries are elaborated in:

- [Foam::transformFvPatchField](doxy_id://Foam::transformFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/partialSlip" %>

API:

- [Foam::partialSlipFvPatchField](doxy_id://Foam::partialSlipFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
