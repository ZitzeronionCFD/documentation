---
title: uniformJump
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- coupled
menu_id: boundary-conditions-uniformJump
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-coupled
group: bcs-coupled-jump
---

<%= page_toc %>

## Description

The `uniformJump` is a general coupled boundary condition that
provides a jump condition, using the `cyclic` condition as a base.
The jump is specified as a time-varying uniform value across the patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            uniformJump;
        jumpTable       <Function1<Type>>;

        // Inherited entries
        patchType       cyclic;
        ...
    }

<% assert :string_exists, "uniformJumpFvPatchField.H", "uniformJump" %>
<% assert :string_exists, "uniformJumpFvPatchField.C", "jumpTable" %>

where:

Property | Description                  | Type    | Required | Default
---------|------------------------------|---------|----------|---------
`type`   | Type name: `uniformJump`     | word    | yes      | -
`jumpTable`   | Jump values             | Function1\<Type\>  | yes  | -

The inherited entries are elaborated in:

- fixedJumpFvPatchField.H
- Function1.H
<!-- end of the list -->

- The underlying `patchType` should be set to `cyclic`.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/RAS/damBreakPorousBaffle" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/uniformJump" %>

API:

- [Foam::uniformJumpFvPatchField](doxy_id://Foam::uniformJumpFvPatchField)

<%= history "2.2.0" %>

<!----------------------------------------------------------------------------->
