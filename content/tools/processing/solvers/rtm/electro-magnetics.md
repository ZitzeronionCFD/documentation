---
title: Electromagnetics
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
menu_id: solvers-electro-magnetics
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

## Solvers {#solvers}

- electrostaticFoam
<%= insert_models "solvers-electro-magnetics" %>

## Further information {#further-information}

- [Source documentation](doxy_id://grpElectroMagneticsSolvers)
