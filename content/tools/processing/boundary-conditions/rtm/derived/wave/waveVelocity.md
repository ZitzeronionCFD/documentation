---
title: waveAlpha
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-waveAlpha
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wave
group: bcs-wave
---

<%= page_toc %>

## Description

The `waveAlpha` is a boundary condition that establishes
the phase fraction by combining multiple wave models.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            waveAlpha;

        // Optional entries
        waveDict        <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "waveAlphaFvPatchScalarField.H", "waveAlpha" %>
<% assert :string_exists, "waveAlphaFvPatchScalarField.C", "waveDict" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `waveAlpha`                     | word    | yes      | -
`waveDict`             | Name of the wave variables dictionary      | word    | no       | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- waveModel.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/cnoidal" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/waveModels/derivedFvPatchFields/waveAlpha" %>

API:

- [Foam::waveAlphaFvPatchScalarField](doxy_id://Foam::waveAlphaFvPatchScalarField)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
