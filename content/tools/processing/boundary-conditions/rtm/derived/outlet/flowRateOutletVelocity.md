---
title: fluxCorrectedVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- outlet
menu_id: boundary-conditions-fluxCorrectedVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-outlet
group: bcs-outlet-general
---

<%= page_toc %>

## Description

The `fluxCorrectedVelocity` is a velocity-outlet boundary condition that
for patches where the pressure is specified. The outflow velocity is obtained
by `zeroGradient` and then corrected from the flux.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fluxCorrectedVelocity;

        // Conditional entries

            // Option-1
            volumetricFlowRate      <Function1<scalar>>;

            // Option-2
            massFlowRate            <Function1<scalar>>;

        // Optional entries
        rho             <word>;
        rhoOutlet       <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "fluxCorrectedVelocityFvPatchVectorField.H", "fluxCorrectedVelocity" %>
<% assert :string_exists, "fluxCorrectedVelocityFvPatchVectorField.C", "volumetricFlowRate" %>
<% assert :string_exists, "fluxCorrectedVelocityFvPatchVectorField.C", "massFlowRate" %>
<% assert :string_exists, "fluxCorrectedVelocityFvPatchVectorField.C", "rho" %>
<% assert :string_exists, "fluxCorrectedVelocityFvPatchVectorField.C", "rhoOutlet" %>

where:

Property             | Description                        | Type    | Required | Default
---------------------|------------------------------------|---------|----------|---------
`type`               | Type name: `fluxCorrectedVelocity` | word    | yes      | -
`volumetricFlowRate` | Volumetric flow rate               | Function1\<scalar\>  | choice   | -
`massFlowRate`       | Mass flow rate                     | Function1\<scalar\>  | choice   | -
`rho`                | Name of density field              | word    | no       | rho
`rhoOutlet`          | Density initialisation value       | scalar  | no       | -VGREAT

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

- For a mass-based flux:
  - The flow rate should be provided in \[kg/s\].
  - If `rho` is `none` the flow rate is in \[m^3/s\].
  - Otherwise `rho` should correspond to the name of the density field
  - If the density field cannot be found in the database, the user must
    specify the outlet density using the `rhoOutlet` entry.
- For a volumetric-based flux:
  - The flow rate is in \[m^3/s\].
- `rhoOutlet` is required for the case of a mass flow rate, where the
  density field is not available at start-up.
- The value is positive out of the domain (as an outlet).
- May not work correctly for transonic outlets.
- Strange behaviour with potentialFoam since the U equation is not solved.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interIsoFoam/iobasin" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fluxCorrectedVelocity" %>

API:

- [Foam::fluxCorrectedVelocityFvPatchVectorField](doxy_id://Foam::fluxCorrectedVelocityFvPatchVectorField)

<%= history "v1712" %>

<!----------------------------------------------------------------------------->
