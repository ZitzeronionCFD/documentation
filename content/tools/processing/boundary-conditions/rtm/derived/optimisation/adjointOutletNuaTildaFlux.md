---
title: adjointOutletNuaTildaFlux
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-adjointOutletNuaTildaFlux
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-optimisation
group: bcs-optimisation
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            adjointOutletNuaTildaFlux;
        solverName      <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "adjointOutletNuaTildaFluxFvPatchScalarField.H", "adjointOutletNuaTildaFlux" %>
<% assert :string_exists, "adjointOutletNuaTildaFluxFvPatchScalarField.C", "solverName" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `adjointOutletNuaTildaFlux`     | word    | yes      | -
`solverName`   | Name of operand solver                     | word    | yes      | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- adjointBoundaryConditions.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/optimisation/adjointOptimisation/adjoint/turbulenceModels/incompressibleAdjoint/adjointRAS/derivedFvPatchFields/adjointOutletNuaTildaFlux" %>

API:

- [Foam::adjointOutletNuaTildaFluxFvPatchScalarField](doxy_id://Foam::adjointOutletNuaTildaFluxFvPatchScalarField)

<%= history "v1906" %>

<!----------------------------------------------------------------------------->
