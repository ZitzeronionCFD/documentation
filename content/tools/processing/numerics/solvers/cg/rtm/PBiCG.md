---
title: Preconditioned bi-conjugate gradient (PBiCG)
short_title: PBiCG
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- conjugate gradient
- solvers
- PBiCG
menu_id: cg-pbicg
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-cg
group: solvers-cg
---

<%= page_toc %>

## Properties {#properties}

- asymmetric matrices
- run-time selectable [preconditioner](<%= ref_id "linearequationsolvers-cg", "preconditioners" %>)
- good parallel scaling

## Usage {#usage}

    solver          PBiCG;
    preconditioner  <conditioner>;
    relTol          <relative tolerance>;
    tolerance       <absolute tolerance>;

## Details {#details}

### Operation {#details-operation}

### Computation cost {#details-computational-cost}

Start-up

- 2 matrix-vector multiplies
- 1 parallel reduction

Per iteration

- 2 matrix-vector multiplies
- 3 parallel reductions
- and preconditioner - applied 2x

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/solvers/PBiCG" %>

API:

- [Foam::PBiCG](doxy_id://Foam::PBiCG)
