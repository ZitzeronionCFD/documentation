---
title: Hydrostatic pressure effects
copyright:
- Copyright (C) 2021 OpenCFD Ltd.
menu_id: algorithm-p-rgh
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

For cases that the hydrostatic pressure contribution
$$
    \rho ( \vec{g} \dprod \vec{h} )
$$
is important, e.g. for buoyant and multiphase cases, it is numerically
convenient to solve for an alternative pressure defined by
$$
    p' = p - \rho ( \vec{g} \dprod \vec{h} ).
$$
In OpenFOAM solver applications the $$p'$$ pressure term is named `p_rgh`.
The momentum equation

$$
    \ddt{\rho \u} + \div ( \rho \u \otimes \u ) - \div ( \mu_{\eff} \grad \u )
  = - \grad p + \rho \vec{g}
$$

is transformed to use $$p'$$:

$$
    p' = p - \rho ( \vec{g} \dprod \vec{h} ).
$$

After the following substitutions:

$$
\begin{align}
        - p & = - p' - \rho ( \vec{g} \dprod \vec{h} ) \\
  - \grad p & = - \grad( p')  - \grad ( \rho ( \vec{g} \dprod \vec{h} ) ) \\
            & = - \grad( p')  - \rho \vec{g} \dprod \grad \vec{h} - \vec{h} \dprod \grad(\rho \vec{g}) \\
            & = - \grad( p')  - \rho \vec{g} \dprod \tensor{I} - \vec{g} \dprod \vec{h} \grad (\rho) - \cancelto{0}{\rho \vec{h} \dprod \grad (\vec{g})} \\
            & = - \grad( p')  - \rho \vec{g} - \vec{g} \dprod \vec{h} \grad \rho
\end{align}
$$

where, for CFD meshes the term $$ \grad \vec{h} $$ is given by the gradient of
the cell centres, which equates to the tensor $$\tensor{I}$$, the momentum
equation becomes:

$$
    \ddt{\rho \u} + \div ( \rho \u \otimes \u ) - \div ( \mu_{\eff} \grad \u )
  = - \grad p' - \vec{g} \dprod \vec{h} \grad \rho
$$

For constant density applications this can be further simplified to

$$
    \ddt{\rho \u} + \div ( \rho \u \otimes \u ) - \div ( \mu_{\eff} \grad \u )
  = - \grad p'
$$

<!--
A pressure field `p` is still requested in the solver to set the initial
pressure level.
{: .note}
-->

For examples of the use of this variable transformation, see:

- <%= link_to_menuid "solvers-heat-transfer" %>
- <%= link_to_menuid "solvers-multiphase" %>
