---
title: Turbulent plane channel flow with smooth walls
short_title: Plane channel
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- channel
- DES
- example
- LES
- pimpleFoam
- pisoFoam
- Smagorisnky
- synthetic
- turbulence
- verification
- validation
menu_id: verification-validation-turbulent-plane-channel-flow
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-turbulent
group: verification-validation-turbulent
---

<%= page_toc %>

## Overview {#sec-overview}

- Replicates Poletto et al.'s <%= cite "poletto_new_2013" %> setup and settings
  for [Direct numerical simulation of turbulent channel flow up to Ret=590](https://doi.org/10.1063/1.869966) <%= cite "moser_direct_1999" %>
- References: Van Driest (1956) <%= cite "vandriest_turbulent_1956" %>,
  Smagorinsky (1963) <%= cite "smagorinsky_general_1963" %>,
  Kim et al. (1987) <%= cite "kim_turbulence_1987" %>,
  Moser et al. (1999) <%= cite "moser_direct_1999" %>, and
  Poletto et al. (2013) <%= cite "poletto_new_2013" %> (Synthetic turbulence
  inflow generator)
- See the [resources](sec-resources) section for additional data files

**Flow physics:**

- Internal flow
- Moderate Reynolds number
- Spanwise direction: Statistically homogeneous
- Streamwise and channel-height directions: Statistically developing
- Newtonian, single-phase, incompressible, non-reacting

**Solver:**

- [pimpleFoam](ref_id://pimplefoam)

**Tutorial case:**

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/planeChannel" %>

## Physics and Numerics {#sec-physics-and-numerics}

**Physical domain:**

- The case is a statistically-developing internal flow through parallel smooth
  walls which are two characteristic-length apart.
  - $$ x $$: Longitudinal direction (Mean-flow direction)
  - $$ y $$: Vertical direction (Wall-normal direction)
  - $$ z $$: Spanwise direction (Statistically homogeneous direction)
  - $$ O $$: Origin at the left-bottom corner of the numerical domain

**Physical modelling:**

- Reynolds number based on friction velocity: $$ \text{Re}_{u_\tau} = \vert \mathbf{U}_\tau \vert \delta / \nu_\text{fluid} = 395 $$ \[-\]
  - (Estimated) Friction velocity: $$ \mathbf{U}_\tau = (1.0, 0.0, 0.0)$$, and
    $$ \vert \mathbf{U}_\tau \vert  = u_\tau = 1.0 $$ \[m⋅s<sup>-1</sup>\]
  - Characteristic length (Channel half-height): $$\delta = 1.0 $$ \[m\]
  - Kinematic viscosity of fluid: $$ \nu_{\text{fluid}} \approx 0.002532 $$
    \[m<sup>2</sup>⋅s<sup>-1</sup>\]
  - Bulk velocity of flow: $$ \mathbf{U}_b = (17.55, 0.00, 0.00)$$
    \[m⋅s<sup>-1</sup>\]
- Turbulence model: [Large eddy simulation](ref_id://turbulence-les) with the
  [Smagorinsky](ref_id://les-smagorinsky) sub-filter scale model utilising the
  [van Driest](ref_id://les-delta-vandriest) wall-damping function. The
  sub-filter scale model constants:
  - \$$ C_k \approx 0.02655 $$
  - \$$ C_e = 1.048 $$
  - \$$ C_s \approx 0.065 $$
    - \$$ C_s = (C_k \{C_k/C_e\}^{0.5} )^{0.5} $$

**Numerical domain modelling:**

- Shape: Rectangular prism
- Dimensions: $$ (x, y, z) = (20.0\pi, 2.0, \pi)$$ \[m\]
- Sketch:

![Numerical domain (not in scale)](pcf-1-domain-574x246.png)

**Spatial domain discretisation:**

- Mesh type: Rectangular cuboid mesh
- Mesher: [blockMesh](ref_id://blockmesh)
- Number of nodes, $$N$$: $$ (N_x, N_y, N_z) = (500, 46, 82)$$ \[nodes\]
- Spatial resolution $$(\Delta)$$ distribution:
  - Uniform in $$(x, z)$$-directions
  - Stretched in $$(y)$$-direction; clustered nearby walls
- Uniform mesh particulars:
  - $$ \Delta_x^+ = (\Delta_x u_\tau )/\nu_{\text{fluid}} \approx 49.6$$ \[-\]
  - $$ \Delta_z^+ \approx 15.1$$ \[-\]
- Wall-normal mesh particulars:
  - Simple grading expansion ratio: 25.0 \[-\] (From top to bottom wall, the ratio is 0.04)
  - First wall-normal node height: $$\Delta_y^+ \approx 1.1$$
  - Mesh details:

| ![Mesh (Front part)](pcf-2-detail-654-445.png) | ![Mesh](pcf-3-detail-654-445.png) |
{: .noBorder}

**Temporal domain discretisation:**

- Time-step size: $$ \Delta_t = 0.004$$ \[s\]
- Estimated [Courant-Friedrichs-Lewy](ref_id://function-objects-field-courantno)
  (CFL) number based on $$ \{ \overline{u_x} \}_{y^+ = 392} = 20.133$$\[m⋅s<sup>-1</sup>\]: CFL $$\approx 0.64$$

**Equation discretisation:**

Spatial derivatives and variables:

- Gradient: [Gauss linear](ref_id://schemes-divergence-linear)
- Divergence: [Gauss](ref_id://schemes-gausstheorem) `linear`
- Laplacian: `Gauss linear orthogonal`
- Surface-normal gradient: [orthogonal](ref_id://schemes-sngrad-orthogonal)

Temporal derivatives and variables:

- `ddtSchemes`: [backward](ref_id://schemes-time-backward)

**Numerical boundary conditions:**

- Velocity, $$ \mathbf{U} $$

| Patch | Condition | Value \[m⋅s<sup>-1</sup>\] |
|-------|-----------|--------------------------|
| Inlet | [turbulentDFSEMInlet](ref_id://boundary-conditions-turbulentDFSEMInlet) | (17.55, 0.00, 0.00) |
| Outlet | [inletOutlet](ref_id://boundary-conditions-inletOutlet)          | (0.0, 0.0, 0.0)     |
| Sides ($$z$$-dir) | [cyclic](ref_id://boundary-conditions-cyclic) | -                   |
| Walls ($$y$$-dir) | [fixedValue](ref_id://boundary-conditions-fixedValue) | (0.0, 0.0, 0.0) |

- Pressure, `p`

| Patch | Condition | Value \[m<sup>2</sup>⋅s<sup>-2</sup>\] |
|-------|-----------|--------------------------------------|
| Inlet | [zeroGradient](ref_id://boundary-conditions-zeroGradient) | - |
| Outlet | [fixedValue](ref_id://boundary-conditions-fixedValue) | 0.0          |
| Sides ($$z$$-dir) | [cyclic](ref_id://boundary-conditions-cyclic) | -       |
| Walls ($$y$$-dir) | [zeroGradient](ref_id://boundary-conditions-zeroGradient) | - |

- Turbulent kinematic viscosity, `nut` (i.e. $$ \nu_t $$)

| Patch | Condition | Value \[m<sup>2</sup>⋅s<sup>-1</sup>\]|
|-------|-----------|-------------------------------------|
| Inlet | calculated | -                                  |
| Outlet | calculated | -                                 |
| Sides ($$z$$-dir) | [cyclic](ref_id://boundary-conditions-cyclic) | -              |
| Walls ($$y$$-dir) | [zeroGradient](ref_id://boundary-conditions-zeroGradient)  | - |

**Solution algorithms and solvers:**

- Pressure-velocity: [PISO](ref_id://pv-piso)
- Parallel decomposition of spatial domain and fields:
  [scotch](doxy_id://Foam::scotchDecomp)
- The bandwidth of the coefficient matrix is minimised by
  renumberMesh
- Linear solvers:

| Field | Linear Solver | Smoother | Relative Tolerance |
|----|----|----|----|
| `U`       | [smooth](ref_id://linearequationsolvers-smooth) | [GaussSeidel](ref_id://smooth-gauss-seidel) | 0.0 |
| `p`       | [GAMG](ref_id://multigrid-gamg) | [GaussSeidel](ref_id://smooth-gauss-seidel) | 0.0 |
| `nuTilda` | [smooth](ref_id://linearequationsolvers-smooth) | [GaussSeidel](ref_id://smooth-gauss-seidel) | 0.0 |

**Initialisation and sampling:**

- Computation time for a single domain pass-through based on $$ { \overline{U_x} }_{y^+ = 392} = 20.133$$) \[m<sup>2</sup>⋅s<sup>-1</sup>\] $$\approx 3.121$$ \[s\]
- Initialisation pass-throughs = $$ \approx 2.7 $$ <%= cite "poletto_new_2013" %>
- Sampling pass-throughs = $$ \approx 24.5 $$ <%= cite "poletto_new_2013" %>

## Results {#sec-results}

**List of metrics:**

- Prescribed vs. reproduced Reynolds stress tensor components at inlet patch
- $$ \overline{u^\prime u^\prime} $$ downstream development vs. $$ x/ \delta $$
- $$ \overline{v^\prime v^\prime} $$ downstream development vs. $$ x/ \delta $$
- $$ \overline{u^\prime v^\prime} $$ downstream development vs. $$ x/ \delta $$
- Surface skin friction coefficient $$\mathrm{C}_f$$ vs. $$x/ \delta $$
- Streamwise mean flow speed and Reynolds stress tensor components at
  uniform-interval downstream profiles
- Streamwise vorticity $$ \omega_x $$ at $$x/ \delta = 0.1$$
- Streamwise vorticity $$ \omega_x $$ at $$x/ \delta = 1.0$$
- Metrics are time and spanwise averaged
- $$ < \cdot > $$ is the time-averaging operator

| ![](pcf-uu.png) | ![Prescribed vs. reproduced Reynolds stress tensor at inlet patch (Poletto et al., Fig. 4)](pcf-vv.png) | |![](pcf-ww.png) | ![](pcf-uv.png) |
{: .noBorder}

| ![<u'u'>-component of Reynolds stress tensor - Downstream development (Poletto et al., Fig. 14) ](pcf-Fig14.png)|
{: .noBorder}

| ![<v'v'>-component of Reynolds stress tensor - Downstream development (Poletto et al., Fig. 15) ](pcf-Fig15.png) |
{: .noBorder}

| ![<u'v'>-component of Reynolds stress tensor - Downstream development (Poletto et al., Fig. 13)](pcf-Fig13.png) |
{: .noBorder}

| ![Longitudinal skin friction coefficient at top patch - Downstream development (Poletto et al., Fig. 9)](pcf-Fig9-top.png) |
{: .noBorder}

| ![Longitudinal skin friction coefficient at bottom patch - Downstream development (Poletto et al., Fig. 9)](pcf-Fig9-bottom.png) |
{: .noBorder}

| ![Streamwise vorticity component at y/&delta;=0.05 (Poletto et al., Fig. 11)](pcf-vorticityX01.png) |
{: .noBorder}

| ![Streamwise vorticity component at y/&delta;=1.0 (Poletto et al., Fig. 12)](pcf-vorticityX1.png) |
{: .noBorder}

## Resources {#sec-resources}

Note: Links will take you to the <i>University of Texas at Austin</i> website

### Datasets for verifications (plain text)

Reynolds stress tensor profiles:

- [Numerical (DNS)](http://turbulence.ices.utexas.edu/data/MKM/chan395/profiles/chan395.reystress) <%= cite "moser_direct_1999" %>

Mean velocity profiles:

- [Numerical (DNS)](http://turbulence.ices.utexas.edu/data/MKM/chan395/profiles/chan395.means) <%= cite "moser_direct_1999" %>

Two-point velocity correlations (i.e. Auto- and cross-correlation functions):

- [Numerical (DNS)](http://turbulence.ices.utexas.edu/data/MKM/chan395/correlations/) <%= cite "moser_direct_1999" %>
