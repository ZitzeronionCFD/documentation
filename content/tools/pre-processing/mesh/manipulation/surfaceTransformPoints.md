---
title: surfaceTransformPoints
tags:
- points
- surface
- transform
copyright:
- Copyright 2022-2023 (C) OpenCFD Ltd.
menu_id: surfaceTransformPoints
license: CC-BY-NC-ND-4.0
menu_parent: meshing-manipulation
group: meshing-manipulation
---

<%= page_toc %>

## Overview {#overview}

The `surfaceTransformPoints` utility performs geometric transformations on
surfaces, e.g. scale, translate, rotate, and convert between surface formats.
It is similar to the `transformPoints` utility.

## Usage

### Synopsis

<%= help_util_usage "surfaceTransformPoints" %>

#### Examples

Translate all points by the vector (0 0 1):

~~~
surfaceTransformPoints -translate '(0 0 1)' input.obj output.obj
~~~

Scale a surface to convert from \[mm\] to \[m\] and recentre:

~~~
surfaceTransformPoints -recentre -scale 0.001 input.stl output.stl
~~~

### Input

#### Arguments

<%= help_util_arguments "surfaceTransformPoints" %>

#### Options

<%= help_util_options "surfaceTransformPoints" %>

- The surface format is deduced from the input and output file extensions.
- `roll`=rotate about x, `pitch`=rotate about y, `yaw`=rotate about z.
{: .note}

#### Files

<%= no_input "file" %>

#### Fields

<%= no_input "field" %>

### Output

#### Logs

<%= no_output "log" %>

#### Files

Property       | Description                 | Path      | Type
---------------|-----------------------------|-----------|-----------
`<output>`     | New surface file            | `<case>/` | input type

#### Fields

<%= no_output "field" %>

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoSimpleFoam/squareBend" %>
- <%= repo_link2 "$FOAM_TUTORIALS/mesh/snappyHexMesh/distributedTriSurfaceMesh" %>

Source code:

- <%= repo_link2 "$FOAM_UTILITIES/surface/surfaceTransformPoints" %>

<%= history "1.5" %>

<!----------------------------------------------------------------------------->