# frozen_string_literal: true

# Utilities
module Utils

  # ID tools
  module IdTools
    def link_to_id(id)
      item = @items[id]
      if item.nil?
        "invalid item #{id}"
      else
        link_to(item[:short_title] || item[:title], item)
      end
    end

    def link_to_menuid(menu_id, title = nil, anchor = '')
      item = @items.find { |i| i[:menu_id] == menu_id }
      if item.nil?
        puts "Warning: invalid item #{menu_id}"
        "invalid item #{menu_id}"
      else
        # puts "link_to_menuid item: #{item.identifier}"
        link_to(
          title || (item[:short_title] || item[:title]),
          "#{item.path}\##{anchor}"
        )
      end
    end

    def ref_id(id, anchor = '')
      item = @items.find { |i| i[:menu_id] == id }
      if item.nil?
        puts "Warning: invalid item #{id}"
        "invalid item #{id}"
      else
        # puts "link_to_menuid item: #{item.identifier}"
        "#{item.path}\##{anchor}"
      end
    end

    def title_of_id(id)
      item = @items[id]
      item[:short_title] || item[:title]
    end
  end
end
