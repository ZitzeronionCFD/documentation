---
title: ignore
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-ignore
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-fa
group: bcs-fa
---

<%= page_toc %>

## Description

The `ignore` is a boundary condition that provides
a zero gradient condition which ignores invalid edges.
This condition is a placeholder; its naming and definition are still subject to
change.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            ignore;

        // Inherited entries
        ...
    }

<% assert :string_exists, "ignoreFaPatchField.H", "ignore" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `ignore`                        | word    | yes      | -

The inherited entries are elaborated in:

- faPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteArea/fields/faPatchFields/derived/ignore" %>

API:

- [Foam::ignoreFaPatchField](doxy_id://Foam::ignoreFaPatchField)

<%= history "v2306" %>

<!----------------------------------------------------------------------------->
