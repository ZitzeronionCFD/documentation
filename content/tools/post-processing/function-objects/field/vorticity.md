---
title: vorticity
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- vorticity
menu_id: function-objects-field-vorticity
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `vorticity` function object
computes the vorticity, the curl of the velocity.

$$
    \vec \Omega = \nabla \times \u
$$

where:

$$\vec \Omega$$
: Vorticity vector \[1/s\]

$$\u$$
: Velocity         \[m/s\]

## Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `volVectorField`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `volVectorField`     | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `vorticity` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    vorticity1
    {
        // Mandatory entries (unmodifiable)
        type        vorticity;
        libs        (fieldFunctionObjects);

        // Optional (inherited) entries
        field           <inpField>;
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "vorticity.H", "vorticity" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files", "vorticity", "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "fields" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: vorticity                | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func vorticity

## Sample output {#sample-output}

<%= vimeo 208844500, 600, 400 %>

## Further information {#further-information}

Tutorial:

- [pimpleFoam](ref_id://pimplefoam) `surfaceMountedCube` tutorial:
  <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/surfaceMountedCube/fullCase" %>

\ofAssert functionObjectUseExists incompressible/pimpleFoam/LES/surfaceMountedCube/fullCase vorticity

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/vorticity" %>

API:

- [Foam::functionObjects::vorticity](doxy_id://Foam::functionObjects::vorticity)

Related:

- [Q criterion](ref_id://function-objects-field-q)

<%= history "v1812" %>

<!----------------------------------------------------------------------------->
