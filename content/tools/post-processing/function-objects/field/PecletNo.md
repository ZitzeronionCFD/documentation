---
title: PecletNo
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- Peclet
- post-processing
menu_id: function-objects-field-pecletno
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `PecletNo` function object
computes the Peclet number as a `surfaceScalarField.`

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `surfaceScalarField` | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `surfaceScalarField` | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `PecletNo` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    PecletNo1
    {
        // Mandatory entries (unmodifiable)
        type            PecletNo;
        libs            (fieldFunctionObjects);

        // Optional entries (runtime modifiable)
        rho             rho;

        // Optional (inherited) entries
        field           <inpField>;
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "PecletNo/PecletNo.H", "PecletNo" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "PecletNo",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "PecletNo/PecletNo.H", "rho" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: PecletNo                 | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 rho          | Name of density field               | word | no       | rho

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func PecletNo

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/PecletNo" %>

API:

- [Foam::functionObjects::PecletNo](doxy_id://Foam::functionObjects::PecletNo)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
