---
title: rotatingPressureInletOutletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-rotatingPressureInletOutletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `rotatingPressureInletOutletVelocity` is a velocity inlet/outlet boundary
condition that applies a zero-gradient condition for outflow (as defined by
the flux); and obtains velocity from the flux with a direction normal to
the patch faces. The condition is applied to patches in a rotating frame
where the pressure is specified.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            rotatingPressureInletOutletVelocity;
        omega           <Function1<vector>>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "rotatingPressureInletOutletVelocityFvPatchVectorField.H", "rotatingPressureInletOutletVelocity" %>
<% assert :string_exists, "rotatingPressureInletOutletVelocityFvPatchVectorField.C", "omega" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `rotatingPressureInletOutletVelocity`   | word    | yes      | -
`oemga`        | Angular velocity of the frame \[rad/s\]      | Function1\<vector\> | yes  | -

The inherited entries are elaborated in:

- [Foam::pressureInletOutletVelocityFvPatchVectorField](doxy_id://Foam::pressureInletOutletVelocityFvPatchVectorField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

- Sign conventions:
  - positive flux (out of domain): apply zero-gradient condition
  - negative flux (into of domain): derive from the flux with specified
    direction
{: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/rotatingPressureInletOutletVelocity" %>

API:

- [Foam::rotatingPressureInletOutletVelocityFvPatchVectorField](doxy_id://Foam::rotatingPressureInletOutletVelocityFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
