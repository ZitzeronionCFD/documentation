---
title: Realizable k-ε
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-realizablekepsilon
license: CC-BY-NC-ND-4.0
menu_parent: ras-linearevm
group: turbulence-ras-evm
---

<%= page_toc %>

## Properties {#properties}

## Model equations {#model-equations}

The turbulence kinetic energy equation is given by:

$$
    \Ddt{\rho k}
  =
    \div \left(\rho D_k \grad k\right)
  + \rho G
  - \frac{2}{3}\rho \left(\div \u\right) k
  - \rho \epsilon
  + S_k
$$

and the dissipation rate by:

$$
    \Ddt{\rho \epsilon}
  =
    \div \left(\rho D_\epsilon \grad \epsilon \right)
  + C_1 \rho \mag{\tensor{S}} \epsilon
  - C_2 \rho \frac{\epsilon^2}{k + \left(\nu \epsilon\right)^{0.5}}
  + S_\epsilon
$$

The turbulence viscosity is calculated using:

$$
    \nu_t = C_{\mu} \frac{k^2}{\epsilon}
$$

where the $$ C_{\mu} $$ is given by:

$$
    C_{\mu} = \frac{1}{A_0 + A_s U^{*} \frac{k}{\epsilon}}
$$

## Default model coefficients {#default-coeffs}

## Initialisation {#initialisation}

## Usage {#usage}

The model is specified using:

    RAS
    {
        turbulence      on;
        RASModel        realizableKE;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "realizableKE.H", "realizableKE" %>

## Boundary conditions {#bcs}

Walls

## Further information {#further-information}

Source code:

- [Foam::RASModels::realizableKE](doxy_id://Foam::RASModels::realizableKE)

References:

- *Shih et al.* <%= cite "shih_new_1995" %>
