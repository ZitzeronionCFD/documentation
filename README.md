<!----------------------------------------------------------------------------->
# OpenFOAM&reg; Docs Project

This project generates and deploys the OpenFOAM&reg; documentation website
to https://docs.openfoam.com.

# System Requirements

The installation procedure has been tested on the following operating systems:

    openSUSE Leap 15.4
    Ubuntu 22.04.2 LTS

# Prerequisites

The installation requires the following software:

    Doxygen
    Markdown
    ruby
    nanoc

### Install and Configure: `ruby`

The project uses `rbenv` to manage multiple `ruby` versions.

Install `rbenv`:

    git clone https://github.com/rbenv/rbenv.git ~/.rbenv

Update the `PATH`:

    echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc

Initialise the `ruby` environment:

    ~/.rbenv/bin/rbenv init

Update `rbenv` (if needed):

    cd ~/.rbenv && git pull

Update the list of available `ruby` versions:

    cd ~/.rbenv/plugins/ruby-build && git pull

List the versions:

    rbenv install -l

Install `ruby 3.1.3`:

    rbenv install 3.1.3
    rbenv global 3.1.3

# Installation

The following instructions assume that the user is building the OpenFOAM&reg;
Docs Projectthe in the directory `~/openfoam/documentation`. Please update this
directory path as necessary.

Create the environment variable `OF_DOC_DIR`:

    export OF_DOC_DIR=~/openfoam/documentation

Check if the environment variable is set correctly:

    echo "$OF_DOC_DIR"

Retrieve the source of the OpenFOAM&reg; documentation from the public GitLab
repository:

    git clone https://gitlab.com/openfoam/documentation.git "$OF_DOC_DIR"
    cd "$OF_DOC_DIR"

Install the `ruby` development environment:

    bundle install

The following instruction assumes that the user has built OpenFOAM&reg; in the
directory `~/openfoam/openfoam-v2212`. (For more information, see the
[OpenFOAM&reg; build guides](https://develop.openfoam.com/Development/openfoam/-/blob/master/doc/Build.md).)
Please update this directory path as necessary.

Source the target OpenFOAM&reg; environment:

    source ~/openfoam/openfoam-v2212/etc/bashrc

Add the `Doxygen` tag file for the API cross-references. The tags file is
generated while building the `Doxygen` API documentation. This is available
for `v2212` download at:

    https://dl.openfoam.com/source/v2212/OpenFOAM-api.tags

Alternatively, create the file locally using:

    # Build the Doxygen-based OpenFOAM documentation - this may take time
    cd "$WM_PROJECT_DIR"/doc
    ./Allwmake -online

    # Copy the tags file
    mkdir -p "$OF_DOC_DIR"/assets/doxygen
    cp -f Doxygen/DTAGS "$OF_DOC_DIR"/assets/doxygen/OpenFOAM-api.tags

Build the OpenFOAM&reg; Docs Project:

    cd "$OF_DOC_DIR"
    ./Allmake

# Usage

View the OpenFOAM&reg; Docs Project in browser by issuing:

    cd "$OF_DOC_DIR"
    nanoc view

Navigate to `localhost:3000/<API>` on your browser, e.g.:

    export OF_API="$(foamEtcFile -show-api)"
    firefox localhost:3000/"$OF_API"

# Troubleshooting

Check the internal links:

    cd "$OF_DOC_DIR"
    nanoc check ilinks

# Contact

You can contact [OpenCFD](https://www.openfoam.com/) via its
[Contact us](https://www.openfoam.com/contact-us) page.

# Copyright

The licence of the OpenFOAM&reg; Docs Project is
[CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/):
[LICENSE](LICENSE.md).

## Internals

Main files:

- content directory
- layouts directory

Main controls

- nanoc.yaml
- Rules

Custom code:

- lib directory
  - data_sources
  - filters
  - helpers

Navigation built using `menu_list.rb`

<!----------------------------------------------------------------------------->
