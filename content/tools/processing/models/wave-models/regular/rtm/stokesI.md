---
title: Stokes I
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- regular
- Stokes
- waves
menu_id: waves-regular-stokesi
license: CC-BY-NC-ND-4.0
menu_parent: models-waves
group: wave-regular
---

<%= page_toc %>

## Properties {#properties}

- Regular waves

## Model equations {#model-equations}

The wave height is modelled by the equation:

$$
    \eta = \frac{H}{2} \cos (k x -\omega t + \phi)
$$

Where:

$$H$$
: wave height

$$k$$
: wave number

$$\omega$$
: angular frequency

$$\phi$$
: phase shift

$$t$$
: time

## Default model coefficients {#default-coeffs}

## Usage {#usage}

Inlet patch example

    <patch>
    {
        alpha           alpha.water;
        waveModel       StokesI;
        nPaddle         1;
        waveHeight      0.05;
        waveAngle       0.0;
        rampTime        3.0;
        activeAbsorption yes;
        wavePeriod      3.0;
    }

## Further information {#further-information}

Source code:

- [Foam::waveModels::StokesI](doxy_id://Foam::waveModels::StokesI)

References:

- ...

Tutorials:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/stokesI" %>
