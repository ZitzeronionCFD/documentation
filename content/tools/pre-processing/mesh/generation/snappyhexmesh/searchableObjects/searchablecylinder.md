---
title: searchableCylinder
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: searchablecylinder
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh-geometry
group: searchableobjects
---

<%= page_toc %>

A cylinder given by two points and a radius.

## Usage

    cylinder
    {
        type            searchableCylinder;
        point1          (1.5 1 -0.5);
        point2          (3.5 2 0.5);
        radius          0.05;
    }

<% assert :string_exists, "searchableCylinder.H", "searchableCylinder" %>
<% assert :string_exists, "searchableCylinder.C", "point1" %>
<% assert :string_exists, "searchableCylinder.C", "point2" %>
<% assert :string_exists, "searchableCylinder.C", "radius" %>

Where:

Property     | Description               | Required
-------------|---------------------------|----------
`point1`     | Axis point at the start   |      yes
`point2`     | Axis point at the end     |      yes
`radius`     | Cylinder radius           |      yes
