---
title: Mesh-motion boundary conditions
short_title: Mesh motion
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-meshMotion
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

Available mesh-motion boundary conditions include:

<%= insert_models "bcs-meshMotion" %>
