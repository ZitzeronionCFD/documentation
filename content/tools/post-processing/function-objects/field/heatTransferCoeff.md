---
title: heatTransferCoeff
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- heat transfer
- post-processing
menu_id: function-objects-field-heattransfercoeff
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `heatTransferCoeff` function object computes
the heat transfer coefficient as a `volScalarField` for a set of patches.

### Operands

Operand      | Type               | Location
:---         |:---                |:---
input        | -                  | -
output file  | -                  | -
output field | `volScalarField`   | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `heatTransferCoeff` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    heatTransferCoeff1
    {
        // Mandatory entries (unmodifiable)
        type            heatTransferCoeff;
        libs            (fieldFunctionObjects);
        field           <field>;
        patches         (<patch1> <patch2> ... <patchN>);
        htcModel        <htcModel>;
        UInf            (20 0 0);
        Cp              CpInf;
        CpInf           1000;
        rho             rhoInf;
        rhoInf          1.2;

        // Optional (inherited) entries
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists,
   "heatTransferCoeff/heatTransferCoeff.H", "heatTransferCoeff" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "heatTransferCoeff",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: heatTransferCoeff        | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 field        | Name of the operand field           | word | yes      | -
 patches      | Names of the operand patches        | wordList | yes  | -
 htcModel     | Model for coefficient computation   | word | yes      | -
 UInf         | Reference flow speed                | scalar | yes    | -
 rho          | Fluid density                       | scalar | yes    | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func "heatTransferCoeff(<field>)"

The following options are available for the `htcModel` entry:

- `ReynoldsAnalogy`: Reynolds analogy

$$ h = 0.5 \rho_\infty C_{p,\infty} |U_{\infty}| C_f $$

- `localReferenceTemperature`: local reference temperature

$$ h = \frac{q}{T_c - T_w} $$

- `fixedReferenceTemperature`: fixed reference temperature

$$ h = \frac{q}{T_c - T_{\ref}} $$

<% assert :string_exists, "ReynoldsAnalogy.H", "ReynoldsAnalogy" %>
<% assert :string_exists, "localReferenceTemperature.C", "localReferenceTemperature" %>
<% assert :string_exists, "fixedReferenceTemperature.C", "fixedReferenceTemperature" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/heatTransferCoeff" %>

API:

- [Foam::functionObjects::heatTransferCoeff](doxy_id://Foam::functionObjects::heatTransferCoeff)

<%= history "v1712" %>

<!----------------------------------------------------------------------------->
