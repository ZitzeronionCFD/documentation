---
title: inclinedFilmNusseltHeight
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-inclinedFilmNusseltHeight
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-region
group: bcs-region
---

<%= page_toc %>

## Description

The `inclinedFilmNusseltHeight` is boundary condition that provides
a film height boundary condition for inclined films that imposes a
sinusoidal perturbation on top of a mean flow rate, where the height is
calculated using the Nusselt solution.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            inclinedFilmNusseltHeight;
        GammaMean       <Function1<scalar>>;
        a               <Function1<scalar>>;
        omega           <Function1<scalar>>;

        // Optional entries
        filmRegion      <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "inclinedFilmNusseltHeightFvPatchScalarField.H", "inclinedFilmNusseltHeight" %>
<% assert :string_exists, "inclinedFilmNusseltHeightFvPatchScalarField.C", "GammaMean" %>
<% assert :string_exists, "inclinedFilmNusseltHeightFvPatchScalarField.C", "a" %>
<% assert :string_exists, "inclinedFilmNusseltHeightFvPatchScalarField.C", "omega" %>
<% assert :string_exists, "inclinedFilmNusseltHeightFvPatchScalarField.C", "filmRegion" %>

where:

Property     | Description                                | Type    | Required | Default
-------------|--------------------------------------------|---------|----------|---------
`type`       | Type name: `inclinedFilmNusseltHeight`     | word    | yes      | -
`GammaMean`  | Mean mass flow rate per unit length \[kg/s/m\] | Function1\<scalar\> | yes | -
`a`          | Perturbation amplitude \[m\]               | Function1\<scalar\>   | yes | -
`omega`      | Perturbation frequency \[rad/s/m\]         | Function1\<scalar\>   | yes | -
`filmRegion` | Name of film region                        | word    | no       | surfaceFilmProperties

The inherited entries are elaborated in:

- fixedValueFvPatchScalarFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/regionModels/surfaceFilmModels/derivedFvPatchFields/inclinedFilmNusseltHeight" %>

API:

- [Foam::inclinedFilmNusseltHeightFvPatchScalarField](doxy_id://Foam::inclinedFilmNusseltHeightFvPatchScalarField)

<%= history "2.2.0" %>

<!----------------------------------------------------------------------------->
