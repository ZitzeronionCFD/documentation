
window.onload = function()
{
    var version_select = document.getElementById("select-version-opt");

    versions.forEach(v => {
        option = document.createElement('option');
        option.value = v;
        option.text = v;
        version_select.add(option);
    });

    var selected_version = localStorage.getItem("version");

    if (selected_version) {
        version_select.value = selected_version;
    }

    $("#select-version-opt").change(function (e) {
        localStorage.setItem("version", e.target.value);
        window.location.assign('/' + e.target.value + '/');
        console.log(e.target.value);
        console.log(localStorage);
    });
}