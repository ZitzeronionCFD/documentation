---
title: Face-corrected surface-normal gradient scheme
short_title: Face-corrected
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- gradient
menu_id: schemes-sngrad-facecorrected
license: CC-BY-NC-ND-4.0
menu_parent: schemes-sngrad
group: schemes-sngrad
---

<%= page_toc %>

## Properties {#properties}

- Central-difference snGrad scheme with non-orthogonal correction
- delta coefficients given by: nonOrthDeltaCoeffs
- explicit non-orthogonal correction

## Usage {#usage}

The scheme is specified using:

    snGradSchemes
    {
        default         none;
        grad(U)         faceCorrected;
    }

## Further information {#further-information}

Source code

- [Foam::fv::faceCorrectedSnGrad](doxy_id://Foam::fv::faceCorrectedSnGrad)
