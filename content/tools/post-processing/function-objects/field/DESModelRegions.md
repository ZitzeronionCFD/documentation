---
title: DESModelRegions
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- DES
- field
- function object
- post-processing
menu_id: function-objects-field-desmodelregions
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `DESModelRegions` function object computes an indicator field for
detached eddy simulation (DES) turbulence calculations, where the values
the indicator mean:

    0 = Reynolds-averaged Navier-Stokes (RAS) regions
    1 = Large eddy simulation (LES) regions

The `DESModelRegions` function object can only be executed for DES simulations.

### Operands

Operand       | Type            | Location
:---          |:---             |:---
 input        | -               | -
 output file  | dat             | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
 output field | volScalarField  | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `DESModelRegions` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    DESModelRegions1
    {
        // Mandatory entries (unmodifiable)
        type            DESModelRegions;
        libs            (fieldFunctionObjects);

        // Optional entries (runtime modifiable)
        result          DESField;

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "DESModelRegions.H", "DESModelRegions" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "DESModelRegions",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "DESModelRegions.H", "result" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: DESModelRegions          | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 result       | Name of DES indicator field         | word | no       | `<FO>`

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/DESModelRegions" %>

API:

- [Foam::functionObjects::DESModelRegions](doxy_id://Foam::functionObjects::DESModelRegions)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
