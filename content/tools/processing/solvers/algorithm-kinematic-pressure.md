---
title: Kinematic pressure
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
menu_id: algorithm-kinematic-pressure
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

For constant density problems the system of equations can be simplified.
The [incompressible](ref_id://solvers-incompressible) solvers
transform the pressure from static pressure, $$ p_s $$ \[pa\] to kinematic
pressure $$ p_k $$, i.e.

$$
    p_k = \frac{p_s}{\rho} \qquad \mathrm{[m^2/s^2]}
$$

Note that the kinematic pressure has the name `p` in OpenFOAM solvers, the
same as the name for the static pressure `p`.  Accordingly, to interpret the
results from incompressible solvers, the kinematic pressure should be multiplied
by the reference density (constant) value.
