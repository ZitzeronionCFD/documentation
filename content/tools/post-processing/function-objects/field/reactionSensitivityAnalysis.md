---
title: reactionSensitivityAnalysis
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- reaction
- sensitivity
menu_id: function-objects-field-reactionsensitivityanalysis
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `reactionsSensitivityAnalysis` function object
computes indicators for reaction rates of creation or destruction
of species in each reaction.

This function object creates four data files named:

    "consumption"    :   consumption rate
    "production"     :   destruction rate
    "productionInt"  :   integral between dumps of the production rate
    "consumptionInt" :   integral between dumps of the consumption rate

### Operands

Operand       | Type       | Location
:---          |:---        |:---
 input        | -          | -
 output file  | dat        | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
 output field | -          | -

## Usage {#usage}

Example of the `reactionsSensitivityAnalysis` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    reactionsSensitivityAnalysis1
    {
        // Mandatory entries (unmodifiable)
        type            reactionsSensitivityAnalysis;
        libs            (fieldFunctionObjects);

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists,
   "reactionsSensitivityAnalysis.H", "reactionsSensitivityAnalysis" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "reactionsSensitivityAnalysis",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: reactionsSensitivityAnalysis    | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Usage by the `postProcess` utility is not available.

### Notes on entries

- Function object only applicable to single cell cases.
- Needs a `chemistryModel` chosen.

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/reactionSensitivityAnalysis" %>

API:

- [Foam::functionObjects::reactionsSensitivityAnalysis](doxy_id://Foam::functionObjects::reactionsSensitivityAnalysis)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
