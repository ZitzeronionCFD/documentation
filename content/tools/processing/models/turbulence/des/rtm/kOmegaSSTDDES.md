---
title: k-ω-SST Delayed Detached Eddy Simulation (DDES)
short_title: kOmegaSSTDDES
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- DES
- DDES
- turbulence
menu_id: des-komegasstddes
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-des
group: turbulence-des
---

<%= page_toc %>

## Properties {#properties}

- Two equation model for the turbulence kinetic energy, $$ k $$, and
  turbulence specific dissipation rate, $$ \omega $$.
- Based on the [k-omega SST](ref_id://ras-komegasst) model

## Model equations {#model-equations}

The model equations are the same as used by the
[DES variant](<%= ref_id "des-komegasstiddes", "model-equations" %>)
of the model, with a different approximation for $$ \tilde{d} $$:

## Default model coefficients {#default-coeffs}

## Initialisation {#initialisation}

## Usage {#usage}

The model is specified using:

    LES
    {
        turbulence      on;
        LESModel        kOmegaSSTDDES;
    }

<% assert :string_exists, "LESModel.C", "LES" %>
<% assert :string_exists, "LESModel.C", "LESModel" %>
<% assert :string_exists, "kOmegaSSTDDES.H", "kOmegaSSTDDES" %>

## Boundary conditions {#bcs}

Inlet

- <%= link_to_menuid "boundary-conditions-fixedValue" %>

Outlet

- [Constrained outlet](ref_id://boundary-conditions-inletOutlet)

Walls

- wall functions

## Further information {#further-information}

Source code:

- [Foam::LESModels::kOmegaSSTDDES](doxy_id://Foam::LESModels::kOmegaSSTDDES)

References:

- *Gritskevich et al.* <%= cite "gritskevich_development_2012" %>

See also:

- [DES variant](ref_id://des-komegasstdes)
