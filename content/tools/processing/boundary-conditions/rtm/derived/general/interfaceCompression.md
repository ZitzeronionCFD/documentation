---
title: interfaceCompression
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-interfaceCompression
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `interfaceCompression` is a generic boundary condition that applies
interface-compression to the phase-fraction distribution at the
patch by setting the phase-fraction to 0 if it is below 0.5, otherwise
to 1.

This approach is useful to avoid unphysical "bleed" of the lighter phase
along the surface in regions of high shear adjacent to the surface which
is at a shallow angle to the interface.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            interfaceCompression;

        // Inherited entries
        ...
    }

<% assert :string_exists, "interfaceCompressionFvPatchScalarField.H", "interfaceCompression" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `interfaceCompression` | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/interfaceCompression" %>

API:

- [Foam::interfaceCompressionFvPatchScalarField](doxy_id://Foam::interfaceCompressionFvPatchScalarField)

<%= history "v1706" %>

<!----------------------------------------------------------------------------->
