---
title: copiedFixedValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-copiedFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `copiedFixedValue` is a generic boundary condition that
copies the boundary values from a user specified field.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            copiedFixedValue;
        sourceFieldName     <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "copiedFixedValueFvPatchScalarField.H", "copiedFixedValue" %>
<% assert :string_exists, "copiedFixedValueFvPatchScalarField.C", "sourceFieldName" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `copiedFixedValue` | word    | yes  | -
`sourceFieldName`  | Name of the source field      | word    | yes  | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/reactingTwoPhaseEulerFoam/RAS/wallBoiling1D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/phaseSystemModels/reactingEuler/multiphaseSystem/derivedFvPatchFields/copiedFixedValue" %>

API:

- [Foam::copiedFixedValueFvPatchScalarField](doxy_id://Foam::copiedFixedValueFvPatchScalarField)

<%= history "v2012" %>

<!----------------------------------------------------------------------------->
