---
title: PIMPLE algorithm
menu_id: pv-pimple
license: CC-BY-NC-ND-4.0
menu_parent: pressure-velocity
---

<%= page_toc %>

\underConstruction

Combines the [PISO](ref_id://pv-piso) and
[SIMPLE](ref_id://pv-simple) algorithms

<!--
while t < endtime do
Set t := t + ∆t
repeat
Construct [M[U]] ] {momentum matrix}
if Under-relax U then
Under-relax [M[U]] ]
end if
if Predict momentum then
Solve Eqn. (3) for U
end if {momentum predictor}
Set pIter := 0
repeat
Set pIter := pIter + 1
Construct pressure equation Eqn. (8)
Solve Eqn. (8) for p {pressure corrector}
Substitute p into Eqn. (7) ⇒ φ {flux corrector}
if Under-relax p then
Under-relax p
end if
Substitute p into Eqn. (6) ⇒ U {momentum corrector}
until PISO loop converged or pIter > nIter
until SIMPLE loop converged or pIter > nIter
end while
-->
