---
title: uniformFixedGradient
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-uniformFixedGradient
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-fa
group: bcs-fa
---

<%= page_toc %>

## Description

The `uniformFixedGradient` is a boundary condition that provides
a uniform fixed gradient condition.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            uniformFixedGradient;
        uniformGradient <Function1<Type>>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "uniformFixedGradientFaPatchField.H", "uniformFixedGradient" %>
<% assert :string_exists, "uniformFixedGradientFaPatchField.C", "uniformGradient" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `uniformFixedGradient`          | word    | yes      | -
`uniformGradient`      | Uniform gradient field                     | Function1\<Type\>  | yes      | -

The inherited entries are elaborated in:

- fixedGradientFaPatchField.H
- Function1.H
<!-- end of the list -->

-   The `value` entry (optional) is used for the initial values.
    Otherwise the `uniformGradient` is used for the evaluation.
    In some cases (eg, coded or expression entries with references to other
    fields) this can be problematic and the `value` entry will be needed.
{: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteArea/fields/faPatchFields/derived/uniformFixedGradient" %>

API:

- [Foam::uniformFixedGradientFaPatchField](doxy_id://Foam::uniformFixedGradientFaPatchField)

<%= history "v2306" %>

<!----------------------------------------------------------------------------->
