---
title: Reynolds stress transport models (RSTM)
short_title: RSTM
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- Reynolds stress
- RAS
- turbulence
menu_id: ras-rstm
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-ras
---

<%= page_toc %>

Reynolds stress transport turbulence model selections include:
<%= insert_models "turbulence-ras-rstm" %>

## Background {#background}

The Reynolds stress transport equation reads:

$$
    \Ddt{\tensor{R}}
  =
    \underbrace{\tensor{P}}_{production}
  + \underbrace{\tensor{\Pi}}_{pressure-strain}
  - \underbrace{\tensor{\epsilon}}_{dissipation}
  + \underbrace{\tensor{D}}_{diffusion}
  + \underbrace{\tensor{\Omega}}_{rotation}
$$

Here, the production term $$ \tensor{P} $$ is given by:

$$
    \tensor{P}
  =
  - \tensor{R} \dprod \left(\grad \u + \left(\grad \u \right)^T \right)
$$

All other terms on the r.h.s. require modelling.
