---
title: Boussinesq
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- Boussinesq
- equation of state
menu_id: equationOfState-Boussinesq
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-equationOfState
group: equation-of-state-models
---

<%= page_toc %>

Density, $$ \rho $$ \[kg/m$$^3$$\]

$$
\rho = \rho_0 \left( 1 - \beta (T - T_0) \right)
$$

Entropy, $$ S $$ \[J/kg/K \]

$$
S = 0
$$

Compressibility, $$ \psi $$ \[s$$^2$$/m$$^2$$\]

$$
\psi = 0
$$

## Usage

The `Boussinesq` equation of state is specified as:

~~~
equationOfState
{
    rho0        <scalar>;
    T0          <scalar>;
    beta        <scalar>;
}
~~~

Where

- `rho0` : Reference density \[kg/m$$^3$$\]
- `T0` : Reference temperature \[K\]
- `beta` : Thermal expansion coefficient \[1/K\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/equationOfState/Boussinesq" %>
