---
title: nutkWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-nutkWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `nutkWallFunction` is boundary condition that provides
a wall function for the turbulent viscosity (i.e. `nut`) based
on the turbulent kinetic energy, (i.e. `k`) for for low- and high-Reynolds
number applications.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            nutkWallFunction;

        // Inherited entries
        ...
    }

<% assert :string_exists, "nutkWallFunctionFvPatchScalarField.H", "nutkWallFunction" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `nutkWallFunction`         | word    | yes      | -

The inherited entries are elaborated in:

- nutWallFunctionFvPatchScalarField.H
- wallFunctionBlenders.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/pipeCyclic" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/nutWallFunctions/nutkWallFunction" %>

API:

- [Foam::nutkWallFunctionFvPatchScalarField](doxy_id://Foam::nutkWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
