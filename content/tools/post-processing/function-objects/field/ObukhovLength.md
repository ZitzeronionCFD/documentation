---
title: ObukhovLength
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- friction velocity
- function object
- length
- Obukhov
- post-processing
menu_id: function-objects-field-obukhovlength
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `ObukhovLength` function object computes the Obukhov length field
and associated friction velocity field.

When scaled by the ground-normal height, i.e. `z`, the Obukhov length becomes
a dimensionless stability parameter, i.e. `z/L`, for atmospheric boundary
layer modelling, expressing the relative roles of buoyancy and shear in the
production and dissipation of turbulent kinetic energy.

The model expressions (<%= cite "obukhov_atm_1971" %>, Eq. 26):

$$
    u^* = \sqrt{ \max \left(\nu_t \sqrt{| \grad{\u} + \grad{\u}^T |^2}, \zeta \right)}
$$

$$
    L_o = - \frac{(u^*)^3}{ {sign}(B) \kappa \max (|B|, \zeta)}
$$

with

$$
    B = \alpha_t \beta \frac{\grad{T} \cdot \vec{g}}{\rho}
$$

where:

$$u^*$$
: Friction velocity, \[m/s\]

$$\nu_t$$
: Turbulent viscosity, \[m2/s\]

$$\u$$
: Velocity, \[m/s\]

$$L_o$$
: Obukhov length, \[m\]

$$B$$
: Buoyancy production term, \[m2/s3\]

$$\alpha_t$$
: Kinematic turbulent thermal conductivity, \[m2/s\]/\[kg/m/s\]

$$\rho$$
: Density of fluid, \[kg/m3\]

$$\beta$$
: Thermal expansion coefficient, \[1/K\]

$$T$$
: Temperature, \[K\]

$$\vec{g}$$
: Gravitational acceleration, \[m/s2\]

$$\zeta$$
: A very small number to avoid floating point exceptions, \[-\]

Required fields:

    U           | Velocity                                 [m/s]
    T           | Temperature                              [K]
    nut         | Turbulent viscosity                      [m2/s]
    alphat      | Kinematic turbulent thermal conductivity [m2/s]/[kg/m/s]
    g           | Gravitational acceleration               [m/s2]

### Operands

Operand        | Type           | Location
:---           |:---            |:---
input          | -              | -
output file    | -              | -
output field 1 | volScalarField | `$FOAM_CASE/<time>/<ObukhovLength>`
output field 2 | volScalarField | `$FOAM_CASE/<time>/<Ustar>`

## Usage {#usage}

Example of the `ObukhovLength` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    ObukhovLength1
    {
        // Mandatory entries (unmodifiable)
        type            ObukhovLength;
        libs            (fieldFunctionObjects);

        // Optional entries (runtime modifiable)
        U               U;
        result1         ObukhovLength;
        result2         Ustar;
        rhoRef          1.0;
        kappa           0.4;
        beta            3e-3;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "ObukhovLength.H", "ObukhovLength" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "ObukhovLength",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "ObukhovLength.C", "U" %>
<% assert :string_exists, "ObukhovLength.C", "result1" %>
<% assert :string_exists, "ObukhovLength.C", "result2" %>
<% assert :string_exists, "ObukhovLength.C", "rhoRef" %>
<% assert :string_exists, "ObukhovLength.C", "kappa" %>
<% assert :string_exists, "ObukhovLength.C", "beta" %>
<% assert :string_exists, "fieldsExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: ObukhovLength            | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 U            | Name of the velocity field          | word |  no      | U
 result1   | Name of the output field for ObukhovLength | word | no   | ObukhovLength
 result2   | Name of the output field for Ustar     | word | no       | Ustar
 rhoRef    | Reference density (to convert from kinematic to static pressure) | scalar | no  | 1.0
 kappa     | von Kármán constant                    | scalar | no     | 0.40
 beta      | Thermal expansion coefficient          | scalar | no     | 3e-3

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Example by using the `postProcess -func` utility:

    postProcess -func "ObukhovLength(<UField>)"

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmForestStability" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/functionObjects/ObukhovLength" %>

API:

- [Foam::functionObjects::ObukhovLength](doxy_id://Foam::functionObjects::ObukhovLength)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
