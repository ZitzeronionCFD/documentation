---
title: postProcess
tags:
- post-processing
copyright:
- Copyright 2022 (C) OpenCFD Ltd.
menu_id: postProcess
license: CC-BY-NC-ND-4.0
menu_parent: post-processing-utilities
group: utilities-post-processing
---

<%= page_toc %>

## Overview {#overview}

The `postProcess` utility evaluates function objects supplied in a
`dictionary` or via the command line.

## Usage

When specified without additional options, the `postProcess` utility
executes all function objects listed in the [controlDict](ref_id://controldict)
file for all time directories.

Also, the `-postProcess` option is available to almost all solvers,
and operates similarly to the stand-alone `postProcess` utility.

Note that the `postProcess` utility does not read any fields by default.
Required fields can be made available using additional command line options,
or inserting a `readFields` function object if using dictionary-based input.
{: .note}

### Synopsis

<%= help_util_usage "postProcess" %>

~~~
<solver> -postProcess [OPTIONS]
~~~

#### Examples

##### Custom dictionary

Using a custom dictionary, here using the user-specified `myDict`:

~~~
postProcess -dict myDict
~~~

where `myDict` contains the list of function objects, e.g.

~~~
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      myDict;
}

functions
{
    // Note: the phi field is required by the CourantNo function object

    readFields1
    {
        type        readFields;
        libs        (fieldFunctionObjects);
        fields      (phi);
    }

    CourantNo1
    {
        type        CourantNo;
        libs        (fieldFunctionObjects);
    }
}
~~~
<% assert :string_exists, "CourantNo/CourantNo.H", "CourantNo" %>

##### Command line

Most function objects can be invoked directly without the need to specify
the input dictionary using the `-func` option, e.g. to execute the
[CourantNo](ref_id://function-objects-field-courantno) function object:

~~~
postProcess -func CourantNo
~~~

Function objects that require fields can use the `-func` option and insert
the field(s) using a quoted string and parentheses, e.g. to extract velocity
[components](ref_id://function-objects-field-components):

~~~
postProcess -func "components(U)"
~~~

or to [add](ref_id://function-objects-field-add) fields `T1` and `T2`:

~~~
postProcess -func "add(T1, T2)"
~~~

Also, multiple function objects can be invoked simultaneously:

~~~
postProcess -funcs '(components(U) grad(p))'
~~~

### Input

#### Arguments

<%= help_util_arguments "postProcess" %>

#### Options

<%= help_util_options "postProcess" %>

#### Files

<%= no_input "file" %>

#### Fields

<%= no_input "field" %>

### Output

#### Logs

<%= no_output "log" %>

#### Files

<%= no_output "file" %>

#### Fields

<%= no_output "field" %>

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/basic/potentialFoam/cylinder" %>
- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleAdiabaticFoam/rutlandVortex2D" %>

Source code:

- <%= repo_link2 "$FOAM_UTILITIES/postProcessing/postProcess" %>

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
