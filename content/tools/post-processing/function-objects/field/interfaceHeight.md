---
title: interfaceHeight
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- interface
- height
- post-processing
menu_id: function-objects-field-interfaceheight
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `interfaceHeight` function object reports the height of the interface above
a set of locations. For each location, it writes the vertical distance of the
interface above both the location and the lowest boundary. It also writes
the point on the interface from which these heights are computed. It uses
an integral approach, so if there are multiple interfaces above or below a
location then this method will generate average values.

### Operands

Operand         | Type            | Location
:---            |:---             |:---
 input          | -               | -
 output file 1  | dat  | `$FOAM_CASE/postProcessing/<FO>/<time>/height`
 output file 2  | dat  | `$FOAM_CASE/postProcessing/<FO>/<time>/position`
 output field   | -               | -

## Usage {#usage}

Example of the `interfaceHeight` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    interfaceHeight1
    {
        // Mandatory entries (unmodifiable)
        type            interfaceHeight;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        locations       ((0 0 0) (10 0 0) (20 0 0));

        // Optional entries (runtime modifiable)
        alpha           alpha.water;
        liquid          true;
        direction       (1 0 0);
        interpolationScheme    cellPoint;

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "interfaceHeight.H", "interfaceHeight" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "interfaceHeight",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "interfaceHeight.C", "locations" %>
<% assert :string_exists, "interfaceHeight.C", "alpha" %>
<% assert :string_exists, "interfaceHeight.C", "liquid" %>
<% assert :string_exists, "interfaceHeight.C", "direction" %>
<% assert :string_exists, "interfaceHeight.C", "interpolationScheme" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: interfaceHeight          | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 locations    | Locations to report the height at   | vectorList | yes | -
 alpha        | Name of alpha field                 | word |  no      | alpha
 liquid       | Flag if the alpha field that of the liquid | bool | no | true
 direction    | Direction of interface              | vector | no     | g
 interpolationScheme | Interpolation scheme         | word | no  | cellPoint

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/multiphase/StefanProblem" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/interfaceHeight" %>

API:

- [Foam::functionObjects::interfaceHeight](doxy_id://Foam::functionObjects::interfaceHeight)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
