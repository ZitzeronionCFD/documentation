---
title: Joule heating
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- energy
- fvOption
- heat transfer
- source
menu_id: fvoption-jouleheating
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-energy
---

<%= page_toc %>

This source provides users with the ability to add Joule Heating contributions
to any thermal solver.

## Properties {#properties}

The option solves an equation for the electrical potential, V, of the form

$$
    \div (\sigma \grad V) = 0
$$

Where $$ \sigma $$ (`sigma`) is the electrical conductivity. The thermal source is then given by

$$
    \dot{Q} = (\sigma \grad V ) \dprod \grad V
$$

A sample result is shown below
![Joule heating example](../example-fvoptions-sources-joule-heating-source.png)

## Usage {#usage}

The option is specified using:

    heating
    {
        type            jouleHeatingSource;
        active          true;

        jouleHeatingSourceCoeffs
        {
            anisotropicElectricalConductivity no;
        }
    }

<% assert :string_exists, "fvOptions/sources/derived/jouleHeatingSource/jouleHeatingSource.H", "jouleHeatingSource" %>
<% assert :string_exists, "fvOptions/sources/derived/jouleHeatingSource/jouleHeatingSource.H", "anisotropicElectricalConductivity" %>

The electrical conductivity can be specified using either (see usage):

- If not present the `sigma` field will be read from file (standard field sigma)
- If the `sigma` entry is present the electrical conductivity is
specified as a (potentially uniform) function of temperature using a
`Function1` type, e.g.

    heating
    {
        type            jouleHeatingSource;
        active          true;

        jouleHeatingSourceCoeffs
        {
            anisotropicElectricalConductivity no;

            // Optionally specify sigma as a function of temperature
            sigma           table
            (
                (0      127599.8469)
                (1000   127599.8469)
            );
        }
    }

<% assert :string_exists, "fvOptions/sources/derived/jouleHeatingSource/jouleHeatingSource.H", "anisotropicElectricalConductivity" %>
<% assert :string_exists, "fvOptions/sources/derived/jouleHeatingSource/jouleHeatingSource.H", "sigma" %>

- If the `anisotropicElectricalConductivity` flag is set to 'true', `sigma` should be specified as a vector quantity.

<% assert :string_exists, "fvOptions/sources/derived/jouleHeatingSource/jouleHeatingSource.H", "anisotropicElectricalConductivity" %>

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/chtMultiRegionSimpleFoam/jouleHeatingSolid" %>

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/derived/jouleHeatingSource" %>
