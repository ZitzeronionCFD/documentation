---
title: patchProbes
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- function object
- patch
- points
- post-processing
- sampling
menu_id: function-objects-sampling-patch-probes
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-sampling
group: function-objects-sampling
---

<%= page_toc %>

## Overview {#overview}

The `patchProbes` function object samples patch field values at point locations
and  writes the result to file using the chosen output file format.

## Usage {#usage}

Example of function object specification:

    patchProbes1
    {
        // Mandatory entries
        type                patchProbes;
        libs                (sampling);

        // Conditional entries

            // Specify either 'patch' or 'patches'
            // - if both entries are present, 'patches' wins
            patch               <word>;
            patches             (<wordRes>);

        // Inherited entries
        ...
    }

<% assert :string_exists, "patchProbes.H", "patchProbes" %>
<% assert :class_library_exists, "sampling/Make/files", "probes", "libsampling" %>
<% assert :string_exists, "patchProbes.C", "patch" %>
<% assert :string_exists, "patchProbes.C", "patches" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
`type`        | Type name: patchProbes              | word | yes      | -
`libs`        | Library name: sampling              | word | yes      | -
`patch`       | Name of operand patch               | word | choice   | -
`patches`     | Names of operand patches            | wordRes | choice | -

The inherited entries are elaborated in:

- [probes](ref_id://function-objects-sampling-probes)

### Operands

Operand      | Type                 | Location
|------------|----------------------|---------
Input        | -                    | -
Output file  | dat                  | `<case>/postProcessing/<function object>/<time>/`
Output field | -                    | -

## Stored properties {#stored-properties}

### Reduced data

The following data are stored by the function object for the set of sample
values, and can be used by other function objects:

- `average(<field-name>)` : average set value
- `min(<field-name>)` : minimum set value
- `max(<field-name>)` : maximum set value
- `size(<field-name>)` : size of set

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/backwardFacingStep2D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/sampling/probes" %>

API:

- [Foam::patchProbes](doxy_id://Foam::patchProbes)

<%= history "2.0.0" %>

<!----------------------------------------------------------------------------->
