---
title: atmAlphatWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-atmAlphatWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

The `atmAlphatWallFunction` is a boundary condition that provides
a wall constraint on the kinematic turbulent thermal conductivity
(i.e. `alphat`) for atmospheric boundary layer modelling.  It assumes a
logarithmic distribution of the potential temperature within the first cell.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            atmAlphatWallFunction;
        Pr              <Function1<scalar>>;
        Prt             <PatchFunction1<scalar>>;
        z0              <PatchFunction1<scalar>>;

        // Optional entries
        Cmu             <scalar>;
        kappa           <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "atmAlphatkWallFunctionFvPatchScalarField.H", "atmAlphatWallFunction" %>
<% assert :string_exists, "atmAlphatkWallFunctionFvPatchScalarField.C", "Pr" %>
<% assert :string_exists, "atmAlphatkWallFunctionFvPatchScalarField.C", "Prt" %>
<% assert :string_exists, "atmAlphatkWallFunctionFvPatchScalarField.C", "z0" %>
<% assert :string_exists, "atmAlphatkWallFunctionFvPatchScalarField.C", "Cmu" %>
<% assert :string_exists, "atmAlphatkWallFunctionFvPatchScalarField.C", "kappa" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `atmAlphatWallFunction`         | word    | yes      | -
`Pr`                   | Molecular Prandtl number                   | Function1\<scalar\>  | yes | -
`Prt`                  | Turbulent Prandtl number                   | PatchFunction1\<scalar\> | yes | -
`z0`                   | Surface roughness length \[m\]             | PatchFunction1\<scalar\> | yes | -
`Cmu`                  | Empirical model constant                   | scalar | no        | 0.09
`kappa`                | von Karman constant                        | scalar | no        | 0.41

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- Function1.H
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmFlatTerrain" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/wallFunctions/atmAlphatkWallFunction" %>

API:

- [Foam::atmAlphatkWallFunctionFvPatchScalarField](doxy_id://Foam::atmAlphatkWallFunctionFvPatchScalarField)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
