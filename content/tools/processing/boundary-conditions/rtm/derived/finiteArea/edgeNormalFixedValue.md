---
title: edgeNormalFixedValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-edgeNormalFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-fa
group: bcs-fa
---

<%= page_toc %>

## Description

The `edgeNormalFixedValue` is a boundary condition that provides
an edge normal fixed value vector field for finite area calculations.

This condition describes a surface normal vector boundary condition by its
magnitude.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            edgeNormalFixedValue;
        refValue        <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "edgeNormalFixedValueFaPatchVectorField.H", "edgeNormalFixedValue" %>
<% assert :string_exists, "edgeNormalFixedValueFaPatchVectorField.C", "refValue" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `edgeNormalFixedValue`          | word    | yes      | -
`refValue`             |  Edge-normal velocity field                | scalarField | yes  | -

The inherited entries are elaborated in:

- fixedValueFaPatchFields.H
<!-- end of the list -->

- The value is positive for outward-pointing vectors.
{: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteArea/fields/faPatchFields/derived/edgeNormalFixedValue" %>

API:

- [Foam::edgeNormalFixedValueFaPatchVectorField](doxy_id://Foam::edgeNormalFixedValueFaPatchVectorField)

<%= history "v1712" %>

<!----------------------------------------------------------------------------->
