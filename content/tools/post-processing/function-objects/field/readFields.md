---
title: readFields
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- read
menu_id: function-objects-field-readfields
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `readFields` function object
reads fields from the time directories and adds them to the mesh database
for further post-processing.

### Operands

Operand       | Type             | Location
:---          |:---              |:---
input         | `vol<Type>Field` | `$FOAM_CASE/<time>/<inpField>`
output file   | -                | -
output field  | `vol<Type>Field` | Mesh database

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `readFields` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    readFields1
    {
        // Mandatory entries (unmodifiable)
        type            readFields;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        fields      (<field1> <field2> ... <fieldN>);

        // Optional entries (runtime modifiable)
        readOnStart true;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists,
   "functionObjects/field/readFields/readFields.H", "readFields" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "readFields",
   "libfieldFunctionObjects" %>
<% assert :string_exists,
   "functionObjects/field/readFields/readFields.C", "fields" %>
<% assert :string_exists,
   "functionObjects/field/readFields/readFields.C", "readOnStart" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: readFields               | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 fields       | Names of the operand fields         | wordList |  yes | -
 readOnStart  | Flag to start reading on start-up   | bool | no       | true

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>
- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/RAS/externalCoupledSquareBendLiq" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/readFields" %>

API:

- [Foam::functionObjects::readFields](doxy_id://Foam::functionObjects::readFields)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->