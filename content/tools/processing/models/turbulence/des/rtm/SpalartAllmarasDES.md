---
title: Spalart-Allmaras Detached Eddy Simulation (DES)
short_title: SpalartAllmarasDES
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- DES
- turbulence
menu_id: des-spalartallmarasdes
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-des
group: turbulence-des
---

<%= page_toc %>

## Properties {#properties}

- One equation model based on a modified turbulence viscosity,
  $$ \tilde{\nu} $$

## Model equations {#model-equations}

The $$ \tilde{\nu} $$ transport equation is given by:

$$
    \Ddt{\rho \tilde{\nu}}
    =
    \div \left( \rho D_\tilde{\nu} \tilde{\nu} \right)
    + \frac{C_{b2}}{\sigma_{\nu_t}} \rho \mag{\grad \tilde{\nu}}^2
    + C_{b1} \rho \tilde{S} \tilde{\nu} \left(1 - f_{t2}\right)
    - \left(C_{w1} f_w - \frac{C_{b1}}{\kappa^2} f_{t2}\right)
    \rho \frac{\tilde{\nu}^2}{\tilde{d}^2}
    + S_\tilde{\nu}
$$

where the length scale $$ \tilde{d} $$ is defined by:

$$
    \tilde{d} = \min \left( \Psi C_{DES} \Delta, y \right)
$$

and $$ \Psi $$ is the low Reynolds number correction function:

$$
    \Psi^2 =
        \min
        \left[
            10^2,
            \frac{1 - \frac{1 - C_{b1}}{C_{w1} \kappa^2 f_w^{*}} \left[ f_{t2} + \left(1 - f_{t2}\right) f_{v2} \right]}{f_{v1} \max \left(10^{-10}, 1-f_{t2} \right)}
        \right]
$$

## Default model coefficients {#default-coeffs}

$$ \sigma_{\nu_t} $$ | $$ C_{b1} $$ | $$ C_{b2} $$ | $$ C_{w1} $$ | $$ C_{w2} $$ | $$ C_{w3} $$
--------------------|----------------|----------------|----------------|----------------|---------------
  2/3 | 0.1355 | 0.622 | $$\frac{C_{b1}}{\kappa^2} + \frac{1 + C_{b2}}{\sigma_{\nu_t}} $$  |  0.3 |    2

$$ C_{v1} $$ | $$ C_{s} $$ | $$ C_{\mathit{DES}} $$| $$ C_{k} $$ | $$ C_{t3} $$ | $$ C_{t4} $$ | $$ f_w^{*} $$
---------------|---------------|---------------------------|--------------|----------------|---------------|------------
           7.1 |           0.3 |                      0.65 |    0.07      |   1.2          | 0.5 | 0.424

## Initialisation {#initialisation}

## Usage {#usage}

The model is specified using:

    LES
    {
        turbulence      on;
        LESModel        SpalartAllmarasDES;

        // Optional entries
        SpalartAllmarasDESCoeffs
        {
            // Apply low-Reynolds number correction; default = yes
            lowReCorrection     yes;
        }
    }

<% assert :string_exists, "LESModel.C", "LES" %>
<% assert :string_exists, "LESModel.C", "LESModel" %>
<% assert :string_exists, "SpalartAllmarasDES.H", "SpalartAllmarasDES" %>

## Boundary conditions {#bcs}

Inlet

- <%= link_to_menuid "boundary-conditions-fixedValue" %>

Outlet

- [Constrained outlet](ref_id://boundary-conditions-inletOutlet)

Walls

- wall functions

## Further information {#further-information}

Source code:

- [Foam::LESModels::SpalartAllmarasDES](doxy_id://Foam::LESModels::SpalartAllmarasDES)

References:

- Principle reference: *Spalart et al.* <%= cite "spalart_comments_1997" %>
- Low Reynolds number correction:  *Spalart et al.* <%= cite "spalart_new_2006" %>
