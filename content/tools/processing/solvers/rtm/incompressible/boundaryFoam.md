---
title: boundaryFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- solvers
- incompressible
menu_id: boundaryFoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-incompressible
group: solvers-incompressible
solver_models:
- turbulence
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-incompressible" %>
- steady state
- incompressible
- [turbulence](ref_id://models-turbulence)
- evolves the flow system to reach a target average velocity
- useful to set velocity and turbulence inflow conditions based on a fully
  developed flow profile

## Equations {#equations}

The velocity is modelled according to the equation

$$
    \div \tensor{R}_{dev} = \grad p + S_U
$$

where the velocity and pressure gradient are corrected to achieve the target
velocity $$ \overline{\u} $$.  After solving the above equation, the velocity
is corrected from the current volume-averaged velocity, i.e.

$$
    \u^{n+1} = \u^n + \left( \overline{\u} - \overline{\u}^n \right)
$$

and the pressure gradient from:

$$
    \left(\grad p\right)^{n+1} = \left(\grad p\right)^{n} + \overline{\frac{1}{\mat{A}}\left( \overline{\u} - \overline{\u}^n \right)}
$$

where the averages are calculated as volume averages.

## Input requirements {#input-requirements}

Mandatory fields:

- p: <%= link_to_menuid "algorithm-kinematic-pressure" %> \[m2/s2\]
- U: velocity \[m/s\]

### Physical models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence):
  `constant/turbulenceProperties`
- [finite volume options](ref_id://numerics-fvoptions#usage):
  `constant/fvOptions` (optional)

### Solution controls {#input-requirements-solution-controls}

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/incompressible/boundaryFoam" %>

See also

- <%= link_to_menuid "simplefoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/boundaryFoam" %>
