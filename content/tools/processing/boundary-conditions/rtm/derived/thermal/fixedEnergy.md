---
title: fixedEnergy
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-fixedEnergy
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `fixedEnergy` is a boundary condition that provides
a fixed condition for internal energy.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedEnergy;

        // Inherited entries
        ...
    }

<% assert :string_exists, "fixedEnergyFvPatchScalarField.H", "fixedEnergy" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `fixedEnergy`                   | word    | yes      | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/basic/derivedFvPatchFields/fixedEnergy" %>

API:

- [Foam::fixedEnergyFvPatchScalarField](doxy_id://Foam::fixedEnergyFvPatchScalarField)

<%= history "v1906" %>

<!----------------------------------------------------------------------------->
