---
title: uniformTotalPressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-uniformTotalPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-uniformTotalPressure
group: bcs-inlet-pressure
---

<%= page_toc %>

## Description

The `uniformTotalPressure` is a boundary condition that provides
provides a time-varying form of the uniform total pressure boundary condition
`totalPressure`.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            uniformTotalPressure;
        p0              <Function1<scalar>>;

        // Optional entries
        U               <word>;
        psi             <word>;
        rho             <word>;
        phi             <word>;

        // Conditional entries

            // if 'psi' is not 'none'
            gamma       <scalar>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "uniformTotalPressureFvPatchScalarField.H", "uniformTotalPressure" %>
<% assert :string_exists, "uniformTotalPressureFvPatchScalarField.C", "p0" %>
<% assert :string_exists, "uniformTotalPressureFvPatchScalarField.C", "gamma" %>
<% assert :string_exists, "uniformTotalPressureFvPatchScalarField.C", "U" %>
<% assert :string_exists, "uniformTotalPressureFvPatchScalarField.C", "psi" %>
<% assert :string_exists, "uniformTotalPressureFvPatchScalarField.C", "phi" %>
<% assert :string_exists, "uniformTotalPressureFvPatchScalarField.C", "rho" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `uniformTotalPressure`          | word    | yes      | -
`p0`           | Total pressure field                       | Function1\<scalar\> | yes  | -
`U`            | Name of velocity field                     | word    | no       | U
`psi`          | Name of compressibility field              | word    | no       | psi
`phi`          | Name of flux field                         | word    | no       | phi
`rho`          | Name of density field                      | word    | no       | rho
`gamma`        | Heat-capacity ratio                        | scalar  | conditional | 1

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- Function1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/TJunction" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/uniformTotalPressure" %>

API:

- [Foam::uniformTotalPressureFvPatchScalarField](doxy_id://Foam::uniformTotalPressureFvPatchScalarField)

<%= history "2.1.0" %>

<!----------------------------------------------------------------------------->
