---
title: adjointOutletFlux
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-adjointOutletFlux
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-optimisation
group: bcs-optimisation
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            adjointOutletFlux;

        // Inherited entries
        ...
    }

<% assert :string_exists, "adjointOutletFluxFvPatchField.H", "adjointOutletFlux" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `adjointOutletFlux`             | word    | yes      | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/optimisation/adjointOptimisation/adjoint/turbulenceModels/incompressibleAdjoint/adjointRAS/derivedFvPatchFields/adjointOutletFlux" %>

API:

- [Foam::adjointOutletFluxFvPatchField](doxy_id://Foam::adjointOutletFluxFvPatchField)

<%= history "v1906" %>

<!----------------------------------------------------------------------------->
