---
title: Mixture
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- mixture
- thermophysical
menu_id: thermophysical-mixture
license: CC-BY-NC-ND-4.0
menu_parent: models-thermophysical
---

<%= page_toc %>

## Options {#options}

Mixture model selections include:

<%= insert_models "mixture-models" %>

Additional non-documented models (links to API guide):

- [egrMixture](doxy_id://Foam::egrMixture)
- [homogeneousMixture](doxy_id://Foam::homogeneousMixture)
- [inhomogeneousMixture](doxy_id://Foam::inhomogeneousMixture)
- [singleStepReactingMixture](doxy_id://Foam::singleStepReactingMixture)
- [veryInhomogeneousMixture](doxy_id://Foam::veryInhomogeneousMixture)

## Usage {#usage}

The mixture type is specified in the `thermoType` dictionary:

~~~
thermoType
{
    mixture         <model>;
    <...>
}
~~~

Additional entries will then be required based on the user selection.

## Further information {#further-information}

Source code

- [basicMixture](doxy_id://Foam::basicMixture)
