---
title: pisoFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- solvers
- incompressible
menu_id: pisofoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-incompressible
group: solvers-incompressible
solver_models:
- transient
- turbulence
- fvOption
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-incompressible" %>
- transient
- incompressible
- <%= link_to_menuid "models-turbulence" %>
- <%= link_to_menuid "fvoptions" %>

## Equations {#equations}

- <%= link_to_menuid "pv-piso" %>

## Input requirements {#input-requirements}

Mandatory fields:

- p: <%= link_to_menuid "algorithm-kinematic-pressure" %> \[m2/s2\]
- U: velocity \[m/s\]

### Physical models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence):
  `constant/turbulenceProperties`
- [finite volume options](ref_id://numerics-fvoptions#usage) :
  `constant/fvOptions` (optional)

### Solution controls {#input-requirements-solution-controls}

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/incompressible/pisoFoam" %>

See also

- <%= link_to_menuid "simplefoam" %>
- <%= link_to_menuid "rhopimplefoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam" %>
