---
title: JohnsonJacksonParticleSlip
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-JohnsonJacksonParticleSlip
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-multiphase
group: bcs-multiphase
---

<%= page_toc %>

## Description

The `JohnsonJacksonParticleSlip` is a boundary condition that provides
partial slip boundary condition for the particulate velocity.

#### References

    Reuge, N., Cadoret, L., Coufort-Saudejaud, C.,
    Pannala, S., Syamlal, M., & Caussat, B. (2008).
    Multifluid Eulerian modeling of dense gas–solids fluidized bed
    hydrodynamics: influence of the dissipation parameters.
    Chemical Engineering Science, 63(22), 5540-5551.

    Johnson, P. C., & Jackson, R. (1987).
    Frictional–collisional constitutive relations for granular materials,
    with application to plane shearing.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            JohnsonJacksonParticleSlip;
        specularityCoefficient  <dimensionedScalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "JohnsonJacksonParticleSlipFvPatchVectorField.H", "JohnsonJacksonParticleSlip" %>
<% assert :string_exists, "JohnsonJacksonParticleSlipFvPatchVectorField.C", "specularityCoefficient" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `JohnsonJacksonParticleSlip`    | word    | yes      | -
`specularityCoefficient` | Specularity coefficient \[-\]            | dimensionedScalar  | yes     | -

The inherited entries are elaborated in:

- partialSlipFvPatchFields.H
<!-- end of the list -->

- The specularity coefficient has to be within (0, 1).
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/reactingTwoPhaseEulerFoam/RAS/LBend" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/phaseSystemModels/twoPhaseEuler/phaseCompressibleTurbulenceModels/kineticTheoryModels/derivedFvPatchFields/JohnsonJacksonParticleSlip" %>

API:

- [Foam::JohnsonJacksonParticleSlipFvPatchVectorField](doxy_id://Foam::JohnsonJacksonParticleSlipFvPatchVectorField)

<%= history "v2012" %>

<!----------------------------------------------------------------------------->
