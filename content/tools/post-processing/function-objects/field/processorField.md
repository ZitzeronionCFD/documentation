---
title: processorField
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- parallel
- post-processing
- processor
menu_id: function-objects-field-processorfield
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `processorField` function object
writes a scalar field whose value is the local processor ID.
The output field name is `processorID`.

### Operands

Operand       | Type             | Location
:---          |:---              |:---
input         | `volScalarField` | `$FOAM_CASE/<time>/<inpField>`
output file   | -                | -
output field  | `volScalarField` | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `processorField` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    processorField1
    {
        // Mandatory entries (unmodifiable)
        type            processorField;
        libs            (fieldFunctionObjects);

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "processorField/processorField.H", "processorField" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "processorField",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: processorField           | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Example by using the `postProcess` utility:

    postProcess -func processorField

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/overSimpleFoam/aeroFoil" %>
- <%= repo_link2 "$FOAM_TUTORIALS/mesh/snappyHexMesh/motorBike_leakDetection" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/processorField" %>

API:

- [Foam::functionObjects::processorField](doxy_id://Foam::functionObjects::processorField)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->