---
title: setFlow
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- setFlow
menu_id: function-objects-field-setflow
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `setFlow` function object
provides options to set the velocity and flux fields as a function of time.

Useful for testing advection behaviour of numerical schemes by e.g.
imposing solid body rotation, vortex flows.  All types include a scaling
`Foam::Function1` type enabling the strength of the transformation to vary as
a function of time.

### Operands

Operand       | Type                       | Location
:---          |:---                        |:---
input         | `{vol,surface}<Type>Field` | `$FOAM_CASE/<time>/<inpField>`
output file   | -                          | -
output field  | `{vol,surface}<Type>Field` | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `setFlow` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    setFlow1
    {
        // Mandatory entries (unmodifiable)
        type            setFlow;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        mode        rotation;
        scale       1;

        // Optional entries (runtime modifiable)
        U           U;
        rho         none;
        phi         phi;
        reverseTime 1;

        // When mode=function
        velocity    (1 0 0);

        // When mode=rotation
        omega       6.28318530718;
        origin      (0.5 0 0.5);
        refDir      (1 0 0);
        axis        (0 1 0);

        // When mode=(vortex2D|vortex3D)
        origin      (0.5 0 0.5);
        refDir      (1 0 0);
        axis        (0 1 0);

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "setFlow.H", "setFlow" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files", "setFlow", "libfieldFunctionObjects" %>
<% assert :string_exists, "setFlow.C", "axis" %>
<% assert :string_exists, "setFlow.C", "refDir" %>
<% assert :string_exists, "setFlow.C", "origin" %>
<% assert :string_exists, "setFlow.C", "omega" %>
<% assert :string_exists, "setFlow.C", "velocity" %>
<% assert :string_exists, "setFlow.C", "reverseTime" %>
<% assert :string_exists, "setFlow.C", "phi" %>
<% assert :string_exists, "setFlow.C", "rho" %>
<% assert :string_exists, "setFlow.C", "U" %>
<% assert :string_exists, "setFlow.C", "scale" %>
<% assert :string_exists, "setFlow.C", "mode" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: setFlow                  | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 mode         | Operating mode - see below          | word | yes      | -
 scale        | Scaling function        | Function1\<scalar\> | yes     | -
 U            | Name of velocity field              | word |  no      | U
 rho          | Name of density field               | word |  no      | none
 phi          | Name of flux field                  | word |  no      | phi
 reverseTime | Specified time to reverse flow direction  | scalar  | no | VGREAT
 velocity     | Velocity function                   | Function1\<vector\> | conditional | -
 omega        | Rotational speed function           | Function1\<scalar\> | conditional | -
 origin       | Rotation vector origin              | vector            | conditional | -
 refDir       | Rotation vector reference direction | vector  | conditional | -
 axis         | Rotation vector axis                | vector  | conditional | -

Options for the `mode` entry:

    function
    rotation
    vortex2D
    vortex3D

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2("$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity") %>
- <%= repo_link2(
  "$FOAM_TUTORIALS/multiphase/interIsoFoam/discInReversedVortexFlow") %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/setFlow" %>

API:

- [Foam::functionObjects::setFlow](doxy_id://Foam::functionObjects::setFlow)

<%= history "v1706" %>

<!----------------------------------------------------------------------------->