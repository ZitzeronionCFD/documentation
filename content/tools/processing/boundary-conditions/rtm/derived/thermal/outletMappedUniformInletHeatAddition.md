---
title: outletMappedUniformInletHeatAddition
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-outletMappedUniformInletHeatAddition
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `outletMappedUniformInletHeatAddition` is a boundary condition that provides
a temperature boundary condition averages the temperature over the
"outlet" patch specified by name "outletPatchName" and applies an extra
heat source. This is set as a uniform temperature value on this patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            outletMappedUniformInletHeatAddition;
        outletPatch     <word>;
        Q               <scalar>;

        // Optional entries
        phi             <word>;
        TMin            <scalar>;
        TMax            <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "outletMappedUniformInletHeatAdditionFvPatchField.H", "outletMappedUniformInletHeatAddition" %>
<% assert :string_exists, "outletMappedUniformInletHeatAdditionFvPatchField.C", "outletPatch" %>
<% assert :string_exists, "outletMappedUniformInletHeatAdditionFvPatchField.C", "Q" %>
<% assert :string_exists, "outletMappedUniformInletHeatAdditionFvPatchField.C", "phi" %>
<% assert :string_exists, "outletMappedUniformInletHeatAdditionFvPatchField.C", "TMin" %>
<% assert :string_exists, "outletMappedUniformInletHeatAdditionFvPatchField.C", "TMax" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `outletMappedUniformInletHeatAddition` | word    | yes    | -
`outletPatch`  | Name of the outlet patch to be mapped      | word    | yes      | -
`Q`            | Heat addition \[W\]                        | scalar  | yes      | -
`phi`          | Name of flux field                         | word    | no       | phi
`TMin`         | Minimum temperature limit                  | scalar  | no       | 0
`TMax`         | Maximum temperature limit                  | scalar  | no       | 5000

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/RAS/TJunction" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/outletMappedUniformInletHeatAddition" %>

API:

- [Foam::outletMappedUniformInletHeatAdditionFvPatchField](doxy_id://Foam::outletMappedUniformInletHeatAdditionFvPatchField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
