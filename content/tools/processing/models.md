---
title: Models
copyright:
- Copyright (C) 2018-2023 OpenCFD Ltd.
tags:
- thermophysical
- turbulence
- waves
menu_id: models
license: CC-BY-NC-ND-4.0
menu_parent: processing
---

<%= page_toc %>

## Turbulence {#turbulence}

- [Overview](turbulence)
- [Reynolds Averaged Simulation (RAS)](turbulence/ras)
- [Large Eddy Simulation (LES)](turbulence/les)
- [Detached Eddy Simulation (DES)](turbulence/des)

## Thermophysical {#thermophysical}

- [Overview](thermophysical)
- [grpCombustionModels](doxy_id://grpCombustionModels)

## Multiphase {#multiphase}

- [Wave modelling](wave-models)
- Eulerian multiphase
- Discrete particle modelling
  - Lagrangian modelling (intermediate library)
    - [grpLagrangianIntermediateClouds](doxy_id://grpLagrangianIntermediateClouds)
    - [grpLagrangianIntermediateParcels](doxy_id://grpLagrangianIntermediateParcels)
    - [grpLagrangianIntermediateSubModels](doxy_id://grpLagrangianIntermediateSubModels)
    - [grpLagrangianIntermediateKinematicSubModels](doxy_id://grpLagrangianIntermediateKinematicSubModels)
    - [grpLagrangianIntermediateThermoSubModels](doxy_id://grpLagrangianIntermediateThermoSubModels)
    - [grpLagrangianIntermediateReactingSubModels](doxy_id://grpLagrangianIntermediateReactingSubModels)
    - [grpLagrangianIntermediateReactingMultiphaseSubModels](doxy_id://grpLagrangianIntermediateReactingMultiphaseSubModels)
    - [grpLagrangianIntermediateMPPICSubModels](doxy_id://grpLagrangianIntermediateMPPICSubModels)
    - [grpLagrangianIntermediateFunctionObjects](doxy_id://grpLagrangianIntermediateFunctionObjects)
