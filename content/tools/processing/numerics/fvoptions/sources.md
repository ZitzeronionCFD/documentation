---
title: Sources
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- source
menu_id: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
menu_parent: numerics-fvoptions
---

<%= page_toc %>

## General {#general}

<%= insert_models "fvoptions-sources-general" %>

## Momentum {#momentum}

### Single region {#momentum-single-region}

<%= insert_models "fvoptions-sources-momentum" %>

### Multi-region {#momentum-multi-region}

<%= insert_models "fvoptions-sources-momentum-multi-region" %>

## Energy {#energy}

### Single region {#energy-single-region}

<%= insert_models "fvoptions-sources-energy" %>

### Multi-region {#energy-multi-region}

<%= insert_models "fvoptions-sources-energy-multi-region" %>

## Atmospheric boundary layer {#atm}

<%= insert_models "fvoptions-sources-atm" %>

<!----------------------------------------------------------------------------->
