---
title: searchableDisk
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: searchabledisk
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh-geometry
group: searchableobjects
---

<%= page_toc %>

![disk example](../images/disk-small.png)

A disk with zero thickness based its origin, normal direction and radius.

## Usage

    disk
    {
        type            searchableDisk;
        origin          (0 0 0);
        normal          (0 1 0);
        radius          0.314;
    }

<% assert :string_exists, "searchableDisk.H", "searchableDisk" %>
<% assert :string_exists, "searchableDisk.C", "origin" %>
<% assert :string_exists, "searchableDisk.C", "normal" %>
<% assert :string_exists, "searchableDisk.C", "radius" %>

Where:

Property     | Description               | Required
-------------|---------------------------|----------
`origin`     | Disk origin               |      yes
`normal`     | Normal direction          |      yes
`radius`     | Disk radius               |      yes
