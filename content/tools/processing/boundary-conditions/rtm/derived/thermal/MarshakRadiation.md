---
title: MarshakRadiation
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-MarshakRadiation
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `MarshakRadiation` is a mixed boundary condition that
provides a Marshak condition for the incident radiation field
(usually written as `G`).

The radiation temperature is retrieved from the mesh database, using a
user specified temperature field name.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            MarshakRadiation;

        // Optional entries
        T               <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "MarshakRadiationFvPatchScalarField.H", "MarshakRadiation" %>
<% assert :string_exists, "MarshakRadiationFvPatchScalarField.C", "T" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `MarshakRadiation`        | word    | yes      | -
`T`                    | Name of temperature field                  | word    | no       | T

The inherited entries are elaborated in:

- mixedFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantSimpleFoam/hotRadiationRoom" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/radiation/derivedFvPatchFields/MarshakRadiation" %>

API:

- [Foam::radiation::MarshakRadiationFvPatchScalarField](doxy_id://Foam::radiation::MarshakRadiationFvPatchScalarField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
