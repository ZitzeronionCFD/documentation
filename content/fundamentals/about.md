---
title: About
copyright:
- Copyright (C) 2021-2023 OpenCFD Ltd.
menu_id: about
license: CC-BY-NC-ND-4.0
menu_parent: fundamentals
menu_weight: 10
---

Welcome to the OpenFOAM&reg; documentation!

<!-- Brief overview of the software: What is its purpose? -->

OpenFOAM&reg; (for "**Open**-source **F**ield **O**peration **A**nd **M**anipulation")
is a powerful, free and open-source continuum mechanics software.

The main purpose of OpenFOAM&reg; is to enable engineers, scientists, and
researchers to easily and reliably model and analyze continuum mechanics
problems in a wide range of applications. With OpenFOAM&reg;, users can gain
valuable insights into fluid flow phenomena, optimize designs,
evaluate performance, and make informed engineering decisions.

<!-- What does OpenFOAM offer its users? -->
OpenFOAM&reg; offers a comprehensive suite of pre-processing, processing and
post-processing tools, and libraries for simulating diverse scenarios,
such as incompressible and compressible flows, [turbulence](ref_id://models-turbulence)
and [heat transfer](ref_id://models-thermophysical), optimisation, acoustics,
chemical reactions, solid mechanics and electromagnetics. It finds applications
in industries such as automotive, aerospace, energy, environmental engineering,
chemical processing, and academic research.

<!-- What does OpenFOAM offer to its users? -->

OpenFOAM&reg;'s modular and easily extensible nature allows users to customize
and extend the software to suit their specific needs in no time. It provides a
flexible framework for implementing user-defined models, boundary conditions,
and numerical schemes, enabling advanced and customised simulation capabilities.

OpenFOAM&reg;'s free and open-source nature encourages collaboration, knowledge
sharing, and innovation within its software community.
The transparency offered by OpenFOAM&reg; allows users to see the
inner workings of the algorithms and models, providing a robust platform for
development and collaborative research.
OpenFOAM&reg; empowers its users to contribute to the software's development,
improve its functionality, and benefit from the collective expertise of the
community.

OpenFOAM&reg; is programmed in [C++](https://isocpp.org/), using the
[GNU GLP3 license](https://www.gnu.org/licenses/gpl-3.0.en.html).
This license guarantees the open sourceness of the project while
allowing private and commercial usage. OpenFOAM&reg; is publicly hosted in
[GitLab](https://develop.openfoam.com/)
and is actively maintained and developed by [OpenCFD Ltd.](https://www.openfoam.com).

In OpenFOAM&reg;, complex concepts can be written in a familiar fashion.
For example, systems of equations are implemented using a syntax that closely
follows the mathematical notation, e.g.:

Time rate of change | $$ \ddt{\phi} $$ | `fvc::ddt(phi)`
Gradient            | $$ \grad \phi $$ | `fvc::grad(phi)`
Divergence          | $$ \div \phi $$ | `fvc::div(phi)`
Laplacian           | $$ \laplacian \phi $$ | `fvc::laplacian(phi)`
Linearised sources  | $$ s \phi $$ |  `fvc::Sp(s,phi)`

This abstraction permits complex equations to be written concisely in a
human-readable form, e.g. the transport equation to evolve the P-1 radiation
model:

$$
    \div \left( \Gamma \grad G \right) - a G = -4 \epsilon \sigma T^4 - E
$$

is represented by the code:

~~~
// Solve G transport equation
solve
(
    fvm::laplacian(gamma, G_)
  - fvm::Sp(a_, G_)
  ==
  - 4.0*(e_*physicoChemical::sigma*pow4(T_)) - E_
);
~~~

These examples make use of explicit operations using *Finite Volume Calculus*,
represented by the `fvc::` prefix, where implicit terms are represented
analogously using the *Finite Volume Method* `fvm::` form. For more
information, please see <%= link_to_menuid "numerics" %>.
{: .note}

Many pre-built applications are supplied ready-to-use complete with a
[tutorial suite](repo_id://tutorials) to showcase functionality.

<!----------------------------------------------------------------------------->

