---
title: turbulentInlet
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
- turbulence
menu_id: boundary-conditions-turbulentInlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `turbulentInlet` is a boundary condition that produces spatiotemporal-variant
field by summing a set of pseudo-random numbers and a given spatiotemporal-invariant
mean field. The field can be any type, e.g. `scalarField`. At a single point and
time, all components are summed by the same random number, e.g. velocity
components (u, v, w) are summed by the same random number, p; thus, output
is (u+p, v+p, w+p).

The pseudo-random number generator obeys the probability density function
of the uniform distribution constrained by the range \[0:1\]. The seed for
the random number generator is hard-coded; therefore, it will produce the
same sequence of random numbers at every execution.

$$
    x_p = (1 - \alpha) x_p^{n - 1} + \alpha (x_{ref} + c s R |x_{ref}|)
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$x_p$$               | Patch field
$$x_{ref}$$           | Spatiotemporal-invariant patch scalar
$$n$$                 | Time level
$$\alpha$$            | A scalar attempting to build two-temporal-point correlations by heuristically adding a fraction of the new random component to the previous time patch field
$$c$$                 | A heuristic automatically calculated correction term to compensate energy level losses due to the alpha scalar
$$R$$                 | Pseudo-random number \[HARD-CODED seed\]
$$s$$                 | Fluctuation scale (proportional to the xRef)

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type             patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type             turbulentInlet;
        fluctuationScale <scalar>;               // the term `s` above
        referenceField   <scalarField>;          // the term `xRef` above

        // Optional entries
        alpha            <scalar>;               // the term `alpha` above

        // Inherited entries
        ...
    }

<% assert :string_exists, "turbulentInletFvPatchField.H", "turbulentInlet" %>
<% assert :string_exists, "turbulentInletFvPatchField.C", "fluctuationScale" %>
<% assert :string_exists, "turbulentInletFvPatchField.C", "referenceField" %>
<% assert :string_exists, "turbulentInletFvPatchField.C", "alpha" %>

where:

Property              | Description                                | Type    | Required | Default
----------------------|--------------------------------------------|---------|----------|---------
`type`                | Type name: `turbulentInlet`                | word    | yes      | -
`fluctuationScale`    | RMS fluctuation scale (fraction of mean)   | scalar  | yes      | -
`referenceField`      | Reference (mean) field                     | scalarField | yes  | -
`alpha`               | Fraction of new random component added to previous  | scalar | no | 0.1

The inherited entries are elaborated in:

- [fixedValueFvPatchField.H](ref_id://boundary-conditions-fixedValue)
- Random.H
<!-- end of the list -->

- This boundary condition should not be used for DES or LES computations as a
  turbulent velocity inflow condition, because the BC will not produce
  turbulence-alike time-series, and will decay almost immediately downstream
  of the inlet boundary although its historical name suggests the opposite.
- Nevertheless, the BC may be still used for other applications, e.g. as a
  uniform-random noise source in aeroacoustics.
  {: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/LES/pitzDaily" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/turbulentInlet" %>

API:

- [Foam::turbulentInletFvPatchField](doxy_id://Foam::turbulentInletFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
