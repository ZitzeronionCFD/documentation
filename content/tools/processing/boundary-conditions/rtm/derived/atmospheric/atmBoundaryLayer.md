---
title: atmBoundaryLayer
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-convectiveHeatTransfer
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

Base class to set log-law type ground-normal inlet boundary conditions for
wind velocity and turbulence quantities for homogeneous, two-dimensional,
dry-air, equilibrium and neutral atmospheric boundary layer (ABL) modelling.

The ground-normal profile expressions are due to YGCJ
(refer to references below) whereat RH expressions were generalised:

$$
    u = \frac{u^*}{\kappa} \ln \left( \frac{z - d + z_0}{z_0} \right)
$$

$$
    v = w = 0
$$

$$
    k = \frac{(u^*)^2}{\sqrt{C_\mu}}
    \sqrt{C_1 \ln \left( \frac{z - d + z_0}{z_0} \right) + C_2}
$$

$$
    \epsilon = \frac{(u^*)^3}{\kappa (z - d + z_0)}
    \sqrt{C_1 \ln \left( \frac{z - d + z_0}{z_0} \right) + C_2}
$$

$$
    \omega = \frac{u^*}{\kappa \sqrt{C_\mu}} \frac{1}{z - d + z_0}
$$

$$
    u^* =
        \frac{u_{ref} \kappa}{\ln\left(\frac{z_{ref} + z_0}{z_0}\right)}
$$

where:

Property     | Description
-------------|--------------------------------------------
$$u$$        | Ground-normal streamwise flow speed profile          \[m/s\]
$$v$$        | Spanwise flow speed                                  \[m/s\]
$$w$$        | Ground-normal flow speed                             \[m/s\]
$$k$$        | Ground-normal turbulent kinetic energy (TKE) profile \[m^2/s^2\]
$$\epsilon$$ | Ground-normal TKE dissipation rate profile           \[m^2/s^3\]
$$\omega$$   | Ground-normal specific dissipation rate profile      \[m^2/s^3\]
$$u^*$$      | Friction velocity                                    \[m/s\]
$$\kappa$$   | von Kármán constant                                  \[-\]
$$C_\mu$$    | Empirical model constant                             \[-\]
$$z$$        | Ground-normal coordinate component                   \[m\]
$$d$$        | Ground-normal displacement height                    \[m\]
$$z_0$$      | Aerodynamic roughness length                         \[m\]
$$u_{ref}$$  | Reference mean streamwise wind speed at $$z_{ref}$$ \[m/s\]
$$z_{ref}$$  | Reference height being used in $$u^*$$ estimations \[m\]
$$C_1$$      | Curve-fitting coefficient for YGCJ profiles       \[-\]
$$C_2$$      | Curve-fitting coefficient for YGCJ profiles       \[-\]

### References

- The ground-normal profile expressions (tag:RH):
    Richards, P. J., & Hoxey, R. P. (1993).
    Appropriate boundary conditions for computational wind
    engineering models using the k-ε turbulence model.
    In Computational Wind Engineering 1 (pp. 145-153).
    DOI:10.1016/B978-0-444-81688-7.50018-8

- Modifications to preserve the profiles downstream (tag:HW):
    Hargreaves, D. M., & Wright, N. G. (2007).
    On the use of the k–ε model in commercial CFD software
    to model the neutral atmospheric boundary layer.
    Journal of wind engineering and
    industrial aerodynamics, 95(5), 355-369.
    DOI:10.1016/j.jweia.2006.08.002

- Expression generalisations to allow height variation for turbulence quantities (tag:YGCJ):
    Yang, Y., Gu, M., Chen, S., & Jin, X. (2009).
    New inflow boundary conditions for modelling the neutral equilibrium
    atmospheric boundary layer in computational wind engineering.
    J. of Wind Engineering and Industrial Aerodynamics, 97(2), 88-95.
    DOI:10.1016/j.jweia.2008.12.001

- The generalised ground-normal profile expression for omega (tag:YGJ):
    Yang, Y., Gu, M., & Jin, X., (2009).
    New inflow boundary conditions for modelling the
    neutral equilibrium atmospheric boundary layer in SST k-ω model.
    In: The Seventh Asia-Pacific Conference on Wind Engineering,
    November 8-12, Taipei, Taiwan.

- Theoretical remarks (tag:E):
    Emeis, S. (2013).
    Wind Energy Meteorology: Atmospheric
    Physics for Wind Power Generation.
    Springer-Verlag Berlin Heidelberg.
    DOI:10.1007/978-3-642-30523-8

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        flowDir         <vector>;
        zDir            <vector>;
        Uref            <scalar>;
        Zref            <scalar>;
        z0              <scalarField>;
        d               <scalarField>;

        // Optional entries
        kappa           <scalar>;
        Cmu             <scalar>;
        initABL         <bool>;
        phi             <word>;
        C1              <scalar>;
        C2              <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "atmBoundaryLayer.H", "atmBoundaryLayer" %>
<% assert :string_exists, "atmBoundaryLayer.C", "flowDir" %>
<% assert :string_exists, "atmBoundaryLayer.C", "zDir" %>
<% assert :string_exists, "atmBoundaryLayer.C", "Uref" %>
<% assert :string_exists, "atmBoundaryLayer.C", "Zref" %>
<% assert :string_exists, "atmBoundaryLayer.C", "z0" %>
<% assert :string_exists, "atmBoundaryLayer.C", "d" %>
<% assert :string_exists, "atmBoundaryLayer.C", "kappa" %>
<% assert :string_exists, "atmBoundaryLayer.C", "Cmu" %>
<% assert :string_exists, "atmBoundaryLayer.C", "initABL" %>
<% assert :string_exists, "atmBoundaryLayer.C", "phi" %>
<% assert :string_exists, "atmBoundaryLayer.C", "C1" %>
<% assert :string_exists, "atmBoundaryLayer.C", "C2" %>

where:

Property    | Description                                | Type    | Required | Default
------------|--------------------------------------------|---------|----------|---------
`flowDir`   | Flow direction           | Function1\<vector\> | yes | -
`zDir`      | Ground-normal direction  | Function1\<vector\> | yes | -
`Uref`      | Reference mean streamwise flow speed being used in $$u^*$$ estimations \[m/s\] | Function1\<scalar\> | yes | -
`Zref`      | Reference height being used in $$u^*$$ estimations \[m\] | Function1\<scalar\> | yes | -
`z0`        | Surface roughness length \[m\] | PatchFunction1\<scalar\> | yes | -
`d`         | Displacement height \[m\] - see Notes  | PatchFunction1\<scalar\> | yes | -
`kappa`     | von Karman constant      | scalar | no  | 0.41
`Cmu`       | Empirical model constant | scalar | no  | 0.09
`initABL`   | Flag to initialise profiles with the theoretical ABL expressions, otherwise use "value" list | bool   | no  | true
`value`     | ABL profile content when initABL=false  | scalarList | conditional | -
`phi`       | Name of the flux field   | word         | no        | phi
`C1`        | Curve-fitting coefficient YGCJ profiles | scalar | no | 0.0
`C2`        | Curve-fitting coefficient YGCJ profiles | scalar | no | 1.0

The inherited entries are elaborated in:

- Function1.H
- PatchFunction1.H
<!-- end of the list -->

- The RH expressions are special cases of those in YGCJ when `C1=0`
and `C2=1`. Both `C1` and `C2` can be determined by nonlinear fitting
of (YGCJ:Eqs. 19-20) with an experimental dataset for `k`. By default,
`atmBoundaryLayerInlet` boundary conditions compute RH expressions.
- `z` is the ground-normal height relative to the global minimum height
of the inlet patch; therefore, the minimum of `z` is always zero
irrespective of the absolute z-coordinate of the computational patch.
- The derived ABL expressions automatically satisfy the simplified transport
equation for `k`. Yet the same expressions only satisfy the simplified
transport equation for `epsilon` when the model constants `sigmaEpsilon`
is 1.11 with `kappa=0.4` (HW:p. 358).
- `atmBoundaryLayerInlet` boundary conditions inherit `inletOutlet`
traits, so that a given inlet condition can be supplied from all sides of
the domain, e.g. a ground-normal cylinder domain having a single
inlet/outlet boundary where the changes between inlet and outlet depend
on the wind direction and patch normals, so that any change in inflow
orientation can be handled with the same mesh.
- `d` is the displacement height, and "is relevant for flows over forests
and cities" (E:p. 28). "The displacement height gives the vertical
displacement of the entire flow regime over areas which are densely covered
with obstacles such as trees or buildings" (E:p. 28).
{: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/atmBoundaryLayer" %>

API:

- [Foam::atmBoundaryLayer](doxy_id://Foam::atmBoundaryLayer)

<%= history "v1806" %>

<!----------------------------------------------------------------------------->
