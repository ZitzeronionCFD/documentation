---
title: Minmod divergence scheme
short_title: Minmod
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-minmod
license: CC-BY-NC-ND-4.0
group: schemes-divergence-nvdtvd
---

<%= page_toc %>

## Properties {#properties}

- Minimum modulus
- First order
- Unbounded

## Normalised Variable Diagram {#nvd}

![minmod](../minMod.png)

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss Minmod;
    }

<% assert :string_exists, "limitedSchemes/Minmod/Minmod.H", "Minmod" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/limitedSchemes/Minmod" %>

API:

- [Foam::MinmodLimiter](doxy_id://Foam::MinmodLimiter)

Reference:

- *Harten* <%= cite "harten_high_1983" %>
