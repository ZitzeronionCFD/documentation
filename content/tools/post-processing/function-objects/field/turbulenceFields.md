---
title: turbulenceFields
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- turbulence
menu_id: function-objects-field-turbulencefields
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `turbulenceFields` function object
computes various turbulence-related quantities that are not typically
output during calculations, including:

- `k`:          Turbulent kinetic energy
- `epsilon`:    Turbulent kinetic energy dissipation rate
- `omega`:      Specific dissipation rate
- `R`:          Reynolds stress tensor
- `L`:          Integral-length/Mixing-length scale
- `I`:          Turbulence intensity
- `nuTilda`:    Modified turbulent viscosity

<% assert :string_exists, "turbulenceFields.C", "k" %>
<% assert :string_exists, "turbulenceFields.C", "epsilon" %>
<% assert :string_exists, "turbulenceFields.C", "omega" %>
<% assert :string_exists, "turbulenceFields.C", "R" %>
<% assert :string_exists, "turbulenceFields.C", "L" %>
<% assert :string_exists, "turbulenceFields.C", "I" %>
<% assert :string_exists, "turbulenceFields.C", "nuTilda" %>

Additional field selections for incompressible cases:

- `nut`:        Turbulent (edy) kinematic viscosity
- `nuEff`:      Effective kinematic viscosity (laminar + turbulent contributions)
- `devReff`:    Deviatoric stress tensor

<% assert :string_exists, "turbulenceFields.C", "nut" %>
<% assert :string_exists, "turbulenceFields.C", "nuEff" %>
<% assert :string_exists, "turbulenceFields.C", "devReff" %>

Additional field selections for compressible cases:

- `mut`:        Turbulent (eddy) dynamic viscosity
- `muEff`:      Effective dynamic viscosity (laminar + turbulent contributions)
- `alphat`:     Thermal eddy diffusivity
- `alphaEff`:   Effective eddy thermal diffusivity
- `devRhoReff`: Deviatoric stress tensor

<% assert :string_exists, "turbulenceFields.C", "mut" %>
<% assert :string_exists, "turbulenceFields.C", "muEff" %>
<% assert :string_exists, "turbulenceFields.C", "alphat" %>
<% assert :string_exists, "turbulenceFields.C", "alphaEff" %>
<% assert :string_exists, "turbulenceFields.C", "devRhoReff" %>

These fields are stored on the mesh database for further post-processing, and
written as regular volume fields at write intervals.

### Operands

Operand      | Type             | Location
:---         |:---              |:---
input        | -                | -
output file  | -                | -
output field | `vol<Type>Field` | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `turbulenceFields` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    turbulenceFields1
    {
        // Mandatory entries (unmodifiable)
        type            turbulenceFields;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        // Either field or fields entries
        fields          (R devRhoReff);
        field           R;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "turbulenceFields.H", "turbulenceFields" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "turbulenceFields",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: turbulenceFields         | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 fields       | Names of fields to store (see below)  | wordList | yes | -
 field        | Name of a field to store (see below)  | word | yes    | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Example by using the `postProcess` utility:

    <solver> -postProcess -func turbulenceFields

## Sample output {#sample-output}

Fields stored on the mesh database with the prefix `turbulenceProperties:`, e.g.

    turbulenceProperties:mut
    turbulenceProperties:R
    turbulenceProperties:devRhoReff

and written to the standard time directories according to the
[writeControl](ref_id://post-processing-function-objects#usage-run-time-control)
setting.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/turbulenceFields" %>

API:

- [Foam::functionObjects::turbulenceFields](doxy_id://Foam::functionObjects::turbulenceFields)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
