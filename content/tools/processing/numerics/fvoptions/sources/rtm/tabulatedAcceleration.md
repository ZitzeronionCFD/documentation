---
title: Tabulated acceleration
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- acceleration
- fvOption
- source
menu_id: fvoption-tabulatedacceleration
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-momentum
---

<%= page_toc %>

## Properties {#properties}

## Usage {#usage}

The option is specified using:

    disk1
    {
        type            tabulatedAccelerationSource;
        selectionMode   cellSet;
        cellSet         sourceCells1;
        fields          (U);

        timeDataFileName "constant/acceleration-terms.dat";
    }

<% assert :string_exists, "tabulatedAccelerationSource.H", "tabulatedAccelerationSource" %>
<% assert :string_exists, "tabulated6DoFAcceleration.C", "timeDataFileName" %>

The `timeDataFileName` provides the name of a file containing a list of
the motion as a function of time, e.g.:

    //     t      acc    omega   dOmegadt
    (
        ( 0.0  ((1 0 0) (0 0 0) (0 0 0)))
        (10.0  ((2 0 0) (0 0 0) (0 0 0)))
    );

where:

- `t` = time \[s\]
- `acc` = acceleration vector \[m/s2\]
- `omega` = rotation vector \[rad/s\]
- `dOmegadt` = angular acceleration \[rad/s2\]

## Further information {#further-information}

Tutorials

- *none*

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/derived/tabulatedAccelerationSource" %>
