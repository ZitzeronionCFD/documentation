---
title: fieldExpression
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- expression
- field
- function object
- post-processing
menu_id: function-objects-field-fieldexpression
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

Intermediate class for handling field expression function objects
(e.g. `blendingFactor` etc.) whereinto a single field is input.

The class is not an executable function object itself, yet a provider for
common entries to its derived function objects.

## Usage {#usage}

The optional-inherited entries provided by `fieldExpression` to the derived
function objects are as follows:

    <userDefinedSubDictName1>
    {
        // Mandatory and other optional entries
        ...

        // Optional (inherited) entries (runtime modifiable)
        field             <field>;
        result            <fieldResult>;
    }

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 field        | Name of the operand field           | word | yes      | -
 result       | Name of the output field            | wordList | no   | \<FO\>(\<f1\>,...)

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/fieldExpression" %>

API:

- [Foam::functionObjects::fieldExpression](doxy_id://Foam::functionObjects::fieldExpression)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
