---
title: Energy transport equation
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
menu_id: energy-transport
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

The energy transport equation based on enthalpy is given as follows:

$$
    \frac{\partial \rho h}{\partial t}
  + \div (\rho \u h)
  + \frac{\partial \rho K}{\partial t}
  + \div (\rho \u K)
  - \frac{\partial p}{\partial t}
  - \div (\alpha_{eff} \grad h)
    =
    \rho \u \cdot \vec{g}
  + f_{rad}(h)
  + \vec{S}_{\rho, h}
$$

The energy transport equation based on internal energy is given as follows:

$$
    \frac{\partial \rho e}{\partial t}
  + \div (\rho \u e)
  + \frac{\partial \rho K}{\partial t}
  + \div (\rho \u K)
  + \div (p \u)
  - \div (\alpha_{eff} \grad e)
    =
    \rho \u \cdot \vec{g}
  + f_{rad}(e)
  + \vec{S}_{\rho, e}
$$

where:

Property  | Description
----------|-------------
$$t$$     | Time
$$\rho$$  | Density
$$h$$     | Enthalpy
$$e$$     | Internal energy
$$\u$$    | Velocity
$$K$$     | Kinetic energy
$$p$$     | Pressure
$$\alpha_{eff}$$  | Effective thermal diffusivity
$$\vec{g}$$     | Gravitational acceleration
$$f_{rad}$$     | Radiation function
$$\vec{S}$$| Source term through `fvOption`


<!----------------------------------------------------------------------------->
