---
title: atmAmbientTurbSource
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- atmospheric
- fvOption
- source
- turbulence
menu_id: fvoption-atmambientturbSource
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-atm
---

<%= page_toc %>

## Properties {#properties}

- The `atmAmbientTurbSource` applies sources on `k` and either `epsilon`
  or `omega` to prevent them dropping below a specified ambient value for
  atmospheric boundary layer modelling.
- Such adjustment reportedly increases numerical stability for very stable
  atmospheric stability conditions, and prevents nonphysical oscillations in
  regions of low shear at higher altitudes.
- The `atmAmbientTurbSource` can be applied on `epsilon` or
  `omega` based RAS turbulence models.
- The `atmAmbientTurbSource` inherits the traits of the
  [fvOption](ref_id://fvoptions), and `cellSetOption`.

Corrections applied to:

`k`
: Turbulent kinetic energy \[m$$^2$$/s$$^2$$\]

Corrections applied to either of the below, if exist:

`epsilon`
: Turbulent kinetic energy dissipation rate \[m$$^2$$/s$$^3$$\]

`omega`
: Specific dissipation rate \[1/s\]

Required fields:

`k`
: Turbulent kinetic energy \[m$$^2$$/s$$^2$$\]

and either of:

`epsilon`
: Turbulent kinetic energy dissipation rate \[m$$^2$$/s$$^3$$\]

`omega`
: Specific dissipation rate \[1/s\]

## Model equations {#model-equations}

The model expression for `epsilon` (Heuristically derived from
<%= cite "rumsey_atm_2009" %>, Eq. 4, rhs-term:5):

$$
    S_p = \alpha \rho C_2 \frac{\epsilon_{amb}^2}{k_{amb} \epsilon_o} \epsilon
$$

The model expression for `omega` (<%= cite "rumsey_atm_2009" %>, Eq. 4, rhs-term:5):

$$
    S_p = \alpha \rho C_\mu \beta \frac{\omega^2_{amb}}{\omega_o} \omega
$$

The model expression for `k` when `epsilon` is available
(Heuristically derived from (<%= cite "rumsey_atm_2009" %>, Eq. 3, rhs-term:4):

$$
    S_p = \alpha \rho \frac{\epsilon_{amb}}{k_o} k
$$

The model expression for `k` when `omega` is available
(<%= cite "rumsey_atm_2009" %>, Eq. 3, rhs-term:4):

$$
    S_p = \alpha \rho C_\mu \frac{\omega_{amb} k_{amb}}{k_o} k
$$

Where:

$$S_p$$
: Source term without boundary conditions

$$\epsilon$$
: Turbulent kinetic energy dissipation rate (Current iteration) \[m2/s3\]

$$\omega$$
: Specific dissipation rate (Current iteration) \[1/s\]

$$k$$
: Turbulent kinetic energy (Current iteration) \[m2/s2\]

$$\epsilon_{amb}$$
: Ambient epsilon value \[m2/s3\]

$$\omega_{amb}$$
: Ambient omega value \[1/s\]

$$k_{amb}$$
: Ambient k value \[m2/s2\]

$$\epsilon_o$$
: Previous-iteration epsilon \[m2/s3\]

$$\omega_o$$
: Previous-iteration omega \[1/s\]

$$k_o$$
: Previous-iteration k \[m2/s2\]

$$C_2$$
: Model constant \[-\]

$$C_\mu$$
: Empirical model constant \[-\]

$$\beta$$
: Model constant \[-\]

$$\alpha$$
: Phase fraction in multiphase computations, otherwise equals to 1

$$\rho$$
: Fluid density in compressible computations, otherwise equals to 1

## Usage {#usage}

Example of the fvOptions specification using `constant/fvOptions` file:

    atmAmbientTurbSource1
    {
        // Mandatory entries (unmodifiable)
        type                  atmAmbientTurbSource;

        atmAmbientTurbSourceCoeffs
        {
            // Mandatory (inherited) entries (unmodifiable)
            selectionMode    all;

            // Mandatory entries (unmodifiable)
            kAmb              0.0;

            // Optional entries (unmodifiable)
            rho               rho;
            epsilonAmb        0.0;
            omegaAmb          0.0;
        }

        // Optional (inherited) entries
        ...
    }
<% assert :string_exists, "atmAmbientTurbSource.H", "atmAmbientTurbSource" %>
<% assert :string_exists, "atmAmbientTurbSource.C", "kAmb" %>
<% assert :string_exists, "atmAmbientTurbSource.C", "rho" %>
<% assert :string_exists, "atmAmbientTurbSource.C", "epsilonAmb" %>
<% assert :string_exists, "atmAmbientTurbSource.C", "omegaAmb" %>
<% assert :string_exists, "cellSetOption.C", "selectionMode" %>
<% assert :string_exists, "cellSetOption.C", "cellZone" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: atmAmbientTurbSource     | word   | yes    | -
kAmb          | Ambient value for k                 | scalar | yes    | -
rho           | Name of density field               | word   | no     | rho
epsilonAmb    | Ambient value for `epsilon`        | scalar | no     | 0.0
omegaAmb      | Ambient value for `omega`          | scalar | no     | 0.0

The inherited entries are elaborated in:

- [fvOption](ref_id://fvoptions)
  - `cellSetOption`

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmFlatTerrain" %>
- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmForestStability" %>

Source code

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/fvOptions/atmAmbientTurbSource" %>

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
