---
title: k-kl-ω
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-kklomega
license: CC-BY-NC-ND-4.0
menu_parent: ras-linearevm
group: turbulence-ras-evm
---

<%= page_toc %>

## Properties {#properties}

- incompressible only
- three-equation model
- low Reynolds number
- transition modelling

## Model equations {#model-equations}

Specific dissipation rate equation:

$$
    \Ddt{\omega}
  =
    \div \left(D_\omega \grad \omega\right)
    + C_{w1} P_{kt} \frac{\omega}{k_t}
    - \left(1.0 - \frac{C_{wR}}{f_w} \right) k_l \left(R_{bp} + R_{nat}\right) \frac{\omega}{k_t}
    - C_{w2} f_w^2 \omega^2
    + C_{w3} f_\omega \alpha_t f_w^2 \frac{k_t^{0.5}}{y^3}
$$

Laminar kinetic energy equation:

$$
    \Ddt{k_l}
  =
    \div \left( \nu \grad k_l \right)
    + P_{kl}
    - R_{bp} + R_{nat} + D_l
$$

Turbulent kinetic energy equation:

$$
    \Ddt{k_t}
  =
    \div \left(D_k \grad k_t\right)
    + P_{kt}
    + \left(R_{bp} + R_{nat}\right) k_l
    - \omega + D_t
$$

## Default model coefficients {#default-coeffs}

## Usage {#usage}

The model is specified using:

    RAS
    {
        turbulence      on;
        RASModel        kkLOmega;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "kkLOmega.H", "kkLOmega" %>

## Further information {#further-information}

Source code:

- [Foam::incompressible::RASModels::kkLOmega](doxy_id://Foam::incompressible::RASModels::kkLOmega)

References:

- Standard model: *Walters and Cokljat* <%= cite "walters_three-equation_2008" %>
- Corrections to the standard model: *Furst* <%= cite "furst_numerical_2013" %>
