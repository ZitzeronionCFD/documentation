---
title: Decay of homogeneous incompressible isotropic turbulence
short_title: Decay of HIIT
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- example
- turbulence
- verification
- validation
menu_id: verification-validation-turbulent-decay-homogeneous-isotropic-turbulence
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-turbulent
group: verification-validation-turbulent
---

<%= page_toc %>

## Overview {#sec-overview}

- Solver: [pimpleFoam](ref_id://pimplefoam)
- 3D decay of homogeneous incompressible isotropic turbulence (HIIT) based on
  the reference <%= cite "comte-bellot_simple_1971" %>
- Mesh and initial velocity field created using the `createBoxTurb` utility
- Energy spectrum calculated using the `energySpectrum` function object
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/decayIsoTurb" %>

The domain comprises a 3D cube with a side length is
$$ L = 0.09 \times 2 \pi $$ m:

![Case description](turbulent-decay-hit-description-small.png)

Cyclic conditions are applied to all patches.

Measurements were performed at a Reynolds number of 34000, based on a length
scale of 5.08 cm and velocity of 10 m/s.  This suggests a laminar viscosity of

$$
    \nu = \frac{|\u_0| L}{\mathrm{Re}}
        = \frac{10 \times 0.0508}{34000}
        = 1.5 \times 10^{-5} m^2/s
$$

Turbulence quantities were measured at non-dimensional times of:

$$
    t^* = \frac{t |\u_0|}{L} = 42, 98, 171,
$$

equating to dimensional times of 0.213, 0.498 and 0.869 seconds.  By basing the
initial condition on the 0.213s data, the comparisons can be performed at
0.285s and 0.656s.

## Mesh {#sec-mesh}

The block mesh is created using the `createBoxTurb` utility, with the
`-createBlockMesh` option

## Boundary conditions {#sec-boundary-conditions}

Cyclic conditions are applied to all patches

## Initial conditions {#sec-intiial-conditions}

An initial velocity field was created using the `createBoxTurb` utility.
This is based on the procedure described by Saad et. al.
<%= cite "saad_scalable_2017" %>, using the turbulence energy spectra at
$$ t^*= 42 $$.

![Initial condition for the 256x256x256 box](dhiit-256cube-init.png)

## Results {#sec-results}

These are initial results and further details will be available shortly
{: .note}

An animation for a 256x256x256 box showing the velocity field, and turbulence
structures using the Q criterion shows the decay of the complex flow field.

<%= vimeo 307065762, 600, 258 %>

An example of the decay profiles for the WALE LES model shows good agreement.

|![WALE 128x128x128](dhiit-128-sa-iddes-400x400.png)|
{: .noBorder}

The grey region represents the wave numbers above the Nyquist limit for this
mesh.
