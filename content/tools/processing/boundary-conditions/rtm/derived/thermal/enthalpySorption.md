---
title: enthalpySorption
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-enthalpySorption
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `enthalpySorption` is a boundary condition that provides
a temperature boundary condition which works in conjunction with the
`speciesSorption` condition for species.

This boundary condition substracts or adds enthalpy associated with the
adsorption provided by the `speciesSorption` condition.

It can handle two enthalpy models:
- estimate
- calculated

On top of this, the enthalpy associated with the sensible enthalpy
corresponding with the species transport can be added using `includeHs`.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                enthalpySorption;
        enthalpyModel       <word>;
        species             <word>;

        // Conditional mandatory entries

        // when enthalpyModel == calculated
        enthalpyTable       <Function1<scalar>>

            // enthalpyTable
            // {
            //     type            table;
            //     values          ((0 0)(1 50));
            // }

        // Optional entries
        includeHs           <bool>;
        C                   <scalar>;
        Hvap                <scalar>;
        dhdt                <scalarField>;
        p                   <word>;
        T                   <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "enthalpySorptionFvPatchScalarField.H", "enthalpySorption" %>
<% assert :string_exists, "enthalpySorptionFvPatchScalarField.C", "enthalpyModel" %>
<% assert :string_exists, "enthalpySorptionFvPatchScalarField.C", "species" %>
<% assert :string_exists, "enthalpySorptionFvPatchScalarField.C", "enthalpyTable" %>
<% assert :string_exists, "enthalpySorptionFvPatchScalarField.C", "includeHs" %>
<% assert :string_exists, "enthalpySorptionFvPatchScalarField.C", "C" %>
<% assert :string_exists, "enthalpySorptionFvPatchScalarField.C", "Hvap" %>
<% assert :string_exists, "enthalpySorptionFvPatchScalarField.C", "dhdt" %>
<% assert :string_exists, "enthalpySorptionFvPatchScalarField.C", "p" %>
<% assert :string_exists, "enthalpySorptionFvPatchScalarField.C", "T" %>

where:

Property        | Description                                | Type    | Required | Default
----------------|--------------------------------------------|---------|----------|---------
`type`          | Type name: `enthalpySorption`              | word    | yes      | -
`enthalpyModel` | Adsorption enthalpy model                  | word    | yes      | -
`species`       | Name of associated species                 | word    | yes      | -
`enthalpyTable` | Calculated enthalpy model table            | Function1\<scalar\> | no | -
`includeHs`     | Include sensible enthalpy                  | bool    | no       | true
`C`             | Estimate enthalpy model constant           | scalar  | no       | 0
`Hvap`          | Evaporation heat for species               | scalar  | no       | 0
`p`             | Name of operand pressure field             | word    | no       | p
`T`             | Name of operand temperature field          | word    | no       | T
`dhdt`          | Enthalpy change on cells next to patch \[J/kg\] | scalarField | no     | Zero

The inherited entries are elaborated in:

- zeroGradientFvPatchFields.H
- Function1.H
- boundarySourcePatch.H
<!-- end of the list -->

Options for the `enthalpyModel` entry:

Property       | Description
---------------|----------------------------------------------
`estimated`    | Enthalpy is estimated
`calculated`   | Enthalpy is calculated based on enthalpyTable

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/rhoReactingFoam/groundAbsorption" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/reactionThermo/derivedFvPatchFields/enthalpySorption" %>

API:

- [Foam::enthalpySorptionFvPatchScalarField](doxy_id://Foam::enthalpySorptionFvPatchScalarField)

<%= history "2206" %>

<!----------------------------------------------------------------------------->
