---
title: atmTurbulentHeatFluxTemperature
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-atmTurbulentHeatFluxTemperature
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

The `atmTurbulentHeatFluxTemperature` is a boundary condition that provides
provides a fixed heat constraint on temperature
(i.e. `T`) to specify temperature gradient through an input heat source
which can either be specified as absolute power \[W\], or as flux \[W/m^2\].

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            atmTurbulentHeatFluxTemperature;
        heatSource      <word>;
        alphaEff        <word>;
        Cp0             <scalar>;
        q               <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "atmTurbulentHeatFluxTemperatureFvPatchScalarField.H", "atmTurbulentHeatFluxTemperature" %>
<% assert :string_exists, "atmTurbulentHeatFluxTemperatureFvPatchScalarField.C", "heatSource" %>
<% assert :string_exists, "atmTurbulentHeatFluxTemperatureFvPatchScalarField.C", "alphaEff" %>
<% assert :string_exists, "atmTurbulentHeatFluxTemperatureFvPatchScalarField.C", "Cp0" %>
<% assert :string_exists, "atmTurbulentHeatFluxTemperatureFvPatchScalarField.C", "q" %>

where:

Property     | Description                                | Type    | Required | Default
-------------|--------------------------------------------|---------|----------|---------
`type`       | Type name: `atmTurbulentHeatFluxTemperature` | word  | yes      | -
`heatSource` | Heat source type                           | word    |  yes     | -
`alphaEff`   | Name of turbulent thermal diff. field \[kg/m/s\] | word | yes     | -
`Cp0`        | Specific heat capacity \[m2/s2/K\]           | Function1\<scalar\> | yes | -
`q`          | Heat source value \[W (power) or W/m^2 (flux)\] | PatchFunction1\<scalar\> | yes | -

Options for the `heatSource` entry:

Property              | Description
----------------------|--------------------------------------------
`power`               | Absolute power heat source \[W\]
`flux`                | Flux heat source           \[W/m^2\]

The inherited entries are elaborated in:

- fixedGradientFvPatchScalarField.H
- Function1.H
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmForestStability" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/atmTurbulentHeatFluxTemperature" %>

API:

- [Foam::atmTurbulentHeatFluxTemperatureFvPatchScalarField](doxy_id://Foam::atmTurbulentHeatFluxTemperatureFvPatchScalarField)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
