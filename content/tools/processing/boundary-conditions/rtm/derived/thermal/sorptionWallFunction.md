---
title: sorptionWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-sorptionWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `sorptionWallFunction` is a boundary condition that provides
a wall boundary condition to specify scalar/concentration gradient for
turbulent and laminar flows.

The governing equation of the boundary condition is:

$$
    \nabla C = \frac{C^* - C_p}{\Delta_y} = \frac{F}{a \Delta_y}
$$

with:

$$
    C^* = \frac{C_{surf}}{K}
$$

and with the mass-transfer coefficient is calculated for turbulent flows:

$$
    a = \frac{C_\mu^{0.25} k_p^{0.5}}{y^+_{blended}}
$$

or for laminar-flow and molecular-diffusion-only states:

$$
    a = \frac{D_m}{y_1}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\nabla C$$          | Gradient of concentration
$$C^*$$               | Wall-adjacent concentration
$$C_p$$               | Near-wall cell concentration
$$\Delta_y$$          | First-cell centre wall distance
$$F$$                 | Flux of concentration
$$a$$                 | Mass-transfer coefficient
$$C_{surf}$$          | Wall-surface concentration
$$K$$                 | Adsorption or absorption/permeation coefficient
$$C_\mu$$             | Empirical model coefficient
$$k_p$$               | Turbulent kinetic energy in near-wall cell
$$y^+_{blended}$$     | Non-dimensional blended near-wall cell height
$$D_m$$               | Molecular-diffusion coefficient
$$y_1$$               | First-cell centre wall distance

#### References

Standard model for exponential blending (tag:FDC):

    Foat, T., Drodge, J., Charleson, A., Whatmore, B.,
    Pownall, S., Glover, P., ... & Marr, A. (2022).
    Predicting vapour transport from semi-volatile organic
    compounds concealed within permeable packaging.
    International Journal of Heat and Mass Transfer, 183, 122012.
    DOI:10.1016/j.ijheatmasstransfer.2021.122012

Standard model for stepwise blending (tag:F):

    Foat, T. (2021).
    Modelling vapour transport in indoor environments for
    improved detection of explosives using dogs.
    Doctoral dissertation. University of Southampton.
    URI:http://eprints.soton.ac.uk/id/eprint/456709

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            compressible::sorptionWallFunction;
        Sc              <scalar>;
        Sct             <scalar>;
        kAbs            <PatchFunction1<scalar>>;

        // Optional entries
        laminar         <bool>;
        D               <scalar>;
        kName           <word>;
        nuName          <word>;

        // Inherited entries
        Cmu             <scalar>;
        kappa           <scalar>;
        E               <scalar>;
        blending        <word>;
        ...
    }

<% assert :string_exists, "sorptionWallFunctionFvPatchScalarField.H", "sorptionWallFunction" %>
<% assert :string_exists, "sorptionWallFunctionFvPatchScalarField.C", "Sc" %>
<% assert :string_exists, "sorptionWallFunctionFvPatchScalarField.C", "Sct" %>
<% assert :string_exists, "sorptionWallFunctionFvPatchScalarField.C", "kAbs" %>
<% assert :string_exists, "sorptionWallFunctionFvPatchScalarField.C", "laminar" %>
<% assert :string_exists, "sorptionWallFunctionFvPatchScalarField.C", "D" %>
<% assert :string_exists, "sorptionWallFunctionFvPatchScalarField.C", "kName" %>
<% assert :string_exists, "sorptionWallFunctionFvPatchScalarField.C", "nuName" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `compressible::sorptionWallFunction` | word | yes    | -
`Sc`                   | Schmidt number                             | scalar  | yes      | -
`Sct`                  | Turbulent Schmidt number                   | scalar  | yes      | -
`kAbs`                 | Adsorption or absorption/permeation coefficient | PatchFunction1\<scalar\> | yes  | -
`laminar`              | Flag to calculate mass-transfer coefficient under the laminar-flow or -diffusion-only states | bool | no   | false
`kName`                | Name of operand turbulent kinetic energy field | word | no      | k
`nuName`               | Name of operand kinematic viscosity field      | word | no      | nu

The inherited entries are elaborated in:

- fixedGradientFvPatchField.H
- wallFunctionCoefficients.H
- wallFunctionBlenders.H
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/wallFunctions/sorptionWallFunction" %>

API:

- [Foam::sorptionWallFunctionFvPatchScalarField](doxy_id://Foam::sorptionWallFunctionFvPatchScalarField)

<%= history "v2212" %>

<!----------------------------------------------------------------------------->
