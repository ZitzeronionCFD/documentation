---
title: codedMixed
copyright:
- Copyright (C) 2016-2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-codedMixed
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `codedMixed` is a mixed-value boundary condition that
provides an interface to prescribe a user-coded condition and constructs
a new boundary condition on-the-fly which is then used to evaluate.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            <type>;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                  codedMixed;
        name                  <word>;

        // Optional entries
        codeInclude
        #{
            #include "fvCFD.H"
        #};

        codeOptions
        #{
            -I$(LIB_SRC)/finiteVolume/lnInclude \
            -I$(LIB_SRC)/meshTools/lnInclude
        #};

        codeLibs
        #{
            -lfiniteVolume \
            -lmeshTools
        #};

        localCode
        #{
            // OpenFOAM C++ code
        #};

        codeContext             <dict>;

        // Conditional entries

            // if the 'code' keyword is present
            code
            #{
                // OpenFOAM C++ code
            #};

            // if the 'code' keyword is not present,
            // read the code from a dictionary from 'system/codeDict'
            <patchName>
            {
                code
                #{
                    // OpenFOAM C++ code
                #};
            }

        // Inherited entries
        refValue        min(10, 0.1*this->db().time().value());
        refGradient     Zero;
        valueFraction   uniform 1;
    }


where:

Property              | Description                                | Type    | Required | Default
----------------------|--------------------------------------------|---------|----------|---------
`type`                | Type name: `codedMixed`                    | word    | yes      | -
`name`                | Name of the generated condition            | word    | yes      | -
`code`                | OpenFOAM C++ code for patch value assignment | code  | yes      | -
`codeInclude`         | Include files                                  | -   | no       | -
`codeOptions`         | Compiler line: added to EXE_INC (Make/options) | -   | no       | -
`codeLibs`            | Linker line: added to LIB_LIBS (Make/options)  | -   | no       | -
`localCode`           | Local static functions                         | -   | no       | -
`codeContext`         | Additional dictionary context for the code     | dict | no      | -

<% assert :string_exists, "codedMixedFvPatchField.H", "codedMixed" %>
<% assert :string_exists, "codedMixedFvPatchField.C", "code" %>
<% assert :string_exists, "codedMixedFvPatchField.C", "name" %>
<% assert :string_exists, "codedMixedFvPatchField.C", "codeContext" %>

The inherited entries are elaborated in:

- [Foam::mixedFvPatchFields](doxy_id://Foam::mixedFvPatchFields)
- [Foam::codedBase](doxy_id://Foam::codedBase)

<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/codedMixed" %>

API:

- [Foam::codedMixedFvPatchField](doxy_id://Foam::codedMixedFvPatchField)

<%= history "2.1.0" %>


<!----------------------------------------------------------------------------->