---
title: fan
copyright:
- Copyright (C) 2016-2023 OpenCFD Ltd.
tags:
- boundary conditions
- coupled
- jump
menu_id: boundary-conditions-fan
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-coupled
group: bcs-coupled-jump
---

<%= page_toc %>

## Description

The `fan` is a general boundary condition to simulate a jump
condition between a pair of patches, using the `cyclic` condition as a base.
The condition can accept user-defined fan curve of pressure rise vs velocity.

The non-dimensional flux is calculated as follows:

$$
  \phi = \frac{120 U_n}{\pi^3 d_m r_{pm}}
$$

The non-dimensional pressure:

$$
  \Psi = \frac{2 \Delta P}{\rho (\pi \omega d_m)^2}
$$

Property         | Description
-----------------|-------
$$d_m$$          | Fan mean diameter
$$r_{pm}$$       | Fan rpm
$$\Delta P$$     | Pressure drop
$$\omega$$       | Rotational speed \[rad/s\]

The non-dimensional table should be given as $$\Psi = F(\phi)$$.

## Usage {#usage}

The condition requires entries in both the `boundary` and field files.

### Boundary file

    <patchName>
    {
        type            cyclic;
        ...
    }

<% assert :string_exists, "cyclicPolyPatch.H", "cyclic" %>

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fan;
        jumpTable       <Function1<Type>>;

        // Optional entries
        phi             <word>;
        rho             <word>;
        uniformJump     <bool>;
        nonDimensional  <bool>;

        // Conditional entries

          // if 'nonDimensional' is true
          rpm           <Function1<scalar>>;
          dm            <Function1<scalar>>;

        // Inherited entries
        patchType       cyclic;
        ...
    }

<% assert :string_exists, "fanFvPatchField.H", "fan" %>
<% assert :string_exists, "fixedJumpFvPatchField.C", "patchType" %>
<% assert :string_exists, "uniformJumpFvPatchField.C", "jumpTable" %>
<% assert :string_exists, "fanFvPatchField.C", "phi" %>
<% assert :string_exists, "fanFvPatchField.C", "rho" %>
<% assert :string_exists, "fanFvPatchField.C", "uniformJump" %>
<% assert :string_exists, "fanFvPatchField.C", "nonDimensional" %>
<% assert :string_exists, "fanFvPatchField.C", "rpm" %>
<% assert :string_exists, "fanFvPatchField.C", "dm" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `fan`                           | word    | yes      | -
`jumpTable`    | Jump dataset                               | Function1\<Type\>    | yes      | -
`phi`          | Name of the flux transporting the field    | word    | no       | phi
`rho`          | Name of the density field for normalising the mass flux | word  | no       | rho
`uniformJump`  | Flag to apply uniform pressure drop        | bool    | no       | false
`nonDimensional`  | Flag to use non-dimensional curves      | bool    | no       | false
`rpm`          | Fan rpm (for non-dimensional curve)        | Function1\<scalar\>  | conditional | -
`dm`        | Fan mean diameter (for non-dimensional curve) | Function1\<scalar\>  | conditional | -

The inherited entries are elaborated in:

- [Foam::uniformJumpFvPatchField](doxy_id://Foam::uniformJumpFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

- The underlying `patchType` should be set to `cyclic`.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/TJunctionFan" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fan" %>

API:

- [Foam::fanFvPatchField](doxy_id://Foam::fanFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
