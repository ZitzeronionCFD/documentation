---
title: Pure Mixture
short_title: Pure
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- mixture
- single component
- single phase
- thermophysical
menu_id: mixture-pureMixture
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-mixture
group: mixture-models
---

<%= page_toc %>

The `pureMixture` model describes a single phase mixture that can be
approximated by a single component, e.g. an individual specie, approximation
for air etc.

## Usage

The `pureMixture` is set in the top-level `thermoType` dictionary:

~~~
thermoType
{
    mixture         pureMixture;
    <...>
}
~~~

All properties must be defined in a `mixture` sub-dictionary taking the form:

~~~
mixture
{
    specie
    {
        <...>
    }
    equationOfState
    {
        <...>
    }
    thermodynamics
    {
        <...>
    }
    transport
    {
        <...>
    }
}
~~~

where `<...>` entries depend on the top-level `thermoType` options
[detailed earlier](ref_id://models-thermophysical).

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/basic/mixtures/pureMixture" %>
singleStepReactingMixture;
inhomogeneousMixture;
veryInhomogeneousMixture

openfoam/src/thermophysicalModels/reactionThermo/mixtures
egrMixture
inhomogeneousMixture
singleComponentMixture
singleStepReactingMixture
veryInhomogeneousMixture