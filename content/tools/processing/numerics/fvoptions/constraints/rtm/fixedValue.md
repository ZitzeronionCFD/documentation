---
title: Fixed value
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- constraint
menu_id: fvoption-fixedvalue
menu_parent: numerics-fvoptions-constraints
license: CC-BY-NC-ND-4.0
group: fvoptions-constraints-general
---

<%= page_toc %>

Enables users to set field values in the domain, irrespective of the local flow
conditions.

## Properties {#properties}

Downstream fields outside the region constrained will continue developing normally.

No information will be added to the log file other than the identification of the constraint at the start of the simulation.
{: .note}

## Usage {#usage}

The constraint is available for all primitive field types, i.e. scalar, vector,
sphericalTensor, symTensor and tensor, where the `type` entry is set as

    type            <type>FixedValueConstraint

For example, the scalar variant can be applied to the turbulence `k` and
`epsilon` fields using:

    fixedValue
    {
        type            scalarFixedValueConstraint;
        active          yes;

        selectionMode   cellZone;    //cellZone or all
        cellZone        porosity;
        fieldValues
        {
            k           1;
            epsilon     150;     // any field values can be modified, including scalars.
        }
    }

<% assert :string_exists, "FixedValueConstraint.H", "FixedValueConstraint" %>
<% assert :string_exists, "FixedValueConstraint.C", "fieldValues" %>
<% assert :string_exists, "cellSetOption.C", "cellZone" %>

## Further information {#further-information}

Tutorials

- <%= repo_link2(
  "$FOAM_TUTORIALS/compressible/rhoSimpleFoam/angledDuctExplicitFixedCoeff") %>

Source code

- <%= repo_link2(
  "$FOAM_SRC/fvOptions/constraints/general/fixedValueConstraint") %>
