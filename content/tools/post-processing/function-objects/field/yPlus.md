---
title: yPlus
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- y+
menu_id: function-objects-field-yplus
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#sec-fos-field-yPlus-description}

The `yPlus` function object computes the near wall $$y_1^+$$ for turbulence
models.

### Operands

Operand       | Type             | Location
:---          |:---              |:---
input         | -                | -
output file   | dat             | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
output field  | `volScalarField` | `$FOAM_CASE/<time>/<outField>`

## Usage {#sec-fos-field-yPlus-usage}

Example of the `yPlus` function object by using `functions` sub-dictionary in
`system/controlDict` file:

    yPlus1
    {
        // Mandatory entries (unmodifiable)
        type            yPlus;
        libs            (fieldFunctionObjects);

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "yPlus.H", "yPlus" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "yPlus",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: yPlus                    | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Example by using the `postProcess` utility:

    <solver> -postProcess -func yPlus

## Further information {#sec-fos-field-yPlus-further-information}

Tutorial:

- [pimpleFoam](ref_id://pimplefoam) `cylinder2D` tutorial:
  <%= repo_link2(
  "$FOAM_TUTORIALS/incompressible/pimpleFoam/laminar/cylinder2D") %>

\ofAssert functionObjectUseExists incompressible/pimpleFoam/LES/channel395DFSEM yPlus

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/yPlus" %>

API:

- [Foam::functionObjects::yPlus](doxy_id://Foam::functionObjects::yPlus)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
