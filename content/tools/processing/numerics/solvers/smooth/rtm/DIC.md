---
title: DIC Smoother
short_title: DIC
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- solvers
- DILU
menu_id: smooth-dic
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-smooth
group: solvers-smooth-smoothers
---

<%= page_toc %>

## Properties {#properties}

- Simplified <B>D</B>iagonal-based <B>I</B>ncomplete <B>C</B>holesky smoother
- Symmetric matrices

## Usage {#usage}

    smoother        DIC;

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/smoothers/DIC" %>

API:

- [Foam::DICSmoother](doxy_id://Foam::DICSmoother)
