---
title: mapFields
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- map
- post-processing
menu_id: function-objects-field-mapfields
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `mapFields` function object
maps input fields from local mesh to secondary mesh at runtime.

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `vol<Type>Field`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `vol<Type>Field`     | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `mapFields` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    mapFields1
    {
        // Mandatory entries (unmodifiable)
        type            mapFields;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entries (runtime modifiable)
        fields          (<field1> <field2> ... <fieldN>);
        mapRegion       coarseMesh;
        mapMethod       cellVolumeWeight;
        consistent      true;

        // Optional entries (runtime modifiable)
        // patchMapMethod  direct;  // AMI-related entry
        // enabled if consistent=false
        // patchMap        (<patchSrc> <patchTgt>);
        // cuttingPatches  (<patchTgt1> <patchTgt2> ... <patchTgtN>);

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "mapFields/mapFields.H", "mapFields" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "mapFields",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "mapFields/mapFields.H", "fields" %>
<% assert :string_exists, "mapFields/mapFields.H", "mapRegion" %>
<% assert :string_exists, "mapFields/mapFields.H", "mapMethod" %>
<% assert :string_exists, "mapFields/mapFields.H", "consistent" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: mapFields                | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 fields       | Names of operand fields             | wordList |  yes | -
 mapRegion    | Name of region to map to            | word |  yes     | -
 mapMethod    | Mapping method                      | word |  yes     | -
 consistent   | Mapping meshes have consistent boundaries | bool | yes | -
 patchMapMethod | Patch mapping method for AMI cases | word |  no     | -
 patchMap   | Coincident source/target patches in two cases | wordHashTable | no | -
 cuttingPatches | Target patches cutting the source domain | wordList | no | -

Options for the `mapMethod` entry:

    direct
    mapNearest
    cellVolumeWeight
    correctedCellVolumeWeight

Options for the `patchMapMethod` entry:

    directAMI
    mapNearestAMI
    faceAreaWeightAMI
    partialFaceAreaWeightAMI

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/icoFoam/cavity/cavityClipped" %>
- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/DPMFoam/Goldschmidt" %>
- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/cavitatingFoam/LES/throttle3D" %>
- <%= repo_link2 "$FOAM_TUTORIALS/preProcessing/PDRsetFields/simplePipeCage" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/mapFields" %>

API:

- [Foam::functionObjects::mapFields](doxy_id://Foam::functionObjects::mapFields)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->