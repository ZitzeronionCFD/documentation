---
title: valueAverage
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- average
- field
- function object
- post-processing
menu_id: function-objects-field-valueaverage
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `valueAverage` function object
computes the ensemble- or time-based singular-value average values,
with optional windowing, from the output of function objects
that generate non-field type values, e.g. `Cd` of `forceCoeffs` or
`momentum_x` in `momentum` function objects.

### Operands

Operand       | Type  | Location
:---          |:---   |:---
input         | -     | -
output file   | dat   | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
output field  | -     | -

## Usage {#usage}

Example of the `valueAverage` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    valueAverage1
    {
        // Mandatory entries (unmodifiable)
        type              valueAverage;
        libs              (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        functionObject    <FO>;                       // forceCoeffs1;
        fields            (<field1> ... <fieldN>);    // (Cm Cd Cl);

        // Optional entries (runtime modifiable)
        resetOnRestart    false;
        window            0.5;

        // Optional (inherited) entries
        writePrecision    8;
        writeToFile       true;
        useUserTime       true;
        region            region0;
        enabled           true;
        log               true;
        timeStart         0;
        timeEnd           1000;
        executeControl    timeStep;
        executeInterval   1;
        writeControl      timeStep;
        writeInterval     1;
    }

<% assert :string_exists, "valueAverage.H", "valueAverage" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "valueAverage",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "valueAverage.C", "window" %>
<% assert :string_exists, "valueAverage.C", "functionObject" %>
<% assert :string_exists, "valueAverage.C", "fields" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: valueAverage             | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 functionObject | Name of function object to retrieve data | word | yes | -
 fields       | Names of operand fields               | wordList | yes | -
 resetOnRestart | Reset the averaging on restart      | bool | no | false
 window       | Averaging window                    | scalar | no | VGREAT

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Usage by the `postProcess` utility is not available.

## Sample output {#sample-output}

Text reporting:

- Average value, e.g. using drag coefficient, `Cd`, retrieved from
  a [forceCoeffs](ref_id://function-objects-forces-forcecoeffs) function object:

      CdMean: 0.3

<% assert :string_exists, "forceCoeffs.C", "Cd" %>

## Stored properties {#stored-properties}

### Reduced data

The following data are stored by the function object, and can be used by other
function objects:

- `<field-name>Mean` : average value

## Further information {#sec-os-field-valueAverage-further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/valueAverage" %>

API:

- [Foam::functionObjects::valueAverage](doxy_id://Foam::functionObjects::valueAverage)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
