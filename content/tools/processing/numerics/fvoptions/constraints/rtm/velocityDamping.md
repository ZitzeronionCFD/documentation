---
title: Velocity damping
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- constraint
menu_id: fvoption-velocitydamping
menu_parent: numerics-fvoptions-constraints
license: CC-BY-NC-ND-4.0
group: fvoptions-constraints-momentum
---

<%= page_toc %>

This constraint is primarily used to dampen velocity fluctuations in the
start-up phase of simulations.  When the local velocity exceeds the
user-supplied maximum value a sink term is activated in the affected region
to lower the velocity to the limiting value.

## Properties {#properties}

Additional reporting is provided for the number of cells being damped, and as a
percentage of cells over the complete mesh.

When active, this constraint manipulates the system of equations.  Users
should ensure that it is not active when the case is converged (steady-state)
or during the period of interest (transient) to ensure that its presence does
not pollute the results.
{: .note}

## Usage {#usage}

    velocityDamping
    {
        type            velocityDampingConstraint;
        selectionMode   all;
        UMax            200;
    }

<% assert :string_exists, "velocityDampingConstraint.H", "velocityDampingConstraint" %>
<% assert :string_exists, "velocityDampingConstraint.C", "UMax" %>
<% assert :string_exists, "cellSetOption.C", "all" %>

## Further information {#further-information}

Tutorials

- *none*

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/constraints/derived/velocityDampingConstraint" %>
