---
title: Van Leer divergence scheme
short_title: Van Leer
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-vanleer
license: CC-BY-NC-ND-4.0
group: schemes-divergence-nvdtvd
---

<%= page_toc %>

## Properties {#properties}

- Second order
- Unbounded
- By *Van Leer* <%= cite "van_leer_towards_1974" %>

## Normalised Variable Diagram {#nvd}

![vanLeer](../vanLeer.png)

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss vanLeer;
    }

<% assert :string_exists, "vanLeer.H", "vanLeer" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/limitedSchemes/vanLeer" %>

API:

- [Foam::vanLeerLimiter](doxy_id://Foam::vanLeerLimiter)
