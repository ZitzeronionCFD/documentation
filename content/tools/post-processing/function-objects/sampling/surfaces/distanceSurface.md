---
title: distanceSurface
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- post-processing
- sampling
- surface
menu_id: surfaces-distanceSurface
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-sampling-surfaces
group: sampling-surfaces
---

<%= page_toc %>

Creates a cutting plane through a mesh.

## Usage {#usage}

Basic operation of the `distanceSurface` surface comprises:

    surfaces1
    {
        type            surfaces;
        libs            (sampling);

        surfaces
        {
            mySurface1
            {
                type            distanceSurface;
                surfaceType     triSurfaceMesh;

                // Optional
                surfaceName     mySurface.obj;
                distance        0;
                signed          true;
                topology        <topology-type>;
                absProximity    1e-5;
                maxDistance     1000;
            }
        }
    }

<% assert :string_exists, "sampledSurfaces.H", "surfaces" %>
<% assert :class_library_exists, "sampling/Make/files", "sampledSurfaces", "libsampling" %>
<% assert :string_exists, "distanceSurface.H", "distanceSurface" %>

The `topology-type` value can be::

- `none` : no filtering
<% assert :string_exists, "distanceSurface.C", "none" %>

Pre-filter options:

- `largestRegion`: pre-filter - retains the region with the most cut cells
<% assert :string_exists, "distanceSurface.C", "largestRegion" %>
- `nearestPoints` : pre-filter - retains the regions where cut cells are closest to the set user-defined points. Requires additional
  input:

~~~
topology        nearestPoints;
nearestPoints
(
    (0 0 0)
    (1 0 0)
    (1 1 0)
);
~~~
<% assert :string_exists, "distanceSurface.C", "nearestPoints" %>

Post-filter options:

- `proximityRegions`: post filter - uses the area-weighted distance of each
  topologically connected region to the input surface to reject regions at a distance greater than `absProximity`
<% assert :string_exists, "distanceSurface.C", "proximityRegions" %>
- `proximityFaces`: post-filter - rejects resulting faces with a distance greater than `absProximity` from the input surface
<% assert :string_exists, "distanceSurface.C", "proximityFaces" %>
- `proximity`: post-filter - same as `proximityFaces`
<% assert :string_exists, "distanceSurface.C", "proximity" %>

## Sample output {#sample-output}

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/sampling/surface/distanceSurface" %>

API:

- [Foam::distanceSurface](doxy_id://Foam::distanceSurface)

Example usage:

- <%= repo_link2("$FOAM_TUTORIALS/compressible/rhoSimpleFoam/gasMixing/injectorPipe") %>
- <%= repo_link2("$FOAM_TUTORIALS/compressible/rhoSimpleFoam/squareBend") %>
