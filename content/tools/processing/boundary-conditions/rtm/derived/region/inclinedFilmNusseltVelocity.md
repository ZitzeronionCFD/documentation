---
title: inclinedFilmNusseltInletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-inclinedFilmNusseltInletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-region
group: bcs-region
---

<%= page_toc %>

## Description

The `inclinedFilmNusseltInletVelocity` is boundary condition that provides
a film velocity boundary condition for inclined films that imposes a
sinusoidal perturbation on top of a mean flow rate, where the velocity is
calculated using the Nusselt solution.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            inclinedFilmNusseltInletVelocity;
        GammaMean       <Function1<scalar>>;
        a               <Function1<scalar>>;
        omega           <Function1<scalar>>;

        // Optional entries
        filmRegion      <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "inclinedFilmNusseltInletVelocityFvPatchVectorField.H", "inclinedFilmNusseltInletVelocity" %>
<% assert :string_exists, "inclinedFilmNusseltInletVelocityFvPatchVectorField.C", "GammaMean" %>
<% assert :string_exists, "inclinedFilmNusseltInletVelocityFvPatchVectorField.C", "a" %>
<% assert :string_exists, "inclinedFilmNusseltInletVelocityFvPatchVectorField.C", "omega" %>
<% assert :string_exists, "inclinedFilmNusseltInletVelocityFvPatchVectorField.C", "filmRegion" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `inclinedFilmNusseltInletVelocity`   | word    | yes      | -
`GammaMean`    | Mean mass flow rate per unit length \[kg/s/m\] | Function1\<scalar\> | yes | -
`a`            | Perturbation amplitude \[m\]               | Function1\<scalar\>   | yes | -
`omega`        | Perturbation frequency \[rad/s/m\]         | Function1\<scalar\>   | yes | -
`filmRegion`   | Name of film region                        | word    | no       | surfaceFilmProperties

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- Function1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/regionModels/surfaceFilmModels/derivedFvPatchFields/inclinedFilmNusseltInletVelocity" %>

API:

- [Foam::inclinedFilmNusseltInletVelocityFvPatchVectorField](doxy_id://Foam::inclinedFilmNusseltInletVelocityFvPatchVectorField)

<%= history "2.2.0" %>

<!----------------------------------------------------------------------------->
