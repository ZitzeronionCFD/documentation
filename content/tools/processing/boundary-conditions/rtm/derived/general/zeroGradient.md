---
title: Zero gradient
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-zeroGradient
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Properties {#properties}

- Wrapper around the
  [fixed gradient](ref_id://boundary-conditions-fixedGradient) condition
- Sets the field to the internal field value
- Applicable to all variable types

$$
    \frac{\partial}{\partial n} \phi = 0
$$

## Usage {#usage}

The condition is specified in the field file using:

    <patchName>
    {
        type            zeroGradient;
    }

<% assert :string_exists, "zeroGradientFvPatchField.H", "zeroGradient" %>

## Further information {#further-information}

Source code:

- [Foam::fixedGradientFvPatchField](doxy_id://Foam::fixedGradientFvPatchField)
- [Foam::zeroGradientFvPatchField](doxy_id://Foam::zeroGradientFvPatchField)
