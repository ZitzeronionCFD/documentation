---
title: searchableCone
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: searchablecone
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh-geometry
group: searchableobjects
---

<%= page_toc %>

![hollow cone example](../images/hollowCone-small.png)

A cone, optionally hollow

## Usage

    cone
    {
        type            searchableCone;
        point1          (0 0 0);
        radius1         1.5;
        innerRadius1    0.25;
        point2          (10 0 0);
        radius2         3.0;
        innerRadius2    1.0;
    }

<% assert :string_exists, "searchableCone.H", "searchableCone" %>
<% assert :string_exists, "searchableCone.C", "point1" %>
<% assert :string_exists, "searchableCone.C", "radius1" %>
<% assert :string_exists, "searchableCone.C", "innerRadius1" %>
<% assert :string_exists, "searchableCone.C", "point2" %>
<% assert :string_exists, "searchableCone.C", "radius2" %>
<% assert :string_exists, "searchableCone.C", "innerRadius2" %>

Where:

Property       | Description               | Required
---------------|---------------------------|----------
`point1`       | Axis point at the start   |      yes
`radius1`      | Outer radius at the start |      yes
`innerRadius1` | Inner radius at the start |      yes
`point2`       | Axis point at the end     |      yes
`radius2`      | Outer radius at the end   |      yes
`innerRadius2` | Inner radius at the end   |      yes
