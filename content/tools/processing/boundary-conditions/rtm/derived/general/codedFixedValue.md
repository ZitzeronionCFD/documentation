---
title: codedFixedValue
copyright:
- Copyright (C) 2016-2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-codedFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `codedFixedValue` is a fixed-value boundary condition that
provides an interface to prescribe a user-coded condition and constructs
a new boundary condition on-the-fly (derived from
[fixedValue condition](ref_id://boundary-conditions-fixedValue))
which is then used to evaluate.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            <type>;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                  codedFixedValue;
        name                  <word>;

        // Optional entries
        codeInclude
        #{
            #include "fvCFD.H"
        #};

        codeOptions
        #{
            -I$(LIB_SRC)/finiteVolume/lnInclude \
            -I$(LIB_SRC)/meshTools/lnInclude
        #};

        codeLibs
        #{
            -lfiniteVolume \
            -lmeshTools
        #};

        localCode
        #{
            // OpenFOAM C++ code
        #};

        codeContext             <dict>;

        // Conditional entries

            // if the 'code' keyword is present
            code
            #{
                // OpenFOAM C++ code
            #};

            // if the 'code' keyword is not present,
            // read the code from a dictionary from 'system/codeDict'
            <patchName>
            {
                code
                #{
                    // OpenFOAM C++ code
                #};
            }

        // Inherited entries
        ...
    }


where:

Property              | Description                                | Type    | Required | Default
----------------------|--------------------------------------------|---------|----------|---------
`type`                | Type name: `codedFixedValue`               | word    | yes      | -
`name`                | Name of the generated condition            | word    | yes      | -
`code`                | OpenFOAM C++ code for patch value assignment | code  | yes      | -
`codeInclude`         | Include files                                  | -   | no       | -
`codeOptions`         | Compiler line: added to EXE_INC (Make/options) | -   | no       | -
`codeLibs`            | Linker line: added to LIB_LIBS (Make/options)  | -   | no       | -
`localCode`           | Local static functions                         | -   | no       | -
`codeContext`         | Additional dictionary context for the code     | dict | no      | -

<% assert :string_exists, "codedFixedValueFvPatchField.H", "codedFixedValue" %>
<% assert :string_exists, "codedFixedValueFvPatchField.C", "code" %>
<% assert :string_exists, "codedFixedValueFvPatchField.C", "name" %>
<% assert :string_exists, "codedFixedValueFvPatchField.C", "codeContext" %>

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::codedBase](doxy_id://Foam::codedBase)

<!-- end of the list -->


A simple example showing how to ramp the patch values as a function of time

$$
    p = \min (10, 0.1 t)
$$

is specified in the field file using:

    <patchName>
    {
        type            codedFixedValue;
        value           uniform 0;

        name            rampedFixedValue;

        code
        #{
            const scalar t = this->db().time().value();
            operator==(min(10, 0.1*t));
        #};
    }

<% assert :string_exists, "codedFixedValueFvPatchField.H", "codedFixedValue" %>
<% assert :string_exists, "codedFixedValueFvPatchField.C", "code" %>
<% assert :string_exists, "codedFixedValueFvPatchField.C", "redirectType" %>

Only basic functionality is offered by default, e.g. access to the mesh
(`Foam::fvMesh`) and time (`Foam::Time`) databases.  For more complex conditions
access to additional classes is enabled via the optional `codeInclude` and
`codeOptions` entries, e.g.

    codeInclude
    #{
        #include "fvCFD.H"
    #};

    codeOptions
    #{
        -I$(LIB_SRC)/finiteVolume/lnInclude
    #};

<% assert :string_exists, "dynamicCodeContext.C", "codeInclude" %>
<% assert :string_exists, "dynamicCodeContext.C", "codeOptions" %>

This is equivalent to the required entries in the `Make/options` file when
building code libraries.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/pipeCyclic" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/codedFixedValue" %>

API:

- [Foam::codedFixedValueFvPatchField](doxy_id://Foam::codedFixedValueFvPatchField)

<%= history "2.0.0" %>


<!----------------------------------------------------------------------------->