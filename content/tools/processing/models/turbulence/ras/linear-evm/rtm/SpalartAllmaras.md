---
title: Spalart-Allmaras
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-spalartallmaras
license: CC-BY-NC-ND-4.0
menu_parent: ras-linearevm
group: turbulence-ras-evm
---

<%= page_toc %>

## Properties {#properties}

- One equation model based on a modified turbulence viscosity,
  $$ \tilde{\nu} $$

## Model equations {#model-equations}

$$
    \Ddt{\rho \tilde{\nu}}
  =
    \div \left( \rho D_\tilde{\nu} \tilde{\nu} \right)
  + \frac{C_{b2}}{\sigma_{\nu_t}} \rho \mag{\grad \tilde{\nu}}^2
  + C_{b1} \rho \tilde{S} \tilde{\nu} \left(1 - f_{t2}\right)
  - \left(C_{w1} f_w - \frac{C_{b1}}{\kappa^2} f_{t2}\right)
    \rho \frac{\tilde{\nu}^2}{\tilde{d}^2}
  + S_\tilde{\nu}
$$

The $$ f_{t2} $$ term is not implemented.
{: .note}

The turbulence viscosity is obtained using:

$$
    \nu_t =  \tilde{\nu} f_{v1}
$$

where the function $$ f_{v1} $$ is given by

$$
    f_{v1} = \frac{\chi^3}{\chi^3 + C_{v1}^3}
$$

and

$$
    \chi = \frac{\tilde{\nu}}{\nu}
$$

## Default model coefficients {#default-coeffs}

| $$ \sigma_{\nu_t} $$ | $$ C_{b1} $$ | $$ C_{b2} $$ | $$ C_{w1} $$ | $$ C_{w2} $$ | $$ C_{w3} $$ |
|--------------------|----------------|----------------|----------------|----------------|--------------------|
|  2/3 | 0.1355 | 0.622 | $$\frac{C_{b1}}{\kappa^2} + \frac{1 + C_{b2}}{\sigma_{\nu_t}} $$  |  0.3 |    2   |

| $$ C_{v1} $$ | $$ C_{s} $$ |
|----------------|---------------|
|            7.1 |           0.3 |

## Usage {#usage}

The model is specified using:

    RAS
    {
        turbulence      on;
        RASModel        SpalartAllmaras;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "turbulenceModels/RAS/SpalartAllmaras/SpalartAllmaras.H", "SpalartAllmaras" %>

## Further information {#further-information}

Source code:

- [Foam::RASModels::SpalartAllmaras](doxy_id://Foam::RASModels::SpalartAllmaras)

References:

- Standard model: *Spalart* <%= cite "spalart_one-equation_1994" %>

Related:

- <%= link_to_menuid "turbulence-des" %> variants, e.g.
- <%= link_to_menuid "des-spalartallmarasdes" %>
