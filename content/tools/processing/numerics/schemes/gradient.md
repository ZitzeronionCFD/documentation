---
title: Gradient schemes
short_title: Gradient
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- gradient
menu_id: schemes-gradient
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes
---

<%= page_toc %>

The gradient of a scalar property $$\phi$$ is represented using the notation:

$$
    \grad \phi
  =
    \vec{e}_1 \frac{\partial}{\partial x_1} \phi
  + \vec{e}_2 \frac{\partial}{\partial x_2} \phi
  + \vec{e}_3 \frac{\partial}{\partial x_3} \phi
$$

where the $$ \vec{e} $$ vectors represent the unit vectors of the 3-D space.

## Usage {#usage}

Gradient schemes are specified in the
<%= link_to_menuid "fvschemes" %> file under the `gradSchemes`
sub-dictionary using the syntax:

~~~
gradSchemes
{
    default         none;
    grad(p)         <optional limiter> <gradient scheme> <interpolation scheme>;
}
~~~

## Options {#options}

### Gradient schemes {#options-schemes}

<%= insert_models "schemes-gradient" %>

### Interpolation schemes {#options-interpolation-schemes}

- `linear`: cell-based linear
- `pointLinear`: point-based linear
- `leastSquares`: Least squares

### Gradient limiters {#options-gradient-limiters}

The limited gradient schemes attempt to preserve the monotonicity condition
by limiting the gradient to ensure that the extrapolated face value is
bounded by the neighbouring cell values.

<%= insert_models "schemes-gradient-limiter" %>

- `clippedLinear`: limits linear scheme according to a hypothetical cell size
  ratio

## Example {#example}

See [this example](ref_id://schemes-gradientexample) to see the
relative performance of the schemes.

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/finiteVolume/gradSchemes/gradScheme" %>

API:

- [Foam::fv::gradScheme](doxy_id://Foam::fv::gradScheme)
