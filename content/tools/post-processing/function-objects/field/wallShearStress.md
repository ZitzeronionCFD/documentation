---
title: wallShearStress
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- shear stress
- wall
menu_id: function-objects-field-wallshearstress
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `wallShearStress` function object computes the wall-shear stress at selected
wall patches.

$$
    \vec \tau = \vec R \cdot \vec n
$$

where:

$$\vec \tau$$
: Wall shear stress vector \[m2/s2\]

$$\vec R$$
: Shear-stress symmetric tensor (retrieved from turbulence model)

$$\vec n$$
: Patch normal vector (into the domain)

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | -                    | -
output file  | dat          | `$FOAM_CASE/postProcessing/<FO>/<time>/<field>`
output field | `volVectorField` (only boundaryField) | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `wallShearStress` function object by using `functions`
sub-dictionary in `system/controlDict` file:

    wallShearStress1
    {
        // Mandatory entries (unmodifiable)
        type            wallShearStress;
        libs            (fieldFunctionObjects);

        // Optional entries (runtime modifiable)
        patches         (<patch1> ... <patchN>); // (wall1 "(wall2|wall3)");

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
   }

<% assert :string_exists, "wallShearStress.H", "wallShearStress" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "wallShearStress",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "wallShearStress.C", "patches" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: wallShearStress          | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 patches      | Names of operand patches            | wordList | no   | all wall patches

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Example by using the `postProcess` utility:

    <solver> -postProcess -func wallShearStress

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/wallShearStress" %>

API:

- [Foam::functionObjects::wallShearStress](doxy_id://Foam::functionObjects::wallShearStress)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
