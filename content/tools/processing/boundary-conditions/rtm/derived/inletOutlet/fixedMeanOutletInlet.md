---
title: fixedMeanOutletInlet
copyright:
- Copyright (C) 2017-2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-fixedMeanOutletInlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `fixedMeanOutletInlet` is a general boundary condition that extrapolates
field to the patch using the near-cell values and adjusts the distribution to
match the specified, optionally time-varying, mean value. This extrapolated
field is applied as a `fixedValue` for outflow faces but `zeroGradient` is
applied to inflow faces.

This boundary condition can be applied to pressure when `inletOutlet` is
applied to the velocity so that a `zeroGradient` condition is applied to the
pressure at inflow faces where the velocity is specified to avoid an
unphysical over-specification of the set of boundary conditions.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedMeanOutletInlet;
        meanValue       <Function1<Type>>;

        // Optional entries
        phi             <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "fixedMeanOutletInletFvPatchField.H", "fixedMeanOutletInlet" %>
<% assert :string_exists, "fixedMeanOutletInletFvPatchField.C", "meanValue" %>
<% assert :string_exists, "fixedMeanOutletInletFvPatchField.C", "phi" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `fixedMeanOutletInlet`          | word    | yes      | -
`meanValue` | Mean value that the field is adjusted to maintain | Function1\<Type\> | yes  | -
`phi`          | Name of flux field                         | word    | no       | phi

The inherited entries are elaborated in:

- [Foam::outletInletFvPatchField](doxy_id://Foam::outletInletFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fixedMeanOutletInlet" %>

API:

- [Foam::fixedMeanOutletInletFvPatchField](doxy_id://Foam::fixedMeanOutletInletFvPatchField)

<%= history "v1806" %>

<!----------------------------------------------------------------------------->
