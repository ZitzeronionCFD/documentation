---
title: MarshakRadiationFixedTemperature
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-MarshakRadiationFixedTemperature
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `MarshakRadiationFixedTemperature` is a mixed boundary condition that
provides a Marshak condition for the incident radiation field (usually written
as `G`).

The radiation temperature field across the patch is supplied by the user
using the `Trad` entry.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            MarshakRadiationFixedTemperature;
        Trad            <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "MarshakRadiationFixedTemperatureFvPatchScalarField.H", "MarshakRadiationFixedTemperature" %>
<% assert :string_exists, "MarshakRadiationFixedTemperatureFvPatchScalarField.C", "Trad" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `MarshakRadiationFixedTemperature`        | word     | yes   | -
`Trad`                 | Radiation temperature field                | scalarField | yes  | -

The inherited entries are elaborated in:

- mixedFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/radiation/derivedFvPatchFields/MarshakRadiationFixedTemperature" %>

API:

- [Foam::radiation::MarshakRadiationFixedTemperatureFvPatchScalarField](doxy_id://Foam::radiation::MarshakRadiationFixedTemperatureFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
