---
title: Empty
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
- constraint
menu_id: boundary-conditions-empty
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-constraint
group: boundary-conditions-constraint
---

<%= page_toc %>

## Properties {#properties}

Applied pairs in reduced dimension cases to represent the directions that are
not solved, i.e.:

- 1-D: sides of the 1-D stack of cells
- 2-D: front and back planes

## Usage {#usage}

The condition requires entries in both the `boundary` and field files. Mesh
`boundary` file:

    <patchName>
    {
        type            empty;
        ...
    }

Field file:

    <patchName>
    {
        type            empty;
    }

## Details {#details}

- Separation between `empty` patch pairs must be
  - a single layer of cells
  - cells should be uniform thickness in the non-solved direction, and
  - points should appear collocated when viewed in the patch normal direction
- Particles will be snapped to the mid-plane between pairs of `empty` patches

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/constraint/empty" %>

API:

- [Foam::emptyFvPatchField](doxy_id://Foam::emptyFvPatchField)
