---
title: waveSurfacePressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-waveSurfacePressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `waveSurfacePressure` is a pressure boundary condition whose value
is calculated as the hydrostatic pressure based on a given displacement:

$$
  p = -\rho \, g \, \zeta
$$

Property  | Description
----------|--------------------------------------
$$\rho$$  | Density \[kg/m^3\]
$$g$$     | Acceleration due to gravity \[m/s^2\]
$$\zeta$$ | Wave amplitude \[m\]

The wave amplitude is updated as part of the calculation, derived from the
local volumetric flux.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            waveSurfacePressure;

        // Optional entries
        phi             <word>;
        zeta            <word>;
        rho             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "waveSurfacePressureFvPatchScalarField.H", "waveSurfacePressure" %>
<% assert :string_exists, "waveSurfacePressureFvPatchScalarField.C", "phi" %>
<% assert :string_exists, "waveSurfacePressureFvPatchScalarField.C", "zeta" %>
<% assert :string_exists, "waveSurfacePressureFvPatchScalarField.C", "rho" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `waveSurfacePressure`    | word     | yes      | -
`phi`              | Name of flux field        | word    | no       | phi
`zeta`             | Name of wave-amplitude field | word    | no       | zeta
`rho`              | Name of density field     | word    | no       | rho

The inherited entries are elaborated in:

- mixedFvPPatchScalarFields.H
<!-- end of the list -->

- The density field is only required if the flux is mass-based as opposed to
  volumetric-based.
  {: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/potentialFreeSurfaceDyMFoam/oscillatingBox" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/waveSurfacePressure" %>

API:

- [Foam::waveSurfacePressureFvPatchScalarField](doxy_id://Foam::waveSurfacePressureFvPatchScalarField)

<%= history "2.1.0" %>

<!----------------------------------------------------------------------------->
