---
title: flowType
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- flow
- function object
- post-processing
menu_id: function-objects-field-flowtype
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `flowType` function object computes
the flow type indicator of an input velocity field.

The flow type indicator is obtained according to the following equation:

$$
    \lambda = \frac{\mag{\tensor D} - \mag{\tensor \Omega}}{ \mag{\tensor D} + \mag{\tensor \Omega}}
$$

where

$$\lambda$$
: Flow type indicator

$$\tensor D$$
: Symmetric part of the gradient tensor of velocity

$$\tensor \Omega$$
: Skew-symmetric part of the gradient tensor of velocity

The flow type indicator values mean:

-1
: rotational flow

0
: simple shear flow

1
: planar extensional flow

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `volVectorField`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `volScalarField`     | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `flowType` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    flowType1
    {
        // Mandatory entries (unmodifiable)
        type            flowType;
        libs            (fieldFunctionObjects);

        // Optional (inherited) entries
        field           <field>;
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "flowType/flowType.H", "flowType" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "flowType",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: flowType                 | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func flowType

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/flowType" %>

API:

- [Foam::functionObjects::flowType](doxy_id://Foam::functionObjects::flowType)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
