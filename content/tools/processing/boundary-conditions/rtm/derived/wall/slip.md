---
title: slip
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-slip
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `slip` is boundary condition that provides the slip constraint.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            slip;

        // Inherited entries
        ...
    }

<% assert :string_exists, "slipFvPatchField.H", "slip" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `slip`                          | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::basicSymmetryFvPatchField](doxy_id://Foam::basicSymmetryFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/laminar/sloshing2D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/slip" %>

API:

- [Foam::slipFvPatchField](doxy_id://Foam::slipFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
