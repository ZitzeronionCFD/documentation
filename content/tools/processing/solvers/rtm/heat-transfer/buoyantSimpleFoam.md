---
title: buoyantSimpleFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- heat transfer
- solvers
menu_id: buoyantSimpleFoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-heat-transfer
group: solvers-heat-transfer
solver_models:
- compressible
- turbulence
- fvOption
- heat-transfer
- buoyancy
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-heat-transfer" %>
- steady state
- compressible
- <%= link_to_menuid "models-turbulence" %>
- <%= link_to_menuid "models-thermophysical" %>
- <%= link_to_menuid "fvoptions" %>

<% assert :file_exists, "buoyantSimpleFoam.C" %>
<% assert :string_exists, "buoyantSimpleFoam.C", "simpleControl.H" %>
<% assert :string_exists, "buoyantSimpleFoam.C", "fvOptions.H" %>

## Equations {#equations}

- <%= link_to_menuid "pv-simple" %>
- <%= link_to_menuid "algorithm-p-rgh" %>

## Input requirements {#input-requirements}

Mandatory fields:

- p: pressure \[pa\]
- p_rgh: pressure - hydrostatic contribution \[pa\]
- U: velocity \[m/s\]
- T: temperature \[K\]

<% assert :string_exists, "buoyantSimpleFoam/createFields.H", "p_rgh" %>
<% assert :string_exists, "buoyantSimpleFoam/createFields.H", "U" %>

### Physical models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence):
  `constant/turbulenceProperties`
- [heat transfer](<%= ref_id "models-thermophysical", "usage" %>):
  `constant/thermophysicalModels`
- [finite volume options](<%= ref_id "numerics-fvoptions", "usage" %>):
  `constant/fvOptions` (optional)

### Solution controls {#input-requirements-solution-controls}

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "schemes-divergence", "Steady state schemes", "special-cases-steady-state" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/heatTransfer/buoyantSimpleFoam" %>

See also

- <%= link_to_menuid "simplefoam" %>
- <%= link_to_menuid "rhosimplefoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantSimpleFoam" %>
