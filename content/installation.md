---
title: Installation
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- install
- download
menu_id: installation
license: CC-BY-NC-ND-4.0
menu_parent: home
menu_weight: 15
---

- [Download](./download)
- [Contents](./contents)