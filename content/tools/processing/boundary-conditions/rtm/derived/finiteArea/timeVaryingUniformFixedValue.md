---
title: timeVaryingUniformFixedValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-timeVaryingUniformFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-fa
group: bcs-fa
---

<%= page_toc %>

## Description

The `timeVaryingUniformFixedValue` is a boundary condition that provides
a time-varying form of a uniform fixed value condition for finite area
calculations.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            timeVaryingUniformFixedValue;

        // Inherited entries
        ...
    }

<% assert :string_exists, "timeVaryingUniformFixedValueFaPatchField.H", "timeVaryingUniformFixedValue" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `timeVaryingUniformFixedValue`  | word    | yes      | -

The inherited entries are elaborated in:

- faPatchFields.H
- interpolationTable.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteArea/fields/faPatchFields/derived/timeVaryingUniformFixedValue" %>

API:

- [Foam::timeVaryingUniformFixedValueFaPatchField](doxy_id://Foam::timeVaryingUniformFixedValueFaPatchField)

<%= history "v1712" %>

<!----------------------------------------------------------------------------->
