---
title: CourantNo
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- Courant
- field
- function object
- post-processing
menu_id: function-objects-field-courantno
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `CourantNo` function object computes the field of Courant number, i.e. `Co`,
for time-variant simulations.

$$
    \text{Co} = \Delta_t \, \tau
$$

where $$\Delta_t$$ is the time-step size, and $$\tau$$ is a characteristic
time scale based on the local cell flow scales:

$$
    \tau = \frac{1}{2 V} \sum_{faces} \left| \phi_i \right|
$$

Here, $$V$$ is the cell volume, $$\phi$$ the face volumetric flux,
and $$\sum_{faces}$$ summation is over all cell faces.

The Courant number provides

- a measure of the rate at which information is
  transported under the influence of a flux field,
- a limiting factor for the performance of numerical schemes.

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | -                    | -
output file  | -                    | -
output field | volScalarField       | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `CourantNo` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    Co1
    {
        // Mandatory entries (unmodifiable)
        type            CourantNo;
        libs            (fieldFunctionObjects);

        // Optional entries (runtime modifiable)
        rho             rho;

        // Optional (inherited) entries
        field           <phi>;
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "CourantNo/CourantNo.H", "CourantNo" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "CourantNo",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: CourantNo                | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 rho          | Name of density field               | word |  no      | rho

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func CourantNo

## Stored properties  {#stored-properties}

### Fields

The Courant number is stored on the mesh database, using the default name

    Co

If the `field` entry is set to a value other than `phi`, the default name
becomes

    Co(<field>)

This name can be overridden by using the `result` entry.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/CourantNo" %>

API:

- [Foam::functionObjects::CourantNo](doxy_id://Foam::functionObjects::CourantNo)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
