---
title: atmNutkWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-atmNutkWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

The `atmNutkWallFunction` is a boundary condition that provides
a wall constraint on the turbulent viscosity (i.e. `nut`) based on the
turbulent kinetic energy (i.e. `k`) for atmospheric boundary layer modelling.
It is designed to be used in conjunction with the `atmBoundaryLayerInletVelocity`
boundary condition.

The governing equation of the boundary condition:

$$
    u = \frac{u^*}{\kappa} ln \left(\frac{z + z_0}{z_0}\right)
$$

where:

Property     | Description
-------------|--------------------------------------------
$$u^*$$      | Friction velocity
$$\kappa$$   | von Karman constant
$$z_0$$      | Surface roughness length       \[m\]
$$z$$        | Ground-normal coordinate

### References

- Theoretical expressions (tag:HW):

    Hargreaves, D. M., & Wright, N. G. (2007).
    On the use of the k–ε model in commercial CFD software to model
    the neutral atmospheric boundary layer.
    J. of wind engineering and industrial aerodynamics, 95(5), 355-369.
    DOI:10.1016/j.jweia.2006.08.002

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            atmNutkWallFunction;
        z0              <PatchFunction1<scalar>>;

        // Optional entries
        boundNut        <bool>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "atmNutkWallFunctionFvPatchScalarField.H", "atmNutkWallFunction" %>
<% assert :string_exists, "atmNutkWallFunctionFvPatchScalarField.C", "z0" %>
<% assert :string_exists, "atmNutkWallFunctionFvPatchScalarField.C", "boundNut" %>

where:

Property   | Description                      | Type    | Required | Default
-----------|----------------------------------|---------|----------|---------
`type`     | Type name: `atmNutkWallFunction` | word    | yes      | -
`z0`       | Surface roughness length \[m\]   | PatchFunction1\<scalar\> | yes | -
`boundNut` | Flag to zero-bound nut near wall | bool   | no      | false

The inherited entries are elaborated in:

- nutkWallFunctionFvPatchScalarField.H
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/turbineSiting" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/wallFunctions/atmNutkWallFunction" %>

API:

- [Foam::atmNutkWallFunctionFvPatchScalarField](doxy_id://Foam::atmNutkWallFunctionFvPatchScalarField)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
