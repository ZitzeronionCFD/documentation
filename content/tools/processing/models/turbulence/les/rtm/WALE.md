---
title: Wall Adapting Local Eddy-viscosity (WALE)
short_title: WALE
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- LES
- turbulence
menu_id: les-wale
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-les
group: turbulence-les
---

<%= page_toc %>

## Properties {#properties}

- <B>W</B>all <B>A</B>dapting <B>L</B>ocal <B>E</B>ddy-viscosity

## Model equations {#model-equations}

## Default model coefficients {#default-coeffs}

$$ C_e $$ | $$ C_k $$ | $$ C_w $$
------------|-------------|------------
      1.048 |       0.094 |      0.325

## Initialisation {#initialisation}

## Usage {#usage}

The model is specified using:

    LES
    {
        turbulence      on;
        LESModel        WALE;
    }

<% assert :string_exists, "LESModel.C", "LES" %>
<% assert :string_exists, "LESModel.C", "LESModel" %>
<% assert :string_exists, "WALE.H", "WALE" %>

## Boundary conditions {#bcs}

Inlet

- <%= link_to_menuid "boundary-conditions-fixedValue" %>

Outlet

- [Constrained outlet](ref_id://boundary-conditions-inletOutlet)

Walls

- wall functions

## Further information {#further-information}

Source code:

- [Foam::LESModels::WALE](doxy_id://Foam::LESModels::WALE)

References:

- *Nicoud and Ducros* <%= cite "nicoud_subgrid-scale_1999" %>
