---
title: nutUBlendedWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-nutUBlendedWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `nutUBlendedWallFunction` is boundary condition that provides
a wall function for the turbulent
viscosity (i.e. `nut`) based on velocity (i.e. `U`) using a
binomial-function wall-function blending method between the viscous
and inertial sublayer predictions of \c nut for low- and high-Reynolds
number applications.

$$
    u_\tau = (u_{\tau,vis}^n + u_{\tau,log}^n)^{1/n}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$u_\tau$$            | Friction velocity
$$u_{\tau,vis}$$      | Friction velocity in the viscous sublayer
$$u_{\tau,log}$$      | Friction velocity in the inertial sublayer

#### References

- See the section that describes 'automatic wall treatment':

    Menter, F., Ferreira, J. C., Esch, T., Konno, B. (2003).
    The SST turbulence model with improved wall treatment
    for heat transfer predictions in gas turbines.
    In Proceedings of the International Gas Turbine Congress.
    November, 2003. Tokyo, Japan. pp. 2-7.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            nutUBlendedWallFunction;

        // Optional entries
        n               4.0;

        // Inherited entries
        ...
    }

<% assert :string_exists, "nutUBlendedWallFunctionFvPatchScalarField.H", "nutUBlendedWallFunction" %>
<% assert :string_exists, "nutUBlendedWallFunctionFvPatchScalarField.C", "n" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `nutUBlendedWallFunction`       | word    | yes      | -
`n`            | Blending type                              | scalar  | no       | 4.0

The inherited entries are elaborated in:

- nutWallFunctionFvPatchScalarField.H
<!-- end of the list -->

- The full 'automatic wall treatment' description also requires use of the
  \link omegaWallFunctionFvPatchScalarField.H with the \c blending
  option `binomial` or with the deprecated `blended` flag set to `on`.
- Suffers from non-exact restart since `correctNut()` (called through
  `turbulence->validate`) returns a slightly different value every time
  it is called. See `nutUSpaldingWallFunctionFvPatchScalarField.C`.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/backwardFacingStep2D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/nutWallFunctions/nutUBlendedWallFunction" %>

API:

- [Foam::nutUBlendedWallFunctionFvPatchScalarField](doxy_id://Foam::nutUBlendedWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
