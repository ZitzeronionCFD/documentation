---
title: DILU preconditioner
short_title: DILU
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- DILU
- preconditioner
- solvers
menu_id: cg-preconditioner-dilu
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-cg
group: solvers-cg-preconditioner
---

<%= page_toc %>

## Properties {#properties}

- Asymmetric matrices
- Simplified <B>D</B>iagonal-based <B>I</B>ncomplete <B>LU</B> preconditioner
- Parallel inconsistent

## Usage {#usage}

    preconditioner  DILU;

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/preconditioners/DILUPreconditioner" %>

API:

- [Foam::DILUPreconditioner](doxy_id://Foam::DILUPreconditioner)
