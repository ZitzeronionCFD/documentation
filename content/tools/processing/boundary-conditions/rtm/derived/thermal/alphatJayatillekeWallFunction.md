---
title: alphatJayatillekeWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-alphatJayatillekeWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `alphatJayatillekeWallFunction` is a boundary condition that provides
a thermal wall function for turbulent thermal diffusivity (usually `alphat`)
based on the Jayatilleke model.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            compressible::alphatJayatillekeWallFunction;

        // Optional entries
        Prt             <scalar>;
        kappa           <scalar>;
        E               <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "alphatJayatillekeWallFunctionFvPatchScalarField.H", "alphatJayatillekeWallFunction" %>
<% assert :string_exists, "alphatJayatillekeWallFunctionFvPatchScalarField.C", "Prt" %>
<% assert :string_exists, "alphatJayatillekeWallFunctionFvPatchScalarField.C", "kappa" %>
<% assert :string_exists, "alphatJayatillekeWallFunctionFvPatchScalarField.C", "E" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `compressible::alphatJayatillekeWallFunction` | word | yes | -
`Prt`                  | Turbulent Prandtl number                   | scalar  | no       | 0.85
`kappa`                | von Karman constant                        | scalar  | no       | 0.41
`E`                    | Wall roughness parameter                   | scalar  | no       | 9.8

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantSimpleFoam/comfortHotRoom" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/wallFunctions/alphatWallFunctions/alphatJayatillekeWallFunction" %>

API:

- [Foam::compressible::alphatJayatillekeWallFunctionFvPatchScalarField](doxy_id://Foam::compressible::alphatJayatillekeWallFunctionFvPatchScalarField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
