---
title: References
copyright:
- Copyright(C) 2022 OpenCFD Ltd.
authors:
- OpenCFD Ltd.
license: CC-BY-NC-ND-4.0
menu_id: references
menu_parent: home
menu_weight: 1000
---

<%= page_toc %>

<ol>
<% @config[:cites].each do |key, value| %>
  <li><a name="<%= key %>"></a><%= value[:url] %></li>
<% end %>
</ol>
