---
title: Optimisation boundary conditions
short_title: Optimisation
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-optimisation
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

Available optimisation boundary conditions include:

<%= insert_models "bcs-optimisation" %>
