---
title: atmNutUWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-atmNutUWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

The `atmNutUWallFunction` is a boundary condition that provides
a wall constraint on the turbulent viscosity (i.e. `nut`) based on velocity
(i.e. `U`) for atmospheric boundary layer modelling.  It is designed to be
used in conjunction with the `atmBoundaryLayerInletVelocity` boundary condition.

The governing equation of the boundary condition:

$$
    u = \frac{u^*}{\kappa} ln \left(\frac{z + z_0}{z_0}\right)
$$

where:

Property     | Description
-------------|--------------------------------------------
$$u^*$$      | Friction velocity
$$\kappa$$   | von Karman constant
$$z_0$$      | Surface roughness length       \[m\]
$$z$$        | Ground-normal coordinate

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            atmNutUWallFunction;
        z0              <PatchFunction1<scalar>>;

        // Optional entries
        boundNut        <bool>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "atmNutUWallFunctionFvPatchScalarField.H", "atmNutUWallFunction" %>
<% assert :string_exists, "atmNutUWallFunctionFvPatchScalarField.C", "z0" %>
<% assert :string_exists, "atmNutUWallFunctionFvPatchScalarField.C", "boundNut" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `atmNutUWallFunction`           | word    | yes      | -
`z0`                   | Surface roughness length \[m\]             | PatchFunction1\<scalar\> | yes | -
`boundNut`             | Flag to zero-bound nut near wall           | bool   | no      | false

The inherited entries are elaborated in:

- nutUWallFunctionFvPatchScalarField.H
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/wallFunctions/atmNutUWallFunction" %>

API:

- [Foam::atmNutUWallFunctionFvPatchScalarField](doxy_id://Foam::atmNutUWallFunctionFvPatchScalarField)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
