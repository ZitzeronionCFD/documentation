---
title: Direct Numerical Simulation (DNS)
short_title: DNS
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
menu_id: solvers-dns
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

## Solvers {#solvers}

- dnsFoam

<!-- <%= insert_models "solvers-dns" %> -->

## Further information {#further-information}

- [Source documentation](doxy_id://grpDNSSolvers)
