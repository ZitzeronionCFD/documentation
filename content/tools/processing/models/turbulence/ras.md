---
title: Reynolds Averaged Simulation (RAS)
short_title: RAS
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: turbulence-ras
license: CC-BY-NC-ND-4.0
menu_parent: models-turbulence
---

<%= page_toc %>

## Options {#options}

OpenFOAM includes Reynolds Averaged Simulation turbulence closures based on
linear and non-linear eddy viscosity models, and Reynolds stress transport
models.

- [Linear eddy viscosity models](linear-evm)
- [Non-linear eddy viscosity models](nonlinear-evm)
- [Reynolds stress transport models](rstm)

## Usage {#usage}

RAS is selected by setting the `simulationType` entry

    simulationType  RAS;

    RAS
    {
        \\ Model input parameters
    }

<% assert :string_exists, "TurbulenceModel.C", "simulationType" %>
<% assert :string_exists, "RASModel.C", "RAS" %>

Suitable for:

- 1-D, 2-D and 3-D cases
- steady-state or transient

## Initialisation {#initialisation}

Care should be taken to provide an adequate initialisation of turbulence fields.
For open systems, the flow can evolve via the inlets.  However, note that the
initial field values should be approximate for the flow medium, where
non-physical levels will result in non-physical turbulence viscosity
predictions and case instability.

Appropriate formulae and/or methods are provided in the documentation for
each model.

## Background {#background}

Reynolds decomposition of the velocity into its mean and fluctuating
contributions takes the form

$$
    \u(\vec{x}, t)
  = \av{\u}(\vec{x}, t) + \u'(\vec{x}, t)
$$

where the mean of the fluctuating component is defined as zero:

$$ \av{\u'} = 0 $$

Applied to the Navier Stokes equations, this leads to equations for the mean
velocity and pressure:

$$
    \ddt{\rho} +  \div \left(\rho \av{\u} \right) = 0
$$

$$
    \ddt{\rho \av{\u}}
  + \div \left( \rho \av{\u} \otimes \av{\u} \right)
  =
    \vec{g}
  + \div \av{\tensor(\tau)}
  - \div \left( \rho \tensor{R} \right)
$$

where the averaged stress tensor, $$ \av{\tau} $$, for Newtonian fluids is
given by:

$$
    \av{\tensor{\tau}}
  =
  - \left(p + \frac{2}{3}\mu \div \av{\u} \right) \tensor{I}
  + \mu \left(\grad \av{\u} + \left(\grad \av{\u} \right)^T \right)
$$

Using the relationship:

$$
    \div \av{\u}
  = \mathrm{tr} \left( \grad \av{\u} \right)
  = \mathrm{tr} \left( \left( \grad \av{\u} \right)^T \right)
$$

the stress tensor becomes:

$$
    \av{\tensor{\tau}}
  =
- p \tensor{I}
  + \mu \left[\grad \av{\u} + \left( \grad \av{\u} \right)^T - \frac{2}{3} \mathrm{tr} \left( \left( \grad \av{\u} \right)^T \right) \tensor{I}  \right]
  =
- p \tensor{I}
  + \mu \left[\grad \av{\u} + \mathrm{dev2} \left( \left( \grad \av{\u} \right)^T \right) \right]
$$

and the $$ \mathrm{dev2} $$ operator is defined as:

$$
    \mathrm{dev2}\left(\phi\right)
  =
    \phi - \frac{2}{3} \mathrm{tr} \left(\phi\right) \tensor{I}
$$

$$ \tensor{R} $$, is the *Reynolds stress tensor*

$$ \tensor{R} = \av{\u' \otimes \u'} $$

The Reynolds stress tensor is further divided into isotropic and deviatoric
anisotropic contributions:

$$
    \tensor{R}
  = \av{\u' \otimes \u'}
  = \underbrace{\frac{2}{3}k \tensor{I}}_{\mathrm{isotropic}}
  + \underbrace{\av{\u' \otimes \u'} - \frac{2}{3}k \tensor{I}}_{\mathrm{deviatoric}}
$$

where $$ k $$ is the turbulent kinetic energy, defined by

$$
    k = \frac{1}{2} \av{\u' \dprod \u'}
      = \frac{1}{2} \mathrm{tr}\left( \tensor{R} \right)
$$

Only the anisotropic contribution of the Reynolds stress tensor transports
momentum, whereby the isotropic contribution can be added to the mean
pressure, leading to the full form:

$$
    \ddt{\rho \av{\u}}
  + \div \left( \rho \av{\u} \otimes \av{\u} \right)
  =
    \vec{g}
  - \grad \av{p}'
  + \div \left( \mu \grad \av{\u} \right)
  + \div \left[ \mu \, \mathrm{dev2}\left(\left(\grad \av{\u}\right)^T \right) \right]
  - \div \left( \rho \tensor{R}_\mathit{dev} \right)
$$

where

$$
    \tensor{R}_\mathit{dev}
  = \av{\u' \otimes \u'} - \frac{2}{3}k \tensor{I}
$$

and

$$ \av{p}' = \av{p} + \frac{2}{3} \rho k $$

The RAS family of turbulence models provide methods to approximate the
deviatoric anisotropic stress contribution due to $$ \tensor{R}_\mathit{dev} $$.

<!-- Use of modified pressure to absorb -2/3 rho k  - see Pope p. 88 -->
<!-- Eddy viscosity hypothesis - see Pope p. 93 -->
<!-- Gradient diffusion hypothesis for nu_eff = nu + nu_t - see Pope p. 93 -->

## Wall modelling {#wall-modelling}

- See: [Wall functions](wall-functions)

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/RAS" %>
- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions" %>

Related:

- [Source documentation](doxy_id://grpRASTurbulence)
