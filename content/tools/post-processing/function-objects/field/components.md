---
title: components
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- components
- field
- function object
- post-processing
menu_id: function-objects-field-components
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `components` function object extracts the components of a field and outputs
the result into new fields, where the fields possess the same sizes and
dimensions.

More than one field input per function object is not allowed. Therefore, for
each given field, a separate `components` function object should be created.

Depending on the base type of the input field, the number of output fields
and output suffixes are as follows (the base type cannot be `scalar` since
scalars have no components):

    Base type       | Number of components | Suffixes
    vector          | 3                    | (x y z)
    sphericalTensor | 3                    | (x y z)
    symmTensor      | 6                    | (xx xy xz yy yz zz)
    tensor          | 9                    | (xx xy xz yx yy yz zx zy zz)

### Operands

Operand      | Type                                            | Location
:---         |:---                                             |:---
input        | `{vol,surface}<Type>Field` (`Type` != Scalar)   | `$FOAM_CASE/<time>/<inpField>`
output file  | -                                               | -
output field | `{vol,surface}ScalarField` for each component   | `$FOAM_CASE/<time>/<outField><suffix>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `components` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    components1
    {
        // Mandatory entries (unmodifiable)
        type            components;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entry (runtime modifiable)
        field           <field>;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "components.H", "components" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "components",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "functionObjectList.C", "field" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: components               | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 field        | Name of the operand field           | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Minimal example by using the `postProcess` utility:

    postProcess -func "components(<field>)"

### Notes on entries

The `components` function object is not executed for fields whose base type is
`scalar`, e.g. `volScalarField`, since `scalar` has a single component which
is itself.

The `result` entry is intentionally omitted by the `components` function object,
therefore the default names for the output fields (i.e. component suffixes)
cannot be overridden.

## Stored properties  {#stored-properties}

### Fields

The `components` function object is stored on the mesh database,
using the default name:

    <field><component>

For instance, for a field named `U.air`, resulting the following fields:

    U.airx U.airy U.airz

The default names **cannot** be overridden by using the `result` entry.

## Further information {#further-information}

Tutorial:

- <%= repo_link2(
  "$FOAM_TUTORIALS/multiphase/twoPhaseEulerFoam/laminar/bubbleColumnIATE") %>
- <%= repo_link2("$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity") %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/components" %>

API:

- [Foam::functionObjects::components](doxy_id://Foam::functionObjects::components)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
