---
title: fixedValueOutflow
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-fixedValueOutflow
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-fa
group: bcs-fa
---

<%= page_toc %>

## Description

The `fixedValueOutflow` is a boundary condition that provides
a fixed value outflow condition for finite-area calculations.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedValueOutflow;

        // Inherited entries
        ...
    }

<% assert :string_exists, "fixedValueOutflowFaPatchField.H", "fixedValueOutflow" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `fixedValueOutflow`             | word    | yes      | -

The inherited entries are elaborated in:

- faPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteArea/fields/faPatchFields/derived/fixedValueOutflow" %>

API:

- [Foam::fixedValueOutflowFaPatchField](doxy_id://Foam::fixedValueOutflowFaPatchField)

<%= history "v1712" %>

<!----------------------------------------------------------------------------->
