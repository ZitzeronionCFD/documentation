---
title: Rotor disk
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- disk
- fvOption
- momentum
- rotor
- source
menu_id: fvoption-rotordisk
license: CC-BY-NC-ND-4.0
menu_parent: numerics-fvoptions-sources
group: fvoptions-sources-momentum
---

<%= page_toc %>

## Properties {#properties}

## Usage {#usage}

The option is specified using:

    disk1
    {
        type            rotorDiskSource;
        selectionMode   cellSet;
        cellSet         rotorDisk1;
        fields          (U);
    }

<% assert :string_exists, "rotorDiskSource.H", "rotorDiskSource" %>

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/rotorDisk" %>

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/derived/rotorDiskSource" %>
