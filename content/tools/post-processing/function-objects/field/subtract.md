---
title: subtract
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- subtract
menu_id: function-objects-field-subtract
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#sec-fos-field-subtract-description}

The `subtract` function object
subtracts a given list of (at least one or more)
fields from a field and produces a new field, where the fields possess the
same sizes and dimensions:

    fieldResult = field1 - field2 - ... - fieldN

### Operands

Operand      | Type                          | Location
:---         |:---                           |:---
input        | `{vol,surface}<Type>Field(s)` | `$FOAM_CASE/<time>/<inpField>s`
output file  | -                             | -
output field | `{vol,surface}<Type>Field`    | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#sec-fos-field-subtract-usage}

Example of the `add` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    subtract1
    {
        // Mandatory entries (unmodifiable)
        type            subtract;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entry (runtime modifiable)
        fields          (<field1> <field2> ... <fieldN>);

        // Optional (inherited) entries
        result          <fieldResult>;
        region          region0;
        subRegion       region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "subtract.H", "subtract" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "subtract",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "functionObjectList.C", "fields" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "regionFunctionObject.C", "subRegion" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

The minimal set of entries comprise:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: subtract                 | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 field        | Names of the operand fields         | wordList | yes  | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldsExpression](ref_id://function-objects-field-fieldsexpression)

Example by using the `postProcess` utility:

    postProcess -func "subtract(<field1>, <field2>, ..., <fieldN>)"

## Stored properties  {#sec-fos-field-subtract-stored-properties}

### Fields

The `subtract` function object is stored on the mesh database,
using the default name:

    subtract(<field1>,<field2>,...,<fieldN>)

This can be overridden by using the `result` entry.

## Further information {#sec-fos-field-subtract-further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/twoPhaseEulerFoam/laminar/bubbleColumn" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/subtract" %>

API:

- [Foam::functionObjects::subtract](doxy_id://Foam::functionObjects::subtract)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
