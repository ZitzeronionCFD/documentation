---
title: Cyclic
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
- coupled
menu_id: boundary-conditions-cyclic
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-coupled
group: bcs-coupled
---

<%= page_toc %>

## Properties {#properties}

- Coupling condition between a pair of patches
- Faces on each coupled patch must have the same topology, i.e. they must have
  collocated points such a one-to-one mapping exists between the faces on
  each side.
- The coupling is treated implicitly using the cell values adjacent to
  each pair of cyclic patches
- The face values are determined using linear interpolation between the cell
  values

## Usage {#usage}

The condition requires entries in both the `boundary` and field files. Mesh
`boundary` file:

    <patchName>
    {
        type            cyclic;
        neighbourPatch  <coupled patch name>;
        transform       <transform type>;
        ...
    }

<% assert :string_exists, "cyclicPolyPatch.H", "cyclic" %>
<% assert :string_exists, "cyclicPolyPatch.C", "neighbourPatch" %>
<% assert :string_exists, "coupledPolyPatch.C", "transform" %>

Field file:

    <patchName>
    {
        type            cyclic;
    }

<% assert :string_exists, "cyclicPolyPatch.H", "cyclic" %>

## Details {#details}

![Cyclic boundary types](../boundary-conditions-cyclic-small.png)

## Further information {#further-information}

Source code:

- [Foam::cyclicPolyPatch](doxy_id://Foam::cyclicPolyPatch)
- [Foam::cyclicFvPatch](doxy_id://Foam::cyclicFvPatch)
- [Foam::cyclicFvPatchField](doxy_id://Foam::cyclicFvPatchField)

Example usage:

- pimpleFoam `decayIsoTurb` tutorial:
  <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/decayIsoTurb" %>

\ofAssert patchTypeExists incompressible/pimpleFoam/LES/decayIsoTurb cyclic
