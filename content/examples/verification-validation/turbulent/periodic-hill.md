---
title: Periodic hill
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- example
- turbulence
- verification
- validation
menu_id: verification-validation-turbulent-periodic-hill
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-turbulent
group: verification-validation-turbulent
---

<%= page_toc %>

## Overview {#overview}

- 3D flow over a bump at a Reynolds number of $$ 10565 $$ (L=0.028m)
- Steady case used to initialise the transient case
  - Steady case: [simpleFoam](ref_id://simplefoam)
  - Transient case: [pimpleFoam](ref_id://pimplefoam)
- Flow driven using a [mean velocity force](ref_id://fvoption-meanvelocityforce)
  [fvOption](ref_id://numerics-fvoptions)
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/periodicHill" %>

![Case description](periodic-hill-boundary-conditions-small.png)

## Mesh {#mesh}

- 3D structured mesh created using [blockMesh](ref_id://blockmesh)
- hills described by the function

  $$
    y(x) =
    \begin{cases}
    \min(1, a_1 + b_1 x + c_1 x^2 + d_1 x^3)  & 0 \le x \lt 9, \\
    a_2 + b_2 x + c_2 x^2 + d_2 x^3  & 9 \le x \lt 14, \\
    a_3 + b_3 x + c_3 x^2 + d_3 x^3  & 14 \le x \lt 20, \\
    a_4 + b_4 x + c_4 x^2 + d_4 x^3  & 20 \le x \lt 30, \\
    a_5 + b_5 x + c_5 x^2 + d_5 x^3  & 30 \le x \lt 40, \\
    \max(0, a_6 + b_6 x + c_6 x^2 + d_6 x^3)  & 40 \le x \lt 54. \\
    \end{cases}
  $$

|     |  a  |  b  |  c  |  d  |
|-----|----:|----:|----:|----:|
|  1  | $$ 28 $$ |  $$ 0 $$ | $$ 6.775070969851 \times 10^{-3} $$ | $$ - 2.124527775800 \times 10^{-3} $$ |
|  2  | $$ 25.07355893131 \times 10^0$$ | $$ 0.9754803562315 \times 10^{0} $$ | $$ - 1.016116352781 \times 10^{-1} $$ | $$ 1.889794677828 \times 10^{-3} $$ |
|  3  | $$ 2.579601052357 \times 10^1 $$ | $$  8.206693007457 \times 10^{-1} $$ | $$ - 9.055370274339 \times 10^{-2} $$ | $$ 1.626510569859 \times 10^{-3} $$ |
|  4  | $$ 4.046435022819 \times 10^1 $$ | $$ -1.379581654948 \times 10^{0} $$ | $$ 1.945884504128 \times 10^{-2} $$ | $$  - 2.070318932190 \times 10^{-4} $$ |
|  5  | $$ 1.792461334664 \times 10^1 $$ | $$  8.743920332081 \times 10^{-1} $$ | $$ - 5.567361123058 \times 10^{-2} $$ | $$  6.277731764683 \times 10^{-4} $$ |
|  6  | $$ 5.639011190988 \times 10^1 $$ | $$ -2.010520359035 \times 10^{0} $$ | $$ 1.644919857549 \times 10^{-2} $$ | $$  2.674976141766 \times 10^{-5} $$ |

- this has been set in the `blockMeshDict` using a `codeStream`

<!--
![Mesh](periodic-hill-mesh-hills-small.png)
-->

![Close-up around a hill](periodic-hill-mesh-hills-small.png)

## Boundary conditions {#boundary-conditions}

- The mean bulk velocity $$ \u_b $$ at the inlet patch is defined as:

  $$
    \u_b = \frac{1}{2.0355H}\int\limits_{H}^{3.035H} \u_x (y) dy
  $$

- This is set to 1 m/s, and maintained using a
  [mean velocity force](ref_id://fvoption-meanvelocityforce)
  [fvOption](ref_id://numerics-fvoptions)
- The laminar viscosity is set to achieve the target Reynolds numbers, where
  the reference length scale is given by the hill height

- The laminar viscosity is derived from the Reynolds number, i.e.

  $$
      \nu_\infty
    = \frac{|\u_b| H}{Re}
    = \frac{1 \times 0.028}{10565}
    = 2.65 \times 10^{-6} m^2/s
  $$

### Common fields {#boundary-conditions-turbulence-common}

Velocity: `U`

| Patch   | Condition                                             | Value  |
|---------|-------------------------------------------------------|--------|
| Inlet   | [cyclic](ref_id://boundary-conditions-cyclic)         |        |
| Outlet  | [cyclic](ref_id://boundary-conditions-cyclic)         |        |
| Hills   | [noSlip](ref_id://boundary-conditions-noSlip)        |        |
| Walls   | [noSlip](ref_id://boundary-conditions-noSlip)        |        |

Pressure: `p`

| Patch   | Condition                                              | Value |
|---------|--------------------------------------------------------|-------|
| Inlet   | [cyclic](ref_id://boundary-conditions-cyclic)          |       |
| Outlet  | [cyclic](ref_id://boundary-conditions-cyclic)          |       |
| Hills   | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |   |
| Walls   | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |   |

### Turbulence fields {#boundary-conditions-turbulence}

- turbulence model: [SA-IDDES](ref_id://des-spalartallmarasiddes)

Turbulence viscosity: `nut`

| Patch   | Condition                                              | Value |
|---------|--------------------------------------------------------|-------|
| Inlet   | [cyclic](ref_id://boundary-conditions-cyclic)          |       |
| Outlet  | [cyclic](ref_id://boundary-conditions-cyclic)          |       |
| Hills   | `nutUSpaldingWallFunction`                             |       |
| Walls   | `nutUSpaldingWallFunction`                             |       |

#### Spalart-Allmaras IDDES

Modified turbulence viscosity: `nuTilda`

| Patch   | Condition                                              | Value |
|---------|--------------------------------------------------------|-------|
| Inlet   | [cyclic](ref_id://boundary-conditions-cyclic)          |       |
| Outlet  | [cyclic](ref_id://boundary-conditions-cyclic)          |       |
| Hills   | [fixedValue](ref_id://boundary-conditions-fixedValue) |     0 |
| Walls   | [fixedValue](ref_id://boundary-conditions-fixedValue) |     0 |

## Results {#results}

The precursor steady computation is used to initialise the transient
calculation.  After evolving the transient case for XXX flow-throughs a fully
turbulent flow is established, as shown by the instantaneous velocity:

![Instantaneous velocity](periodic-hill-U-small.png)

The average velocity prediction shows differences compared to the velocity
derived from the precursor steady calculation:

![Mean velocity](periodic-hill-UMean-small.png)

Turbulent structures are clearly evident in the instantaneous Q criterion
prediction:

![Instantaneous Q critereon](periodic-hill-Q-U-small.png)

The following series of images provide a quantitative comparison between
OpenFOAM predictions and both measured data and results from another CFD code
at various streamwise locations.

### Profiles {#results-profiles}

Average velocity profiles:

| ![U 0.05](U_at_x_by_h0.05-small.png) | ![U 0.5](U_at_x_by_h0.5-small.png) |
| ![U 1](U_at_x_by_h1-small.png) | ![U 2](U_at_x_by_h2-small.png) |
| ![U 3](U_at_x_by_h3-small.png) | ![U 4](U_at_x_by_h4-small.png) |
| ![U 5](U_at_x_by_h5-small.png) | ![U 6](U_at_x_by_h6-small.png) |
| ![U 7](U_at_x_by_h7-small.png) | ![U 8](U_at_x_by_h8-small.png) |
{: .noBorder}

Average normal stresses: uu

| ![uu 0.05](uu_at_x_by_h0.05-small.png) | ![uu 0.5](uu_at_x_by_h0.5-small.png)|
| ![uu 1](uu_at_x_by_h1-small.png) | ![uu 2](uu_at_x_by_h2-small.png) |
| ![uu 3](uu_at_x_by_h3-small.png) | ![uu 4](uu_at_x_by_h4-small.png) |
| ![uu 5](uu_at_x_by_h5-small.png) | ![uu 6](uu_at_x_by_h6-small.png) |
| ![uu 7](uu_at_x_by_h7-small.png) | ![uu 8](uu_at_x_by_h8-small.png) |
{: .noBorder}

Average normal stresses: vv

| ![vv 0.05](vv_at_x_by_h0.05-small.png) | ![vv 0.5](vv_at_x_by_h0.5-small.png)|
| ![vv 1](vv_at_x_by_h1-small.png) | ![vv 2](vv_at_x_by_h2-small.png) |
| ![vv 3](vv_at_x_by_h3-small.png) | ![vv 4](vv_at_x_by_h4-small.png) |
| ![vv 5](vv_at_x_by_h5-small.png) | ![vv 6](vv_at_x_by_h6-small.png) |
| ![vv 7](vv_at_x_by_h7-small.png) | ![vv 8](vv_at_x_by_h8-small.png) |
{: .noBorder}

Average shear stress: uv

| ![uv 0.05](uv_at_x_by_h0.05-small.png) | ![uv 0.5](uv_at_x_by_h0.5-small.png)|
| ![uv 1](uv_at_x_by_h1-small.png) | ![uv 2](uv_at_x_by_h2-small.png) |
| ![uv 3](uv_at_x_by_h3-small.png) | ![uv 4](uv_at_x_by_h4-small.png) |
| ![uv 5](uv_at_x_by_h5-small.png) | ![uv 6](uv_at_x_by_h6-small.png) |
| ![uv 7](uv_at_x_by_h7-small.png) | ![uv 8](uv_at_x_by_h8-small.png) |
{: .noBorder}
