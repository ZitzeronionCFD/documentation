---
title: Finite volume options
short_title: fvOptions
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- fvOption
menu_id: numerics-fvoptions
license: CC-BY-NC-ND-4.0
menu_parent: numerics
---

<%= page_toc %>

## Introduction {#introduction}

OpenFOAM solver applications typically include core functionality such as
turbulence modelling, heat transfer, and buoyancy effects.

Further flexibility is offered via `fvOptions`---a collection of run-time
selectable finite volume options to manipulate systems of equations by adding
sources/sinks, imposing constraints and applying corrections.

These are specified in the <%= link_to_menuid "fvoptions" %> file
located in the [$FOAM_CASE/system](ref_id://fvoptions) or
[$FOAM_CASE/constant](ref_id://fvoptions) directories.

## Options {#sources}

- [Sources](sources)
- [Constraints](constraints)
- [Corrections](corrections)

## Usage {#usage}

### Selecting the region {#usage-cell-set-option}

The majority of options are applied to collections of mesh cells.  These can
be selected according to the entry `selectionMode`, e.g.

    selectionMode       all;

<% assert :string_exists, "cellSetOption.C", "selectionMode" %>
<% assert :string_exists, "cellSetOption.C", "all" %>

Valid `selectionMode` entries include:

- `all`: all cells
- `cellZone`: cells defined by a cell zone.  This requires an additional entry to specify the name of the cell zone, e.g.

~~~
selectionMode       cellZone;
cellZone            myCellZone;
~~~

<% assert :string_exists, "cellSetOption.C", "selectionMode" %>
<% assert :string_exists, "cellSetOption.C", "cellZone" %>

where `myCellZone` is the name of the cell zone

- `cellSet`: cells defined by a cell set.  This requires an additional entry
to specify the name of the cell set, e.g.

~~~
selectionMode       cellSet;
cellSet             myCellSet;
~~~

<% assert :string_exists, "cellSetOption.C", "selectionMode" %>
<% assert :string_exists, "cellSetOption.C", "cellSet" %>

where `myCellSet` is the name of the cell set.

- `points`: a list of points.  This requires an additional entry to list the
points, e.g.

~~~
selectionMode       points;
points              ((0 0 0) (1 1 1) (2 2 2));
~~~

<% assert :string_exists, "cellSetOption.C", "selectionMode" %>
<% assert :string_exists, "cellSetOption.C", "points" %>

<!-- ## Inter-region option entries {#usage-inter-region-option} -->
<!--
# Further information {#further-information}
- [Source documentation]()
-->
