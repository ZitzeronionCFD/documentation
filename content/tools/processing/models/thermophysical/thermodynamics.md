---
title: Thermodynamics
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- thermodynamics
menu_id: thermophysical-thermo
license: CC-BY-NC-ND-4.0
menu_parent: models-thermophysical
---

<%= page_toc %>

The thermodynamics model is responsible for evaluating:

- Specific heat capacity at constant pressue, $$ c_p $$ \[J/kg/k\]
- Absolute enthalpy, $$ H_a $$ \[J/kg\]
- Sensible enthalpy, $$ H_s $$ \[J/kg\]
- Chemical enthalpy, $$ H_c $$ \[J/kg/K\]
- Entropy, $$ S $$ \[J/kg\]
- Gibbs free energy at the standard state, $$ G_{\std} $$, \[J/kg\]

## Options {#options}

Equation of state model selections include:

<%= insert_models "thermo-models" %>

## Usage {#usage}

The thermodynamics type is specified in the `thermoType` dictionary:

~~~
thermoType
{
    thermodynamics      <model>;
    <...>
}
~~~
