---
title: Mass transport equation
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
menu_id: mass-transport
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

The mass transport equation is given as follows:

$$
    \frac{\partial \rho}{\partial t}
  + \div(\rho \u)
    =
    0
$$

where:

Property  | Description
----------|-------------
$$t$$     | Time
$$\rho$$  | Density
$$\u$$    | Velocity


<!----------------------------------------------------------------------------->
