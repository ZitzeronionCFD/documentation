---
title: Gauss gradient scheme
short_title: Gauss
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- Gauss
- gradient
menu_id: schemes-gradient-gauss
license: CC-BY-NC-ND-4.0
#menu_parent: schemes-gradient
group: schemes-gradient
---

<%= page_toc %>

## Properties {#properties}

The cell gradient is calculated using [Gauss'](ref_id://schemes-gausstheorem) theorem:

$$
    \int_V \left( \div \u \right) dV
  = \oint_S \left( \vec{n} \dprod \u \right) dS
$$

## Usage {#usage}

The scheme is specified using:

    gradSchemes
    {
        default         none;
        grad(U)         Gauss <interpolation scheme>;
    }

## Further information {#further-information}

Source code

- [Foam::fv::gaussGrad](doxy_id://Foam::fv::gaussGrad)
