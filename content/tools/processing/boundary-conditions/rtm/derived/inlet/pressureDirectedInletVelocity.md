---
title: pressureDirectedInletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-pressureDirectedInletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `pressureDirectedInletVelocity` is a velocity boundary condition that
calculates inflow velocity from the flux with the specified inlet
direction.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                pressureDirectedInletVelocity;
        inletDirection      <vectorField>;

        // Optional entries
        rho                 <word>;
        phi                 <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "pressureDirectedInletVelocityFvPatchVectorField.H", "pressureDirectedInletVelocity" %>
<% assert :string_exists, "pressureDirectedInletVelocityFvPatchVectorField.C", "inletDirection" %>
<% assert :string_exists, "pressureDirectedInletVelocityFvPatchVectorField.C", "phi" %>
<% assert :string_exists, "pressureDirectedInletVelocityFvPatchVectorField.C", "rho" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `pressureDirectedInletVelocity` | word    | yes      | -
`inletDirection`       | Inlet direction field                      | vectorField  | yes | -
`rho`                  | Name of the density field                  | word    | no       | rho
`phi`                  | Name of the flux field                     | word    | no       | phi

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

- If reverse flow is possible or expected use the
  `pressureDirectedInletOutletVelocity` condition instead.
  {: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/pressureDirectedInletVelocity" %>

API:

- [Foam::pressureDirectedInletVelocityFvPatchVectorField](doxy_id://Foam::pressureDirectedInletVelocityFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
