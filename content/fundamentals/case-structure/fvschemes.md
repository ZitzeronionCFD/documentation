---
title: fvSchemes
copyright:
- Copyright (C) 2021 OpenCFD Ltd.
tags:
- navigation
- system
- fvSchemes
menu_id: fvschemes
license: CC-BY-NC-ND-4.0
menu_parent: case-structure
---

<%= page_toc %>

<%= repo_file_snippet "icoFoam/cavity/cavity/system/fvSchemes" %>
