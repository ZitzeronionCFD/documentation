# frozen_string_literal: true

require 'nokogiri'

# Add class for tables
# class BootstrapFilter < Nanoc::Filter
#   identifier :bootstrap
#   def run(content, _params)
#     content.gsub('<table>', '<table class="table">')
#   end
# end
class BootstrapFilter < Nanoc::Filter
  identifier :bootstrap

  def run(content, _params)
    doc = Nokogiri::HTML(content)

    doc.search('table').each do |table|
      table.append_class("table")
      table.wrap('<div class="table-responsive"></div>')
    end

    doc.to_s
  end
end
