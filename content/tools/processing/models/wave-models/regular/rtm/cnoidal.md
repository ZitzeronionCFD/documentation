---
title: Cnoidal
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- cnoidal
- regular
- waves
menu_id: waves-regular-cnoidal
license: CC-BY-NC-ND-4.0
menu_parent: models-waves
group: wave-regular
---

<%= page_toc %>

## Properties {#properties}

- Regular waves

## Model equations {#model-equations}

The wave height is modelled by the equation:

$$
    \eta = H
        \left[
            \frac{1}{m} \left( 1 - \frac{E(m)}{K(m)} \right)
          - 1
          + cn^2\left( 2 K(m) \frac{x - ct}{\lambda} \right)
        \right]
$$

Where:

$$\lambda$$
: wavelength

$$m$$
: elliptic parameter that depends on the wave characteristic

$$K(m)$$
: complete elliptic integral of the first kind

$$E(m)$$
: complete elliptic integral of the second kind

$$cn$$
: Jacobi's elliptic functions

$$c$$
: wave celerity

$$H$$
: wave height

$$t$$
: time

## Default model coefficients {#default-coeffs}

## Usage {#usage}

Inlet patch example

    <patch>
    {
        alpha           alpha.water;
        waveModel       cnoidal;
        nPaddle         1;
        waveHeight      0.1;
        waveAngle       0.0;
        rampTime        6.0;
        activeAbsorption yes;
        wavePeriod      4.0;
    }

## Further information {#further-information}

Source code:

- [Foam::waveModels::cnoidal](doxy_id://Foam::waveModels::cnoidal)

References:

- ...

Tutorials:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/cnoidal" %>
