---
title: atmOmegaWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-atmOmegaWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

The `atmOmegaWallFunction` is a boundary condition that provides
a wall constraint on the specific dissipation rate (i.e. `omega`) and
the turbulent kinetic energy production contribution (i.e. `G`) for
atmospheric boundary layer modelling.

### References

- Theoretical expressions (tags:PGVB, B):

    Parente, A., Gorlé, C., Van Beeck, J., & Benocci, C. (2011).
    Improved k–ε model and wall function formulation
    for the RANS simulation of ABL flows.
    J. of wind engineering and industrial aerodynamics, 99(4), 267-278.
    DOI:10.1016/j.jweia.2010.12.017

    Bredberg, J. (2000).
    On the wall boundary condition for turbulence models.
    Chalmers University of Technology, Depart. of Thermo and Fluid Dyn.
    Internal Report 00/4. Sweden: Göteborg.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            atmOmegaWallFunction;
        z0              <PatchFunction1<scalar>>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "atmOmegaWallFunctionFvPatchScalarField.H", "atmOmegaWallFunction" %>
<% assert :string_exists, "atmOmegaWallFunctionFvPatchScalarField.C", "z0" %>

where:

Property | Description                       | Type    | Required | Default
---------|-----------------------------------|---------|----------|---------
`type`   | Type name: `atmOmegaWallFunction` | word    | yes      | -
`z0`     | Surface roughness length \[m\]    | PatchFunction1\<scalar\> | yes | -

The inherited entries are elaborated in:

- omegaWallFunctionFvPatchScalarField.H
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmFlatTerrain" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/wallFunctions/atmOmegaWallFunction" %>

API:

- [Foam::atmOmegaWallFunctionFvPatchScalarField](doxy_id://Foam::atmOmegaWallFunctionFvPatchScalarField)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
