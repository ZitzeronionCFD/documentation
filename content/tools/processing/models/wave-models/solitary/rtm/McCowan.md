---
title: McCowan
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- McCowan
- regular
- waves
menu_id: waves-regular-mccowan
license: CC-BY-NC-ND-4.0
menu_parent: models-waves
group: wave-solitary
---

<%= page_toc %>

## Properties {#properties}

- Solitary wave

## Model equations {#model-equations}

The wave height is modelled by the equation:

$$
    \eta =
        \frac{a}{\tan (0.5 m (a + h))}
        \frac{\sin (m z)}{\cos (m z) + \cosh (m x) }
$$

Where:

$$h$$
: water depth

$$a$$
: wave amplitude

$$m$$
: constant coefficient

## Default model coefficients {#default-coeffs}

## Usage {#usage}

Inlet patch example

    <patch>
    {
        alpha           alpha.water;
        waveModel       McCowan;
        nPaddle         1;
        waveHeight      0.05;
        waveAngle       0.0;
        activeAbsorption yes;
    }

## Further information {#further-information}

Source code:

- [Foam::waveModels::McCowan](doxy_id://Foam::waveModels::McCowan)

References:

- ...

Tutorials:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/solitaryMcCowan" %>
