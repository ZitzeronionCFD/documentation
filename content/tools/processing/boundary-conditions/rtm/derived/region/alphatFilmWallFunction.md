---
title: alphatFilmWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-alphatFilmWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-region
group: bcs-region
---

<%= page_toc %>

## Description

The `alphatFilmWallFunction` is boundary condition that provides
a turbulent thermal diffusivity condition
when using wall functions, for use with surface film models. This
condition varies from the standard wall function by taking into account any
mass released from the film model.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            alphatFilmWallFunction;

        // Optional entries
        filmRegion      <word>;
        B               <scalar>;
        yPlusCrit       <scalar>;
        Cmu             <scalar>;
        kappa           <scalar>;
        Prt             <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "alphatFilmWallFunctionFvPatchScalarField.H", "alphatFilmWallFunction" %>
<% assert :string_exists, "alphatFilmWallFunctionFvPatchScalarField.C", "B" %>
<% assert :string_exists, "alphatFilmWallFunctionFvPatchScalarField.C", "yPlusCrit" %>
<% assert :string_exists, "alphatFilmWallFunctionFvPatchScalarField.C", "Cmu" %>
<% assert :string_exists, "alphatFilmWallFunctionFvPatchScalarField.C", "kappa" %>
<% assert :string_exists, "alphatFilmWallFunctionFvPatchScalarField.C", "Prt" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `alphatFilmWallFunction`        | word    | yes      | -
`B`            | B coefficient                              | scalar  | no       | 5.5
`yPlusCrit`    | y+ value for laminar -> turbulent transition | scalar  | no       | 11.05
`Cmu`          | Empirical model coefficient                | scalar  | no       | 0.09
`kappa`        | von Karman constant                        | scalar  | no       | 0.41
`Prt`          | Turbulent Prandtl number                   | scalar  | no       | 0.85

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/reactingParcelFoam/hotBoxes" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/regionModels/surfaceFilmModels/derivedFvPatchFields/wallFunctions/alphatFilmWallFunction" %>

API:

- [Foam::compressible::RASModels::alphatFilmWallFunctionFvPatchScalarField](doxy_id://Foam::compressible::RASModels::alphatFilmWallFunctionFvPatchScalarField)

<%= history "2.0.0" %>

<!----------------------------------------------------------------------------->
