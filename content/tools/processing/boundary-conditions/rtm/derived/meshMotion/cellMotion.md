---
title: cellMotion
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-cellMotion
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-meshMotion
group: bcs-meshMotion
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            cellMotion;

        // Inherited entries
        ...
    }

<% assert :string_exists, "cellMotionFvPatchField.H", "cellMotion" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `cellMotion`                    | word    | yes      | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/sonicDyMFoam/movingCone" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/fvMotionSolver/fvPatchFields/derived/cellMotion" %>

API:

- [Foam::cellMotionFvPatchField](doxy_id://Foam::cellMotionFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
