---
title: Limited surface-normal gradient scheme
short_title: Limited
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- gradient
menu_id: schemes-sngrad-limited
license: CC-BY-NC-ND-4.0
menu_parent: schemes-sngrad
group: schemes-sngrad
---

<%= page_toc %>

## Properties {#properties}

- snGrad scheme with limited non-orthogonal correction
- delta coefficients given by: nonOrthDeltaCoeffs
- explicit non-orthogonal correction

## Usage {#usage}

The scheme is specified using:

    snGradSchemes
    {
        default         none;
        grad(U)         limited <corrected scheme> <psi>;
    }

`psi` coefficient between 0 and 1

- 0: no correction, equivalent to the [uncorrected](ref_id://schemes-sngrad) scheme
- 1: full correction applied
- 0.5: non-orthogonal contribution does not exceed the orthogonal part

## Further information {#further-information}

Source code

- [Foam::fv::limitedSnGrad](doxy_id://Foam::fv::limitedSnGrad)
