---
title: Discrete methods
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
menu_id: solvers-discrete-methods
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

## Solvers {#solvers}

- mdFoam
- dsmcFoam

<!-- <%= insert_models "solvers-discrete-methods" %> -->

## Further information {#further-information}

- [Source documentation](doxy_id://grpDiscreteMethodsSolvers)
