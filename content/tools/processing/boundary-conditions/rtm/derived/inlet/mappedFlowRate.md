---
title: mappedFlowRate
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-mappedFlowRate
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `mappedFlowRate` is a velocity boundary condition that describes a
volumetric/mass flow normal vector boundary condition by its magnitude
as an integral over its area.

The inlet mass flux is taken from the neighbour region.

The basis of the patch (volumetric or mass) is determined by the
dimensions of the flux, `phi`.  The current density is used to correct the
velocity when applying the mass basis.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            mappedPatch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                mappedFlowRate;

        // Optional entries
        nbrPhi              <word>;
        phi                 <word>;
        rho                 <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "mappedFlowRateFvPatchVectorField.H", "mappedFlowRate" %>
<% assert :string_exists, "mappedFlowRateFvPatchVectorField.C", "nbrPhi" %>
<% assert :string_exists, "mappedFlowRateFvPatchVectorField.C", "phi" %>
<% assert :string_exists, "mappedFlowRateFvPatchVectorField.C", "rho" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `mappedFlowRate`                | word    | yes      | -
`nbrPhi`               | Name of the neighbour flux setting the inlet mass flux | word   | no | phi
`phi`                  | Name of the local mass flux                | word      | no     | phi
`rho`                  | Name of the density field used to normalize the mass flux | word  | no | rho

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/fireFoam/LES/flameSpreadWaterSuppressionPanel" %>
- <%= repo_link2 "$FOAM_TUTORIALS/combustion/fireFoam/LES/oppositeBurningPanels" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/mappedFlowRate" %>

API:

- [Foam::mappedFlowRateFvPatchVectorField](doxy_id://Foam::mappedFlowRateFvPatchVectorField)

<%= history "2.1.0" %>

<!----------------------------------------------------------------------------->
