---
title: Implementation details
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- implementation
menu_id: schemes-divergence-implementationdetails
license: CC-BY-NC-ND-4.0
menu_parent: schemes-divergence
---

<%= page_toc %>

For a cell with volume $$V$$, Gauss' theorem is applied to give:
$$
    \div \u
  = \frac{1}{V}\int_V \left( \div \u \right) dV
  = \frac{1}{V} \oint_S \left( \vec{n} \dprod \u \right) d\vec{S}
  = \frac{1}{V} \sum_i^{\mathrm{nFace}}\u_{f,i} \dprod \vec{S}_{f,i}
$$
i.e. the divergence is given by the sum over all faces of the dot product of
the face normal with the vector quantity at the face.  To transform the quantity
from the cell centre to the face centre, an
[interpolation scheme](ref_id://schemes-interpolation) is required.

The divergence term contributions enter the system of equations via the owner
and neighbour cells (see gaussConvectionScheme.C)

    fvm.lower() = -weights.internalField()*faceFlux.internalField();
    fvm.upper() = fvm.lower() + faceFlux.internalField();
    fvm.negSumDiag();

The overall weights $$w$$ are derived from the selected scheme weights,
$$\Psi$$, and the linear $$w_l$$ interpolation weights, such that for a
positive flux:
$$
    w = w_l \Psi + (1 - \Psi)_u
$$
where the subscript $$()_u$$ denotes the upwind contribution.  The face
values are then calculated as:
$$
    \phi_f = \phi_N + w \left(\phi_P - \phi_N \right)
$$

Source code:

- [Foam::limitedSurfaceInterpolationScheme](doxy_id://Foam::limitedSurfaceInterpolationScheme)
