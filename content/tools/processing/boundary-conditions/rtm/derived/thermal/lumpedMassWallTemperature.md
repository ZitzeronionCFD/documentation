---
title: lumpedMassWallTemperature
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-lumpedMassWallTemperature
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `lumpedMassWallTemperature` is a boundary condition that provides
a lumped mass model for temperature.

It considers a single temperature value for the whole patch and evaluates
the temperature evolution using the net heat flux into the patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            lumpedMassWallTemperature;
        Cp              <scalar>;
        mass            <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "lumpedMassWallTemperatureFvPatchScalarField.H", "lumpedMassWallTemperature" %>
<% assert :string_exists, "lumpedMassWallTemperatureFvPatchScalarField.C", "Cp" %>
<% assert :string_exists, "lumpedMassWallTemperatureFvPatchScalarField.C", "mass" %>

where:

Property | Description                            | Type    | Required | Default
---------|----------------------------------------|---------|----------|---------
`type`   | Type name: `lumpedMassWallTemperature` | word    | yes      | -
`Cp`     | Heat capacity \[J/kg/K\]               | scalar  | yes      | -
`mass`   | Total mass \[kg\]                      | scalar  | yes      | -

The inherited entries are elaborated in:

- mixedFvPatchFields.H
- temperatureCoupledBase.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantPimpleFoam/hotRoom" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/lumpedMassWallTemperature" %>

API:

- [Foam::lumpedMassWallTemperatureFvPatchScalarField](doxy_id://Foam::lumpedMassWallTemperatureFvPatchScalarField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
