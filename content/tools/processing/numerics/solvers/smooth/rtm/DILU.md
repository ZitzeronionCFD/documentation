---
title: DILU Smoother
short_title: DILU
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- solvers
- DILU
menu_id: smooth-dilu
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-smooth
group: solvers-smooth-smoothers
---

<%= page_toc %>

## Properties {#properties}

- Simplified <B>D</B>iagonal-based <B>I</B>ncomplete <B>LU</B> smoother
- Asymmetric matrices

## Usage {#usage}

    smoother        DILU;

## Further information {#further-information}

Source code:

- [Foam::DILUSmoother](doxy_id://Foam::DILUSmoother)
