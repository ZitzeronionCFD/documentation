---
title: particleDistribution
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- distribution
- field
- function object
- particle
- post-processing
menu_id: function-objects-field-particledistribution
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `particleDistribution` function object
generates a particle distribution for lagrangian data at a given time.

### Operands

Operand      | Type       | Location
:---         |:---        |:---
input        | -          | -
output file  | dat        | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
output field | -          | -

## Usage {#usage}

Example of the `particleDistribution` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    particleDistribution1
    {
        // Mandatory entries (unmodifiable)
        type        particleDistribution;
        libs        (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        cloud       <cloudName>;
        nameVsBinWidth
        (
            (d 0.1)
            (U 10)
        );
        setFormat   raw;

        // Optional entries (runtime modifiable)
        tagField    none;

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "particleDistribution.H", "particleDistribution" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "particleDistribution",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "particleDistribution.C", "cloud" %>
<% assert :string_exists, "particleDistribution.C", "nameVsBinWidth" %>
<% assert :string_exists, "particleDistribution.C", "format" %>
<% assert :string_exists, "particleDistribution.C", "tagField" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: particleDistribution     | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 cloud        | Name of cloud to process            | word | yes      | -
 nameVsBinWidth | List of cloud field-bin width     | wordHashTable | yes | -
 setFormat    | Output format                       | word | yes      | -
 tagField     | Name of cloud field to use group particles | word | no  | none

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/sprayFoam/aachenBomb" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/particleDistribution" %>

API:

- [Foam::functionObjects::particleDistribution](doxy_id://Foam::functionObjects::particleDistribution)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
