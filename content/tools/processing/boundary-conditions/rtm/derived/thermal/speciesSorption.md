---
title: speciesSorption
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-speciesSorption
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `speciesSorption` is a boundary condition that provides
a first-order zero-gradient condition for a given scalar field to
model time-dependent adsorption-desorption processes.

$$
    \frac{d c}{d t} = k_{ads} (c_{eq} - c_{abs})
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$c_{eq}$$            | Equilibrium concentration
$$c_{abs}$$           | Absorbed at wall
$$k_{ads}$$           | Adsorption rate constant \[1/s\]

$$
    c_{eq} = c_{max} \frac{k_l \, c_{int}}{1 + k_l \, c_{int}}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$c_{max}$$           | Maximum concentration
$$k_l$$               | Langmuir constant
$$c_{int}$$           | Local cell value concentration

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    speciesSorption;
        equilibriumModel        <word>;
        kinematicModel          <word>;
        kabs                    <scalar>;
        kl                      <scalar>;
        max                     <scalar>;
        thickness               <PatchFunction1<scalar>>;
        rhoS                    <scalar>;

        // Optional entries
        dfldp                   <scalarField>;
        mass                    <scalarField>;
        pName                   <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "speciesSorptionFvPatchScalarField.H", "speciesSorption" %>
<% assert :string_exists, "speciesSorptionFvPatchScalarField.C", "equilibriumModel" %>
<% assert :string_exists, "speciesSorptionFvPatchScalarField.C", "kinematicModel" %>
<% assert :string_exists, "speciesSorptionFvPatchScalarField.C", "kabs" %>
<% assert :string_exists, "speciesSorptionFvPatchScalarField.C", "kl" %>
<% assert :string_exists, "speciesSorptionFvPatchScalarField.C", "max" %>
<% assert :string_exists, "speciesSorptionFvPatchScalarField.C", "thickness" %>
<% assert :string_exists, "speciesSorptionFvPatchScalarField.C", "rhoS" %>
<% assert :string_exists, "speciesSorptionFvPatchScalarField.C", "dfldp" %>
<% assert :string_exists, "speciesSorptionFvPatchScalarField.C", "mass" %>
<% assert :string_exists, "speciesSorptionFvPatchScalarField.C", "pName" %>

where:

Property           | Description                                | Type    | Required | Default
-------------------|--------------------------------------------|---------|----------|---------
`type`             | Type name: `speciesSorption`               | word    | yes      | -
`equilibriumModel` | Equilibrium model                          | word    | yes      | -
`kinematicModel`   | Kinematic model                            | word    | yes      | -
`kabs`             | Adsorption rate constant \[1/s\]           | scalar  | yes      | -
`kl`               | Langmuir constant \[1/Pa\]                 | scalar  | yes      | -
`max`              | Maximum concentation at wall \[mol/kg\]    | scalar  | yes      | -
`thickness`        | Solid thickness along the patch            | PatchFunction1\<scalar\> | yes | -
`rhoS`             | Solid density                              | scalar  | yes      | -
`dfldp`            | Source on cells next to patch              | scalarField | no   | Zero
`mass`             | Absorbed mass per kg of absorbent \[mol/kg\] | scalarField | no   | Zero
`pName`            | Name of operand pressure field             | word    | no       | p

Options for the `equilibriumModel` entry:

Property           | Description
-------------------|--------------------------------------------
`Langmuir`         | Langmuir model

Options for the `kinematicModel` entry:

Property              | Description
----------------------|--------------------------------------------
`PseudoFirstOrder`    | Pseudo first-order model

The inherited entries are elaborated in:

- zeroGradientFvPatchFields.H
- PatchFunction1.H
- boundarySourcePatch.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/rhoReactingFoam/groundAbsorption" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/reactionThermo/derivedFvPatchFields/speciesSorption" %>

API:

- [Foam::speciesSorptionFvPatchScalarField](doxy_id://Foam::speciesSorptionFvPatchScalarField)

<%= history "v2112" %>

<!----------------------------------------------------------------------------->
