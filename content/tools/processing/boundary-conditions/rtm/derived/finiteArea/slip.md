---
title: slip
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-slip
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-fa
group: bcs-fa
---

<%= page_toc %>

## Description

The `slip` is a boundary condition that provides the slip constraint.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            slip;

        // Inherited entries
        ...
    }

<% assert :string_exists, "slipFaPatchField.H", "slip" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `slip`                          | word    | yes      | -

The inherited entries are elaborated in:

- basicSymmetryFaPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/finiteArea/liquidFilmFoam/cylinder" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteArea/fields/faPatchFields/derived/slip" %>

API:

- [Foam::slipFaPatchField](doxy_id://Foam::slipFaPatchField)

<%= history "v1712" %>

<!----------------------------------------------------------------------------->
