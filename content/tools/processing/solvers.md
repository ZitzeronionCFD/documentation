---
title: Solver Applications
tags:
- solvers
copyright:
- Copyright (C) OpenCFD Ltd.
menu_parent: processing
menu_id: applications-solvers
license: CC-BY-NC-ND-4.0
---

<%= page_toc %>

## Flow {#flow}

- <%= link_to_menuid "solvers-basic" %>
- <%= link_to_menuid "solvers-incompressible" %>
- <%= link_to_menuid "solvers-compressible" %>
- <%= link_to_menuid "solvers-heat-transfer" %>
- <%= link_to_menuid "solvers-multiphase" %>
- <%= link_to_menuid "solvers-lagrangian-particle" %>
- <%= link_to_menuid "solvers-discrete-methods" %>
- <%= link_to_menuid "solvers-combustion" %>
- <%= link_to_menuid "solvers-dns" %>

## Other {#other}

- <%= link_to_menuid "solvers-electro-magnetics" %>
- <%= link_to_menuid "solvers-financial" %>
- <%= link_to_menuid "solvers-stress-analysis" %>

## Common variable transformations {#common-variable-transformations}

Many OpenFOAM solver applications employ common variable transformations,
including:

- [kinematic pressure](ref_id://algorithm-kinematic-pressure) for
  incompressible solvers
- [hydrostatic pressure](ref_id://algorithm-p-rgh) effects

## Pressure-velocity coupling {#pressure-velocity}

- Introduction: <%= link_to_menuid "pressure-velocity" %>
- Steady state: [SIMPLE](ref_id://pv-simple)
- Transient: [PISO](ref_id://pv-piso)
- Transient: [PIMPLE](ref_id://pv-pimple)

## Capability matrix {#capability-matrix}

<% str = +"|Solver| "%>
<% str += @config[:solvers][:models].join("|") + "|\n" %>
<% str += "|-|" + @config[:solvers][:models].join("|").gsub(/[a-zA-Z_ \-]+/,"-") + "|\n" %>
<% @config[:solvers][:ids].each do |id| %>
  <% str += "|" + link_to_menuid(@items[id][:menu_id]) + "|" %>
  <% @config[:solvers][:models].each do |model| %>
    <% if @items[id][:solver_models].include? model %>
      <% str += "x|" %>
    <% else %>
      <% str += "|" %>
    <% end %>
  <% end %>
  <% str += "\n" %>
<% end %>
<% str += "{: .table .proptable}" %>
<%= str %>
