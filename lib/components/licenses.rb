# frozen_string_literal: true

require 'yaml'

module Utils
  class Licenses
    # attr_reader :licenses

    def initialize()
      file = "#{ENV['PWD']}/assets/licenses/licenses.json"
      @licenses = YAML.load_file(file)
      @licenses.transform_keys!(&:downcase)
    end

    def html(license)
      details = @licenses[license.downcase]
      if details.nil?
        puts "ERROR: Unknown SPLDX license identifier #{license.downcase}"
        puts "Available licenses: #{@licenses.keys}"
        puts "To add a new license, edit /assets/licenses/licenses.json"
        raise StandardError
      end
      ops = 'target="_blank" rel="nofollow noreferrer noopener"'
      out = +'<div class="license-container">'
      out << "<img class=\"no-resize license-description\"src=\"#{details["img"]}\" alt=\"#{details["img_alt"]}\">"
      out << '<p class="license-description">'
      out << "This work is licensed under a <a href=#{details["url"]} #{ops}>#{details["description"]}</a>"
      out << '</p>'
      out << '</div>'
      out
    end
  end
end
