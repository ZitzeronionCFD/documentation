---
title: SRFVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-SRFVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
groups:
- bcs-inlet
---

<%= page_toc %>

## Description

The `SRFVelocity` is a boundary condition that provides
a velocity condition to be used in conjunction with the single
rotating frame (SRF) model (see: `SRFModel` class).

Given the free stream velocity in the absolute frame, the condition
applies the appropriate rotation transformation in time and space to
determine the local velocity.

The optional `relative` flag switches the behaviour of the patch
such that:

- `relative` = yes: inlet velocity applied 'as is':

$$
    \u_p = \u_{in}
$$

- `relative` = no : SRF velocity is subtracted from the inlet velocity:

$$
    \u_p = \u_{in} - \u_{p,srf}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\u_p$$              | Patch velocity \[m/s\]
$$\u_{in}$$           | User-specified inlet velocity
$$\u_{p,srf}$$        | SRF velocity

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            SRFVelocity;
        inletValue      <vectorField>;
        relative        <bool>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "SRFVelocityFvPatchVectorField.H", "SRFVelocity" %>
<% assert :string_exists, "SRFVelocityFvPatchVectorField.C", "inletValue" %>
<% assert :string_exists, "SRFVelocityFvPatchVectorField.C", "relative" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `SRFVelocity`                   | word    | yes      | -
`inletValue`   | Inlet velocity                             | vectorField | yes  | -
`relative`     | Flag to decide if the supplied inlet value relative to the SRF | bool | yes | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/SRFSimpleFoam/mixer" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/cfdTools/general/SRF/derivedFvPatchFields/SRFVelocityFvPatchVectorField" %>

API:

- [Foam::SRFVelocityFvPatchVectorField](doxy_id://Foam::SRFVelocityFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
