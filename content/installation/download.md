---
title: Download
copyright:
- Copyright (C) 2021-2023 OpenCFD Ltd.
tags:
- download
- installation
menu_id: download
license: CC-BY-NC-ND-4.0
menu_parent: installation
menu_weight: 20
---

The code is released in multiple formats:

- [source code](repo_base://)
- [binaries](repo_base://-/wikis/precompiled) for Windows, Mac, Linux and containers

For the latest release, please see [www.openfoam.com/download/](https://www.openfoam.com/download/).

