---
title: Interpolation schemes
short_title: Interpolation
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- interpolation
menu_id: schemes-interpolation
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes
---

<%= page_toc %>

Interpolation schemes are used to transform cell-centre quantities to face
centres.  The operation is used in many of the finite volume calculations, e.g.
for the calculation of [gradient](ref_id://schemes-gradient),
[divergence](ref_id://schemes-divergence), and
[Laplacian](ref_id://schemes-laplacian) terms.

## Usage {#usage}

Interpolation schemes are specified in the
<%= link_to_menuid "fvschemes" %>file under the `interpolationSchemes`
sub-dictionary using the syntax:

    interpolationSchemes
    {
        default         none;
        <equation term> <interpolation scheme>;
    }

## Options {#options}

A wide variety of interpolation schemes are available, ranging from those that
are based solely on geometry, and others, e.g. convection schemes that are
functions of the local flow.

General

- [Linear](ref_id://schemes-divergence-linear)
- [Point linear](ref_id://schemes-interpolation-point-linear)

Convection

- See [divergence schemes](ref_id://schemes-divergence)

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/surfaceInterpolationScheme" %>

API:

- [Foam::surfaceInterpolationScheme](doxy_id://Foam::surfaceInterpolationScheme)
