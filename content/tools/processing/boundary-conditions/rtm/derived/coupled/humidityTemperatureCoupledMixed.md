---
title: humidityTemperatureCoupledMixed
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
- coupled
menu_id: boundary-conditions-humidityTemperatureCoupledMixed
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-coupled
group: bcs-coupled-jump
---

<%= page_toc %>

## Description

The `humidityTemperatureCoupledMixed` is a mixed coupled boundary condition for
temperature to be used at the coupling interface between fluid and solid regions.

Various governing equations of this boundary condition are based on
Bergman et al.<%= cite "Bergman_2011" %>.

The condition assumes a drop-wise type of condensation, whereby its heat
transfer Nusselt number is calculated using:

$$
    Nu = 51104 + 2044 (T - 273.15) \,  \text{if} \, \, T > 295 \, \, \& \, \, T < 373
$$

$$
    Nu = 255510                    \, \, \text{if} \, \, T > 373
$$

The mass transfer correlation used is:

$$
    h_m
  =
    D_{ab} \frac{Sh}{L}
$$

The Sherwood number is calculated using:

$$
    Sh = 0.664 Re^\frac{1}{2} Sc^\frac{1}{3} \, \, \text{if} \, \, Re < 5.0E+05
$$

$$
    Sh = 0.037 Re^\frac{4}{5} Sc^\frac{1}{3} \, \, \text{if} \, \, Re > 5.0E+05
$$

where:

Property             | Description
---------------------|-------
$$D_{ab}$$           | Mass vapour difussivity
$$L$$                | Characteristic length
$$Sh$$               | Sherwood number
$$Re$$               | Reynolds number
$$Sc$$               | Schmidt number
$$Nu$$               | Nusselt number

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

On the fluid side:

    <patchName>
    {
        // Mandatory entries
        type            humidityTemperatureCoupledMixed;
        mode            <word>;

        // Optional entries
        p               <word>;
        U               <word>;
        rho             <word>;
        mu              <word>;
        Tnbr            <word>;
        qrNbr           <word>;
        qr              <word>;
        specie          <word>;
        thicknessLayers <scalarList>;

        // Conditional entries

            // when 'thicknessLayers' entry is present
            kappaLayers         <scalarList>;

            // when 'mode' == 'constantMass'
            thickness           <scalarField>;
            cp                  <scalarField>;
            rho                 <scalarField>;

            // when 'mode' != 'constantMass'
            carrierMolWeight    <scalar>;
            L                   <scalar>;
            Tvap                <scalar>;
            liquid              <dict>;
            thickness           <scalarField>;

        // Inherited entries
        ...
    }

On the solid side:

    <patchName>
    {
        // Mandatory entries
        type            humidityTemperatureCoupledMixed;

        // Optional entries
        p               <word>;
        U               <word>;
        rho             <word>;
        mu              <word>;
        Tnbr            <word>;
        qrNbr           <word>;
        qr              <word>;
        specie          <word>;
        thicknessLayers <scalarList>;

        // Conditional entries

            // when 'thicknessLayers' entry is present
            kappaLayers         <scalarList>;

        // Inherited entries
        ...
    }

where:

Property              | Description                                | Type    | Required | Default
----------------------|--------------------------------------------|---------|----------|---------
`type`                | Type name: `humidityTemperatureCoupledMixed` | word  | yes      | -
`mode`                | Operation mode                             | word    | yes      | -
`p`                   | Name of pressure field                     | word    | no       | `p`
`U`                   | Name of velocity field                     | word    | no       | `U`
`rho`                 | Name of density field                      | word    | no       | `rho`
`mu`                  | Name of dynamic viscosity field            | word    | no       | `thermo:mu`
`Tnbr`                | Name of neighbour temperature field        | word    | no       | `T`
`qrNbr`               | Name of neighbour radiative heat flux field | word   | no       | `none`
`qr`                  | Name of radiative heat flux field          | word    | no       | `none`
`specie`              | Name of specie field                       | word    | no       | `none`
`thicknessLayers`     | List of kappa-layer thicknesses            | scalarList | no       | -
`kappaLayers`         | List of kappas corresponding to thicknesses | scalarList | conditional | -
`thickness`           | Thickness field                         | scalarField | conditional | -
`cp`                  | Specific heat capacity field            | scalarField | conditional | -
`rho`                 | Density field                           | scalarField | conditional | -
`carrierMolWeight`    | Carrier molecular weight                | scalar | conditional | -
`L`                   | Characteristic length of the wall       | scalar | conditional | -
`Tvap`                | Vaporisation temperature                | scalar | conditional | -
`liquid`              | Liquid properties                       | dict   | conditional | -

The inherited entries are elaborated in:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
- [Foam::temperatureCoupledBase](doxy_id://Foam::temperatureCoupledBase)
- [Foam::liquidProperties](doxy_id://Foam::liquidProperties)
<!-- end of the list -->

Options for the `mode` entry:

Property              | Description
----------------------|--------------------------------------------
`constantMass`        | Thermal inertia only
`condensation`        | Condensation only
`evaporation`         | Evaporation only
`condensationAndEvaporation` | Simultaneous condensation and evaporation

- The correlation used to calculate `Tdew` is for water vapour.
- A scalar transport equation for the carrier specie is required, e.g.
  supplied via a function object or in the main solver. This specie
  transports the vapour phase in the main ragion.
- The boundary condition of this specie on the coupled wall must be
  `fixedGradient` in order to allow condensation or evaporation of the
  vapour in or out of this wall.
- There is no mass flow on the wall, i.e. the mass condensed on a face
  remains on that face. It uses a 'lumped mass' model to include thermal
  inertia effects.
- With `mode==condensation`, when the wall temperature (Tw) is below the dew
  temperature (Tdew) condesation takes place and the resulting condensed mass
  is stored on the wall.
- With `mode==evaporation`, initial mass is vaporized when the wall temperature
  (Tw) is above the input vaporization temperature (Tvap).
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/chtMultiRegionFoam/windshieldCondensation" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/humidityTemperatureCoupledMixed" %>

API:

- [Foam::compressible::humidityTemperatureCoupledMixedFvPatchScalarField](doxy_id://Foam::compressible::humidityTemperatureCoupledMixedFvPatchScalarField)

<%= history "v1706" %>

<!----------------------------------------------------------------------------->
