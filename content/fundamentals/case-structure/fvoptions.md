---
title: fvOptions
copyright:
- Copyright (C) 2021 OpenCFD Ltd.
tags:
- navigation
- system
- fvOption
menu_id: fvoptions
license: CC-BY-NC-ND-4.0
menu_parent: case-structure
---

<%= page_toc %>

Many OpenFOAM applications contain equation systems that can be manipulated at
run time via user-specified finite volume options, given by the shorthand
`fvOptions`.  These provide, e.g. additional source/sink terms, or enforce
constraints.

Options include:

- <%= link_to_menuid "numerics-fvoptions-sources" %>
- <%= link_to_menuid "numerics-fvoptions-corrections" %>
- <%= link_to_menuid "numerics-fvoptions-constraints" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/fvOptions" %>

API:

- [grpFvOptions](doxy_id://grpFvOptions)
