---
title: specularRadiation
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-specularRadiation
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `specularRadiation` is a mixed boundary condition that
provides a specular radiation condition for
axisymmetric and symmetry-plane `fvDOM` computations.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            specularRadiation;

        // Optional entries
        interpolate         <bool>;

        // Inherited entries
        patchType           <word>;
        ...
    }

<% assert :string_exists, "specularRadiationMixedFvPatchScalarField.H", "specularRadiation" %>
<% assert :string_exists, "specularRadiationMixedFvPatchScalarField.C", "interpolate" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `specularRadiation`             | word    | yes      | -
`interpolate`          | Flag to enable ray-intensity interp        | bool    | no       | false

The inherited entries are elaborated in:

- mixedFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/radiation/derivedFvPatchFields/specularRadiation" %>

API:

- [Foam::radiation::specularRadiationMixedFvPatchScalarField](doxy_id://Foam::radiation::specularRadiationMixedFvPatchScalarField)

<%= history "v2306" %>

<!----------------------------------------------------------------------------->
