---
title: Equation of state
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- EoS
menu_id: thermophysical-equationOfState
license: CC-BY-NC-ND-4.0
menu_parent: models-thermophysical
---

<%= page_toc %>

## Options {#options}

Equation of state model selections include:

<%= insert_models "equation-of-state-models" %>

## Usage {#usage}

The equation of state type is specified in the `thermoType` dictionary:

~~~
thermoType
{
    equationOfState     <model>;
    <...>
}
~~~

## Further information {#further-information}

Source code

- [grpSpecieEquationOfState](doxy_id://grpSpecieEquationOfState)
