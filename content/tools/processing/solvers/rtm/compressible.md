---
title: Compressible
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
menu_id: solvers-compressible
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

## Solvers {#solvers}

<%= insert_models "solvers-compressible" %>

## Common options {#common-options}

### Transonic {#common-options-transonic}

In the <%= link_to_menuid "fvsolution" %>

~~~
SIMPLE
{
    transonic       yes;
}
~~~

or

~~~
PIMPLE
{
    transonic       yes;
}
~~~

<!--
From the semi-discretised form of the momentum equation:
$$
    \matrix{\u} = A \u - H = -grad p,
$$
giving
$$
    \u = \frac{H}{A} - \frac{1}{A} \grad p.
$$

This is substituted into the continuity equation
$$
    \ddt{\rho} + \div (\rho_f \u_f) = 0
$$

to yield:
$$
    \ddt{\rho} + \div (\rho_f (\frac{H}{A})_f ) - \div ((\frac{rho}{A})_f \grad p).
$$

For compressible flows
$$
    \rho = \psi p,
$$
leading to:
$$
    \ddt{\psi p} + \div (\psi_f (\frac{H}{A})_f p) - \div ((\frac{\rho}{A})_f grad p)
$$

For constant density flows this can be simplified to:
$$
    \div ((\frac{1}{A})_f \grad p) = \div (\rho_f (\frac{H}{A})_f)
$$
-->

## Further information {#further-information}

- [Source documentation](doxy_id://grpIncompressibleSolvers)
