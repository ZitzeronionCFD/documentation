---
title: setTurbulenceFields
tags:
- boundary layer
- initialisation
- turbulence
- wall
copyright:
- Copyright 2023 (C) OpenCFD Ltd.
menu_id: setTurbulenceFields
license: CC-BY-NC-ND-4.0
menu_parent: pre-processing-field
group: pre-processing-field
---

<%= page_toc %>

## Overview {#overview}

The `setTurbulenceFields` utility applies a two-step automatic initialisation
procedure for [RAS](ref_id://turbulence-ras) computations based on the work of
Manceau<%= cite "manceau_two_yyyy" %> to address slow or non-convergence
attributed in the first O(10) time steps and improve resulting predictions.

## Usage

### Synopsis

<%= help_util_usage "setTurbulenceFields" %>

#### Examples

~~~
setTurbulenceFields -region region1
~~~


### Input

#### Arguments

<%= help_util_arguments "setTurbulenceFields" %>

#### Options

<%= help_util_options "setTurbulenceFields" %>

#### Files

The `setTurbulenceFields` utility is configured using a
`<case>/system/setTurbulenceFieldsDict`, taking the form:

~~~
// Mandatory entries
uRef            17.55;


// Optional entries
initialiseU     true;
initialiseEpsilon true;
initialiseK     true;
initialiseOmega true;
initialiseR     true;
writeF          true;

kappa           0.41;
Cmu             0.09;
dPlusRef        15.0;

f               f;
U               U;
epsilon         epsilon;
k               k;
omega           omega;
R               R;
~~~

Property              | Description                                | Type | Required  | Default
----------------------|--------------------------------------------|---------|--------|--------
`uRef`      | Reference speed              | scalar | yes  | -
`initialiseU` | Flag to initialise U         | bool | no   | false
`initialiseEpsilon` | Flag to initialise epsilon    | bool | no   | false
`initialiseK` | Flag to initialise k         | bool | no   | false
`initialiseOmega` | Flag to initialise omega | bool | no   | false
`initialiseR` | Flag to initialise R         | bool | no   | false
`writeF` | Flag to write elliptic-blending field, f | bool | no   | false
`kappa`  | von Karman constant             | scalar | no   | 0.41
`Cmu`    | Empirical constant              | scalar | no   | 0.09
`dPlusRef` | Reference dPlus               | scalar | no   | 15
`f`      | Name of operand f field         | word   | no   | f
`U`      | Name of operand U field         | word   | no   | U
`epsilon`  | Name of operand epsilon field | word   | no   | epsilon
`k`      | Name of operand k field         | word   | no   | k
`omega`      | Name of operand omega field | word   | no   | omega
`R`      | Name of operand R field         | word   | no   | R

- Time that the utility applies to is determined by the
  `startFrom` and `startTime` entries of the `controlDict`.
- The utility modifies near-wall fields, hence can be more effective
  for low-Re mesh cases.
{: .note}


#### Fields

<%= no_input "field" %>

### Output

#### Logs

<%= no_output "log" %>

#### Files

<%= no_output "file" %>

#### Fields

Property  | Description                 | Path      | Type
----------|-----------------------------|-----------|---------
`U`       | Velocity                    | `<case>/<time>/`| volVectorField
`nut`     | Turbulence viscosity        | `<case>/<time>/`| volScalarField
`epsilon` | Turbulence kinetic energy dissipation rate | `<case>/<time>/`| volScalarField
`k`       | Turbulence kinetic energy   | `<case>/<time>/`| volScalarField
`omega`   | Specific dissipation rate   | `<case>/<time>/`| volScalarField
`R`       | Reynolds stress tensor      | `<case>/<time>/`| volSymmScalarField
`f`       | Elliptic blending factor    | `<case>/<time>/`| volScalarField

## Method

Equations 3, 6, 8-13 and page 2 of Manceau<%= cite "manceau_two_yyyy" %>.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/turbulenceModels/planeChannel" %>

Source code:

- <%= repo_link2 "$FOAM_UTILITIES/preProcessing/setTurbulenceFields" %>

<%= history "v2206" %>

<!----------------------------------------------------------------------------->