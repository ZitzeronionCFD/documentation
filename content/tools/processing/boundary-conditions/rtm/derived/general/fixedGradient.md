---
title: Fixed gradient
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-fixedGradient
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Properties {#properties}

- Neumann condition
- fixed normal-gradient
- implicit

Face values are evaluated according to:
$$
    \phi_f = \phi_c + \Delta \grad{\phi}_{\ref}
$$

where
$$\phi_f$$
: face value

$$\phi_c$$
: cell value

$$\grad(\phi)_{\ref}$$
: reference gradient

$$\Delta$$
: face-to-cell distance

## Usage {#usage}

    <patchName>
    {
        type        fixedGradient;
        gradient    <field value>;
    }

## Further information {#further-information}

Source code:

- [Foam::fixedGradientFvPatchField](doxy_id://Foam::fixedGradientFvPatchField)
