---
title: Residuals
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- residuals
- solvers
menu_id: linearequationsolvers-residuals
license: CC-BY-NC-ND-4.0
menu_parent: numerics-linearequationsolvers
---

<%= page_toc %>

## Tolerances {#tolerances}

Equation tolerances are described in terms of *absolute* and *relative*
quantities:

    tolerance       1e-6;
    relTol          0.1;

If the equation initial residual satisfies either of the specified values,
the system of equations are assumed solved and will not be evolved.

## Calculation  {#calculation}

The residual calculation is solver-specific.  However, the general approach
follows:

For a matrix system

$$
    \mat{A} \vec{x} = \vec{b},
$$

the residual is defined as

$$
    \vec{r} = \vec{b} - \mat{A} \vec{x}.
$$

We then apply residual scaling using the following normalisation procedure:

$$
    n = \sum \left(
        \mag{\mat{A}\vec{x} - \mat{A}\av{\vec{x}}}
        + \mag{\vec{b} - \mat{A}\av{\vec{x}}}
        \right)
$$

where $$ \av{\vec{x}} $$ is the average of the solution vector.  The
scaled residual is finally given by:

$$
    r = \frac{1}{n} \sum \mag{\vec{b} - \mat{A} \vec{x}}.
$$

This form leads to a normalised residual of 1 for uniform systems, i.e. where
$$ \vec{x} = \av{\vec{x}} $$.  However, this also shows that if the
initial solution changes, e.g. using non-uniform conditions, the normalisation
also changes, leading to a different convergence history.
