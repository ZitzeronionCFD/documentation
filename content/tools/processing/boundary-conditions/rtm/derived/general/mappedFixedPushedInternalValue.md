---
title: mappedFixedPushedInternalValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-mappedFixedPushedInternalValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `mappedFixedPushedInternalValue` is a generic boundary condition that maps
the boundary values of a neighbour patch field to the boundary and internal
cell values of the local patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            mappedPatch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    mappedFixedPushedInternalValue;

        // Inherited entries
        ...
    }

<% assert :string_exists, "mappedFixedPushedInternalValueFvPatchField.H", "mappedFixedPushedInternalValue" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `mappedFixedPushedInternalValue` | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::mappedFixedValueFvPatchField](doxy_id://Foam::mappedFixedValueFvPatchField)
<!-- end of the list -->

- This boundary condition can only be applied to patches that are of
  the `mappedPolyPatch` type.
  {: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/mappedFixedPushedInternalValue" %>

API:

- [Foam::mappedFixedPushedInternalValueFvPatchField](doxy_id://Foam::mappedFixedPushedInternalValueFvPatchField)

<%= history "2.1.0" %>

<!----------------------------------------------------------------------------->
