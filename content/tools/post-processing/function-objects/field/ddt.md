---
title: ddt
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- ddt
- derivative
- field
- function object
- post-processing
menu_id: function-objects-field-ddt
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `ddt` function object computes the Eulerian
time derivative of an input volume field for time-variant simulations
(not appropriate to steady-state simulations).

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `vol<Type>Field`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `vol<Type>Field`     | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `ddt` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    ddt1
    {
        // Mandatory entries (unmodifiable)
        type            ddt;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entries (runtime modifiable)
        field           <field>;

        // Optional (inherited) entries
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "ddt/ddt.H", "ddt" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "ddt",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: ddt                      | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 field        | Name of the operand field           | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func "ddt(<field>)"

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/ddt" %>

API:

- [Foam::functionObjects::ddt](doxy_id://Foam::functionObjects::ddt)

<%= history "v1712" %>

<!----------------------------------------------------------------------------->
