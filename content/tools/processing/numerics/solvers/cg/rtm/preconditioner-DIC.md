---
title: DIC preconditioner
short_title: DIC
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- DIC
- preconditioner
- solvers
menu_id: cg-preconditioner-dic
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-cg
group: solvers-cg-preconditioner
---

<%= page_toc %>

## Properties {#properties}

- Simplified <B>D</B>iagonal-based <B>I</B>ncomplete <B>C</B>holesky
  preconditioner (symmetric equivalent of
  [DILU](../preconditioner-dilu)).
- Symmetric matrices
- Parallel inconsistent

## Usage {#usage}

    preconditioner  DIC;

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/preconditioners/DICPreconditioner" %>

API:

- [Foam::DICPreconditioner](doxy_id://Foam::DICPreconditioner)
