---
title: kLowReWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-kLowReWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `kLowReWallFunction` is boundary condition that provides
a wall function for the turbulent kinetic
energy (i.e. `k`) for low- and high-Reynolds number simulations.

Viscous and inertial sublayer predictions for \c k are blended in
a stepwise manner:

$$
    k = k_{log} \qquad if \quad y^+ > y^+_{intersection}
$$

$$
    k = k_{vis} \qquad if \quad y^+ <= y^+_{intersection}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$k_{vis}$$           | k prediction in the viscous sublayer
$$k_{log}$$           | k prediction in the inertial sublayer
$$y^+$$               | Estimated wall-normal height of the cell centre in wall units
$$y^+_{intersection}$$ | Estimated $$$y^+$$ where sublayers intersect

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            kLowReWallFunction;

        // Optional entries
        Ceps2           <scalar>;
        Ck              <scalar>;
        Bk              <scalar>;
        C               <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "kLowReWallFunctionFvPatchScalarField.H", "kLowReWallFunction" %>
<% assert :string_exists, "kLowReWallFunctionFvPatchScalarField.C", "Ceps2" %>
<% assert :string_exists, "kLowReWallFunctionFvPatchScalarField.C", "Ck" %>
<% assert :string_exists, "kLowReWallFunctionFvPatchScalarField.C", "Bk" %>
<% assert :string_exists, "kLowReWallFunctionFvPatchScalarField.C", "C" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `kLowReWallFunction`            | word    | yes      | -
`Ceps2`        | Model coefficient                          | scalar  | no       |  1.9
`Ck`           | Model coefficient                          | scalar  | no       | -0.416
`Bk`           | Model coefficient                          | scalar  | no       |  8.366
`C`            | Model coefficient                          | scalar  | no       |  11.0

The inherited entries are elaborated in:

- wallFunctionCoefficients.H
- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/kqRWallFunctions/kLowReWallFunction" %>

API:

- [Foam::kLowReWallFunctionFvPatchScalarField](doxy_id://Foam::kLowReWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
