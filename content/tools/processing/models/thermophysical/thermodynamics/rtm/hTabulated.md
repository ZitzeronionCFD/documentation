---
title: Tabulated (enthalpy)
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- enthalpy
- polynomial
- thermodynamics
menu_id: thermo-hTabulated
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-thermo
group: thermo-models
---

<%= page_toc %>

Heat capacity at constant pressure, $$ c_p $$ \[J/kg/K\]

$$
c_p = f(p, T)
$$

## Usage

The `hTabulated` thermodynamics model is defined using the entries:

~~~
thermodynamics
{
    Hf              <scalar>;
    Sf              <scalar>;
    Cp
    (
        (<T1>   <cp1>)
        (<T2>   <cp2>)
        (<T3>   <cp3>)
        ...
        (<Tn>   <cpn>)
    )
}
~~~

Where

- `Hf` : heat of formation \[J/kg\]
- `Sf` : entropy of formation \[J/kg\]
- `Cp` : List of tuples of (temperature heat capacity) describing the table

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/thermo/hTabulated" %>
