---
title: fieldAverage
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- average
- field
- function object
- post-processing
menu_id: function-objects-field-fieldaverage
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `fieldAverage` function object computes ensemble- and/or time-based
field averages, with optional windowing, for a user-specified selection of
volumetric and/or surface fields.

Fields are entered as a list of sub-dictionaries, which indicate the type of
averages to perform, and can be updated during the calculation.  The current
options include:

- `mean`: arithmetic mean

$$
    \overline{x} = \frac{1}{N}\displaystyle\sum\limits_{i=0}^N x_i
$$

- `prime2Mean`: prime-squared mean

$$
    \overline{x'}^2 = \frac{1}{N} \displaystyle\sum\limits_{i=0}^N
    (x_i - \overline{x})^2
$$

- `base`: average over 'time', or 'iteration' (`N` in the above)
- `window`: optional averaging window, specified in 'base' units

Average field names are constructed by concatenating the base field with
the averaging type, e.g. when averaging field 'U', the name of resultant fields
becomes:

- arithmetic mean field, `UMean`
- prime-squared field, `UPrime2Mean`

Note that averaging is performed per cell. If the mesh is moving, e.g.
a rotating body within a static outer mesh, cells in the rotating region will
sample different regions of space which may lead to unexpected/non-continuous
field average
results.
{: .note}

Information regarding the number of averaging steps, and total averaging
time are written on a per-field basis to the
`<functionObject name>Properties` dictionary, located in `<time>/uniform`.

When restarting form a previous calculation, the averaging is continuous or
may be restarted using the `restartOnRestart` option.

The averaging process may be restarted after each calculation output time
using the `restartOnOutput` option or restarted periodically using the
`periodicRestart` option and setting `restartPeriod` to the required
averaging period.

With the `subRegion` option, also supports fields on function object
surface output (e.g., `sampledSurfaces`).

### Operands

Operand      | Type                          | Location
:---         |:---                           |:---
input        | `{vol,surface}<Type>Field(s)` | `$FOAM_CASE/<time>/<inpField>s`
output file  | -                             | -
output field | `{vol,surface}<Type>Field(s)` | `$FOAM_CASE/<time>/<outField>s`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `fieldAverage` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    fieldAverage1
    {
        // Mandatory entries (unmodifiable)
        type            fieldAverage;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        fields
        (
            <field name 1>
            {
                mean        <yes|no>;
                prime2Mean  <yes|no>;
                base        <time|iteration>;
                windowType   exact;
                window       10.0;
                windowName   <name>;
                allowRestart true;
            }
            ...
            <field name N>
            {
                mean        <yes|no>;
                prime2Mean  <yes|no>;
                base        <time|iter>;
           }
        );

        // Optional entries (runtime modifiable)
        restartOnRestart    false;
        restartOnOutput     false;
        periodicRestart     false;
        restartPeriod       0.002;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
     }

<% assert :string_exists, "fieldAverage.H", "fieldAverage" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "fieldAverage",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldAverage.C", "fields" %>
<% assert :string_exists, "fieldAverageItemIO.C", "mean" %>
<% assert :string_exists, "fieldAverageItemIO.C", "prime2Mean" %>
<% assert :string_exists, "fieldAverageItemIO.C", "base" %>
<% assert :string_exists, "fieldAverage.H", "fieldAverage" %>
<% assert :string_exists, "fieldAverage.C", "restartOnRestart" %>
<% assert :string_exists, "fieldAverage.C", "restartOnOutput" %>
<% assert :string_exists, "fieldAverage.C", "periodicRestart" %>
<% assert :string_exists, "fieldAverage.C", "restartPeriod" %>
<% assert :string_exists, "fieldAverageItemIO.C", "window" %>
<% assert :string_exists, "fieldAverageItemIO.C", "windowName" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: fieldAverage             | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 fields       | Names of the operand fields and averaging options | dict |  yes  | -
 restartOnRestart| Restart the averaging on restart   | bool | no     | false
 restartOnOutput | Restart the averaging on output    | bool | no     | false
 periodicRestart | Periodically restart the averaging | bool | no   | false
 restartPeriod   | Periodic restart period            | scalar | conditional | -
 restartTime     | One-shot reset of the averaging    | scalar | no   | GREAT
 subRegion    | Name for alternative objectRegistry   | word   | no   | ""

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldAverageItem](ref_id://function-objects-field-fieldaverageitem)

Usage by the `postProcess` utility is not available.

## Stored properties  {#stored-properties}

### Fields

Average fields are stored on the mesh database, using the default name
constructions:

    <field>Mean
    <field>Prime2Mean

These are written to the standard time directories according to the
[writeControl](<%= ref_id "post-processing-function-objects", "usage-run-time-control" %>) setting.

## Further information {#sec-os-field-fieldAverage-further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/LES/pitzDaily" %>
- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/cavitatingFoam/LES/throttle" %>
- <%= repo_link2 "$FOAM_TUTORIALS/discreteMethods/dsmcFoam/freeSpacePeriodic" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/fieldAverage" %>

API:

- [Foam::functionObjects::fieldAverage](doxy_id://Foam::functionObjects::fieldAverage)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
