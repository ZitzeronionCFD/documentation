---
title: alphatPhaseChangeJayatillekeWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-alphatPhaseChangeJayatillekeWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-multiphase
group: bcs-multiphase
---

<%= page_toc %>

## Description

The `compressible::alphatPhaseChangeJayatillekeWallFunction` is a boundary condition that provides
a thermal wall function for turbulent thermal diffusivity (usually `alphat`)
based on the Jayatilleke model for the Eulerian multiphase solvers.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            compressible::alphatPhaseChangeJayatillekeWallFunction;

        // Optional entries
        Prt             <scalar>;
        Cmu             <scalar>;
        kappa           <scalar>;
        E               <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "alphatPhaseChangeJayatillekeWallFunctionFvPatchScalarField.H", "alphatPhaseChangeJayatillekeWallFunction" %>
<% assert :string_exists, "alphatPhaseChangeJayatillekeWallFunctionFvPatchScalarField.C", "Prt" %>
<% assert :string_exists, "alphatPhaseChangeJayatillekeWallFunctionFvPatchScalarField.C", "Cmu" %>
<% assert :string_exists, "alphatPhaseChangeJayatillekeWallFunctionFvPatchScalarField.C", "kappa" %>
<% assert :string_exists, "alphatPhaseChangeJayatillekeWallFunctionFvPatchScalarField.C", "E" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `compressible::alphatPhaseChangeJayatillekeWallFunction` | word    | yes      | -
`Prt`                  | Turbulent Prandtl number                   | scalar | no   | 0.85
`Cmu`                  | Empirical model coefficient                | scalar | no   | 0.09
`kappa`                | Von Karman constant                        | scalar | no   | 0.41
`E`                    | Wall roughness parameter                   | scalar | no   | 9.8

The inherited entries are elaborated in:

- alphatPhaseChangeWallFunctionFvPatchScalarField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/reactingTwoPhaseEulerFoam/RAS/wallBoiling" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/phaseSystemModels/reactingEuler/multiphaseSystem/derivedFvPatchFields/alphatPhaseChangeJayatillekeWallFunction" %>

API:

- [Foam::compressible::alphatPhaseChangeJayatillekeWallFunctionFvPatchScalarField](doxy_id://Foam::compressible::alphatPhaseChangeJayatillekeWallFunctionFvPatchScalarField)

<%= history "v2012" %>

<!----------------------------------------------------------------------------->
