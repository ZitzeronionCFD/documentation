---
title: waveDisplacement
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-waveDisplacement
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-meshMotion
group: bcs-meshMotion
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            waveDisplacement;
        amplitude       <vector>;
        omega           <scalar>;

        // Optional entries
        waveNumber      <vector>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "waveDisplacementPointPatchVectorField.H", "waveDisplacement" %>
<% assert :string_exists, "waveDisplacementPointPatchVectorField.C", "amplitude" %>
<% assert :string_exists, "waveDisplacementPointPatchVectorField.C", "omega" %>
<% assert :string_exists, "waveDisplacementPointPatchVectorField.C", "waveNumber" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `waveDisplacement`              | word    | yes      | -
`amplitude`    | Amplitude                                  | word    | yes      | -
`omega`        | Omega                                      | word    | yes      | -
`waveNumber`   | Wave number                                | word    | yes      | -

The inherited entries are elaborated in:

- fixedValuePointPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/fvMotionSolver/pointPatchFields/derived/waveDisplacement" %>

API:

- [Foam::waveDisplacementPointPatchVectorField](doxy_id://Foam::waveDisplacementPointPatchVectorField)

<%= history "2.1.0" %>

<!----------------------------------------------------------------------------->
