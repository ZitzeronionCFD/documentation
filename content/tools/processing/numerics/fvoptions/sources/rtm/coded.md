---
title: Coded source
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- coded
- fvOption
- source
menu_id: fvoption-codedsource
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-general
---

<%= page_toc %>

The `coded` option provides several hooks that can be implemented in C++ code
to manipulate the system of equations, e.g. by including additional source
terms and modifying the equation matrices.

## Properties {#properties}

The `coded` option is available to all primitive field types, i.e.

- `scalarCodedSource`: scalar equations
- `vectorCodedSource`: vector equations
- `sphericalTensorCodedSource`: vector equations
- `symmTensorCodedSource`: vector equations
- `tensorCodedSource`: vector equations

It provides hooks to:

- include sources: `codeAddSup`
- constrain values before the equation is solved: `codeSetValue`
- apply corrections after the equation has been solved: `codeCorrect`

<% assert :string_exists, "CodedFvSource.C", "codeAddSup" %>
<% assert :string_exists, "CodedFvSource.C", "codeSetValue" %>
<% assert :string_exists, "CodedFvSource.C", "codeCorrect" %>

## Usage {#usage}

An example showing how to add a source to the energy equation of the form
$$
    S_h = \frac{1}{V} 100000 \sin(200 C_x)
$$

is shown below:

    codedSource
    {
        type            coded;
        selectionMode   all;

        fields          (h);
        name            sourceTime;

        codeAddSup
        #{
            const Time& time = mesh().time();
            const scalarField& V = mesh_.V();
            scalarField& heSource = eqn.source();

            // Start time
            const scalar startTime = 2.0;

            // Retrieve the x component of the cell centres
            const scalarField& cellx = mesh_.C().component(0);

            // Only apply when we have reached the start time
            if (time.value() > startTime)
            {
                // Apply the source
                forAll(cellx, i)
                {
                    // cell volume specific source
                    heSource[i] += 1e5*sin(200*cellx[i])*V[i];
                };
            }
        #};
    }

<% assert :string_exists, "CodedFvSource.H", "coded" %>
<% assert :string_exists, "cellSetOption.C", "all" %>
<% assert :string_exists, "CodedFvSource.C", "fields" %>
<% assert :string_exists, "CodedFvSource.C", "name" %>

By default the coded option links against the following libraries:

- `finiteVolume`: finite volume machinery
- `meshTools`: mesh tools
- `sampling`: sampling

These can be extended using the following options:

- `codeInclude` : include additional header files
- `codeOptions` : path to additional header files
- `codeLibs` : linking additional libraries

<% assert :string_exists, "dynamicCodeContext.C", "codeInclude" %>
<% assert :string_exists, "dynamicCodeContext.C", "codeOptions" %>
<% assert :string_exists, "dynamicCodeContext.C", "codeLibs" %>

## Further information {#further-information}

Tutorials

- *none*

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/general/codedSource" %>
