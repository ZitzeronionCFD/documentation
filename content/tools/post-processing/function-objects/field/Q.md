---
title: Q
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- Q criterion
menu_id: function-objects-field-q
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `Q` function object
computes the second invariant of the velocity gradient tensor \[$$s^{-2}$$\]:

$$
    Q = \frac{1}{2}\left[\left(\mathrm{tr} \left(\grad \u\right)\right)^2
      - \mathrm{tr} \left(\grad \u \dprod \grad \u\right)\right]
$$

`Q` iso-surfaces are good indicators of turbulent flow structures.

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `volVectorField`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `volScalarField`     | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `Q` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    Q1
    {
        // Mandatory entries (unmodifiable)
        type            Q;
        libs            (fieldFunctionObjects);

        // Optional (inherited) entries
        field           <inpField>;
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "Q.H", "Q" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files", "Q", "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: Q                        | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func Q

## Stored properties  {#stored-properties}

### Fields

The Q field is stored on the mesh database, using the default name

    Q

If the `field` entry is set to a value other than `U`, the default name
becomes

    Q(<field>)

This can be overridden using the `result` entry.

## Sample output {#sample-output}

<%= vimeo 173060023, 640, 406 %>

## Further information {#further-information}

Tutorial:

- [pimpleFoam](ref_id://pimplefoam) `surfaceMountedCube`
  tutorial: <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/surfaceMountedCube/fullCase" %>

\ofAssert functionObjectUseExists incompressible/pimpleFoam/LES/surfaceMountedCube/fullCase Q

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/Q" %>

API:

- [Foam::functionObjects::Q](doxy_id://Foam::functionObjects::Q)

Related:

- [Vorticity](ref_id://function-objects-field-vorticity)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
