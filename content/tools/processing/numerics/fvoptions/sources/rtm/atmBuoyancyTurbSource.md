---
title: atmBuoyancyTurbSource
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- atmospheric
- buoyancy
- fvOption
- source
- turbulence
menu_id: fvoption-atmbuoyancyturbSource
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-atm
---

<%= page_toc %>

## Properties {#properties}

- The `atmBuoyancyTurbSource` applies sources on `k`
  and either `epsilon` or `omega` to incorporate effects
  of buoyancy for atmospheric boundary layer modelling.
- The `atmBuoyancyTurbSource` can be applied on `epsilon` or
  `omega` based RAS turbulence models.
- The `atmBuoyancyTurbSource` inherits the traits of the
  [fvOption](ref_id://fvoptions), and `cellSetOption`.

Corrections applied to:

`k`
: Turbulent kinetic energy \[m$$^2$$/s$$^2$$\]

Corrections applied to either of the below, if exist:

`epsilon`
: Turbulent kinetic energy dissipation rate \[m$$^2$$/s$$^3$$\]

`omega`
: Specific dissipation rate \[1/s\]

Required fields:

`k`
: Turbulent kinetic energy \[m$$^2$$/s$$^2$$\]

`alphat`
: Kinematic turbulent thermal conductivity \[m$$^2$$/s\]

and either of:

`epsilon`
: Turbulent kinetic energy dissipation rate \[m$$^2$$/s$$^3$$\]

`omega`
: Specific dissipation rate \[1/s\]

## Model equations {#model-equations}

### Turbulent kinetic energy dissipation rate

The model expressions for `epsilon` (<%= cite "alletto_atm_2018" %>, Eq. 5, rhs-term:3):

$$
    S_p = \alpha \rho \frac{C_3 B}{k_o} \epsilon
$$

with ((<%= cite "sogachev_atm_2012" %>, Eq. 18, rhs-term:3),
(<%= cite "alletto_atm_2018" %>, Eq. 5, rhs-term:3 has a typo)):

$$
    C_3 = (C_1 - C_2) \alpha_B + 1.0;
$$

and (<%= cite "alletto_atm_2018" %>, Eq. 10, with a typo of $$C_2$$ instead of using $$(C_2 - 1.0)$$):

$$
    \alpha_B = {neg}_0 (R) (1.0 - (1.0 + \frac{C_2 - 1.0}{C_2 - C_1}) L )
             + pos(R) (1.0 - L)
$$

Mixing-length scale estimation (<%= cite "pope_atm_2000" %>, Eq. 10.37 & p. 374) normalised by $$L_{max}$$:

$$
    L = \frac{C_\mu^{3/4}}{L_{max}} \frac{k_o^{3/2}}{\epsilon_o}
$$

Gradient Richardson number (<%= cite "alletto_atm_2018" %>, Eq. 4):

$$
    R = - \frac{B}{G_o + \zeta}
$$

Buoyancy production term (<%= cite "alletto_atm_2018" %>, Eq. 7):

$$
    B = \beta_B \alpha_{t_o} (\grad{T_o} \cdot \vec{g})
$$

### Specific dissipation rate

The model expression for `omega` ((<%= cite "langner_atm_2016" %>) (<%= cite "alletto_atm_2018" %>, Eq. 5, rhs-term:3)):

$$
    S_p = \alpha \rho \frac{C_3 B}{k_o} \omega
$$

with ((<%= cite "sogachev_atm_2012" %>, Eq. 19, rhs-term:3),
(<%= cite "alletto_atm_2018" %>, Eq. 5, rhs-term:3 has a typo)):

$$
    C_3 = (\gamma - \beta) \alpha_B;
$$

and (<%= cite "alletto_atm_2018" %>, Eq. 10):

$$
    \alpha_B = {neg}_0 (R) (1.0 - (1.0 + \frac{\beta}{\beta - \gamma}) L )
             + pos(R) (1.0 - L)
$$

Mixing-length scale estimation (<%= cite "langner_atm_2016" %> Eq. 3.20) normalised by $$L_{max}$$:

$$
    L = \frac{1}{C_\mu^{1/4} L_{max}} \frac{\sqrt{k_o}}{\omega_o}
$$

Gradient Richardson number (<%= cite "alletto_atm_2018" %>, Eq. 4):

$$
    R = - \frac{B}{G_o + \zeta}
$$

Buoyancy production term (<%= cite "alletto_atm_2018" %>, Eq. 7):

$$
    B = \beta_B \alpha_{t_o} (\grad{T_o} \cdot \vec{g})
$$

### Turbulent kinetic energy

The model expression for `k:`

$$
    S_p = \alpha \rho \frac{B}{k_o} k
$$

Where:

$$S_p$$
: Source term without boundary conditions

$$\epsilon$$
: Turbulent kinetic energy dissipation rate (Current iteration) \[m2/s3\]

$$\omega$$
: Specific dissipation rate (Current iteration) \[1/s\]

$$k$$
: Turbulent kinetic energy (Current iteration) \[m2/s2\]

$$\epsilon_o$$
: Previous-iteration epsilon \[m2/s3\]

$$\omega_o$$
: Previous-iteration omega \[1/s\]

$$k_o$$
: Previous-iteration k \[m2/s2\]

$$C_1$$
: Model constant (epsilon-based models) \[-\]

$$C_2$$
: Model constant (epsilon-based models) \[-\]

$$\beta$$
: Model constant (omega-based models) \[-\]

$$\gamma$$
: Model constant (omega-based models) \[-\]

$$C_3$$
: Modified model constant field \[-\]

$$L$$
: Normalised mixing-length scale \[-\]

$$L_{max}$$
: Maximum mixing-length scale \[m\]

$$B$$
: Buoyancy production term \[m2/s3\]

$$T_o$$
: Previous-iteration temperature \[K\]

$$\alpha_{t_o}$$
: Previous-iteration kinematic turbulent thermal conductivity \[m2/s\]

$$G_o$$
: Previous-iteration turbulent kinetic energy production contribution \[m2/s2\]

$$\vec{g}$$
: Gravitational field \[m/s2\]

$$C_\mu$$
: Empirical model constant \[-\]

$$R$$
: Gradient Richardson number \[-\]

$$\beta_B$$
: Thermal expansion coefficient \[-\]

$$\alpha$$
: Phase fraction in multiphase computations, otherwise equals to 1

$$\rho$$
: Fluid density in compressible computations, otherwise equals to 1

$$\zeta$$
: Small value to prevent floating-point exceptions \[-\]

## Usage {#usage}

Example of the fvOptions specification using `constant/fvOptions` file:

    atmBuoyancyTurbSource1
    {
        // Mandatory entries (unmodifiable)
        type                  atmBuoyancyTurbSource;

        atmBuoyancyTurbSourceCoeffs
        {
            // Mandatory (inherited) entries (unmodifiable)
            selectionMode    all;

            // Optional (unmodifiable)
            rho          rho;
            Lmax         41.575;
            beta         3.3e-03;
        }

        // Optional (inherited) entries
        ...
    }

<% assert :string_exists, "atmBuoyancyTurbSource.H", "atmBuoyancyTurbSource" %>
<% assert :string_exists, "atmBuoyancyTurbSource.C", "rho" %>
<% assert :string_exists, "atmBuoyancyTurbSource.C", "Lmax" %>
<% assert :string_exists, "atmBuoyancyTurbSource.C", "beta" %>
<% assert :string_exists, "cellSetOption.C", "selectionMode" %>
<% assert :string_exists, "cellSetOption.C", "cellZone" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: atmBuoyancyTurbSource    | word   | yes    | -
 kAmb         | Ambient value for `k`               | scalar | yes    | -
 rho          | Name of density field               | word   | no     | rho
 Lmax         | Maximum mixing-length scale         | scalar | no     | 41.575
 beta         | Thermal expansion coefficient       | scalar | no     | 3.3e-03

The inherited entries are elaborated in:

- [fvOption](ref_id://fvoptions)
  - `cellSetOption`

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmFlatTerrain" %>
- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmForestStability" %>

Source code

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/fvOptions/atmBuoyancyTurbSource" %>

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
