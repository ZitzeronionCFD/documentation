---
title: activeBaffleVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- coupled
menu_id: boundary-conditions-activeBaffleVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-coupled
group: bcs-coupled
---

<%= page_toc %>

## Description

The `activeBaffleVelocity` is a velocity boundary condition that simulates the
opening of a baffle due to local flow conditions, by merging the behaviours of
`wall` and `cyclic` conditions. The baffle joins two mesh regions, where the
open fraction determines the interpolation weights applied to each cyclic- and
neighbour-patch contribution.

We determine whether the baffle is opening or closing from the sign of
the net force across the baffle, from which the baffle open fraction is
updated using:

$$
  x = x_{old} + \text{sign}(\vec{F}_{net}) \frac{\Delta t}{T}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$x$$                 | Baffle open fraction \[0-1\]
$$x_{old}$$           | Baffle open fraction on previous evaluation \[0-1\]
$$\Delta t$$          | Simulation time step \[s\]
$$T$$                 | Time taken to open the baffle \[s\]
$$\vec{F}_{net}$$     | Net force across the baffle \[N\]

The open fraction is then applied to scale the patch areas.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                  activeBaffleVelocity;
        cyclicPatch           <word>;
        orientation           <label>;
        openFraction          <scalar>;
        openingTime           <scalar>;
        maxOpenFractionDelta  <scalar>;

        // Optional entries
        p                     <word>;

        // Inherited entries
        ...
    }

where:

Property              | Description                                | Type    | Required | Default
----------------------|--------------------------------------------|---------|----------|---------
`type`                | Type name: `activeBaffleVelocity`          | word    | yes      | -
`cyclicPatch`         | Name of cyclic patch                       | word    | yes      | -
`orientation`         | Flag to switch flow direction, either 1 or -1 | label | yes     | -
`openFraction`        | Current open fraction of patch \[0-1\]     | scalar  | yes      | -
`openingTime`         | Time taken to open the baffle              | scalar  | yes      | -
`maxOpenFractionDelta` | Max open fraction change per timestep     | scalar  | yes      | -
`p`                   | Name of pressure field                     | word    | no       | `p`

<% assert :string_exists, "activeBaffleVelocityFvPatchVectorField.C", "cyclicPatch" %>
<% assert :string_exists, "activeBaffleVelocityFvPatchVectorField.C", "orientation" %>
<% assert :string_exists, "activeBaffleVelocityFvPatchVectorField.C", "openFraction" %>
<% assert :string_exists, "activeBaffleVelocityFvPatchVectorField.C", "openingTime" %>
<% assert :string_exists, "activeBaffleVelocityFvPatchVectorField.C", "maxOpenFractionDelta" %>
<% assert :string_exists, "activeBaffleVelocityFvPatchVectorField.C", "p" %>

The inherited entries are elaborated in:

- fixedValueFvPatchField.H

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/activeBaffleVelocity" %>

API:

- [Foam::activeBaffleVelocityFvPatchVectorField](doxy_id://Foam::activeBaffleVelocityFvPatchVectorField)

<%= history "1.6" %>


<!----------------------------------------------------------------------------->
