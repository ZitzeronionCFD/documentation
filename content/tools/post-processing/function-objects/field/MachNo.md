---
title: MachNo
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- compressible
- field
- function object
- Mach
- post-processing
menu_id: function-objects-field-machno
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `MachNo` function object computes the Mach number as a `volScalarField`.

The Mach number is a dimensionless quantity representing the ratio of flow
velocity past a boundary to the local speed of sound:

$$
    M = \frac{|\u|}{\sqrt{\gamma p/\rho}}
$$

where:

$$M$$
: Mach number

$$\u$$
: Velocity

$$\gamma$$
: The ratio of specific heat of a gas at a constant pressure to heat at a constant volume

$$p$$
: Static pressure

$$\rho$$
: Fluid density

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `volVectorField`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `volScalarField`     | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `MachNo` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    MachNo1
    {
        // Mandatory entries (unmodifiable)
        type            MachNo;
        libs            (fieldFunctionObjects);

        // Optional (inherited) entries
        field           <inpField>;
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "MachNo/MachNo.H", "MachNo" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "MachNo",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: MachNo                   | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func MachNo

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/MachNo" %>

API:

- [Foam::functionObjects::MachNo](doxy_id://Foam::functionObjects::MachNo)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
