---
title: Pre-processing
copyright:
- Copyright (C) 2021-2023 OpenCFD Ltd.
menu_id: pre-processing
license: CC-BY-NC-ND-4.0
menu_parent: tools
menu_weight: 10
---

<%= page_toc%>

- [Mesh](mesh)
- [Surface](surface)
- [Field](field)
