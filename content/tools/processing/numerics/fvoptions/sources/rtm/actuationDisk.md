---
title: Actuation disk
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- disk
- fvOption
- source
- momentum
menu_id: fvoption-acuationdisk
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-momentum
---

<%= page_toc %>

## Properties {#properties}

- The `actuationDiskSource` applies sources on velocity, i.e. `U`,
  to enable actuator disk models for aero/hydro thrust loading of
  rotary disks (e.g. propellers, horizontal-axis wind/tidal turbines,
  or helicopter rotors) on surrounding flow field in terms of
  energy conversion processes.
- The `actuationDiskSource` is applied on the volume cells (not on faces).
  This nuance might be important since theoretical actuator disks possess
  zero-thickness.
- The `actuationDiskSource` inherits the traits of the
  [fvOption](ref_id://fvoptions), and `cellSetOption`.

Corrections applied to:

`U`
: Velocity \[m/s\]

Required fields:

`U`
: Velocity \[m/s\]

## Model equations {#model-equations}

The `actuationDiskSource` possesses two options for force computations:

`Froude`
: Froude's one-dimensional ideal actuator disk method

`variableScaling`
: Variable-scaling actuator disk method

### Froude's method

The model expressions for `Froude` method (<%= cite "burton_atm_2011" %>, Eq. 3.9):

$$
    T = 2 \rho_{\ref} A | \u_m \cdot \vec{n} |^2 a (1-a)
$$

with

$$
    a = 1 - \frac{C_p}{C_T}
$$

Where:

$$T$$
: Thrust magnitude \[N\]

$$\rho_{\ref}$$
: Monitored incoming fluid density \[kg/m$$^3$$\]

$$A$$
: Actuator disk planar surface area \[m$$^2$$\]

$$\u_m$$
: Incoming velocity spatial-averaged on monitored region \[m/s\]

$$\vec{n}$$
: Surface-normal vector of the actuator disk pointing upstream \[-\]

$$a$$
: Axial induction factor \[-\]

$$C_p$$
: Power coefficient \[-\]

$$C_T$$
: Thrust coefficient \[-\]

### Variable-scaling method

The model expressions for `variableScaling` method
((<%= cite "laan_atm_2015" %>, Eqs. 5-6), (<%= cite "laan2_atm_2015" %>)):

$$
    T = 0.5 \rho_{\ref} A | \u_o \cdot \vec{n} |^2 C_T^*
$$

with

$$
    C_T^* = C_T \left( \frac{ | \u_m | }{ | \u_o | } \right)^2
$$

Where:

$$T$$
: Thrust magnitude \[N\]

$$\rho_{\ref}$$
: Monitored incoming fluid density \[kg/m$$^3$$\]

$$A$$
: Actuator disk planar surface area \[m$$^2$$\]

$$\u_m$$
: Incoming velocity spatial-averaged on monitored region \[m/s\]

$$\u_o$$
: Incoming velocity spatial-averaged on actuator disk \[m/s\]

$$\vec{n}$$
: Surface-normal vector of the actuator disk pointing upstream \[-\]

$$a$$
: Axial induction factor \[-\]

$$C_p$$
: Power coefficient \[-\]

$$C_T$$
: Thrust coefficient \[-\]

$$C_T^*$$
: Calibrated thrust coefficient \[-\]

### Operands

Operand       | Type   | Location
:---          |:---    |:---
 input        | -      | -
 output file  | dat    | `$POST=$FOAM_CASE/postProcessing/<FvO>/<time>/<file>`

## Usage {#usage}

Example of the fvOptions specification using `constant/fvOptions` file:

    actuationDiskSource1
    {
        // Mandatory entries (unmodifiable)
        type            actuationDiskSource;

        // Mandatory (inherited) entries (unmodifiable)
        selectionMode   <mode>;    // e.g. cellSet as shown below
        cellSet         <cellSetName>;

        // Mandatory entries (runtime modifiable)
        diskArea        40.0;
        diskDir         (1 0 0);
        Cp              <Function1>;
        Ct              <Function1>;

        // Conditional optional entries (unmodifiable)
        monitorMethod   <points|cellSet>;
        monitorCoeffs
        {
            // Option-1
            points
            (
                (p1x p1y p1z)
                (p2x p2y p2z)
                ...
            );

            // Option-2
            cellSet     <monitorCellSet>;
        }

        // Optional entries (unmodifiable)
        variant         <forceMethod>;

        // Optional entries (runtime modifiable)
        sink            true;
        writeFileStart  0;
        writeFileEnd    100;

        // Optional (inherited) entries
        ...
    }
<% assert :string_exists, "actuationDiskSource.H", "actuationDiskSource" %>
<% assert :string_exists, "actuationDiskSource.C", "monitorMethod" %>
<% assert :string_exists, "actuationDiskSource.C", "monitorCoeffs" %>
<% assert :string_exists, "actuationDiskSource.C", "points" %>
<% assert :string_exists, "actuationDiskSource.C", "cellSet" %>
<% assert :string_exists, "actuationDiskSource.C", "diskArea" %>
<% assert :string_exists, "actuationDiskSource.C", "diskDir" %>
<% assert :string_exists, "actuationDiskSource.C", "Cp" %>
<% assert :string_exists, "actuationDiskSource.C", "Ct" %>
<% assert :string_exists, "actuationDiskSource.C", "variant" %>
<% assert :string_exists, "actuationDiskSource.C", "sink" %>
<% assert :string_exists, "actuationDiskSource.C", "writeFileStart" %>
<% assert :string_exists, "actuationDiskSource.C", "writeFileEnd" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: actuationDiskSource      | word | yes      | -
 diskArea     | Actuator disk planar surface area   | scalar | yes | -
 diskDir      | Surface-normal vector of the actuator disk pointing upstream | vector | yes | -
 Cp           | Power coefficient                   | Function1 | yes | -
 Ct           | Thrust coefficient                  | Function1 | yes | -
 monitorMethod | Type of incoming velocity monitoring method - see below | word | no | points
 variant      | Type of the force computation method - see below | word | no | Froude
 sink         | Flag for body forces to act as a source (`true`) or a sink (`false`) | bool | no  | true
 writeFileStart | Start time for file output        | scalar | no  | 0
 writeFileEnd   | End time for file output          | scalar | no  | VGREAT

The inherited entries are elaborated in:

- [fvOption](ref_id://fvoptions)
  - `cellSetOption`
- [writeFile](ref_id://function-objects-field-writefile)
  - `Function1`

Options for the `monitorMethod` entry:

    points  | Monitor incoming velocity field at a given set of points
    cellSet | Monitor incoming velocity field at a given cellSet

Options for the `variant` entry:

    Froude          | Froude's one-dimensional ideal actuator disk method
    variableScaling | Variable-scaling actuator disk method

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/turbineSiting" %>

Sample result

![Actuator disk example](../example-fvoptions-sources-actuation-disk-source.png)

Source code:

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/derived/actuationDiskSource" %>

<%= history "2.2.0" %>

<!----------------------------------------------------------------------------->
