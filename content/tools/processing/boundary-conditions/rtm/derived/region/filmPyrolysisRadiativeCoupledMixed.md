---
title: filmPyrolysisRadiativeCoupledMixed
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-filmPyrolysisRadiativeCoupledMixed
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-region
group: bcs-region
---

<%= page_toc %>

## Description

The `filmPyrolysisRadiativeCoupledMixed` is boundary condition that provides
a mixed boundary condition for temperature, to be used in the flow and
pyrolysis regions when a film region model is used.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            filmPyrolysisRadiativeCoupledMixed;
        Tnbr            <word>;
        qr              <word>;
        filmDeltaDry    <scalar>;
        filmDeltaWet    <scalar>;

        // Optional entries
        filmRegion      <word>;
        pyrolysisRegion  <word>;
        convectiveScaling  <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "filmPyrolysisRadiativeCoupledMixedFvPatchScalarField.H", "filmPyrolysisRadiativeCoupledMixed" %>
<% assert :string_exists, "filmPyrolysisRadiativeCoupledMixedFvPatchScalarField.C", "Tnbr" %>
<% assert :string_exists, "filmPyrolysisRadiativeCoupledMixedFvPatchScalarField.C", "qr" %>
<% assert :string_exists, "filmPyrolysisRadiativeCoupledMixedFvPatchScalarField.C", "filmDeltaDry" %>
<% assert :string_exists, "filmPyrolysisRadiativeCoupledMixedFvPatchScalarField.C", "filmDeltaWet" %>
<% assert :string_exists, "filmPyrolysisRadiativeCoupledMixedFvPatchScalarField.C", "pyrolysisRegion" %>
<% assert :string_exists, "filmPyrolysisRadiativeCoupledMixedFvPatchScalarField.C", "convectiveScaling" %>
<% assert :string_exists, "filmPyrolysisRadiativeCoupledMixedFvPatchScalarField.C", "filmRegion" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `filmPyrolysisRadiativeCoupledMixed` | word    | yes      | -
`Tnbr`         | Name of field on the neighbour region      | word    | yes      | -
`qr`           | Name of the radiative heat flux            | word    | yes      | -
`filmDeltaDry` | Minimum delta film to be considered dry    | scalar  | yes      | -
`filmDeltaWet` | Maximum delta film to be considered wet    | scalar  | yes      | -
`pyrolysisRegion` | Name of pyrolysis region                | word    | no       | pyrolysisProperties
`filmRegion`   | Name of film region                        | word    | no       | surfaceFilmProperties
`convectiveScaling` | Convective scaling factor             | scalar  | no       | 1.0

The inherited entries are elaborated in:

- mixedFvPatchFields.H
- pyrolysisModel.H
- temperatureCoupledBase.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/fireFoam/LES/flameSpreadWaterSuppressionPanel" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/regionModels/regionCoupling/derivedFvPatchFields/filmPyrolysisRadiativeCoupledMixed" %>

API:

- [Foam::filmPyrolysisRadiativeCoupledMixedFvPatchScalarField](doxy_id://Foam::filmPyrolysisRadiativeCoupledMixedFvPatchScalarField)

<%= history "2.2.2" %>

<!----------------------------------------------------------------------------->
