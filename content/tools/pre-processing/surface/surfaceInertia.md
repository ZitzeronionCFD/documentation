---
title: surfaceInertia
tags:
- inertia
- surface
copyright:
- Copyright 2022 (C) OpenCFD Ltd.
menu_id: surfaceInertia
license: CC-BY-NC-ND-4.0
menu_parent: pre-processing-surface
group: pre-processing-surface
---

<%= page_toc %>

## Overview {#overview}

The `surfaceInertia` utility determines the following properties of a
user-supplied surface file:

- inertia tensor for the thin shell or solid body enclosed by the surface;
- principal axes; and
- moments.

## Usage

### Synopsis

<%= help_util_usage "surfaceInertia" %>

#### Examples

~~~
surfaceInertia -referencePoint '(0 0 1)' surface.stl
~~~

### Input


#### Arguments

<%= help_util_arguments "surfaceInertia" %>

#### Options

<%= help_util_options "surfaceInertia" %>

#### Files

<%= no_input "file" %>

#### Fields

<%= no_input "field" %>

### Output

#### Logs

Property                               | Type
---------------------------------------|-------
Mass of the surface                    | scalar
Centre of mass of the surface          | vector
Surface area                           | scalar
Inertia tensor around centre of mass   | tensor
Eigen values - principal moments       | vector
Eigen vectors - principal axes         | vectors
Transform tensor from reference state (orientation) | tensor

#### Files

Property       | Description                 | Path               | Type
---------------|-----------------------------|--------------------|------
`axes.obj`     | Writes scaled principal axes at centre of mass of the surface | `<case>/` | `obj`

#### Fields

<%= no_output "field" %>

## Method

The inertia of the surface or the volume is evaluated as follows.

### Shell

The first step is to compute the centre of mass, $$ c_m $$:

$$
c_m = \frac{\sum_{i=1}^{n} | S_{t,i} | c_{t,i}}{\sum_{i=1}^{n} | S_{t,i} |},
$$

where $$ S_t $$ is the triangle area and $$ n $$ the number of triangles in the
surface.

The tensorial inertia, $$ J $$, around the centre of mass determined using:

$$
J = \sum_{i=1}^{n} J_{t,i},
$$

where $$ J_t $$ is the inertia of a triangle.

### Solid

The calculation follows the procedure from the
[Geometric Tools](https://www.geometrictools.com) library. Further details can
be found in <%= cite "eberly_polyhedral_2009" %>.

## Further information {#further-information}

Tutorial:

- *None*

Source code:

- <%= repo_link2 "$FOAM_UTILITIES/surface/surfaceInertia" %>
- <%= repo_link2 "$FOAM_SRC/meshTools/momentOfInertia" %>

<%= history "1.7.0" %>

<!----------------------------------------------------------------------------->