---
title: SIMPLE algorithm
menu_id: pv-simple
license: CC-BY-NC-ND-4.0
menu_parent: pressure-velocity
---

<%= page_toc %>

\underConstruction

## SIMPLE {#SIMPLE}

- <B>S</B>emi-<B>I</B>mplicit <B>M</B>ethod for <B>P</B>ressure <B>L</B>inked
  <B>E</B>quations
- By *Caretto et al.* <%= cite "caretto_two_1972" %>

The sequence for each iteration follows:

1. Advance to the next iteration $$ t = t^{n + 1} $$
2. Initialise $$ \u^{n+1} $$ and $$ p^{n+1} $$ using latest available values of
   $$ \u $$ and $$ p $$
3. Construct the
   [momentum equations](ref_id://pressure-velocity#eqn-solver-momentum)
4. Under-relax the
   [momentum matrix](ref_id://pressure-velocity#eqn-solver-matrix-composition)
5. Solve the momentum equations to obtain a prediction for $$ \u^{n+1} $$
6. Construct the
   [pressure equation](ref_id://pressure-velocity#eqn-solver-pressure)
7. Solve the pressure equation for $$ p^{n+1} $$
8. [Correct the flux](ref_id://pressure-velocity#eqn-solver-flux-corrector)
   for $$ \phi^{n+1} $$
9. Under-relax $$ p^{n+1} $$
10. [Correct the velocity](ref_id://pressure-velocity#eqn-solver-velocity-corrector")
    for $$ \u^{n+1} $$
11. If not converged, go back to step 2

## SIMPLEC {#SIMPLEC}

- By *Van Doormaal and Raithby* <%= cite "van_doormaal_enhancements_1984" %>
