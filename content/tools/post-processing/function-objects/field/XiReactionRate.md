---
title: XiReactionRate
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- reaction
- Xi
menu_id: function-objects-field-reactionrate
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `XiReactionRate` function object writes the turbulent flame-speed and
reaction-rate volScalarFields for the Xi-based combustion models.

### Operands

Operand       | Type             | Location
:---          |:---              |:---
input         | -                | -
output file   | -                | -
output field  | `volScalarField` | `$FOAM_CASE/<time>/{St,wdot}`

## Usage {#usage}

Example of the `XiReactionRate` function object by using `functions`
sub-dictionary in `system/controlDict` file:

    XiReactionRate1
    {
        // Mandatory entries (unmodifiable)
        type            XiReactionRate;
        libs            (fieldFunctionObjects);

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "XiReactionRate/XiReactionRate.H", "XiReactionRate" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "XiReactionRate",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: XiReactionRate           | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2(
  "$FOAM_TUTORIALS/combustion/XiDyMFoam/annularCombustorTurbine") %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/XiReactionRate" %>

API:

- [Foam::functionObjects::XiReactionRate](doxy_id://Foam::functionObjects::XiReactionRate)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->