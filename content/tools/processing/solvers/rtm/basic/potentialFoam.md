---
title: potentialFoam
copyright:
- Copyright (C) 2021 OpenCFD Ltd.
tags:
- basic
- incompressible
- solvers
menu_id: potentialfoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-basic
group: solvers-basic
solver_models:
- inviscid
---

<%= page_toc %>

Solves for the velocity potential to provide velocity and incompressible flux
fields, typically used to initialise viscous calculations.

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-basic" %>
- steady state
- incompressible

## Equations {#equations}

Solves for the velocity potential, $$ \Phi $$, using the equation

$$
    \laplacian \Phi = \div \phi
$$

where the volumetric flux, $$ \phi $$ is given by dotting the interpolated
velocity with the face area vectors:

$$
    \phi = \u_f \dprod \vec{S}_f
$$

Finally the cell centre velocity is derived by reconstructing the volumetric
flux.

## Input requirements {#input-requirements}

Mandatory fields:

- `p`: kinematic pressure \[m2/s2\] or `Phi`: velocity potential \[m2/s\]
- `U`: velocity \[m/s\]

<% assert :string_exists, "potentialFoam/createFields.H", "p" %>
<% assert :string_exists, "potentialFoam/createFields.H", "Phi" %>
<% assert :string_exists, "potentialFoam/createFields.H", "U" %>

### Physical models {#input-requirements-physical-models}

- None

### Solution controls {#input-requirements-solution-controls}

[$FOAM_CASE/system/fvSchemes](ref_id://fvschemes) requirements:

- [gradient schemes](ref_id://schemes-gradient): `grad(Phi)`
- [laplacian schemes](ref_id://schemes-laplacian): `laplacian(1,Phi)`

<% assert :text_exists, "potentialFoam.C", "fvc::div" %>
<% assert :text_exists, "potentialFoam.C", "fvm::laplacian" %>

[$FOAM_CASE/system/fvSolution](ref_id://fvsolution) requirements:

- [solver entry](ref_id://numerics-linearequationsolvers#options) for `Phi`

<% assert :string_exists, "potentialFoam/createFields.H", "Phi" %>

### Command line options {#input-requirements-command-line-options}

- `-pName <name>`: name of the pressure field
- `-initialiseUBCs`: initialise the velocity boundary conditions prior to
  computing the volumetric flux
- `-writePhi`: write the velocity potential field
- `-writep`: write the Euler pressure field

<% assert :string_exists, "potentialFoam.C", "pName" %>
<% assert :string_exists, "potentialFoam.C", "initialiseUBCs" %>
<% assert :string_exists, "potentialFoam.C", "writePhi" %>
<% assert :string_exists, "potentialFoam.C", "writep" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/basic/potentialFoam" %>

See also

- <%= link_to_menuid "simplefoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/basic/potentialFoam" %>
