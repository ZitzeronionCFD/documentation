---
title: makeFaMesh
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
  - meshing
menu_id: makeFaMesh
license: CC-BY-NC-ND-4.0
menu_parent: mesh-generation
---

<%= page_toc %>

## Overview {#overview}

The `makeFaMesh` utility creates finite-area meshes from volume-mesh patches.

## Usage {#usage}

### Synopsis

<%= help_util_usage "makeFaMesh" %>

#### Examples

Run `makeFaMesh` and write the mesh as a VTK file for display:

~~~
makeFaMesh -write-vtk
~~~

### Input

#### Arguments

<%= help_util_arguments "makeFaMesh" %>

#### Options

<%= help_util_options "makeFaMesh" %>

#### Files

The `makeFaMesh` utility is configured using a `<case>/system/faMeshDefinition`,
taking the form:

~~~
polyMeshPatches             ( <patch1> <patch2> ... <patchN> );

boundary
{
    <faPatch1>
    {
        type                <word>;
        neighbourPolyPatch  <word>;
        ownerPolyPatch      <word>;
    }
}

defaultPatch
{
    name    <word>;
    type    <word>;
}
~~~

Property              | Description                                | Type    | Required | Default
----------------------|--------------------------------------------|---------|----------|---------
`polyMeshPatches`     | List of volume mesh patches to use as the base of the finite area  mesh |`wordRes` | yes | -
`boundary`            | Dictionary providing list of edge-boundary conditions | `dict` | yes | -
`type`                | Patch type, e.g. `patch`                   | `word` | yes | -
`neighbourPolyPatch`  | Finite-volume patch neighbouring the finite-area | `word` | yes | -
`ownerPolyPatch`      | Finite-volume patch corresponding to the finite-area patch | `word` | no | -
`defaultPatch`        | Dictionary providing characteristics of default patches | `dict` | no | -
`name`                | Name of default patches | `word` | yes | -
`type`                | Patch type of default patches, e.g. `empty` | `word` | yes | -

#### Fields

<%= no_input "field" %>

### Output

#### Logs

<%= no_output "log" %>

#### Files

<%= no_output "file" %>

#### Fields

The default path to write fields is `<case>/constant/faMesh/`.

Property       | Description                 | Type
---------------|-----------------------------|------------------
`faBoundary`   | Finite-area boundary info   | `faBoundaryMesh`
`faceLabels`   | Finite-area mesh info       | `labelList`

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/finiteArea/liquidFilmFoam/cylinder/system/faMeshDefinition" %>
- <%= repo_link2 "$FOAM_TUTORIALS/compressible/acousticFoam/obliqueAirJet/main/system/faMeshDefinition" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/laminar/filmPanel0/system/faMeshDefinition" %>

Source code:

- <%= repo_link2 "$FOAM_UTILITIES/finiteArea/makeFaMesh" %>

<%= history "v1712" %>


<!----------------------------------------------------------------------------->
