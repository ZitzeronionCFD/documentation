---
title: searchableSurfaceCollection
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: searchablesurfacecollection
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh-geometry
group: searchableobjects
---

<%= page_toc %>

A set of transformed _searchableSurfaces_.

After creating the searchable object the _searchableSurfaceCollection_ can be
scaled and transformed in the specified local co-ordinate system.

![searchable collection example](../images/searchableCollection-small.png)

In the example shown here, the _box1_ has two copies called _seal_ and _herring_.
The geometries are always merged. Regions are merged when

    mergeSubRegions true;

As it is shown in Figure 1.  Figure 2 shows the situation with regions not
merged.

![searchable collection example](../images/searchableCollection_noMerge-small.png)

Boolean operations are not performed, i.e. on meshing, parts may be identified
'inside'.
{: .note}

## Usage

    box1
    {
        type            searchableBox;
        min             (0 0 0);
        max             (1 1 1);
    }


    twoFridgeFreezers
    {
        type            searchableSurfaceCollection;

        mergeSubRegions true; //create single region

        seal
        {
            surface         box1;
            scale           (1.0 1.0 2.1);
            transform
            {
                coordinateSystem
                {
                    type            cartesian;
                    origin          (3 3 0);
                    coordinateRotation
                    {
                        type            axesRotation;
                        e1              (1 0 0);
                        e3              (0 0 1);
                    }
                }
            }
        }

        herring
        {
            surface         box1;
            scale           (1.0 1.0 2.1);
            transform
            {
                coordinateSystem
                {
                    type            cartesian;
                    origin          (2.5 2.5 0);
                    coordinateRotation
                    {
                        type            axesRotation;
                        e1              (1 0 0);
                        e3              (0 0 1);
                    }
                }
            }
        }
    }
