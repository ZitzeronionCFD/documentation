---
title: Limit velocity
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- correction
- velocity
- momentum
menu_id: fvoption-limitvelocity
license: CC-BY-NC-ND-4.0
menu_parent: numerics-fvoptions-corrections
group: fvoptions-corrections-momentum
---

## Properties {#properties}

## Usage {#usage}

<% assert :string_exists, "fvOptions/corrections/limitVelocity/limitVelocity.H", "limitVelocity" %>

## Further information {#further-information}

Tutorials

- *none*

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/corrections/limitVelocity" %>
