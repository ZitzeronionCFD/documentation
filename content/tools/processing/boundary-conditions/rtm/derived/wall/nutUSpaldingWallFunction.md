---
title: nutUSpaldingWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-nutUSpaldingWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `nutUSpaldingWallFunction` is boundary condition that provides
a wall function for the turbulent viscosity (i.e. `nut`) based on velocity
(i.e. `U`). Using Spalding's law gives a continuous `nut` profile to the wall.

$$
    y^+ = u^+ + \frac{1}{E} \left[exp(\kappa u^+) - 1 - \kappa u^+\,
        - 0.5 (\kappa u^+)^2 - \frac{1}{6} (\kappa u^+)^3\right]
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$y^+$$               | Wall-normal height of a cell centre in wall units
$$u^+$$               | Velocity at \f$y^+\f$ in wall units
$$\kappa$$            | von Karman constant

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            nutUSpaldingWallFunction;

        // Optional entries
        maxIter         <label>;
        tolerance       <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "nutUSpaldingWallFunctionFvPatchScalarField.H", "nutUSpaldingWallFunction" %>
<% assert :string_exists, "nutUSpaldingWallFunctionFvPatchScalarField.C", "maxIter" %>
<% assert :string_exists, "nutUSpaldingWallFunctionFvPatchScalarField.C", "tolerance" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `nutUSpaldingWallFunction`      | word    | yes      | -
`maxIter`      | Number of Newton-Raphson iterations        | label   | no       | 10
`tolerance`    | Convergence tolerance                      | scalar  | no       | 0.0001

The inherited entries are elaborated in:

- nutWallFunctionFvPatchScalarField.H
<!-- end of the list -->

- Suffers from non-exact restart since `correctNut()` (called through
`turbulence->validate`) returns a slightly different value every time
it is called. This is since the seed for the Newton-Raphson iteration
uses the current value of `*this (\c =nut )`.
- This can be avoided by overriding the tolerance. This also switches on
a pre-detection whether the current nut already satisfies the turbulence
conditions and if so does not change it at all. This means that the nut
only changes if it 'has really changed'. This probably should be used with
a tight tolerance, to make sure to kick every iteration, e.g.
    maxIter     100;
    tolerance   1e-7;
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/airFoil2D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/nutWallFunctions/nutUSpaldingWallFunction" %>

API:

- [Foam::nutUSpaldingWallFunctionFvPatchScalarField](doxy_id://Foam::nutUSpaldingWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
