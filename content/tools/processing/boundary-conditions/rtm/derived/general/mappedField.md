---
title: mappedField
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-mappedField
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `mappedField` is a generic boundary condition that provides a
self-contained version of the `mapped` condition. It does not use information
on the patch; instead it holds the data locally.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            mappedPatch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    mappedField;

        // Inherited entries
        ...
    }

<% assert :string_exists, "mappedFieldFvPatchField.H", "mappedField" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `mappedField`                   | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::mappedPatchBase](doxy_id://Foam::mappedPatchBase)
- [Foam::mappedPatchFieldBase](doxy_id://Foam::mappedPatchFieldBase)
- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

- Since this condition can be applied on a per-field and per-patch basis,
  it is possible to duplicate the mapping information.  If possible, employ
  the `mapped` condition in preference to avoid this situation, and only
  employ this condition if it is not possible to change the underlying
  geometric (poly) patch type to `mapped`.
  {: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/fireFoam/LES/flameSpreadWaterSuppressionPanel" %>
- <%= repo_link2 "$FOAM_TUTORIALS/combustion/fireFoam/LES/oppositeBurningPanels" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/mappedField" %>

API:

- [Foam::mappedFieldFvPatchField](doxy_id://Foam::mappedFieldFvPatchField)

<%= history "2.1.0" %>

<!----------------------------------------------------------------------------->
