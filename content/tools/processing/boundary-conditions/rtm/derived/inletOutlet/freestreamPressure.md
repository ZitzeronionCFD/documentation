---
title: freestreamPressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-freestreamPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `freestreamPressure` is a generic boundary condition that provides
a free-stream condition for pressure. It is an outlet-inlet condition that
uses the velocity orientation to continuously blend between zero gradient for
normal inlet and fixed value for normal outlet flow.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            freestreamPressure;
        freestreamValue <scalarField>;

        // Optional entries
        U               <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "freestreamPressureFvPatchScalarField.H", "freestreamPressure" %>
<% assert :string_exists, "freestreamPressureFvPatchScalarField.C", "freestreamValue" %>
<% assert :string_exists, "freestreamPressureFvPatchScalarField.C", "U" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `freestreamPressure`            | word    | yes      | -
`freestreamValue` | Patch value                             | scalarField | yes  | -
`U`            | Name of velocity field                     | word    | no       | U

The inherited entries are elaborated in:

- [Foam::fvPatchField](doxy_id://Foam::fvPatchField)
- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/RAS/aerofoilNACA0012" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/airFoil2D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/freestreamPressure" %>

API:

- [Foam::freestreamPressureFvPatchScalarField](doxy_id://Foam::freestreamPressureFvPatchScalarField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
