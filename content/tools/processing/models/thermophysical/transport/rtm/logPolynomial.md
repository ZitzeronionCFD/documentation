---
title: Log polynomial
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- transport
menu_id: transport-logPolynomial
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-transport
group: transport-models
---

<%= page_toc %>

Dynamic viscosity, $$ \mu $$ \[Pa.s\]

$$
\ln(\mu) = \sum_{i=1}^N \left( c_i \ln(T)^{i-1} \right)
$$

Thermal conductivity, $$ \kappa $$ \[W/mK\]

$$
\ln(\kappa) = \sum_{i=1}^N \left( c_i \ln(T)^{i-1} \right)
$$

Thermal diffusivity, $$ \alpha_h $$ \[kg/ms\]

$$
\alpha_h = \frac{\kappa}{c_p}
$$

## Usage

The `logPolynomial` transport model is defined using the entries:

~~~
transport
{
    muCoeffs<8>     ( <c1> <c2> <c3> <c4> <c5> <c6> <c7> <c8> );
    kappaCoeffs<8>  ( <c1> <c2> <c3> <c4> <c5> <c6> <c7> <c8> );
}
~~~

Where

- `muCoeffs<8>` : Polynomial coefficients (x8)
- `kappaCoeffs<8>` : Polynomial coefficients (x8)

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/transport/logPolynomial" %>
