---
title: semiPermeableBaffleMassFraction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-semiPermeableBaffleMassFraction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `semiPermeableBaffleMassFraction` is a boundary condition that provides
a mass-fraction boundary condition for a semi-permeable baffle.

This condition models a baffle which is permeable to a some species and
impermeable to others. It must be used in conjunction with the
corresponding velocity condition,
`semiPermeableBaffleVelocity`.

The mass flux of a species is calculated as a coefficient multiplied by the
difference in mass fraction across the baffle.

$$
    \phi_{Yi} = c A (Y_i - Y_{i,n})
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\phi_{Yi}$$         | Flux of the permeable species \[kg/s\]
$$c$$                 | Transfer coefficient \[kg/m^2/s\]
$$A$$                 | Patch face area \[m^2\]
$$Y_i$$               | Mass fraction on the patch \[-\]
$$Y_{i,n}$$           | Mass fraction on the neighbour patch \[-\]

A species that the baffle is permeable to will, therefore, have a
coefficient greater than zero, whilst a species that does not transfer will
have a coefficient equal to zero.

This condition calculates the species flux. The fluxes are summed up by the
velocity condition to generate the net mass transfer across the baffle.
This mass-fraction condition then generates a corrective diffusive flux to
ensure that the correct amounts of the permeable species are transferred.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            semiPermeableBaffleMassFraction;

        // Optional entries
        c               <scalar>;
        phi             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "semiPermeableBaffleMassFractionFvPatchScalarField.H", "semiPermeableBaffleMassFraction" %>
<% assert :string_exists, "semiPermeableBaffleMassFractionFvPatchScalarField.C", "c" %>
<% assert :string_exists, "semiPermeableBaffleMassFractionFvPatchScalarField.C", "phi" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `semiPermeableBaffleMassFraction` | word    | yes      | -
`c`                    | Transfer coefficient                         | scalar  | no       | 0
`phi`                  | Name of flux field                         | word    | no       | phi

The inherited entries are elaborated in:

- mappedPatchBase.H
- mixedFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/semiPermeableBaffle/semiPermeableBaffleMassFraction" %>

API:

- [Foam::semiPermeableBaffleMassFractionFvPatchScalarField](doxy_id://Foam::semiPermeableBaffleMassFractionFvPatchScalarField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
