---
title: prghPermeableAlphaTotalPressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-prghPermeableAlphaTotalPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `prghPermeableAlphaTotalPressure` is a is a mixed boundary condition
for the `p_rgh` variable in multiphase flows.

It switches between an open boundary using the `prghTotalPressure`
boundary condition and a wall condition using the `fixedFluxPressure`
boundary condition.

In the `prghPermeableAlphaTotalPressure`, an open condition is
applied when `alpha` is under a user-defined `alphaMin` value and a wall
condition is applied when `alpha` is larger than the `alphaMin`.

This boundary condition can be used in conjunction with
`pressurePermeableAlphaInletOutletVelocity` for the `U` variable.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            prghPermeableAlphaTotalPressure;
        p               <PatchFunction1<scalar>>;

        // Optional entries
        phi             <word>;
        rho             <word>;
        U               <word>;
        alpha           <word>;
        alphaMin        <scalar>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "prghPermeableAlphaTotalPressureFvPatchScalarField.H", "prghPermeableAlphaTotalPressure" %>
<% assert :string_exists, "prghPermeableAlphaTotalPressureFvPatchScalarField.C", "p" %>
<% assert :string_exists, "prghPermeableAlphaTotalPressureFvPatchScalarField.C", "phi" %>
<% assert :string_exists, "prghPermeableAlphaTotalPressureFvPatchScalarField.C", "rho" %>
<% assert :string_exists, "prghPermeableAlphaTotalPressureFvPatchScalarField.C", "U" %>
<% assert :string_exists, "prghPermeableAlphaTotalPressureFvPatchScalarField.C", "alpha" %>
<% assert :string_exists, "prghPermeableAlphaTotalPressureFvPatchScalarField.C", "alphaMin" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `prghPermeableAlphaTotalPressure` | word    | yes      | -
`p`            | Total pressure                             | PatchFunction1\<scalar\> | yes | -
`phi`          | Name of flux field                         | word    | no       | phi
`rho`          | Name of density field                      | word    | no       | rho
`U`            | Name of velocity field                     | word    | no       | U
`alpha`        | Name of mixture field                      | word    | no       | alpha
`alphaMin`     | Minimum value of alpha                     | scalar  | no       | 1

The inherited entries are elaborated in:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
- [Foam::PatchFunction1](doxy_id://Foam::PatchFunction1)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/damBreakPermeable" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/prghPermeableAlphaTotalPressure" %>

API:

- [Foam::prghPermeableAlphaTotalPressureFvPatchScalarField](doxy_id://Foam::prghPermeableAlphaTotalPressureFvPatchScalarField)

<%= history "v2106" %>

<!----------------------------------------------------------------------------->
