---
title: Wall functions
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
- wall function
menu_id: ras-wallfunctions
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-ras
---

<%= page_toc %>

## Momentum {#momentum}

Momentum wall functions are imposed by setting the turbulence viscosity at the
wall.  By combining the dimensionless velocity

$$
    u^+ = \frac{u}{u_t}
$$

dimensionless wall distance

$$
    y^+ = \frac{\rho y u_t}{\mu}
$$

and friction velocity

$$
    u_t = \sqrt {\frac{\tau_{wall}}{\rho}}
$$

the wall shear stress can be described according to:

$$
    \tau_{wall} = \mu \frac{y^+}{u^+} \frac{u}{y}
                = (\mu + \mu_t) \frac{u}{y}
$$

where

$$
    \mu_t = \mu\left(\frac{y^+}{u^+} - 1\right)
$$

## High Reynolds number {#highre}

<%= insert_models "turbulence-wall-functions" %>

<!--
# Low Reynolds number {#lowre}

<%= insert_models "turbulence-wall-functions-lowre" %>
-->
