---
title: fixedNormalInletOutletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-fixedNormalInletOutletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `fixedNormalInletOutletVelocity` is a velocity boundary condition that
combines a fixed normal component obtained from the `normalVelocity`
patch field supplied with a fixed or zero-gradiented tangential component.

The tangential component is set depending on the direction
of the flow and the setting of `fixTangentialInflow` keyword:

- Outflow: apply zero-gradient condition to tangential components.
- Inflow:
    - `fixTangentialInflow` is true: apply value provided by the `normalVelocity`
    patch field to the tangential components,
    - `fixTangentialInflow` is false: apply zero-gradient condition to
    tangential components.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    fixedNormalInletOutletVelocity;
        fixTangentialInflow     <bool>;
        normalVelocity          <fvPatchVectorField>;

        //
        // normalVelocity
        // {
        //     type            uniformFixedValue;
        //     uniformValue    sine;
        //     uniformValueCoeffs
        //     {
        //         frequency 1;
        //         amplitude table
        //         (
        //             (0  0)
        //             (2  0.088)
        //             (8  0.088)
        //         );
        //         scale     (0 1 0);
        //         level     (0 0 0);
        //     }
        // }


        // Optional entries
        phi             <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "fixedNormalInletOutletVelocityFvPatchVectorField.H", "fixedNormalInletOutletVelocity" %>
<% assert :string_exists, "fixedNormalInletOutletVelocityFvPatchVectorField.C", "fixTangentialInflow" %>
<% assert :string_exists, "fixedNormalInletOutletVelocityFvPatchVectorField.C", "normalVelocity" %>
<% assert :string_exists, "fixedNormalInletOutletVelocityFvPatchVectorField.C", "phi" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `fixedNormalInletOutletVelocity`| word    | yes      | -
`phi`          | Name of flux field                         | word    | no       | phi
`fixTangentialInflow` | Flag to fix the tangential component for inflow | bool | yes | -
`normalVelocity` | Boundary condition that provides the normal component of the velocity | fvPatchVectorField | yes | -

The inherited entries are elaborated in:

- [Foam::fvPatchField](doxy_id://Foam::fvPatchField)
- [Foam::directionMixedFvPatchField](doxy_id://Foam::directionMixedFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/potentialFreeSurfaceFoam/oscillatingBox" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fixedNormalInletOutletVelocity" %>

API:

- [Foam::fixedNormalInletOutletVelocityFvPatchVectorField](doxy_id://Foam::fixedNormalInletOutletVelocityFvPatchVectorField)

<%= history "2.3.1" %>

<!----------------------------------------------------------------------------->
