---
title: Launder, Reece and Rodi (LRR)
short_title: LRR
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-lrr
license: CC-BY-NC-ND-4.0
menu_parent: ras-rstm
group: turbulence-ras-rstm
---

<%= page_toc %>

## Properties {#properties}

- Reynolds stress model

## Model equations {#model-equations}

## Default model coefficients {#default-coeffs}

## Usage {#usage}

The model is specified using:

    RAS
    {
        turbulence      on;
        RASModel        LRR;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "LRR.H", "LRR" %>

## Further information {#further-information}

Source code:

- [Foam::RASModels::LRR](doxy_id://Foam::RASModels::LRR)

References:

- Standard model: *Launder et al.* <%= cite "launder_progress_1975" %>
- Recommended generalized gradient diffusion model: *Daly and Harlow* <%= cite "daly_transport_1970" %>
- Gibson-Launder wall-reflection: *Gibson and Launder* <%= cite "gibson_ground_1978" %>
