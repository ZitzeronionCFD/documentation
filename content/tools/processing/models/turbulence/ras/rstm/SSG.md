---
title: Speziale, Sarkar and Gatski (SSG)
short_title: SSG
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-ssg
license: CC-BY-NC-ND-4.0
menu_parent: ras-rstm
group: turbulence-ras-rstm
---

<%= page_toc %>

## Properties {#properties}

- Reynolds stress model

## Model equations {#model-equations}

$$
    \Ddt{\rho \epsilon}
  =
    \div \left( D_\epsilon \grad \epsilon \right)
  + C_{\epsilon 1} \rho G \frac{\epsilon}{k}
  - C_{\epsilon 2} \rho \frac{\epsilon^2}{k}
  + S_\epsilon
$$

$$
    \Ddt{\rho \tensor{R}}
  =
    \div \left( \rho D_{\tensor{R}} \grad \tensor{R} \right)
  - \left( \frac{C_1}{2}\epsilon + \frac{C_{1}^{*}}{2}G \right) \rho \frac{\rho}{k}\tensor{R}
  + \rho P
  - \frac{1}{3}\tensor{I} ((2 - C_1)\epsilon - C_{1}^{*} G) \rho
  + C_2 \rho \epsilon \mathrm{dev}(innerSqr(\tensor{b}))
  + \rho k
    \left(
        (C_3 - C_{3}^{*} \mag{\tensor{b}}) \mathrm{dev}(\tensor{S})
      + C_4 \mathrm{dev}(twoSymm(\tensor{b} \dprod \tensor{S}))
      + C_5 twoSymm(\tensor{b} \dprod \tensor{\Omega})
    \right)
$$

## Default model coefficients {#default-coeffs}

## Usage {#usage}

The model is specified using:

    RAS
    {
        turbulence      on;
        RASModel        SSG;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "SSG.H", "SSG" %>

## Further information {#further-information}

Source code:

- [Foam::RASModels::SSG](doxy_id://Foam::RASModels::SSG)

References:

- Base model: *Speziale et al.* <%= cite "speziale_modelling_1991" %>
- Generalized gradient diffusion model of *Daly and Harlow* <%= cite "daly_transport_1970" %>
