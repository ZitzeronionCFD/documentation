---
title: DIC Gauss Seidel Smoother
short_title: DIC Gauss Seidel
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- solvers
- DIC
- Gauss Seidel
menu_id: smooth-dicgs
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-smooth
group: solvers-smooth-smoothers
---

<%= page_toc %>

## Properties {#properties}

- Combined DIC/GaussSeidel smoother in which DIC smoothing is followed by
  GaussSeidel to ensure that any "spikes" created by the DIC sweeps are
  smoothed-out
- Symmetric matrices

## Usage {#usage}

    smoother        DICGaussSeidel;

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/smoothers/DICGaussSeidel" %>

API:

- [Foam::DICGaussSeidelSmoother](doxy_id://Foam::DICGaussSeidelSmoother)
