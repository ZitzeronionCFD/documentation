---
title: Shih quadratic k-ε
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-shihquadratickepsilon
license: CC-BY-NC-ND-4.0
menu_parent: ras-nonlinearevm
group: turbulence-ras-nlevm
---

<%= page_toc %>

## Properties {#properties}

- Incompressible only

## Model equations {#model-equations}

The turbulence kinetic energy equation is given by:

$$
    \Ddt{k}
  =
    \div \left(D_k \grad k \right)
  + G
  - \epsilon
$$

and the dissipation rate by:

$$
    \Ddt{\epsilon}
  =
    \div \left( D_{\epsilon} \grad \epsilon \right)
  + C_1 G \frac{\epsilon}{k}
  - C_2 \frac{\epsilon^2}{k}
$$

The turbulence generation, $$ G $$ is given by:

$$
    G = \left[\nu_t \left( \grad \u + \left(\grad \u\right)^T \right) - \tensor{\tau}_{nl}\right] \colon \grad \u
$$

where $$ \tensor{\tau}_{nl} $$ is the non-linear stress.

## Default model coefficients {#default-coeffs}

## Usage {#usage}

The model is specified using:

    RAS
    {
        turbulence      on;
        RASModel        ShihQuadraticKE;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "ShihQuadraticKE.H", "ShihQuadraticKE" %>

## Further information {#further-information}

Source code:

- [Foam::incompressible::RASModels::ShihQuadraticKE](doxy_id://Foam::incompressible::RASModels::ShihQuadraticKE)

Reference:

- *Shih et al.* <%= cite "shih_realizable_1993" %>
