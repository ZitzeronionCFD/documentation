---
title: incompressible::alphatJayatillekeWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-incompressible-alphatJayatillekeWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `incompressible::alphatJayatillekeWallFunction` is boundary condition that
provides a kinematic turbulent thermal conductivity
for using wall functions, using the Jayatilleke 'P' function.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            alphatJayatillekeWallFunction;
        Prt             <scalar>;

        // Optional entries
        kappa           <scalar>;
        E               <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "alphatJayatillekeWallFunctionFvPatchScalarField.H", "alphatJayatillekeWallFunction" %>
<% assert :string_exists, "alphatJayatillekeWallFunctionFvPatchScalarField.H", "Prt" %>
<% assert :string_exists, "alphatJayatillekeWallFunctionFvPatchScalarField.H", "kappa" %>
<% assert :string_exists, "alphatJayatillekeWallFunctionFvPatchScalarField.H", "E" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `alphatJayatillekeWallFunction` | word    | yes      | -
`Prt`          | Turbulent Prandtl number                   | scalar  | yes      | -
`kappa`        | von Karman constant                        | scalar  | no       | 0.41
`E`            | Wall roughness parameter                   | scalar  | no       | 9.8

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantBoussinesqPimpleFoam/hotRoom" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/incompressible/turbulentTransportModels/derivedFvPatchFields/wallFunctions/alphatWallFunctions/alphatJayatillekeWallFunction" %>

API:

- [Foam::incompressible::alphatJayatillekeWallFunctionFvPatchScalarField](doxy_id://Foam::incompressible::alphatJayatillekeWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
