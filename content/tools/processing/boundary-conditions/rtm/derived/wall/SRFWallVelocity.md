---
title: SRFWallVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-SRFWallVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-velocity
---

<%= page_toc %>

## Description

The `SRFWallVelocity` is a boundary condition that provides
wall-velocity condition to be used in conjunction with the single rotating
frame (SRF) model.

The condition applies the appropriate rotation transformation in time and
space to determine the local SRF velocity of the wall.

$$
    \u_p = - \u_{p,srf}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\u_p$$              | Patch velocity \[m/s\]
$$\u_{p,srf}$$        | SRF velocity

The normal component of $$\u_p$$ is removed to ensure zero wall-flux even
if the wall patch faces are irregular.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            SRFWallVelocity;

        // Inherited entries
        ...
    }

<% assert :string_exists, "SRFWallVelocityFvPatchVectorField.H", "SRFWallVelocity" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `SRFWallVelocity`               | word    | yes      | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/cfdTools/general/SRF/derivedFvPatchFields/SRFWallVelocityFvPatchVectorField" %>

API:

- [Foam::SRFWallVelocityFvPatchVectorField](doxy_id://Foam::SRFWallVelocityFvPatchVectorField)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
