---
title: interstitialInletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-interstitialInletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `interstitialInletVelocity` is a velocity boundary condition with which
the actual interstitial velocity is calculated by dividing the specified
`inletVelocity` field with the local phase-fraction.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                interstitialInletVelocity;
        inletVelocity       <vectorField>;
        alpha               <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "interstitialInletVelocityFvPatchVectorField.H", "interstitialInletVelocity" %>
<% assert :string_exists, "interstitialInletVelocityFvPatchVectorField.C", "inletVelocity" %>
<% assert :string_exists, "interstitialInletVelocityFvPatchVectorField.C", "alpha" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `interstitialInletVelocity`     | word    | yes      | -
`inletVelocity`        | Non-interstitial inlet velocity            | vectorField  | yes | -
`alpha`                | Name of the phase-fraction field           | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/DPMFoam/Goldschmidt" %>
- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/reactingTwoPhaseEulerFoam/RAS/fluidisedBed" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/interstitialInletVelocity" %>

API:

- [Foam::interstitialInletVelocityFvPatchVectorField](doxy_id://Foam::interstitialInletVelocityFvPatchVectorField)

<%= history "2.2.2" %>

<!----------------------------------------------------------------------------->
