---
title: Uncorrected surface-normal gradient scheme
short_title: Uncorrected
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- gradient
menu_id: schemes-sngrad-uncorrected
license: CC-BY-NC-ND-4.0
menu_parent: schemes-sngrad
group: schemes-sngrad
---

<%= page_toc %>

$$
    \snGrad Q
  =
    \alpha \frac{Q_P - Q_N}{| \vec{d} |}
$$

where

$$
    \alpha = \frac{1}{cos(\theta)}
$$

## Properties {#properties}

- Central-difference scheme without non-orthogonal correction
- Implicit evaluation
- delta coefficients given by: nonOrthDeltaCoeffs

## Usage {#usage}

The scheme is specified using:

    snGradSchemes
    {
        default         none;
        grad(U)         uncorrected;
    }

## Further information {#further-information}

Source code

- [Foam::fv::uncorrectedSnGrad](doxy_id://Foam::fv::uncorrectedSnGrad)
