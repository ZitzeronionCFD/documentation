---
title: atmNutSource
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- atmospheric
- fvOption
- source
- turbulence
menu_id: fvoption-atmnutsource
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-atm
---

<%= page_toc %>

## Properties {#properties}

- The `atmNutSource` adds/subtracts a given artificial turbulent
viscosity field to/from `nut` for atmospheric boundary layer modelling.
- The `atmNutSource` can be applied on any RAS turbulence model.
- The `atmNutSource` inherits the traits of the
[fvOption](ref_id://fvoptions), and `cellSetOption`.

Corrections applied to:

`nut`
: Turbulent viscosity \[m$$^2$$/s\]

Required fields:

`nut`
: Turbulent viscosity \[m$$^2$$/s\]

`artNut`
: Artificial turbulent viscosity \[m$$^2$$/s\]

## Model equations {#model-equations}

The model expression:

$$
    \nu_t += \nu_{t_{art}}
$$

Where:

$$\nu_t$$
: Turbulent viscosity \[m2/s\]

$$\nu_{t_{art}}$$
: Artificial turbulent viscosity \[m2/s\]

## Usage {#usage}

Example of the fvOptions specification using `constant/fvOptions` file:

    atmNutSource1
    {
        // Mandatory entries (unmodifiable)
        type                  atmNutSource;

        atmNutSourceCoeffs
        {
            // Mandatory (inherited) entries (unmodifiable)
            selectionMode    all;

            // Optional entries (unmodifiable)
            nut              artNut;
        }

        // Optional (inherited) entries
        ...
    }

<% assert :string_exists, "atmNutSource.H", "atmNutSource" %>
<% assert :string_exists, "atmNutSource.C", "nut" %>
<% assert :string_exists, "cellSetOption.C", "selectionMode" %>
<% assert :string_exists, "cellSetOption.C", "cellZone" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: atmNutSource             | word | yes      | -
 nut          | Field name of artificial `nut`      | word | no       | artNut

The inherited entries are elaborated in:

- [fvOption](ref_id://fvoptions)
  - `cellSetOption`

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/fvOptions/atmNutSource" %>

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
