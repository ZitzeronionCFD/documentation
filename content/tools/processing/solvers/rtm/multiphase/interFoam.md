---
title: interFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- solvers
- multiphase
menu_id: interfoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-multiphase
group: solvers-multiphase
solver_models:
- transient
- multiphase
- turbulence
- fvOption
- dynamic-mesh
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-multiphase" %>
- transient
- incompressible
- multiphase: 2 immiscible phases
- isothermal
- <%= link_to_menuid "models-turbulence" %>
- <%= link_to_menuid "fvoptions" %>

## Equations {#equations}

- <%= link_to_menuid "pv-pimple" %>
- <%= link_to_menuid "algorithm-p-rgh" %>

## Input requirements {#input-requirements}

Mandatory fields:

- p: pressure \[pa\]
- p_rgh: pressure - hydrostatic contribution \[pa\]
- U: velocity \[m/s\]

### Physical models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence):
  `constant/turbulenceProperties`
- [finite volume options](ref_id://numerics-fvoptions#usage):
  `constant/fvOptions` (optional)

### Solution controls {#input-requirements-solution-controls}

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/multiphase/interFoam" %>

See also

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam" %>
