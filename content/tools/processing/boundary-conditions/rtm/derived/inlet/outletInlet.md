---
title: outletInlet
copyright:
- Copyright (C) 2017-2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-outletInlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-general
---

<%= page_toc %>

## Description

The `outletInlet` is a generic [mixed](ref_id://boundary-conditions-mixed)
boundary condition that sets the patch value to a user-specified [fixedValue](ref_id://boundary-conditions-fixedGradient) for reverse flows, and treats inflow using
a [zeroGradient](ref_id://boundary-conditions-zeroGradient) condition.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            outletInlet;
        outletValue     <Field>;

        // Optional entries
        phi             phi;

        // Inherited entries
    }

<% assert :string_exists, "outletInletFvPatchField.H", "outletInlet" %>
<% assert :string_exists, "outletInletFvPatchField.C", "outletValue" %>
<% assert :string_exists, "outletInletFvPatchField.C", "phi" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `outletInlet`                   | word    | yes      | -
`outletValue`          | Outlet field for reverse flow              | Field   | yes      | -
`phi`                  | Name of the local mass flux                | word    | no       | phi

The inherited entries are elaborated in:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
<!-- end of the list -->

- The mode of operation is determined by the sign of the flux across the
  patch faces.
- Sign conventions:
  - Positive flux (out of domain): apply the "outletValue" fixed-value
  - Negative flux (into domain): apply zero-gradient condition
  {: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/adjointOptimisationFoam/sensitivityMaps/naca0012/laminar/drag" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/outletInlet" %>

API:

- [Foam::outletInletFvPatchField](doxy_id://Foam::outletInletFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
