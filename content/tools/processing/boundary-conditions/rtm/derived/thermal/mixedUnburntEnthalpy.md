---
title: mixedUnburntEnthalpy
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-mixedUnburntEnthalpy
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `mixedUnburntEnthalpy` is a boundary condition that provides
a mixed condition for unburnt enthalpy.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            mixedUnburntEnthalpy;

        // Inherited entries
        ...
    }

<% assert :string_exists, "mixedUnburntEnthalpyFvPatchScalarField.H", "mixedUnburntEnthalpy" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `mixedUnburntEnthalpy`          | word    | yes      | -

The inherited entries are elaborated in:

- mixedFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/reactionThermo/derivedFvPatchFields/mixedUnburntEnthalpy" %>

API:

- [Foam::mixedUnburntEnthalpyFvPatchScalarField](doxy_id://Foam::mixedUnburntEnthalpyFvPatchScalarField)

<%= history "1.6" %>

<!----------------------------------------------------------------------------->
