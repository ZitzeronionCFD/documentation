---
title: Home
copyright:
- Copyright (C) 2021 OpenCFD Ltd.
menu_id: home
license: CC-BY-NC-ND-4.0
---

  <div class="card-columns card-container">

    <div class="card card-detail">
      <div class="card-body">
        <h4 class="card-title">About</h4>
        <p class="card-text">some text</p>
      </div>
    </div>

    <div class="card card-detail">
      <div class="card-body">
        <h4 class="card-title">Need help?</h4>
        <p class="card-text">The latest training offerings can
        get you up and running in the shortest possible time</p>
        <ul class="card-links">
          <li><%= link_to_external("Foundation", "https://www.openfoam.com/trainings/courses/core-foundation") %></li>
          <li><%= link_to_external("Advanced", "https://www.openfoam.com/trainings/courses/core-advanced") %></li>
          <li><%= link_to_external("Applications", "https://www.openfoam.com/trainings/courses#applications") %></li>
        </ul>
      </div>
    </div>

    <div class="card card-detail">
      <div class="card-body">
        <h4 class="card-title">Fundamentals</h4>
        <p class="card-text">some text</p>
      </div>
    </div>

    <div class="card card-detail">
      <div class="card-body">
        <h4 class="card-title">Tools</h4>
        <p class="card-text">Discover ...</p>
        <ul class="card-links">
          <li><%= link_to_menuid("pre-processing") %>: geometry manipulation, meshing, field initialisation ...</li>
          <li><%= link_to_menuid("processing") %>: models, numerics, solver applications ...</li>
          <li><%= link_to_menuid("post-processing") %>: sampling, function objects ... </li>
        </ul>
      </div>
    </div>

    <div class="card card-detail">
      <div class="card-body">
        <h4 class="card-title">OpenFOAM Wiki</h4>
        <p class="card-text">Help for building and installing OpenFOAM</p>
        <ul class="card-links">
          <li><%= link_to_external("Building from source", "https://develop.openfoam.com/Development/openfoam/-/wikis/building") %></li>
          <li><%= link_to_external("Binary packs", "https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled") %></li>
        </ul>
        <p class="card-text">Help for developers</p>
        <ul class="card-links">
          <li><%= link_to_external("Upgrade guides", "https://develop.openfoam.com/Development/openfoam/-/wikis/upgrade/upgrade") %></li>
          <li><%= link_to_external("Coding style", "https://develop.openfoam.com/Development/openfoam/-/wikis/coding/style/style") %></li>
          <li><%= link_to_external("Coding patterns", "https://develop.openfoam.com/Development/openfoam/-/wikis/coding/patterns/patterns") %></li>
        </ul>
      </div>
    </div>

    <div class="card card-detail">
      <div class="card-body">
        <h4 class="card-title">Get involved</h4>
        <p class="card-text">...</p>
        <ul class="card-links">
           <li><%= link_to_menuid "contributing-contributors" %></li>
           <li><%= link_to_menuid "contributing-how-to" %></li>
        </ul>
      </div>
    </div>

  </div>
