---
title: fixedIncidentRadiation
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-fixedIncidentRadiation
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `fixedIncidentRadiation` is a boundary condition for
thermal coupling for solid regions. It is used to emulate a fixed incident
radiative heat flux on a wall.

The gradient heat flux is calculated as:

$$
    q_r = \epsilon*(q_{r, incident} - \sigma_ T^4)
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\epsilon$$          | Emissivity of the solid
$$q_{r, incident}$$   | Specified fixed incident radiation
$$T$$                 | Temperature

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedIncidentRadiation;
        qrIncident      <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "fixedIncidentRadiationFvPatchScalarField.H", "fixedIncidentRadiation" %>
<% assert :string_exists, "fixedIncidentRadiationFvPatchScalarField.C", "qrIncident" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `fixedIncidentRadiation`        | word    | yes      | -
`qrIncident`           | Incident radiative heat flux               | scalarField  | yes | -

The inherited entries are elaborated in:

- fixedGradientFvPatchField.H
- temperatureCoupledBase.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/fireFoam/LES/simplePMMApanel" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/fixedIncidentRadiation" %>

API:

- [Foam::radiation::fixedIncidentRadiationFvPatchScalarField](doxy_id://Foam::radiation::fixedIncidentRadiationFvPatchScalarField)

<%= history "2206" %>

<!----------------------------------------------------------------------------->
