---
title: adjointFarFieldTMVar1
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-adjointFarFieldTMVar1
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-optimisation
group: bcs-optimisation
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            adjointFarFieldTMVar1;
        solverName      <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "adjointFarFieldTMVar1FvPatchScalarField.H", "adjointFarFieldTMVar1" %>
<% assert :string_exists, "adjointFarFieldTMVar1FvPatchScalarField.C", "solverName" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `adjointFarFieldTMVar1`       | word    | yes      | -
`solverName`   | Name of operand solver                     | word    | yes      | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- adjointBoundaryConditions.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/adjointOptimisationFoam/shapeOptimisation/naca0012/kOmegaSST/lift" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/optimisation/adjointOptimisation/adjoint/turbulenceModels/incompressibleAdjoint/adjointRAS/derivedFvPatchFields/adjointFarFieldTMVar1" %>

API:

- [Foam::adjointFarFieldTMVar1FvPatchScalarField](doxy_id://Foam::adjointFarFieldTMVar1FvPatchScalarField)

<%= history "v1906" %>

<!----------------------------------------------------------------------------->
