---
title: uniformInletOutlet
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-uniformInletOutlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-uniformInletOutlet
groups:
- bcs-inletOutlet
---

<%= page_toc %>

## Description

The `uniformInletOutlet` is a boundary condition that provides
a variant of the `inletOutlet` boundary condition with uniform `inletValue`.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            uniformInletOutlet;
        uniformInletValue  <Function1<Type>>;

        // Optional entries
        phi             <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "uniformInletOutletFvPatchField.H", "uniformInletOutlet" %>
<% assert :string_exists, "uniformInletOutletFvPatchField.C", "uniformInletValue" %>
<% assert :string_exists, "uniformInletOutletFvPatchField.C", "phi" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `uniformInletOutlet`            | word    | yes      | -
`uniformInletValue` | Uniform inlet value field             | Function1\<Type\>  | yes  | -
`phi`          | Name of flux field                         | word    | no       | phi

The inherited entries are elaborated in:

- mixedFvPatchField.H
- Function1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/uniformInletOutlet" %>

API:

- [Foam::uniformInletOutletFvPatchField](doxy_id://Foam::uniformInletOutletFvPatchField)

<%= history "2.2.2" %>

<!----------------------------------------------------------------------------->
