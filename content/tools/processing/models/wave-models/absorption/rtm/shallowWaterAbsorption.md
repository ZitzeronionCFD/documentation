---
title: Shallow water Absorption
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- absorption
- shallow
- waves
menu_id: waves-shallowwaterabsorption
license: CC-BY-NC-ND-4.0
menu_parent: models-waves
group: wave-absorption
---

<%= page_toc %>

## Properties {#properties}

- Outlet condition

## Model equations {#model-equations}

## Default model coefficients {#default-coeffs}

## Usage {#usage}

Outlet patch example

    <patch>
    {
        alpha           alpha.water;
        waveModel       shallowWaterAbsorption;
        nPaddle         1;
    }

## Further information {#further-information}

Source code:

- [Foam::waveModels::shallowWaterAbsorption](doxy_id://Foam::waveModels::shallowWaterAbsorption)

References:

- ...

Tutorials:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/cnoidal" %>
- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/stokesI" %>
- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/stokesII" %>
- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/stokesV" %>
