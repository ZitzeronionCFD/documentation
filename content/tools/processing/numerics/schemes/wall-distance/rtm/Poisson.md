---
title: Poisson wall distance
short_title: Poisson
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- wall
- Poisson
menu_id: schemes-walldistance-poisson
license: CC-BY-NC-ND-4.0
#menu_parent: schemes-walldistance
group: schemes-wall-distance
---

<%= page_toc %>

## Properties {#properties}

Calculation of approximate distance to nearest patch for all cells and
boundary by solving a Poisson equation.

## Usage {#usage}

The scheme is specified using:

    wallDist
    {
        method          Poisson;
    }

This requires the additional entry in the
[divSchemes](ref_id://schemes-divergence) sub-dictionary in the
[fvSchemes](ref_id://fvschemes) file, e.g.:

    laplacianSchemes
    {
        laplacian(yPsi) <laplacian scheme>;
    }

An solver entry for the `yPsi` variable is also required for the
[fvSolution](ref_id://fvsolution) file, e.g.

    yPsi
    {
        solver          <solver>;
        ...
        tolerance       1e-5;
        relTol          0;
    }

## Further information {#further-information}

Source code

- [Foam::patchDistMethods::Poisson](doxy_id://Foam::patchDistMethods::Poisson)

References

- *Spalding* <%= cite "spalding_calculation_1994" %>
- *Fares and Schroder* <%= cite "fares_differential_2002" %>
- *Tucker* <%= cite "tucker_differential_2003" %>
