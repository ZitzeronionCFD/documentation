const version = process.argv[2];

if (version === undefined)
{
    throw "Unknown version";
}

const out_dir = process.cwd() + '/output/' + version;

const lunr = require(out_dir + '/js/lunr.js');
const fs = require('fs');

console.log("Creating search index for version", version);

const readJsonFile = (path, callback) => {
    fs.readFile(path + '.json', 'utf8', (error, data) => {
        if (error){
            console.log(error);
            callback(error)
        }

        callback(null, path, JSON.parse(data))
    })
}

const createIndex = (err, path, data) => {
    if (err) throw err;

    let idxName = path + '.idx';

    var idx = lunr(function () {
        this.ref('id')
        this.field('filename', { boost: 10 });
        this.field('title', { boost: 10 });
        this.field('tags', { boost: 5 });
        this.field('body');

        data.forEach(function (doc) {
            this.add(doc)
            }, this
        );
    });

    fs.writeFile(idxName, JSON.stringify(idx), (err) => {
        if (err) throw err;

        console.log("- written " + idxName);
    });
}

readJsonFile(out_dir + '/js/search-docs', createIndex);
readJsonFile(out_dir + '/js/search-docs-source', createIndex);
