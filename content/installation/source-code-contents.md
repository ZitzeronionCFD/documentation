---
title: Source code contents
copyright:
- Copyright (C) 2021-2023 OpenCFD Ltd.
tags:
- source
menu_id: source-code-contents
license: CC-BY-NC-ND-4.0
menu_parent: installation
menu_weight: 30
---

Discover what's included in OpenFOAM, including our source and pre-compiled
distributions.

After installation you'll find the main directories:

- [src](repo_id://src): the core OpenFOAM libraries
- [applications](repo_id://applications): solvers and utilities
- [modules](repo_id://modules): Third-party code contributions
- [tutorials](repo_id://tutorials): test-cases that
  demonstrate a wide-range of OpenFOAM functionality
- [doc](repo_id://doc): documentation

Follow [this link](repo_base://) to navigate to the source code repository
online.