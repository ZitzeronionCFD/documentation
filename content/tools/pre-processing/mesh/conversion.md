---
title: Mesh conversion
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- conversion
- meshing
menu_id: meshing-conversion
license: CC-BY-NC-ND-4.0
menu_parent: pre-processing-mesh
---

## Conversion {#conversion}

- ccmToFoam
- fireToFoam
- fluentMeshToFoam, fluent3DMeshToFoam
- gmshToFoam
- ...
