---
title: Wall distance calculation methods
short_title: Wall distance
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- wall
menu_id: schemes-walldistance
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes
---

<%= page_toc %>

## Usage {#usage}

Wall distance properties are input in the [fvSchemes](ref_id://fvschemes) file
under the `wallDist` sub-dictionary using the syntax:

    wallDist
    {
        method          <method>;
    }

## Options {#options}

<%= insert_models "schemes-wall-distance" %>

The wall distance field can be written using the `checkMesh` utility using the
`writeAllFields` option.
{: .note}

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fvMesh/wallDist/wallDist" %>
- <%= repo_link2 "$FOAM_SRC/finiteVolume/fvMesh/wallDist/patchDistMethods/patchDistMethod" %>

API:

- [Foam::wallDist](doxy_id://Foam::wallDist)
- [Foam::patchDistMethod](doxy_id://Foam::patchDistMethod)
