---
title: rotatingWallVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-rotatingWallVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-velocity
---

<%= page_toc %>

## Description

The `rotatingWallVelocity` is a boundary condition that provides a rotational
velocity condition.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            rotatingWallVelocity;
        origin          <vector>;
        axis            <vector>;
        omega           <Function1<vector>>

        // Inherited entries
        ...
    }

<% assert :string_exists, "rotatingWallVelocityFvPatchVectorField.H", "rotatingWallVelocity" %>
<% assert :string_exists, "rotatingWallVelocityFvPatchVectorField.C", "origin" %>
<% assert :string_exists, "rotatingWallVelocityFvPatchVectorField.C", "axis" %>
<% assert :string_exists, "rotatingWallVelocityFvPatchVectorField.C", "omega" %>


where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `rotatingWallVelocity`          | word    | yes      | -
`origin`       | Origin of the rotation in Cartesian coordinates | vector | yes  | -
`axis`         | Axis of rotation                           | vector  | yes      | -
`omega`        | Angular velocity of the frame \[rad/s\]    | Function1\<vector\>  | yes | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/kinematicParcelFoam/spinningDisk" %>
- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/RAS/mixerVesselAMI" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/rotatingWallVelocity" %>

API:

- [Foam::rotatingWallVelocityFvPatchVectorField](doxy_id://Foam::rotatingWallVelocityFvPatchVectorField)

<%= history "1.6" %>

<!----------------------------------------------------------------------------->
