---
title: Surfaces
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- function object
- post-processing
- sampling
- surfaces
menu_id: function-objects-sampling-surfaces
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-sampling
group: function-objects-sampling
---

<%= page_toc %>

The `surfaces` function object samples field values on surfaces and writes
the result to file using the chosen output file format.

## Usage {#usage}

Basic operation of the `surfaces` function object comprises:

    surfaces1
    {
        type            surfaces;
        libs            (sampling);

        surfaceFormat   <format-type>;
        formatOptions
        {
            <format-type>
            {
                <options>
            }
        }

        fields          (<fields>);

        interpolationScheme <scheme-type>;

        surfaces
        {
            mySurface1
            {
                // set definition for 'mySurface1'
                type            <surface-type>;
                ...
            }
            mySurface2
            {
                // set definition for 'mySurface2'
                type            <surface-type>;
                ...
            }
            ...
            mySurfaceN
            {
                // set definition for 'mySurfaceN'
                type            <surface-type>;
                ...
            }
        }
    }

<% assert :string_exists, "sampledSurfaces.H", "surfaces" %>
<% assert :class_library_exists, "sampling/Make/files", "sampledSurfaces", "libsampling" %>
<% assert :string_exists, "sampledSurfaces.C", "fields" %>
<% assert :string_exists, "sampledSurfaces.C", "interpolationScheme" %>

Surfaces to be returned are listed in the `surfaces` sub-dictionary, each in
dictionary format with a user-supplied name, e.g. `mySurface1`, `mySurface2`,
and `mySurfaceN` above.

## Options

The `<surface-type>` value can be set to:

<%= insert_models "sampling-surfaces" %>

## Surface format

The surface output format is controlled using the `surfaceFormat` entry, with
additional format controls in the optional `formatOptions` sub-dictionary.
For example, to generate EnSight surfaces in ASCII format with collated times:

~~~
surfaceFormat       ensight;

// Optional controls
formatOptions
{
    ensight
    {
        format          ascii;
        collateTimes    true;
    }
}
~~~

<% assert :string_exists, "sampledSurfaces.C", "surfaceFormat" %>
<% assert :string_exists, "sampledSurfaces.C", "formatOptions" %>

Available surface format options that include both geometry and data are:

- `abaqus` : Abaqus
- `ensight` : EnSight
- `nastran` : nastran
- `vtk` : VTK

Further options are available, but are limited to geometry only, e.g.

- `obj` : Lightwave OBJ
- `stl` : STL

## Sample output {#sample-output}

This information is also written in the user-specified output format to file
in the directory:

    $FOAM_CASE/postProcessing/<function object>/<time>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/sampling/sampledSurface/sampledSurfaces" %>

API:

- [Foam::sampledSurfaces](doxy_id://Foam::sampledSurfaces)

Example usage:

- [pimpleFoam](ref_id://pimplefoam) `propeller` tutorial:
  <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/propeller" %>

\ofAssert functionObjectUseExists incompressible/pimpleFoam/RAS/propeller surfaces
