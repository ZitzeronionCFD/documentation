---
title: Non-linear eddy viscosity models
short_title: Non-linear EVM
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- eddy viscosity model
- non-linear
- RAS
- turbulence
menu_id: ras-nonlinearevm
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-ras
---

<%= page_toc %>

Non-linear eddy viscosity turbulence model selections include:

<%= insert_models "turbulence-ras-nlevm" %>
