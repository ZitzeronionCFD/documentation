---
title: Examples
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- example
menu_id: schemes-examples
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes
---

<%= page_toc %>

Test cases to evaluate OpenFOAM numerical selections include:

<%= insert_models "schemes-examples" %>
