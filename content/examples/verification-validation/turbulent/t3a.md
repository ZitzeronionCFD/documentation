---
title: Turbulence transition T3A
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- channel
- example
- turbulence
- verification
- validation
menu_id: verification-validation-turbulent-t3a
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-turbulent
group: verification-validation-turbulent
---

<%= page_toc %>

## Overview {#sec-overview}

- Solver: [simpleFoam](ref_id://simplefoam)
- Incompressible
- Steady
- Turbulence transition modelled using the [kOmegaSST-LM](ref_id://ras-komegasstlm) model
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/T3A" %>

![Case description](boundary-conditions-small.png)

## Mesh {#sec-mesh}

- 2D structured mesh created using [blockMesh](ref_id://blockmesh)

![Mesh](mesh-600.png)

![Close-up around the step](mesh-close-up-600.png)

## Boundary conditions {#sec-boundary-conditions}

- Ux: 5.4 m/s
- Isotropic turbulence: 3.3 %
- Viscosity ratio: 12

Fluid properties

- Kinematic viscosity: 1.5e-5 m2/s

<!-- plate length  L = 1.5m -->

## Results {#sec-results}

|![Turbulence kinetic energy](k-400x400.png) | ![Wall shear stress](tauw-400x400.png) |
{: .noBorder}
