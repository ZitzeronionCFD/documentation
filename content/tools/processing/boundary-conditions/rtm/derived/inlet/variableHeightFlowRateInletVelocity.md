---
title: variableHeightFlowRateInletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-variableHeightFlowRateInletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-variableHeightFlowRateInletVelocity
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `variableHeightFlowRateInletVelocity` is a boundary condition that provides
a velocity boundary condition for multphase flow based on a user-specified
volumetric flow rate.

The flow rate is made proportional to the phase fraction alpha at each
face of the patch and alpha is ensured to be bound between 0 and 1.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            variableHeightFlowRateInletVelocity;
        flowRate        <Function1<scalar>>;
        alpha           <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "variableHeightFlowRateInletVelocityFvPatchVectorField.H", "variableHeightFlowRateInletVelocity" %>
<% assert :string_exists, "variableHeightFlowRateInletVelocityFvPatchVectorField.C", "flowRate" %>
<% assert :string_exists, "variableHeightFlowRateInletVelocityFvPatchVectorField.C", "alpha" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `variableHeightFlowRateInletVelocity` | word    | yes | -
`flowRate`     | Volumetric flow rate \[m^3/s\]             | Function1\<scalar\> | yes  | -
`alpha`        | Name of phase-fraction field               | word    | no       | alpha

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- Function1.H
<!-- end of the list -->

- The value is positive into the domain.
- The condition may not work correctly for transonic inlets.
- Strange behaviour with [potentialFoam](ref_id://potentialfoam) since the
  momentum equation is not solved.
  {: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/RAS/weirOverflow" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/variableHeightFlowRateInletVelocity" %>

API:

- [Foam::variableHeightFlowRateInletVelocityFvPatchVectorField](doxy_id://Foam::variableHeightFlowRateInletVelocityFvPatchVectorField)

<%= history "2.1.1" %>

<!----------------------------------------------------------------------------->
