---
title: buoyantPimpleFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- heat transfer
- solvers
menu_id: buoyantpimplefoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-heat-transfer
group: solvers-heat-transfer
solver_models:
- transient
- compressible
- turbulence
- fvOption
- heat-transfer
- buoyancy
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-heat-transfer" %>
- Transient
- Compressible
- <%= link_to_menuid "models-turbulence" %>
- <%= link_to_menuid "models-thermophysical" %>
- <%= link_to_menuid "fvoptions" %>

## Usage

### Synopsis

<%= help_util_usage "buoyantPimpleFoam" %>

#### Examples

~~~
buoyantPimpleFoam -region region1
~~~

### Input

#### Arguments

<%= help_util_arguments "buoyantPimpleFoam" %>

#### Options

<%= help_util_options "buoyantPimpleFoam" %>

#### Files

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

#### Fields

Property  | Description                 | Type      | Required | Default
----------|-----------------------------|-----------|----------|--------
`p`       | Pressure \[Pa\]             | volScalarField | yes | -
`p_rgh`   | Pressure - hydrostatic contribution \[Pa\] | volScalarField | yes | -
`U`       | Velocity \[m/s\]            | volVectorField | yes | -
`T`       | Temperature \[K\]           | volScalarField | yes | -

#### Models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence):
  `constant/turbulenceProperties`
- [heat transfer](<%= ref_id "models-thermophysical", "usage" %>):
  `constant/thermophysicalModels`
- [finite volume options](<%= ref_id "numerics-fvoptions", "usage" %>):
  `constant/fvOptions` (optional)


## Method

The solver employs the [PIMPLE](ref_id://pv-pimple)
algorithm to solve the following transport equations:

- <%= link_to_menuid "mass-transport" %>
- <%= link_to_menuid "momentum-transport" %>
- <%= link_to_menuid "energy-transport" %>


## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantPimpleFoam" %>

Source code:

- <%= repo_link2 "$FOAM_SOLVERS/heatTransfer/buoyantPimpleFoam" %>

<%= history "1.7.0" %>


<!----------------------------------------------------------------------------->
