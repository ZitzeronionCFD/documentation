---
title: kqRWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-kqRWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `kqRWallFunction` is boundary condition that provides
a simple wrapper around the zero-gradient
condition, which can be used for the turbulent kinetic energy (i.e. `k`),
square-root of turbulent kinetic energy (i.e. `q`) and Reynolds stress
symmetric-tensor fields (i.e. `R`) for the cases of high Reynolds number
flow using wall functions. It is not a wall-function condition.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            kqRWallFunction;

        // Inherited entries
        ...
    }

<% assert :string_exists, "kqRWallFunctionFvPatchField.H", "kqRWallFunction" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `kqRWallFunction`               | word    | yes      | -

The inherited entries are elaborated in:

- zeroGradientFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/pitzDaily" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/kqRWallFunctions/kqRWallFunction" %>

API:

- [Foam::kqRWallFunctionFvPatchField](doxy_id://Foam::kqRWallFunctionFvPatchField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
