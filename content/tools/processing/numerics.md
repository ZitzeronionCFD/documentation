---
title: Numerics
copyright:
- Copyright (C) 2016-2023 OpenCFD Ltd.
tags:
- numerics
menu_id: numerics
license: CC-BY-NC-ND-4.0
menu_parent: processing
---

<%= page_toc %>

## Overview {#sec-guide-numerics-overview}

OpenFOAM includes a wide range of solution and scheme controls, specified
via dictionary files in the case `system` sub-directory.  These are described
by:

- [Numerical schemes](schemes): The treatment of each term in the
  system of equations is specified in the
  <%= link_to_menuid "fvschemes" %> dictionary.  This enables
  fine-grain control of e.g. temporal, gradient, divergence and interpolation
  schemes.
- [Solution methods](solvers): Case solution parameters are
  specified in the <%= link_to_menuid "fvsolution" %> dictionary.
  These include choice of linear equation solver per field variable, algorithm
  controls e.g. number of inner and outer iterations and under-relaxation.
- [Finite volume options](fvoptions): Additional run-time selectable
  physical modelling and general finite terms are prescribed in the
  [fvOptions](ref_id://fvoptions) dictionary, targeting e.g.
  acoustics, heat transfer, momentum sources, multi-region coupling, linearised
  sources/sinks and many more

## Topics {#sec-guide-numerics-topics}

- [Overset meshes](overset): framework to interpolate between multiple meshes
