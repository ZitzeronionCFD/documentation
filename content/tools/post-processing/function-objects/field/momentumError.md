---
title: momentumError
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- error
- field
- function object
- momentum
- post-processing
menu_id: function-objects-field-momentumerror
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `momentumError` function object
computes balance terms for the steady momentum equation.

### Operands

Operand       | Type             | Location
:---          |:---              |:---
input         | -                | -
output file   | -                | -
output field  | `volVectorField` | `$FOAM_CASE/<time>/<file>`

## Usage {#usage}

Example of the `momentumError` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    momentumError1
    {
        // Mandatory entries (unmodifiable)
        type            momentumError;
        libs            (fieldFunctionObjects);

        // Optional entries (runtime modifiable)
        p               <pName>;
        U               <UName>;
        phi             <phiName>;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "momentumError/momentumError.H", "momentumError" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "momentumError",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "momentumError/momentumError.H", "p" %>
<% assert :string_exists, "momentumError/momentumError.H", "U" %>
<% assert :string_exists, "momentumError/momentumError.H", "phi" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: momentumError            | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 p            | Name of pressure field              | word | no       | p
 U            | Name of velocity field              | word | no       | U
 phi          | Name of flux field                  | word | no       | phi

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/airFoil2D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/momentumError" %>

API:

- [Foam::functionObjects::momentumError](doxy_id://Foam::functionObjects::momentumError)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->