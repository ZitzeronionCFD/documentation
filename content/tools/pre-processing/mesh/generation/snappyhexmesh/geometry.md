---
title: Geometry
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry
- snappyHexMesh
menu_id: snappyhexmesh-geometry
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh
---

<%= page_toc %>

The `geometry` dictionary describes all input geometry required by
`snappyHexMesh`:

- the surface to conform to, e.g. triangulated surfaces and analytical shapes
- regions for volumetric or surface refinement
- setting constraints on internal faces to generate face sets or face zones for
  regions

The geometry section can describe triangulated surfaces or
[searchable objects](#searchable-objects). A simple
definition may take the form:

    geometry
    {
        sphere.stl  // name of the geometrical entity
        {
            type            triSurfaceMesh;
        }

        myBox       // name of the geometrical entity
        {
            type            searchableBox;
            min             (1.5 1 -0.5);
            max             (3.5 2 0.5);
        }
    };

<% assert :string_exists, "triSurfaceMesh.H", "triSurfaceMesh" %>

<% assert :string_exists, "searchableBox.H", "searchableBox" %>
<% assert :string_exists, "searchableBox.C", "min" %>
<% assert :string_exists, "searchableBox.C", "max" %>

## Triangulated surfaces {#triangulated-surfaces}

Triangulated surfaces must be located in the `$FOAM_CASE/constant/triSurface`
directory.  The following formats are supported:

- **stl** - Stereolithography ASCII
- **stlb** - Stereolithography binary
- **obj** - Wavefront object
- **vtk** - Legacy Visualisation Toolkit (VTK)
- **ftr** - OpenFOAM triangulated format
- **ac**, **smesh**, **tri** and **dx**

Some formats support multiple regions, e.g. 'SOLID' in the *stl*
format. Each surface and region in a surface becomes a separate
patch in the final mesh.

<%= insert_models "trisurfacemeshes" %>

## Searchable objects {#searchable-objects}

These objects define analytical shapes, and are treated similarly to
[triangulated surfaces](#triangulated-surfaces),
e.g. to define the geometry to mesh or refinement regions.

Options:

<%= insert_models "searchableobjects" %>
