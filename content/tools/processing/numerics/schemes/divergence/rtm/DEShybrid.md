---
title: DES hybrid divergence scheme
short_title: DES hybrid
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
- DES
menu_id: schemes-divergence-deshybrid
license: CC-BY-NC-ND-4.0
group: schemes-divergence-general
---

<%= page_toc %>

Hybrid convection scheme of Travin et al. for hybrid RAS/LES calculations.

## Properties {#properties}

- Blends two schemes based on local properties including
- wall distance
- velocity gradient
- eddy viscosity
- Originally developed for DES calculations to blend a low-dissipative scheme,
  e.g. [linear](../linear), in the  vorticity-dominated, finely-resolved regions
  and a numerically more robust, e.g. [linearUpwind](../linear-upwind),
  convection scheme in irrotational or coarsely-resolved regions.

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss DEShybrid
            linear                        // scheme 1
            linearUpwind grad(U)          // scheme 2
            0.65                          // DES coefficient, typically = 0.65
            30                            // Reference velocity scale
            2                             // Reference length scale
            0                             // Minimum sigma limit (0-1)
            1                             // Maximum sigma limit (0-1)
            1e-3;                         // Limiter of B function, typically 1e-03
    }

<% assert :string_exists, "DEShybrid.H", "DEShybrid" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/schemes/DEShybrid" %>

API:

- [Foam::DEShybrid](doxy_id://Foam::DEShybrid)

References:

- First published in *Travin et al.* <%= cite "travin_physical_2000" %>
- Original publication contained a typo for C_H3 constant. Corrected version
    with minor changes for 2 lower limiters published in *Spalart et al.*
    <%= cite "spalart_sensitivity_2012" %>
