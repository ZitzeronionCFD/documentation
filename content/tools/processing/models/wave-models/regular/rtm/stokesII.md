---
title: Stokes II
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- regular
- Stokes
- waves
menu_id: waves-regular-stokesii
license: CC-BY-NC-ND-4.0
menu_parent: models-waves
group: wave-regular
---

<%= page_toc %>

## Properties {#properties}

- Regular waves

## Model equations {#model-equations}

The wave height is modelled by the equation:

$$
    \eta =
        \frac{H}{2} \cos (k x -\omega t + \phi)
      + k \frac{H^2}{4} \frac{3 - \sigma^2}{4 \sigma^3} \cos \left(2(k x - \omega t + \phi)\right)
$$

Where:

$$H$$
: wave height

$$k$$
: wave number

$$\omega$$
: angular frequency

$$\sigma$$
: radian wave frequency

$$\phi$$
: phase shift

$$t$$
: time

## Default model coefficients {#default-coeffs}

## Usage {#usage}

Inlet patch example

    <patch>
    {
        alpha           alpha.water;
        waveModel       StokesII;
        nPaddle         1;
        waveHeight      0.05;
        waveAngle       0.0;
        rampTime        3.0;
        activeAbsorption yes;
        wavePeriod      3.0;
    }

## Further information {#further-information}

Source code:

- [Foam::waveModels::StokesII](doxy_id://Foam::waveModels::StokesII)

References:

- ...

Tutorials:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/stokesII" %>
