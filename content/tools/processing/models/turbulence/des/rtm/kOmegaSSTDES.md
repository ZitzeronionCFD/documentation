---
title: k-ω-SST Detached Eddy Simulation (DES)
short_title: kOmegaSSTDES
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- DES
- turbulence
menu_id: des-komegasstdes
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-des
group: turbulence-des
---

<%= page_toc %>

## Properties {#properties}

- Two equation model for the turbulence kinetic energy, $$ k $$, and
  turbulence specific dissipation rate, $$ \omega $$.
- Based on the [k-omega SST](ref_id://ras-komegasst) model

## Model equations {#model-equations}

The turbulence specific dissipation rate equation is given by:

$$
    \Ddt{\rho \omega}
    =
    \div \left( \rho D_\omega \grad \omega \right)
    + \rho \gamma \frac{G}{\nu}
    - \frac{2}{3} \rho \gamma \omega \left( \div \u \right)
    - \rho \beta \omega^2
    - \rho \left( F_1 - 1 \right) CD_{k \omega}
    + S_\omega,
$$

and the turbulence kinetic energy by:

$$
    \Ddt{\rho k}
    =
    \div \left( \rho D_k \grad k \right)
    + \min\left( \rho G, \left(c_1 \beta^{*}\right) \rho k \omega \right)
    - \frac{2}{3} \rho k \left( \div \u \right)
    - \rho \frac{k^{1.5}}{\tilde{d}}
    + S_k.
$$

The length scale, $$ \tilde{d} $$, is given by:

$$
    \min \left(C_{DES} \Delta, \frac{\sqrt{k}}{\beta^{*} \omega}\right)
$$

The turbulence viscosity is obtained using:

$$
    \nu_t = a_1 \frac{k}{\max (a_1 \omega_, b_1 F_{23} \tensor{S})}
$$

## Default model coefficients {#default-coeffs}

Base model coefficients:

$$\alpha_{k1}$$ | $$\alpha_{k2}$$ | $$\alpha_{\omega 1}$$ | $$\alpha_{\omega 2}$$ | $$\beta_1$$ | $$\beta_2$$ | $$\gamma_1$$ | $$\gamma_2$$
-------------|-----------|-----------|-----------------|----------------|--------------|-----|------------
        0.85 |       1.0 |       0.5 |      0.856      |      0.075     |     0.0828   | 5/9 | 0.44

$$\beta^{*}$$ | $$a_1$$ | $$b_1$$ | $$c_1$$
----------------|-----------|-----------|----------
           0.09 |      0.31 |       1.0 |     10.0

DES model coefficients:

 $$CDESkom$$ | $$CDESkeps$$
---------------|----------------
        0.82   |            0.6

## Initialisation {#initialisation}

## Usage {#usage}

The model is specified using:

    LES
    {
        turbulence      on;
        LESModel        kOmegaSSTDES;
    }

<% assert :string_exists, "LESModel.C", "LES" %>
<% assert :string_exists, "LESModel.C", "LESModel" %>
<% assert :string_exists, "kOmegaSSTDES.H", "kOmegaSSTDES" %>

## Boundary conditions {#bcs}

Inlet

- <%= link_to_menuid "boundary-conditions-fixedValue" %>

Outlet

- [Constrained outlet](ref_id://boundary-conditions-inletOutlet)

Walls

- wall functions

## Further information {#further-information}

Source code:

- [Foam::LESModels::kOmegaSSTDES](doxy_id://Foam::LESModels::kOmegaSSTDES)

References:

- *Strelets* <%= cite "strelets_detached_2001" %>

See also:

- [Base k-omega SST model](ref_id://ras-komegasst)
