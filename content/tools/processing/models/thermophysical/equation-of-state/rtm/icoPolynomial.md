---
title: Incompressible polynomial
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- equation of state
- incompressible
- polynomial
menu_id: equationOfState-icoPolynomial
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-equationOfState
group: equation-of-state-models
---

<%= page_toc %>

Density, $$ \rho $$ \[kg/m$$^3$$\]

$$
\rho = \sum_{i=1}^N c_i T^{i-1}
$$

Entropy, $$ S $$ \[J/kg/K \]

$$
S = 0
$$

Compressibility, $$ \psi $$ \[s$$^2$$/m$$^2$$\]

$$
\psi = 0
$$

## Usage

The `icoPolynomial` equation of state is specified as:

~~~
equationOfState
{
    rhoCoeffs<8>        (<c1> <c2> <c3> <c4> <c5> <c6> <c7> <c8>);
}
~~~

Where

- `rhoCoeffs<8>` : Polynomial coefficients (x8)

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/equationOfState/icoPolynomial" %>
