---
title: zeroGradient
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- zeroGradient
menu_id: function-objects-field-zerogradient
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `zeroGradient` function object creates a volume field with zero-gradient
boundary conditions from another volume field.

The result can be used, for example, to post-process near-wall field values.

### Operands

Operand       | Type              | Location
:---          |:---               |:---
input         | `vol<Type>Field`  | `$FOAM_CASE/<time>/<inpField>`
output file   | -                 | -
output field  | `vol<Type>Field`  | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `zeroGradient` function object by using `functions`
sub-dictionary in `system/controlDict` file:

    zeroGradient1
    {
        // Mandatory entries (unmodifiable)
        type            zeroGradient;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        fields          (<field1> ... <fieldN>); \\(U "(T|k|epsilon|omega)");

        // Optional entries (runtime modifiable)
        result          @@<name>;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "zeroGradient.H", "zeroGradient" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "zeroGradient",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "zeroGradient.C", "fields" %>
<% assert :string_exists, "zeroGradient.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: zeroGradient             | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 fields       | Name of the operand fields          | wordList | yes | -
 result       | Name of the output field            | word | no | zeroGradient(@@)

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

### Notes on entries

A list of fields can contain exact names or regular expressions.
The token '\@\@' in the result name is replaced by the name of the source
field. In the special case of a single source field (specified as
a non-regex), the '\@\@' token checking is suppressed.

The function object will skip over fields that would not benefit e.g.
`processor`, `empty`, `zeroGradient`, `symmetry` patches.
This check should also prevent processing fields multiple times.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/zeroGradient" %>

API:

- [Foam::functionObjects::zeroGradient](doxy_id://Foam::functionObjects::zeroGradient)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->