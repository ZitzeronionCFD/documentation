---
title: acousticWaveTransmissive
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- outlet
menu_id: boundary-conditions-acoustic-wave-transmissive
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-outlet
group: bcs-outlet-general
---

<%= page_toc %>

## Description

The `acousticWaveTransmissive` boundary condition provides a wave transmissive
outflow condition by solving the following governing equation at the boundary:

$$
    \frac{D \, W f}{D t} = 0
$$

where $$W$$ represents the wave speed and $$f$$ corresponds to the field to
which this boundary condition is applied.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type                patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                acousticWaveTransmissive;
        advectiveSpeed      <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "acousticWaveTransmissiveFvPatchField.C", "advectiveSpeed" %>

Property    | Description       | Type | Required  | Default
------------|-------------------|---------|--------|--------
`type`      | Type name: `acousticWaveTransmissive` | word | yes  | -
`advectiveSpeed`      | Advective speed value | scalar | yes  | -

The inherited entries are elaborated in:

- [Foam::advectiveFvPatchField](doxy_id://Foam::advectiveFvPatchField)

<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/acousticFoam/obliqueAirJet" %>

\ofAssert patchTypeExists compressible/acousticFoam/obliqueAirJet acousticWaveTransmissive

Source code:

- [Foam::acousticWaveTransmissiveFvPatchField](doxy_id://Foam::acousticWaveTransmissiveFvPatchField)

<%= history "1.5" %>


<!----------------------------------------------------------------------------->
