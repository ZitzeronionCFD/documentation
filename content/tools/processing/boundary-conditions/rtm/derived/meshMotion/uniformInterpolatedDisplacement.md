---
title: uniformInterpolatedDisplacement
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-uniformInterpolatedDisplacement
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-meshMotion
group: bcs-meshMotion
---

<%= page_toc %>

## Description

The `uniformInterpolatedDisplacement` is a boundary condition that
interpolates a pre-specified motion which is specified as `pointVectorFields`.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            uniformInterpolatedDisplacement;
        field           <word>;
        interpolationScheme  <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "uniformInterpolatedDisplacementPointPatchVectorField.H", "uniformInterpolatedDisplacement" %>
<% assert :string_exists, "uniformInterpolatedDisplacementPointPatchVectorField.C", "field" %>
<% assert :string_exists, "uniformInterpolatedDisplacementPointPatchVectorField.C", "interpolationScheme" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `uniformInterpolatedDisplacement` | word  | yes      | -
`field`        | Name of displacement field                 | word    | yes      | -
`interpolationScheme` | Name of interpolation scheme        | word    | yes      | -

The inherited entries are elaborated in:

- fixedValuePointPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/fvMotionSolver/pointPatchFields/derived/uniformInterpolatedDisplacement" %>

API:

- [Foam::uniformInterpolatedDisplacementPointPatchVectorField](doxy_id://Foam::uniformInterpolatedDisplacementPointPatchVectorField)

<%= history "2.1.1" %>

<!----------------------------------------------------------------------------->
