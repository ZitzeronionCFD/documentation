---
title: Spalart-Allmaras Delayed Detached Eddy Simulation (DDES)
short_title: SpalartAllmarasDDES
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- DES
- DDES
- turbulence
menu_id: des-spalartallmarasddes
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-des
group: turbulence-des
---

<%= page_toc %>

## Properties {#properties}

- One equation model based on a modified turbulence viscosity,
  $$ \tilde{\nu} $$
- based on the [Spalart-Allmaras DES](ref_id://des-spalartallmarasdes)
  model.

## Model equations {#model-equations}

The model equations are the same as used by the
[DES variant](<%= ref_id "des-spalartallmarasdes", "model-equations" %>)
of the model, with a different approximation for $$ \tilde{d} $$:

$$
    \tilde{d}
    =
    \max \left[ L_{RAS} - f_d, \max (L_{RAS} - L_{LES}, 0) \right]
$$

The [RAS](ref_id://turbulence-ras) length scale is:

$$
    L_{RAS} = y
$$

and the [LES](ref_id://turbulence-les) length:

$$
    L_{LES} = \Psi C_{DES} \Delta
$$

The delay function is given by:

$$
    f_d = 1 - \tanh \left[ \left( C_{d1} r_d \right)^{C_{d2}} \right]
$$

Here, when $$ f_d = 0 $$ [RAS](ref_id://turbulence-ras) is recovered, and
when $$ f_d = 1 $$ the [DES](ref_id://des-spalartallmarasdes)
mode is recovered.  The $$ r_d $$ parameter is given by:

$$
    r_d = \min \left( \frac{\nu_{\mathit{eff}}}{\mag {\grad \u} \kappa^2 y^2}, 10\right)
$$

## Default model coefficients {#default-coeffs}

## Initialisation {#initialisation}

## Usage {#usage}

The model is specified using:

    LES
    {
        turbulence      on;
        LESModel        SpalartAllmarasDDES;
    }

<% assert :string_exists, "LESModel.C", "LES" %>
<% assert :string_exists, "LESModel.C", "LESModel" %>
<% assert :string_exists, "SpalartAllmarasDDES.H", "SpalartAllmarasDDES" %>

## Boundary conditions {#bcs}

Inlet

- <%= link_to_menuid "boundary-conditions-fixedValue" %>

Outlet

- [Constrained outlet](ref_id://boundary-conditions-inletOutlet)

Walls

- wall functions

## Further information {#further-information}

Source code:

- [Foam::LESModels::SpalartAllmarasDDES](doxy_id://Foam::LESModels::SpalartAllmarasDDES)

References:

- *Spalart et al.* <%= cite "spalart_new_2006" %>
