---
title: uncoupledSixDoFRigidBodyDisplacement
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-uncoupledSixDoFRigidBodyDisplacement
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-meshMotion
group: bcs-meshMotion
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            uncoupledSixDoFRigidBodyDisplacement;

        // Optional entries
        initialPoints   <pointField>;

        // Inherited entries
        // sixDoFRigidBodyMotion settings
        ...
    }

<% assert :string_exists, "uncoupledSixDoFRigidBodyDisplacementPointPatchVectorField.H", "uncoupledSixDoFRigidBodyDisplacement" %>
<% assert :string_exists, "uncoupledSixDoFRigidBodyDisplacementPointPatchVectorField.H", "initialPoints" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `uncoupledSixDoFRigidBodyDisplacement`    | word     | yes | -
`initialPoints` | Initial positions of points on the patch  | pointField | no    | -

The inherited entries are elaborated in:

- fixedValuePointPatchField.H
- sixDoFRigidBodyMotion.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/sixDoFRigidBodyMotion/pointPatchFields/derived/uncoupledSixDoFRigidBodyDisplacement" %>

API:

- [Foam::uncoupledSixDoFRigidBodyDisplacementPointPatchVectorField](doxy_id://Foam::uncoupledSixDoFRigidBodyDisplacementPointPatchVectorField)

<%= history "2.3.0" %>

<!----------------------------------------------------------------------------->
