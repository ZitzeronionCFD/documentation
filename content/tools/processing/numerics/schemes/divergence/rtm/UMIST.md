---
title: UMIST divergence scheme
short_title: UMIST
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-umist
license: CC-BY-NC-ND-4.0
group: schemes-divergence-nvdtvd
---

<%= page_toc %>

## Properties {#properties}

- <B>U</B>pstream <B>M</B>onotonic <B>I</B>nterpolation for <B>S</B>calar
  <B>T</B>ransport
- Second order
- Unbounded
- By *Lien and Leschzinger* <%= cite "lien_upstream_1994" %>

## Normalised Variable Diagram {#nvd}

![UMIST](../umist.png)

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss UMIST;
    }

<% assert :string_exists, "UMIST.H", "UMIST" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/limitedSchemes/UMIST" %>

API:

- [Foam::UMISTLimiter](doxy_id://Foam::UMISTLimiter)
