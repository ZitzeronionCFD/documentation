---
title: filmHeightInletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-filmHeightInletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-region
group: bcs-region
---

<%= page_toc %>

## Description

The `filmHeightInletVelocity` is boundary condition designed to be used in
conjunction with surface film modelling.  It provides a velocity inlet boundary
condition for patches where the film height is specified.  The inflow velocity is
obtained from the flux with a direction normal to the patch faces using:

$$
    \u_p = \frac{\vec{n} \phi}{\rho |\vec{S}_f| \delta}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\u_p$$              | Patch velocity \[m/s\]
$$\vec{n}$$           | Patch normal vector
$$\phi$$              | Mass flux \[kg/s\]
$$\rho$$              | Density \[kg/m^3\]
$$\vec{S}_f$$         | Patch face area vectors \[m^2\]
$$\delta$$            | Film height \[m\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            filmHeightInletVelocity;

        // Optional entries
        phi             <word>;
        rho             <word>;
        deltaf          <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "filmHeightInletVelocityFvPatchVectorField.H", "filmHeightInletVelocity" %>
<% assert :string_exists, "filmHeightInletVelocityFvPatchVectorField.C", "phi" %>
<% assert :string_exists, "filmHeightInletVelocityFvPatchVectorField.C", "rho" %>
<% assert :string_exists, "filmHeightInletVelocityFvPatchVectorField.C", "deltaf" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `filmHeightInletVelocity`       | word    | yes      | -
`phi`          | Name of flux field                         | word    | yes      | -
`rho`          | Name of density field                      | word    | yes      | -
`deltaf`       | Name of height field                       | word    | yes      | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/regionModels/surfaceFilmModels/derivedFvPatchFields/filmHeightInletVelocity" %>

API:

- [Foam::filmHeightInletVelocityFvPatchVectorField](doxy_id://Foam::filmHeightInletVelocityFvPatchVectorField)

<%= history "2.0.0" %>

<!----------------------------------------------------------------------------->
