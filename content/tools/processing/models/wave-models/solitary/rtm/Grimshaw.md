---
title: Grimshaw
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- Grimshaw
- regular
- waves
menu_id: waves-regular-grimshaw
license: CC-BY-NC-ND-4.0
menu_parent: models-waves
group: wave-solitary
---

<%= page_toc %>

## Properties {#properties}

- Solitary wave

## Model equations {#model-equations}

The wave height is modelled by the equation:

$$
    \eta =
        1
      + \epsilon s^2
      + \frac{3}{4} \epsilon^2 s^2 t^2
      + \epsilon^3 \left( \frac{5}{8} s^2 t^2 - \frac{101}{80} s^4 t^2\right)
$$

and

$$
    \epsilon = \frac{a}{h}; s = \mathrm{sech} (\alpha x); t = \tanh(\alpha x);
    \alpha = \sqrt{\frac{3}{4} \epsilon} \left( 1 - \frac{5}{8} \epsilon + \frac{71}{128}\epsilon^2 \right)
$$

Where:

$$h$$
: water depth

$$a$$
: wave amplitude

$$t$$
: time

## Default model coefficients {#default-coeffs}

## Usage {#usage}

Inlet patch example

    <patch>
    {
        alpha           alpha.water;
        waveModel       Grimshaw;
        nPaddle         1;
        waveHeight      0.05;
        waveAngle       0.0;
        activeAbsorption no;
    }

## Further information {#further-information}

Source code:

- [Foam::waveModels::Grimshaw](doxy_id://Foam::waveModels::Grimshaw)

References:

- ...

Tutorials:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/solitaryGrimshaw" %>
