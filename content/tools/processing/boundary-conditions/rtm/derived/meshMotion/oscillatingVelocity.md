---
title: oscillatingVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-oscillatingVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-meshMotion
group: bcs-meshMotion
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            oscillatingVelocity;
        amplitude       <vector>;
        omega           <scalar>;

        // Optional entries
        p0              <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "oscillatingVelocityPointPatchVectorField.H", "oscillatingVelocity" %>
<% assert :string_exists, "oscillatingVelocityPointPatchVectorField.C", "amplitude" %>
<% assert :string_exists, "oscillatingVelocityPointPatchVectorField.C", "omega" %>
<% assert :string_exists, "oscillatingVelocityPointPatchVectorField.C", "p0" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `oscillatingVelocity`           | word    | yes     | -
`amplitude`    | Oscillation amplitude                      | scalar  | yes     | -
`omega`        | Rotation speed                             | scalar  | yes     | -
`p0`           | Initial displacement field                 | pointField | no   | -

The inherited entries are elaborated in:

- fixedValuePointPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/fvMotionSolver/pointPatchFields/derived/oscillatingVelocity" %>

API:

- [Foam::oscillatingVelocityPointPatchVectorField](doxy_id://Foam::oscillatingVelocityPointPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
