---
title: turbulentMixingLengthDissipationRateInlet
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-turbulentMixingLengthDissipationRateInlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
group: bcs-inletoutlet
---

<%= page_toc %>

## Description

The `turbulentMixingLengthDissipationRateInlet` is a boundary condition that
calculates turbulent kinetic energy dissipation rate, i.e. `epsilon`,
based on a specified mixing length.  The patch values are calculated using:

$$
    \epsilon_p = \frac{C_{\mu}^{0.75} k^{1.5}}{L}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\epsilon_p$$        | Patch epsilon values     \[m^2/s^3\]
$$C_\mu$$             | Empirical model constant retrieved from turbulence model
$$k$$                 | Turbulent kinetic energy \[m^2/s^2\]
$$L$$                 | Mixing length scale      \[m\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            turbulentMixingLengthDissipationRateInlet;
        mixingLength    <scalar>;

        // Optional entries
        Cmu             <scalar>;
        k               <word>;
        phi             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "turbulentMixingLengthDissipationRateInletFvPatchScalarField.H", "turbulentMixingLengthDissipationRateInlet" %>
<% assert :string_exists, "turbulentMixingLengthDissipationRateInletFvPatchScalarField.C", "mixingLength" %>
<% assert :string_exists, "turbulentMixingLengthDissipationRateInletFvPatchScalarField.C", "Cmu" %>
<% assert :string_exists, "turbulentMixingLengthDissipationRateInletFvPatchScalarField.C", "k" %>
<% assert :string_exists, "turbulentMixingLengthDissipationRateInletFvPatchScalarField.C", "phi" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `turbulentMixingLengthDissipationRateInlet` | word   | yes      | -
`mixingLength` | Mixing length scale \[m\]                  | scalar  |  yes     | -
`Cmu`          | Empirical model constant                   | scalar  |  no      | 0.09
`phi`          | Name of flux field                         | word    |  no      | phi
`k`            | Name of turbulent kinetic energy field     | word    | no       | k

The inherited entries are elaborated in:

- inletOutletFvPatchFields.H
<!-- end of the list -->

- The boundary condition is derived from `inletOutlet` condition.
  Therefore, in the event of reverse flow, a zero-gradient condition
  is applied.
- The order of precedence to input the empirical model constant `Cmu` is:
  turbulence model, boundary condition dictionary, and default value=0.09.
- The empirical model constant `Cmu` is not a spatiotemporal variant field.
  Therefore, the use of the boundary condition may not be fully consistent
  with the turbulence models where `Cmu` is a variant field, such as
  `realizableKE` closure model in this respect. Nevertheless, workflow
  observations suggest that the matter poses no importance.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/RAS/TJunctionAverage" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/RAS/derivedFvPatchFields/turbulentMixingLengthDissipationRateInlet" %>

API:

- [Foam::turbulentMixingLengthDissipationRateInletFvPatchScalarField](doxy_id://Foam::turbulentMixingLengthDissipationRateInletFvPatchScalarField)

<%= history "2.2.2" %>

<!----------------------------------------------------------------------------->
