---
title: Verification and Validation
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- example
- verification
- validation
menu_id: examples-verification-validation
license: CC-BY-NC-ND-4.0
menu_parent: examples
---

<%= page_toc %>

## Introduction {#introduction}

The following sections provide links to OpenFOAM tutorial cases where the
predictions are compared to reference data sets.

- [Laminar flow](laminar)
- [Turbulent flow](turbulent)
- [Heat transfer](heat-transfer)
- [Combustion](combustion)
- [Chemistry](chemistry)
