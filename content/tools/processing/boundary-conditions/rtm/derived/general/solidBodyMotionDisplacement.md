---
title: solidBodyMotionDisplacement
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-solidBodyMotionDisplacement
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `solidBodyMotionDisplacement` is a boundary condition that
enables the specification of a fixed value boundary condition using the
solid body motion functions for point displacements.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            solidBodyMotionDisplacement;
        solidBodyMotionFunction    <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "solidBodyMotionDisplacementPointPatchVectorField.H", "solidBodyMotionDisplacement" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `solidBodyMotionDisplacement` | word | yes | -

The inherited entries are elaborated in:

- fixedValuePointPatchFields.H
- solidBodyMotionFunction.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/mesh/moveDynamicMesh/relativeMotion/box2D_moveDynamicMesh" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/dynamicMesh/motionSolvers/displacement/solidBody/pointPatchFields/derived/solidBodyMotionDisplacement" %>

API:

- [Foam::solidBodyMotionDisplacementPointPatchVectorField](doxy_id://Foam::solidBodyMotionDisplacementPointPatchVectorField)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
