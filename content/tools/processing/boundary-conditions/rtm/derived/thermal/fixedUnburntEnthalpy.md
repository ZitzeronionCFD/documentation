---
title: fixedUnburntEnthalpy
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-fixedUnburntEnthalpy
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `fixedUnburntEnthalpy` is a boundary condition that provides
a fixed boundary condition for unburnt enthalpy.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedUnburntEnthalpy;

        // Inherited entries
        ...
    }

<% assert :string_exists, "fixedUnburntEnthalpyFvPatchScalarField.H", "fixedUnburntEnthalpy" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `fixedUnburntEnthalpy`          | word    | yes      | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/reactionThermo/derivedFvPatchFields/fixedUnburntEnthalpy" %>

API:

- [Foam::fixedUnburntEnthalpyFvPatchScalarField](doxy_id://Foam::fixedUnburntEnthalpyFvPatchScalarField)

<%= history "1.6" %>

<!----------------------------------------------------------------------------->
