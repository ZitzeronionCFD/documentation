---
title: pressureInletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-pressureInletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `pressureInletVelocity` is a velocity boundary condition where
the inflow velocity is obtained from the flux with a direction normal
to the patch faces.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type           pressureInletVelocity;

        // Optional entries
        phi            <word>;
        rho            <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "pressureInletVelocityFvPatchVectorField.H", "pressureInletVelocity" %>
<% assert :string_exists, "pressureInletVelocityFvPatchVectorField.C", "phi" %>
<% assert :string_exists, "pressureInletVelocityFvPatchVectorField.C", "rho" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `pressureInletVelocity`         | word    | yes      | -
`phi`                  | Name of the flux field                     | word    | no       | phi
`rho`                  | Name of the density field                  | word    | no       | rho

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

- If reverse flow is possible or expected use the
  `pressureInletOutletVelocity` condition instead.
  {: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/laminar/helmholtzResonance" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/pressureInletVelocity" %>

API:

- [Foam::pressureInletVelocityFvPatchVectorField](doxy_id://Foam::pressureInletVelocityFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
