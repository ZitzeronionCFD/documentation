---
title: oscillatingDisplacement
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-oscillatingDisplacement
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-meshMotion
group: bcs-meshMotion
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            oscillatingDisplacement;
        amplitude       <vector>;
        omega           <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "oscillatingDisplacementPointPatchVectorField.H", "oscillatingDisplacement" %>
<% assert :string_exists, "oscillatingDisplacementPointPatchVectorField.C", "amplitude" %>
<% assert :string_exists, "oscillatingDisplacementPointPatchVectorField.C", "omega" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `oscillatingDisplacement`           | word    | yes     | -
`amplitude`    | Oscillation amplitude                      | scalar  | yes     | -
`omega`        | Rotation speed                             | scalar  | yes     | -

The inherited entries are elaborated in:

- fixedValuePointPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/potentialFreeSurfaceDyMFoam/oscillatingBox" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/fvMotionSolver/pointPatchFields/derived/oscillatingDisplacement" %>

API:

- [Foam::oscillatingDisplacementPointPatchVectorField](doxy_id://Foam::oscillatingDisplacementPointPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
