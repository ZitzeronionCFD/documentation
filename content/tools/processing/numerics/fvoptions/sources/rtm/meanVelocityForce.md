---
title: Mean velocity force
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- momentum
- source
menu_id: fvoption-meanvelocityforce
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-momentum
---

<%= page_toc %>

## Properties {#properties}

- Applies a force to maintain a user-specified volume-averaged mean velocity,
  e.g. useful for channel flow cases

## Usage {#usage}

The option is specified using:

    meanVelocity1
    {
        type            meanVelocityForce;
        selectionMode   all;
        fields          (U);
        Ubar            (10 0 0);
    }

<% assert :string_exists, "meanVelocityForce.H", "meanVelocityForce" %>
<% assert :string_exists, "meanVelocityForce.C", "fields" %>
<% assert :string_exists, "meanVelocityForce.C", "Ubar" %>

Currently only available to incompressible solvers
{: .note}

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/periodicPlaneChannel" %>

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/derived/meanVelocityForce" %>
