---
title: surfaceInterpolate
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- interpolation
- post-processing
- surface
menu_id: function-objects-field-surfaceinterpolate
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `surfaceInterpolate` function object
linearly interpolates volume fields to generate surface fields.

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `vol<Type>Field`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `surface<Type>Field` | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `surfaceInterpolate` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    surfaceInterpolate1
    {
        // Mandatory entries (unmodifiable)
        type            surfaceInterpolate;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        fields      ((<inpField1> <outField1>) ... (<inpFieldN> <outFieldN>));

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists,
   "surfaceInterpolate/surfaceInterpolate.H", "surfaceInterpolate" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "surfaceInterpolate",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "surfaceInterpolate/surfaceInterpolate.C", "fields" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: surfaceInterpolate       | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 fields       | Names of operand and output fields  | wordList | yes  | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/surfaceInterpolate" %>

API:

- [Foam::functionObjects::surfaceInterpolate](doxy_id://Foam::functionObjects::surfaceInterpolate)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->