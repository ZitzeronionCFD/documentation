# frozen_string_literal: true

# Removes 'ofAssert' lines used for assertions
class RemoveOfAssertFilter < Nanoc::Filter
  identifier :remove_of_assert

  def run(content, _params)
    out = +''
    content.split("\n").each do |line|
      out << line << "\n" unless line.include? 'ofAssert'
    end

    out
  end
end
