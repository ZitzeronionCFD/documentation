---
title: fixedMean
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-fixedMean
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-general
---

<%= page_toc %>

## Description

The `fixedMean` is a general boundary condition to extrapolate a field to
the patch using the near-cell values and adjusts the distribution to match the
specified, optionally time-varying, mean value.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedMean;
        meanValue       <Function1<Type>>;

        // Inherited entries
        ...
    }

where:

Property    | Description                                | Type    | Required | Default
------------|--------------------------------------------|---------|----------|---------
`type`      | Type name: `fixedMean`                     | word    | yes      | -
`meanValue` | Mean value that the field is adjusted to maintain | Function1\<Type\> | yes  | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/laminar/helmholtzResonance" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/wallMountedHump" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fixedMean" %>

API:

- [Foam::fixedMeanFvPatchField](doxy_id://Foam::fixedMeanFvPatchField)

<%= history "2.1.1" %>

<!----------------------------------------------------------------------------->
