---
title: applyBoundaryLayer
tags:
- boundary layer
- initialisation
- turbulence
- wall
copyright:
- Copyright 2022 (C) OpenCFD Ltd.
menu_id: applyBoundaryLayer
license: CC-BY-NC-ND-4.0
menu_parent: pre-processing-field
group: pre-processing-field
---

<%= page_toc %>

## Overview {#overview}

The `applyBoundaryLayer` utility sets the velocity and turbulence-field profiles
near to wall patches based on the one-seventh power law.

## Usage

### Synopsis

<%= help_util_usage "applyBoundaryLayer" %>

#### Examples

Specify the boundary-layer thickness of 0.1 \[m\] and write optional turbulence
fields:

~~~
applyBoundaryLayer -ybl 0.1 -writeTurbulenceFields
~~~


### Input

#### Arguments

<%= help_util_arguments "applyBoundaryLayer" %>

#### Options

<%= help_util_options "applyBoundaryLayer" %>

#### Files

<%= no_input "file" %>

#### Fields

<%= no_input "field" %>

### Output

#### Logs

<%= no_output "log" %>

#### Files

<%= no_output "file" %>

#### Fields

By default, the path to write the fields is `<case>/<time>/` and the utility
will overwrite the local velocity field. In addition, the
`-writeTurbulenceFields` option can be used to evaluate turbulence fields
using the new velocity profile for:

Property  | Description                 | Type
----------|-----------------------------|-----
`nut`     | Turbulence viscosity        | volScalarField
`epsilon` | Turbulence kinetic energy dissipation rate | volScalarField
`k`       | Turbulence kinetic energy   | volScalarField
`omega`   | Specific dissipation rate   | volScalarField
`nuTilda` | Modified turbulence viscosit | volScalarField

## Method

$$
\frac{u}{U} = \left(\frac{y}{\delta}\right)^{\frac{1}{7}}
$$

Where

$$u$$
: boundary layer velocity, \[m/s\]

$$U$$
: freestream velocity, \[m/s\]

$$y$$
: normal wall distance, \[m\]

$$\delta$$
: boundary layer thickness, \[m\]

The boundary layer thickness is user defined as either:

- a fixed height
- proportional to the average wall distance for the entire mesh

For example, the boundary layer profile using a freestream velocity of 1 m/s
and boundary layer height of 0.05 m is shown below.

![boundary layer profile](../applyBoundaryLayer.png)

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/wallMountedHump" %>

Source code:

- <%= repo_link2 "$FOAM_UTILITIES/preProcessing/applyBoundaryLayer" %>

<%= history "1.5" %>

<!----------------------------------------------------------------------------->