---
title: atmPlantCanopyTSource
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- atmospheric
- fvOption
- source
- temperature
menu_id: fvoption-atmplantcanopytsource
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-atm
---

<%= page_toc %>

## Properties {#properties}

- The `atmPlantCanopyTSource` applies sources on temperature, i.e. `T`, to
  incorporate effects of plant canopy for atmospheric boundary layer modelling.
- The `atmPlantCanopyTSource` can be applied on any RAS turbulence model.
- The `atmPlantCanopyTSource` inherits the traits of the
  [fvOption](ref_id://fvoptions), and `cellSetOption`.

Corrections applied to:

`T`
: Temperature \[K\]

Required fields:

`T`
: Temperature \[K\]

`qPlant`
: Tree-height based specific heat flux \[m$$^2$$/s$$^3$$\]

## Model equations {#model-equations}

The model expression:

$$
    f_M += \alpha \rho \frac{q_{plant}}{C_{p_0}}
$$

Where:

$$f_M$$
: fvMatrix of the temperature field \[K/s\]

$$q_{plant}$$
: Tree-height based specific heat flux \[m2/s3\]

$$C_{p_0}$$
: Specific heat capacity \[m2/s2/K\]

$$\alpha$$
: Phase fraction in multiphase computations, otherwise equals to 1

$$\rho$$
: Fluid density in compressible computations, otherwise equals to 1

## Usage {#usage}

Example of the fvOptions specification using `constant/fvOptions` file:

    atmPlantCanopyTSource1
    {
        // Mandatory entries (unmodifiable)
        type                  atmPlantCanopyTSource;

        atmPlantCanopyTSourceCoeffs
        {
            // Mandatory (inherited) entries (unmodifiable)
            selectionMode    all;

            // Optional entries (unmodifiable)
            rho             rho;
            Cp0             1005.0;
        }

        // Optional (inherited) entries
        ...
    }

<% assert :string_exists, "atmPlantCanopyTSource.H", "atmPlantCanopyTSource" %>
<% assert :string_exists, "atmPlantCanopyTSource.C", "rho" %>
<% assert :string_exists, "atmPlantCanopyTSource.C", "Cp0" %>
<% assert :string_exists, "cellSetOption.C", "selectionMode" %>
<% assert :string_exists, "cellSetOption.C", "cellZone" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: atmPlantCanopyTSource    | word | yes      | -
 rho          | Name of density field               | word | no       | rho
 Cp0          | Specific heat capacity \[m2/s2/K\]    | scalar | no     | 1005.0

The inherited entries are elaborated in:

- [fvOption](ref_id://fvoptions)
  - `cellSetOption`

## Further information {#further-information}

Tutorials

- <%= repo_link2(
  "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmFlatTerrain")%>
- <%= repo_link2(
  "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmForestStability") %>

Source code

- <%= repo_link2(
  "$FOAM_SRC/atmosphericModels/fvOptions/atmPlantCanopyTSource") %>

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
