---
title: randomise
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- random
menu_id: function-objects-field-randomise
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `randomise` function object
adds a random component to an input field,
with a specified perturbation magnitude.

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `vol<Type>Field`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `vol<Type>Field`     | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `randomise` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    randomise1
    {
        // Mandatory entries (unmodifiable)
        type            randomise;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        magPerturbation 0.1;

        // Mandatory (inherited) entries (runtime modifiable)
        field           <inpField>;

        // Optional (inherited) entries
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "randomise.H", "randomise" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files", "randomise", "libfieldFunctionObjects" %>
<% assert :string_exists, "randomise.H", "magPerturbation" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: randomise                | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 magPerturbation | The magnitude of the perturbation | scalar |  yes  | -
 field        | Name of the operand field           | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func "randomise(<field>)"

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>
- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/reactingParcelFoam/splashPanel" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/randomise" %>

API:

- [Foam::functionObjects::randomise](doxy_id://Foam::functionObjects::randomise)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
