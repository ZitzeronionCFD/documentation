---
title: sixDoFRigidBodyDisplacement
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-sixDoFRigidBodyDisplacement
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-meshMotion
group: bcs-meshMotion
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            sixDoFRigidBodyDisplacement;

        // Optional entries
        rho             <word>;
        g               <vector>;
        initialPoints   <pointField>;

        // Conditional entries

            // if 'rho rhoInf;'
            rhoInf      <scalar>;

        // Inherited entries
        // sixDoFRigidBodyMotion settings
        ...
    }

<% assert :string_exists, "sixDoFRigidBodyDisplacementPointPatchVectorField.H", "sixDoFRigidBodyDisplacement" %>
<% assert :string_exists, "sixDoFRigidBodyDisplacementPointPatchVectorField.H", "rho" %>
<% assert :string_exists, "sixDoFRigidBodyDisplacementPointPatchVectorField.H", "g" %>
<% assert :string_exists, "sixDoFRigidBodyDisplacementPointPatchVectorField.H", "initialPoints" %>
<% assert :string_exists, "sixDoFRigidBodyDisplacementPointPatchVectorField.H", "rhoInf" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `sixDoFRigidBodyDisplacement`   | word    | yes      | -
`rho`          | Name of density field                      | word    | no       | rho
`g`            | Gravitational acceleration                 | vector  | no       | -
`initialPoints` | Initial positions of points on the patch  | pointField | no    | -
`rhoInf`       | Value of density                           | scalar  | conditional | -

The inherited entries are elaborated in:

- fixedValuePointPatchField.H
- sixDoFRigidBodyMotion.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/compressibleInterDyMFoam/laminar/sphereDrop" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/sixDoFRigidBodyMotion/pointPatchFields/derived/sixDoFRigidBodyDisplacement" %>

API:

- [Foam::sixDoFRigidBodyDisplacementPointPatchVectorField](doxy_id://Foam::sixDoFRigidBodyDisplacementPointPatchVectorField)

<%= history "2.3.0" %>

<!----------------------------------------------------------------------------->
