---
title: Viscous dissipation
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- dissipation
- energy
- fvOption
- source
menu_id: fvoption-viscousdissipation
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-energy
---

<%= page_toc %>

## Properties {#properties}

## Usage {#usage}

The option is specified using:

    dissipation1
    {
        type            viscousDissipation;
        selectionMode   cellSet;
        cellSet         sourceCells1;
    }

<% assert :string_exists, "viscousDissipation.H", "viscousDissipation" %>

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/RAS/TJunction" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/TJunctionArrheniusBirdCarreauTransport" %>
- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/multiphaseInterFoam/laminar/mixerVessel2D" %>

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/derived/viscousDissipation" %>
