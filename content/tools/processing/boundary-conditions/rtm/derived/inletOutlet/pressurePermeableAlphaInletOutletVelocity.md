---
title: pressurePermeableAlphaInletOutletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-pressurePermeableAlphaInletOutletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `pressurePermeableAlphaInletOutletVelocity` is a velocity inlet/outlet boundary
condition that can be applied to velocity boundaries for multiphase flows when
the pressure boundary condition is specified.

In this condition , an open condition is applied when `alpha` is under a
user-defined `alphaMin` value and a wall condition is applied when `alpha`
is larger than the `alphaMin`.

This boundary condition can be used in conjunction with
`prghPermeableAlphaTotalPressure` for the `p_rgh` variable.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            pressurePermeableAlphaInletOutletVelocity;

        // Optional entries
        phi             <word>;
        rho             <word>;
        alpha           <word>;
        alphaMin        <scalar>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "pressurePermeableAlphaInletOutletVelocityFvPatchVectorField.H", "pressurePermeableAlphaInletOutletVelocity" %>
<% assert :string_exists, "pressurePermeableAlphaInletOutletVelocityFvPatchVectorField.C", "phi" %>
<% assert :string_exists, "pressurePermeableAlphaInletOutletVelocityFvPatchVectorField.C", "rho" %>
<% assert :string_exists, "pressurePermeableAlphaInletOutletVelocityFvPatchVectorField.C", "alpha" %>
<% assert :string_exists, "pressurePermeableAlphaInletOutletVelocityFvPatchVectorField.C", "alphaMin" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `pressurePermeableAlphaInletOutletVelocity` | word   | yes | -
`phi`          | Name of flux field                         | word    | no       | phi
`rho`          | Name of density field                      | word    | no       | rho
`alpha`        | Name of the mixture VOF field              | word    | no       | none
`alphaMin`     | Minimum alpha value to outlet blockage     | scalar  | no       | 1.0

The inherited entries are elaborated in:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/pressurePermeableAlphaInletOutletVelocity" %>

API:

- [Foam::pressurePermeableAlphaInletOutletVelocityFvPatchVectorField](doxy_id://Foam::pressurePermeableAlphaInletOutletVelocityFvPatchVectorField)

<%= history "v2106" %>

<!----------------------------------------------------------------------------->
