---
title: uniformNormalFixedValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-uniformNormalFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `uniformNormalFixedValue` is a boundary condition that
provides a uniform surface-normal vector condition by its magnitude.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            uniformNormalFixedValue;
        uniformValue    <PatchFunction1<Type>>;

        // Optional entries
        ramp            <Function1<scalar>>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "uniformNormalFixedValueFvPatchVectorField.H", "uniformNormalFixedValue" %>
<% assert :string_exists, "uniformNormalFixedValueFvPatchVectorField.C", "uniformValue" %>
<% assert :string_exists, "uniformNormalFixedValueFvPatchVectorField.C", "ramp" %>

where:

Property       | Description                          | Type    | Required | Default
---------------|--------------------------------------|---------|----------|---------
`type`         | Type name: `uniformNormalFixedValue` | word    | yes      | -
`uniformValue` | Patch field values                   | PatchFunction1\<Type\>  | yes | -
`ramp`         | Time-variant ramp                    | Function1\<scalar\>     | no  | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- PatchFunction1.H
- Function1.H
<!-- end of the list -->

- Sign conventions:
  - the value is positive for outward-pointing vectors
  {: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/chtMultiRegionFoam/windshieldCondensation" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/uniformNormalFixedValue" %>

API:

- [Foam::uniformNormalFixedValueFvPatchVectorField](doxy_id://Foam::uniformNormalFixedValueFvPatchVectorField)

<%= history "v1912" %>

<!----------------------------------------------------------------------------->
