---
title: mappedMixed
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-mappedMixed
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `mappedMixed` is a generic boundary condition that maps the value at a
set of cells or patch faces back to the local patch.

The sample mode is set by the underlying mapping engine, provided by the
`mappedPatchBase` class.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            mappedPatch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    mappedMixed;

        // Optional entries
        weightField             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "mappedMixedFvPatchField.H", "mappedMixed" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `mappedMixed`                   | word    | yes      | -
`weightField`          | Name of weight field to sample             | word    | no       | null

The inherited entries are elaborated in:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
- [Foam::mappedPatchFieldBase](doxy_id://Foam::mappedPatchFieldBase)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/basic/laplacianFoam/multiWorld2" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/mappedMixed" %>

API:

- [Foam::mappedMixedFvPatchField](doxy_id://Foam::mappedMixedFvPatchField)

<%= history "v2012" %>

<!----------------------------------------------------------------------------->
