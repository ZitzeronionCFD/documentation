--
title: phaseHydrostaticPressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-phaseHydrostaticPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `phaseHydrostaticPressure` is a generic boundary condition that
provides a phase-based hydrostatic pressure condition, calculated as:

$$
    p_{hyd} = p_{ref} + \rho g (x - x_{ref})
$$

where:

Property             | Type
---------------------|-------
$$p_{hyd}$$ | Hyrostatic pressure \[Pa\]
$$p_{ref}$$ | Reference pressure \[Pa\]
$$x_{ref}$$ | Reference point in Cartesian coordinates
$$\rho$$    | Density (assumed uniform)
$$g$$       | Acceleration due to gravity \[m/s2\]

The values are assigned according to the phase-fraction field:

- 1: apply $$p_{hyd}$$
- 0: apply a zero-gradient condition

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            mappedPatch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            phaseHydrostaticPressure;
        rho             <scalar>;
        pRefValue       <scalar>;
        pRefPoint       <vector>;

        // Optional entries
        phaseFraction   <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "phaseHydrostaticPressureFvPatchScalarField.H", "phaseHydrostaticPressure" %>
<% assert :string_exists, "phaseHydrostaticPressureFvPatchScalarField.C", "rho" %>
<% assert :string_exists, "phaseHydrostaticPressureFvPatchScalarField.C", "pRefValue" %>
<% assert :string_exists, "phaseHydrostaticPressureFvPatchScalarField.C", "pRefPoint" %>
<% assert :string_exists, "phaseHydrostaticPressureFvPatchScalarField.C", "phaseFraction" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `phaseHydrostaticPressure`      | word    | yes      | -
`rho`                  | Constant value of density in the far field | scalar  | yes      | -
`pRefValue`            | Reference pressure \[Pa\]                  | scalar  | yes      | -
`pRefPoint`            | Reference pressure location                | vector  | yes      | -
`phaseFraction`        | Name of phase-fraction field               | word    | no       | alpha

The inherited entries are elaborated in:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/phaseHydrostaticPressure" %>

API:

- [Foam::phaseHydrostaticPressureFvPatchScalarField](doxy_id://Foam::phaseHydrostaticPressureFvPatchScalarField)

<%= history "2.1.1" %>

<!----------------------------------------------------------------------------->
