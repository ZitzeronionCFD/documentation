---
title: General conditions
short_title: General
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-general
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

## Boundary mesh type {#boundary-mesh-type}

General conditions are specified using the `patch` type entry in the
`$FOAM_CASE/constant/polyMesh/boundary` file:

    <patchName>
    {
        type            patch;
        ...
    }

## Base types {#base-types}

Boundaries reduce to one of the following types:

- [Dirichlet](<%= ref_id "boundary-conditions-fixedValue"%>): fixed value
- [Neumann](ref_id://boundary-conditions-fixedGradient): fixed gradient
- [Mixed](ref_id://boundary-conditions-mixed): linear combination of
  Dirichlet and Neumann

## Options {#options}

Available general conditions include

<%= insert_models "boundary-conditions-general" %>

## Further information {#further-information}

Source code:

- [Foam::polyPatch](doxy_id://Foam::polyPatch)
- [Foam::fvPatch](doxy_id://Foam::fvPatch)

See also

- [grpGenericBoundaryConditions](doxy_id://grpGenericBoundaryConditions)
