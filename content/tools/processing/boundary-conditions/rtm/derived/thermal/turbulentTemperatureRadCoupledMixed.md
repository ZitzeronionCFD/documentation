---
title: turbulentTemperatureRadCoupledMixed
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-turbulentTemperatureRadCoupledMixed
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `turbulentTemperatureRadCoupledMixed` is a boundary condition that provides
a mixed boundary condition for temperature, to be used for heat-transfer
on back-to-back baffles. Optional thin thermal layer resistances can be
specified through `thicknessLayers` and `kappaLayers` entries.

The thermal conductivity `kappa` can either be retrieved from various
possible sources, as detailed in the class `temperatureCoupledBase`.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            compressible::turbulentTemperatureRadCoupledMixed;
        Tnbr            <word>;

        // Optional entries
        Tnbr                <word>;
        qrNbr               <word>;
        qr                  <word>;
        logInterval         <scalar>;
        thermalInertia      <bool>;
        verbose             <bool>;
        prefix              <word>;

        // Conditional entries

            // Option-1
            thicknessLayers <scalarList>;
            kappaLayers     <scalarList>;

            // Option-2
            thicknessLayer  <PatchFunction1<scalar>>;
            kappaLayer      <PatchFunction1<scalar>>;

        // Inherited entries
        kappaMethod         <word>;
        kappa               <word>;
        ...
    }

<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.H", "turbulentTemperatureRadCoupledMixed" %>
<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.C", "Tnbr" %>
<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.C", "qrNbr" %>
<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.C", "qr" %>
<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.C", "logInterval" %>
<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.C", "thermalInertia" %>
<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.C", "verbose" %>
<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.C", "prefix" %>
<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.C", "thicknessLayer" %>
<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.C", "kappaLayer" %>
<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.C", "thicknessLayers" %>
<% assert :string_exists, "turbulentTemperatureRadCoupledMixedFvPatchScalarField.C", "kappaLayers" %>

where:

Property          | Description                                | Type    | Required | Default
------------------|--------------------------------------------|---------|----------|---------
`type`            | Type name: `turbulentTemperatureRadCoupledMixed` | word    | yes      | -
`Tnbr`            | Name of field on the neighbour region      | word    | yes      | -
`thicknessLayer`  | Thickness of a single layer                | scalarList | no    | -
`thicknessLayers` | Thickness of multiple layers               | PatchFunction1\<scalar\> | no | -
`kappaLayer`      | Conductivity of a single layer             | scalarList | no    | -
`kappaLayers`     | Conductivity of multiple layers            | PatchFunction1\<scalar\> | no | -
`Tnbr`            | Name of the nbr temperature field          | word  | no   | T
`qrNbr`           | Name of the radiative flux in the nbr region | word | no | none
`qr`              | Name of the radiative flux in this region  | word | no | none
`logInterval`     | Log-file output frequency \[s\]            | scalar | no  | -1
`thermalInertia`  | Flag to add thermal inertia to wall node   | bool | no | false
`verbose`         | Flag to enable verbose output with extra fields | bool | no | false
`prefix`          | Name of output-field prefix (in verbose mode)  | word | no | multiWorld

The inherited entries are elaborated in:

- mappedPatchFieldBase.H
- mixedFvPatchFields.H
- temperatureCoupledBase.H
- writeFile.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/basic/chtMultiRegionFoam/2DImplicitCyclic" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/turbulentTemperatureRadCoupledMixed" %>

API:

- [Foam::compressible::turbulentTemperatureRadCoupledMixedFvPatchScalarField](doxy_id://Foam::compressible::turbulentTemperatureRadCoupledMixedFvPatchScalarField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
