---
title: variableHeightFlowRate
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-variableHeightFlowRate
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `variableHeightFlowRate` is a boundary condition that provides
a phase fraction condition based on the local flow conditions, whereby the
values are constrained to lay between user-specified upper and lower bounds.

The behaviour is described by:

- if alpha > upperBound:
  - apply a fixed value condition, with a uniform level of the upper bound
- if lower bound <= alpha <= upper bound:
  - apply a  zero-gradient condition
- if alpha < lowerBound:
  - apply a fixed value condition, with a uniform level of the lower bound

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            variableHeightFlowRate;
        lowerBound      <scalar>;
        upperBound      <scalar>;

        // Optional entries
        phi             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "variableHeightFlowRateFvPatchField.H", "variableHeightFlowRate" %>
<% assert :string_exists, "variableHeightFlowRateFvPatchField.C", "lowerBound" %>
<% assert :string_exists, "variableHeightFlowRateFvPatchField.C", "upperBound" %>
<% assert :string_exists, "variableHeightFlowRateFvPatchField.C", "phi" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `variableHeightFlowRate` | word     | yes      | -
`lowerBound`       | Lower bound for clipping  | scalar  | yes      | -
`upperBound`       | Upper bound for clipping  | scalar  | yes      | -
`phi`              | Name of flux field        | word    | no       | phi

The inherited entries are elaborated in:

- mixedFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/RAS/weirOverflow" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/variableHeightFlowRate" %>

API:

- [Foam::variableHeightFlowRateFvPatchField](doxy_id://Foam::variableHeightFlowRateFvPatchField)

<%= history "2.1.1" %>

<!----------------------------------------------------------------------------->
