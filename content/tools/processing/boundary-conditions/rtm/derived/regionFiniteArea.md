---
title: Region finite-area boundary conditions
short_title: Region finite area
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-regionFa
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

Available region finite-area boundary conditions include:

<%= insert_models "bcs-regionFa" %>
