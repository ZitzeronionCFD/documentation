---
title: Scalar transport
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- function object
- post-processing
- solver
menu_id: function-objects-solvers-scalartransport
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-solvers
group: function-objects-solvers
---

<%= page_toc %>

The `scalarTransport` function object evolves a scalar transport equation, which
can be seeded using

- boundary conditions, e.g.
  [fixed value](ref_id://boundary-conditions-fixedValue)
- equation sources, e.g. `fvOptions`

## Usage {#usage}

Basic operation of the `scalarTransport` function object comprises:

    scalar1
    {
        type            scalarTransport;
        libs            ("libsolverFunctionObjects.so");
    }

<% assert :string_exists, "scalarTransport.H", "scalarTransport" %>
<% assert :class_library_exists,
   "functionObjects/solvers/Make/files",
   "scalarTransport",
   "libsolverFunctionObjects" %>

For more complete control, the full set of input entries includes:

    scalar1
    {
        type            scalarTransport;
        libs            ("libsolverFunctionObjects.so");


        // Optional entries

        // Name of scalar field to transport, default = 's'
        field           vapour;

        // Name of flux field, default = 'phi'
        phi             phi;

        // Name of density field for compressible cases, default = 'rho'
        rho             rho;

        // Name of phase field to constrain scalar to, default = 'none'
        phase           none;

        // Set the scalar to zero on start/re-start
        resetOnStartUp  no;

        // Name of field to use when looking up schemes from fvSchemes
        // default = <field>
        schemesField    U;


        // Diffusivity

        // Fixed value diffusivity
        D               0.001;

        // Name of field to use as diffusivity, default = 'none'
        nut             none;

        // Run-time selectable sources
        fvOptions
        {
            ...
        }
    }
<% assert :string_exists, "scalarTransport.C", "resetOnStartUp" %>
<% assert :string_exists, "scalarTransport.C", "field" %>
<% assert :string_exists, "scalarTransport.C", "s" %>
<% assert :string_exists, "scalarTransport.C", "phi" %>
<% assert :string_exists, "scalarTransport.C", "rho" %>
<% assert :string_exists, "scalarTransport.C", "phase" %>
<% assert :string_exists, "scalarTransport.C", "resetOnStartUp" %>
<% assert :string_exists, "scalarTransport.C", "schemesField" %>
<% assert :string_exists, "scalarTransport.C", "D" %>
<% assert :string_exists, "scalarTransport.C", "nut" %>
<% assert :string_exists, "scalarTransport.C", "fvOptions" %>

Order of importance when setting the *diffusivity*
{: .note}

- Fixed value: `D`
- Field value: `nut`
- If none, the code will
  - attempt to retrieve the effective viscosity from a turbulence model, or
  - set a value of zero

## Sample output {#sample-output}

The scalar will only be bounded if evolved using the
[upwind](ref_id://schemes-divergence-upwind) scheme.
{: .note}

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/solvers/scalarTransport" %>

API:

- [Foam::functionObjects::scalarTransport](doxy_id://Foam::functionObjects::scalarTransport)

Example usage:

- [chtMultiRegionFoam](ref_id://chtMultiRegionFoam)
  `windshieldCondensation` tutorial:
  <%= repo_link2(
  "$FOAM_TUTORIALS/heatTransfer/chtMultiRegionFoam/windshieldCondensation") %>

\ofAssert functionObjectUseExists heatTransfer/chtMultiRegionFoam/windshieldCondensation scalarTransport

heatTransfer/chtMultiRegionFoam/windshieldCondensation
