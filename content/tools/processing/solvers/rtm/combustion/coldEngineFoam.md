---
title: coldEngineFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- combustion
- heat transfer
- solvers
menu_id: coldenginefoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-combustion
group: solvers-combustion
solver_models:
- transient
- compressible
- turbulence
- heat-transfer
- fvOption
- dynamic-mesh
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-combustion" %>
- transient
- compressible
- heat transfer
- <%= link_to_menuid "models-turbulence" %>
- <%= link_to_menuid "fvoptions" %>
- moving mesh
- time defined in crank-angle (CA) degrees

## Equations {#equations}

- <%= link_to_menuid "pv-pimple" %>

## Input requirements {#input-requirements}

Mandatory fields:

- p: pressure \[pa\]
- U: velocity \[m/s\]
- T: temperature \[K\]

### Physical models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence):
  `constant/turbulenceProperties`
- [heat transfer](ref_id://models-thermophysical#usage):
  `constant/thermophysicalModels`
- [finite volume options](ref_id://numerics-fvoptions#usage):
  `constant/fvOptions` (optional)

### Solution controls {#input-requirements-solution-controls}

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/combustion/coldEngineFoam" %>

See also

- <%= link_to_menuid "enginefoam" %>
- <%= link_to_menuid "xifoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/coldEngineFoam" %>
