---
title: columnAverage
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- average
- channel
- column
- field
- function object
- post-processing
menu_id: function-objects-field-columnaverage
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `columnAverage` function object computes the arithmetic average of given
quantities along columns of cells in a given direction for **structured-like
layered meshes**. It is, for example, useful for channel-like cases where
spanwise average of a field is desired. However, the `columnAverage` function
object **does not operate on arbitrary unstructured meshes**.

The `columnAverage` function object, for each patch face, calculates the average
value of all cells attached in the patch face normal direction, and then pushes
the average value back to *all cells in the column*.

The `columnAverage` function object can be applied to any volume fields,
generating a volume field. However, the operation **cannot be applied onto
surface fields**, e.g `wallShearStress`.

### Operands

Operand      | Type                          | Location
:---         |:---                           |:---
input        | `vol<Type>Field`              | `$FOAM_CASE/<time>/<inpField>`
output file  | -                             | -
output field | `vol<Type>Field`              | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `columnAverage` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    columnAverage1
    {
        // Mandatory entries (unmodifiable)
        type            columnAverage;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        patches         (<patch1> <patch2> ... <patchN>)
        fields          (<field1> <field2> ... <fieldN>);

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "columnAverage.H", "columnAverage" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "columnAverage",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "columnAverage.C", "patches" %>
<% assert :string_exists, "functionObjectList.C", "fields" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: columnAverage            | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 patches      | Names of patches to collapse onto   | word | yes      | -
 fields       | Names of the operand fields         | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/periodicHill/transient" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/columnAverage/" %>

API:

- [Foam::functionObjects::columnAverage](doxy_id://Foam::functionObjects::columnAverage)

<%= history "v1812" %>

<!----------------------------------------------------------------------------->
