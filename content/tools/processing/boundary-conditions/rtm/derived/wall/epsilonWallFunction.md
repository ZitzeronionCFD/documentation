---
title: epsilonWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-epsilonWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `epsilonWallFunction` is boundary condition that provides
wall functions for the turbulent kinetic energy dissipation rate
(i.e. `epsilon`) and the turbulent kinetic energy production contribution
(i.e. `G`) for low- and high-Reynolds number simulations.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            epsilonWallFunction;

        // Optional entries
        lowReCorrection  <bool>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "epsilonWallFunctionFvPatchScalarField.H", "epsilonWallFunction" %>
<% assert :string_exists, "epsilonWallFunctionFvPatchScalarField.C", "lowReCorrection" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `epsilonWallFunction`           | word    | yes      | -
`lowReCorrection` | Flag: apply low-Re correction           | bool    | no       | false

The inherited entries are elaborated in:

- wallFunctionCoefficients.H
- wallFunctionBlenders.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/turbulenceModels/planeChannel" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/epsilonWallFunctions/epsilonWallFunction" %>

API:

- [Foam::epsilonWallFunctionFvPatchScalarField](doxy_id://Foam::epsilonWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
