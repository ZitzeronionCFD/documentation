---
title: Sutherland
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- transport
menu_id: transport-polynomial
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-transport
group: transport-models
---

<%= page_toc %>

Transport model based on the work of Sutherland
<%= cite "sutherland_viscosity_1893" %>.

Dynamic viscosity, $$ \mu $$ \[Pa.s\]

$$
\mu = A_s \frac{\sqrt{T}}{1 + T_s/T}
$$

Thermal conductivity, $$ \kappa $$ \[W/mK\]

$$
\kappa = \mu c_v \left( 1.32 + 1.77 \frac{R}{c_v} \right)
$$

Thermal diffusivity, $$ \alpha_h $$ \[kg/ms\]

$$
\alpha_h = \frac{\kappa}{c_p}
$$

## Usage

The `sutherland` transport model is defined using the entries:

~~~
transport
{
    As              <scalar>;
    Ts              <scalar>;
}
~~~

Where

- `As` : Model coefficient \[\]
- `Ts` : Model coefficient \[K\]

For example, for air:

~~~
transport
{
    As          1.4792e-06;
    Ts          116;
}
~~~

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/transport/sutherland" %>
