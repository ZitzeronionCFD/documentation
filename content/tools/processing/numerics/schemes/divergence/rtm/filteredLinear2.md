---
title: Filtered Linear (2) divergence scheme
short_title: Filtered linear (2)
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-filteredlinear2
license: CC-BY-NC-ND-4.0
group: schemes-divergence-general
---

<%= page_toc %>

## Properties {#properties}

- Second order
- Unbounded
- Attempts to limit the unboundedness of the [linear](../linear) scheme

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss filteredLinear2 <k> <l>;
    }

<% assert :string_exists, "filteredLinear2.H", "filteredLinear2" %>

Where the coefficients

- `k`: Scales the rate at which the correction is applied
  - 0 : linear
  - 1 : fully limited
- `l`: Maximum allowed overshoot/undershoot relative to the difference across
   the face.
  - 0 : no overshoot/undershoot
  - 1 : overshoot/undershoot equal to the difference across the face

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/limitedSchemes/filteredLinear2" %>

API:

- [Foam::filteredLinear2Limiter](doxy_id://Foam::filteredLinear2Limiter)
