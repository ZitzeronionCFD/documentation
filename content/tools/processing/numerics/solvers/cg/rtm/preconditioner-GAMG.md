---
title: GAMG preconditioner
short_title: GAMG
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- GAMG
- preconditioner
- solvers
menu_id: cg-preconditioner-gamg
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-cg
group: solvers-cg-preconditioner
---

<%= page_toc %>

## Properties {#properties}

- <B>G</B>eometric agglomerated <B>A</B>lgebraic <B>M</B>ulti<B>G</B>rid
  preconditioner
- Symmetric and asymmetric matrices
- Parallel inconsistent
- See [GAMG solver details](ref_id://multigrid-gamg)

## Usage {#usage}

    preconditioner  GAMG;
    smoother        <smoother>;
    relTol          <relatve tolerance>;
    tolerance       <absolute tolerance>;

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/preconditioners/GAMGPreconditioner" %>

API:

- [Foam::GAMGPreconditioner](doxy_id://Foam::GAMGPreconditioner)
