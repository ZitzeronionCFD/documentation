---
title: outletPhaseMeanVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- outlet
menu_id: boundary-conditions-outletPhaseMeanVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-outlet
group: bcs-outlet-velocity
---

<%= page_toc %>

## Description

The `outletPhaseMeanVelocity` is a velocity-outlet boundary condition that
adjusts the velocity for the given phase to achieve the specified mean thus
causing the phase-fraction to adjust according to the mass flow rate.

Typical usage is as the outlet condition for a towing-tank ship simulation
to maintain the outlet water level at the level as the inlet.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            outletPhaseMeanVelocity;
        Umean           <scalar>;
        alpha           <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "outletPhaseMeanVelocityFvPatchVectorField.H", "outletPhaseMeanVelocity" %>
<% assert :string_exists, "outletPhaseMeanVelocityFvPatchVectorField.C", "Umean" %>
<% assert :string_exists, "outletPhaseMeanVelocityFvPatchVectorField.C", "alpha" %>

where:

Property             | Description                        | Type    | Required | Default
---------------------|------------------------------------|---------|----------|---------
`type`               | Type name: `outletPhaseMeanVelocity` | word    | yes      | -
`Umean`              | Mean velocity normal to the boundary | scalar  | yes      | -
`rho`                | Name of phase-fraction field         | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/RAS/DTCHull" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/outletPhaseMeanVelocity" %>

API:

- [Foam::outletPhaseMeanVelocityFvPatchVectorField](doxy_id://Foam::outletPhaseMeanVelocityFvPatchVectorField)

<%= history "2.3.0" %>

<!----------------------------------------------------------------------------->
