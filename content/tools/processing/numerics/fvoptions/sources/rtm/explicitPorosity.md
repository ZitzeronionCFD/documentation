---
title: Explicit porosity
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- momentum
- porosity
- source
menu_id: fvoption-explicitporosity
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-momentum
---

<%= page_toc %>

## Properties {#properties}

- Provides a wrapper around a run-time selectable porosity model

## Usage {#usage}

The option is specified using:

    porosity1
    {
        type            explicitPorositySource;
        selectionMode   cellZone;

        explicitPorositySourceCoeffs
        {
            type            <porosity model>;
            ...
        }
    }

The `selectionMode` entry should be set to `cellZone`
{: .note}

<% assert :string_exists, "explicitPorositySource.H", "explicitPorositySource" %>

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/RAS/angledDuct" %>
- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoSimpleFoam/angledDuctExplicitFixedCoeff" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/laminar/porousBlockage" %>
- <%= repo_link2 "$FOAM_TUTORIALS/mesh/parallel/filter" %>
- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/RAS/angledDuct" %>

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/derived/explicitPorositySource" %>
