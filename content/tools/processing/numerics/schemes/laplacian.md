---
title: Laplacian schemes
short_title: Laplacian
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- Laplacian
menu_id: schemes-laplacian
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes
---

<%= page_toc %>

Taking the Laplacian of a property $$\phi$$ is represented using the notation:

$$
    \laplacian \phi
    =
    \frac{\partial^2}{\partial x_1^2} \phi
    + \frac{\partial^2}{\partial x_2^2} \phi
    + \frac{\partial^2}{\partial x_3^2} \phi
$$

or as a combination of divergence and gradient operators

$$
    \div \left( \Gamma \grad \phi \right)
$$

where $$ \Gamma $$ is a diffusion coefficient.

## Usage {#usage}

Laplacian schemes are specified in the
<%= link_to_menuid "fvschemes" %> file under the `laplacianSchemes`
sub-dictionary using the syntax:

    laplacianSchemes
    {
        default         none;
        laplacian(gamma,phi) Gauss <interpolation scheme> <snGrad scheme>
    }

All options are based on the application of
[Gauss theorem](../gauss-theorem), requiring an
[interpolation scheme](../interpolation) to
transform coefficients from cell values to the faces, and a
[surface-normal gradient](../sngrad) scheme.

## Example {#example}

## Further information {#further-information}

- See the [implementation details](implementation-details) to see how the
  schemes are coded.
