---
title: Directional mesh-wave wall distance
short_title: Directional mesh wave
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- wall
- mesh-wave
menu_id: schemes-walldistance-directionalmeshwave
license: CC-BY-NC-ND-4.0
#menu_parent: schemes-walldistance
group: schemes-wall-distance
---

<%= page_toc %>

## Properties {#properties}

`directionalMeshWave` is a variant of
[meshWave](ref_id://schemes-walldistance-meshwave) distance-to-patch
calculation method, with which the distance component in a specified direction
can be ignored, e.g. the distance-to-patch can be computed in the wall-normal
direction only.

For a given point within a given mesh, the existing `meshWave` method gives
the orthogonal distance to a specified patch. In meshes with very steep terrain
(e.g. a hill of 90 \[`deg`\]), this might be problematic for the fields that
require the distance to the patch associated with the terrain surface.

An example of `directionalMeshWave` output in terms of the wall distance
can be seen as follows:

![Wall distance field by directionalMeshWave](../directionalMeshWave.png)

## Usage {#usage}

Example specification of `directionalMeshWave` method
in `system/fvSchemes.wallDist`:

    wallDist
    {
        // Mandatory entries (unmodifiable)
        method      directionalMeshWave;
        normal      (0 0 1);

        // Optional (inherited) entries (unmodifiable)
        ...
    }

<% assert :string_exists, "directionalMeshWavePatchDistMethod.H", "directionalMeshWave" %>
<% assert :string_exists, "directionalMeshWavePatchDistMethod.C", "normal" %>

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 method       | Method name: directionalMeshWave    | word |  yes     | -
 normal       | The direction component to ignore   | vector |  yes   | -

The inherited entries are elaborated in:

- [meshWavePatchDistMethod](ref_id://schemes-walldistance-meshwave)

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/mesh/moveDynamicMesh/SnakeRiverCanyon" %>

Source code

- [Foam::patchDistMethods::directionalMeshWave](doxy_id://Foam::patchDistMethods::directionalMeshWave)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
