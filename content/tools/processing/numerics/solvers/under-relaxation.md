---
title: Under relaxation
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- relax
- solvers
menu_id: linearequationsolvers-underrelaxation
license: CC-BY-NC-ND-4.0
menu_parent: numerics-linearequationsolvers
---

<%= page_toc %>

Under relaxation factors are applied to stabilise calculations by limiting the
rate of change of both fields and equations.

## Field under-relaxation {#field}

$$
   Q^n = Q^o + \alpha \left( Q - Q^o \right)
$$

## Equation under-relaxation {#equations}

The linear solvers require that the coefficient matrix
is at least diagonally equal, and preferably diagonally dominant, i.e. per row,
the magnitude of the diagonal must be greater than or equal to the sum of the
off-diagonal components.

## Usage {#usage}

Specified in the [fvSolution](ref_id://fvsolution) file
under the `relaxationFactors` sub-dictionary

    relaxationFactors
    {
        fields
        {
        }
        equations
        {
        }
    }

Values relate to

- 0: no under-relaxation (equivalent to not specified}
- 1: matrix forced to become diagonally equal
