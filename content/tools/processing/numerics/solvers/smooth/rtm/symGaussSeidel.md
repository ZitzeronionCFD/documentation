---
title: Symmetric Gauss Seidel Smoother
short_title: Symmetric Gauss Seidel
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- solvers
- smooth
- symGaussSeidel
menu_id: smooth-symGaussSeidel
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-smooth
group: solvers-smooth-smoothers
---

<%= page_toc %>

## Properties {#properties}

- Gauss Seidel smoother
- Symmetric or asymmetric matrices
- Forward and reverse sweeps

## Usage {#usage}

    smoother        symGaussSeidel;

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/smoothers/symGaussSeidel" %>

API:

- [Foam::symGaussSeidelSmoother](doxy_id://Foam::symGaussSeidelSmoother)
