---
title: JANAF
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- thermodynamics
menu_id: thermo-janaf
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-thermo
group: thermo-models
---

<%= page_toc %>

The `janaf` model provides thermodynamic property characteristics as a
function of temperature. The model is based on two sets of coefficients that
describe the property variation with temperature above, and below, the model
'common' temperature, $$ T_{\text{common}} $$, i.e.

$$
  \mathrm{coeffs} =
  \begin{cases}
    \mathrm{low} & \text{if}\, T \gt T_{\text{common}} \\
    \mathrm{high} & \text{otherwise} \\
  \end{cases}
$$

The functions are valid within the range $$ 200 \lt T \lt 6000 \; \text{[K]}. $$

If the bounds are exceeded, calculations will fail with a fatal error.

Heat capacity at constant pressure, $$ c_p $$ \[J/kg/K\]

$$
c_p = c_1 + c_2 T + c_3 T^2 + c_4 T^3 + c_5 T^5 + c_{p, eos}
$$

Absolute enthalpy, $$ H_a $$ \[J/kg\]

$$
H_a = c_1 T
    + \frac{c_2 T^2}{2}
    + \frac{c_3 T^3}{3}
    + \frac{c_4 T^4}{4}
    + \frac{c_5 T^5}{5}
    + H_{eos}
$$

Entropy, $$ S $$ \[J/kg/K\]

$$
S   = c_1 \ln T
    + c_2 T
    + c_3 \frac{T^2}{2}
    + c_4 \frac{T^3}{3}
    + c_5 \frac{T^4}{4}
    + c_6
    + S_{eos}
$$

## Usage

The `janaf` thermodynamics model is defined using the entries:

~~~
thermodynamics
{
    Tlow            <scalar>;
    Thigh           <scalar>;
    Tcommon         <scalar>;
    lowCpCoeffs     (<scalar> <scalar> <scalar> <scalar> <scalar> <scalar> <scalar>);
    highCpCoeffs    (<scalar> <scalar> <scalar> <scalar> <scalar> <scalar> <scalar>);
}
~~~

Where

- `Tlow` : Low temperature limit \[K\]
- `Thigh` : High temperature limit \[K\]
- `Tcommon` : Common temperature \[K\]
- `lowCpCoeffs` : Low coefficients \[\]
- `highCpCoeffs` : High coefficients \[\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/thermo/janaf" %>
