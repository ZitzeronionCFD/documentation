---
title: Gauss Theorem
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- Gauss
menu_id: schemes-gausstheorem
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes
---

<%= page_toc %>

$$
    \int_V \left( \div \u \right) dV
  = \oint_S \left( \vec{n} \dprod \u \right) dS
$$
