---
title: FDIC preconditioner
short_title: FDIC
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- FDIC
- preconditioner
- solvers
menu_id: cg-preconditioner-fdic
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-cg
group: solvers-cg-preconditioner
---

<%= page_toc %>

## Properties {#properties}

- <B>F</B>aster version of the <B>D</B>iagonal-based <B>I</B>ncomplete
  <B>C</B>holesky preconditioner (symmetric equivalent of DILU)
- Symmetric matrices
- parallel inconsistent

## Usage {#usage}

    preconditioner  FDIC;

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/preconditioners/FDICPreconditioner" %>

API:

- [Foam::FDICPreconditioner](doxy_id://Foam::FDICPreconditioner)
