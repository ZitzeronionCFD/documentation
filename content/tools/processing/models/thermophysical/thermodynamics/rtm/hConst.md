---
title: Constant (enthalpy)
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- constant
- enthalpy
- thermodynamics
menu_id: thermo-hConst
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-thermo
group: thermo-models
---

<%= page_toc %>

Heat capacity at constant pressure, $$ c_p $$ \[J/kg/K\]

$$
c_p = \text{constant}
$$

## Usage

The `hConst` thermodynamics model is defined using the entries:

~~~
thermodynamics
{
    Cp              <scalar>;
    Hf              <scalar>;
    Tref            <scalar>;
    Href            <scalar>;
}
~~~

Where

- `Cp` : specific heat capacity at constant pressure \[J/kg/K\]
- `Hf` : heat of formation \[J/kg\]
- `Tref` : reference temperature \[K\]; default is `Tstd`
- `Href` : reference sensible enthalpy around which to linearise \[J/kg\];
  default is zero.

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/thermo/hConst" %>
