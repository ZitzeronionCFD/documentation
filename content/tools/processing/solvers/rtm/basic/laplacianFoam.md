---
title: laplacianFoam
copyright:
- Copyright (C) 2021 OpenCFD Ltd.
tags:
- basic
- incompressible
- solvers
menu_id: laplacianfoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-basic
group: solvers-basic
solver_models:
- fvOption
- transient
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-basic" %>
- Steady state or transient

## Equations {#equations}

The solver evolves a scalar, $$ T $$, using the equation:

$$
    \ddt{T} - \div (D_T \grad T) = S_T
$$

<% assert :text_exists, "laplacianFoam.C", "ddt(T)" %>
<% assert :text_exists, "laplacianFoam.C", "laplacian(DT, T)" %>
<% assert :text_exists, "laplacianFoam.C", "fvOptions(T)" %>

## Input requirements {#input-requirements}

Mandatory fields:

- Transported field: `T`: scalar \[-\]

<% assert :string_exists, "laplacianFoam/createFields.H", "T" %>

### Physical models {#input-requirements-physical-models}

- Transport `$FOAM_CASE/constant/transportProperties` requirements:

      // Diffusion coefficient \[m2/s\]
      DT              4e-05;

<% assert :string_exists, "laplacianFoam/createFields.H", "transportProperties" %>
<% assert :string_exists, "laplacianFoam/createFields.H", "DT" %>

### Solution controls {#input-requirements-solution-controls}

[$FOAM_CASE/system/fvSchemes](ref_id://fvschemes) requirements:

- [time schemes](ref_id://schemes-time): `ddt(T)`
- [gradient schemes](ref_id://schemes-gradient): `grad(T)`
- [laplacian schemes](ref_id://schemes-laplacian): `laplacian(DT,T)`

<% assert :text_exists, "laplacianFoam.C", "ddt" %>
<% assert :text_exists, "laplacianFoam.C", "laplacian" %>

[$FOAM_CASE/system/fvSolution](ref_id://fvsolution) requirements:

- [solver entry](ref_id://numerics-linearequationsolvers#options) for `T`

<% assert :string_exists, "laplacianFoam/createFields.H", "T" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/basic/laplacianFoam" %>

See also

- [simpleFoam](ref_id://simplefoam)

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/basic/laplacianFoam" %>
