---
title: pressureInletOutletParSlipVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-pressureInletOutletParSlipVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `pressureInletOutletParSlipVelocity` is a velocity inlet/outlet boundary
condition that applies a zero-gradient condition for outflow (as defined by
the flux); and obtains velocity from the flux with specified inlet direction.

The `slip` condition is applied tangential to the patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            pressureInletOutletParSlipVelocity;

        // Optional entries
        phi             <word>;
        rho             <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "pressureInletOutletParSlipVelocityFvPatchVectorField.H", "pressureInletOutletParSlipVelocity" %>
<% assert :string_exists, "pressureInletOutletParSlipVelocityFvPatchVectorField.C", "phi" %>
<% assert :string_exists, "pressureInletOutletParSlipVelocityFvPatchVectorField.C", "rho" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `pressureInletOutletParSlipVelocity` | word    | yes      | -
`phi`          | Name of flux field                         | word    | no       | phi
`rho`          | Name of density field                      | word    | no       | rho

The inherited entries are elaborated in:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
<!-- end of the list -->

- Sign conventions:
  - positive flux (out of domain): apply zero-gradient condition
  - negative flux (into of domain): derive from the flux with specified
    direction
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/potentialFreeSurfaceDyMFoam/oscillatingBox" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/pressureInletOutletParSlipVelocity" %>

API:

- [Foam::pressureInletOutletParSlipVelocityFvPatchVectorField](doxy_id://Foam::pressureInletOutletParSlipVelocityFvPatchVectorField)

<%= history "2.1.0" %>

<!----------------------------------------------------------------------------->
