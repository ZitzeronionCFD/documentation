---
title: Constraints
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- constraint
menu_id: numerics-fvoptions-constraints
license: CC-BY-NC-ND-4.0
menu_parent: numerics-fvoptions
---

<%= page_toc %>

## General {#general}

<%= insert_models "fvoptions-constraints-general" %>

## Momentum {#momentum}

<%= insert_models "fvoptions-constraints-momentum" %>

## Energy {#energy}

<%= insert_models "fvoptions-constraints-energy" %>
