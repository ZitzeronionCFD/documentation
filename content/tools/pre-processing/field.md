---
title: Field
tags:
- initialisation
- set-up
copyright:
- Copyright 2021 (C) OpenCFD Ltd.
menu_id: pre-processing-field
license: CC-BY-NC-ND-4.0
menu_parent: pre-processing
menu_weight: 30
---

<%= page_toc %>

<%= insert_models "pre-processing-field" %>
