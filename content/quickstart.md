---
title: Quickstart
copyright:
- Copyright (C) 2021-2023 OpenCFD Ltd.
menu_id: quick-start
license: CC-BY-NC-ND-4.0
menu_parent: home
menu_weight: 10
---

<%= page_toc %>

<!--
- Identify the objectives
- Show, don't tell
- Define the scenarios
- Research the user
- Assume a low level of understanding
- Add headings and timings
- Keep it simple, but descriptive
- Be clear and consistent
- Test changes with real users
- Keep the content up-to-date
--->

Welcome to the OpenFOAM&reg; quickstart guide!

This guide provides a simplified step-by-step overview to help you quickly
get started with the installation, setup, and basic usage of OpenFOAM&reg;,
a powerful free/open-source computational fluid dynamics (CFD) software.

## OpenFOAM&reg; Installation

OpenFOAM&reg; is easily accessible on a wide range of operating systems,
including Linux distributions, Microsoft Windows, and macOS. You can
conveniently obtain OpenFOAM&reg; in the form of
[precompiled packages](https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled)
or as [source code](https://develop.openfoam.com/Development/openfoam/-/wikis/building)
that you can build yourself.

In this guide, we will install the precompiled package of the latest OpenFOAM&reg;
version on Microsoft Windows 10 Enterprise operating system.

Download the package of the latest OpenFOAM&reg; version by simply clicking
the link below (Please ensure that you have at least 300MB of available storage
space on your hard disk):

[Link to OpenFOAM-windows-mingw.exe](https://dl.openfoam.com/source/latest/OpenFOAM-windows-mingw.exe)

The package is an executable. Run this executable to open the installation
wizard, and simply follow the wizard's installation instructions specific to
your system. A typical view of the wizard is shown below
(Please ensure that you have enough available storage
space on your hard disk):

!["Installation wizard"](openfoam-installation-wizard-windows.png)
{: .half-width}

You may also need to install Microsoft MPI (MS-MPI). Similary, download
the following package (i.e. `msmpisetup.exe`), and follow its installation
wizard:

[Link to Microsoft MPI v10.0](https://www.microsoft.com/en-us/download/details.aspx?id=57467)

Upon completion of the above steps, an icon will be created on your desktop.
Double-click this icon to launch a terminal window. Running this icon will load
the OpenFOAM&reg; environment. Verify the contents of OpenFOAM&reg; in a
terminal as follows (`$` is the command prompt, indicating that the
[terminal](https://opensource.com/article/21/8/linux-terminal) is ready to
accept a command.):

~~~
$ ls -p
OpenFOAM/

$ cd $WM_PROJECT_DIR

$ ls -p
bin/  COPYING  etc/  etc-mingw  META-INFO  platforms/  README.md  ThirdParty/  tutorials/
~~~

## Problem Setup

The problem setup is derived from the experiment conducted by [Pitz and Daily
(1983)](https://doi.org/10.2514/3.8290).

The setup involves a steady simulation of a non-reacting, single-phase,
Newtonian, incompressible fluid flow, which comprises a range of intricate
turbulence phenomena such as flow detachment/reattachment and formation of
recirculation zones.

You can browse the problem setup:

~~~
$ cd $FOAM_TUTORIALS/incompressible/simpleFoam/pitzDaily/

$ ls -p
0/  constant/  system/

$ ls -p system/
blockMeshDict  controlDict  fvSchemes  fvSolution  streamlines

$ ls -p constant/
transportProperties  turbulenceProperties

$ ls -p 0/
epsilon  k  p  U
~~~

In OpenFOAM&reg;, simulation settings are customised using text files.
Each parameter in these files follows a straightforward key-value pair format.
However, it is important to note that each file has its own unique set of
mandatory and optional keywords. The location and content of these files vary
depending on the chosen solver application, simulation setup, and the specific
aspects you wish to modify. Therefore, it is necessary to familiarize yourself
with the structure and significance of these keywords in relevant files.

## Pre-process

Pre-processing involves the typical preparation steps of a CFD problem
setup.

To carry out these steps, it is necessary to be familiar first with the typical
directory structure of an OpenFOAM&reg; setup and the meaning of these
directories.

An OpenFOAM&reg; simulation generally consists of the following three
directories:

- `0`: (zero directory) contains field initial/boundary conditions at timestep
(or iteration) zero. Note that fields are typically named according to their
mathematical representation, e.g. `u` for velocity, `p` for pressure, `T` for
temperature etc.
- `constant`: contains files that define the geometric and physical properties
of the simulation.
- `system`: contains files that define the settings of the simulation
applications and tools.

### Physical domain

In OpenFOAM&reg;, there are multiple ways to provide the physical domain
specifications for a simulation. These include:

- Geometry import: OpenFOAM&reg; supports the import of geometry from
external sources in formats such as `STL`, `STEP`, or `IGES`, to name a few.
- Geometry definition: OpenFOAM&reg; allows manual definition of the physical
domain within itself.
- Mesh generation: OpenFOAM&reg; provides native meshing tools including
[blockMesh](ref_id://blockmesh), [snappyHexMesh](ref_id://snappyhexmesh), and
mesh converters to generate meshes that represent the physical domain.

In this example, we generate the physical domain alongside the mesh
using the [blockMesh](ref_id://blockmesh) utility. The physical domain includes
a narrow inlet passage, a backward-facing step, and a converging section, as
shown in the side view below:

![Side view of the physical domain](boundary-conditions-pitzDaily.svg)

### Physical properties

In this example, we consider a simple non-reacting, single-phase, Newtonian,
incompressible fluid flow, with the following physical properties:

Metrics                                 | Values
----------------------------------------|------------
Fluid kinematic viscosity, $$\nu$$      | $$1e\text{-}5\,[\mathrm{m}^2\text{/}\mathrm{s}]$$

In OpenFOAM&reg;, the type of fluid and the value of the fluid kinematic
viscosity are set using the `transportProperties` file located in the
`constant` directory.

~~~
$ cat constant/transportProperties

[...]

transportModel  Newtonian;
nu              1e-05;

[...]
~~~

### Spatial domain discretisation

In this example, we generate the spatial domain discretisation, also known
as the mesh, using the [blockMesh](ref_id://blockmesh) utility configured by
the `blockMeshDict` file in the `system` directory.

You can skim through the content of this file via (for further information, please
see [blockMesh](ref_id://blockmesh)):

~~~
$ cat system/blockMeshDict
~~~

The side view of the generated mesh is illustrated below:

![](mesh.png)

In OpenFOAM&reg;, the spatial domain discretization is typically performed
using one of three methods, depending on the complexity of the
physical domain and the desired mesh characteristics:

- [blockMesh](ref_id://blockmesh): built-in mesh generation tool in
OpenFOAM&reg; providing a simple and intuitive way to generate structured meshes
for simple geometries.
- [snappyHexMesh](ref_id://snappyhexmesh): powerful built-in mesh generation
tool in OpenFOAM&reg; used for generating high-quality, unstructured meshes. It
can handle complex geometries and supports various meshing strategies, such as
refinement regions and boundary layer additions.
- Mesh conversion: OpenFOAM&reg; supports the import of meshes generated by
various external mesh generators using tools like `fluentMeshToFoam`.

### Temporal domain discretisation

In this example, the temporal domain discretization, i.e. the division of the
simulation time into discrete steps, is steady state.

In OpenFOAM&reg;, the temporal domain discretization is set in the
[controlDict](ref_id://controldict) file located in the `system` directory.

The following key-value pairs in the [controlDict](ref_id://controldict) file
allow users to configure the temporal behaviour of the simulation:

~~~
$ cat system/controlDict

[...]

startFrom       startTime;
startTime       0;
stopAt          endTime;
endTime         2000;
deltaT          1;
adjustTimeStep  off;

[...]
~~~

OpenFOAM&reg; provides several methods to handle the temporal
discretization, allowing users to control the time advancement and accuracy of
their simulations. Here are the main methods for generating the temporal domain
discretization in OpenFOAM&reg;:

- Steady-state simulations
- Explicit time stepping
- Implicit time stepping
- Adaptive time stepping

### Equation discretisation

In OpenFOAM&reg;, the equation discretization refers to the numerical
approximation of the governing equations that describe fluid flow or other
physical phenomena. OpenFOAM&reg; provides a wide range of discretization
schemes that can be used to discretize the equations and solve them numerically.

The selection of discretization schemes is usually defined for each equation
field and equation type in the [fvSchemes](ref_id::fvschemes) file found in the
`system` directory.

Within the [fvSchemes](ref_id::fvschemes) file, you will find configurations for
discretization schemes applied to gradient, divergence, Laplacian, and other
terms in the governing equations.

For instance, you can examine the settings for time and gradient schemes within
the [fvSchemes](ref_id::fvschemes) file, as shown below:

~~~
$ cat system/fvSchemes

[...]

ddtSchemes
{
    default         steadyState;
}

gradSchemes
{
    default         Gauss linear;
}

[...]
~~~

### Initial/boundary conditions

In this example, the initial and boundary conditions of the governing
equations at geometric boundaries are set as follows:

<table>
    <thead>
        <tr>
            <th>Property</th><th>Inlet</th><th>Outlet</th><th>Walls</th><th>Internal</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>$$\text{Velocity}, \u \, [\text{m/s}]$$</td>
            <td>$$(10, 0, 0)$$</td>
            <td>$$\grad \u = \tensor{0}$$</td>
            <td>$$(0, 0, 0)$$</td>
            <td>$$(0, 0, 0)$$</td>
        </tr>
        <tr>
            <td>$$\text{Pressure}, p \, [\mathrm{m}^2\text{/}\mathrm{s}^2]$$</td>
            <td>$$\grad p = \vec{0}$$</td>
            <td>$$0$$</td>
            <td>$$\grad p = \vec{0}$$</td>
            <td>$$0$$</td>
        </tr>
        <tr>
            <td>$$\text{Turbulent kinetic energy}, k \, [\mathrm{m}^2\text{/}\mathrm{s}^2]$$</td>
            <td>$$0.375$$</td>
            <td>$$\grad k = \vec{0}$$</td>
            <td>$$\text{wall function}$$</td>
            <td>$$0.375$$</td>
        </tr>
        <tr>
            <td>$$\text{Turbulent kinetic energy dissipation rate}, \epsilon \, [\mathrm{m}^2\text{/}\mathrm{s}^3]$$</td>
            <td>$$14.855$$</td>
            <td>$$\grad \epsilon = \vec{0}$$</td>
            <td>$$\text{wall function}$$</td>
            <td>$$14.855$$</td>
        </tr>
    </tbody>
</table>

In OpenFOAM&reg;, the initial and boundary conditions are specified using
field files located within time directories for each field variable, such as
velocity. The initial conditions are applied to the entire domain volume,
while the boundary conditions are specified on the boundaries of the domain,
which are divided into numerical `patches`.

For example, the initial condition for pressure field ($$p$$) is specified in
the `p` file within the `0` directory as shown below:

~~~
$ cat 0/p

[...]

internalField   uniform 0;

[...]
~~~

Similarly, as an example, the boundary condition for velocity field ($$\u$$) at
inlet patch is set in the `U` file within the `0` directory in the following
manner:

~~~
$ cat 0/U

[...]

boundaryField
{
    inlet
    {
        type            fixedValue;
        value           uniform (10 0 0);
    }

[...]
~~~

### Pressure-velocity coupling algorithm

In this example, the choice of the pressure-velocity coupling algorithm
is the SIMPLE algorithm. The corresponding solver application in OpenFOAM&reg;
is called [simpleFoam](ref_id://simplefoam).

The settings for the SIMPLE algorithm can be specified in a sub-dictionary within
the [fvSolution](ref_id://fvsolution) file, which is located in the `system`
directory:

~~~
$ cat system/fvSolution

[...]

SIMPLE
{
    nNonOrthogonalCorrectors 0;
    consistent      yes;           // SIMPLEC

    residualControl                // Additional control on simulation
    {
        p               1e-2;
        U               1e-3;
        "(k|epsilon)"   1e-3;
    }
}

[...]
~~~

Note that OpenFOAM&reg; provides three pressure-velocity coupling algorithms:

- [SIMPLE](ref_id://pv-simple): Semi-Implicit Method for Pressure-Linked Equations
- [PISO](ref_id://pv-piso): Pressure Implicit with Splitting of Operators
- [PIMPLE](ref_id://pv-pimple): Combination of PISO and SIMPLE

### Linear solvers

OpenFOAM&reg; offers a range of linear solvers, including iterative solvers like
[GAMG](ref_id://multigrid-gamg), [PCG](ref_id://cg-pcg), and
[PBiCG](ref_id://cg-pbicg), as well as interfaces to external solvers, e.g.
`PETSc`. The choice of linear solvers and their parameters depends on various
factors such as the problem characteristics, size of the linear system, and
desired computational efficiency. OpenFOAM&reg; provides extensive options for
fine-tuning its solver suites.

In OpenFOAM&reg;, the settings for linear solvers are typically defined in the
[fvSolution](ref_id://fvsolution) file located in the `system` directory.

For instance, the linear solver settings for the pressure field ($$p$$) are
specified with the corresponding smoother and convergence parameters,
as demonstrated below:

~~~
$ cat system/fvSolution

[...]

p
{
    solver          GAMG;
    smoother        GaussSeidel;
    tolerance       1e-06;
    relTol          0.1;
}

[...]
~~~

### Case control

In OpenFOAM&reg;, the main case controls can be set in the
[controlDict](ref_id://controldict) file
within the `system` directory. The [controlDict](ref_id://controldict) file is
a key configuration file that allows you to control various aspects of the
simulation and specify case-specific settings such as:

- Time/Iteration control
- Output control
- Runtime control
- Function-object control
- Debug control
- Optimisation control
- External library control

The following shows a typical set of key-value pairs available in the
[controlDict](ref_id://controldict):

~~~
$ cat system/controlDict

[...]

application     simpleFoam;         // name of solver application

startFrom       startTime;          <--
startTime       0;                  //
stopAt          endTime;            // Time/Iteration control
endTime         2000;               //
deltaT          1;                  -->

writeControl    timeStep;           <--
writeInterval   100;                //
purgeWrite      0;                  //
writeFormat     ascii;              // Output control
writePrecision  6;                  //
writeCompression off;               //
timeFormat      general;            //
timePrecision   6;                  -->

runTimeModifiable true;             // Runtime modifications

functions                           <--
{                                   //
    #includeFunc streamlines        // Function objects
    #include "streamlines"          //
}                                   -->

DebugSwitches                       <--
{                                   //
    SolverPerformance   0;          // Debug control
}                                   -->

OptimisationSwitches                <--
{                                   //
    writeNowSignal  12;             // Optimisation control
}                                   -->

libs                                <--
(                                   //
    externalLibraryName             // External library control
);                                  -->

[...]
~~~

## Process

To run this case, you can simply execute two consecutive commands.

First, the mesh is generated using the [blockMesh](ref_id://blockmesh) utility.
Run the utility with the following command (the execution should take a few
seconds):

~~~
$ blockMesh
~~~

This command will display various utility messages and generate the mesh in the
`constant/polyMesh` directory. You can verify the mesh generation by checking
the contents of the directory using the command:

~~~
$ ls -p constant/polyMesh/
boundary  faces  neighbour  owner  points
~~~

If you need to make changes to the `blockMeshDict` settings or redirect the
utility's output to a log file, you can rerun the
[blockMesh](ref_id://blockmesh) using the command:

~~~
$ blockMesh >& log.blockMesh
~~~

You can also use the internal runner functions available in OpenFOAM&reg;
(e.g. `runApplication`):

~~~
$ source ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions

$ runApplication blockMesh
~~~

The next and final step is to run the solver application by executing
the following command (the execution should take a few seconds):

~~~
$ simpleFoam >& log.simpleFoam
~~~

Running this command will run the simulation and print valuable information
into the log file. This information includes updates on the convergence of the
linear solvers, progress in simulation iterations, and details about function
objects, among others.

Upon completion of the simulation, you will notice that OpenFOAM&reg; has
created new time directories containing solutions specific to each time step,
post-processing data generated by function objects, and log files:

~~~
$ ls -p
0/  100/  200/  282/  constant/  log.blockMesh  log.simpleFoam  postProcessing/  system/
~~~

## Post-process

Once the simulation has been completed, the next step is post-processing
the results to extract meaningful information and visualize the data.
OpenFOAM&reg; provides many powerful tools for post-processing that can
be executed during runtime and/or after the simulation, e.g.
[postProcess](ref_id://postProcess) utility.

You can visualise the simulation results by an external visualiation toolkit.
One of the most commonly used tools for field visualization is ParaView,
an open-source visualization software that integrates well with OpenFOAM&reg;.

An example visualisation of the velocity magnitude field at the last
iteration step using ParaView can be seen below:

![](velocity-magnitude-paraview.png)

<!----------------------------------------------------------------------------->
