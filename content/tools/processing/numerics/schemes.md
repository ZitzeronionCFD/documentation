---
title: Schemes
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- schemes
menu_id: numerics-schemes
license: CC-BY-NC-ND-4.0
menu_parent: numerics
---

<%= page_toc %>

## Introduction {#intro}

OpenFOAM applications are designed for use with unstructured meshes, offering up
to second order accuracy, predominantly using collocated variable arrangements.
Most focus on the Finite Volume Method, for which the conservative form
of the general scalar transport equation for the property $$ \phi $$ takes the
form:

$$
      \underbrace{\ddt{\rho \phi}}_{\mathrm{unsteady}}
    + \underbrace{\div \left(\rho \phi \u \right)}_{\mathrm{convection}}
    = \underbrace{\div \left(\Gamma \grad \phi \right)}_{\mathrm{diffusion}}
    + \underbrace{S_\phi}_{\mathrm{source}}
$$

The Finite Volume Method requires the integration over a 3-D control volume,
such that:

$$
      \int_V \ddt{\rho \phi} dV
    + \int_V \div \left(\rho \phi \u \right) dV
    = \int_V \div \left(\Gamma \grad \phi \right) dV
    + \int_V S_\phi dV
$$

This equation is discretised to produce a system of algebraic equations of the
form

$$
    \begin{bmatrix}
        a_{11} & a_{12} & \dots  & a_{1n}  \\
        a_{21} & a_{22} & \dots  & a_{2n}  \\
        \vdots & \vdots & \ddots & \vdots  \\
        a_{n1} & a_{n2} & \dots  & a_{nn}
    \end{bmatrix}
    \begin{bmatrix}
        x_{1}  \\
        x_{2}  \\
        \vdots \\
        x_{n}
    \end{bmatrix}
    =
    \begin{bmatrix}
        b_{1}  \\
        b_{2}  \\
        \vdots \\
        b_{n}
    \end{bmatrix}
$$

or more concisely:

$$
    \mat{A} \vec{x} = \vec{b}
$$

where:

$$\mat{A}$$
: coefficient matrix

$$\vec{x}$$
: vector of unknowns

$$\vec{b}$$
: source vector

The discretisation process employs user selected schemes to build the
$$\mat{A}$$ matrix and $$ \vec{b}$$ vector, described in the following
sections.  Choice of schemes are set in the
<%= link_to_menuid "fvschemes" %> dictionary.

## Temporal schemes {#time}

OpenFOAM includes a variety of schemes to integrate fields with respect to time:

- <%= link_to_menuid "schemes-time" %>

## Spatial schemes {#spatial}

At their core, spatial schemes rely heavily on
[interpolation schemes](ref_id://schemes-interpolation) to transform
cell-based quantities to cell faces, in combination with
[Gauss Theorem](ref_id://schemes-gausstheorem) to
convert volume integrals to surface integrals.

- <%= link_to_menuid "schemes-gradient" %>
- <%= link_to_menuid "schemes-divergence" %>
- <%= link_to_menuid "schemes-laplacian" %>
- <%= link_to_menuid "schemes-sngrad" %>

## Wall distance calculation method {#wall-distance}

Distance to the nearest wall is required, e.g. for a number of turbulence
models.  Several calculation methods are available:

- <%= link_to_menuid "schemes-walldistance" %>

## Further information {#further-information}

- <%= link_to_menuid "schemes-examples" %>
- Further guidance on scheme usage, refer to the
[User Guide](http://www.openfoam.com/documentation/user-guide/fvSchemes.php).
- [Source documentation](doxy_id://grpFvSchemes)
