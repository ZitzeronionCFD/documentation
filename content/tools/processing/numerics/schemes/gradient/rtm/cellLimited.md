---
title: Cell-limited gradient scheme
short_title: Cell-limited
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- limited
- gradient
menu_id: schemes-gradient-celllimited
license: CC-BY-NC-ND-4.0
#menu_parent: schemes-gradient
group: schemes-gradient-limiter
---

<%= page_toc %>

## Properties {#properties}

The cell gradient is limited to ensure that the face values obtained by
extrapolating the cell value to the cell faces using the gradient are bounded
by the neighbouring cells minimum and maximum limits.

As an example, consider the limiter to apply between cells P and N.

![cellLimited](../cell-limited.png)

The value of $$ \phi_P $$ is extrapolated using the gradient at P to the cell
face $$ \phi_{f,e} $$.  When using a coefficient of 1, if the extrapolated
value exceeds the value of the neighbour value $$ \phi_N $$ the gradient is
scaled so as to achieve $$ \phi_{N} $$.

## Usage {#usage}

The scheme is specified using:

    gradSchemes
    {
        default         none;
        grad(U)         cellLimited <scheme> <coefficient>;
    }

The coefficient should be specified in the range zero to one, where values of

- 1 represent limiting the gradient to ensure the extrapolated face value is
  bounded by the minimum and maximum neighbour cell values
- smaller values allow variation the extrapolated value to exceed the neighbour
  cell limiting values by a multiple of
  $$ \mathrm{max}(\phi_N) -  \mathrm{min}(\phi_N) $$

## Further information  {#further-information}

Source code

- [Foam::fv::cellLimitedGrad](doxy_id://Foam::fv::cellLimitedGrad)

Mult-directional form

- [cellMDLimited](ref_id://schemes-gradient-cellmdlimited)
