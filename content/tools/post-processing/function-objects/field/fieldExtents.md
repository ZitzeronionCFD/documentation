---
title: fieldExtents
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- extents
- field
- function object
- post-processing
menu_id: function-objects-field-fieldextents
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `fieldExtents` function object computes the spatial minimum and maximum
extents of an input field.

The extents are derived from the bound box limits after identifying the
locations where field values exceed the user-supplied threshold value.

### Operands

Operand       | Type       | Location
:---          |:---        |:---
input         | -          | -
output file   | dat        | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
output field  | -          | -

## Usage {#usage}

Example of the `fieldExtents` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    fieldExtents1
    {
        // Mandatory entries (unmodifiable)
        type            fieldExtents;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        fields              (<field1> <field2> ... <fieldN>);
        threshold           0.5;

        // Optional entries (runtime modifiable)
        internalField       true;
        referencePosition   (0 0 0);

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "fieldExtents.H", "fieldExtents" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "fieldExtents",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExtents.H", "fields" %>
<% assert :string_exists, "fieldExtents.H", "threshold" %>
<% assert :string_exists, "fieldExtents.H", "internalField" %>
<% assert :string_exists, "fieldExtents.H", "referencePosition" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: fieldExtents             | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 threshold    | Value to identify extents boundary  | scalar | yes    | -
 fields       | List of operand fields              | wordList | yes  | -
 internalField | Flag to process the internal field | bool   |  no    | true
 referencePosition | Reference position             | vector |  no    | (0 0 0)
 patches      | List of patches to process  | wordList  | no  | \<all patches\>

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Usage by the `postProcess` utility is not available.

### Notes on entries

For non-scalar fields, the magnitude of the field is employed and compared
to the value of `threshold`.

## Stored properties {#stored-properties}

### Reduced data

The following data are stored by the function object, and can be used by other
function objects:

- `<field-name>_internal_min` : bound box minimum for field
- `<field-name>_internal_max` : bound box maximum for field

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/reactingParcelFoam/rivuletPanel" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/fieldExtents" %>

API:

- [Foam::functionObjects::fieldExtents](doxy_id://Foam::functionObjects::fieldExtents)

<%= history "v1812" %>

<!----------------------------------------------------------------------------->
