---
title: Time scheme example
short_title: Example
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- time
menu_id: schemes-time-example
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes-time
---

<%= page_toc %>

- Solver: [scalarTransportFoam](ref_id://scalartransportfoam)
- TODO: Repository link: TBA

## Introduction {#introduction}

The test case comprises a 2-D box section, across which a uniform flow
is applied.  A passive scalar is introduced in a circular region at the start
time, and transported across the domain.  By looking at the profile of the
scalar across the section, we can determine the characteristics of our
time scheme selection.

## Case set-up {#case-set-up}

The impact of the choice of time scheme is assessed by
varying the `ddt` scheme entry of the <%= link_to_menuid "fvschemes" %> file:

    ddtSchemes
    {
        ddt(phi)        <time scheme>
    }

### Boundary conditions {#boundary-conditions}

Velocity boundary conditions

| Patch         | Type         | Value   |
| :------------ | :----------- | ------: |
| left          | fixedValue   | (1,1,0) |
| right         | zeroGradient |         |
| top           | zeroGradient |         |
| bottom        | fixedValue   | (1,1,0) |
| frontAndBack  | empty        |         |

Scalar boundary conditions

| Patch         | Type         | Value   |
| :------------ | :----------- | ------: |
| left          | fixedValue   | 0       |
| right         | zeroGradient |         |
| top           | zeroGradient |         |
| bottom        | fixedValue   | 0       |
| frontAndBack  | empty        |         |

![Initial field](images/time-test-euler-000.png)

### Mesh {#mesh}

![Case set-up](divergence-test-mesh.png)

## Results {#results}

### Euler results

| ![Time 1](images/time-test-euler-002.png) | ![Time 2](images/time-test-euler-007.png) | ![Time 3](images/time-test-euler-012.png) |
{: .noBorder}

### backward results

| ![Time 1](images/time-test-backward-002.png) | ![Time 2](images/time-test-backward-007.png) | ![Time 3](images/time-test-backward-012.png) |
{: .noBorder}

### Crank-Nicolson results

| ![Time 1](images/time-test-crank-nicolson-002.png) | ![Time 2](images/time-test-crank-nicolson-007.png) | ![Time 3](images/time-test-crank-nicolson-012.png) |
{: .noBorder}
