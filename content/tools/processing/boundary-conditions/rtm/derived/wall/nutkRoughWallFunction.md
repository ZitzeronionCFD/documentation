---
title: nutkRoughWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-nutkRoughWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `nutkRoughWallFunction` is boundary condition that provides
a wall-function for the turbulent viscosity (i.e. `nut`) when using wall
functions for rough walls, based on the turbulent kinetic energy (i.e. `k`).
The condition manipulates the wall roughness parameter (i.e. `E`) to account
for roughness effects.

Parameter ranges:
- Roughness height (i.e. `Ks`)= sand-grain roughness (0 for smooth walls)
- Roughness constant (i.e. `Cs`) = 0.5-1.0.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            nutkRoughWallFunction;
        Ks              <scalarField>;
        Cs              <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "nutkRoughWallFunctionFvPatchScalarField.H", "nutkRoughWallFunction" %>
<% assert :string_exists, "nutkRoughWallFunctionFvPatchScalarField.C", "Ks" %>
<% assert :string_exists, "nutkRoughWallFunctionFvPatchScalarField.C", "Cs" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `nutkRoughWallFunction`         | word    | yes      | -
`Ks`           | Sand-grain roughness height                | scalarField  | yes | -
`Cs`           | Roughness constant                         | scalarField  | yes | -

The inherited entries are elaborated in:

- nutkWallFunctionFvPatchScalarField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/damBreakWithObstacle" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/nutWallFunctions/nutkRoughWallFunction" %>

API:

- [Foam::nutkRoughWallFunctionFvPatchScalarField](doxy_id://Foam::nutkRoughWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
