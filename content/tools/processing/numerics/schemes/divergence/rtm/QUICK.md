---
title: QUICK divergence scheme
short_title: QUICK
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-quick
license: CC-BY-NC-ND-4.0
group: schemes-divergence-nvdtvd
---

<%= page_toc %>

## Properties {#properties}

- <B>Q</B>uadratic <B>U</B>pstream <B>I</B>nterpolation for <B>C</B>onvective
  <B>K</B>inematics
- Second order
- Unbounded
- By *Leonard* <%= cite "leonard_stable_1979" %>

## Normalised Variable Diagram {#nvd}

![QUICK](../QUICL.png)

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss QUICK;
    }

<% assert :string_exists, "QUICK.H", "QUICK" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/limitedSchemes/QUICK" %>

API:

- [Foam::QUICKLimiter](doxy_id://Foam::QUICKLimiter)
