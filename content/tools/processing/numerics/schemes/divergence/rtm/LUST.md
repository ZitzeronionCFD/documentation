---
title: LUST divergence scheme
short_title: LUST
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-lust
license: CC-BY-NC-ND-4.0
group: schemes-divergence-general
---

<%= page_toc %>

## Properties {#properties}

- <B>L</B>inear-<B>U</B>pwind <B>S</B>tabilised <B>T</B>ransport
- Fixed blended scheme with 0.25 [linearUpwind](../linear-upwind)
  and 0.75 [linear](../linear) weights
- Unbounded

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss LUST <grad-scheme>;
    }

<% assert :string_exists, "LUST.H", "LUST" %>

Where `<grad-scheme>` is a gradient scheme, e.g.

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss LUST grad(U);
    }

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/schemes/LUST" %>

API:

- [Foam::LUST](doxy_id://Foam::LUST)
