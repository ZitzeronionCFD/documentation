---
title: norm
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
menu_id: function-objects-field-norm
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Overview {#overview}

The `norm` function object normalises an input field with
a chosen norm, and writes a new normalised field.

## Usage {#usage}

Example of function object specification:

    norm1
    {
        // Mandatory entries
        type            norm;
        libs            (fieldFunctionObjects);
        field           <word>;
        norm            <word>;

        // Conditional entries

            // When norm = Lp
            p               <scalar>;

            // When norm = composite
            divisor         <Function1<scalar>>;

            // When norm = divisorField
            divisorField    <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "norm/norm.H", "norm" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "norm",
   "libfieldFunctionObjects" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
`type`        | Type name: norm                     | word | yes      | -
`libs`        | Library name: fieldFunctionObjects  | word | yes      | -
`field`       | Name of the operand field           | word | yes      | -
`norm`        | Name of normalisation operation     | word | yes      | -
`p`           | Norm exponent for the p-norm        | scalar | conditional | -
`divisor`     | Norm divisor for the composite norm | Function1\<scalar\> | conditional | -
`divisorField` | Divisor scalar field for the field norm | word | conditional | -

Options for the `norm` entry:

| Property | Description | Expression
|------|------------|-------------
| `L1` | L1/Taxicab norm  | $$ \vec{y} = \frac{\vec{x}}{\Sigma_{i=1}^n \vert x_i \vert} $$
| `L2` | L2/Euclidean norm| $$ \vec{y} = \frac{\vec{x}}{\sqrt{x_1^2 + ... + x_n^2}} $$
| `Lp` | p norm | $$ \vec{y} = \frac{\vec{x}}{(\Sigma_{i=1}^n \vert x_i \vert^p)^{1/p}} $$
| `max` | Maximum norm | $$ \vec{y} = \frac{\vec{x}}{\max\vert x_i \vert} $$
| `composite` |Composite norm with Function1 divisor| $$ \vec{y} = \frac{\vec{x}}{f(t)} $$
| `divisorField` |Normalise by field| $$ \vec{y} = \frac{\vec{x}}{\vec{z}} $$

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)
- Function1

Divisor quantity is perturbed by `SMALL` value to prevent any divisions
by zero irrespective of whether the quantity is close to zero or not.
{: .note}

### Operands

Operand      | Type                 | Location
|------------|----------------------|---------
Input        | `{vol,surface,polySurface}<Type>Field` | `<case>/<time>/`
Output file  | -                    | -
Output field | `{vol,surface,polySurface}<Type>Field` | `<case>/<time>/`

where `<Type>={Scalar,Vector,SphericalTensor,SymmTensor,Tensor}`.


## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/norm" %>

API:

- [Foam::functionObjects::norm](doxy_id://Foam::functionObjects::norm)

<%= history "v2206" %>


<!----------------------------------------------------------------------------->
