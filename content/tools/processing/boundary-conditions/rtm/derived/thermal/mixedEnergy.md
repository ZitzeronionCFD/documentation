---
title: mixedEnergy
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-mixedEnergy
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `mixedEnergy` is a boundary condition that provides
a mixed condition for internal energy.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            mixedEnergy;

        // Inherited entries
        ...
    }

<% assert :string_exists, "mixedEnergyFvPatchScalarField.H", "mixedEnergy" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `mixedEnergy`                   | word    | yes      | -

The inherited entries are elaborated in:

- mixedFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/basic/derivedFvPatchFields/mixedEnergy" %>

API:

- [Foam::mixedEnergyFvPatchScalarField](doxy_id://Foam::mixedEnergyFvPatchScalarField)

<%= history "2106" %>

<!----------------------------------------------------------------------------->
