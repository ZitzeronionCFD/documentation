---
title: controlDict
copyright:
- Copyright (C) 2021 OpenCFD Ltd.
tags:
- navigation
- system
- controlDict

menu_id: controldict
license: CC-BY-NC-ND-4.0
menu_parent: case-structure
---

<%= page_toc %>

The `controlDict` dictionary is used to specify the main case controls.  This
includes, e.g. timing information, write format, and optional libraries that can
be loaded at run time.

An example dictionary is shown below

<%= repo_file_snippet "icoFoam/cavity/cavity/system/controlDict" %>
