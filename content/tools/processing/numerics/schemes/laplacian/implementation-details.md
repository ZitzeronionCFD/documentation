---
title: Implementation details
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- implementation
- Laplacian
menu_id: laplacian-implementation-details
license: CC-BY-NC-ND-4.0
menu_parent: schemes-laplacian
---

<%= page_toc %>

For a cell with volume $$V$$, Gauss' theorem is applied to give:
$$
    \div \left( \Gamma \grad \phi \right)
  = \frac{1}{V}\int_V \div \left( \Gamma \grad \phi \right)dV
  = \frac{1}{V} \oint_S \Gamma \grad \phi d\vec{S}
  = \frac{1}{V} \sum_i^{\mathrm{nFace}}\left(\grad{\phi}\right)_{f,i} \dprod \Gamma_f \vec{S}_{f,i}
$$
i.e. the laplacian is given by the sum over all faces of the dot product of
the face normal with the gradient value at the face.  To transform the quantity
from the cell centre to the face centre, an
[interpolation scheme](../../interpolation) is required.

See <%= repo_file_link "laplacianScheme.C" %>
