---
title: Lagrangian particles
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
menu_id: solvers-lagrangian-particle
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

## Solvers {#solvers}

<%= insert_models "solvers-lagrangian-particle" %>

## Further information {#further-information}

- [Source documentation](doxy_id://grpLagrangianSolvers)
