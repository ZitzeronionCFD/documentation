---
title: Smagorinsky
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- LES
- turbulence
menu_id: les-smagorinsky
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-les
group: turbulence-les
---

<%= page_toc %>

## Properties {#properties}

## Model equations {#model-equations}

The turbulence viscosity is given by:

$$
    \nu_t = C_k \Delta k^{0.5}
$$

where the turbulence kinetic energy is given by the solution of the quadratic
equation:

$$
    a k^2 + b k + c = 0
$$

The coefficients are:

$$
    \begin{align}
        a &= \frac{C_e}{\Delta} \\
        b &= \frac{2}{3} \mathrm{tr} (\tensor{D}) \\
        c &= 2 C_k \Delta \left( \mathrm{dev}(\tensor{D}) \ddprod \tensor{D} \right)
    \end{align}
$$

and

$$
    \tensor{D} = \frac{1}{2} \left( \grad \u + \grad (\u)^T \right)
$$

## Default model coefficients {#default-coeffs}

$$ C_e $$ | $$ C_k $$
------------|------------
      1.048 |      0.094

## Initialisation {#initialisation}

## Usage {#usage}

The model is specified using:

    LES
    {
        turbulence      on;
        LESModel        Smagorinsky;
    }

<% assert :string_exists, "LESModel.C", "LES" %>
<% assert :string_exists, "LESModel.C", "LESModel" %>
<% assert :string_exists, "Smagorinsky.H", "Smagorinsky" %>

## Boundary conditions {#bcs}

Inlet

- <%= link_to_menuid "boundary-conditions-fixedValue" %>

Outlet

- [Constrained outlet](ref_id://boundary-conditions-inletOutlet)

Walls

- wall functions

## Further information {#further-information}

Source code:

- [Foam::LESModels::Smagorinsky](doxy_id://Foam::LESModels::Smagorinsky)

References:

- *Smagorinsky* <%= cite "smagorinsky_general_1963" %>
