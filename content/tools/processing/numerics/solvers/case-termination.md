---
title: Case termination
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- solvers
- termination
menu_id: linearequationsolvers-case-termination
license: CC-BY-NC-ND-4.0
menu_parent: numerics-linearequationsolvers
---

<%= page_toc %>

By default, cases will run until the time settings are achieved in the
case [controlDict](ref_id://controldict) dictionary.
Alternatively, the `residualControl` object can be added to the
[fvSolution](ref_id://fvsolution) dictionary to enable
additional controls.  This operates in two modes:

## Steady state {#steady-state}

To terminate the case when the initial residual of the field
equations falls below user-specified threshold values for SIMPLE-based
solvers:

    residualControl
    {
        p           1e-2;
        "(Ux Uy)"   1e-4;
        "(k|epsilon|omega)" 1e-4;
    }

<% assert :string_exists, "solutionControl.C", "residualControl" %>

## Transient {#transient}

To control the number of outer corrector iterations for PIMPLE-based solvers:

    // Maximum number of outer correctors
    nOuterCorrectors    50;

    residualControl
    {
        "(U|k|epsilon|omega)"
        {
            relTol          0;
            tolerance       1e-4;
        }
    }

<% assert :string_exists, "solutionControl.C", "residualControl" %>
