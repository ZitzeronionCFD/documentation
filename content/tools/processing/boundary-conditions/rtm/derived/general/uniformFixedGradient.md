---
title: uniformFixedGradient
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-uniformFixedGradient
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `uniformFixedGradient` is a boundary condition that
provides a uniform fixed gradient condition.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                uniformFixedGradient;
        uniformGradient     <PatchFunction1<Type>>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "uniformFixedGradientFvPatchField.H", "uniformFixedGradient" %>
<% assert :string_exists, "uniformFixedGradientFvPatchField.C", "uniformGradient" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `uniformFixedGradient` | word    | yes      | -
`uniformGradient`  | Uniform gradient values   | PatchFunction1\<Type\> | yes | -

The inherited entries are elaborated in:

- [fixedGradientFvPatchField.H](ref_id://boundary-conditions-fixedGradient)
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/uniformFixedGradient" %>

API:

- [Foam::uniformFixedGradientFvPatchField](doxy_id://Foam::uniformFixedGradientFvPatchField)

<%= history "2.2.1" %>

<!----------------------------------------------------------------------------->
