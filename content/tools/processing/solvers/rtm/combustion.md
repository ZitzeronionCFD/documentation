---
title: Combustion
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
menu_id: solvers-combustion
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

## Solvers {#solvers}

Solver applications in the *combustion* category focus on compressible,
multi-specie and multi-phase systems, with additional modelling for combustion
and chemistry effects.  Options include:

<%= insert_models "solvers-combustion" %>

## Further information {#further-information}

- [Source documentation](doxy_id://grpCombustionSolvers)
