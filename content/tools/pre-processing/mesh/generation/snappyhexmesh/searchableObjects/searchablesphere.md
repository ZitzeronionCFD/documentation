---
title: searchableSphere
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: searchablesphere
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh-geometry
group: searchableobjects
---

<%= page_toc %>

![sphere example](../images/sphere-small.png)

A sphere given by its centre radius.

## Usage

    sphere
    {
        type            searchableSphere;
        centre          (0 0 0);
        radius          3;
    }

<% assert :string_exists, "searchableSphere.H", "searchableSphere" %>
<% assert :string_exists, "searchableSphere.C", "centre" %>
<% assert :string_exists, "searchableSphere.C", "radius" %>

Where:

Property  | Description                  | Required
----------|------------------------------|---------
`centre`  | Centre of sphere             |      yes
`radius`  | Radius of sphere             |      yes
