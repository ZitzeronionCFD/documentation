---
title: nearWallFields
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- near-wall
- post-processing
menu_id: function-objects-field-nearwallfields
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `nearWallFields` function object
samples near-patch volume fields within an input distance range.

### Operands

Operand       | Type             | Location
:---          |:---              |:---
input         | `vol<Type>Field` | `$FOAM_CASE/<time>/<inpField>`
output file   | -                | -
output field  | `vol<Type>Field` | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `nearWallFields` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    nearWallFields1
    {
        // Mandatory entries (unmodifiable)
        type            nearWallFields;
        libs            (fieldFunctionObjects);
        fields
        (
            (<field1> <outField1>)
            (<field2> <outField2>)
        );
        patches         (<patch1> <patch2> ... <patchN>);
        distance        0.01;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "nearWallFields/nearWallFields.H", "nearWallFields" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "nearWallFields",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "nearWallFields.C", "fields" %>
<% assert :string_exists, "nearWallFields.C", "patches" %>
<% assert :string_exists, "nearWallFields.C", "distance" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: nearWallFields           | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 fields       | Names of input-output fields        | wordHashTable | yes | -
 patches      | Names of patches to sample          | wordList | yes  | -
 distance     | Wall-normal distance from patch to sample | scalar | yes  | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/nearWallFields" %>

API:

- [Foam::functionObjects::nearWallFields](doxy_id://Foam::functionObjects::nearWallFields)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->