---
title: fixedValue
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-fixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Properties {#properties}

- Dirichlet
- explicit

Face values are evaluated according to:
$$
    \phi_f = \phi_{\ref}
$$

where
$$\phi_f$$
: face value

$$\phi_{\ref}$$
: reference value

## Usage {#usage}

    <patchName>
    {
        type        fixedValue;
        value       <field value>;
    }

## Further information {#further-information}

Source code:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
