---
title: Tabulated
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- transport
menu_id: transport-tabulated
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-transport
group: transport-models
---

<%= page_toc %>

Dynamic viscosity, $$ \mu $$ \[Pa.s\]

$$
\mu = f(p, T)
$$

Thermal conductivity, $$ \kappa $$ \[W/mK\]

$$
\kappa = f(p, T)
$$

Thermal diffusivity, $$ \alpha_h $$ \[kg/ms\]

$$
\alpha_h = \frac{\kappa}{c_p}
$$

## Usage

The `tabulated` transport model is defined using the entries:

~~~
transport
{
    mu
    (
        (<T0>     <mu0>)
        (<T1>     <mu1>)
        (<T2>     <mu2>)
        ...
        (<TN>     <muN>)
    );

    kappa
    (
        (<T0>     <kappa0>)
        (<T1>     <kappa1>)
        (<T2>     <kappa2>)
        ...
        (<TN>     <kappaN>)
    );
}
~~~

Where

- `mu` : List of tuples of (temperature viscosity) values describing the dynamic
  viscosity table
- `kappa` : List of tuples of (temperature conductivity) values describing the
  thermal conductivity table

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/transport/sutherland" %>
