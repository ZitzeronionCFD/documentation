---
title: Linear equation solvers
short_title: Solvers
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- solvers
menu_id: numerics-linearequationsolvers
license: CC-BY-NC-ND-4.0
menu_parent: numerics
---

<%= page_toc %>

## Introduction {#intro}

This section describes the linear solver options available to solve the matrix
system

$$
    \mat{A} \vec{x} = \vec{b}
$$

where:

$$\mat{A}$$
: coefficient matrix

$$\vec{x}$$
: vector of unknowns

$$\vec{b}$$
: source vector

If the coefficient matrix only has values on its diagonal, the solution vector
can be obtained inverting the matrix system:

$$
    \vec{x} = \mat{A}^{-1} \vec{b}
$$

Where the inverse of the diagonal matrix is simply:

$$
    \mat{A}^{-1} = \frac{1}{\mathrm{diag}(\mat{A})}
$$

This is available as the `diagonalSolver`.  More typically the matrix cannot be
inverted easily and the system is solved using iterative methods, as described
in the following sections.

## Options {#options}

Solver options include:

- [Smooth](ref_id://linearequationsolvers-smooth)
- [Conjugate Gradient](ref_id://linearequationsolvers-cg)
- [Multigrid](ref_id://linearequationsolvers-multigrid)

## Solver control {#control}

- [Under relaxation](ref_id://linearequationsolvers-underrelaxation)
- [Residuals](ref_id://linearequationsolvers-residuals)
- [Case termination](ref_id://linearequationsolvers-case-termination)

## Common usage {#common-usage}

- `minIter`: minimum number of solver iterations
- `maxIter`: maximum number of solver iterations
- `nSweeps`: number of solver iterations between checks for solver convergence

<% assert :string_exists, "LduMatrixSolver.C", "minIter" %>
<% assert :string_exists, "LduMatrixSolver.C", "maxIter" %>

## Implementation details {#implementation-details}

### Matrix structure

Matrix coefficients are stored in upper-triangular order

- neighbour cell index always higher than owner cell index across a face
- when looping over cell faces, the face index increases with increasing cell
  index
- for the 1-D case, if cell index 0 is at the boundary, this equates to a
  monotonic increase in cell numbers, i.e. defines a continuous sweep across
  the 1-D region
- used in [Gauss-Seidel](ref_id://smooth-gauss-seidel)
  method

## Further information {#further-information}

- [Source documentation](doxy_id://grpLduMatrix)
