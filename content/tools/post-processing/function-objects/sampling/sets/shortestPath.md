---
title: shortestPath
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- points
- post-processing
- sampling
- sets
menu_id: sampling-sets-shortestPath
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-sampling-sets
group: sampling-sets
---

<%= page_toc %>

The `shortestPath` set determines the shortest path between a set of inside and
outside points, by walking from cell centre to cell centre.

This is particularly useful to determine holes in surface files used in
meshing processes.

At least one to sample field must be specified
{: .note}

## Usage {#usage}

Basic operation of the `shortestPath` set comprises:

    sets1
    {
        type            sets;
        libs            (sampling);

        mySet1
        {
            type            shortestPath;
            axis            <axis>;
            insidePoints    (<points>);
            outsidePoints   (<points>);
        }
    }

<% assert :string_exists, "sampledSets.H", "sets" %>
<% assert :class_library_exists, "sampling/Make/files", "sampledSets", "libsampling" %>
<% assert :string_exists, "shortestPathSet.H", "shortestPath" %>
<% assert :string_exists, "shortestPathSet.C", "insidePoints" %>
<% assert :string_exists, "shortestPathSet.C", "outsidePoints" %>

## Sample output {#sample-output}

The following image is taken from a `motorBike` tutorial case where visor patch
and some patch faces on the back of the motorbike have been removed. During
meshing, these changes allow the mesh to flood into previously closed surfaces.

![Example](../shortestPath-example.png)

Application of the `shortestPath` set enables the leak path to be identified
easily.

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/sampling/sampledSet/shortestPath" %>

API:

- [Foam::shortestPathSet](doxy_id://Foam::shortestPathSet)

Example usage:

- simpleFoam tutorials:
  - <%= repo_link2(
  "$FOAM_TUTORIALS/mesh/snappyHexMesh/motorBike_leakDetection") %>

<%= history "v1812" %>
