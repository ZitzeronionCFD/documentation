---
title: Limited linear divergence scheme
short_title: Limited linear
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-limitedlinear
license: CC-BY-NC-ND-4.0
group: schemes-divergence-nvdtvd
---

<%= page_toc %>

## Properties {#properties}

- First/second order
- Unbounded

## Normalised Variable Diagram {#nvd}

![limitedLinear](../limitedLinear-10.png)

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss limitedLinear <coeff>;
    }

<% assert :string_exists, "limitedLinear.H", "limitedLinear" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/limitedSchemes/limitedLinear" %>

API:

- [Foam::limitedLinearLimiter](doxy_id://Foam::limitedLinearLimiter)
