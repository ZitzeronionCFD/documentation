---
title: atmBoundaryLayerInletOmega
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-atmBoundaryLayerInletOmega
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

The `atmBoundaryLayerInletOmega` is a boundary condition that provides
boundary condition for the specific dissipation rate (i.e. `omega`)
for homogeneous, two-dimensional, dry-air, equilibrium and neutral
atmospheric boundary layer modelling.

The ground-normal `omega` profile expression:

$$
    \omega = \frac{u^*}{\kappa \sqrt{C_\mu}} \frac{1}{z - d + z_0}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\omega$$ | Ground-normal specific dissipation rate profile \[m^2/s^3\]
$$u^*$$    | Friction velocity                               \[m/s\]
$$\kappa$$ | von Karman constant                             \[-\]
$$C_\mu$$  | Empirical model constant                        \[-\]
$$z$$      | Ground-normal coordinate component              \[m\]
$$d$$      | Ground-normal displacement height               \[m\]
$$z_0$$    | Aerodynamic roughness length                    \[m\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            atmBoundaryLayerInletOmega;

        // Inherited entries
        ...
    }

<% assert :string_exists, "atmBoundaryLayerInletOmegaFvPatchScalarField.H", "atmBoundaryLayerInletOmega" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `atmBoundaryLayerInletOmega`    | word    | yes      | -

The inherited entries are elaborated in:

- atmBoundaryLayer.H
- inletOutletFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmDownstreamDevelopment" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/atmBoundaryLayerInletOmega" %>

API:

- [Foam::atmBoundaryLayerInletOmegaFvPatchScalarField](doxy_id://Foam::atmBoundaryLayerInletOmegaFvPatchScalarField)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
