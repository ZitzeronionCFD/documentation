---
title: Stokes V
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- regular
- Stokes
- waves
menu_id: waves-regular-stokesv
license: CC-BY-NC-ND-4.0
menu_parent: models-waves
group: wave-regular
---

<%= page_toc %>

## Properties {#properties}

- Regular waves

## Model equations {#model-equations}

The wave height is modelled by the equation:

$$
    \eta =
        \frac{\lambda}{k} \cos (k x -\omega t + \phi)
      + \frac{(\lambda^2 B_{22} + \lambda^4 B_{24})}{\lambda}
        \cos\left(2(k x - \omega t + \phi)\right)
      + \frac{(\lambda^3 B_{33} + \lambda^5 B_{35})}{\lambda}
        \cos\left(3(k x - \omega t + \phi)\right)
      + \frac{(\lambda^4 B_{44})}{\lambda}
        \cos\left(4(k x - \omega t + \phi)\right)
      + \frac{(\lambda^5 B_{55})}{\lambda}
        \cos\left(5(k x - \omega t + \phi)\right)
$$

Where:

$$\lambda$$
: first order wave amplitude

$$k$$
: wave number

$$\omega$$
: angular frequency

$$\phi$$
: phase shift

$$B_{xx}$$
: coefficients in the fifth order solution

$$t$$
: time

## Default model coefficients {#default-coeffs}

## Usage {#usage}

Inlet patch example

    <patch>
    {
        alpha           alpha.water;
        waveModel       StokesV;
        nPaddle         1;
        waveHeight      0.1;
        waveAngle       0.0;
        rampTime        4.0;
        activeAbsorption yes;
        wavePeriod      2.0;
    }

## Further information {#further-information}

Source code:

- [Foam::waveModels::StokesV](doxy_id://Foam::waveModels::StokesV)

References:

- ...

Tutorials:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/stokesV" %>
