---
title: wideBandDiffusiveRadiation
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-wideBandDiffusiveRadiation
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `wideBandDiffusiveRadiation` is a mixed boundary condition that
provides a wide-band, diffusive radiation
condition, where the patch temperature is specified.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            wideBandDiffusiveRadiation;

        // Inherited entries
        ...
    }

<% assert :string_exists, "wideBandDiffusiveRadiationMixedFvPatchScalarField.H", "wideBandDiffusiveRadiation" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `wideBandDiffusiveRadiation`    | word    | yes      | -

The inherited entries are elaborated in:

- mixedFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/chtMultiRegionFoam/solarBeamWithTrees" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/radiation/derivedFvPatchFields/wideBandDiffusiveRadiation" %>

API:

- [Foam::radiation::wideBandDiffusiveRadiationMixedFvPatchScalarField](doxy_id://Foam::radiation::wideBandDiffusiveRadiationMixedFvPatchScalarField)

<%= history "1.6" %>

<!----------------------------------------------------------------------------->
