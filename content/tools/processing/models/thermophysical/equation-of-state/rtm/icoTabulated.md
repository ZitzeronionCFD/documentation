---
title: Incompressible tabulated
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- equation of state
- incompressible
- tabulated
menu_id: equationOfState-icoTabulated
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-equationOfState
group: equation-of-state-models
---

<%= page_toc %>

Density, $$ \rho $$ \[kg/m$$^3$$\]

$$
\rho = f(p, T)
$$

Entropy, $$ S $$ \[J/kg/K \]

$$
S = 0
$$

Compressibility, $$ \psi $$ \[s$$^2$$/m$$^2$$\]

$$
\psi = 0
$$

## Usage

The `icoTabulated` equation of state is specified as:

~~~
equationOfState
{
    rho
    (
        (<T1> <rho1>)
        (<T2> <rho2>)
        (<T3> <rho3>)
        ...
        (<Tn> <rhon>)
    );
}
~~~

Where

- `rho` : List of tuples of (temperature density) describing the table

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/equationOfState/icoTabulated" %>
