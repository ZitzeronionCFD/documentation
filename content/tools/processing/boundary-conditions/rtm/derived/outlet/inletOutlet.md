---
title: Inlet outlet
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- boundary conditions
- outlet
menu_id: boundary-conditions-inletOutlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-outlet
group: bcs-outlet-general
---

<%= page_toc %>

## Properties {#properties}

- Wrapper around the [mixed](ref_id://boundary-conditions-mixed) condition
- Applicable to all variables
- Sets the patch value to a user-specified
  [fixedValue](ref_id://boundary-conditions-fixedGradient) for reverse flow
- Outflow treated using a
  [zeroGradient](ref_id://boundary-conditions-zeroGradient) condition

## Usage {#usage}

The condition is specified in the field file using:

    <patchName>
    {
        type            inletOutlet;
        inletValue      <field value>;
        value           <field value>;

        // Optional entries
        phi             phi;
    }

<% assert :string_exists, "inletOutletFvPatchField.H", "inletOutlet" %>
<% assert :string_exists, "inletOutletFvPatchField.C", "inletValue" %>
<% assert :string_exists, "inletOutlet/inletOutletFvPatchField.C", "phi" %>

## Further information {#further-information}

Source code:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
- [Foam::zeroGradientFvPatchField](doxy_id://Foam::zeroGradientFvPatchField)
- [Foam::outletInletFvPatchField](doxy_id://Foam::outletInletFvPatchField)
