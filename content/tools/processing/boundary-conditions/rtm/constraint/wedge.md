---
title: Wedge
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
- constraint
menu_id: boundary-conditions-wedge
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-constraint
group: boundary-conditions-constraint
---

<%= page_toc %>

## Properties {#properties}

Applied in pairs to 2-D rotationally symmetric cases to represent planes in
the swirl direction.

## Usage {#usage}

The condition requires entries in both the `boundary` and field files. Mesh
`boundary` file:

    <patchName>
    {
        type            wedge;
        ...
    }

Field file:

    <patchName>
    {
        type            wedge;
    }

## Details {#details}

- The patch pair must straddle a co-ordinate plane equally
- Separation between `wedge` patch pairs must be
  - a single layer of cells
  - points should appear collocated when viewed in the mid-plane normal
    direction
- Particles will be snapped to the mid-plane between pairs of `wedge` patches

## Further information {#further-information}

Source code:

- [Foam::wedgeFvPatchField](doxy_id://Foam::wedgeFvPatchField)
