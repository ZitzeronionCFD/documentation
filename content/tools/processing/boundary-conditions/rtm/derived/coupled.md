---
title: Coupled conditions
short_title: Coupled
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-coupled
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

## Boundary mesh type {#boundary-mesh-type}

Coupled conditions require the `type` entry to be set in the
`$FOAM_CASE/constant/polyMesh/boundary` file, e.g.:

    <patchName>
    {
        type            <couple type>;
        ...
    }

This is explained in further detail in the documentation for each coupled
boundary type.

## Coupled conditions {#coupled-conditions}

Available coupled conditions include

<%= insert_models "bcs-coupled", "bcs-coupled-jump"%>

## Further information {#further-information}

Source code:

- [Foam::coupledPolyPatch](doxy_id://Foam::coupledPolyPatch)
- [Foam::coupledFvPatch](doxy_id://Foam::coupledFvPatch)

See also

- [grpCoupledBoundaryConditions](doxy_id://grpCoupledBoundaryConditions)
