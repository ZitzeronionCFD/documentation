---
title: prghTotalHydrostaticPressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-prghTotalHydrostaticPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `prghTotalHydrostaticPressure` is a boundary condition that provides
static pressure condition for `p_rgh`, calculated as:

$$
    p_{rgh} = {ph}_{rgh} - 0.5 \rho |\vec{u}|^2
$$

where:

Property             | Description
---------------------|-------
$$p_{rgh}$$          | Pressure: $$ \rho g (h - h_{ref}) $$ \[Pa\]
$$p$$                | Hydrostatic pressure: $$\rho g (h - h_{ref})$$ \[Pa\]
$$h$$                | Height in the opposite direction to gravity
$$h_{ref}$$          | Reference height in the opposite direction to gravity
$$\rho$$             | Density
$$g$$                | Acceleration due to gravity \[m/s^2\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            prghTotalHydrostaticPressure;

        // Optional entries
        rho             <word>;
        phi             <word>;
        U               <word>;
        ph_rgh          <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "prghTotalHydrostaticPressureFvPatchScalarField.H", "prghTotalHydrostaticPressure" %>
<% assert :string_exists, "prghTotalHydrostaticPressureFvPatchScalarField.C", "rho" %>
<% assert :string_exists, "prghTotalHydrostaticPressureFvPatchScalarField.C", "phi" %>
<% assert :string_exists, "prghTotalHydrostaticPressureFvPatchScalarField.C", "U" %>
<% assert :string_exists, "prghTotalHydrostaticPressureFvPatchScalarField.C", "ph_rgh" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `prghTotalHydrostaticPressure` | word    | yes      | -
`rho`              | Name of density field     | word    | no       | rho
`phi`              | Name of flux field        | word    | no       | phi
`U`                | Name of velocity field    | word    | no       | U
`ph_rgh`           | Name of ph_rgh field      | word    | no       | ph_rgh

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/fireFoam/LES/smallPoolFire2D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/prghTotalHydrostaticPressure" %>

API:

- [Foam::prghTotalHydrostaticPressureFvPatchScalarField](doxy_id://Foam::prghTotalHydrostaticPressureFvPatchScalarField)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
