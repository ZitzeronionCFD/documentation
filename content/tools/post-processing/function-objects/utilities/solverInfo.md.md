---
title: solverInfo
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- function object
- post-processing
- solver
menu_id: function-objects-utilities-solverinfo
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-utilities
group: function-objects-utilities
---

<%= page_toc %>

The `solverInfo` function object reports the solver performance information for
a list of fields

- residual fields
- solver type
- initial residual
- final residual
- number of solver iterations
- convergence flag

## Usage {#usage}

The `solverInfo` function object is specified using:

    solverInfo
    {
        type            solverInfo;
        libs            ("libutilityFunctionObjects.so");
        ...
        fields          (U p);
        writeResidualFields yes;
    }

<% assert :string_exists, "solverInfo.H", "solverInfo" %>
<% assert :class_library_exists,
   "functionObjects/utilities/Make/files",
   "solverInfo",
   "libutilityFunctionObjects" %>
<% assert :string_exists, "solverInfo.C", "fields" %>
<% assert :string_exists, "solverInfo.C", "writeResidualFields" %>

## Sample output {#sample-output}

Solver information is written to the directory according to the
[executeControl](ref_id://post-processing-function-objects#usage-run-time-control)
settings:

    $FOAM_CASE/postProcessing/<functionObject>/<timeDir>

An example output is shown below

    # Solver information
    # Time          p_rgh_solver    p_rgh_initial   p_rgh_final     p_rgh_iters     p_rgh_converged
    50              GAMG            9.294030e-03    4.311920e-05    4               1
    100             GAMG            1.168520e-02    3.014190e-05    3               1
    150             GAMG            6.986510e-03    1.489130e-05    3               1
    200             GAMG            6.059480e-03    5.917190e-05    2               1
    ...

If the `writeResidualFields` is active, residual fields will be output according
to the [writeControl](<%= ref_id "post-processing-function-objects", "usage-run-time-control" %>)  settings.

## Stored properties {#stored-properties}

### Reduced data

The following data are stored by the function object, and can be used by other
function objects:

- `<field-name><component>_initial` : initial residual
- `<field-name><component>_final` : final residual
- `<field-name><component>_iters` : number of solver iterations

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/utilities/solverInfo" %>

API:

- [Foam::functionObjects::solverInfo](doxy_id://Foam::functionObjects::solverInfo)

Related:

- [Solver residual calculation](ref_id://linearequationsolvers-residuals)

<%= history "v3.0+", [{"v1906" => "solverInfo"}, {"v3.0+" => "residuals"}] %>
