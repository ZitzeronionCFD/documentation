---
title: fixedProfile
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-fixedProfile
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `fixedProfile` is a generic boundary condition that provides a fixed
value profile condition.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedProfile;
        profile         <Function1<Type>>;
        dir             <vector>;
        origin          <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "fixedProfileFvPatchField.H", "fixedProfile" %>
<% assert :string_exists, "fixedProfileFvPatchField.C", "profile" %>
<% assert :string_exists, "fixedProfileFvPatchField.C", "dir" %>
<% assert :string_exists, "fixedProfileFvPatchField.C", "origin" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `fixedProfile` | word    | yes      | -
`profile`          | Profile data              | Function1\<Type\>    | yes | -
`dir`              | Profile direction         | vector  | yes      | -
`origin`           | Profile origin            | scalar  | yes      | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fixedProfile" %>

API:

- [Foam::fixedProfileFvPatchField](doxy_id://Foam::fixedProfileFvPatchField)

<%= history "v3.0+" %>

<!----------------------------------------------------------------------------->
