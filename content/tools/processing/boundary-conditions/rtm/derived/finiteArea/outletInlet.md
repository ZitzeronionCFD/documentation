---
title: outletInlet
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-outletInlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-fa
group: bcs-fa
---

<%= page_toc %>

## Description

The `outletInlet` is a boundary condition that provides
a generic inflow condition, with specified outflow for the case of reverse flow.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            outletInlet;
        outletValue     <Field>;

        // Optional entries
        phi             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "outletInletFaPatchField.H", "outletInlet" %>
<% assert :string_exists, "outletInletFaPatchField.C", "outletValue" %>
<% assert :string_exists, "outletInletFaPatchField.C", "phi" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `outletInlet`                   | word    | yes      | -
`outletValue`          | Outlet value field                         | scalarField | yes  | -
`phi`                  | Name of flux field                         | word    | no       | phi

The inherited entries are elaborated in:

- mixedFaPatchField.H
<!-- end of the list -->

- The mode of operation is determined by the sign of the flux across the
  patch edges.
- Sign conventions:
  - Positive flux (out of domain): apply the "outletValue" fixed-value
  - Negative flux (into domain): apply zero-gradient condition
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/finiteArea/surfactantFoam/planeTransport" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteArea/fields/faPatchFields/derived/outletInlet" %>

API:

- [Foam::outletInletFaPatchField](doxy_id://Foam::outletInletFaPatchField)

<%= history "v1712" %>

<!----------------------------------------------------------------------------->
