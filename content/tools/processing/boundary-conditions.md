---
title: Boundary conditions
copyright:
- Copyright (C) 2016-2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions
license: CC-BY-NC-ND-4.0
menu_parent: processing
---

<%= page_toc %>

In the absence of sources and sinks, system behaviour is driven by its boundary
conditions.  These form a critical aspect of case specification where ill-posed
combinations will lead to physically incorrect predictions, and in many cases,
solver failure.

OpenFOAM offers a wide range of conditions, grouped according to:

- [Constraints](ref_id://boundary-conditions-constraint): geometrical constraints, e.g. for
  2-D, axisymmetric etc.
- [General](ref_id://boundary-conditions-general): available to all patch types and
  fields
- [Inlet](ref_id://boundary-conditions-inlet): inlet conditions
- [Outlet](ref_id://boundary-conditions-outlet): outlet conditions
- [Wall](ref_id://boundary-conditions-wall): wall conditions
- [Coupled](ref_id://boundary-conditions-coupled): coupled conditions, e.g. cyclic

## Usage {#usage}

Boundary conditions are assigned in the `boundaryField` section of the field
files within each time directory for each mesh patch.  The format follows:

    boundaryField
    {
        <patch 1>
        {
            type        <patch type>;
            ...
        }
        <patch 2>
        {
            type        <patch type>;
            ...
        }
        ...
        <patch N>
        {
            type        <patch type>;
            ...
        }
    }

Each condition is set in a dictionary given by the name of the underlying mesh
patch, according to the `type` keyword.

## Details {#details}

- For 1-sided, e.g. external boundaries, the normal vector points out of the
  domain
- Non-orthogonality not included

![Boundary conditions schematic](boundary-conditions-schematic-small.png)

Used when solving the general transport equation to provide:

- value at boundary
- gradient at boundary

## Further information {#further-information}

Usage

- <%= link_to_menuid "boundary-conditions-common-combinations" %>
- Many conditions employ a `Function1` type to describe a property as a function
  of another, typically time

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields" %>

API:

- [Foam::Function1](doxy_id://Foam::Function1)

See also:

- [Source documentation](doxy_id://grpBoundaryConditions)
