---
title: distributedTriSurfaceMesh
copyright:
- Copyright (C) 2019-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: distributedtrisurfacemesh
license: CC-BY-NC-ND-4.0
#menu_parent: snappyhexmesh-geometry
group: trisurfacemeshes
---

<%= page_toc %>

## Description

A distributed version of `triSurfaceMesh`.  Usage and specification is the same
as for `triSurfaceMesh`, the difference is in the underlying decomposition of
the surface into equal parts across all processors, and parallelisation of all
queries, e.g. finding the nearest point on the surface, finding intersections.
The routines have been especially written to handle large surfaces with minimal
memory overhead.

Internally, each processor represents the surface using bounding boxes, and
it is therefore beneficial to have similarly sized triangles to minimise the
amount of overlap and processor communication.

## Usage

An example use of the `distributedTriSurfaceMesh`:

## Further information

See also

- <%= link_to_menuid "trisurfacemesh" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/mesh/snappyHexMesh/distributedTriSurfaceMesh}
