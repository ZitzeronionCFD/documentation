---
title: compressible-thermalBaffle
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-compressible-thermalBaffle
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-meshMotion
group: bcs-meshMotion
---

<%= page_toc %>

## Description

The `compressible::thermalBaffle` is a boundary condition that provides a
coupled temperature condition between multiple mesh regions.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            thermalBaffle;

        // Optional entries
        internal        <bool>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "thermalBaffleFvPatchScalarField.H", "compressible::thermalBaffle" %>
<% assert :string_exists, "thermalBaffleFvPatchScalarField.C", "internal" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `compressible-thermalBaffle`    | word    | yes      | -
`internal`     | Flag to deduce if the baffle is internal   | bool    | no       | true

The inherited entries are elaborated in:

- turbulentTemperatureRadCoupledMixedFvPatchScalarField.H
- thermalBaffleModel.H
- extrudePatchMesh.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantSimpleFoam/roomWithThickCeiling" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/regionModels/thermalBaffleModels/derivedFvPatchFields/thermalBaffle" %>

API:

- [Foam::compressible::thermalBaffleFvPatchScalarField](doxy_id://Foam::compressible::thermalBaffleFvPatchScalarField)

<%= history "2.2.0" %>

<!----------------------------------------------------------------------------->
