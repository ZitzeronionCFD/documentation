---
title: Steady state time scheme
short_title: Steady state
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- time
menu_id: schemes-time-steadystate
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes-time
group: schemes-time
---

<%= page_toc %>

## Properties {#properties}

- Steady state
- Sets temporal derivative contributions to zero

$$
    \ddt{\phi} = 0
$$

## Usage {#usage}

The scheme is specified using:

    ddtSchemes
    {
        default         steadyState;
    }

## Further information {#further-information}

Source code

- [Foam::fv::steadyStateDdtScheme](doxy_id://Foam::fv::steadyStateDdtScheme)

Additional

- (scheme settings)[<%= ref_id "schemes-divergence", "special-cases-steady-state"%>)
