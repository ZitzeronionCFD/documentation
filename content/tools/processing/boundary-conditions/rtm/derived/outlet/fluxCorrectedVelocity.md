---
title: fluxCorrectedVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- outlet
menu_id: boundary-conditions-fluxCorrectedVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-outlet
group: bcs-outlet-general
---

<%= page_toc %>

## Description

The `fluxCorrectedVelocity` is a velocity-outlet boundary condition that
corrects the extrapolated velocity to match the specified flow rate.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fluxCorrectedVelocity;

        // Optional entries
        phi             <word>;
        rho             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "fluxCorrectedVelocityFvPatchVectorField.H", "fluxCorrectedVelocity" %>
<% assert :string_exists, "fluxCorrectedVelocityFvPatchVectorField.C", "phi" %>
<% assert :string_exists, "fluxCorrectedVelocityFvPatchVectorField.C", "rho" %>


where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `fluxCorrectedVelocity`         | word    | yes      | -
`phi`                  | Name of flux field                         | word    | no       | phi
`rho`                  | Name of density field                      | word    | no       | rho

The inherited entries are elaborated in:

- [Foam::fvPatchField](doxy_id://Foam::fvPatchField)
- [Foam::zeroGradientFvPatchField](doxy_id://Foam::zeroGradientFvPatchField)
<!-- end of the list -->

## Method

$$
  \mathbf{u}_p =
  \mathbf{u}_c
- \mathbf{n} (\mathbf{n} \cdot \mathbf{u}_c)
+ \frac{\mathbf{n} \phi_p}{|\mathbf{S}_f|}
$$

where:

Property             | Description
---------------------|-------
$$\mathbf{u}_p$$     | Patch velocity                            \[m/s\]
$$\mathbf{u}_c$$     | Velocity in cells adjacent to the patch   \[m/s\]
$$\mathbf{u}_c$$     | Patch-normal vectors                      \[-\]
$$\phi_p$$           | Patch flux                                \[m^3/s\] or \[kg/s\]
$$\mathbf{S}_f$$     | Patch face area vectors                   \[m^2\]


## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/multiphaseEulerFoam/damBreak4phase" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fluxCorrectedVelocity" %>

API:

- [Foam::fluxCorrectedVelocityFvPatchVectorField](doxy_id://Foam::fluxCorrectedVelocityFvPatchVectorField)

<%= history "1.5" %>


<!----------------------------------------------------------------------------->
