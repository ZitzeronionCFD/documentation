---
title: clampedPlate
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-clampedPlate
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-fa
group: bcs-fa
---

<%= page_toc %>

## Description

The `clampedPlate` is a boundary condition that provides
a clamped condition which sets zero fixed value and zero gradient.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            clampedPlate;

        // Inherited entries
        ...
    }

<% assert :string_exists, "clampedPlateFaPatchField.H", "clampedPlate" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `clampedPlate`                  | word    | yes      | -

The inherited entries are elaborated in:

- faPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/acousticFoam/obliqueAirJet" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteArea/fields/faPatchFields/derived/clampedPlate" %>

API:

- [Foam::clampedPlateFaPatchField](doxy_id://Foam::clampedPlateFaPatchField)

<%= history "v1906" %>

<!----------------------------------------------------------------------------->
