---
title: Geometric constraint conditions
short_title: Constraint
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-constraint
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

Some cases can be reduced to their equivalent simplified systems by taking
advantage of dimensionality, symmetric and cyclic behaviour.  In OpenFOAM,
this is achieved by the application of *constraint* conditions, e.g. for

- 2-D cases: empty, wedge
- reduced geometry cases: symmetry, cyclic
- parallel cases: processor

## Options {#options}

Available constraint conditions include

<%= insert_models "boundary-conditions-constraint" %>

## Boundary mesh type {#boundary-mesh-type}

Constraint conditons are specified using the `<constraint type>` type entry
in the `$FOAM_CASE/constant/polyMesh/boundary` file:

    patchName
    {
        type            <constraint type>;
        ...
    }

## Field type {#field-type}

The field type entry typically takes the same value as the boundary mesh type,
with additional properties as required

    patchName
    {
        type            <constraint type>;
        ...
    }

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/constraint" %>

See also

- [grpConstraintBoundaryConditions](doxy_id://grpConstraintBoundaryConditions)
