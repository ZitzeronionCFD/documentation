---
title: swirlInletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-swirlInletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `swirlInletVelocity` is a velocity boundary condition
that describes an inlet vector boundary condition in swirl coordinates given a
central axis, central point, axial, radial and tangential velocity profiles.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type                patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                swirlInletVelocity;
        axialVelocity       <Function1<scalar>>;
        radialVelocity      <Function1<scalar>>;
        tangentialVelocity  <Function1<scalar>>;
        origin         <vector>;
        axis           <vector>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "swirlInletVelocityFvPatchVectorField.H", "swirlInletVelocity" %>
<% assert :string_exists, "swirlInletVelocityFvPatchVectorField.C", "axialVelocity" %>
<% assert :string_exists, "swirlInletVelocityFvPatchVectorField.C", "radialVelocity" %>
<% assert :string_exists, "swirlInletVelocityFvPatchVectorField.C", "tangentialVelocity" %>
<% assert :string_exists, "swirlInletVelocityFvPatchVectorField.C", "origin" %>
<% assert :string_exists, "swirlInletVelocityFvPatchVectorField.C", "axis" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `swirlInletVelocity`            | word    | yes      | -
`axialVelocity`        | Axial-velocity profile                     | Function1\<scalar\> | yes | -
`radialVelocity`       | Radial-velocity profile                    | Function1\<scalar\> | yes | -
`tangentialVelocity`   | Tangential-velocity profile                | Function1\<scalar\> | yes | -
`origin`               | Origin of rotation                         | vector  | no       | Zero
`axis`                 | Axis of rotation                           | vector  | no       | Zero

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/swirlInletVelocity" %>

API:

- [Foam::swirlInletVelocityFvPatchVectorField](doxy_id://Foam::swirlInletVelocityFvPatchVectorField)

<%= history "v1712" %>

<!----------------------------------------------------------------------------->
