---
title: Financial
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
menu_id: solvers-financial
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

## Solvers {#solvers}

- financialFoam

<!-- <%= insert_models "solvers-financial" %> -->

## Further information {#further-information}

- [Source documentation](doxy_id://grpFinancialSolvers)
