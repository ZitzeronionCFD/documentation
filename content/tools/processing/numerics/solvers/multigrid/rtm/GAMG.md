---
title: GAMG Solver
short_title: GAMG
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- multigrid
- GAMG
- solvers
menu_id: multigrid-gamg
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-multigrid
group: solvers-multigrid
---

<%= page_toc %>

## Properties {#properties}

- symmetric or asymmetric matrices
- run-time selectable [smoother](<%= ref_id "linearequationsolvers-smooth", "smoothers" %>)
- efficient transport of information across the solution domain
- moderate parallel scaling

## Usage {#usage}

Many of the `GAMG` solver inputs can be omitted and left to their default values
such that the basic usage comprises:

    solver                  GAMG;
    smoother                <smoother>;
    relTol                  <relative tolerance>;
    tolerance               <absolute tolerance>;

For more complete control, the full set of input entries includes:

    // Mandatory entries
    solver                  GAMG;
    smoother                <smoother>;
    relTol                  <relative tolerance>;
    tolerance               <absolute tolerance>;


    // Optional entries and associated default values

    // Agglomeration
    cacheAgglomeration      yes;
    nCellsInCoarsestLevel   10;
    processorAgglomerator   <processor agglomeration method>;

    // Solver
    nPreSweeps              0;
    preSweepsLevelMultiplier 1;
    maxPreSweeps            4;
    nPostSweeps             2;
    postSweepsLevelMultiplier 1;
    maxPostSweeps           4;
    nFinestSweeps           2;
    interpolateCorrection   no;
    scaleCorrection         yes;  // Yes: symmetric No: Asymmetric
    directSolveCoarsest     no;

<% assert :string_exists, "GAMGSolver.C", "cacheAgglomeration" %>
<% assert :string_exists, "GAMGAgglomeration.C", "nCellsInCoarsestLevel" %>
<% assert :string_exists, "GAMGAgglomeration.C", "processorAgglomerator" %>
<% assert :string_exists, "GAMGSolver.C", "nPreSweeps" %>
<% assert :string_exists, "GAMGSolver.C", "preSweepsLevelMultiplier" %>
<% assert :string_exists, "GAMGSolver.C", "maxPostSweeps" %>
<% assert :string_exists, "GAMGSolver.C", "nFinestSweeps" %>
<% assert :string_exists, "GAMGSolver.C", "interpolateCorrection" %>
<% assert :string_exists, "GAMGSolver.C", "scaleCorrection" %>
<% assert :string_exists, "GAMGSolver.C", "directSolveCoarsest" %>

## Details {#details}

### Operation {#details-operation}

Coarsens using agglomeration

- face area pair (geometric)
- algebraic pair (numerical coefficients)

Agglomeration - number of levels depends on

- number of cells at coarsest level
- hard-limit of a maximum of 50 levels

V-cycle

- optional pre- and post- solve smoothing at each level
- optional smoothing at finest level

Data exchange between levels:

- restriction: fine-to-coarse (summation)
- prolongation: coarse-to-fine (injection)

Coarsest level solved using either:

- direct solver
- [PCG](ref_id://cg-pcg) (symmetric matrices)
- [PBiCGStab](ref_id://cg-pbicgstab) (asymmetric matrices)

The system becomes more difficult to solve with increasing coarse
levels due to the increasing amount of (explicit) processor contributions.
This requires an efficient
[pre-conditioner](<%= ref_id "linearequationsolvers-cg", "preconditioners" %>),
which are generally not parallel-aware
{: .note}

### Computation cost {#details-computational-cost}

Start-up:

- construction of agglomeration levels
- 1 matrix-vector multiply
- 1 parallel reduction

Per iteration:

- 1 matrix-vector multiply per level
- 1 parallel reduction per level
- 1 matrix-vector multiply per V-cycle per iteration (finest level)
- 1 parallel reduction per V-cycle per iteration (finest level)
- and cost of coarsest level solution, e.g. using the [PCG](ref_id://cg-pcg) solver

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/lduMatrix/solvers/GAMG" %>

API:

- [Foam::GAMGSolver](doxy_id://Foam::GAMGSolver)
