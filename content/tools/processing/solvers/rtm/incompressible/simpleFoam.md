---
title: simpleFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- solvers
- incompressible
menu_id: simplefoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-incompressible
group: solvers-incompressible
solver_models:
- turbulence
- fvOption
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-incompressible" %>
- steady state
- incompressible
- <%= link_to_menuid "models-turbulence" %>
- <%= link_to_menuid "fvoptions" %>

## Equations {#equations}

The solver employs the [SIMPLE](ref_id://pv-simple)
algorithm to solve the continuity equation:

$$
    \div \u = 0
$$

and momentum equation:

$$
    \div ( \u \otimes \u ) - \div \vec{R} = - \grad p + \vec{S}_u
$$

Where:

$$\u$$
: Velocity

$$p$$
: Kinematic pressure

$$\vec{R}$$
: Stress tensor

$$\vec{S}_u$$
: Momentum source

## Input requirements {#input-requirements}

Mandatory fields:

- p: <%= link_to_menuid "algorithm-kinematic-pressure" %> \[m2/s2\]
- U: velocity \[m/s\]

### Physical models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence):
  `constant/turbulenceProperties`
- [finite volume options](ref_id://numerics-fvoptions#usage):
  `constant/fvOptions` (optional)

### Solution controls {#input-requirements-solution-controls}

[$FOAM_CASE/system/fvSchemes](ref_id://fvschemes) requirements:

- [time schemes](ref_id://schemes-time-steadystate): `steadyState`
- [gradient schemes](ref_id://schemes-gradient): `grad(U)`, `grad(p)`
- [divergence schemes](ref_id://schemes-divergence): `div(phi,U)`
- note: [Bounded schemes for steady state](ref_id://schemes-divergence#special-cases-steady-state)
- [laplacian schemes](ref_id://schemes-laplacian):
  `laplacian(nuEff,U)`, `laplacian((1|A(U)),p)`

<% assert :text_exists, "simpleFoam/UEqn.H", "fvm::div" %>
<% assert :text_exists, "simpleFoam/pEqn.H", "fvm::laplacian" %>

[$FOAM_CASE/system/fvSolution](ref_id://fvsolution) requirements:

- [solver entries](ref_id://numerics-linearequationsolvers#options) for `U`, `p`

<% assert :string_exists, "simpleFoam/createFields.H", "U" %>
<% assert :string_exists, "simpleFoam/createFields.H", "p" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/incompressible/simpleFoam" %>

See also

- <%= link_to_menuid "pimplefoam" %>
- <%= link_to_menuid "rhosimplefoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam" %>
