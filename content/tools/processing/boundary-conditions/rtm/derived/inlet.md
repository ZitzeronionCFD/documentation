---
title: Inlet conditions
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-inlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

## Boundary mesh type {#boundary-mesh-type}

Inlet conditions are specified using the `patch` type entry in the
`$FOAM_CASE/constant/polyMesh/boundary` file:

    <patchName>
    {
        type            patch;
        ...
    }

## General conditions {#general}

Available inlet conditions include

<%= insert_models "bcs-inlet-general" %>

## Velocity conditions {#velocity}

- fixed velocity: [fixedValue](ref_id://boundary-conditions-fixedGradient)
<%= insert_models "bcs-inlet-velocity" %>

## Pressure conditions {#pressure}

- fixed static pressure: see
  [fixedValue](ref_id://boundary-conditions-fixedGradient)
- flow inlet: see [zeroGradient](ref_id://boundary-conditions-zeroGradient)
<%= insert_models "bcs-inlet-pressure" %>

## Temperature conditions {#temperature}

- fixed temperature: [fixedValue](ref_id://boundary-conditions-fixedGradient)

## Turbulence conditions {#turbulence}

<%= insert_models "bcs-inlet-turbulence-k",
    "bcs-inlet-turbulence-epsilon",
    "bcs-inlet-turbulence-omega" %>

## Further information {#further-information}

Source code:

- [Foam::polyPatch](doxy_id://Foam::polyPatch)
- [Foam::fvPatch](doxy_id://Foam::fvPatch)

See also

- [grpInletBoundaryConditions](doxy_id://grpInletBoundaryConditions)
