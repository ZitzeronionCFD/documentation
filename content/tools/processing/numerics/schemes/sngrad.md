---
title: Surface-normal gradient schemes
short_title: SnGrad
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- gradient
menu_id: schemes-sngrad
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes
---

<%= page_toc %>

The full gradient of a property $$ Q $$ at a face can can be
[interpolated](ref_id://schemes-interpolation) from the
[cell-based gradient](ref_id://schemes-gradient).
The surface-normal contribution is then represented by:

$$
    \snGrad Q
    = \vec{n} \dprod \left( \grad Q \right)_f
$$

where $$ \vec{n} $$ is the face unit normal.  The stencil
for the gradient calculation at the face, $$ f $$, between
cells P and N is described by the following figure:
![Surface-normal gradient schematic](schemes-sngrad-schematic.png)
where the vector $$ \vec{d} $$ joins the two cell centres.
A variety of schemes are available that differ in their
application based on the angle, $$ \theta $$, between the $$ \vec{d} $$
and $$ \vec{n} $$ vectors, representing the degree of
non-orthogonality.

## Usage {#usage}

Surface-normal gradient schemes are specified in the
<%= link_to_menuid "fvschemes" %>file under the `snGradSchemes`
sub-dictionary using the syntax:

    snGradSchemes
    {
        default         none;
        snGrad(Q)       <snGrad scheme>;
    }

## Options {#options}

<%= insert_models "schemes-sngrad" %>

## Example {#example}

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/finiteVolume/snGradSchemes/snGradScheme" %>

API:

- [Foam::fv::snGradScheme](doxy_id://Foam::fv::snGradScheme)
