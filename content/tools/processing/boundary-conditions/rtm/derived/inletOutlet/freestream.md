---
title: freestream
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-freestream
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `freestream` is a generic boundary condition that provides
a free-stream condition. It is a `mixed` condition derived from the
`inletOutlet` condition, whereby the mode of operation switches between
fixed (free stream) value and zero gradient based on the sign of the flux.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            freestream;

        // Conditional entries

            // Option-1
            freestreamValue        <Field<Type>>;

            // Option-2
            freestreamBC           <fvPatchField<Type>>;

        // Optional entries
        phi             <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "freestreamFvPatchField.H", "freestream" %>
<% assert :string_exists, "freestreamFvPatchField.C", "freestreamValue" %>
<% assert :string_exists, "freestreamFvPatchField.C", "freestreamBC" %>
<% assert :string_exists, "freestreamFvPatchField.C", "phi" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `freestream`                    | word    | yes      | -
`freestreamValue` | Patch value                             | Field\<Type\> | choice | -
`freestreamBC` | Separate boundary condition                | fvPatchField\<Type\> | choice | -
`phi`          | Name of flux field                         | word    | no       | phi

The inherited entries are elaborated in:

- [Foam::inletOutletFvPatchField](doxy_id://Foam::inletOutletFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/RAS/aerofoilNACA0012" %>
- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmFlatTerrain" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/freestream" %>

API:

- [Foam::freestreamFvPatchField](doxy_id://Foam::freestreamFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
