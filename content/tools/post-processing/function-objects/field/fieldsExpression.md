---
title: fieldsExpression
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- expression
- field
- function object
- post-processing
menu_id: function-objects-field-fieldsexpression
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `fieldsExpression` class is an intermediate class for handling field
expression function objects (e.g. `add`, `subtract` etc.) whereinto more
than one fields are input.

The class is not an executable function object itself, yet a provider for
common entries to its derived function objects.

## Usage {#usage}

The optional-inherited entries provided by `fieldsExpression` to the derived
function objects are as follows:

    <userDefinedSubDictName1>
    {
        // Mandatory and other optional entries
        ...

        // Optional (inherited) entries (runtime modifiable)
        fields            (<field1> <field2> ... <fieldN>);
        result            <fieldResult>;
    }

<% assert :string_exists, "fieldsExpression.H", "fieldsExpression" %>
<% assert :string_exists, "fieldsExpression.C", "fields" %>
<% assert :string_exists, "fieldsExpression.C", "result" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 fields       | Names of the operand fields         | wordList | yes  | -
 result       | Names of the output fields          | wordList | no   | \<FO\>(\<f1\>,...)

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/fieldsExpression" %>

API:

- [Foam::functionObjects::fieldsExpression](doxy_id://Foam::functionObjects::fieldsExpression)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
