# frozen_string_literal: true

require 'kramdown'

class Kramdown2LatexFilter < Nanoc::Filter
  identifier :kramdown2latex

  def run(content,_params)
    ::Kramdown::Document.new(content, params).to_latex
  end
end
