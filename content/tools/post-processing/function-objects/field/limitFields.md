---
title: limitFields
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- limit
- post-processing
menu_id: function-objects-field-limitfields
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `limitFields` function object
limits input fields to user-specified min and max bounds.

For non-scalar types of input fields, the user limit is used to create a
scaling factor using the field magnitudes.

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `vol<Type>Field`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `vol<Type>Field`     | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `limitFields` function object by using `functions` sub-dictionary
in `system/controlDict` file:

    limitFields1
    {
        // Mandatory entries (unmodifiable)
        type            limitFields;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        fields          (U);
        limit           max;
        max             100;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "limitFields/limitFields.H", "limitFields" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "limitFields",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "limitFields.C", "field" %>
<% assert :string_exists, "limitFields.C", "limit" %>
<% assert :string_exists, "limitFields.C", "max" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: limitFields              | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 fields       | List of fields to process           | wordList | yes  | -
 limit        | Bound to limit - see below          | word |  yes     | -
 min          | Min limit value                     | scalar | conditional | -
 max          | Max limit value                     | scalar | conditional | -

Options for the `limit` entry:

- `min`  : Specify a minimum value
- `max`  : Specify a maximum value
- `both` : Specify a minimum value and a maximum value

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Example by using the `postProcess` utility:

    postProcess -func limitFields

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/limitFields" %>

API:

- [Foam::functionObjects::limitFields](doxy_id://Foam::functionObjects::limitFields)

<%= history "v1912" %>

<!----------------------------------------------------------------------------->
