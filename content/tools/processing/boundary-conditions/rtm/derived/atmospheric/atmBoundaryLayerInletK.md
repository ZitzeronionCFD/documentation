---
title: atmBoundaryLayerInletK
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-atmBoundaryLayerInletK
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

The `atmBoundaryLayerInletK` is a boundary condition that provides
boundary condition for the turbulent kinetic energy (i.e. `k`)
for homogeneous, two-dimensional, dry-air, equilibrium and neutral
atmospheric boundary layer modelling.

The ground-normal `k` profile expression:

$$
    k = \frac{(u^*)^2}{\sqrt{C_\mu}}
    \sqrt{C_1 \ln \left( \frac{z - d + z_0}{z_0} \right) + C_2}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$k$$        | Ground-normal turbulent kinetic energy profile \[m^2/s^3\]
$$u^*$$      | Friction velocity                              \[m/s\]
$$C_\mu$$    | Empirical model constant                       \[-\]
$$C_1$$      | Curve-fitting coefficient for YGCJ profiles \[-\]
$$C_2$$      | Curve-fitting coefficient for YGCJ profiles \[-\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            atmBoundaryLayerInletK;

        // Inherited entries
        ...
        flowDir         (1 0 0);    // not used
        zDir            (0 0 1);    // not used
    }

<% assert :string_exists, "atmBoundaryLayerInletKFvPatchScalarField.H", "atmBoundaryLayerInletK" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `atmBoundaryLayerInletK`        | word    | yes      | -

The inherited entries are elaborated in:

- atmBoundaryLayer.H
- inletOutletFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/turbineSiting" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/atmBoundaryLayerInletK" %>

API:

- [Foam::atmBoundaryLayerInletKFvPatchScalarField](doxy_id://Foam::atmBoundaryLayerInletKFvPatchScalarField)

<%= history "v1806" %>

<!----------------------------------------------------------------------------->
