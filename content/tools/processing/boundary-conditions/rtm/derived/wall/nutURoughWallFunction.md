---
title: nutURoughWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-nutURoughWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `nutURoughWallFunction` is boundary condition that provides
a wall function on the turbulent viscosity (i.e. `nut`) based on velocity
(i.e. `U`) for low- and high-Reynolds number applications for rough walls.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                nutURoughWallFunction;
        roughnessHeight     <scalar>;
        roughnessConstant   <scalar>;
        roughnessFactor     <scalar>;

        // Optional entries
        maxIter             <label>;
        tolerance           <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "nutURoughWallFunctionFvPatchScalarField.H", "nutURoughWallFunction" %>
<% assert :string_exists, "nutURoughWallFunctionFvPatchScalarField.C", "roughnessHeight" %>
<% assert :string_exists, "nutURoughWallFunctionFvPatchScalarField.C", "roughnessConstant" %>
<% assert :string_exists, "nutURoughWallFunctionFvPatchScalarField.C", "roughnessFactor" %>
<% assert :string_exists, "nutURoughWallFunctionFvPatchScalarField.C", "maxIter" %>
<% assert :string_exists, "nutURoughWallFunctionFvPatchScalarField.C", "tolerance" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `nutURoughWallFunction`         | word    | yes      | -
`roughnessHeight`   | Roughness height                      | scalar  | yes      | -
`roughnessConstant` | Roughness constant                    | scalar  | yes      | -
`roughnessFactor`   | Scaling factor                        | scalar  | yes      | -
`maxIter`           | Number of Newton-Raphson iterations   | label   | no       | 10
`tolerance`         | Convergence tolerance                 | scalar  | no       | 0.0001

The inherited entries are elaborated in:

- nutWallFunctionFvPatchScalarField.H
<!-- end of the list -->

- Suffers from non-exact restart since `correctNut()` (called through
  `turbulence->validate`) returns a slightly different value every time
  it is called. See `nutUSpaldingWallFunctionFvPatchScalarField.C`.

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/nutWallFunctions/nutURoughWallFunction" %>

API:

- [Foam::nutURoughWallFunctionFvPatchScalarField](doxy_id://Foam::nutURoughWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
