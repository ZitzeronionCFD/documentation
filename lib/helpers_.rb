# frozen_string_literal: true

use_helper Nanoc::Helpers::LinkTo
use_helper Nanoc::Helpers::Filtering
use_helper Nanoc::Helpers::XMLSitemap
use_helper Nanoc::Helpers::Rendering
use_helper Nanoc::Helpers::Tagging

use_helper Nanoc::Helpers::Breadcrumbs

use_helper Utils::IdTools
use_helper Utils::PageComponents
use_helper OpenFOAM::Utils

def cite(key)
  refs = @config[:cites][key]
  if refs.nil?
    puts "ERROR: fn cite(key) - did not find reference #{key}"
  else
    '\[' + link_to("#{refs[:id]}", "/references/\##{key}") + '\]'
  end
end

def doxy_link(cls, title = nil)
  @config[:doxytags].link(cls, title)
end

def doxy_url(cls)
  @config[:doxytags].url(cls)
end

def cleansed_text(line)
  return '' if line.nil?

  singular_words = %w[
    about addition advantage allow arise
    become belong
    choice comprise
    default detail drop
    enforce equivalent
    follow form further
    give great
    help home
    if ill in it
    lack lead lend
    mean
    need new not
    offer out
    page present problem provide
    require
    see set sometime
    take their thing top
    up usage use user
    way well where while will
  ]

  plural_words = singular_words.map{|s| s + 's'}

  al_base_words = %w[
    addition
  ]

  al_words = al_base_words.map{|s| + 'al'}

  ing_base_words = %w[
    accord allow arise
    become belong
    comprise
    default define detail dropp during
    enforce
    follow form further
    give
    have help
    lack lead lend
    manage mean
    need note
    offer out
    page present provide
    require
    sett specify
    take top
    use
    while widen will
  ]

  ing_words = ing_base_words.map{|s| s[-1] == 'e' ? s.chop + 'ing' : s + 'ing'}

  other_words = %w[
    above additional after all also an and any applicable are around as at available
    be because been between both but by
    can cannot category
    defined during
    each
    familiar few for from
    given good
    has have here home how
    information into is
    large later
    managed many may more most must
    necessary new not
    of on only or
    same several should so some specified such suited
    tba that the their then these they this through thus to typically
    under underlying unfamiliar used usual usually
    via
    was we what when which whilst wide would with within
 ]

 removed_words = singular_words + plural_words + al_base_words + al_words + ing_base_words + ing_words + other_words


  replacement_rules = {
    'Open[∇]*FOAM[:]*' => '',
    ' [a-zA-Z] ' => '',
    ' [0-9] ' => '',
    ' _ ' => '',
    '\r' => ' ',
    '\n' => ' ',
    '\$.*[\s$]' => '',  # $FOAM_TUTORIALS/abc/def/ghi
    '^[\s]*' => ''      # empty spaces at the start
  }
  matcher = /#{replacement_rules.keys.join('|')}/

  line.downcase.gsub(matcher, replacement_rules)

  words = line.downcase.split(/\W+/)
  keywords = words.uniq - removed_words
  keywords.join(' ')
end

def camelWords(word)
  # Split camel case into words, preserve blocks of capital letters
  # taken from https://stackoverflow.com/a/48019684
  words = word \
    .gsub(/([[:lower:]\\d])([[:upper:]])/, '\1 \2') \
    .gsub(/([^-\\d])(\\d[-\\d]*( |$))/,'\1 \2') \
    .gsub(/([[:upper:]])([[:upper:]][[:lower:]\\d])/, '\1 \2')

  # create list of combinations
  all_words = +""
  sum = +""
  words.split.each do |w|
    all_words << " " << w.downcase
    sum << w
    all_words << " " << sum.downcase unless sum == w
  end

  all_words
end

def update_search_data(search_content, rep)
  path = rep.path

  return if path.nil?

  #puts "update_search_data - path: #{path}"

  content = rep.compiled_content(snapshot: :last)

  html = content.encode(
    'UTF-8',
    invalid: :replace,
    undef: :replace,
    replace: '',
    universal_newline: true
  ).gsub(/\P{ASCII}/, '')
  doc = Nokogiri::HTML(html, nil, Encoding::UTF_8.to_s)
  body = doc.at('div#main')

  return if body.nil?

  p1 = body.search('p').to_s.gsub(/\n+/, ' ')[0..200]
  p1 += "..." if !p1.empty?
  summary = Nokogiri::HTML(p1, nil, Encoding::UTF_8.to_s).search('p')
  summary.search('img').each do |img|
    src, alt = %w[src alt].map{ |e| img[e] }
    alt = "(no description)" if alt.nil?
    img.replace("<i>image: #{alt}</i>")
  end

  body.xpath('.//code')&.remove
  body = body.xpath('.//text()').text.strip.gsub(/\s+/, ' ')

  search_content.append({
    title: rep.item[:title] || 'no title',
    filename: File.basename(path),
    body: cleansed_text(body),
    tags: rep.item[:tags]&.join(',') || '',
    id: path,
    summary: summary || '(no summary)'
  })
end

def update_search_source_data(search_content, filename)
  stub = File.basename(filename, ".*")

  #puts "update_search_source_data - path: #{filename}"

  search_content.append({
    title: "Source: #{File.basename(filename)}",
    filename: File.basename(filename),
    body: "source file #{camelWords(stub)}",
    tags: '',
    id: "#{@config[:openfoam][:master_url]}#{filename}",
    summary: "Implementation for $WM_PROJECT_DIR/#{filename}"
  })
end