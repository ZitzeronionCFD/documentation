---
title: pressureNormalInletOutletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-pressureNormalInletOutletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `pressureNormalInletOutletVelocity` is a velocity inlet/outlet boundary
condition that applies a zero-gradient condition for outflow (as defined by
the flux); and obtains velocity from the flux with a direction normal
to the patch faces.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            pressureNormalInletOutletVelocity;

        // Optional entries
        phi             <word>;
        rho             <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "pressureNormalInletOutletVelocityFvPatchVectorField.H", "pressureNormalInletOutletVelocity" %>
<% assert :string_exists, "pressureNormalInletOutletVelocityFvPatchVectorField.C", "phi" %>
<% assert :string_exists, "pressureNormalInletOutletVelocityFvPatchVectorField.C", "rho" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `pressureNormalInletOutletVelocity`   | word    | yes      | -
`phi`          | Name of flux field                         | word    | no       | phi
`rho`          | Name of density field                      | word    | no       | rho

The inherited entries are elaborated in:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
<!-- end of the list -->

- Sign conventions:
  - positive flux (out of domain): apply zero-gradient condition
  - negative flux (into of domain): derive from the flux with specified
    direction
{: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/pressureNormalInletOutletVelocity" %>

API:

- [Foam::pressureNormalInletOutletVelocityFvPatchVectorField](doxy_id://Foam::pressureNormalInletOutletVelocityFvPatchVectorField)

<%= history "1.6" %>

<!----------------------------------------------------------------------------->
