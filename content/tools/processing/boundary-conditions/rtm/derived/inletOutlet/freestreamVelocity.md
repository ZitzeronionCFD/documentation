---
title: freestreamVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-freestreamVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `freestreamVelocity` is a generic boundary condition that provides
a free-stream condition for velocity. It is an inlet-outlet condition that uses
the velocity orientation to continuously blend between fixed value for normal
inlet and zero gradient for normal outlet flow.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            freestreamVelocity;
        freestreamValue <vectorField>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "freestreamVelocityFvPatchVectorField.H", "freestreamVelocity" %>
<% assert :string_exists, "freestreamVelocityFvPatchVectorField.C", "freestreamValue" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `freestreamVelocity`            | word    | yes      | -
`freestreamValue` | Patch value                             | vectorField | yes  | -

The inherited entries are elaborated in:

- [Foam::fvPatchField](doxy_id://Foam::fvPatchField)
- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/RAS/aerofoilNACA0012" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/airFoil2D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/freestreamVelocity" %>

API:

- [Foam::freestreamVelocityFvPatchVectorField](doxy_id://Foam::freestreamVelocityFvPatchVectorField)

<%= history "v1806" %>

<!----------------------------------------------------------------------------->
