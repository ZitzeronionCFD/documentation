---
title: Bump (2D)
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- example
- turbulence
- verification
- validation
menu_id: verification-validation-turbulent-bump-2d
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-turbulent
group: verification-validation-turbulent
---

<%= page_toc %>

## Overview {#sec-overview}

- Solver: [simpleFoam](ref_id://simplefoam)
- 2D flow over a bump at a Reynolds number of $$ 3 \times 10^6 $$ (L=1m)
- Based on the description provided by the
  [NASA Turbulence Modelling Resource](https://turbmodels.larc.nasa.gov/bump.html)
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/bump2D" %>

![Case description](boundary-conditions-small.png)

## Mesh {#sec-mesh}

- 2D structured mesh created using [blockMesh](ref_id://blockmesh)
- bump described

  $$
    y(x) =
    \begin{cases}
    0.05 [\sin(\pi \frac{x}{0.9}-\frac{\pi}{3})]^4, & 0.3 \le x \le 1.2, \\
    0 , & 0 \le x \lt 0.3 \, \text{and} \, 1.2 \lt x \le 1.5.
    \end{cases}
  $$

- this has been set in the `blockMeshDict` using a `codeStream`

<!-- ![Mesh](mesh-600.png) -->

![Close-up around the bump](mesh-bump-600.png)

## Boundary conditions {#sec-boundary-conditions}

- $$ U $$ based on a Mach number of 0.2.  Mach number, $$ Ma $$, is defined
  as:

  $$
      Ma = \sqrt{\gamma R T}
  $$

  where $$ \gamma $$ is the ratio of specific heats, $$ R $$ the gas
  constant and $$ T $$ the temperature.  Using values for air at 300K, the
  inflow velocity is given as:

  $$
      U = 0.2 Ma = 0.2 \sqrt{1.4 \times 287 \times 300} = 69.44 m/s
  $$

- The laminar viscosity is derived from the Reynolds number, i.e.

  $$
      \nu_\infty = \frac{|\u| L}{Re} = \frac{69.44 \times 1}{3 \times 10^6} = 2.31 \times 10^{-5} m^2/s
  $$

### Common fields {#sec-boundary-conditions-turbulence-common}

Velocity: `U`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [fixedValue](ref_id://boundary-conditions-fixedValue)   | 69.44 m/s in x   |
| Outlet  | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Bump    | [noSlip](ref_id://boundary-conditions-noSlip)       |              |
| Walls   | [symmetryPlane](ref_id://boundary-conditions-symmetry-plane)       |              |

Pressure: `p`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Outlet  | [fixedValue](ref_id://boundary-conditions-fixedValue)   | 0 Pa (static)           |
| Bump    | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Walls   | [symmetryPlane](ref_id://boundary-conditions-symmetry-plane) |              |

### Turbulence fields {#sec-boundary-conditions-turbulence}

- turbulence model: [Spalart Allmaras](ref_id://ras-spalartallmaras)

Turbulence viscosity: `nut`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | `calculated`   |              |
| Outlet  | `calculated`   |              |
| Bump    | `nutUSpaldingWallFunction` |  |
| Walls   | `symmetryPlane` |              |

#### Spalart-Allmaras

Modified turbulence viscosity: `nuTilda`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [fixedValue](ref_id://boundary-conditions-fixedValue)   |  based on $$ 3 \nu_\infty$$ |
| Outlet  | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Bump    | [fixedValue](ref_id://boundary-conditions-fixedValue) | 0          |
| Walls   | [symmetryPlane](ref_id://boundary-conditions-symmetry-plane) |           |

## Results {#sec-results}

The [NASA Turbulence Modelling Resource](https://turbmodels.larc.nasa.gov/bump.html)
employs a code comparison to show that the FUN3D and CFL3D codes produce
equivalent results for this case.  OpenFOAM and CFL3D results are presented in
the following series of images, showing that OpenFOAM results compare very
favourably.

| ![Normalised turbulence viscosity](spalart-allmaras-nut-by-nu-400.png) | |
| ![Skin friction](spalart-allmaras-cf-400.png)| ![Pressure coefficient](spalart-allmaras-cp-400.png) |
{: .noBorder}
