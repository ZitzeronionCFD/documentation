---
title: Thermophysical
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- thermophysical
menu_id: models-thermophysical
license: CC-BY-NC-ND-4.0
menu_parent: models
---

<%= page_toc %>

Thermophysical properties are specified in user-selectable *packages* that
describe collections of models, based on either compressibility
($$\psi$$, `psi`) or density ($$\rho$$, `rho`).

The system starts with the [specie](specie)---the most basic entity that
describes information such as the specie name, molecular weight and gas
constant. This is extended by additional models that return thermophysical
properties as functions of pressure and temperature.

Requirements vary according to the
[solver application](ref_id://applications-solvers), e.g. for single phase,
multiphase, compressibility- or density-based, chemical reactions etc.

## Single phase systems {#single-phase-systems}

Thermophysical properties for single phase calculations are specified in the
file:

~~~
$FOAM_CASE/constant/thermophysicalProperties
~~~

The main `thermoType` dictionary entry defines the set of models used to
describe the system, comprising the entries:

~~~
thermoType
{
    type            <thermophysical-model>;
    mixture         <mixture-model>;

    transport       <transport-model>;
    thermo          <thermodynamics-model>;
    equationOfState <equation-of-state-model>;
    specie          <specie-model>;

    energy          <energy-model>;
}
~~~

Here, the `type` entry sets the top-level thermophysical model type, e.g.

- `hePsiThermo` : compressibility-based ([source code](doxy_id://grpPsiThermo))
- `heRhoThermo` : density-based ([source code](doxy_id://grpRhoThermo))
- `heheuPsiThermo` : compressibility-based with extensions for
  mixture is burnt/unburnt systems ([source code](doxy_id://Foam::heheuPsiThermo))
- `heSolidThermo` : solid-based ([source code](doxy_id://Foam::heSolidThermo))

The `he` prefix signifies the energy variable that may be based on either
enthalpy (`h`) or internal energy (`e`).
{: .note}

The `mixture` type depends on the solver application; mixtures that can be
approximated by a single component are defined using the `pureMixture` type:

~~~
mixture         pureMixture;
~~~

All properties are then defined in a `mixture` sub-dictionary taking the form:

~~~
mixture
{
    specie
    {
        ...
    }
    equationOfState
    {
        ...
    }
    thermodynamics
    {
        ...
    }
    transport
    {
        ...
    }
}
~~~

Each of the `mixture` entries are described in the following sections:

- [Equation of state](equation-of-state)
- [Thermodynamics](thermodynamics)
- [Transport](transport)

Available options for the `energy` entry include:

- absoluteEnthalpy
- absoluteInternalEnergy
- sensibleEnthalpy
- sensibleInternalEnergy

### Multi-component mixtures {#multi-component-mixtures}

Both reacting and non-reacting mixtures are supported by some applications.
The required inputs typically follow the pattern described above:

- [Mixture](mixture)

## Multiphase systems {#multiphase-systems}

Each phase is described separately for multiphase systems, using file names
with the convention:

~~~
$FOAM_CASE/constant/thermophysicalProperties.<phase>
~~~

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels" %>
- [Source documentation](doxy_id://grpThermophysicalModels)
