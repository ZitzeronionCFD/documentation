---
title: mappedFixedValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-mappedFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `mappedFixedValue` is a generic boundary condition that maps the value
at a set of cells or patch faces back to the local patch.

The sample mode is set by the underlying mapping engine, provided by the
`mappedPatchBase` class.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            mappedPatch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                    mappedFixedValue;

        // Inherited entries
        ...
    }

<% assert :string_exists, "mappedFixedValueFvPatchField.H", "mappedFixedValue" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `mappedFixedValue`              | word    | yes      | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::mappedFixedValueFvPatchField](doxy_id://Foam::mappedFixedValueFvPatchField)
<!-- end of the list -->

- It is not possible to sample internal faces since volume fields are not
  defined on faces.
  {: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/mappedFixedValue" %>

API:

- [Foam::mappedFixedValueFvPatchField](doxy_id://Foam::mappedFixedValueFvPatchField)

<%= history "2.1.0" %>

<!----------------------------------------------------------------------------->
