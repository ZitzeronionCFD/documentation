---
title: fixedFluxPressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-fixedFluxPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-pressure
---

<%= page_toc %>

## Description

The `fixedFluxPressure` is a pressure boundary condition to
set the pressure gradient to the provided value such that the flux on
the boundary is that specified by the velocity boundary condition.
This condition ensures the correct flux at fixed-flux boundaries by
compensating for the body forces (gravity in particular) with the
pressure gradient.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedFluxPressure;

        // Inherited entries
        ...
    }

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `fixedFluxPressure`             | word    | yes      | -

<% assert :string_exists, "fixedFluxPressureFvPatchScalarField.H", "fixedFluxPressure" %>

The inherited entries are elaborated in:

- [Foam::fixedGradientFvPatchField](doxy_id://Foam::fixedGradientFvPatchField)
- [Foam::updateableSnGrad](doxy_id://Foam::updateableSnGrad)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantBoussinesqPimpleFoam/hotRoom" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fixedFluxPressure" %>

API:

- [Foam::fixedFluxPressureFvPatchScalarField](doxy_id://Foam::fixedFluxPressureFvPatchScalarField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
