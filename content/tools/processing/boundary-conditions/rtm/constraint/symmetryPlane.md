---
title: Symmetry plane
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
- constraint
menu_id: boundary-conditions-symmetry-plane
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-constraint
group: boundary-conditions-constraint
---

<%= page_toc %>

## Properties {#properties}

Applied to planar patches to represent a symmetry condition.

## Usage {#usage}

The condition requires entries in both the `boundary` and field files. Mesh
`boundary` file:

    <patchName>
    {
        type            symmetryPlane;
        ...
    }

Field file:

    <patchName>
    {
        type            symmetryPlane;
    }

## Further information {#further-information}

Source code:

- [Foam::symmetryPlaneFvPatchField](doxy_id://Foam::symmetryPlaneFvPatchField)
