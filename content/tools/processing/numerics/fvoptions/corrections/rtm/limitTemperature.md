---
title: Limit temperature
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- correction
- temperature
menu_id: fvoption-limittemperature
license: CC-BY-NC-ND-4.0
menu_parent: numerics-fvoptions-corrections
group: fvoptions-corrections-energy
---

<%= page_toc %>

## Properties {#properties}

- limits the temperature to user supplied minimum and maximum values

## Usage {#usage}

    limitT
    {
        type            limitTemperature;
        active          yes;

        selectionMode   all;
        min             200;
        max             500;
        phase           gas; // optional
    }

<% assert :string_exists, "limitTemperature.H", "limitTemperature" %>
<% assert :string_exists, "limitTemperature.C", "min" %>
<% assert :string_exists, "limitTemperature.C", "max" %>
<% assert :string_exists, "limitTemperature.C", "phase" %>

## Further information {#further-information}

Tutorials

- *none*

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/corrections/limitTemperature" %>
