---
title: fanPressure
copyright:
- Copyright (C) 2017-2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-fanPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inlet-pressure
- bcs-outlet-pressure
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `fanPressure` is a pressure boundary condition to assign either a pressure
inlet or outlet total pressure condition for a fan. The condition sets the
static pressure from a definition of the total pressure and pressure drop as a
function of volumetric flow rate across the patch.

The non-dimensional flux is calculated as follows:

$$
    phi = \frac{4 \dot{m}}{\rho \pi^2 d_m^3 \omega}
$$

The non-dimensional flux is calculated as follows:

$$
    \Psi = \frac{2 \Delta P}{\rho (\pi \omega d_m)}
$$

Property         | Description
-----------------|-------
$$d_m$$          | Fan mean diameter \[m\]
$$\Delta P$$     | Pressure drop
$$\omega$$       | Rotational speed  \[rad/s\]
$$\dot{m}$$      | Mass flow rate

The non-dimensional table should be given as $$\Psi = F(\phi)$$.

The condition sets the static pressure at the patch $$ p_p $$ based on a
specification of the total pressure, $$ p_0 $$, and pressure drop,
$$ p_d $$, specified as a function of the volumetric flow rate.

$$
    p_p = p_0 - p_d
$$

## Usage {#usage}

The condition requires entries in both the `boundary` and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fanPressure;
        fanCurve        <Function1<scalar>>;
        direction       <word>;

        // Optional entries
        nonDimensional  <bool>;

        // Conditional entries

          // if 'nonDimensional' is true
          rpm           <Function1<scalar>>;
          dm            <Function1<scalar>>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "fanPressureFvPatchScalarField.H", "fanPressure" %>
<% assert :string_exists, "fanPressureFvPatchScalarField.C", "fanCurve" %>
<% assert :string_exists, "fanPressureFvPatchScalarField.C", "direction" %>
<% assert :string_exists, "fanPressureFvPatchScalarField.C", "nonDimensional" %>
<% assert :string_exists, "fanPressureFvPatchScalarField.C", "rpm" %>
<% assert :string_exists, "fanPressureFvPatchScalarField.C", "dm" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `fan`                           | word    | yes      | -
`fanCurve`     | Dataset of pressure vs flow rate           | Function1\<scalar\> | yes     | -
`direction`    | Direction of flow through fan              | word    | yes      | -
`nonDimensional`  | Flag to use non-dimensional curves      | bool    | no       | false
`rpm`          | Fan rpm (for non-dimensional curve)        | Function1\<scalar\> | conditional | -
`dm`        | Fan mean diameter (for non-dimensional curve) | Function1\<scalar\> | conditional | -

Options for the `direction` entry:

Property    | Description
------------|-------------
`in`        | Into the fan
`out`       | Out of the fan

The inherited entries are elaborated in:

- [Foam::totalPressureFvPatchScalarField](doxy_id://Foam::totalPressureFvPatchScalarField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

- For compatibility with older versions (OpenFOAM-v2006 and earlier),
a missing `fanCurve` keyword is treated as a `tableFile` and makes
the `file` keyword mandatory.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/TJunctionFan" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fanPressure" %>

API:

- [Foam::fanPressureFvPatchScalarField](doxy_id://Foam::fanPressureFvPatchScalarField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
