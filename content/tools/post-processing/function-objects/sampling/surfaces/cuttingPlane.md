---
title: cuttingPlane
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- post-processing
- sampling
- surface
menu_id: surfaces-cuttingPlane
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-sampling-surfaces
group: sampling-surfaces
---

<%= page_toc %>

Creates a cutting plane through a mesh using a point on the plane and plane normal.

## Usage {#usage}

Basic operation of the `cuttingPlane` surface comprises:

    surfaces1
    {
        type            surfaces;
        libs            (sampling);

        surfaces
        {
            mySurface1
            {
                type        cuttingPlane;
                point       <point>;
                normal      <vector>;
            }
        }
    }

<% assert :string_exists, "sampledSurfaces.H", "surfaces" %>
<% assert :class_library_exists, "sampling/Make/files", "sampledSurfaces", "libsampling" %>
<% assert :string_exists, "cuttingPlane.H", "cuttingPlane" %>
<% assert :string_exists, "cuttingPlane.C", "point" %>
<% assert :string_exists, "cuttingPlane.C", "normal" %>

## Sample output {#sample-output}

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/sampling/surface/cutting" %>

API:

- [Foam::cuttingPlane](doxy_id://Foam::cuttingPlane)

Example usage:

- <%= repo_link2("$FOAM_TUTORIALS/compressible/rhoCentralFoam/movingCone") %>
- <%= repo_link2("$FOAM_TUTORIALS/compressible/rhoPorousSimpleFoam/angledDuct/implicit") %>
- <%= repo_link2("$FOAM_TUTORIALS/heatTransfer/solidFoam/movingCone") %>
- <%= repo_link2("$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/propeller") %>
- <%= repo_link2("$FOAM_TUTORIALS/incompressible/simpleFoam/motorBike") %>
