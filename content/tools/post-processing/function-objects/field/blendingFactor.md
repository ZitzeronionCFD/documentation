---
title: blendingFactor
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- blending
- divergence
- field
- function object
- post-processing
menu_id: function-objects-field-blendingfactor
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `blendingFactor` function object computes and stores the blending
coefficient employed by **blended**
[divergence schemes](ref_id://schemes-divergence), giving an indicator as to
which of the schemes is active across the domain.

Blended schemes combine contributions from two schemes, i.e. $$\phi_1$$
and $$\phi_2$$, using a weight field, i.e. $$w$$, such that the
effective scheme value, i.e. $$\phi_\eff$$, is computed as follows:

$$
    \phi_\eff = w \phi_1 + (1 - w) \phi_2
$$

It follows that when $$w = 1$$, the scheme reduces to
$$\phi_\eff = \phi_1$$; similarly, when $$w = 0$$, the
scheme reduces to $$\phi_\eff = \phi_2$$.

The weight field, i.e. $$w$$, is surface field and converted to a volume
field for easier post-processing by setting the cell value to one minus
the minimum of the face values:

$$
    w_c = 1 - \min_{\forall i \in \mathrm{faces}}\left(w_{f,i}\right)
$$

This conversion leads to blending indicator field whose values mean:

    0   =  scheme 0
    1   =  scheme 1
    0-1 =  a blend between scheme 0 and scheme 1

### Operands

Operand      | Type                          | Location
:---         |:---                           |:---
input        | -                             | -
output file  | dat            | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
output field | volScalarField                | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `blendingFactor` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    blendingFactor1
    {
        // Mandatory entries (unmodifiable)
        type            blendingFactor;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entry (runtime modifiable)
        field           <field>;

        // Optional entries (runtime modifiable)
        phi             phi;
        tolerance       0.001;

        // Optional (inherited) entries
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "blendingFactor.H", "blendingFactor" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "blendingFactor",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "blendingFactor.C", "phi" %>
<% assert :string_exists, "blendingFactor.C", "tolerance" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: blendingFactor           | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 field        | Name of the operand field           | word | yes      | -
 phi          | Name of flux field                  | word | no       | phi
 tolerance    | Tolerance for number of blended cells | scalar | no   | 0.001

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)
- [writeFile](ref_id://function-objects-field-writefile)

Usage by the `postProcess` utility is not available.

### Notes on entries

The `field` and `phi` entries are used to construct the scheme name to lookup
from the [fvSchemes](ref_id://fvschemes) dictionary, as:

    div(phi,U)

The `tolerance` entry is used as a threshold when computing the number of cells
identified as using each scheme.
The `tolerance` entry must be in the range 0 to 1, excluding
the endpoints.

## Stored properties  {#stored-properties}

### Fields

The blending factor is stored on the mesh database, using the default name:

    blendingFactor(<field>)

The name can be overridden by using the `result` entry.

## Further information {#further-information}

Related:

- [Courant number blended scheme](ref_id://schemes-divergence-coblended)

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/blendingFactor" %>

API:

- [Foam::functionObjects::blendingFactor](doxy_id://Foam::functionObjects::blendingFactor)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
