---
title: fixedJump
copyright:
- Copyright (C) 2016-2023 OpenCFD Ltd.
tags:
- boundary conditions
- coupled
menu_id: boundary-conditions-fixedJump
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-coupled
group: bcs-coupled-jump
---

<%= page_toc %>

## Description

The `fixedJump` is a general coupled boundary condition to provide a jump cyclic
condition. The jump is specified as a fixed value field, applied as an offset
to the 'owner' patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedJump;
        jump            <Field<Type>>;

        // Optional entries
        jump0           <Field<Type>>;
        minJump         <Type>;
        relax           <scalar>;

        // Inherited entries
        patchType       <word>;
        ...
    }

<% assert :string_exists, "fixedJumpFvPatchField.H", "jump" %>
<% assert :string_exists, "fixedJumpFvPatchField.H", "jump0" %>
<% assert :string_exists, "fixedJumpFvPatchField.H", "minJump" %>
<% assert :string_exists, "fixedJumpFvPatchField.H", "relax" %>

where:

Property | Description                  | Type    | Required | Default
---------|------------------------------|---------|----------|---------
`type`   | Type name: `fixedJump`       | word    | yes      | -
`jump`   | Jump field                   | Field\<Type\> | yes  | -
`jump0`  | Old-time level jump field    | Field\<Type\> | no   | -
`minJump`| Minimum allowable jump value | Type    | no       | pTraits\<Type\>::min
`relax`  | Under-relaxation factor      | scalar  | no       | -1

The inherited entries are elaborated in:

- [Foam::jumpCyclicFvPatchField](doxy_id://Foam::jumpCyclicFvPatchField)
<!-- end of the list -->

- The underlying `patchType` should be set to `cyclic`.
{: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/fixedJump" %>

API:

- [Foam::fixedJumpFvPatchField](doxy_id://Foam::fixedJumpFvPatchField)

<%= history "2.0.1" %>

<!----------------------------------------------------------------------------->
