---
title: Inter-region heat transfer
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- energy
- fvOption
- heat transfer
- multi-region
- source
menu_id: fvoption-interregionheattransfer
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-energy-multi-region
---

<%= page_toc %>

## Properties {#properties}

- Inter-region

## Usage {#usage}

## Further information {#further-information}

Tutorials

- *none*

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/interRegion/interRegionHeatTransfer" %>
