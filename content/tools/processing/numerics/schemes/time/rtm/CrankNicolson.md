---
title: Crank-Nicolson time scheme
short_title: Crank-Nicolson
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- time
menu_id: schemes-time-cranknicolson
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes-time
group: schemes-time
---

<%= page_toc %>

## Properties {#properties}

- Second order
- Transient
- Bounded
- When using uniform time steps the scheme equates to:

$$
    \ddt{\phi} = \frac{\phi - \oldold{\phi}}{2 \Delta t}
$$

## Usage {#usage}

The scheme is specified using:

    ddtSchemes
    {
        default         CrankNicolson <coeff>
        ddt(phi)        CrankNicolson <coeff>;
    }

The coefficient provides a blending between Euler and Crank-Nicolson schemes:

- 0: Euler
- 1: Crank-Nicolson
- A value of 0.9 is a good compromise between accuracy and robustness

## Further information {#further-information}

Source code

- [Foam::fv::CrankNicolsonDdtScheme](doxy_id://Foam::fv::CrankNicolsonDdtScheme)

Reference

- *Crank and Nicolson* <%= cite "crank_practical_1947" %>
