---
title: Mixed
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-mixed
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Properties {#properties}

- Robin condition
- a linear blend of [fixedValue](ref_id://boundary-conditions-fixedGradient)
  and [gradient](ref_id://boundary-conditions-fixedValue) conditions
- blending specified using a *value fraction*
- not usually applied directly, but used in derived types, e.g. the
  [inletOutlet](ref_id://boundary-conditions-inletOutlet) condition
- explicit and implicit contributions

Face values are evaluated according to:
$$
   \phi_f =
        w \phi_{\ref}
      + \left(1-w\right)\left(\phi_c + \Delta \grad{\phi}_{\ref} \right)
$$

where
$$\phi_f$$
: face value

$$\phi_c$$
: cell value

$$\phi_{\ref}$$
: reference value

$$\Delta$$
: face-to-cell distance

$$w$$
: value fraction


## Usage {#usage}

    <patchName>
    {
        type            mixed;
        refValue        <field value>;
        refGradient     <field value>;
        valueFraction   <field value>;
    }

## Further information {#further-information}

Source code:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
