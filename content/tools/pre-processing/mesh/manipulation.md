---
title: Mesh manipulation
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- manipulation
- meshing
menu_id: meshing-manipulation
license: CC-BY-NC-ND-4.0
menu_parent: pre-processing-mesh
---

<%= page_toc %>

The following tools are useful when manipulating the mesh, e.g. scaling the
geometry, identifying patches and creating sets and zones for physical models
and post-processing.

<%= insert_models "meshing-manipulation" %>
