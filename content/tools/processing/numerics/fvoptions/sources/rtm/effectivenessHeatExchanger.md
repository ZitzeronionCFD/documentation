---
title: Heat exchanger
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- energy
- fvOption
- source
menu_id: fvoption-heatexchanger
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-energy
---

<%= page_toc %>

## Properties {#properties}

The heat exchanger is modelled as a volumetric energy source where the energy
transfer between two fluid streams is based on an effectiveness, specified as
a function of the mass flow rates.

$$
    Q_t = e \left(\dot{m}_1 , \dot{m}_2 \right) \left(T_2 - T_1\right) \dot{m}_1 C_p,
$$

Where:

$$Q_t$$
: total heat source

$$e(\dot{m}_1,\dot{m}_2)$$
: effectiveness table

$$\dot{m}_1$$
: net mass flux entering heat exchanger \[kg/s\]

$$\dot{m}_2$$
: secondary mass flow rate \[kg/s\]

$$T_1$$
: primary inlet temperature \[K\]

$$T_2$$
: secondary inlet temperature \[K\]

$$C_p$$
: specific heat capacity \[J/kg/K\]

The energy source is distributed across the cells according to:

$$
    Q_c = \frac{V_c |U_c| (T_c - T_{\ref})}{\sum(V_c |U_c| (T_c - T_{\ref}))},
$$

Where:

$$Q_c$$
: energy source for cell

$$V_c$$
: volume of the cell \[m3\]

$$U_c$$
: local cell velocity \[m/s\]

$$T_c$$
: local call temperature \[K\]

$$T_{\ref}$$
: min or max(T) in cell zone depending on the sign of $$ Q_t $$ \[K\]

Optional entries:

- `primaryInletT` : sets the primary inlet temperature.  If not set, the flux-averaged temperature is used
- `targetQdot` : the secondary inlet temperature is derived from a target heat rejection

## Usage {#usage}

The option is specified using:

    heatExchanger1
    {
        type            effectivenessHeatExchanger;
        selectionMode   cellZone;
        cellZone        porosity;

        secondaryMassFlowRate   1.0;
        secondaryInletT 336;
        faceZone        facesZoneInletOriented;
        outOfBounds     clamp;
        file            "effTable";

        // Optional
        // primaryInletT 293;
        // targetQdot   1500;
    }

<% assert :string_exists, "effectivenessHeatExchangerSource.H", "effectivenessHeatExchanger" %>

The two-dimensional effectiveness table is specified according to, e.g. for
the table

|        | mfr2,1 | mfr2,2 | mfr2,3 |
|--------|--------|--------|--------|
| mfr1,1 |      A |      B |      C |
| mfr1,2 |      D |      E |      F |
| mfr1,3 |      G |      H |      I |

the equivalent entry becomes:

    (
        (
            mfr1,1
            (
                (mfr2,1    A)
                (mfr2,2    B)
                (mfr2,3    C)
            )
        )
        (
            mfr1,2
            (
                (mfr2,1    D)
                (mfr2,2    E)
                (mfr2,3    F)
            )
        )
        (
            mfr1,3
            (
                (mfr2,1    G)
                (mfr2,2    H)
                (mfr2,3    I)
            )
        )
    );

## Further information {#further-information}

Tutorials

- *none*

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/derived/heatExchangerSource" %>

<%= history "v3.0+",
    [
        {"v2212"=>"heatExchangerSource"},
        {"v3.0+"=>"effectivenessHeatExchangerSource"}
    ]
%>
