---
title: Processor
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
- constraint
menu_id: boundary-conditions-processor
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-constraint
group: boundary-conditions-constraint
---

<%= page_toc %>

## Properties {#properties}

Applied in pairs to patches that communicate between processors.  Usually these
patches are not created manually, but are generated when decomposing the
mesh for parallel cases.

## Usage {#usage}

The condition requires entries in both the `boundary` and field files. Mesh
`boundary` file:

    <patchName>
    {
        type            processor;
        ...
    }

Field file:

    <patchName>
    {
        type            processor;
    }

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/constraint/processor" %>

API:

- [Foam::processorFvPatchField](doxy_id://Foam::processorFvPatchField)
