---
title: Contributors
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- contribute
- documentation
menu_id: contributing-contributors
license: CC-BY-NC-ND-4.0
menu_parent: contributing
---

This documentation project was created by the OpenFOAM team in 2016 at
[ESI-OpenCFD Ltd.](https://www.openfoam.com), initially released in
read-only form under the name 'Extended Code Guide'.

Since then it has grown as a result of significant internal investment, and
funding from ESI-OpenCFD Customers.

In 2022-23 the content was migrated to the [nanoc](https://nanoc.app/)
framework and made freely available under a creative commons license at
https://gitlab.com/openfoam/documentation/

ESI-OpenCFD contributors:

- Andrew Heather (Project lead)
- Mattijs Janssens
- Mark Olesen
- Kutalmış Berçin
- Matej Forman
- Prashant Sonakar
- Pawan Ghildiyal
- Chiara Pesci
- Fred Mendonça
- Swapnil Salokhe
- Jiri Polansky

With grateful contributions from the OpenFOAM Community

- <%= @config[:authors].join("\n- ") %>
