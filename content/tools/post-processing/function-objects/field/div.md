---
title: div
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- divergence
- field
- function object
- post-processing
menu_id: function-objects-field-div
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `div` function object computes the divergence of an input field.

To execute `div` function object on an input `<field>`, a numerical scheme
should be defined for `div(<field>)` in `system/fvSchemes.divSchemes`.

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | {surfaceScalar,volVector}Field | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | volScalarField       | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `div` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    div1
    {
        // Mandatory entries (unmodifiable)
        type            div;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entry (runtime modifiable)
        field           <field>;

        // Optional (inherited) entries
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "div/div.H", "div" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "div",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: div                      | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 field        | Name of the operand field           | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func "div(<field>)"

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/div" %>

API:

- [Foam::functionObjects::div](doxy_id://Foam::functionObjects::div)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
