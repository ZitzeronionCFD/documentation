---
title: prghPressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-prghPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `prghPressure` is a boundary condition that provides static pressure
condition for `p_rgh`, calculated as:

$$
    p_{rgh} = p - \rho g (h - h_{ref})
$$

where:

Property             | Description
---------------------|-------
$$p_{rgh}$$          | Pseudo hydrostatic pressure \[Pa\]
$$p$$                | Static pressure \[Pa\]
$$h$$                | Height in the opposite direction to gravity
$$h_{ref}$$          | Reference height in the opposite direction to gravity
$$\rho$$             | Density
$$g$$                | Acceleration due to gravity \[m/s^2\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            prghPressure;
        p               <scalarField>;

        // Optional entries
        rho             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "prghPressureFvPatchScalarField.H", "prghPressure" %>
<% assert :string_exists, "prghPressureFvPatchScalarField.C", "p" %>
<% assert :string_exists, "prghPressureFvPatchScalarField.C", "rho" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `prghPressure` | word    | yes      | -
`p`                | Static pressure           | scalarField | yes  | -
`rho`              | Name of density field     | word    | no       | rho

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/reactingMultiphaseEulerFoam/RAS/wallBoiling1D_2phase" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/prghPressure" %>

API:

- [Foam::prghPressureFvPatchScalarField](doxy_id://Foam::prghPressureFvPatchScalarField)

<%= history "2.3.0" %>

<!----------------------------------------------------------------------------->
