---
title: noise
tags:
- acoustics
- fft
- noise
copyright:
- Copyright 2022 (C) OpenCFD Ltd.
menu_id: noise
license: CC-BY-NC-ND-4.0
menu_parent: post-processing-utilities
group: utilities-post-processing
---

<%= page_toc %>

The `noise` utility requires the [FFTW][FFTW_url] library to compute
Discrete Fourier Transforms (DFTs). Building of the `noise` utility is
skipped if the [FFTW][FFTW_url] library was not found during OpenFOAM
installation.
{: .note}

## Overview {#overview}

The `noise` utility processes [point](pointNoise) and [surface](surfaceNoise)
pressure data to generate:

- Root Mean Square (RMS) pressure \[Pa\];
- Sound Pressure Level (SPL) \[dB\];
- Power Spectral Density (PSD) in \[Pa$$^2$$/Hz\] and/or \[dB/Hz\]; and
- 1/3 Octave SPL data, whereby the bins are constrained to pass through the
  centre frequency at 1 kHz.

Input data can be windowed using a run-time selectable model, where options
include:

- Uniform;
- Hanning, with options including symmetry, and extended (non-zero at the
  end-points); and
- window overlap.

## Usage {#noise}

Command line usage:

<%= help_util_usage "noise" %>

The `noise` utility is configured using a `<case>/system/noiseDict` taking the
general form:

~~~
noiseModel      <modelType>

<modelType>Coeffs
{
    // Model-specific entries (see below)
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    ...

    // Common entries
    // ~~~~~~~~~~~~~~

    windowModel     Hanning;

    HanningCoeffs
    {
        // Window overlap percentage
        overlapPercent  50;
        symmetric       yes;
        extended        yes;

        // Optional number of windows, default = all available
        nWindow         5;
    }

    // Input files list
    files       ("postProcessing/faceSource1/surface/patch/patch.case";)


    // Optional entries
    // ~~~~~~~~~~~~~~~~

    // Reference density (for incompressible cases); default = 1
    rhoRef      1;

    // Number of samples in sampling window; default = 2^16 (=65536)
    N               4096;

    // Lower frequency bound; default = 25 Hz
    fl              25;

    // Upper frequency bound; default = 10000 Hz
    fu              10000;

    // Start time
    startTime       1;

    // Minimum pressure
    minPressure     0;

    // Maximum pressure
    maxPressure     100000;

    // SPL weighting; default = none
    SPLweighting    dBA;

    // Reference for dB calculation; default = 2e-5
    dBRef           2e-5;

    // Output prefix; default = ""
    outputPrefix    "source1";

    // Write options
    writeOptions
    {
        writePrmsf      yes;
        writeSPL        yes;
        writePSD        yes;
        writePSDf       yes;
        writeOctaves    yes;
    }
}
~~~

## SPL Weighting

SPL data can be weighted using the `SPLweighting` entry, where options include:

- `dB` : no weighting
- `dBA` : [A weighting](#A-weighting)
- `dBB` : [B weighting](#B-weighting)
- `dBC` : [C weighting](#C-weighting)
- `dBD` : [D weighting](#D-weighting)

### A weighting {#A-weighting}

~~~
SPLweighting    dBA;
~~~

The gain is computed as:

$$
A(f) = 20\log_{10}(R_A(f)) - 20 \log_{10}(R_A(1000)),
$$

where

$$
R_A(f) = \frac{12194^2 f^4}{(f^2 + 20.6^2)\sqrt{(f^2 + 107.7^2)(f^2 + 739.9^2)}(f^2 + 12194^2)}.
$$

### B weighting {#B-weighting}

~~~
SPLweighting    dBB;
~~~

The gain is computed as:

$$
B(f) = 20\log_{10}(R_B(f)) - 20 \log_{10}(R_B(1000)),
$$

where

$$
R_B(f) = \frac{12194^2 f^3}{(f^2 + 20.6^2)\sqrt{(f^2 + 158.5^2)}(f^2 + 12194^2)}.
$$

### C weighting {#C-weighting}

~~~
SPLweighting    dBC;
~~~

The gain is computed as:

$$
C(f) = 20\log_{10}(R_C(f)) - 20 \log_{10}(R_C(1000)),
$$

where

$$
R_C(f) = \frac{12194^2 f^2}{(f^2 + 20.6^2)(f^2 + 12194^2)}
$$

### D weighting {#D-weighting}

~~~
SPLweighting    dBD;
~~~

The gain is computed as:

$$
D(f) = 20\log_{10}(R_D(f)),
$$

where

$$
R_D(f) = \frac{f}{6.8966888496476e-5}\sqrt{\frac{h(f)}{(f^2 + 79919.29)(f^2 + 1345600)}},
$$

and

$$
h(f) = \frac{(1037918.48 - f^2)^2 + 1080768.16 f^2}{(9837328 - f^2)^2 + 11723776 f^2}.
$$

## Output

All possible data is written by default. The `writeOptions` sub dictionary can
contains a list of boolean entries to select results individually.

~~~
// Write options
writeOptions
{
    writePrmsf      yes;
    writeSPL        yes;
    writePSD        yes;
    writePSDf       yes;
    writeOctaves    yes;
}
~~~

The plain text output files created by the [pointNoise](/.pointNoise) model and
the [surfaceNoise](./surfaceNoise) model's surface averaged data include a
header section that provides case information, e.g. for the average surface RMS
pressure:

~~~
# f [Hz] vs P(f) [Pa]
# Lower frequency: 2.50000000e+01
# Upper frequency: 5.00000000e+01
# Window model  : Hanning
# Window number : 7
# Window samples: 128
# Window overlap %: 5.00000000e+01
# dBRef         : 2.00000000e-05
# Area average  : true
# Area sum      : 8.99999982e-02
# Number of faces: 400
# f                 P(f)
~~~

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_UTILITIES/postProcessing/noise" %>

See also

- [pointNoise](./pointNoise)
- [surfaceNoise](./surfaceNoise)
- [Curle](ref_id://function-objects-field-curle) function object

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/vortexShed" %>

[FFTW_url]: https://www.fftw.org
