---
title: Function objects
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
menu_id: post-processing-function-objects
license: CC-BY-NC-ND-4.0
menu_parent: post-processing
---

<%= page_toc %>

Function objects are OpenFOAM utilities, designed to ease setups and
enhance workflows by generating user-requested data both during
runtime and post-processing, typically in the form of logging to the screen,
or text, image and field files.

Function objects eliminate the need to store all runtime generated data,
hence saving considerable resources. Furthermore, function objects are
readily applied to batch-driven processes, improving reliability by
standardising the sequence of operations and reducing the amount of manual
interaction.

The output of most function objects, e.g. output fields, are
stored on the mesh database to enable retrieval and chaining to other
function objects and applications, e.g. using
[wallShearStress](ref_id://function-objects-field-wallshearstress) function
object output in a [fieldAverage](ref_id://function-objects-field-fieldaverage)
function object to produce the `wallShearStressMean` field.

## Usage {#usage}

Function objects can be executed using two methods:

- at run-time: via the `functions` sub-dictionary in the
  [controlDict](ref_id://controldict) file;
- post-processing on time directories:
  - [postProcess](ref_id://postProcess) command-line utility, and
  - `<application> -postProcess` option available to
    [solver applications](ref_id://applications-solvers).

### Run-time

Each function object should be listed inside the `functions` sub-dictionary of
the [controlDict](ref_id://controldict) file as shown in the following example:

    functions    // sub-dictionary name under the system/controlDict file
    {
        <userDefinedSubDictName1>
        {
            // Mandatory entries
            type                <functionObjectTypeName>;
            libs                (<libType>FunctionObjects);

            // Mandatory entries defined in <functionObjectType>
            ...

            // Optional entries defined in <functionObjectType>
            ...

            // Optional (inherited) entries
            region              region0;
            enabled             true;
            log                 true;
            timeStart           0;
            timeEnd             1000;
            executeControl      timeStep;
            executeInterval     1;
            writeControl        timeStep;
            writeInterval       1;
        }

        <userDefinedSubDictName2>
        {
            ...
        }

        ...

        <userDefinedSubDictNameN>
        {
            ...
        }
    }

where the entries are described below:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name of function object        | word |  yes     | -
 libs         | Library name containing implementation | word |  yes  | -
 region       | Name of region for multi-region cases  | word | no    | region0
 enabled      | Switch to turn function object on/off  | bool | no    | true
 log          | Switch to write log info to standard output | bool | no | true
 timeStart    | Start time for function object execution | scalar  | no | 0
 timeEnd      | End time for function object execution   | scalar  | no | inf
 executeControl  | See time controls below            | word | no   | timeStep
 executeInterval | Steps/time between execute phases  | label | no  | 1
 writeControl    | See time controls below            | word | no    | timeStep
 writeInterval   | Steps/time between write phases    | label | no  | 1

The sub-dictionary name `<userDefinedSubDictName>` is chosen by the user,
and is typically used as the name of the output directory for any data
written by the function object.

As the base mandatory entry, the `type` entry defines the type of
function object properties that follow.  Function objects are packaged into
separate libraries for flexibility and the `libs` entry is used to specify
which library should be loaded.

### Function object controls {#usage-function-object-controls}

When applied at run-time, the objects are controlled according to the optional
two time-based entries:

- `executeControl`: when the object is updated (for updating calculations
  or for management tasks),
- `writeControl`: when the object output is written (for writing the calculated
  data to disk).

<% assert :string_exists, "timeControl.C", "Control" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

If neither entries are present the object will execute and write every time
step---which can create much more data than intended!
{: .note}

Values for these controls take the same type of values as the `writeControl`
entry in the [controlDict](ref_id://controldict), with some additions:

Option            | Description
------------------|-------------------------------------------------------------
none              | Trigger is disabled
timeStep          | Trigger every 'Interval' time-steps, e.g. every x time steps
writeTime | Trigger every 'Interval' output times, i.e. alongside standard field output
runTime  | Trigger every 'Interval' run time period, e.g. every x seconds of calculation time
adjustableRunTime | Currently identical to "runTime"
clockTime         | Trigger every 'Interval' clock time period
cpuTime           | Trigger every 'Interval' CPU time period
onEnd             | Trigger on end of simulation run

<% assert :string_exists, "timeControl.C", "timeStep" %>
<% assert :string_exists, "timeControl.C", "writeTime" %>
<% assert :string_exists, "timeControl.C", "runTime" %>
<% assert :string_exists, "timeControl.C", "adjustableRunTime" %>
<% assert :string_exists, "timeControl.C", "clockTime" %>
<% assert :string_exists, "timeControl.C", "cpuTime" %>
<% assert :string_exists, "timeControl.C", "onEnd" %>

For example, to set a
[Courant Number function object](ref_id://function-objects-field-courantno)
to evaluate every 2 time steps and write at every output time:

    functions
    {
        Co1
        {
            type                CourantNo;
            libs                (fieldFunctionObjects);
            executeControl      timeStep;
            executeInterval     2;
            writeControl        writeTime;
        }
    }

<% assert :string_exists, "CourantNo/CourantNo.H", "CourantNo" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "CourantNo",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "timeControl.C", "Control" %>
<% assert :string_exists, "timeControl.C", "Interval" %>
<% assert :string_exists, "timeControl.C", "timeStep" %>
<% assert :string_exists, "timeControl.C", "writeTime" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

Data derived from function objects are written to the case `postProcessing`
directory or to the time directories, typically in a subdirectory with the same
name as the object, e.g.
<% assert :string_exists, "functionObject.C", "postProcessing" %>

    $FOAM_CASE/postProcessing/<functionObjectName>/<time>/<data>

    $FOAM_CASE/<time>/<outField>

Many function objects create fields that can be stored for later use. To avoid
potential name clashes in the database, the `useNamePrefix` entry can be used to
prepend the name of the function object to the name of the output field,
e.g. applied to a [fieldAverage](ref_id://function-objects-field-fieldaverage)
function object, a velocity mean field could be named `fieldAverage1:UMean`.
{: .note}
<% assert :string_exists, "functionObject.C", "useNamePrefix" %>

### Post-processing on time directories

#### Post-process utility

The standalone `postProcess` utility executes function objects on disk-based
data in time directories. For more information, please see:

- [postProcess](ref_id://postProcess) utility.

#### Application post-processing

The `-postProcess` command-line option is available to all solvers, and operates
similarly to the stand-alone [postProcess](ref_id://postProcess) utility.
For example, having completed a [simpleFoam](ref_id://simplefoam) calculation,
the following will execute all function objects defined in the
[controlDict](ref_id://controldict) file for all time directories:

    simpleFoam -postProcess

which will execute all function objects defined in the
[controlDict](ref_id://controldict).

## Options {#options}

Function objects are grouped into libraries that perform similar operations
or operate on similar object types:

- <%= link_to_menuid "function-objects-field" %>
- <%= link_to_menuid "function-objects-forces" %>
- <%= link_to_menuid "function-objects-graphics" %>
- <%= link_to_menuid "function-objects-lagrangian" %>
- <%= link_to_menuid "function-objects-sampling" %>
- <%= link_to_menuid "function-objects-solvers" %>
- <%= link_to_menuid "function-objects-utilities" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/db/functionObjects" %>
- <%= repo_link2 "$FOAM_SRC/functionObjects" %>

API:

- [Source documentation](doxy_id://grpFunctionObjects)

<!----------------------------------------------------------------------------->
