---
title: reactingFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- combustion
- heat transfer
- solvers
menu_id: reactingfoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-combustion
group: solvers-combustion
solver_models:
- transient
- compressible
- turbulence
- heat-transfer
- chemistry
- combustion
- fvOption
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-combustion" %>
- transient
- compressible
- heat transfer
- <%= link_to_menuid "models-turbulence" %>
- <%= link_to_menuid "fvoptions" %>
- chemistry
- combustion

## Equations {#equations}

- <%= link_to_menuid "pv-pimple" %>
- [transonic option](<%= ref_id "solvers-compressible", "common-options-transonic" %>)

## Input requirements {#input-requirements}

Mandatory fields:

- p: pressure \[pa\]
- U: velocity \[m/s\]
- T: temperature \[K\]
- specie fields, e.g. N2, O2 depending on choice of thermophysical modelling

### Physical models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence):
  `constant/turbulenceProperties`
- [heat transfer](ref_id://models-thermophysical#usage):
  `constant/thermophysicalModels`
- chemistry: `constant/chemistryProperties`
- combustion: `constant/combustionProperties`
- [finite volume options](ref_id://numerics-fvoptions#usage):
  `constant/fvOptions` (optional)

### Solution controls {#input-requirements-solution-controls}

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/combustion/reactingFoam" %>

See also

- <%= link_to_menuid "rhopimplefoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/reactingFoam" %>
