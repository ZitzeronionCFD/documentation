---
title: atmLengthScaleTurbSource
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- atmospheric
- fvOption
- source
- turbulence
menu_id: fvoption-atmlengthscaleturbsource
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-atm
---

<%= page_toc %>

## Properties {#properties}

- The `atmLengthScaleTurbSource` applies sources on either
  `epsilon` or `omega` to correct mixing-length scale estimations
  for atmospheric boundary layer modelling.
- The `atmLengthScaleTurbSource` can be applied on `epsilon` or
  `omega` based RAS turbulence models.
- The `atmLengthScaleTurbSource` inherits the traits of the
  [fvOption](ref_id://fvoptions), and `cellSetOption`.

Corrections applied to either of the below, if exist:

`epsilon`
: Turbulent kinetic energy dissipation rate \[m$$^2$$/s$$^3$$\]

`omega`
: Specific dissipation rate \[1/s\]

Required fields, either of the below:

`epsilon`
: Turbulent kinetic energy dissipation rate \[m$$^2$$/s$$^3$$\]

`omega`
: Specific dissipation rate \[1/s\]

## Model equations {#model-equations}

### Turbulent kinetic energy dissipation rate

The model expression for `epsilon`:

$$
    S_p = \alpha \rho C_1^* \frac{G_o}{\nu} C_\mu k_o
$$

with (<%= cite "apsley_atm_1997" %>, Eq. 16 wherein the
exponentiation `n` is not present. `n` is an ad-hoc implementation):

$$
    C_1^* = (C_2 - C_1) \left( \frac{L}{L_{max}} \right)^n
$$

Mixing-length scale estimation (<%= cite "pope_atm_2000" %>, Eq. 10.37 & p. 374):

$$
    L = C_\mu^{3/4} \frac{k_o^{3/2}}{\epsilon_o}
$$

### Specific dissipation rate

The model expression for `omega`:

$$
    S_p = \alpha \rho \gamma^* \frac{G_o}{\nu}
$$

with (<%= cite "langner_atm_2016" %>, Eq. 3.34):

$$
    \gamma^* = \left( \gamma - \beta \right) \left( \frac{L}{L_{max}} \right)^n
$$

Mixing-length scale estimation (<%= cite "langner_atm_2016" %>, Eq. 3.20):

$$
    L = \frac{\sqrt{k}}{C_\mu^{1/4} \omega_o}
$$

Where:

$$S_p$$
: Source term without boundary conditions

$$\epsilon_o$$
: Previous-iteration epsilon \[m2/s3\]

$$\omega_o$$
: Previous-iteration omega \[1/s\]

$$k_o$$
: Previous-iteration k \[m2/s2\]

$$G_o$$
: Previous-iteration k-production contribution \[m2/s3\]

$$C_1$$
: Model constant (epsilon-based models) \[-\]

$$C_2$$
: Model constant (epsilon-based models) \[-\]

$$C_1^*$$
: Modified model constant field (epsilon-based models) \[-\]

$$C_\mu$$
: Empirical model constant \[-\]

$$\gamma$$
: Model constant (omega-based models) \[-\]

$$\beta$$
: Model constant (omega-based models) \[-\]

$$\gamma^*$$
: Modified model constant field (omega-based models) \[-\]

$$\nu$$
: Kinematic viscosity of fluid \[m2/s\]

$$L$$
: Mixing-length scale \[m\]

$$L_{max}$$
: Maximum mixing-length scale \[m\]

$$\alpha$$
: Phase fraction in multiphase computations, otherwise equals to 1

$$\rho$$
: Fluid density in compressible computations, otherwise equals to 1

## Usage {#usage}

Example of the fvOptions specification using `constant/fvOptions` file:

    atmLengthScaleTurbSource1
    {
        // Mandatory entries (unmodifiable)
        type                  atmLengthScaleTurbSource;

        atmLengthScaleTurbSourceCoeffs
        {
            // Mandatory (inherited) entries (unmodifiable)
            selectionMode    all;

            // Optional entries (unmodifiable)
            rho              rho;
            Lmax             41.575;
            n                3.0;
        }

        // Optional (inherited) entries
        ...
    }

<% assert :string_exists, "atmLengthScaleTurbSource.H", "atmLengthScaleTurbSource" %>
<% assert :string_exists, "atmLengthScaleTurbSource.C", "rho" %>
<% assert :string_exists, "atmLengthScaleTurbSource.C", "Lmax" %>
<% assert :string_exists, "atmLengthScaleTurbSource.C", "n" %>
<% assert :string_exists, "cellSetOption.C", "selectionMode" %>
<% assert :string_exists, "cellSetOption.C", "cellZone" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: atmLengthScaleTurbSource | word | yes      | -
 rho          | Name of density field               | word | no       | rho
 Lmax         | Maximum mixing-length scale         | scalar | no     | 41.575
 n            | Mixing-length scale exponent        | scalar | no     | 3.0

The inherited entries are elaborated in:

- [fvOption](ref_id://fvoptions)
  - `cellSetOption`

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmFlatTerrain" %>
- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmForestStability" %>

Source code

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/fvOptions/atmLengthScaleTurbSource" %>

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
