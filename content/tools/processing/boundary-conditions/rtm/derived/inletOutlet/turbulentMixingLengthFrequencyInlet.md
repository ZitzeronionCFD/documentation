---
title: turbulentMixingLengthFrequencyInlet
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-turbulentMixingLengthFrequencyInlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
group: bcs-inletoutlet
---

<%= page_toc %>

## Description

The `turbulentMixingLengthFrequencyInlet` is a boundary condition that
provides a turbulence specific dissipation, `omega` inlet condition based on
a specified mixing length. The patch values are calculated using:

$$
  \omega_p = \frac{k^{0.5}}{C_{\mu}^{0.25} L}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\omega_p$$          | Patch omega values       \[1/s\]
$$C_\mu$$             | Empirical model constant retrived from turbulence model
$$k$$                 | Turbulent kinetic energy \[m^2/s^2\]
$$L$$                 | Mixing length scale      \[m\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            turbulentMixingLengthFrequencyInlet;
        mixingLength    <scalar>;

        // Optional entries
        k               <word>;
        phi             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "turbulentMixingLengthFrequencyInletFvPatchScalarField.H", "turbulentMixingLengthFrequencyInlet" %>
<% assert :string_exists, "turbulentMixingLengthFrequencyInletFvPatchScalarField.C", "mixingLength" %>
<% assert :string_exists, "turbulentMixingLengthFrequencyInletFvPatchScalarField.C", "k" %>
<% assert :string_exists, "turbulentMixingLengthFrequencyInletFvPatchScalarField.C", "phi" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `turbulentMixingLengthFrequencyInlet` | word   | yes      | -
`mixingLength` | Mixing length scale \[m\]                  | scalar  |  yes     | -
`phi`          | Name of flux field                         | word    |  no      | phi
`k`            | Name of turbulent kinetic energy field     | word    | no       | k

The inherited entries are elaborated in:

- inletOutletFvPatchFields.H
<!-- end of the list -->

- In the event of reverse flow, a zero-gradient condition is applied.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/simpleReactingParcelFoam/verticalChannel" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/RAS/derivedFvPatchFields/turbulentMixingLengthFrequencyInlet" %>

API:

- [Foam::turbulentMixingLengthFrequencyInletFvPatchScalarField](doxy_id://Foam::turbulentMixingLengthFrequencyInletFvPatchScalarField)

<%= history "2.2.2" %>

<!----------------------------------------------------------------------------->
