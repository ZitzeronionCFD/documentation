---
title: Field
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
menu_id: function-objects-field
license: CC-BY-NC-ND-4.0
menu_parent: post-processing-function-objects
---

<%= page_toc %>

Options:

<%= insert_models "function-objects-field" %>

Related:

- [Source documentation](doxy_id://grpFieldFunctionObjects)
