---
title: Blended turbulence viscosity
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
- wall function
menu_id: wallfunctions-nutublended
license: CC-BY-NC-ND-4.0
menu_parent: ras-wallfunctions
group: turbulence-wall-functions
---

<%= page_toc %>

## Properties {#properties}

- Continuous wall function that blends contributions from the viscous sub-layer
  and logarithmic regions

$$
    u_\tau = \left(u_{\tau,sub}^4 + u_{\tau,log}^4 \right)^{0.25}
$$

## Usage {#usage}

The wall function is specified using:

    wallPatch
    {
        type            nutUBlendedWallFunction;
        value           XXX;
    }

## Further information {#further-information}

Source code:

- [Foam::nutUBlendedWallFunctionFvPatchScalarField](doxy_id://Foam::nutUBlendedWallFunctionFvPatchScalarField)

References:

- Automatic wall treatment: *Menter and Esch* <%= cite "menter_elements_2001" %>
