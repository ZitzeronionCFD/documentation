---
title: fixedMultiPhaseHeatFlux
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-fixedMultiPhaseHeatFlux
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-multiphase
group: bcs-multiphase
---

<%= page_toc %>

## Description

The `compressible::fixedMultiPhaseHeatFlux` is a boundary condition that
calculates a wall temperature that produces
the specified overall wall heat flux across
all the phases in an Eulerian multi-phase simulation.

Intended to be used with the `copiedFixedValue` condition
to ensure that phase wall temperature are consistent:

- Set `fixedMultiPhaseHeatFlux` boundary for one of the phases
- Use `copiedFixedValue` for all the other phases.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedMultiPhaseHeatFlux;
        q               <scalarField>;

        // Optional entries
        relax           <scalar>;
        Tmin            <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "fixedMultiPhaseHeatFluxFvPatchScalarField.H", "fixedMultiPhaseHeatFlux" %>
<% assert :string_exists, "fixedMultiPhaseHeatFluxFvPatchScalarField.C", "q" %>
<% assert :string_exists, "fixedMultiPhaseHeatFluxFvPatchScalarField.C", "relax" %>
<% assert :string_exists, "fixedMultiPhaseHeatFluxFvPatchScalarField.C", "Tmin" %>

where:

Property  | Description                          | Type    | Required | Default
----------|--------------------------------------|---------|----------|---------
`type`    | Type name: `fixedMultiPhaseHeatFlux` | word    | yes      | -
`q`       | Heat power \[W\] or flux \[W/m^2\]   | scalarField | yes  | -
`relax`   | Relaxation factor                    | scalar  | no       | 1.0
`Tmin`    | Minimum temperature limit \[K\]      | scalar  | no       | 273.0

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/reactingTwoPhaseEulerFoam/RAS/wallBoilingIATE" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/phaseSystemModels/reactingEuler/multiphaseSystem/derivedFvPatchFields/fixedMultiPhaseHeatFlux" %>

API:

- [Foam::fixedMultiPhaseHeatFluxFvPatchScalarField](doxy_id://Foam::fixedMultiPhaseHeatFluxFvPatchScalarField)

<%= history "v2012" %>

<!----------------------------------------------------------------------------->
