---
title: surfaceDistance
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- distance
- surface
menu_id: function-objects-field-surfacedistance
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `surfaceDistance` function object
computes the distance to the nearest surface from a given geometry.

### Operands

Operand       | Type             | Location
:---          |:---              |:---
input         |-                 | -
output file   | -                | -
output field  | `volScalarField` | `$FOAM_CASE/<time>/surfaceDistance`

## Usage {#usage}

Example of the `surfaceDistance` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    surfaceDistance1
    {
        // Mandatory entries (unmodifiable)
        type            surfaceDistance;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        geometry
        {
            motorBike.obj
            {
                type triSurfaceMesh;
                name motorBike;
            }
        }

        // Optional entries (runtime modifiable)
        calculateCells  true;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "surfaceDistance/surfaceDistance.H", "surfaceDistance" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "surfaceDistance",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "surfaceDistance.C", "geometry" %>
<% assert :string_exists, "surfaceDistance.C", "calculateCells" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: surfaceDistance          | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 geometry     | Surface details                     | dict |  yes     | -
 calculateCells  | Calculate distance from cell     | bool |  no      | true

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/surfaceDistance" %>

API:

- [Foam::functionObjects::surfaceDistance](doxy_id://Foam::functionObjects::surfaceDistance)

<%= history "v1912" %>

<!----------------------------------------------------------------------------->