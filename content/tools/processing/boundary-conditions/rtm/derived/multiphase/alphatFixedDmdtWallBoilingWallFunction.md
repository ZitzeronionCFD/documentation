---
title: alphatFixedDmdtWallBoilingWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-alphatFixedDmdtWallBoilingWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-multiphase
group: bcs-multiphase
---

<%= page_toc %>

## Description

The `alphatFixedDmdtWallBoilingWallFunction` is a boundary condition that provides
a simple `alphatPhaseChangeJayatillekeWallFunction` condition with
a fixed volumetric phase-change mass flux.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type         compressible::alphatFixedDmdtWallBoilingWallFunction;
        vaporPhase   <word>;

        // Optional entries
        relax        <scalar>;
        fixedDmdt    <scalar>;
        L            <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "alphatFixedDmdtWallBoilingWallFunctionFvPatchScalarField.H", "alphatFixedDmdtWallBoilingWallFunction" %>
<% assert :string_exists, "alphatFixedDmdtWallBoilingWallFunctionFvPatchScalarField.C", "vaporPhase" %>
<% assert :string_exists, "alphatFixedDmdtWallBoilingWallFunctionFvPatchScalarField.C", "relax" %>
<% assert :string_exists, "alphatFixedDmdtWallBoilingWallFunctionFvPatchScalarField.C", "fixedDmdt" %>
<% assert :string_exists, "alphatFixedDmdtWallBoilingWallFunctionFvPatchScalarField.C", "L" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `compressible::alphatFixedDmdtWallBoilingWallFunction`  | word    | yes      | -
`vaporPhase`           | Name of the vapor phase                    | word    | yes      | -
`relax`                | Relaxation factor for dmdt                 | scalar  | no       | 1.0
`fixedDmdt`            | Volumetric phase-change mass flux in near wall cells | scalar | no   | 0.0
`L`                    | Latent heat                                | scalar  | no       | 0.0

The inherited entries are elaborated in:

- alphatPhaseChangeJayatillekeWallFunctionFvPatchScalarField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/phaseSystemModels/reactingEuler/multiphaseSystem/derivedFvPatchFields/alphatFixedDmdtWallBoilingWallFunction" %>

API:

- [Foam::compressible::alphatFixedDmdtWallBoilingWallFunctionFvPatchScalarField](doxy_id://Foam::compressible::alphatFixedDmdtWallBoilingWallFunctionFvPatchScalarField)

<%= history "v2012" %>

<!----------------------------------------------------------------------------->
