---
title: nutUWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-nutUWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `nutUWallFunction` is boundary condition that provides
a wall function for the turbulent viscosity (i.e. `nut`) based on velocity
(i.e. `U`) for low- and high-Reynolds number applications.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            nutUWallFunction;

        // Inherited entries
        ...
    }

<% assert :string_exists, "nutUWallFunctionFvPatchScalarField.H", "nutUWallFunction" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `nutUWallFunction`              | word    | yes      | -

The inherited entries are elaborated in:

- nutWallFunctionFvPatchScalarField.H
- wallFunctionBlenders.H
<!-- end of the list -->
- Suffers from non-exact restart since `correctNut()` (called through
  `turbulence->validate`) returns a slightly different value every time
  it is called. See `nutUSpaldingWallFunctionFvPatchScalarField.C`.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantSimpleFoam/buoyantCavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/nutWallFunctions/nutUWallFunction" %>

API:

- [Foam::nutUWallFunctionFvPatchScalarField](doxy_id://Foam::nutUWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
