---
title: Least-squares gradient scheme
short_title: Least-squares
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- gradient
- least-squares
menu_id: schemes-gradient-leastsquares
license: CC-BY-NC-ND-4.0
#menu_parent: schemes-gradient
group: schemes-gradient
---

<%= page_toc %>

## Properties {#properties}

The cell gradient is calculated using least squares:

## Usage {#usage}

The scheme is specified using:

    gradSchemes
    {
        default         none;
        grad(U)         leastSquares;
    }

## Further information  {#further-information}

Source code

- [Foam::fv::leastSquaresGrad](doxy_id://Foam::fv::leastSquaresGrad)

See also

- pointCellsLeastSquares: employs a cell-point-cell stencil
- edgeCellsLeastSquares: employs a cell-edge-cell stencil
