---
title: Time schemes
short_title: Time
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- schemes
- time
menu_id: schemes-time
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes
---

<%= page_toc %>

Time schemes define how a property is integrated as a function of time, i.e.

$$
    \ddt{\phi}
$$

Depending on the choice of scheme, field values at previous time steps are
required, represented in the following as $$ \old{\phi} $$ and
$$ \oldold{\phi} $$ for the old and old-old time levels.

## Usage {#usage}

Time scheme properties are input in the <%= link_to_menuid "fvschemes" %>
file under the `ddtSchemes` sub-dictionary using the syntax:

    ddtSchemes
    {
        default         none;
        ddt(Q)          <time scheme>;
    }

## Options {#options}

Available time schemes include

<%= insert_models "schemes-time" %>

## Example {#example}

See [this example](example) to see the relative performance of the schemes.

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/finiteVolume/ddtSchemes/ddtScheme" %>

API:

- [Foam::fv::ddtScheme](doxy_id://Foam::fv::ddtScheme)
