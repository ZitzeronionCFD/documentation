---
title: Thermal boundary conditions
short_title: Thermal
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-thermal
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

Available thermal boundary conditions include:

<%= insert_models "bcs-thermal" %>
