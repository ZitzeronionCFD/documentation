---
title: Lagrangian
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- function object
- post-processing
- Lagrangian
menu_id: function-objects-lagrangian
license: CC-BY-NC-ND-4.0
menu_parent: post-processing-function-objects
---

<%= page_toc %>

Options:

<%= insert_models "function-objects-lagrangian" %>

Related:

- [Source documentation](doxy_id://grpLagrangianFunctionObjects)
