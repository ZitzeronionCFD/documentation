# frozen_string_literal: true

# LaTeX to PDF
class Latex2PDFFilter < Nanoc::Filter
  identifier :latex2pdf
  type text: :binary

  def run(content,_params)
    Tempfile.open(['nanoc-latex', '.tex']) do |f|
      f.write(content)
      f.flush

      system(
        'pdflatex',
        '-halt-on-error',
        '-output-directory',
        File.dirname(f.path),
        f.path
      )
      system('mv', f.path.sub('.tex', '.pdf'), output_filename)
    end
  end
end
