---
title: k-ω Shear Stress Transport (SST)
short_title: k-ω-SST
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-komegasst
license: CC-BY-NC-ND-4.0
menu_parent: ras-linearevm
group: turbulence-ras-evm
---

<%= page_toc %>

## Properties {#properties}

- Two equation model for the turbulence kinetic energy, $$ k $$, and
  turbulence specific dissipation rate, $$ \omega $$.
- Aims to overcome the defficiencies of the standard k-omega model wrt
  dependency on the freestream values of k and omega
- Able to capture flow separation
- OpenFOAM variant is based on the 2003 model <%= cite "menter_ten_2003" %>

## Model equations {#model-equations}

The turbulence specific dissipation rate equation is given by:

$$
    \Ddt{\rho \omega}
  =
    \div \left( \rho D_\omega \grad \omega \right)
  + \frac{\rho \gamma G}{\nu}
  - \frac{2}{3} \rho \gamma \omega \left( \div \u \right)
  - \rho \beta \omega^2
  - \rho \left(F_1 - 1\right) CD_{k\omega}
  + S_\omega,
$$

and the turbulence kinetic energy by:

$$
    \Ddt{\rho k}
  =
    \div \left( \rho D_k \grad k \right)
  + \rho G
  - \frac{2}{3} \rho k \left( \div \u \right)
  - \rho \beta^{*} \omega k
  + S_k.
$$

The turbulence viscosity is obtained using:

$$
    \nu_t = a_1 \frac{k}{\max (a_1 \omega_, b_1 F_{23} \tensor{S})}
$$

## Default model coefficients {#default-coeffs}

$$\alpha_{k1}$$ | $$\alpha_{k2}$$ | $$\alpha_{\omega 1}$$ | $$\alpha_{\omega 2}$$ | $$\beta_1$$ | $$\beta_2$$ | $$\gamma_1$$ | $$\gamma_2$$
-------------|-----------|-----------|-----------------|----------------|--------------|-----|------------
        0.85 |      1.0 |      0.5 |      0.856      |      0.075        |     0.0828  | 5/9 | 0.44

$$\beta^{*}$$ | $$a_1$$ | $$b_1$$ | $$c_1$$
----------------|-----------|-----------|----------
           0.09 |      0.31 |       1.0 |     10.0

## Initialisation {#initialisation}

For isotropic turbulence, the turbulence kinetic energy can be estimated by:

$$
    k = \frac{3}{2} \left(I |\u_{\ref}|\right)^{2}
$$

where $$ I $$ is the intensity, and $$ \u_{\ref} $$ a reference velocity.
The turbulence specific dissipation rate follows as:

$$
    \omega = \frac{k^{0.5}}{C_{\mu}^{0.25} L}
$$

where $$ C_{\mu} $$ is a constant equal to 0.09, and $$ L $$ a reference
length scale.

## Usage {#usage}

The model is specified using:

    RAS
    {
        turbulence      on;
        RASModel        kOmegaSST;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "turbulenceModels/RAS/kOmegaSST/kOmegaSST.H", "kOmegaSST" %>

## Boundary conditions {#bcs}

Inlet

- <%= link_to_menuid "boundary-conditions-fixedValue" %>
- [turbulentMixingLengthFrequencyInlet]([Foam::turbulentMixingLengthDissipationRateInletFvPatchScalarField](doxy_id://Foam::turbulentMixingLengthDissipationRateInletFvPatchScalarField))

Outlet

- [Constrained outlet](ref_id://boundary-conditions-inletOutlet)

Walls

- wall functions

## Further information {#further-information}

Source code:

- [Foam::RASModels::kOmegaSST](doxy_id://Foam::RASModels::kOmegaSST)
- [Foam::kOmegaSSTBase](doxy_id://Foam::kOmegaSSTBase)

References:

- Base model: *Menter and Esch* <%= cite "menter_elements_2001" %>
- Updated model: *Menter et al.* <%= cite "menter_ten_2003" %>
- Corrections: consistent production terms from the 2001 paper as form in the
  2003 paper is a typo, see <%= cite "nasa_langley_research_center_menter_2015" %>
- F3 term for rough walls: *Hellsten* <%= cite "hellsten_improvements_1997" %>

See also:

- [DES variant](ref_id://des-komegasstdes)
