---
title: Adiabatic perfect fluid
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- adiabatic
- equation of state
- perfect fluid
menu_id: equationOfState-adiabaticPerfectFluid
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-equationOfState
group: equation-of-state-models
---

<%= page_toc %>

Density, $$ \rho $$ \[kg/m$$^3$$\]

$$
\rho = \rho_0 \frac{p + B}{p_0 + B}^{\frac{1}{\gamma}}
$$

Entropy, $$ S $$ \[J/kg/K \]

$$
S = -(p_0 + B)^\frac{1}{\gamma}
    \frac{(p + B)^{1 - \frac{1}{\gamma}} - (P_{\std} + B)^n}{\rho_0 T (1 - \frac{1}{\gamma})}
$$

Compressibility, $$ \psi $$ \[s$$^2$$/m$$^2$$\]

$$
\psi = \frac{\rho_0}{\gamma(p_0 + B)} \frac{p + B}{p_0 + B}^{\frac{1}{\gamma} - 1}
$$

## Usage

The `adiabaticPerfectFluid` equation of state is specified as:

~~~
equationOfState
{
    p0          <scalar>;
    rho0        <scalar>;
    gamma       <scalar>;
    B           <scalar>;
}
~~~

Where

- `p0` : Reference pressure \[Pa\]
- `rho0` : Reference density \[kg/m$$^3$$\]
- `gamma` : Isentropic exponent \[-\]
- `B` : Pressure offset for a stiffened gas \[Pa\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/equationOfState/adiabaticPerfectFluid" %>
