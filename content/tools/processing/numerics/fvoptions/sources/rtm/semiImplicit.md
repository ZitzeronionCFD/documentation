---
title: Semi implicit source
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- fvOption
- source
- semi-implicit
menu_id: fvoption-semiimplicit
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-general
---

<%= page_toc %>

## Properties {#properties}

A typical transport equation takes the form:

$$
    \frac{D}{Dt}x = S(x)
$$

where the source, $$ S(x) $$, is decomposed into explicit and (linearised)
implicit contributions

$$
    S(x) = \underbrace{S_u}_{\mathrm{explicit}} + \underbrace{S_p x}_{\mathrm{implicit}}
$$

## Usage {#usage}

The source is available as a general option, specified using:

    generalSource
    {
        type            <type>SemiImplicitSource;

        ...

        volumeMode      absolute; // absolute | specific

        injectionRateSuSp
        {
            <field>           (<explicit value> <implicit coefficient>);
            ...
        }
    }

<% assert :string_exists, "SemiImplicitSource.H", "SemiImplicitSource" %>
<% assert :string_exists, "SemiImplicitSource.H", "volumeMode" %>
<% assert :string_exists, "SemiImplicitSource.H", "absolute" %>
<% assert :string_exists, "SemiImplicitSource.H", "specific" %>
<% assert :string_exists, "SemiImplicitSource.H", "injectionRateSuSp" %>

Where `<type>` is a primitive field type, e.g. `scalar`,
`vector`, `tensor`.  As an example, a scalar source applied to the
k and epsilon turbulence equations could take the form:

    scalarSource
    {
        type            scalarSemiImplicitSource;
        duration        1000.0;
        selectionMode   all;

        volumeMode      absolute;

        injectionRateSuSp
        {
            k           (30.7 0);
            epsilon     (1.5  0);
        }
    }

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/reactingParcelFoam/filter" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/TJunction" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/laminar/planarPoiseuille" %>
- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/reactingTwoPhaseEulerFoam/laminar/injection" %>

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/general/semiImplicitSource" %>
