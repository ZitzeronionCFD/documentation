---
title: writeCellCentres
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- cell
- centres
menu_id: function-objects-field-writecellcentres
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `writeCellCentres` function object writes the cell-centres volVectorField
and the three component fields as `volScalarField`s.

### Operands

Operand       | Type             | Location
:---          |:---              |:---
input         | -                | -
output file   | -                | -
output field 1| `volVectorField` | `$FOAM_CASE/<time>/C`
output field 2| `volScalarField` | `$FOAM_CASE/<time>/{Cx,Cy,Cz}`

## Usage {#usage}

Example of the `writeCellCentres` function object by using `functions`
sub-dictionary in `system/controlDict` file:

    writeCellCentres1
    {
        // Mandatory entries (unmodifiable)
        type            writeCellCentres;
        libs            (fieldFunctionObjects);

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists,
   "writeCellCentres/writeCellCentres.H", "writeCellCentres" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "writeCellCentres",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: writeCellCentres         | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Example by using the `postProcess` utility:

    postProcess -func writeCellCentres

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/writeCellCentres" %>

API:

- [Foam::functionObjects::writeCellCentres](doxy_id://Foam::functionObjects::writeCellCentres)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->