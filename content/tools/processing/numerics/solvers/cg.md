---
title: Conjugate gradient solvers
short_title: Conjugate gradient
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- conjugate gradient
- solvers
menu_id: linearequationsolvers-cg
license: CC-BY-NC-ND-4.0
menu_parent: numerics-linearequationsolvers
---

<%= page_toc %>

## Options {#options}

<%= insert_models "solvers-cg" %>

## Preconditioners {#preconditioners}

Preconditioners manipulate the matrix equation to a form that is more readily
solved:

$$
    A P^{-1} P \vec{x} = \vec{b}
$$

<%= insert_models "solvers-cg-preconditioner" %>
