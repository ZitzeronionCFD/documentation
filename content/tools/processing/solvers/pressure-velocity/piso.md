---
title: PISO algorithm
menu_id: pv-piso
license: CC-BY-NC-ND-4.0
menu_parent: pressure-velocity
---

<%= page_toc %>

\underConstruction

<B>P</B>ressure <B>I</B>mplicit with <B>S</B>plitting of <B>O</B>perators

- By *Issa* <%= cite "issa_solution_1986" %>

<!--
while t < endtime do
Set t := t + ∆t
Construct [M[U]] ] {momentum matrix}
Solve Eqn. (3) for U {momentum predictor}
Set pIter := 0
repeat
Set pIter := pIter + 1
Construct pressure equation Eqn. (8)
Solve Eqn. (8) for p {pressure corrector}
Substitute p into Eqn. (7) ⇒ φ {flux corrector}
Substitute p into Eqn. (6) ⇒ U {momentum corrector}
until converged or pIter > nIter
end while
-->
