---
title: Fundamentals
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
menu_id: fundamentals
license: CC-BY-NC-ND-4.0
menu_parent: home
menu_weight: 20
---

- [About](about)
- [Case structure](case-structure)
- [Input types](input-types)
- [Command line](command-line)
- [Tracking issues](tracking-issues)

<!----------------------------------------------------------------------------->
