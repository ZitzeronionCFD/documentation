---
title: uniformFixedValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-uniformFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `uniformFixedValue` is a boundary condition that
provides a uniform fixed value condition.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                uniformFixedValue;
        uniformValue        <PatchFunction1<Type>>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "uniformFixedValueFvPatchField.H", "uniformFixedValue" %>
<% assert :string_exists, "uniformFixedValueFvPatchField.C", "uniformValue" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `uniformFixedValue` | word    | yes      | -
`uniformValue`  | Uniform fixed values   | PatchFunction1\<Type\> | yes | -

The inherited entries are elaborated in:

- [fixedValueFvPatchField.H](ref_id://boundary-conditions-fixedValue)
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/turbineSiting" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/uniformFixedValue" %>

API:

- [Foam::uniformFixedValueFvPatchField](doxy_id://Foam::uniformFixedValueFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
