---
title: uniformJumpAMI
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- coupled
menu_id: boundary-conditions-uniformJumpAMI
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-coupled
group: bcs-coupled-jump
---

<%= page_toc %>

## Description

The `uniformJumpAMI` is a general coupled boundary condition that
provides a jump condition, using the `cyclicAMI` condition as a base.
The jump is specified as a time-varying uniform value across the patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            uniformJumpAMI;
        jumpTable       <Function1<Type>>;

        // Inherited entries
        patchType       cyclicAMI;
        ...
    }

<% assert :string_exists, "uniformJumpAMIFvPatchField.H", "uniformJumpAMI" %>
<% assert :string_exists, "uniformJumpAMIFvPatchField.C", "jumpTable" %>

where:

Property | Description                  | Type    | Required | Default
---------|------------------------------|---------|----------|---------
`type`   | Type name: `uniformJumpAMI`  | word    | yes      | -
`jumpTable`   | Jump values             | Function1\<Type\>  | yes  | -

The inherited entries are elaborated in:

- fixedJumpFvPatchField.H
- Function1.H
<!-- end of the list -->

- The underlying `patchType` should be set to `cyclicAMI`.
{: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/uniformJumpAMI" %>

API:

- [Foam::uniformJumpAMIFvPatchField](doxy_id://Foam::uniformJumpAMIFvPatchField)

<%= history "2.2.0" %>

<!----------------------------------------------------------------------------->
