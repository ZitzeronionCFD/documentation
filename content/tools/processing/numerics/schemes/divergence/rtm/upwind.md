---
title: Upwind divergence scheme
short_title: Upwind
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence-upwind
license: CC-BY-NC-ND-4.0
group: schemes-divergence-nvdtvd
---

<%= page_toc %>

## Properties {#properties}

- First order
- Bounded
- Face value set according to the upstream value
- Equivalent to assuming that the cell values are isotropic with a value that
  represents the average value

$$ \phi_f = \phi_c $$

## Normalised Variable Diagram {#nvd}

![upwind](../upwind.png)

## Usage {#usage}

The scheme is specified using:

    divSchemes
    {
        default         none;
        div(phi,U)      Gauss upwind;
    }

<% assert :string_exists, "upwind.H", "upwind" %>

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/interpolation/surfaceInterpolation/limitedSchemes/upwind" %>

API:

- [Foam::upwind](doxy_id://Foam::upwind)

Reference:

- *Spalding* <%= cite "spalding_novel_1972" %>
