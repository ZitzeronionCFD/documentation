---
title: thermalBaffle1D
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-thermalBaffle1D
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `thermalBaffle1D` is a boundary condition that solves a steady 1D thermal baffle.

The solid properties are specify as dictionary. Optionally radiative heat
flux (qr) can be incorporated into the balance. Some under-relaxation might
be needed on qr.  Baffle and solid properties need to be specified on the
master side of the baffle.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            thermalBaffle1D;

        // Optional entries
        baffleActivated <bool>;
        qrRelaxation    <scalar>;
        qr              <word>;
        thickness       <scalarField>;
        qs              <scalarField>;
        qrPrevious      <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "thermalBaffle1DFvPatchScalarField.H", "thermalBaffle1D" %>
<% assert :string_exists, "thermalBaffle1DFvPatchScalarField.C", "baffleActivated" %>
<% assert :string_exists, "thermalBaffle1DFvPatchScalarField.C", "qrRelaxation" %>
<% assert :string_exists, "thermalBaffle1DFvPatchScalarField.C", "qr" %>
<% assert :string_exists, "thermalBaffle1DFvPatchScalarField.C", "thickness" %>
<% assert :string_exists, "thermalBaffle1DFvPatchScalarField.C", "qs" %>
<% assert :string_exists, "thermalBaffle1DFvPatchScalarField.C", "qrPrevious" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `thermalBaffle1D`               | word    | yes      | -
`baffleActivated`      | Flag to enable baffle                      | bool    | no       | true
`qrRelaxation`         | Relaxation factor for qr solution          | scalar  | no       | 1.0
`qr`                   | Name of qr field                           | word    | no       | none
`thickness`            | Baffle thickness field                     | scalarField  | no  | -
`qs`                   | Superficial heat source \[W/m^2\]          | scalarField  | no  | -
`qrPrevious`           | Cache for qr field                         | scalarField  | no  | -


The inherited entries are elaborated in:

- mappedPatchBase.H
- mixedFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantSimpleFoam/circuitBoardCooling" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/thermalBaffle1D" %>

API:

- [Foam::compressible::thermalBaffle1DFvPatchScalarField](doxy_id://Foam::compressible::thermalBaffle1DFvPatchScalarField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
