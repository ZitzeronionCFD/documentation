---
title: searchablePlate
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: searchableplate
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh-geometry
group: searchableobjects
---

<%= page_toc %>

![plane example](../images/searchablePlate-small.png)

A finite plate aligned with co-ordinate system. User must specify span
in two directions, the third must be zero. Plate could be used e.g. to create
a baffle or measuring plane.

## Usage

    plane
    {
        type            searchablePlate;
        origin          (0 0 0);
        span            (2 4 0);
    }

<% assert :string_exists, "searchablePlate.H", "searchablePlate" %>
<% assert :string_exists, "searchablePlate.C", "origin" %>
<% assert :string_exists, "searchablePlate.C", "span" %>

Where:

Property  | Description                  | Required
----------|------------------------------|---------
`origin`  | Centre of plate              |      yes
`span`    | Span in each direction       |      yes

Note that the `span` must have one `0` entry, e.g.

- `(1 1 0)`
- `(1 0 1)`
