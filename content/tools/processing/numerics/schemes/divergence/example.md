---
title: Divergence scheme example
short_title: Example
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- example
menu_id: schemes-divergenceexample
license: CC-BY-NC-ND-4.0
menu_parent: schemes-divergence
group:
  schemes-examples
---

<%= page_toc %>

- Solver: <%= link_to_menuid "scalartransportfoam" %>
- Repository link: <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/schemes/divergenceExample" %>

## Introduction {#introduction}

This example case comprises a 2-D box section, across which a uniform flow
is applied.  A passive scalar is introduced across the left boundary, and
transported across the domain.  By looking at the profile of the scalar part
way across the section, we can determine the characteristics of our divergence
scheme selection.

## Case set-up {#case-set-up}

A frozen flow field is set by the `U` field, and the passive scalar by the
`T` field.  The time step is set to 5e-3s, leading to a maximum Courant
number of 0.5.  The impact of the choice of numerical scheme is assessed by
varying the `div` scheme entry of the [fvSchemes](ref_id://numerics-schemes) file:

    divSchemes
    {
        div(phi,T)      Gauss <interpolation scheme>
    }

### Mesh {#mesh}

A 2-D square domain with side length 1m is employed, with two mesh variants: 50x50
uniform quad cells and a triangle mesh:

| ![Case set-up](images/example-divergence-schematic.png) | ![](images/tri/divergence-test-tri-mesh.png) |
{: .noBorder}

Results are sampled at the location shown by the dashed line.

### Boundary conditions {#boundary-conditions}

The velocity is set to a constant value of (1,1,0) m/s across the domain
with the following boundary conditions:

| Patch         | Type         | Value   |
| :------------ | :----------- | ------: |
| left          | fixedValue   | (1,1,0) |
| right         | zeroGradient |         |
| top           | zeroGradient |         |
| bottom        | fixedValue   | (1,1,0) |
| frontAndBack  | empty        |         |

The passive scalar is initialised to zero and evolved with the velocity
field by assigning a value of 1 at the `left` boundary:

| Patch         | Type         | Value   |
| :------------ | :----------- | ------: |
| left          | fixedValue   | 1       |
| right         | zeroGradient |         |
| top           | zeroGradient |         |
| bottom        | fixedValue   | 0       |
| frontAndBack  | empty        |         |

## Results {#results}

The following sequence of images show the steady results for the spatial
distribution of the passive scalar.
First order methods such as  [upwind](ref_id://schemes-divergence-upwind)
show their diffusive characteristic, whereby the sharp profile is heavily
smeared as the flow is not aligned with the mesh:

| ![](images/quad/divergence-test-quad-upwind.png) |![](images/tri/divergence-test-tri-upwind.png) | ![](images/graphs/divergence-test-upwind-result.png) |
| | [upwind](ref_id://schemes-divergence-upwind) | |
{: .noBorder}

The [linear](ref_id://schemes-divergence-linear) scheme is a second order
method, able to better preserve the step profile, but at the expense of
unboundedness:

| ![](images/quad/divergence-test-quad-linear.png)| ![](images/tri/divergence-test-tri-linear.png) | ![](images/graphs/divergence-test-linear-result.png) |
| | [linear](ref_id://schemes-divergence-linear) | |
{: .noBorder}

The remaining methods provide a blend between first order upwind and
second order linear methods:

| ![](images/quad/divergence-test-quad-Minmod.png) | ![](images/tri/divergence-test-tri-Minmod.png) | ![](images/graphs/divergence-test-Minmod-result.png) |
| | [Minmod](ref_id://schemes-divergence-minmod) | |
| ![](images/quad/divergence-test-quad-SFCD.png) | ![](images/tri/divergence-test-tri-SFCD.png) | ![](images/graphs/divergence-test-SFCD-result.png) |
| | [SFCD](ref_id://schemes-divergence-sfcd) | |
| ![](images/quad/divergence-test-quad-limitedLinear-coeff10.png)| ![](images/tri/divergence-test-tri-limitedLinear-coeff10.png)| ![](images/graphs/divergence-test-limitedLinear_10-result.png) |
| | [limitedLinear 1.0](ref_id://schemes-divergence-limitedlinear) | |
| ![](images/quad/divergence-test-quad-vanLeer.png) | ![](images/tri/divergence-test-tri-vanLeer.png) | ![](images/graphs/divergence-test-vanLeer-result.png) |
| | [vanLeer](ref_id://schemes-divergence-vanleer) | |
| ![](images/quad/divergence-test-quad-MUSCL.png) | ![](images/tri/divergence-test-tri-MUSCL.png) | ![](images/graphs/divergence-test-MUSCL-result.png) |
| | [MUSCL](ref_id://schemes-divergence-muscl) | |
| ![](images/quad/divergence-test-quad-midPoint.png) | ![](images/tri/divergence-test-tri-midPoint.png) | ![](images/graphs/divergence-test-midPoint-result.png) |
| | [midPoint](ref_id://schemes-divergence-midpoint) - equivalent to [linear](ref_id://schemes-divergence-linear) on isotropic structured meshes | |
| ![](images/quad/divergence-test-quad-linearUpwind.png)| ![](images/tri/divergence-test-tri-linearUpwind.png)| ![](images/graphs/divergence-test-linearUpwind_gradU-result.png) |
| | [linearUpwind](ref_id://schemes-divergence-linearupwind) | |
| ![](images/quad/divergence-test-quad-QUICK.png) | ![](images/tri/divergence-test-tri-QUICK.png) | ![](images/graphs/divergence-test-QUICK-result.png) |
| | [QUICK](ref_id://schemes-divergence-quick) | |
{: .noBorder}