---
title: nutLowReWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-nutLowReWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `nutLowReWallFunction` is boundary condition that provides
a simple wrapper around a zero
fixed-value condition for the turbulent viscosity (i.e. `nut`)
for low-Reynolds number applications. It sets `nut` to zero,
and provides an access function to calculate y+.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            nutLowReWallFunction;

        // Inherited entries
        ...
    }

<% assert :string_exists, "nutLowReWallFunctionFvPatchScalarField.H", "nutLowReWallFunction" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `nutLowReWallFunction`          | word    | yes      | -

The inherited entries are elaborated in:

- nutWallFunctionFvPatchScalarField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/ellipsekkLOmega" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/nutWallFunctions/nutLowReWallFunction" %>

API:

- [Foam::nutLowReWallFunctionFvPatchScalarField](doxy_id://Foam::nutLowReWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
