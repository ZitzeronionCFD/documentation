---
title: inletOutletTotalTemperature
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-inletOutletTotalTemperature
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutletsTotalTemperature
groups:
- bcs-inletOutlet
---

<%= page_toc %>

## Description

The `inletOutletTotalTemperature` is a temperature boundary condition that provides
an outflow condition for total temperature for use with supersonic cases, where
a user-specified value is applied in the case of reverse flow.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            inletOutletTotalTemperature;
        T0              <scalarField>;
        gamma           <scalar>;

        // Optional entries
        U               <word>;
        psi             <word>;
        phi             <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "inletOutletTotalTemperatureFvPatchScalarField.H", "inletOutletTotalTemperature" %>
<% assert :string_exists, "inletOutletTotalTemperatureFvPatchScalarField.C", "T0" %>
<% assert :string_exists, "inletOutletTotalTemperatureFvPatchScalarField.C", "gamma" %>
<% assert :string_exists, "inletOutletTotalTemperatureFvPatchScalarField.C", "U" %>
<% assert :string_exists, "inletOutletTotalTemperatureFvPatchScalarField.C", "psi" %>
<% assert :string_exists, "inletOutletTotalTemperatureFvPatchScalarField.C", "phi" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `inletOutletTotalTemperature`   | word    | yes      | -
`T0`           | Total temperature patch field              | scalarField | yes  | -
`gamma`        | Heat-capacity ratio                        | scalar  | yes      | -
`U`            | Name of velocity field                     | word    | no       | U
`psi`          | Name of compressibility field              | word    | no       | psi
`phi`          | Name of flux field                         | word    | no       | phi

The inherited entries are elaborated in:

- [Foam::inletOutletFvPatchField](doxy_id://Foam::inletOutletFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/inletOutletTotalTemperature" %>

API:

- [Foam::inletOutletTotalTemperatureFvPatchScalarField](doxy_id://Foam::inletOutletTotalTemperatureFvPatchScalarField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
