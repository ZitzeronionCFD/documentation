---
title: wallHeatFlux
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- heat flux
- post-processing
- wall
menu_id: function-objects-field-wallheatflux
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#sec-fos-field-wallHeatFlux-description}

The `wallHeatFlux` function object computes the wall-heat flux at selected wall
patches.

### Operands

Operand       | Type       | Location
:---          |:---        |:---
input         | -          | -
output file   | dat        | `$FOAM_CASE/postProcessing/<FO>/<time>/<field>`
output field  | `volScalarField` (only boundaryField) | `$FOAM_CASE/<time>/<outField>`

## Usage {#sec-fos-field-wallHeatFlux-usage}

Example of the `wallHeatFlux` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    wallHeatFlux1
    {
        // Mandatory entries (unmodifiable)
        type            wallHeatFlux;
        libs            (fieldFunctionObjects);

        // Optional entries (runtime modifiable)
        patches     (<patch1> ... <patchN>); // (wall1 "(wall2|wall3)");
        qr          qr;

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "wallHeatFlux.H", "wallHeatFlux" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "wallHeatFlux",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "wallHeatFlux.C", "patches" %>
<% assert :string_exists, "wallHeatFlux.C", "qr" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: wallHeatFlux             | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 patches  | Names of operand patches         | wordList | no | all wall patches
 qr           | Name of radiative heat flux field   | word | no       | qr

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Example by using the `postProcess` utility:

    <solver> -postProcess -func wallHeatFlux

## Stored properties {#stored-properties}

### Reduced data

The following data are stored by the function object, and can be used by other
function objects:

- `min(<patch-name>)` : minimum patch value
- `max(<patch-name>)` : maximum patch value
- `int(<patch-name>)` : integral patch value

## Further information {#sec-fos-field-wallHeatFlux-further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- [Foam::functionObjects::wallHeatFlux](doxy_id://Foam::functionObjects::wallHeatFlux)

API:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/wallHeatFlux" %>

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
