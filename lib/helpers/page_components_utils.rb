# frozen_string_literal: true

module Utils
  module PageComponents
    def doc_toc
      @config[:main_menu].html
    end

    def issue_link(item)
      doc_url = 'https://gitlab.com/openfoam/documentation'
      title = h("Doc improvement: <ADD YOUR TITLE HERE>")
      description = h("Page: #{@item[:filename]}\n\n** Describe your proposed changes **\n\n<!-- Do not edit below this line --> \n\n~improvement")
      "#{doc_url}/-/issues/new?issue[title]=#{title}&issue[description]=#{description}&v=#{@item.identifier.without_ext}"
    end

    def authors(item)
      return "OpenCFD Ltd." if item[:authors].nil?

      item[:authors].join(', ')
    end

    def copyright(item)
      datestr = "#{File.mtime(item[:filename]).to_datetime.strftime("%Y")}"
      str = +"Copyright &#169; #{datestr} OpenCFD Ltd."
      if item[:copyright] && item[:copyright].size() > 1
        str << " and Community"
      end

      str
    end

    def doc_footer(item)
      doc_url = 'https://gitlab.com/openfoam/documentation'
      ops = 'target="_blank" rel="nofollow noreferrer noopener"'

      out = +'<div class="footer">'
      out << "<h3 class=\"no-top\">Found a content problem with this page?</h3>"
      out << "<ul>"
      out << "<li><a #{ops} href=\"#{issue_link(item)}\">Submit a bug report</a></li>"
      out << "<li><a #{ops} href=\"#{doc_url}/-/edit/main/#{item[:filename]}\">Edit source code on GitLab</a></li>"
      out << "<li><a #{ops} href=\"#{doc_url}/-/blob/main/#{item[:filename]}\">View source code on GitLab</a></li>"
      out << "</ul>"
      out << "<p>Want to get more involved? <a href=\"#{ref_id("contributing")}\">Learn how to contribute</a></p>"
      datestr = "#{File.mtime(item[:filename]).to_datetime.strftime("%b %d, %Y")}"
      out << "<p>Last updated: #{datestr}</p>"
      out << "</div>"
      out << '<div>'
      out << "<p> Author(s): #{authors(item)}</p>"
      #out << '<p>' << item[:copyright].join(', ').gsub(/\s*\([Cc]\)/, ' &#169;') << '</p>' unless item[:copyright].nil?
      out << "<p> #{copyright(item)}<p>"
      out << "</div>"
      out << '<hr class="divider-solid">'
      out << @config[:license].html(item[:license]) unless item[:license].nil?
    end
  end
end
