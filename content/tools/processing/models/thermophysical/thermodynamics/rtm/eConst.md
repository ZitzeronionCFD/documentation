---
title: Constant (energy)
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- constant
- energy
- thermodynamics
menu_id: thermo-eConst
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-thermo
group: thermo-models
---

<%= page_toc %>

Heat capacity at constant volume, $$ c_v $$ \[J/kg/K\]

$$
c_v = \sum_{i=1}^N c_i T^{i-1}
$$

## Usage

The `eConst` thermodynamics model is defined using the entries:

~~~
thermodynamics
{
    Cv              <scalar>;
    Hf              <scalar>;
    Tref            <scalar>;
    Esref           <scalar>;
}
~~~

Where

- `Cv` : specific heat capacity at constant volume \[J/kg/K\]
- `Hf` : heat of formation \[J/kg\]
- `Tref` : reference temperature \[K\]; default is `Tstd`
- `Esref` : reference sensible enthalpy around which to linearise \[J/kg\];
  default is zero.

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/thermo/eConst" %>
