---
title: nutUTabulatedWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-nutUTabulatedWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `nutUTabulatedWallFunction` is boundary condition that provides
a wall constraint on the turbulent viscosity (i.e. `nut`) based on velocity
(i.e. `U`), for low- and high-Reynolds number applications.

As input, the user specifies a look-up table of `u+` as a function of
near-wall Reynolds number.

The table should be located in the `$FOAM_CASE/constant` directory.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            nutUTabulatedWallFunction;
        uPlusTable      <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "nutUTabulatedWallFunctionFvPatchScalarField.H", "nutUTabulatedWallFunction" %>
<% assert :string_exists, "nutUTabulatedWallFunctionFvPatchScalarField.H", "uPlusTable" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `nutUTabulatedWallFunction`     | word    | yes      | -
`uPlusTable`   | u+ as a function of Re table name          | word    | yes      | -

The inherited entries are elaborated in:

- nutWallFunctionFvPatchScalarField.H
- uniformInterpolationTable.H
<!-- end of the list -->

- The tables are not registered since the same table object may be used for
more than one patch.
{: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/wallFunctions/nutWallFunctions/nutUTabulatedWallFunction" %>

API:

- [Foam::nutUTabulatedWallFunctionFvPatchScalarField](doxy_id://Foam::nutUTabulatedWallFunctionFvPatchScalarField)

<%= history "2.4.0" %>

<!----------------------------------------------------------------------------->
