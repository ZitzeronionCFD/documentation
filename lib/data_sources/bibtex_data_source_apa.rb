# frozen_string_literal: true

require 'bibtex'
require 'citeproc'
require 'csl/styles'

# Reads bibtex file and formats citations using citeproc's apa style
class BibtexDataSource < Nanoc::DataSource
  identifier :bibtex2

  def up
    @files = Dir[@config[:path].sub(%r{/?$}, '/*.bib')]
  end

  def md_protect(str)
    s = str.gsub(/[{}]/, '')
    s.gsub!(/\\/, '')
    #s.gsub!(/\[/, '\\[')
    #s.gsub!(/\]/, '\\]')
    s
  end

  def items
    @files.map do |file|
      stat = File.stat file
      file_checksum = "#{stat.size}-#{stat.mtime.to_i}"

      cp = CiteProc::Processor.new style: 'apa', format: 'html'

      # NOTE: do not want to convert url
      cp.import BibTeX.open(file).convert(:latex).to_citeproc

      cp.items.map do |key, entry|
        new_item(
          entry.to_s, {
            apa: md_protect(cp.render(:bibliography, id: key)[0]),
            citation: cp.render(:citation, id: key)
          },
          "/#{key}",
          checksum_data: file_checksum
        )
      end
    end.flatten!
  end
end
