# frozen_string_literal: true

require 'nokogiri'

# Doxygen
module Doxy
  # Doxygen tag processing
  class Tags
    # attr_reader :tags

    def initialize(api_url)
      @api_url = api_url
      @tags = {}
      root_path2 = "#{ENV['PWD']}/assets/doxygen"
      files = Dir[root_path2.sub(%r{/?$}, '/*.tags')]
      if files.empty?
        puts '- WARNING: no TAGS file found'
        return
      end
      puts "- API url: #{@api_url}"

      puts "- Reading tags from #{files}"
      reader = Nokogiri::XML::Reader(File.open(files[0]))

      cls = nil
      path = nil
      reader.each do |node|
        next if node.node_type != Nokogiri::XML::Reader::TYPE_ELEMENT

        if node.name == 'compound' && (node.attribute('kind') == 'class' || node.attribute('kind') == 'group')
          @tags[cls] = path unless cls.nil? || path.nil?
          path = nil
          cls = nil
        end
        if !cls && node.name == 'name'
          cls = node.inner_xml.gsub(/&lt;/, '<').gsub(/&gt;/, '>').gsub(/ /, '')
        elsif !path && node.name == 'filename' # 'path'
          path = node.inner_xml
        end
      end
    end

    def link(cls, title = nil)
      puts "- WARNING: tag #{cls} not found!" if @tags[cls].nil?

      # puts "Creating link to #{cls}"

      "<a href=\"#{@api_url}/#{@tags[cls]}\" target=\"_blank\" rel=\"noopener noreferrer\">#{title || cls}</a>"
    end

    def url(cls)
      puts "- WARNING: tag #{cls} not found!" if @tags[cls].nil?

      # puts "Creating link to #{cls}"
      "#{@api_url}/#{@tags[cls]}"
    end
  end
end
