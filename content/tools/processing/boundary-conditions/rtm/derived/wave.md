---
title: Wave boundary conditions
short_title: Wave
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-wave
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

Available wave boundary conditions include:

<%= insert_models "bcs-wave" %>
