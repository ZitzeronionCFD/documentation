---
title: Contributing
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- contribute
- documentation
menu_id: contributing
license: CC-BY-NC-ND-4.0
menu_parent: home
menu_weight: 10000
---

- [Contributors](./contributors)
- [How to contribute](./how-to)