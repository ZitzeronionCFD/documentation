---
title: waveTransmissive
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- outlet
menu_id: boundary-conditions-waveTransmissive
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-outlet
group: bcs-outlet-general
---

<%= page_toc %>

## Description

The `waveTransmissive` is a general boundary condition that
provides a wave transmissive outflow condition, based on solving
DDt(W, field) = 0 at the boundary, `W` is the wave velocity
and `field` is the field to which this boundary condition is applied.

The wave speed is calculated using:

$$
  w_p = \frac{\phi_p}{|\vec{S}_f|} + \sqrt{\frac{\gamma}{\psi_p}}
$$

where:

Property             | Description
---------------------|-------
$$w_p$$              | Patch wave speed
$$\phi_p$$           | Patch face flux
$$\psi_p$$           | Patch compressibility
$$\vec{S}_f$$        | Patch face area vector
$$\gamma$$           | Ratio of specific heats

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            waveTransmissive;
        gamma           <scalar>;

        // Optional entries
        psi             <word>;

        // Inherited entries
        ...
    }

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `waveTransmissive`              | word    | yes      | -
`gamma`                | Ratio of specific heats                    | scalar  | yes      | -
`psi`                  | Name of compressibility field              | word    | no       | thermo:psi

The inherited entries are elaborated in:

- [advective](ref_id//boundary-conditions-advective)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/sonicFoam/RAS/prism" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/waveTransmissive" %>

API:

- [Foam::waveTransmissiveFvPatchField](doxy_id://Foam::waveTransmissiveFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
