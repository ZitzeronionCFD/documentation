---
title: Langtry-Menter k-ω Shear Stress Transport (SST)
short_title: k-ω-SST-LM
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-komegasstlm
license: CC-BY-NC-ND-4.0
menu_parent: ras-linearevm
group: turbulence-ras-evm
---

<%= page_toc %>

## Properties {#properties}

- Based on the <%= link_to_menuid "ras-komegasst" %> model
- Transition from laminar to turbulence
- Four-equation model
- Also known as the $$ \gamma - Re_\theta $$ model

## Model equations {#model-equations}

## Default model coefficients {#default-coeffs}

## Initialisation {#initialisation}

See [kOmegaSST initialisation](<%= ref_id "ras-komegasst", "initialisation" %>)

## Boundary conditions {#bcs}

Inlet

- Intermittency:

$$
    \gamma = 1
$$

- Transition momentum thickness Reynolds number:

$$
    Re_\theta =
    \begin{cases}
        1173.51 - 589.428 Tu + \frac{0.2196}{Tu^2} & \text{if } Tu \le 1.3 \\
        \frac{331.5}{(Tu - 0.5658)^{0.671}} & \text{if } Tu > 1.3
    \end{cases}
$$

Where:

$$
    Tu = 100 \frac{\sqrt{2/3 k}}{|\u_\infty|}
$$

Walls

- Intermittency: [zero gradient](ref_id://boundary-conditions-zeroGradient)

$$
    \frac{\partial}{\partial n} \gamma = 0
$$

- Transition momentum thickness Reynolds number: [zero gradient](ref_id://boundary-conditions-zeroGradient)

$$
    \frac{\partial}{\partial n} Re_\theta = 0
$$

## Usage {#usage}

The model is specified using:

    RAS
    {
        turbulence      on;
        RASModel        kOmegaSSTLM;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "kOmegaSSTLM.H", "kOmegaSSTLM" %>

## Further information {#further-information}

Source code:

- [Foam::RASModels::kOmegaSSTLM](doxy_id://Foam::RASModels::kOmegaSSTLM)

References

- *Langtry and Menter* <%= cite "langtry_correlation-based_2009" %>
- *Menter et al.* <%= cite "menter_transition_2006" %>
- *Langtry* <%= cite "langtry_correlation-based_2006" %>
