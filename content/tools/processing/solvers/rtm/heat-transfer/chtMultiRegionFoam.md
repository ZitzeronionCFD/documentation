---
title: chtMultiRegionFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- heat transfer
- solvers
menu_id: chtMultiRegionFoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-heat-transfer
group: solvers-heat-transfer
solver_models:
- transient
- compressible
- turbulence
- fvOption
- heat-transfer
- buoyancy
- multi-region
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-heat-transfer" %>
- transient
- multiple compressible fluid and solid regions
- <%= link_to_menuid "models-turbulence" %>
- <%= link_to_menuid "models-thermophysical" %>
- <%= link_to_menuid "fvoptions" %>

## Equations {#equations}

- <%= link_to_menuid "pv-pimple" %>
- <%= link_to_menuid "algorithm-p-rgh" %>

## Input requirements {#input-requirements}

Mandatory fields:

Fluid fields

- p: pressure \[pa\]
- p_rgh: pressure - hydrostatic contribution \[pa\]
- U: velocity \[m/s\]
- T: temperature \[K\]

Solid Fields

- T: temperature \[K\]

### Physical models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence): `constant/turbulenceProperties`
- [heat transfer](<%= ref_id "models-thermophysical", "usage" %>): `constant/thermophysicalModels`
- [finite volume options](<%= ref_id "numerics-fvoptions", "usage" %>) : `constant/fvOptions` (optional)

### Solution controls {#input-requirements-solution-controls}

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/heatTransfer/chtMultiRegionFoam" %>

See also

- <%= link_to_menuid "laplacianfoam" %>
- <%= link_to_menuid "buoyantpimplefoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/chtMultiRegionFoam" %>
