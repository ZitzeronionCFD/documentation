---
title: Utilities
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- function object
- post-processing
- utilities
menu_id: function-objects-utilities
license: CC-BY-NC-ND-4.0
menu_parent: post-processing-function-objects
---

<%= page_toc %>

Options:

<%= insert_models "function-objects-utilities" %>

Related:

- [Source documentation](doxy_id://grpUtilitiesFunctionObjects)
