---
title: Turbulent flat plate
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- example
- turbulence
- verification
- validation
menu_id: verification-validation-turbulent-zpg
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-turbulent
group: verification-validation-turbulent
---

<%= page_toc %>

## Overview {#sec-overview}

- Solver: [simpleFoam](ref_id://simplefoam)
- 2D flat plate test case based on the reference [https://turbmodels.larc.nasa.gov/flatplate.html](https://turbmodels.larc.nasa.gov/flatplate.html)
- Zero pressure gradient
- Reynolds number based on the plate length of $$ Re_L = 5 \times 10^6$$
- Turbulence is modelled using the [kOmegaSST](ref_id://ras-komegasst)
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/turbulentFlatPlate" %>

The domain comprises a 2D rectangular slab as shown below:

![Case description](boundary-conditions-small.png)

## Mesh {#sec-mesh}

Several meshes are employed to highlight the sensitivity of the computed
skin friction coefficient to the near wall $$ y^+ $$ over the range
$$ 0.05 < y^+ < 100 $$.  All meshes are generated using
[blockMesh](ref_id://blockmesh) and employ the same number of cells
whereby the first cell height is varied by changing the expansion ratio from
the wall patch.

![Mesh](turbulent-mesh.png)

![Mesh close-up](turbulent-mesh-close-up.png)

## Boundary conditions {#sec-boundary-conditions}

### Common fields {#sec-boundary-conditions-turbulence-common}

Velocity: `U`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [fixedValue](ref_id://boundary-conditions-fixedValue)   | 69.4 m/s in x   |
| Outlet  | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Walls   | [noSlip](ref_id://boundary-conditions-noSlip)       |              |

Pressure: `p`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Outlet  | [fixedValue](ref_id://boundary-conditions-fixedValue)   | 0 Pa (static)           |
| Walls   | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |

Turbulence viscosity: `nut`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | `calculated`   |              |
| Outlet  | `calculated`   |              |
| Walls   | `nutUSpaldingWallFunction` |   |

### Turbulence fields {#sec-boundary-conditions-turbulence}

Inlet conditions are based on an inlet turbulence intensity of 0.0039%, and
turbulence viscosity ratio of 0.009.

Turbulence kinetic energy: `k`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [fixedValue](ref_id://boundary-conditions-fixedValue)   |  based on 0.039% intensity            |
| Outlet  | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Walls   | `kqRWallFunction` |           |

Turbulence specific dissipation rate: `omega`

| Patch   | Condition      | Value        |
|---------|----------------|--------------|
| Inlet   | [fixedValue](ref_id://boundary-conditions-fixedValue)   |  based on nut/nu = 0.009            |
| Outlet  | [zeroGradient](ref_id://boundary-conditions-zeroGradient) |              |
| Walls   | `omegaWallFunction` |         |

## Results {#sec-results}

The following figures show the predicted skin friction coefficient as a
function of Reynolds number, compared against the experimental data from
Weighardt <%= cite "wieghardt_turbulent_1951" %>

![Skin friction: low y-plus](komegasst-low-400x400.png)

![Skin friction: high y-plus](komegasst-high-400x400.png)
