---
title: atmPlantCanopyTurbSource
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- atmospheric
- fvOption
- source
- turbulence
menu_id: fvoption-atmplantcanopyturbsource
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-atm
---

<%= page_toc %>

## Properties {#properties}

- The `atmPlantCanopyTurbSource` applies sources on either `epsilon` or
`omega` to incorporate effects of plant canopy for atmospheric boundary
  layer modelling.
- The `atmPlantCanopyTurbSource` can be applied on `epsilon` or
  `omega` based RAS turbulence models.
- The `atmPlantCanopyTurbSource` inherits the traits of the
  [fvOption](ref_id://fvoptions), and `cellSetOption`.

Corrections applied to either of the below, if exist:

`epsilon`
: Turbulent kinetic energy dissipation rate \[m$$^2$$/s$$^3$$\]

`omega`
: Specific dissipation rate \[1/s\]

Required fields:

`epsilon`/`omega`
: Dissipation rate OR Spec. dissipation rate \[m$$^2$$/s$$^3$$\]/\[1/s\]

`plantCd`
: Plant canopy drag coefficient \[-\]

`leafAreaDensity`
: Leaf area density \[1/m\]

## Model equations {#model-equations}

### Turbulent kinetic energy dissipation rate

The model expression for `epsilon`:

$$
    S_p -= \alpha \rho \left( C_1 - C_2 \right) C_{canopy} \epsilon
$$

with (<%= cite "sogachev_atm_2006" %>, Eq. 42):

$$
    C_{canopy} = 12.0 \sqrt{C_\mu} C_d {LAD} |\u_o|
$$

### Specific dissipation rate

The model expression for `omega`:

$$
    S_p -= \alpha \rho \left( \gamma - \beta \right) C_{canopy} \omega
$$

Where:

$$S_p$$
: Source term without boundary conditions

$$C_1$$
: Model constant (epsilon-based models) \[-\]

$$C_2$$
: Model constant (epsilon-based models) \[-\]

$$C_\mu$$
: Empirical model constant \[-\]

$$\beta$$
: Model constant (omega-based models) \[-\]

$$\gamma$$
: Model constant (omega-based models) \[-\]

$$\epsilon$$
: Turbulent kinetic energy dissipation rate (Current iteration) \[m2/s3\]

$$\omega$$
: Specific dissipation rate (Current iteration) \[1/s\]

$$C_d$$
: Plant canopy drag coefficient \[-\]

$${LAD}$$
: Leaf area density \[1/m\]

$$\u_o$$
: Previous-iteration velocity field \[m/s\]

$$C_{canopy}$$
: Plant canopy term

$$\alpha$$
: Phase fraction in multiphase computations, otherwise equals to 1

$$\rho$$
: Fluid density in compressible computations, otherwise equals to 1

## Usage {#usage}

Example of the fvOptions specification using `constant/fvOptions` file:

    atmPlantCanopyTurbSource1
    {
        // Mandatory entries (unmodifiable)
        type                  atmPlantCanopyTurbSource;

        atmPlantCanopyTurbSourceCoeffs
        {
            // Mandatory (inherited) entries (unmodifiable)
            selectionMode    all;

            // Optional entries (unmodifiable)
            rho             rho;
        }

        // Optional (inherited) entries
        ...
    }

<% assert :string_exists, "atmPlantCanopyTurbSource.H", "atmPlantCanopyTurbSource" %>
<% assert :string_exists, "atmPlantCanopyTurbSource.C", "rho" %>
<% assert :string_exists, "cellSetOption.C", "selectionMode" %>
<% assert :string_exists, "cellSetOption.C", "cellZone" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: atmPlantCanopyTurbSource | word | yes      | -
 rho          | Name of density field               | word | no       | rho

The inherited entries are elaborated in:

- [fvOption](ref_id://fvoptions)
  - `cellSetOption`

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmFlatTerrain" %>
- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmForestStability" %>

Source code

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/fvOptions/atmPlantCanopyTurbSource" %>

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
