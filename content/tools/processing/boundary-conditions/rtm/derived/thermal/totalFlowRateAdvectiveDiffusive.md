---
title: totalFlowRateAdvectiveDiffusive
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-totalFlowRateAdvectiveDiffusive
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `totalFlowRateAdvectiveDiffusive` is a boundary condition that provides
an inlet for species. The diffusion and advection fluxes are
considered to calculate the inlet value for the species.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            totalFlowRateAdvectiveDiffusive;

        // Optional entries
        phi             <word>;
        rho             <word>;
        massFluxFraction <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "totalFlowRateAdvectiveDiffusiveFvPatchScalarField.H", "totalFlowRateAdvectiveDiffusive" %>
<% assert :string_exists, "totalFlowRateAdvectiveDiffusiveFvPatchScalarField.C", "phi" %>
<% assert :string_exists, "totalFlowRateAdvectiveDiffusiveFvPatchScalarField.C", "rho" %>
<% assert :string_exists, "totalFlowRateAdvectiveDiffusiveFvPatchScalarField.C", "massFluxFraction" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `totalFlowRateAdvectiveDiffusive` | word    | yes      | -
`phi`                  | Name of flux field                         | word    | no       | phi
`rho`                  | Name of density field                      | word    | no       | rho
`massFluxFraction`     | Mass flux fraction                         | scalar  | no       | 1.0

The inherited entries are elaborated in:

- mixedFvPatchField.H
<!-- end of the list -->

- The `massFluxFraction` sets the fraction of the flux of each particular
  species.
  {: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/fireFoam/LES/simplePMMApanel" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/totalFlowRateAdvectiveDiffusive" %>

API:

- [Foam::totalFlowRateAdvectiveDiffusiveFvPatchScalarField](doxy_id://Foam::totalFlowRateAdvectiveDiffusiveFvPatchScalarField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
