---
title: writeFile
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- write
- file
menu_id: function-objects-field-writefile
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

Base class for writing single files from the function objects.

## Usage {#usage}

    <userDefinedSubDictName1>
    {
        // Mandatory and other optional entries
        ...

        // Optional (inherited) entries (runtime modifiable)
        writePrecision    8;
        writeToFile       true;
        useUserTime       true;
    }

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 writePrecision  | Number of decimal points | label | no   | \<system dflt\>
 writeToFile     | Flag to produce text file output | bool | no    | true
 useUserTime | Flag to use user time, e.g. degrees  | bool | no    | true

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/db/functionObjects/writeFile" %>

API:

- [Foam::functionObjects::writeFile](doxy_id://Foam::functionObjects::writeFile)

<!----------------------------------------------------------------------------->
