---
title: sprayFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- heat transfer
- lagrangian
- solvers
menu_id: sprayfoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-lagrangian-particle
group: solvers-lagrangian-particle
solver_models:
- transient
- compressible
- particle
- heat-transfer
- buoyancy
- chemistry
- combustion
- turbulence
- fvOption
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-lagrangian-particle" %>
- transient
- compressible
- lagrangian particles: spray
- <%= link_to_menuid "models-turbulence" %>
- <%= link_to_menuid "fvoptions" %>
- chemistry
- combustion

## Equations {#equations}

- <%= link_to_menuid "pv-pimple" %>
- [transonic option](<%= ref_id "solvers-compressible", "common-options-transonic" %>)

## Input requirements {#input-requirements}

Mandatory fields:

- p: pressure \[pa\]
- U: velocity \[m/s\]
- T: temperature \[K\]
- specie fields, e.g. N2, O2 depending on choice of thermophysical modelling

### Physical models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence): `constant/turbulenceProperties`
- [heat transfer](<%= ref_id "models-thermophysical", "usage" %>): `constant/thermophysicalModels`
- spray particles: `constant/sprayCloudProperties`
- chemistry: `constant/chemistryProperties`
- combustion: `constant/combustionProperties`
- [finite volume options](<%= ref_id "numerics-fvoptions", "usage" %>) : `constant/fvOptions` (optional)

### Solution controls {#input-requirements-solution-controls}

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/lagrangian/sprayFoam" %>

See also

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/sprayFoam" %>
