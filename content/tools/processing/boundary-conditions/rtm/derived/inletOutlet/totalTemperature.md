---
title: totalTemperature
copyright:
- Copyright (C) 2017-2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-totalTemperature
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-outlet-temperature
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `totalTemperature` is a fixed-value boundary condition that sets
the static temperature from a definition of the total temperature.

The condition sets the temperature at the patch $$ T_p $$ based on a
specification of the total temperature, $$ T_0 $$:

$$
    T_p = \frac{T_0}{1 + \frac{\gamma - 1}{2 \gamma} \psi \mag{\u}^2}
$$

where:

Property             | Description
---------------------|-------
$$T_p$$              | Temperature at the patch
$$T_0$$              | Total temperature
$$\gamma$$           | Heat capacity ratio
$$\psi$$             | Compressibility
$$\u$$               | Velocity

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            totalTemperature;
        gamma           <scalar>;
        T0              <scalarField>;

        // Optional entries
        U               <word>;
        phi             <word>;
        psi             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists,
   "totalTemperatureFvPatchScalarField.H", "totalTemperature" %>
<% assert :string_exists, "totalTemperatureFvPatchScalarField.C", "gamma" %>
<% assert :string_exists, "totalTemperatureFvPatchScalarField.C", "T0" %>
<% assert :string_exists, "totalTemperatureFvPatchScalarField.C", "U" %>
<% assert :string_exists, "totalTemperatureFvPatchScalarField.C", "phi" %>
<% assert :string_exists, "totalTemperatureFvPatchScalarField.C", "psi" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `totalTemperature`              | word    | yes      | -
`gamma`        | Heat capacity ratio                        | scalar  | yes      | -
`T0`           | Total temperature field                    | scalarField  | yes | -
`U`            | Name of velocity field                     | word    | no       | U
`phi`          | Name of flux field                         | word    | no       | phi
`psi`          | Name of compressibility field              | word    | no       | none

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/totalTemperature" %>

API:

- [Foam::totalTemperatureFvPatchScalarField](doxy_id://Foam::totalTemperatureFvPatchScalarField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
