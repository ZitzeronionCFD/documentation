---
title: atmEpsilonWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-atmEpsilonWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

The `atmEpsilonWallFunction` is a boundary condition that provides
a wall constraint on the turbulent kinetic
energy dissipation rate (i.e. `epsilon`) and the turbulent kinetic energy
production contribution (i.e. `G`) for atmospheric boundary layer
modelling.

### References

- Theoretical expressions (tags:PGVB, RH):

    Parente, A., Gorlé, C., Van Beeck, J., & Benocci, C. (2011).
    Improved k–ε model and wall function formulation
    for the RANS simulation of ABL flows.
    J. of wind engineering and industrial aerodynamics, 99(4), 267-278.
    DOI:10.1016/j.jweia.2010.12.017

    Richards, P. J., & Hoxey, R. P. (1993).
    Appropriate boundary conditions for computational wind
    engineering models using the k-ε turbulence model.
    In Computational Wind Engineering 1 (pp. 145-153).
    DOI:10.1016/B978-0-444-81688-7.50018-8

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            atmEpsilonWallFunction;
        z0              <PatchFunction1<scalar>>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "atmEpsilonWallFunctionFvPatchScalarField.H", "atmEpsilonWallFunction" %>
<% assert :string_exists, "atmEpsilonWallFunctionFvPatchScalarField.C", "z0" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `atmEpsilonWallFunction`        | word    | yes      | -
`z0`                   | Surface roughness length \[m\]             | PatchFunction1\<scalar\> | yes | -

The inherited entries are elaborated in:

- epsilonWallFunctionFvPatchScalarField.H
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmFlatTerrain" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/wallFunctions/atmEpsilonWallFunction" %>

API:

- [Foam::atmEpsilonWallFunctionFvPatchScalarField](doxy_id://Foam::atmEpsilonWallFunctionFvPatchScalarField)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
