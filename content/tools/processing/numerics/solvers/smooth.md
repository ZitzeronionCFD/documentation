---
title: Smooth solvers
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- smooth
- solvers
menu_id: linearequationsolvers-smooth
license: CC-BY-NC-ND-4.0
menu_parent: numerics-linearequationsolvers
---

<%= page_toc %>

## Options {#options}

<%= insert_models "solvers-smooth" %>

## Smoothers {#smoothers}

<%= insert_models "solvers-smooth-smoothers" %>
- nonBlockingGaussSeidelSmoother: Variant of gaussSeidelSmoother that expects
  processor boundary cells to be sorted last and so can block later
