---
title: Cube-root volume
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- delta
- LES
- turbulence
menu_id: les-delta-cuberootvol
license: CC-BY-NC-ND-4.0
#menu_parent: turbulence-les
group: turbulence-les-delta
---

<%= page_toc %>

## Properties {#properties}

- Good representation for isotropic hexahedral meshes
- Less valid when the cell aspect ratio is not 1

## Model equations {#model-equations}

The length scale is given by:

$$
    \Delta = c \left( V_c \right)^{\frac{1}{3}}
$$

where `c` is a model coefficient.

## Usage {#usage}

The model is specified using:

    delta           cubeRootVol;

    cubeRootVolCoeffs
    {
        // Optional entries

        // Multiplier
        deltaCoeff      1;
    }

## Further information {#further-information}

Source code:

- [Foam::LESModels::cubeRootVolDelta](doxy_id://Foam::LESModels::cubeRootVolDelta)
