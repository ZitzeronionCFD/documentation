---
title: scaledFixedValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-scaledFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `scaledFixedValue` is a boundary condition that applies a scalar multiplier
to the value of another boundary condition.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            scaledFixedValue;
        scale           <PatchFunction1<scalar>>;
        refValue        <fvPatchField<Type>>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "scaledFixedValueFvPatchField.H", "scaledFixedValue" %>
<% assert :string_exists, "scaledFixedValueFvPatchField.C", "scale" %>
<% assert :string_exists, "scaledFixedValueFvPatchField.C", "refValue" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `scaledFixedValue` | word    | yes      | -
`scale`            | Scalar scale factor       | PatchFunction1\<scalar\> | yes  | -
`refValue`         | Condition to supply the reference value  | fvPatchField\<Type\> | yes | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::PatchFunction1](doxy_id://Foam::PatchFunction1)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/scaledFixedValue" %>

API:

- [Foam::scaledFixedValueFvPatchField](doxy_id://Foam::scaledFixedValueFvPatchField)

<%= history "v1906" %>

<!----------------------------------------------------------------------------->
