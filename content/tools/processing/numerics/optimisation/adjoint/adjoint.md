---
title: Adjoint
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- adjoint
- optimisation
menu_id: adjoint
license: CC-BY-NC-ND-4.0
menu_parent: numerics-optimisation
publish: false
---

<%= page_toc %>

## Overview {#overview}

## How does it work? {#how-does-it-work}

## Usage {#usage}

`optimisationDict`

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/optimisation/adjointOptimisation/adjoint" %>
- <%= repo_link2 "$FOAM_APP/solvers/incompressible/adjointOptimisationFoam" %>

<%= history "v1906" %>
