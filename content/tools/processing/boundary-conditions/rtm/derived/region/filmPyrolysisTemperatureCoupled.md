---
title: filmPyrolysisTemperatureCoupled
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-filmPyrolysisTemperatureCoupled
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-region
group: bcs-region
---

<%= page_toc %>

## Description

The `filmPyrolysisTemperatureCoupled` is boundary condition
designed to be used in conjunction with surface
film and pyrolysis modelling.  It provides a temperature boundary condition
for patches on the primary region based on whether the patch is seen to
be 'wet', retrieved from the film alpha field.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            filmPyrolysisTemperatureCoupled;

        // Optional entries
        filmRegion      <word>;
        pyrolysisRegion  <word>;
        phi             <word>;
        rho             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "filmPyrolysisTemperatureCoupledFvPatchScalarField.H", "filmPyrolysisTemperatureCoupled" %>
<% assert :string_exists, "filmPyrolysisTemperatureCoupledFvPatchScalarField.C", "pyrolysisRegion" %>
<% assert :string_exists, "filmPyrolysisTemperatureCoupledFvPatchScalarField.C", "filmRegion" %>
<% assert :string_exists, "filmPyrolysisTemperatureCoupledFvPatchScalarField.C", "phi" %>
<% assert :string_exists, "filmPyrolysisTemperatureCoupledFvPatchScalarField.C", "rho" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `filmPyrolysisTemperatureCoupled` | word  | yes      | -
`pyrolysisRegion` | Name of pyrolysis region                | word    | no       | pyrolysisProperties
`filmRegion`   | Name of film region                        | word    | no       | surfaceFilmProperties
`phi`          | Name of flux region                        | word    | no       | phi
`rho`          | Name of density region                     | word    | no       | rho

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/regionModels/regionCoupling/derivedFvPatchFields/filmPyrolysisTemperatureCoupled" %>

API:

- [Foam::filmPyrolysisTemperatureCoupledFvPatchScalarField](doxy_id://Foam::filmPyrolysisTemperatureCoupledFvPatchScalarField)

<%= history "2.0.0" %>

<!----------------------------------------------------------------------------->
