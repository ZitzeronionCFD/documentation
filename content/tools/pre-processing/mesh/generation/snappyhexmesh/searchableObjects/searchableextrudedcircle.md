---
title: searchableExtrudedCircle
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: searchableextrudedcircle
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh-geometry
group: searchableobjects
---

<%= page_toc %>

![extruded circle example](../images/extruded-circle-small.png)

A shape defined by inflating a segmented line into a tube with constant radius.

Not applicable to snappyHexMesh: only 'near' queries are implemented, i.e. no
testing for intersections or inside/outside.
{: .note}

## Usage

    cylinder2
    {
        type            searchableExtrudedCircle;
        file            "curve2.vtk";
        radius          0.5;
    }

<% assert :string_exists, "searchableExtrudedCircle.H", "searchableExtrudedCircle" %>
<% assert :string_exists, "searchableExtrudedCircle.C", "file" %>
<% assert :string_exists, "searchableExtrudedCircle.C", "radius" %>

## Further information

Tutorial

- <%= repo_link2 "$FOAM_TUTORIALS/mesh/blockMesh/pipe/system/blockMeshDict}
