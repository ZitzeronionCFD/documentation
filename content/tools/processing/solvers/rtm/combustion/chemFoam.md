---
title: chemFoam
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- combustion
- heat transfer
- solvers
menu_id: chemfoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-combustion
group: solvers-combustion
solver_models:
- transient
- heat-transfer
- chemistry
- combustion
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-combustion" %>
- transient
- heat transfer
- chemistry
- combustion

## Equations {#equations}

## Input requirements {#input-requirements}

Mandatory fields:

- specie fields, e.g. N2, O2 depending on choice of thermophysical modelling

### Physical models {#input-requirements-physical-models}

- [heat transfer](ref_id://models-thermophysical#usage):
  `constant/thermophysicalModels`
- chemistry: `constant/chemistryProperties`
- combustion: `constant/combustionProperties`

### Solution controls {#input-requirements-solution-controls}

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/combustion/chemFoam" %>

See also

- <%= link_to_menuid "reactingfoam" %>

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/chemFoam" %>
