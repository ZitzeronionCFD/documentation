---
title: Dynamic k equation
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- LES
- turbulence
menu_id: les-dynamickeqn
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-les
group: turbulence-les
---

<%= page_toc %>

## Properties {#properties}

## Model equations {#model-equations}

The turbulence kinetic energy equation is given by:

$$
    \Ddt{\rho k}
    =
    \div \left( \rho D_k \grad k \right)
    + \rho G
    - \frac{2}{3} \rho k \div \u
    - \frac{C_e \rho k^{1.5}}{\Delta}
    + S_k
$$

The $$ C_e $$ and $$ C_k $$ coefficients are derived from local flow
properties.

## Default model coefficients {#default-coeffs}

None

## Initialisation {#initialisation}

## Usage {#usage}

The model is specified using:

    LES
    {
        turbulence      on;
        LESModel        dynamicKEqn;
    }

<% assert :string_exists, "LESModel.C", "LES" %>
<% assert :string_exists, "LESModel.C", "LESModel" %>
<% assert :string_exists, "dynamicKEqn.H", "dynamicKEqn" %>

## Boundary conditions {#bcs}

Inlet

- <%= link_to_menuid "boundary-conditions-fixedValue" %>

Outlet

- [Constrained outlet](ref_id://boundary-conditions-inletOutlet)

Walls

- wall functions

## Further information {#further-information}

Source code:

- [Foam::LESModels::dynamicKEqn](doxy_id://Foam::LESModels::dynamicKEqn)

References:

- *Kim and Menon* <%= cite "kim_new_1995" %>
