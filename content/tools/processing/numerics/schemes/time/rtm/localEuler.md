---
title: Local Euler implicit/explicit time scheme
short_title: Local Euler
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- time
menu_id: schemes-time-localeuler
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes
group: schemes-time
---

<%= page_toc %>

## Properties {#properties}

- First order
- Pseudo transient, designed for steady cases
- Spatially varying, cell-based time scale set by specific Local Time Stepping (LTS) solvers

## Usage {#usage}

The scheme is specified using:

    ddtSchemes
    {
        default         localEuler;
        ddt(phi)        localEuler;
    }

## Further information {#further-information}

Source code

- [Foam::fv::localEulerDdtScheme](doxy_id://Foam::fv::localEulerDdtScheme)
