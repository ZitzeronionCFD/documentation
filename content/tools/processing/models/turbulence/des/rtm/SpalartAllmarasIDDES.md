---
title: Spalart-Allmaras Improved Delayed Detached Eddy Simulation (IDDES)
short_title: SpalartAllmarasIDDES
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- DES
- IDDES
- turbulence
menu_id: des-spalartallmarasiddes
license: CC-BY-NC-ND-4.0
menu_parent: turbulence-des
group: turbulence-des
---

<%= page_toc %>

## Properties {#properties}

- One equation model based on a modified turbulence viscosity,
  $$ \tilde{\nu} $$

## Model equations {#model-equations}

The model equations are the same as used by the
[DES variant](<%= ref_id "des-spalartallmarasdes", "model-equations" %>)
of the model, with a different approximation for $$ \tilde{d} $$:

$$
    \tilde{d} = \max \left( \tilde{f_d} (1 + f_e) L_{RAS} + (1 - \tilde{f_d}) L_{LES}, SMALL \right)
$$

## Default model coefficients {#default-coeffs}

## Initialisation {#initialisation}

## Usage {#usage}

The model is specified using:

    LES
    {
        turbulence      on;
        LESModel        SpalartAllmarasIDDES;
    }

<% assert :string_exists, "LESModel.C", "LES" %>
<% assert :string_exists, "LESModel.C", "LESModel" %>
<% assert :string_exists, "SpalartAllmarasIDDES.H", "SpalartAllmarasIDDES" %>

## Boundary conditions {#bcs}

Inlet

- <%= link_to_menuid "boundary-conditions-fixedValue" %>

Outlet

- [Constrained outlet](ref_id://boundary-conditions-inletOutlet)

Walls

- wall functions

## Further information {#further-information}

Source code:

- [Foam::LESModels::SpalartAllmarasIDDES](doxy_id://Foam::LESModels::SpalartAllmarasIDDES)

References:

- *Gritskevich et al.* <%= cite "gritskevich_development_2012" %>
