---
title: syringePressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-syringePressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `syringePressure` is a boundary condition
that provides a pressure condition, obtained from a
zero-D model of the cylinder of a syringe.

The syringe cylinder is defined by its initial volume, piston area and
velocity profile specified by regions of constant acceleration, speed
and deceleration.  The gas in the cylinder is described by its initial
pressure and compressibility which is assumed constant, i.e. isothermal
expansion/compression.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            syringePressure;
        Ap              <scalar>;
        Sp              <scalar>;
        VsI             <scalar>;
        tas             <scalar>;
        tae             <scalar>;
        tds             <scalar>;
        tde             <scalar>;
        psI             <scalar>;
        psi             <scalar>;
        ams             <scalar>;

        // Optional entries
        phi             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "syringePressureFvPatchScalarField.H", "syringePressure" %>
<% assert :string_exists, "syringePressureFvPatchScalarField.C", "Ap" %>
<% assert :string_exists, "syringePressureFvPatchScalarField.C", "Sp" %>
<% assert :string_exists, "syringePressureFvPatchScalarField.C", "VsI" %>
<% assert :string_exists, "syringePressureFvPatchScalarField.C", "tas" %>
<% assert :string_exists, "syringePressureFvPatchScalarField.C", "tae" %>
<% assert :string_exists, "syringePressureFvPatchScalarField.C", "tds" %>
<% assert :string_exists, "syringePressureFvPatchScalarField.C", "tde" %>
<% assert :string_exists, "syringePressureFvPatchScalarField.C", "psI" %>
<% assert :string_exists, "syringePressureFvPatchScalarField.C", "psi" %>
<% assert :string_exists, "syringePressureFvPatchScalarField.C", "ams" %>

where:

Property | Description                         | Type    | Required | Default
---------|-------------------------------------|---------|----------|---------
`type`   | Type name: `syringePressure`        | word    | yes      | -
`Ap`     | Syringe piston area \[m^2\]         | scalar  | yes      | -
`Sp`     | Syringe piston speed \[m/s\]        | scalar  | yes      | -
`VsI`    | Initial syringe volume \[m^3\]      | scalar  | yes      | -
`tas`    | Start of piston acceleration \[s\]  | scalar  | yes      | -
`tae`    | End of piston acceleration \[s\]    | scalar  | yes      | -
`tds`    | Start of piston deceleration \[s\]  | scalar  | yes      | -
`tde`    | End of piston deceleration \[s\]    | scalar  | yes      | -
`psI`    | Initial syringe pressure \[Pa\]     | scalar  | yes      | -
`psi`    | Gas compressibility \[m^2/s^2\]     | scalar  | yes      | -
`ams`    | Added (or removed) gas mass \[kg\]  | scalar  | yes      | -
`phi`    | Name of flux field                  | word    | no       | phi

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/syringePressure" %>

API:

- [Foam::syringePressureFvPatchScalarField](doxy_id://Foam::syringePressureFvPatchScalarField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
