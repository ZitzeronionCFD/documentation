---
title: uniformDensityHydrostaticPressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-uniformDensityHydrostaticPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `uniformDensityHydrostaticPressure` is a boundary condition that
provides a hydrostatic pressure condition, calculated as:

$$
    p_{hyd} = p_{ref} + \rho g (x - x_{ref})
$$

where:

Property             | Description
---------------------|-------
$$p_{hyd}$$          | Hydrostatic pressure \[Pa\]
$$p_{ref}$$          | Reference pressure \[Pa\]
$$x_{ref}$$          | Reference point in Cartesian coordinates
$$\rho$$             | Density (assumed uniform)
$$g$$                | Acceleration due to gravity \[m/s^2\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            uniformDensityHydrostaticPressure;
        rho             <scalar>;
        pRefValue       <scalar>;
        pRefPoint       <vector>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "uniformDensityHydrostaticPressureFvPatchScalarField.H", "uniformDensityHydrostaticPressure" %>
<% assert :string_exists, "uniformDensityHydrostaticPressureFvPatchScalarField.C", "rho" %>
<% assert :string_exists, "uniformDensityHydrostaticPressureFvPatchScalarField.C", "pRefValue" %>
<% assert :string_exists, "uniformDensityHydrostaticPressureFvPatchScalarField.C", "pRefPoint" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `uniformDensityHydrostaticPressure` | word    | yes      | -
`rho`              | Value of density          | scalar  | yes      | -
`pRefValue`        | Reference pressure \[Pa\] | scalar  | yes      | -
`pRefPoint`        | Reference pressure location | vector  | yes    | -

The inherited entries are elaborated in:

- [fixedValueFvPatchField.H](ref_id://boundary-conditions-fixedValue)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/uniformDensityHydrostaticPressure" %>

API:

- [Foam::uniformDensityHydrostaticPressureFvPatchScalarField](doxy_id://Foam::uniformDensityHydrostaticPressureFvPatchScalarField)

<%= history "1.6" %>

<!----------------------------------------------------------------------------->
