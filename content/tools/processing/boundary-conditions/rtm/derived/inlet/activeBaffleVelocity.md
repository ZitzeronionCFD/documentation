---
title: activeBaffleVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-activeBaffleVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `activeBaffleVelocity` is a velocity boundary condition that simulates the
opening of a baffle due to local flow conditions, by merging the behaviours of
wall and cyclic conditions.  The baffle joins two mesh regions, where the open
fraction determines the interpolation weights applied to each cyclic- and
neighbour-patch contribution.

## Usage {#usage}

The condition requires entries in both the `boundary` and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            activeBaffleVelocity;
        cyclicPatch     <word>;
        orientation     <label>;
        openFraction    <scalar>;
        openingTime     <scalar>;
        maxOpenFractionDelta <scalar>;

        // Optional entries
        p               <word>;

        // Inherited entries
        ...
    }

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `activeBaffleVelocity`          | word  | yes      | -
`cyclicPatch`          | Name of cyclic patch used when the active baffle is open | word    | yes      | -
`orientation`          | Orientation (1 or -1) of the active baffle patch   | label    | yes      | -
`openFraction`         | Current fraction of the active baffle which is open | scalar    | yes      | -
`openingTime`          | Time taken for the active baffle to open | scalar    | yes      | -
`maxOpenFractionDelta` | Maximum fractional change to the active baffle openness per time step | scalar    | yes      | -
`p`                    | Name of pressure field                     | word   | no         | p

The inherited entries are elaborated in:

- fixedValueFvPatchField.H

- The `orientation` is used to change the direction of opening without the need for
reordering the patch faces.
{: .note}

## Method

It is determined whether the baffle is opening or closing from the sign of
the net force across the baffle, from which the baffle open fraction is
updated using:

$$
  x = x_{old} + sign(F_{net})\frac{dt}{DT}
$$

where:

Property             | Description
---------------------|-------
$$x$$                | Baffle open fraction \[0-1\]
$$x_{old}$$          | Baffle open fraction on previous evaluation
$$dt$$               | Simulation time step
$$DT$$               | Time taken to open the baffle
$$F_{net}$$          | Net force across the baffle

The open fraction is then applied to scale the patch areas.

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/activeBaffleVelocity" %>

API:

- [Foam::activeBaffleVelocityFvPatchVectorField](doxy_id://Foam::activeBaffleVelocityFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
