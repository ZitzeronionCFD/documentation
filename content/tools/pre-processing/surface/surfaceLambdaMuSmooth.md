---
title: surfaceLambdaMuSmooth
tags:
- smoothing
- surface
copyright:
- Copyright 2022 (C) OpenCFD Ltd.
menu_id: surfaceLambdaMuSmooth
license: CC-BY-NC-ND-4.0
menu_parent: pre-processing-surface
group: pre-processing-surface
---

<%= page_toc %>

## Overview {#overview}

The `surfaceLambdaMuSmooth` utility smooths a given surface by moving point
positions based on the procedure described by Taubin
<%= cite "taubin_signal_1995" %>.

## Usage

### Synopsis

<%= help_util_usage "surfaceLambdaMuSmooth" %>

#### Examples

~~~
surfaceLambdaMuSmooth surface.stl 0.5 0.5 1 output.stl
~~~

### Input

#### Arguments

<%= help_util_arguments "surfaceLambdaMuSmooth" %>

#### Options

<%= help_util_options "surfaceInertia" %>

#### Files

<%= no_input "file" %>

#### Fields

<%= no_input "field" %>


### Output

#### Logs

Property    | Description                       | Type
------------|-----------------------------------|---------
`Faces`     | Number of faces on the surface    | label
`Vertices`  | Number of vertices on the surface | label
`Bounding Box` | Bounding box of the surface    | min/max vector

#### Files

Property       | Description                 | Path      | Type
---------------|-----------------------------|-----------|-----------
`featureFile`  | New surface file            | `<case>/` | input type

#### Fields

<%= no_output "field" %>

## Method

The discrete Laplacian is given by

$$
\Delta x_i = \sum_{j \in i^{*}} w_{ij}(x_j - x_i)
$$

The weight $$w_{ij}$$ is set to the inverse of the number of points surrounding
the point $$i$$, i.e.

$$
w_{ij} = \frac{1}{i^{*}}
$$

Gaussian filtering is performed iteratively as

$$
x_i^{'} = x_i + \lambda \Delta x_i
$$

$$
x_i^{'} = (1 - \lambda) x_i + \lambda \Delta x_i
$$

$$
x_i^{'} = x_i + \mu \Delta x_i
$$

$$
x_i^{'} = (1 + \mu) x_i - \mu \Delta x_i
$$

For Laplacian smoothing, the $$\mu$$ coefficient should be set to zero.
{: .note}

The procedure will move all points by default, and therefore will degrade
features points and lines. To preserve features, the set of points to fix can
be supplied.

Provide an `edgeMesh` file containing points that are not to be moved during
smoothing in order to preserve features.

## Further information {#further-information}

Tutorial:

- *None*

Source code:

- <%= repo_link2 "$FOAM_UTILITIES/surface/surfaceLambdaMuSmooth" %>

<%= history "2.2.1" %>

<!----------------------------------------------------------------------------->