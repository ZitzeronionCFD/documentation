---
title: surfaceDisplacement
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-surfaceDisplacement
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-meshMotion
group: bcs-meshMotion
---

<%= page_toc %>

## Description

The `surfaceDisplacement` is a boundary condition that provides displacement
fixed by projection onto triSurface. Use in a `displacementMotionSolver`
as a boundary condition on a `pointDisplacement` field.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            surfaceDisplacement;
        velocity        <vector>;
        geometry        <dict>;
        projectMode     <word>;
        projectDirection  <vector>;

        // Optional entries
        wedgePlane      <label>;
        frozenPointsZone  <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "surfaceDisplacementPointPatchVectorField.H", "surfaceDisplacement" %>
<% assert :string_exists, "surfaceDisplacementPointPatchVectorField.C", "velocity" %>
<% assert :string_exists, "surfaceDisplacementPointPatchVectorField.C", "geometry" %>
<% assert :string_exists, "surfaceDisplacementPointPatchVectorField.C", "projectMode" %>
<% assert :string_exists, "surfaceDisplacementPointPatchVectorField.C", "projectDirection" %>
<% assert :string_exists, "surfaceDisplacementPointPatchVectorField.C", "wedgePlane" %>
<% assert :string_exists, "surfaceDisplacementPointPatchVectorField.C", "frozenPointsZone" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `surfaceDisplacement`           | word    | yes     | -
`velocity`     | Maximum velocity                           | vector  | yes     | -
`geometry`     | Dictionary with `searchableSurfaces`. (usually `triSurfaceMeshes` in `constant/triSurface`) | word    | yes     | -
`projectMode`  | Projection mode                            | word    | yes     | -
`projectDirection` | Projection direction                   | vector  | yes     | -
`wedgePlane`   | -1 or component to knock out of intersection normal  | label | no | -1
`frozenPointsZone` | Empty or name of `pointZone` containing points that do not move  | word | no | null

Options for the `projectMode` entry:

Property              | Description
----------------------|--------------------------------------------
`NEAREST`             | Nearest
`POINTNORMAL`         | Intersection with point normal
`FIXEDNORMAL`         | Intersection with fixed vector

The inherited entries are elaborated in:

- fixedValuePointPatchField.H
<!-- end of the list -->

- This displacement is then clipped with the specified velocity*deltaT.
- Optionally (intersection only) removes a component (`wedgePlane`) to
stay in 2D.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/mesh/moveDynamicMesh/SnakeRiverCanyon" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/fvMotionSolver/pointPatchFields/derived/surfaceDisplacement" %>

API:

- [Foam::surfaceDisplacementPointPatchVectorField](doxy_id://Foam::surfaceDisplacementPointPatchVectorField)

<%= history "1.6" %>

<!----------------------------------------------------------------------------->
