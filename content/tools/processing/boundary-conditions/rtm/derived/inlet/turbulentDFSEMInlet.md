---
title: turbulentDFSEMInlet
copyright:
- Copyright (C) 2017-2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
- turbulence
menu_id: boundary-conditions-turbulentDFSEMInlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `turbulentDFSEMInlet` is a synthesised-eddy based velocity inlet
boundary condition to generate synthetic turbulence-alike time-series
from a given set of turbulence statistics for [LES](ref_id://turbulence-les)
and [DES](ref_id://turbulence-des) computations.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            turbulentDFSEMInlet;
        delta           <scalar>;
        R               <PatchFunction1>;
        U               <PatchFunction1>;
        L               <PatchFunction1>;

            // e.g.
            // R        uniform (<Rxx> <Rxy> <Rxz> <Ryy> <Ryz> <Rzz>);
            // U        uniform (<Ux> <Uy> <Uz>);
            // L        uniform <L>;

        // Optional entries
        d               <scalar>;
        nCellPerEddy    <label>;
        kappa           <scalar>;
        Uref            <scalar>;
        Lref            <scalar>;
        scale           <scalar>;
        m               <scalar>;
        writeEddies     <bool>;

        // Inherited entries
        ...
    }

<% assert :string_exists,
   "turbulentDFSEMInletFvPatchVectorField.H", "turbulentDFSEMInlet" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "delta" %>
<% assert :string_exists,
   "turbulentDFSEMInletFvPatchVectorField.C", "nCellPerEddy" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "mapMethod" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "R" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "U" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "L" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "d" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "nCellPerEddy" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "kappa" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "Uref" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "Lref" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "scale" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "m" %>
<% assert :string_exists, "turbulentDFSEMInletFvPatchVectorField.C", "writeEddies" %>

where:

Property   | Description                           | Type    | Required | Default
-----------|---------------------------------------|---------|----------|---------
`type`     | Type name: `turbulentDFSEMInlet`      | word    | yes      | -
`delta`    | Characteristic length scale           | scalar  | yes      | -
`R`        | Reynolds-stress tensor field          | PatchFunction1\<symmTensor\> | yes  | -
`U`        | Mean velocity field                   | PatchFunction1\<vector\>     | yes  | -
`L`        | Integral-length scale field           | PatchFunction1\<scalar\>     | yes  | -
`d`        | Ratio of sum of eddy volumes to eddy box volume i.e. eddy density (fill fraction) | scalar     | no   | 1.0
`nCellPerEddy` | Minimum eddy length in units of number of cells | label      | no   | 5
`kappa`    | von Karman constant                   | scalar     | no   | 0.41
`Uref`     | Normalisation factor for Reynolds-stress tensor and mean velocity          | scalar     | no   | 1.0
`Lref`     | Normalisation factor for integral-length scale | scalar     | no   | 1.0
`scale`    | Heuristic scaling factor being applied on the normalisation factor C1    | scalar     | no   | 1.0
`m`        | The power of V defined in C1          | scalar     | no   | 0.5
`writeEddies`  | Flag to write eddies as OBJ file  | bool     | no   | false

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::PatchFunction1](doxy_id://Foam::PatchFunction1)
- [Foam::PatchFunction1Types::MappedFile](doxy_id://Foam::PatchFunction1Types::MappedFile)
<!-- end of the list -->

- The `delta` value typically represents the characteristic scale of flow
  or flow domain, e.g. a channel half height for plane channel flows.
- `nCellPerEddy` and `scale` entries are heuristic entries
  which do not exist in the standard method, yet are necessary
  to reproduce the results published in the original journal paper.
- In the original journal paper, `C1` in Eq. 11 is not dimensionless.
  It is not clear whether this dimensionality issue was intentional.
  To alleviate this matter, users can alter the input entry `m`, which is
  the power of the eddy-box volume defined in the `C1`, to obtain a
  dimensionless `C1` coefficient. The default value of `m` is 0.5,
  which is the value stated in the original journal paper,
  and `m=1/3` leads to a dimensionless `C1`.
  {: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/planeChannel" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/turbulentDFSEMInlet" %>

API:

- [Foam::turbulentDFSEMInletFvPatchVectorField](doxy_id://Foam::turbulentDFSEMInletFvPatchVectorField)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
