---
title: Polynomial (enthalpy)
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- enthalpy
- polynomial
- thermodynamics
menu_id: thermo-hPolynomial
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-thermo
group: thermo-models
---

<%= page_toc %>

Heat capacity at constant pressure, $$ c_p $$ \[J/kg/K\]

$$
c_p = \sum_{i=1}^N c_i T^{i-1}
$$

## Usage

The `hPolynomial` thermodynamics model is defined using the entries:

~~~
thermodynamics
{
    Hf              <scalar>;
    Sf              <scalar>;
    CpCoeffs<8>     (<c1> <c2> <c3> <c4> <c5> <c6> <c7> <c8>);
}
~~~

Where

- `Hf` : heat of formation \[J/kg\]
- `Sf` : entropy of formation \[J/kg\]
- `CpCoeffs<8>` : Polynomial coefficients (x8)
- `Tref` : reference temperature \[K\]; default is $$ T_{\std} $$
- `Href` : reference enthalpy \[J/kg\]; default is zero
- `Sref` : reference entropy \[J/kg/K\]; default is zero
- `Pref` : reference pressure \[Pa\]; default is $$ P_{\std} $$

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/thermo/hPolynomial" %>
