---
title: thermoFoam
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- heat transfer
- solvers
menu_id: thermoFoam
license: CC-BY-NC-ND-4.0
menu_parent: solvers-heat-transfer
group: solvers-heat-transfer
solver_models:
- buoyancy
- fvOption
- frozen flow
- heat-transfer
- transient
- turbulence
---

<%= page_toc %>

## Overview {#overview}

- Category: <%= link_to_menuid "solvers-heat-transfer" %>
- Transient
- Frozen flow
- <%= link_to_menuid "models-turbulence" %>
- <%= link_to_menuid "models-thermophysical" %>
- <%= link_to_menuid "fvoptions" %>


## Usage

### Synopsis

<%= help_util_usage "thermoFoam" %>

#### Examples

~~~
thermoFoam -region region1
~~~

### Input

#### Arguments

<%= help_util_arguments "thermoFoam" %>

#### Options

<%= help_util_options "thermoFoam" %>

### Files

- <%= link_to_menuid "numerics-schemes" %>
- <%= link_to_menuid "numerics-linearequationsolvers" %>
- <%= link_to_menuid "controldict" %>

#### Fields

Property  | Description                 | Type      | Required | Default
----------|-----------------------------|-----------|----------|--------
`U`       | Velocity \[m/s\]            | volVectorField | yes | -
`T`       | Temperature \[K\]           | volScalarField | yes | -

#### Models {#input-requirements-physical-models}

- [turbulence](ref_id://models-turbulence):
  `constant/turbulenceProperties`
- [heat transfer](<%= ref_id "models-thermophysical", "usage" %>):
  `constant/thermophysicalModels`
- [finite volume options](<%= ref_id "numerics-fvoptions", "usage" %>):
  `constant/fvOptions` (optional)


## Method

The solver can employ the [SIMPLE](ref_id://pv-simple)
or the [PIMPLE](ref_id://pv-pimple) algorithm to solve the
energy transport equation on a frozen flow field:

- <%= link_to_menuid "energy-transport" %>


## Further information {#further-information}

Tutorial:

- *None*

Source code:

- <%= repo_link2 "$FOAM_SOLVERS/heatTransfer/thermoFoam" %>

<%= history "2.2.0" %>

<!----------------------------------------------------------------------------->