---
title: externalWallHeatFluxTemperature
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-externalWallHeatFluxTemperature
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `externalWallHeatFluxTemperature` is a boundary condition that
applies a heat flux condition to temperature on an external wall in
one of three modes:

- fixed power: supply Q
- fixed heat flux: supply q
- fixed heat transfer coefficient: supply h and Ta

where:

Property              | Description
----------------------|--------------------------------------------
Q                     | Power \[W\]
q                     | Heat flux \[W/m^2\]
h                     | Heat transfer coefficient \[W/m^2/K\]
Ta                    | Ambient temperature \[K\]

For heat transfer coefficient mode optional thin thermal layer resistances
can be specified through thicknessLayers and kappaLayers entries.

The thermal conductivity `kappa` can either be retrieved from various
possible sources, as detailed in the class `temperatureCoupledBase`.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            externalWallHeatFluxTemperature;
        mode            <word>;

        // Optional entries
        relaxation      <scalar>;
        emissivity      <scalar>;
        qrRelaxation    <scalar>;
        qr              <word>;

        // Conditional entries

            // if mode==power
            Q           <Function1<scalar>>;

            // if mode==flux
            q           <PatchFunction1<scalar>>;

            // if mode==coefficient
            h           <PatchFunction1<scalar>>;
            Ta          <Function1<scalar>>;
            thicknessLayers <scalarField>;
            kappaLayers <scalarField>;

            // if qr!=none
            qrPrevious  <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.H", "externalWallHeatFluxTemperature" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "mode" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "relaxation" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "emissivity" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "qrRelaxation" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "qr" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "Q" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "q" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "h" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "Ta" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "thicknessLayers" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "kappaLayers" %>
<% assert :string_exists, "externalWallHeatFluxTemperatureFvPatchScalarField.C", "qrPrevious" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `externalWallHeatFluxTemperature` | word  | yes      | -
`mode`         | Operation mode                             | word    | yes      | -
`relaxation`   | Relaxation factor for wall temperature     | scalar  | no       | 1.0
`emissivity`   | Surface emissivity for radiative flux      | scalar  | no       | 0.0
`qrRelaxation` | Relaxation factor for radiative field      | scalar  | no       | 1.0
`qr`           | Name of radiative field                    | word    | no       | none
`Q`            | Heat power \[W\]                           | Function1\<scalar\> | conditional | -
`q`            | Heat flux \[W/m^2\]                        | PatchFunction1\<scalar\> | conditional | -
`h`            | Heat transfer coeff \[W/m^2/K\]            | PatchFunction1\<scalar\> | conditional | -
`Ta`           | Ambient temperature \[K\]                  | PatchFunction1\<scalar\> | conditional | -
`qrPrevious`   | Cached field for qr relaxation             | scalarField | conditional | -

The inherited entries are elaborated in:

- mixedFvPatchFields.H
- temperatureCoupledBase.H
- PatchFunction1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/chtMultiRegionFoam/externalSolarLoad" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/externalWallHeatFluxTemperature" %>

API:

- [Foam::externalWallHeatFluxTemperatureFvPatchScalarField](doxy_id://Foam::externalWallHeatFluxTemperatureFvPatchScalarField)

<%= history "2206" %>

<!----------------------------------------------------------------------------->
