---
title: fixedShearStress
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-fixedShearStress
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wall
group: bcs-wall-general
---

<%= page_toc %>

## Description

The `fixedShearStress` is boundary condition that
sets a user-defined shear stress constant and uniform across a given
patch by using the expression:

$$
    \tau_0 = -\nu_{eff} \frac{d\u}{dn}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\tau$$              | Shear stress
$$\u$$                | Velocity
$$\nu_{eff}$$         | Effective viscosity
$$n$$                 | Wall normal direction

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            wall;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            fixedShearStress;

        // Optional entries
        tau             <vector>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "fixedShearStressFvPatchVectorField.H", "fixedShearStress" %>
<% assert :string_exists, "fixedShearStressFvPatchVectorField.C", "tau" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `fixedShearStress`              | word    | yes      | -
`tau`          | Type name: `fixedShearStress`              | vector  | no       | Zero

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmDownstreamDevelopment" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/TurbulenceModels/turbulenceModels/derivedFvPatchFields/fixedShearStress" %>

API:

- [Foam::fixedShearStressFvPatchVectorField](doxy_id://Foam::fixedShearStressFvPatchVectorField)

<%= history "2.2.2" %>

<!----------------------------------------------------------------------------->
