---
title: mappedVelocityFluxFixedValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-mappedVelocityFluxFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `mappedVelocityFluxFixedValue` is a velocity boundary condition that maps
the velocity and flux from a neighbour patch to this patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            mappedPatch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type                mappedVelocityFluxFixedValue;

        // Optional entries
        phi                 <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "mappedVelocityFluxFixedValueFvPatchField.H", "mappedVelocityFluxFixedValue" %>
<% assert :string_exists, "mappedVelocityFluxFixedValueFvPatchField.C", "phi" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `mappedVelocityFluxFixedValue`  | word    | yes      | -
`phi`                  | Name of the flux field                     | word    | no       | phi

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

- This boundary condition can only be applied to patches that are of
  the `mappedPolyPatch` type.
  {: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/mappedVelocityFluxFixedValue" %>

API:

- [Foam::mappedVelocityFluxFixedValueFvPatchField](doxy_id://Foam::mappedVelocityFluxFixedValueFvPatchField)

<%= history "2.1.0" %>

<!----------------------------------------------------------------------------->
