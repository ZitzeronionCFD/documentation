---
title: Divergence schemes
short_title: Divergence
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- divergence
menu_id: schemes-divergence
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes
---

<%= page_toc %>

The divergence of a property $$Q$$ describes the net rate at which it
changes as a function of space, represented using the notation:

$$
    \div Q
$$

If $$ Q $$ is a vector quantity, this equates to

$$
    \div \vec{Q}
    = \frac{\partial Q_x}{\partial x}
    + \frac{\partial Q_y}{\partial y}
    + \frac{\partial Q_z}{\partial z}
$$

## Usage {#usage}

Divergence schemes are specified in the <%= link_to_menuid "fvschemes" %>
file under the `divSchemes` sub-dictionary using the general syntax:

    divSchemes
    {
        default         none;
        div(Q)          Gauss <interpolation scheme>;
    }

A typical use is for convection schemes, which transport a property, $$ Q $$
under the influence of a velocity field $$\phi$$, specified using:

    divSchemes
    {
        default         none;
        div(phi,Q)      Gauss <interpolation scheme>;
    }

The `phi` keyword is typically used to represent the flux (flow) across cell
faces, i.e.

- volumetric flux: $$ \phi = \u_f \dprod \vec{S}_f $$
- mass flux: $$ \phi = \rho_f \left( \u_f \dprod \vec{S}_f \right) $$

## Options {#options}

### NVD/TVD convection schemes {#nvd-tvd}

Many of the convection schemes available in OpenFOAM are based on the TVD and NVD \[PROVIDE REF\]
For further information, see the page <%= link_to_menuid "schemes-divergence-nvdtvd "%>

<%= insert_models "schemes-divergence-nvdtvd" %>

### Non-NVD/TVD convection schemes {#non-nvd-tvd}

<%= insert_models "schemes-divergence-general" %>

## Special cases {#special-cases}

Several options are available for convection schemes for certain flow cases,
including for steady state, bounded scalar transport and limited schemes.

### Steady state {#special-cases-steady-state}

For steady state cases the `bounded` form can be applied:

    divSchemes
    {
        default         none;
        div(phi,Q)      bounded Gauss <interpolation scheme>;
    }

<% assert :string_exists, "boundedConvectionScheme.H", "bounded" %>

This adds a linearised, implicit source contribution to the transport equation
of the form

$$
    \ddt{\u}
    + \div (\u \otimes \u )
    - \color{red}{( \div \u ) \u}
    = \div ( \Gamma \grad \u )
    + S_u
$$

i.e. it removes a component proportional to the continuity error.  This acts as
a convergence aid to tend towards a bounded solution as the calculation
proceeds.  At convergence, this term becomes zero and does not contribute to
the final solution.

### Bounded scalars  {#special-cases-bounded-scalars}

The `limited<interpolation scheme>` options bound scalar field values to
user-specified limits, e.g. for the
<%= link_to_menuid "schemes-divergence-vanleer" %> scheme

    limitedVanLeer -1 3

Many fields require bounding between 0 and 1, for which the following syntax
is used:

    <interpolation scheme>01

### V schemes  {#special-cases-v-schemes}

A set of schemes for vector fields where the limiter is applied in the
direction of greatest change.

## Example {#example}

See [this example](ref_id://schemes-divergenceexample) to see the
relative performance of the schemes.

## Further information {#further-information}

See the <%= link_to_menuid "schemes-divergence-implementationdetails" %>
"implementation details" to see how the schemes are coded.
