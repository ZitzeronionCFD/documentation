---
title: Van Driest
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- delta
- LES
- turbulence
menu_id: les-delta-vandriest
license: CC-BY-NC-ND-4.0
#menu_parent: turbulence-les
group: turbulence-les-delta
---

<%= page_toc %>

## Properties {#properties}

- Van Driest damping function
- Includes additional limiting
- Applied in the region up to $$ y^+ $$ of 500

## Model equations {#model-equations}

The Van Driest damping function is given by:

$$
    D = 1 - \exp^\frac{-y^+}{A^+}
$$

The final length scale is given by:

$$
    \Delta = \min \left( \frac{\kappa y}{C_s} D , \Delta_g \right)
$$

where $$ \Delta_g $$ is a geometric-based delta function such as the
[cube-root volume](ref_id://les-delta-cuberootvol) delta.

## Usage {#usage}

The model is specified using:

    delta           vanDriest;

    vanDriestCoeffs
    {
        delta           <geometricDelta>;

        // Optional entries
        kappa           0.41;
        Aplus           26;
        Cdelta          0.158;
        calcInterval    1;
    }

The model requires the near wall distance $$y$$ which can be costly to compute
for moving mesh cases.  Here, the optional `calcInterval` entry can be used to
reduce the update frequency of the wall distance calculation at the expense of
increased modelling errors.

## Further information {#further-information}

Source code:

- [Foam::LESModels::cubeRootVolDelta](doxy_id://Foam::LESModels::cubeRootVolDelta)
