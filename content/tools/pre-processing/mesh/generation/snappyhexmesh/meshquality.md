---
title: Mesh quality
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- input
- geometry
- snappyHexMesh

menu_id: snappyhexmesh-meshquality
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh
---

<%= page_toc %>

The `meshQualityDictionary` dictionary is used to define the values of many
extrema encountered during the meshing process to ensure that the resulting mesh
is of sufficient quality for subsequent calculation.  The limits predominantly
affect feature conformance, i.e. regions most likely to incur local mesh
distortion.  If the mesh violates any of the limiting values it attempts to
re-apply the offending changes with updated settings for additional cycles.
This is an override of the mesh quality settings when the
[nRelaxedIter](ref_id://snappyhexmesh-layers#keywords-nRelaxedIter) has been
reached. Values include:

## Keywords

---

### maxNonOrtho \[scalar\] {#keywords-maxNonOrtho}

Maximum face non-orthogonality angle \[deg\]: the angle made by the vector
between the two adjacent cell centres across the common face and the face normal

    maxNonOrtho             65;

---

### maxBoundarySkewness \[scalar\] {#keywords-maxBoundarySkewness}

Maximum boundary skewness:

    maxBoundarySkewness     20;

---

### maxInternalSkewness \[scalar\] {#keywords-maxInternalSkewness}

Maximum internal face skewness:

    maxInternalSkewness     4;

---

### maxConcave \[scalar\] {#keywords-maxConcave}

Maximum cell concavity \[deg\]:

    maxConcave              80;

---

### minVol \[scalar\] {#keywords-minVol}

Minimum cell pyramid volume \[m$$^3$$\]:

    minVol                  1e-13;

---

### minTetQuality \[scalar\] {#keywords-minTetQuality}

Minimum tetrahedron quality:

    //  1e-15 (small positive) to enable tracking
    // -1e+30 (large negative) for best layer insertion
    minTetQuality           1e-15;

---

### minVolCollapseRatio \[scalar\] {#keywords-minVolCollapseRatio}

minVolCollapseRatio:

    // if >0 : preserve single cells with all points on the surface if the
    // resulting volume after snapping (by approximation) is larger than
    // minVolCollapseRatio times old volume (i.e. not collapsed to flat cell).
    //  If <0 : delete always.
    //minVolCollapseRatio   0.5;

---

### minArea \[scalar\] {#keywords-minArea}

Minimum face area \[m$$^2$$\]: A negative value to bypass this metric

    minArea                 -1;

---

### minTwist \[scalar\] {#keywords-minTwist}

Minimum twist:

    minTwist                0.02;

---

### minDeterminant \[scalar\] {#keywords-minDeterminant}

Minimum cell determinant:

    minDeterminant          0.001;

---

### minFaceWeight \[scalar\] {#keywords-minFaceWeight}

Minimum face interpolation weight

    minFaceWeight           0.05;

---

### minFaceFlatness \[scalar\] {#keywords-minFaceFlatness}

minFaceFlatness: Optional

    minFaceFlatness         -1;

---

### minVolRatio \[scalar\] {#keywords-minVolRatio}

minVolRatio:

    minVolRatio             0.01;

---

### minTriangleTwist \[scalar\] {#keywords-minTriangleTwist}

minTriangleTwist:

    minTriangleTwist        -1;

---

### nSmoothScale \[scalar\] {#keywords-nSmoothScale}

Smoothing iterations. Used in combination with [errorReduction](#keywords-errorReduction):

    nSmoothScale            4;

---

### errorReduction \[scalar\] {#keywords-errorReduction}

Error reduction.  Used in combination with [nSmoothScale](#keywords-nSmoothScale)

    errorReduction          0.75;

---

### relaxed \[dictionary\] {#keywords-relaxed}

Any of the mesh quality controls can be relaxed by adding the appropriate
entry to the `relaxed` dictionary, e.g.

    relaxed
    {
        maxNonOrtho             75;
    }
