---
title: atmBoundaryLayerInletEpsilon
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-atmBoundaryLayerInletEpsilon
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

The `atmBoundaryLayerInletEpsilon` is a boundary condition that provides
a log-law type ground-normal inlet boundary condition for the turbulent kinetic
energy dissipation rate (i.e. `epsilon`) for homogeneous, two-dimensional,
dry-air, equilibrium and neutral atmospheric boundary layer modelling.

The ground-normal `epsilon` profile expression:

$$
    \epsilon = \frac{(u^*)^3}{\kappa (z - d + z_0)}
    \sqrt{C_1 \ln \left( \frac{z - d + z_0}{z_0} \right) + C_2}
$$

where:

Property     | Description
-------------|--------------------------------------------
$$\epsilon$$ | Ground-normal TKE dissipation rate profile     \[m^2/s^3\]
$$u^*$$      | Friction velocity                              \[m/s\]
$$\kappa$$   | von Kármán constant                            \[-\]
$$z$$        | Ground-normal coordinate component             \[m\]
$$z_0$$      | Aerodynamic roughness length                   \[m\]
$$d$$        | Ground-normal displacement height              \[m\]
$$C_1$$      | Curve-fitting coefficient for YGCJ profiles    \[-\]
$$C_2$$      | Curve-fitting coefficient for YGCJ profiles    \[-\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            atmBoundaryLayerInletEpsilon;

        // Inherited entries
        ...
    }

<% assert :string_exists, "atmBoundaryLayerInletEpsilonFvPatchScalarField.H", "atmBoundaryLayerInletEpsilon" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `atmBoundaryLayerInletEpsilon`  | word    | yes      | -

The inherited entries are elaborated in:

- atmBoundaryLayer.H
- inletOutletFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/turbineSiting" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/atmBoundaryLayerInletEpsilon" %>

API:

- [Foam::atmBoundaryLayerInletEpsilonFvPatchScalarField](doxy_id://Foam::atmBoundaryLayerInletEpsilonFvPatchScalarField)

<%= history "v1806" %>

<!----------------------------------------------------------------------------->
