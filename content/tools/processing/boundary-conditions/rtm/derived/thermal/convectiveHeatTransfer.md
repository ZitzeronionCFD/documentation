---
title: convectiveHeatTransfer
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-convectiveHeatTransfer
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `convectiveHeatTransfer` is a boundary condition that
provides a convective heat transfer coefficient condition:

- If $$Re > 500000$$:
$$
    h_p = \frac{0.664 \mathrm{Re}^{0.5} \mathrm{Pr}^{0.333} \kappa_p}{L}
$$
- Else:
$$
    h_p = \frac{0.037 \mathrm{Re}^{0.8} \mathrm{Pr}^{0.333} \kappa_p}{L}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$h_p$$               | Patch convective heat transfer coefficient
$$\mathrm{Re}$$       | Reynolds number
$$\mathrm{Pr}$$       | Prandtl number
$$\kappa_p$$          | Thermal conductivity
$$L$$                 | Length scale

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            convectiveHeatTransfer;
        L               <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "convectiveHeatTransferFvPatchScalarField.H", "convectiveHeatTransfer" %>
<% assert :string_exists, "convectiveHeatTransferFvPatchScalarField.C", "L" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `convectiveHeatTransfer`        | word    | yes      | -
`L`                    | Length scale                               | scalar  | yes      | -


The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/reactingParcelFoam/hotBoxes" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/convectiveHeatTransfer" %>

API:

- [Foam::compressible::convectiveHeatTransferFvPatchScalarField](doxy_id://Foam::compressible::convectiveHeatTransferFvPatchScalarField)

<%= history "2206" %>

<!----------------------------------------------------------------------------->
