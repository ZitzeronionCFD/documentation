---
title: continuityError
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- continuity
- error
- field
- function object
- post-processing
menu_id: function-objects-field-continuityerror
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `continuityError` function object computes local, global and cumulative
continuity errors for a flux field.

Local continuity error, $$\epsilon_\text{local}$$:

$$
    \epsilon_\text{local} = \Delta_t \langle |x| \rangle
$$

Global continuity error, $$\epsilon_\text{global}$$:

$$
    \epsilon_\text{global} = \Delta_t \langle |x| \rangle
$$

Cumulative continuity, $$\epsilon_\text{cum}$$:

$$
    \epsilon_\text{cum} += \epsilon_\text{global}
$$

where
$$\Delta_t$$
: Time-step size

$$<.>$$
: Cell-volume weighted average operator

$$x$$
: $$ \nabla \cdot \phi $$

$$\phi$$
: Flux field

### Operands

Operand      | Type           | Location
:---         |:---            |:---
input        | -              | -
output file  | dat            | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
output field | -              | -

## Usage {#usage}

Example of the `continuityError` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    continuityError1
    {
        // Mandatory entries (unmodifiable)
        type            continuityError;
        libs            (fieldFunctionObjects);

        // Optional entries (runtime modifiable)
        phi             phi;

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "continuityError.H", "continuityError" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "continuityError",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "continuityError.C", "phi" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "fieldsExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: continuityError          | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 phi          | Name of flux field                  | word | no       | phi

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Example by using the `postProcess -func` utility:

    postProcess -func "continuityError(<field1>, <field2>, ..., <fieldN>)"

## Stored properties {#stored-properties}

### Reduced data

The following data are stored by the function object, and can be used by other
function objects:

- `local` : local continuity error
- `global` : global continuity error
- `cumulative` : cumulative continuity error

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/continuityError" %>

API:

- [Foam::functionObjects::continuityError](doxy_id://Foam::functionObjects::continuityError)

<%= history "v1906" %>

<!----------------------------------------------------------------------------->
