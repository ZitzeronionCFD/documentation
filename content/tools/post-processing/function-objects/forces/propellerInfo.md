---
title: propellerInfo
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- force
- function object
- post-processing
- propeller
menu_id: function-objects-propellerInfo-propellerInfo
license: http://creativecommons.org/licenses/by-nc-nd/4.0
menu_parent: function-objects-propellerInfo
group: function-objects-propellerInfo
publish: false
---

<%= page_toc %>

The `propellerInfo` function object ...

## Usage {#usage}

Basic operation of the `propellerInfo` function object comprises:

    propellerInfo1
    {
        type            propellerInfo;
        libs            ("libpropellerInfo.so");
    }

<% assert :string_exists, "propellerInfo.H", "propellerInfo" %>
<% assert :class_library_exists, "functionObjects/propellerInfo/Make/files", "propellerInfo", "libpropellerInfo" %>

For more complete control, the full set of input entries includes:

    propellerInfo1
    {
        // Mandatory entries
        type            propellerInfo;
        libs            ("libpropellerInfo.so");

        // Optional entries

    }

## Sample output {#sample-output}

Text reporting:

...


## Stored properties {#stored-properties}

### Reduced data

The following data are stored by the function object, and can be used by other
function objects:

- `n` : xxx
- `URef` : reference velocity
- `Kt` : xxx
- `Kq` : xxx
- `J` : xxx
- `eta0` : xxx

### Fields


## Further information {#sec-os-propellerInfo-propellerInfo-further-information}

Related:

- [forces](../forces) function object

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/forces/propellerInfo" %>

API:

- [Foam::functionObjects::propellerInfo](doxy_id://Foam::functionObjects::propellerInfo)

Example usage:

- xxx

<%= history "v2112" %>
