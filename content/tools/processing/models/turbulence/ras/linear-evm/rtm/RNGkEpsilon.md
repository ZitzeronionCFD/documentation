---
title: RNG k-ε
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- RAS
- turbulence
menu_id: ras-rngkepsilon
license: CC-BY-NC-ND-4.0
menu_parent: ras-linearevm
group: turbulence-ras-evm
---

<%= page_toc %>

## Properties {#properties}

## Model equations {#model-equations}

## Default model coefficients {#default-coeffs}

## Initialisation {#initialisation}

## Usage {#usage}

The model is specified using:

    RAS
    {
        turbulence      on;
        RASModel        RNGkEpsilon;
    }

<% assert :string_exists, "RASModel.C", "RAS" %>
<% assert :string_exists, "RASModel.C", "RASModel" %>
<% assert :string_exists, "RNGkEpsilon.H", "RNGkEpsilon" %>

## Boundary conditions {#bcs}

Walls

## Further information {#further-information}

Source code:

- [Foam::RASModels::RNGkEpsilon](doxy_id://Foam::RASModels::RNGkEpsilon)

References:

- *Yakhot et al.* <%= cite "yakhot_development_1992" %>
