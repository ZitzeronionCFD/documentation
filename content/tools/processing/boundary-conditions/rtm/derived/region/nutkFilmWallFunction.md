---
title: nutkFilmWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-nutkFilmWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-region
group: bcs-region
---

<%= page_toc %>

## Description

The `nutkFilmWallFunction` is boundary condition that provides
a turbulent viscosity condition when
using wall functions, based on turbulence kinetic energy, for use with
surface film models.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            nutkFilmWallFunction;

        // Optional entries
        filmRegion      <word>;
        B               <scalar>;
        yPlusCrit       <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "nutkFilmWallFunctionFvPatchScalarField.H", "nutkFilmWallFunction" %>
<% assert :string_exists, "nutkFilmWallFunctionFvPatchScalarField.C", "B" %>
<% assert :string_exists, "nutkFilmWallFunctionFvPatchScalarField.C", "yPlusCrit" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `nutkFilmWallFunction`          | word    | yes      | -
`B`            | B coefficient                              | scalar  | no       | 5.5
`yPlusCrit`    | y+ value for laminar -> turbulent transition | scalar  | no     | 11.05

The inherited entries are elaborated in:

- nutkWallFunctionFvPatchScalarField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/lagrangian/reactingParcelFoam/hotBoxes" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/regionModels/surfaceFilmModels/derivedFvPatchFields/wallFunctions/nutkFilmWallFunction" %>

API:

- [Foam::compressible::RASModels::nutkFilmWallFunctionFvPatchScalarField](doxy_id://Foam::compressible::RASModels::nutkFilmWallFunctionFvPatchScalarField)

<%= history "2.0.0" %>

<!----------------------------------------------------------------------------->
