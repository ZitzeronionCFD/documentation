---
title: angularOscillatingDisplacement
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-angularOscillatingDisplacement
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-meshMotion
group: bcs-meshMotion
---

<%= page_toc %>

## Description

The `angularOscillatingDisplacement` is boundary condition that provides
oscillating motion condition for moving patches.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            angularOscillatingDisplacement;
        axis            <vector>;
        origin          <vector>;
        angle0          <scalar>;
        amplitude       <scalar>;
        omega           <scalar>;

        // Optional entries
        p0              <pointField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "angularOscillatingDisplacementPointPatchVectorField.H", "angularOscillatingDisplacement" %>
<% assert :string_exists, "angularOscillatingDisplacementPointPatchVectorField.C", "axis" %>
<% assert :string_exists, "angularOscillatingDisplacementPointPatchVectorField.C", "origin" %>
<% assert :string_exists, "angularOscillatingDisplacementPointPatchVectorField.C", "angle0" %>
<% assert :string_exists, "angularOscillatingDisplacementPointPatchVectorField.C", "amplitude" %>
<% assert :string_exists, "angularOscillatingDisplacementPointPatchVectorField.C", "omega" %>
<% assert :string_exists, "angularOscillatingDisplacementPointPatchVectorField.C", "p0" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `angularOscillatingDisplacement` | word    | yes     | -
`axis`         | Rotation axis                               | vector  | yes     | -
`origin`       | Rotation origin                             | vector  | yes     | -
`angle0`       | Initial angle                               | scalar  | yes     | -
`amplitude`    | Oscillation amplitude                       | scalar  | yes     | -
`omega`        | Rotation speed                              | scalar  | yes     | -
`p0`           | Initial displacement field                  | pointField | no   | -

The inherited entries are elaborated in:

- fixedValuePointPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/fvMotionSolver/pointPatchFields/derived/angularOscillatingDisplacement" %>

API:

- [Foam::angularOscillatingDisplacementPointPatchVectorField](doxy_id://Foam::angularOscillatingDisplacementPointPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
