---
title: Linear
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- equation of state
- linear
menu_id: equationOfState-incompressiblePerfectGas
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-equationOfState
group: equation-of-state-models
---

<%= page_toc %>

Density, $$ \rho $$ \[kg/m$$^3$$\]

$$
\rho = \rho_0 + \psi p
$$

Entropy, $$ S $$ \[J/kg/K \]

$$
S = -\frac{1}{T \psi}\ln\left(\frac{\rho_0 + \psi p}{\rho_0 + \psi p_{\std}}\right)
$$

Compressibility, $$ \psi $$ \[s$$^2$$/m$$^2$$\]

$$
\psi = constant
$$

## Usage

The `linear` equation of state is specified as:

~~~
equationOfState
{
    psi         <scalar>;
    rho0        <scalar>;
}
~~~

Where

- `psi` : Compressibility \[s$$^2$$/m$$^2$$\]
- `rho0` : Reference density \[kg/m$$^3$$\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/equationOfState/linear" %>
