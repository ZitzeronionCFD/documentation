---
title: SRFFreestreamVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-SRFFreestreamVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `SRFFreestreamVelocity` is a boundary condition that provides
freestream velocity condition to be used in conjunction with the single
rotating frame (SRF) model (see: `SRFModel` class).

Given the free stream velocity in the absolute frame, the condition
applies the appropriate rotation transformation in time and space to
determine the local velocity using:

$$
    \u_p = cos(\theta) \u_{Inf} + sin(\theta) (n \times \u_{Inf}) - \u_{p,srf}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\u_p$$              | Patch velocity \[m/s\]
$$\u_{Inf}$$          | Free stream velocity in the absolute frame \[m/s\]
$$\theta$$            | Swept angle \[rad\]
$$n$$                 | Axis direction of the SRF
$$\u_{p,srf}$$        | SRF velocity of the patch \[m/s\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            SRFFreestreamVelocity;
        UInf            <vector>;

        // Optional entries
        relative        <bool>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "SRFFreestreamVelocityFvPatchVectorField.H", "SRFFreestreamVelocity" %>
<% assert :string_exists, "SRFFreestreamVelocityFvPatchVectorField.C", "UInf" %>
<% assert :string_exists, "SRFFreestreamVelocityFvPatchVectorField.C", "relative" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `SRFFreestreamVelocity`         | word    | yes      | -
`UInf`         | Velocity of the free stream in the absolute frame  | vector | yes   | -
`relative`     | Flag to decide if the supplied inlet value relative to the SRF | bool | no | false

The inherited entries are elaborated in:

- inletOutletFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/SRFPimpleFoam/rotor2D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/cfdTools/general/SRF/derivedFvPatchFields/SRFFreestreamVelocityFvPatchVectorField" %>

API:

- [Foam::SRFFreestreamVelocityFvPatchVectorField](doxy_id://Foam::SRFFreestreamVelocityFvPatchVectorField)

<%= history "2.0.0" %>

<!----------------------------------------------------------------------------->
