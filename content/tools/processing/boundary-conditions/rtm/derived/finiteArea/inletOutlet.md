---
title: inletOutlet
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-inletOutlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-fa
group: bcs-fa
---

<%= page_toc %>

## Description

The `inletOutlet` is a boundary condition that provides
a generic outflow condition, with specified inflow for the case of return flow.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            inletOutlet;
        inletValue      <Field>;

        // Optional entries
        phi             <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "inletOutletFaPatchField.H", "inletOutlet" %>
<% assert :string_exists, "inletOutletFaPatchField.C", "inletValue" %>
<% assert :string_exists, "inletOutletFaPatchField.C", "phi" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `inletOutlet`                   | word    | yes      | -
`inletValue`           | Inlet value field                          | scalarField | yes  | -
`phi`                  | Name of flux field                         | word    | no       | phi

The inherited entries are elaborated in:

- mixedFaPatchField.H
<!-- end of the list -->

- The mode of operation is determined by the sign of the flux across the
  patch edges.
- Sign conventions:
  - Positive flux (out of domain): apply zero-gradient condition
  - Negative flux (into domain): apply the "inletValue" fixed-value
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/finiteArea/surfactantFoam/planeTransport" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteArea/fields/faPatchFields/derived/inletOutlet" %>

API:

- [Foam::inletOutletFaPatchField](doxy_id://Foam::inletOutletFaPatchField)

<%= history "v1712" %>

<!----------------------------------------------------------------------------->
