---
title: Laminar flow
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- example
- laminar
- verification
- validation
menu_id: verification-validation-laminar
license: CC-BY-NC-ND-4.0
menu_parent: examples-verification-validation
---

<%= page_toc %>

## Laminar flow {#laminar-flow}

<%= insert_models "verification-validation-laminar" %>
