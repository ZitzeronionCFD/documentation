---
title: triSurfaceMesh
copyright:
- Copyright (C) 2019-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: trisurfacemesh
license: CC-BY-NC-ND-4.0
#menu_parent: snappyhexmesh-geometry
group: trisurfacemeshes
---

<%= page_toc %>

## Usage

An example use of the `triSurfaceMesh`:

    sphere.stl // name of the geometrical entity
    {
        type    triSurfaceMesh;

        // Per region the patch name. If not provided will be <surface>_<region>.
        // Note: this name cannot be used to identity this region in any
        //       other part of this dictionary; it is only a name
        //       for the combination of surface+region (which is only used
        //       when creating patches)
        regions
        {
            secondSolid
            {
                name mySecondPatch;
            }
        }
    }

<% assert :string_exists, "triSurfaceMesh.H", "triSurfaceMesh" %>
<% assert :string_exists, "snappyHexMesh.C", "regions" %>

Optional entries include:

    // Tolerance to use for intersection tests
    tolerance           1e-5;

    // Scale factor to apply to the points
    scale               0.001;

    // Name to use to refer to this surface
    name                surface1;

<% assert :string_exists, "triSurfaceSearch.C", "tolerance" %>
<% assert :string_exists, "triSurfaceMesh.C", "scale" %>
<% assert :string_exists, "searchableSurfaces.C", "name" %>

See also

- <%= link_to_menuid "distributedtrisurfacemesh" %>
