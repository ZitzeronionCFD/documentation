---
title: Forces
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- force
- function object
- post-processing
menu_id: function-objects-forces-forces
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-forces
group: function-objects-forces
---

<%= page_toc %>

The `forces` function object generates aerodynamic force and moment
data for surfaces and porous regions, with optional

- inclusion of porous drag
- local co-ordinate system
- data binning

Forces comprise normal pressure

$$
    \vec{F}_p = \sum_i \rho_i \vec{s}_{f,i} \left(p_i - p_{\ref}\right)
$$

and tangential viscous contributions:

$$
    \vec{F}_v = \sum_i s_{f,i} \dprod \left(\mu \tensor{R_{\mathrm{dev}}} \right)
$$

Where $$ \rho $$ is the density, $$ \vec{s}_{f,i} $$ the face area vector,
$$ p $$ the pressure$$ \mu $$ the dynamic viscosity and
$$ \tensor{R_{\mathrm{dev}}} $$ the deviatoric stress tensor.

## Usage {#usage}

Basic operation of the `forces` function object comprises:

    forces1
    {
        type            forces;
        libs            ("libforces.so");
        patches         (<list of patch names>);
    }

<% assert :string_exists, "forces.H", "forces" %>
<% assert :class_library_exists, "functionObjects/forces/Make/files", "forces", "libforces" %>
<% assert :string_exists, "forces.C", "patches" %>

For more complete control, the full set of input entries includes:

    forces1
    {
        // Mandatory entries
        type            forces;
        libs            ("libforces.so");
        patches         (<list of patch names>);


        // Optional entries

        // Field names
        p               p;
        U               U;
        rho             rho;

        // Reference pressure [Pa]
        pRef            0;

        // Include porosity effects?
        porosity        no;

        // Store and write volume field representations of forces and moments
        writeFields     yes;

        // Centre of rotation for moment calculations
        CofR            (0 0 0);

        // Spatial data binning
        // - extents given by the bounds of the input geometry
        binData
        {
            nBin        20;
            direction   (1 0 0);
            cumulative  yes;
        }
    }

<% assert :string_exists, "forces.C", "patches" %>
<% assert :string_exists, "forces.C", "p" %>
<% assert :string_exists, "forces.C", "U" %>
<% assert :string_exists, "forces.C", "rho" %>
<% assert :string_exists, "forces.C", "pRef" %>
<% assert :string_exists, "forces.C", "porosity" %>
<% assert :string_exists, "forces.C", "writeFields" %>
<% assert :string_exists, "forces.C", "CofR" %>
<% assert :string_exists, "forces.C", "binData" %>
<% assert :string_exists, "forces.C", "nBin" %>
<% assert :string_exists, "forces.C", "direction" %>
<% assert :string_exists, "forces.C", "cumulative" %>

Default behaviour assumes that the case is compressible.  For incompressible
cases, i.e. solved using the
[kinematic pressure](ref_id://algorithm-kinematic-pressure)
the `rho` entry can be used to set the free stream density:

    rho         rhoInf;
    rhoInf      100000;

<% assert :string_exists, "forces.C", "rho" %>
<% assert :string_exists, "forces.C", "rhoInf" %>

## Sample output {#sample-output}

Text reporting:

- integrated force and moment data divided into total, pressure,
  viscous and porous contributions

      Sum of forces
          Total    : xxx
          Pressure : xxx
          Viscous  : xxx
          Porous   : xxx

      Sum of moments
          Total    : xxx
          Pressure : xxx
          Viscous  : xxx
          Porous   : xxx

## Stored properties {#stored-properties}

### Reduced data

The following data are stored by the function object, and can be used by other
function objects:

- `normalForce` : total normal force
- `tangentialForce` : total tangential force
- `porousForce` : total porous force
- `normalMoment` : total normal moment
- `tangentialMoment` : total tangential moment
- `porousMoment` : total porous moment

<% assert :string_exists, "forces.C", "normalForce" %>
<% assert :string_exists, "forces.C", "tangentialForce" %>
<% assert :string_exists, "forces.C", "porousForce" %>
<% assert :string_exists, "forces.C", "normalMoment" %>
<% assert :string_exists, "forces.C", "tangentialMoment" %>
<% assert :string_exists, "forces.C", "porousMoment" %>

### Fields

Optional storing and writing of force and moment fields:

- `forces` : forces
- `moments` : moments

<% assert :string_exists, "forces.C", "forces" %>
<% assert :string_exists, "forces.C", "moments" %>

## Further information {#sec-os-forces-forces-further-information}

Related:

- [force coefficients](../force-coeffs) function object

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/forces/forces" %>

API:

- [Foam::functionObjects::forces](doxy_id://Foam::functionObjects::forces)

Example usage:

- [pimpleFoam](ref_id://pimplefoam) `propeller` tutorial:
  <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/propeller" %>

\ofAssert functionObjectUseExists incompressible/pimpleFoam/RAS/propeller forces
