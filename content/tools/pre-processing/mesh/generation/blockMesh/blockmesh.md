---
title: blockMesh
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
  - meshing
menu_id: blockmesh
license: CC-BY-NC-ND-4.0
menu_parent: mesh-generation
---

<%= page_toc %>

## Overview {#overview}

`blockMesh` is a structured hexahedral mesh generator.

Key features:

- structured hex mesh
- built using blocks
- supports cell size grading
- supports curved block edges

Constraints:

- requires consistent block-to-block connectivity
- ordering of points is important

Well suited to simple geometries that can be described by a few blocks, but
challenging to apply to cases with a large number of blocks due to book-keeping
requirements, i.e. the need to manage point connectivity and ordering.

## Usage {#usage}

Command line usage:

<%= help_util_usage "blockMesh" %>

The utility is controlled using a `blockMeshDict` dictionary, located in
the case `system` directory, split into the following sections:

- points
- edges
- blocks
- patches

## Further information {#further-information}

Example usage:

- Tutorials: <%= repo_link2 "$FOAM_TUTORIALS/mesh/blockMesh" %>
