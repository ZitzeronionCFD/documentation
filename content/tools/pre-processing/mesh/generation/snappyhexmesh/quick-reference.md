---
title: Quick reference
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- input
- geometry
- snappyHexMesh

menu_id: snappyhexmesh-quick-reference
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh
---

<%= page_toc %>

The quick reference colours the entries according to:

<table>
<tr><th>Action</th><th>Colour</th></tr>
<tr><td>Required entry</td><td class="input-required"></td></tr>
<tr><td>Change as needed</td><td class="input-change-ok"></td></tr>
<tr><td>Sometimes change</td><td class="input-change-maybe"></td></tr>
<tr><td>Usually leave as default</td><td class="input-change-unusual"></td></tr>
<tr><td>Deprecated entry</td><td class="input-deprecated"></td></tr>
</table>

## Top-level entries {#sec-top}

<table>
    <tr>
        <th>Keyword</th>
        <th>Value type</th>
        <th>Default value</th>
        <th>Change?</th>
    </tr>
    <tr>
        <td><%= link_to "castellatedMesh",
        ref_id("snappyhexmesh-global", "keywords-castellatedMesh") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "snap",
        ref_id("snappyhexmesh-global", "keywords-snap") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "addLayers",
        ref_id("snappyhexmesh-global", "keywords-addLayers") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "singleRegionName",
        ref_id("snappyhexmesh-global", "keywords-singleRegionName") %></td>
        <td>boolean</td>
        <td>yes</td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "mergePatchFaces",
        ref_id("snappyhexmesh-global", "keywords-mergePatchFaces") %></td>
        <td>boolean</td>
        <td>yes</td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "mergeTolerance",
        ref_id("snappyhexmesh-global", "keywords-mergeTolerance") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "keepPatches",
        ref_id("snappyhexmesh-global", "keywords-keepPatches") %></td>
        <td>boolean</td>
        <td>no</td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "debug",
        ref_id("snappyhexmesh-global", "debugging-debug") %></td>
        <td>label</td>
        <td>0</td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "geometry",
        "#sec-snappy-quick-reference-geometry" %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "castellatedMeshControls",
        "#sec-snappy-quick-reference-castellatedMeshControls" %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-required"></td>
    </tr>
    <tr>
        <td><%= link_to "snapControls",
        "#sec-snappy-quick-reference-snapControls" %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-required"></td>
    </tr>
    <tr>
        <td><%= link_to "addLayerControls",
        "#sec-snappy-quick-reference-addLayerControls" %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-required"></td>
    </tr>
    <tr>
        <td><%= link_to "meshQualityControls",
        "#sec-snappy-quick-reference-meshQualityControls" %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-required"></td>
    </tr>
    <tr>
        <td><%= link_to "debugFlags",
        "#sec-snappy-quick-reference-debugFlags" %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "writeFlags",
        "#sec-snappy-quick-reference-writeFlags" %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "outputFlags",
        "#sec-snappy-quick-reference-outputFlags" %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-deprecated"></td>
    </tr>
</table>

## Geometry {#sec-snappy-quick-reference-geometry}

Sub-dictionary: `geometry`

<table>
    <tr>
        <th>Keyword</th>
        <th>Value type</th>
        <th>Default value</th>
        <th>Change?</th>
    </tr>
    <tr>
        <td>&lt;geometryName&gt;</td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td>&lt;geometryName&gt;.type</td>
        <td>geometryType</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td>&lt;geometryName&gt;.name</td>
        <td>word</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td>&lt;geometryName&gt;.regions</td>
        <td>word</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
</table>

## Castellated mesh controls {#sec-snappy-quick-reference-castellatedMeshControls}

Sub-dictionary: `castellatedMeshControls`

<table>
    <tr>
        <th>Keyword</th>
        <th>Value type</th>
        <th>Default value</th>
        <th>Change?</th>
    </tr>
    <tr>
        <td><%= link_to "maxLocalCells",
        ref_id("snappyhexmesh-castellation", "castellation-maxLocalCells") %>
        </td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "maxGlobalCells",
        ref_id("snappyhexmesh-castellation", "castellation-maxGlobalCells") %>
        </td>
        <td>label</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "minRefinementCells",
        ref_id("snappyhexmesh-castellation",
        "castellation-minRefinementCells") %>
        </td>
        <td>label</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "maxLoadUnbalance",
        ref_id("snappyhexmesh-castellation", "castellation-maxLoadUnbalance") %>
        </td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nCellsBetweenLevels",
        ref_id("snappyhexmesh-castellation",
        "castellation-nCellsBetweenLevels") %>
        </td>
        <td>label</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "features",
        ref_id("snappyhexmesh-castellation", "castellation-features") %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-required"></td>
    </tr>
    <tr>
        <td>features.file</td>
        <td>string</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td>features.level</td>
        <td>label</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td>features.levels</td>
        <td>(scalar label)</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "refinementSurfaces",
        ref_id("snappyhexmesh-castellation",
        "castellation-refinementSurfaces") %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-required"></td>
    </tr>
    <tr>
        <td>refinementSurfaces.&lt;geometryName&gt;</td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td>refinementSurfaces.&lt;geometryName&gt;.level</td>
        <td>label</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td>refinementSurfaces.&lt;geometryName&gt;.regions</td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change=ok"></td>
    </tr>
    <tr>
        <td><%= link_to "resolveFeatureAngle",
        ref_id("snappyhexmesh-castellation",
        "castellation-resolveFeatureAngle") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "gapLevelIncrement",
        ref_id("snappyhexmesh-castellation",
        "castellation-gapLevelIncrement") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "planarAngle",
        ref_id("snappyhexmesh-castellation",
        "castellation-planarAngle") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "refinementRegions",
        ref_id("snappyhexmesh-castellation",
        "castellation-refinementRegions") %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-required"></td>
    </tr>
    <tr>
        <td><%= link_to "limitRegions",
        ref_id("snappyhexmesh-castellation", "castellation-limitRegions") %>
        </td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "locationInMesh",
        ref_id("snappyhexmesh-castellation", "castellation-locationInMesh") %>
        </td>
        <td>point</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "locationsInMesh",
        ref_id("snappyhexmesh-castellation", "castellation-locationsInMesh") %>
        </td>
        <td>list of (point word)</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "allowFreeStandingZoneFaces",
        ref_id("snappyhexmesh-castellation",
        "castellation-allowFreeStandingZoneFaces") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "faceZoneControls",
        ref_id("snappyhexmesh-castellation", "castellation-faceZoneControls") %>
        </td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "locationsOutsideMesh",
        ref_id("snappyhexmesh-castellation",
        "castellation-locationsOutsideMes") %></td>
        <td>list of points</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "handleSnapProblems",
        ref_id("snappyhexmesh-castellation",
        "castellation-handleSnapProblems") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "useTopologicalSnapDetection",
        ref_id("snappyhexmesh-castellation",
        "castellation-useTopologicalSnapDetection") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "interfaceRefine",
        ref_id("snappyhexmesh-castellation", "castellation-interfaceRefine") %>
        </td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nCellZoneErodeIter",
        ref_id("snappyhexmesh-castellation",
        "castellation-nCellZoneErodeIter") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
</table>

## Snap controls {#sec-snappy-quick-reference-snapControls}

Sub-dictionary: `snapControls`

<table>
    <tr>
        <th>Keyword</th>
        <th>Value type</th>
        <th>Default value</th>
        <th>Change?</th>
    </tr>
    <tr>
        <td><%= link_to "nSmoothPatch",
        ref_id("snappyhexmesh-snapping", "keywords-nSmoothPatch") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nSmoothInternal",
        ref_id("snappyhexmesh-snapping", "keywords-nSmoothInternal") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "tolerance",
        ref_id("snappyhexmesh-snapping", "keywords-tolerance") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nSolveIter",
        ref_id("snappyhexmesh-snapping", "keywords-nSolveIter") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nRelaxIter",
        ref_id("snappyhexmesh-snapping", "keywords-nRelaxIter") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "detectNearSurfaceSnap",
        ref_id("snappyhexmesh-snapping", "keywords-detectNearSurfaceSnap") %>
        </td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nFeatureSnapIter",
        ref_id("snappyhexmesh-snapping", "keywords-nFeatureSnapIter") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "implicitFeatureSnap",
        ref_id("snappyhexmesh-snapping", "keywords-implicitFeatureSnap") %></td>
        <td>boolean</td>
        <td>false</td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "explicitFeatureSnap",
        ref_id("snappyhexmesh-snapping", "keywords-explicitFeatureSnap") %></td>
        <td>boolean</td>
        <td>true</td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "multiRegionFeatureSnap",
        ref_id("snappyhexmesh-snapping", "keywords-multiRegionFeatureSnap") %>
        </td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "nFaceSplitInterval",
        ref_id("snappyhexmesh-snapping", "keywords-nFaceSplitInterval") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "detectBaffles",
        ref_id("snappyhexmesh-snapping", "keywords-detectBaffles") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "releasePoints",
        ref_id("snappyhexmesh-snapping", "keywords-releasePoints") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "stringFeatures",
        ref_id("snappyhexmesh-snapping", "keywords-stringFeatures") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "avoidDiagonal",
        ref_id("snappyhexmesh-snapping", "keywords-avoidDiagonal") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "concaveAngle",
        ref_id("snappyhexmesh-snapping", "keywords-concaveAngle") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minAreaRatio",
        ref_id("snappyhexmesh-snapping", "keywords-minAreaRatio") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "strictRegionSnap",
        ref_id("snappyhexmesh-snapping", "keywords-strictRegionSnap") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "baffleFeaturePoints",
        ref_id("snappyhexmesh-snapping", "keywords-baffleFeaturePoints") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
</table>

## Layer controls {#sec-snappy-quick-reference-addLayerControls}

Sub-dictionary: `addLayerControls`

<table>
    <tr>
        <th>Keyword</th>
        <th>Value type</th>
        <th>Default value</th>
        <th>Change?</th>
    </tr>
    <tr>
        <td><%= link_to "relativeSizes",
        ref_id("snappyhexmesh-layers", "keywords-relativeSizes") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "nBufferCellsNoExtrude",
        ref_id("snappyhexmesh-layers", "keywords-nBufferCellsNoExtrude") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nLayerIter",
        ref_id("snappyhexmesh-layers", "keywords-nLayerIter") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "nRelaxedIter",
        ref_id("snappyhexmesh-layers", "keywords-nRelaxedIter") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "additionalReporting",
        ref_id("snappyhexmesh-layers", "keywords-additionalReporting") %></td>
        <td>boolean</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "expansionRatio",
        ref_id("snappyhexmesh-layers", "keywords-expansionRatio") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "finalLayerThickness",
        ref_id("snappyhexmesh-layers", "keywords-finalLayerThickness") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "firstLayerThickness",
        ref_id("snappyhexmesh-layers", "keywords-firstLayerThickness") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "thickness",
        ref_id("snappyhexmesh-layers", "keywords-thickness") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "minThickness",
        ref_id("snappyhexmesh-layers", "keywords-minThickness") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td>layers</td>
        <td>dictionary</td>
        <td></td>
        <td class="input-required"></td>
    </tr>
    <tr>
        <td>layers.&lt;patchName&gt;</td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "layers.&lt;patchName&gt;.nSurfaceLayers",
        ref_id("snappyhexmesh-layers", "keywords-nSurfaceLayers") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "layers.&lt;patchName&gt;.expansionRatio",
        ref_id("snappyhexmesh-layers", "keywords-expansionRatio") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "layers.&lt;patchName&gt;.finalLayerThickness",
        ref_id("snappyhexmesh-layers", "keywords-finalLayerThickness") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "layers.&lt;patchName&gt;.firstLayerThickness",
        ref_id("snappyhexmesh-layers", "keywords-firstLayerThickness") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "layers.&lt;patchName&gt;.thickness",
        ref_id("snappyhexmesh-layers", "keywords-thickness") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
    <tr>
        <td><%= link_to "layers.&lt;patchName&gt;.minThickness",
        ref_id("snappyhexmesh-layers", "keywords-minThickness") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "nGrow",
        ref_id("snappyhexmesh-layers", "keywords-nGrow") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "concaveAngle",
        ref_id("snappyhexmesh-layers", "keywords-concaveAngle") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "featureAngle",
        ref_id("snappyhexmesh-layers", "keywords-featureAngle") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "mergePatchFacesAngle",
        ref_id("snappyhexmesh-layers", "keywords-mergePatchFacesAngle") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "maxFaceThicknessRatio",
        ref_id("snappyhexmesh-layers", "keywords-maxFaceThicknessRatio") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "meshShrinker",
        ref_id("snappyhexmesh-layers", "mesh-shrinking") %></td>
        <td>word</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "solver",
        ref_id("snappyhexmesh-layers", "shrinking-displacementmotionsolver") %>
        </td>
        <td>word</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "nSmoothSurfaceNormals",
        ref_id("snappyhexmesh-layers", "keywords-nSmoothSurfaceNormals") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minMedialAxisAngle",
        ref_id("snappyhexmesh-layers", "keywords-minMedialAxisAngle") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minMedianAxisAngle",
        ref_id("snappyhexmesh-layers", "keywords-minMedianAxisAngle") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-deprecated"></td>
    </tr>
    <tr>
        <td><%= link_to "maxThicknessToMedialRatio",
        ref_id("snappyhexmesh-layers", "keywords-maxThicknessToMedialRatio") %>
        </td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nSmoothNormals",
        ref_id("snappyhexmesh-layers", "keywords-nSmoothNormals") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nSmoothThickness",
        ref_id("snappyhexmesh-layers", "keywords-nSmoothThickness") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nMedialAxisIter",
        ref_id("snappyhexmesh-layers", "keywords-nMedialAxisIter") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nSmoothDisplacement",
        ref_id("snappyhexmesh-layers", "keywords-nSmoothDisplacement") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "layerTerminationAngle",
        ref_id("snappyhexmesh-layers", "keywords-layerTerminationAngle") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "slipFeatureAngle",
        ref_id("snappyhexmesh-layers", "keywords-slipFeatureAngle") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "nRelaxIter",
        ref_id("snappyhexmesh-layers", "keywords-nRelaxIter") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
</table>

## Mesh quality controls {#sec-snappy-quick-reference-meshQualityControls}

Sub-dictionary: `meshQualityControls`

<table>
    <tr>
        <th>Keyword</th>
        <th>Value type</th>
        <th>Default value</th>
        <th>Change?</th>
    </tr>
    <tr>
        <td><%= link_to "maxNonOrtho",
        ref_id("snappyhexmesh-meshquality", "keywords-maxNonOrtho") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "maxBoundarySkewness",
        ref_id("snappyhexmesh-meshquality", "keywords-maxBoundarySkewness") %>
        </td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "maxInternalSkewness",
        ref_id("snappyhexmesh-meshquality", "keywords-maxInternalSkewness") %>
        </td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "maxConcave",
        ref_id("snappyhexmesh-meshquality", "keywords-maxConcave") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minVol",
        ref_id("snappyhexmesh-meshquality", "keywords-minVol") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minTetQuality",
        ref_id("snappyhexmesh-meshquality", "keywords-minTetQuality") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minArea",
        ref_id("snappyhexmesh-meshquality", "keywords-minArea") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minTwist",
        ref_id("snappyhexmesh-meshquality", "keywords-minTwist") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minDeterminant",
        ref_id("snappyhexmesh-meshquality", "keywords-minDeterminan") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minFaceWeight",
        ref_id("snappyhexmesh-meshquality", "keywords-minFaceWeight") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minVolRatio",
        ref_id("snappyhexmesh-meshquality", "keywords-minVolRatio") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minTriangleTwist",
        ref_id("snappyhexmesh-meshquality", "keywords-minTriangleTwist") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minVolCollapseRatio",
        ref_id("snappyhexmesh-meshquality", "keywords-minVolCollapseRatio") %>
        </td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "minFaceFlatness",
        ref_id("snappyhexmesh-meshquality", "keywords-minFaceFlatness") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "relaxed",
        ref_id("snappyhexmesh-meshquality", "keywords-relaxed") %></td>
        <td>dictionary</td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td><%= link_to "nSmoothScale",
        ref_id("snappyhexmesh-meshquality", "keywords-nSmoothScale") %></td>
        <td>label</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td><%= link_to "errorReduction",
        ref_id("snappyhexmesh-meshquality", "keywords-errorReduction") %></td>
        <td>scalar</td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
</table>

## Debug flags {#sec-snappy-quick-reference-debugFlags}

Sub-dictionary: `debugFlags`

<table>
    <tr>
        <th>Keyword</th>
        <th>Value type</th>
        <th>Default value</th>
        <th>Change?</th>
    </tr>
    <tr>
        <td>mesh</td>
        <td></td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td>intersections</td>
        <td></td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td>featureSeeds</td>
        <td></td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td>attraction</td>
        <td></td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td>layerInfo</td>
        <td></td>
        <td></td>
        <td class="input-change-ok"></td>
    </tr>
</table>

## Write flags {#sec-snappy-quick-reference-writeFlags}

Sub-dictionary: `writeFlags`

<table>
    <tr>
        <th>Keyword</th>
        <th>Options</th>
        <th>Default value</th>
        <th>Change?</th>
    </tr>
    <tr>
        <td>mesh</td>
        <td></td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td>noRefinement</td>
        <td></td>
        <td></td>
        <td class="input-change-unusual"></td>
    </tr>
    <tr>
        <td>scalarLevels</td>
        <td></td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td>layerSets</td>
        <td></td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
    <tr>
        <td>layerFields</td>
        <td></td>
        <td></td>
        <td class="input-change-maybe"></td>
    </tr>
</table>

## Output flags {#sec-snappy-quick-reference-outputFlags}

Sub-dictionary: `outputFlags`

<table>
    <tr>
        <th>Keyword</th>
        <th>Options</th>
        <th>Default value</th>
        <th>Change?</th>
    </tr>
    <tr>
        <td>layerInfo</td>
        <td></td>
        <td></td>
        <td class="input-deprecated"></td>
    </tr>
</table>
