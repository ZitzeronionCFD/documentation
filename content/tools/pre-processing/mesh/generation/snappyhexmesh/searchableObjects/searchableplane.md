---
title: searchablePlane
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- input
- geometry

menu_id: searchableplane
license: CC-BY-NC-ND-4.0
menu_parent: snappyhexmesh-geometry
group: searchableobjects
---

<%= page_toc %>

![plane example](../images/searchablePlane-small.png)

An infinite plane. The plane can be used to cut-out part of the domain, where
the mesh in the plane normal direction will be preserved.  The plane can be
specified according to:

- __pointAndNormal__
    Plane is defined by point and normal vector

- __embeddedPoints__
    Plane is defined by 3 points

- __planeEquation__
    Employs the algebraic plane equation:

$$ ax + by + cz + d = 0 $$

<% assert :string_exists, "plane.C", "pointAndNormal" %>
<% assert :string_exists, "plane.C", "embeddedPoints" %>
<% assert :string_exists, "plane.C", "planeEquation" %>

## Usage

Plane defined by point and a normal vector:

    plane
    {
        type            searchablePlane;
        planeType       pointAndNormal;

        pointAndNormalDict
        {
            basePoint       (1 1 1);
            normal          (0 1 0);
        }
    }

<% assert :string_exists, "searchablePlane.H", "searchablePlane" %>
<% assert :string_exists, "plane.C", "pointAndNormal" %>
<% assert :string_exists, "plane.C", "basePoint" %>
<% assert :string_exists, "plane.C", "normal" %>

Plane defined by 3 points on the plane:

    plane
    {
        type            searchablePlane;
        planeType       embeddedPoints;

        embeddedPointsDict
        {
            point1          (1 1 1);
            point2          (0 1 0);
            point3          (0 0 1)
        }
    }

<% assert :string_exists, "searchablePlane.H", "searchablePlane" %>
<% assert :string_exists, "plane.C", "embeddedPoints" %>
<% assert :string_exists, "plane.C", "point1" %>
<% assert :string_exists, "plane.C", "point2" %>
<% assert :string_exists, "plane.C", "point3" %>

Plane defined by plane equation:

    plane
    {
        type            searchablePlane;
        planeType       planeEquation;

        planeEquationDict
        {
            a  0;
            b  0;
            c  1; // to create plane with normal towards +z direction ...
            d  2; // ... at coordinate: z = 2
        }
    }

<% assert :string_exists, "searchablePlane.H", "searchablePlane" %>
<% assert :string_exists, "plane.C", "planeEquation" %>
<% assert :string_exists, "plane.C", "a" %>
<% assert :string_exists, "plane.C", "b" %>
<% assert :string_exists, "plane.C", "c" %>
<% assert :string_exists, "plane.C", "d" %>
