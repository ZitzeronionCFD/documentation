---
title: fieldCoordinateSystemTransform
copyright:
- Copyright (C) 2019-2021 OpenCFD Ltd.
tags:
- coordinate system
- field
- function object
- post-processing
- transform
menu_id: function-objects-field-fieldcoordinatesystemtransform
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `fieldCoordinateSystemTransform` function object is used to convert a field
from the global Cartesian system to a local user-specified co-ordinate system.

### Operands

Operand       | Type                          | Location
:---          |:---                           |:---
 input        | `{vol,surface}<Type>Field(s)` | `$FOAM_CASE/<time>/<inpField>s`
 output file  | -                             | -
 output field | `{vol,surface}<Type>Field(s)` | `$FOAM_CASE/<time>/<outField>s`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `fieldCoordinateSystemTransform` function object
by using `functions` sub-dictionary in `system/controlDict` file:

To transform the global velocity into a local cylindrical system defined by

    origin: (0 0 0)
    axis:   (0 0 1)
    radius: (1 0 0)

the following entries may be used:

    transform1
    {
        // Mandatory entries (unmodifiable)
        type            fieldCoordinateSystemTransform;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        fields          (U);
        coordinateSystem
        {
            origin          (0 0 0);
            coordinateRotation
            {
                type            axesRotation;
                e3              (0 0 1);
                e1              (1 0 0);
            }
        }

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists,
   "fieldCoordinateSystemTransform.H", "fieldCoordinateSystemTransform" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "fieldCoordinateSystemTransform",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldCoordinateSystemTransform.C", "fields" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: fieldCoordinateSystemTransform  | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
fields        | Names of the operand fields         | wordList | yes  | -
coordinateSystem | Local coordinate system          | dict     | yes  | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

## Stored properties {#stored-properties}

### Fields

Each input field is transformed into a field with the name:

    <field>:Transformed

For the example above, the `U` field is transformed into

    U:Transformed

which is registered to the mesh database and written to the time directories
according to the `writeControl` entry.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/pipeCyclic" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/fieldCoordinateSystemTransform" %>

API:

- [Foam::functionObjects::fieldCoordinateSystemTransform](doxy_id://Foam::functionObjects::fieldCoordinateSystemTransform)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
