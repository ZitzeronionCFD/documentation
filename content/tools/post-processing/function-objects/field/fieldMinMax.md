---
title: fieldMinMax
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- field
- function object
- max
- min
- post-processing
menu_id: function-objects-field-fieldminmax
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `fieldMinMax` function object computes the values and locations of field
minima and maxima.
These are good indicators of calculation performance, e.g. to confirm that
predicted results are within expected bounds, or how well a case is converging.

Multiple fields can be processed, where for rank > 0 primitives, e.g. vectors
and tensors, the extrema can be calculated per component, or by magnitude.  In
addition, spatial location and local processor index are included in the output.

### Operands

Operand       | Type       | Location
:---          |:---        |:---
input         | -          | -
output file   | dat        | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
output field  | -          | -

## Usage {#usage}

Example of the `fieldMinMax` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    fieldMinMax1
    {
        // Mandatory entries (unmodifiable)
        type        fieldMinMax;
        libs        (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        mode        magnitude;
        fields      (<field1> <field2> ... <fieldN>);

        // Optional entries (runtime modifiable)
        location    true;

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "fieldMinMax.H", "fieldMinMax" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "fieldMinMax",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldMinMax.C", "fields" %>
<% assert :string_exists, "fieldMinMax.C", "location" %>
<% assert :string_exists, "fieldMinMax.C", "mode" %>
<% assert :string_exists, "fieldMinMax.C", "magnitude" %>
<% assert :string_exists, "fieldMinMax.C", "component" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: fieldMinMax              | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 fields       | List of operand fields              | wordRes | yes  | -
 location     | Write location of the min/max value | bool | no       | true
 mode   | Calculation mode: magnitude or component  | word | no     | magnitude

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Usage by the `postProcess` utility is not available.

## Stored properties  {#stored-properties}

### Reduced data

The following data are stored by the function object, and can be used by other
function objects:

- `min(<field>)` : Minimum field value
- `min(<field>)_cell` : Minimum field value cell
- `min(<field>)_position` : Minimum field value position
- `min(<field>)_processor` : Minimum field value processor
- `max(<field>)` : Maximum field value
- `max(<field>)_cell` : Maximum field value cell
- `max(<field>)_position` : Maximum field value position
- `max(<field>)_processor` : Maximum field value processor

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/MPPICInterFoam/twoPhasePachuka" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/turbulentFlatPlate" %>
- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoPimpleFoam/RAS/externalCoupledSquareBendLiq" %>

\ofAssert functionObjectUseExists multiphase/MPPICInterFoam/twoPhasePachuka fieldMinMax

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/fieldMinMax" %>

API:

- [Foam::functionObjects::fieldMinMax](doxy_id://Foam::functionObjects::fieldMinMax)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
