---
title: Common combinations
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-common-combinations
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

## Pressure-velocity systems {#pressure-velocity}

### Incompressible {#subsonic-incompressible}

<table>
<tr>
  <th colspan="2">Inlet</th> <th colspan="2">Outlet</th> <th>Stability</th>
</tr>
<tr>
  <th>Physics</th><th>OpenFOAM</th><th>Physics</th><th>OpenFOAM</th><th></th>
</tr>
<tr>
    <td>Volume flow rate</td>
    <td><%= link_to "flowRateInletVelocity",
    ref_id("boundary-conditions-flowRateInletVelocity") %></td>
    <td>Static pressure</td>
    <td><%= link_to "fixedValue",
    ref_id("boundary-conditions-fixedValue") %></td>
    <td>Excellent</td>
</tr>
<tr>
    <td>Total pressure</td>
    <td><%= link_to "totalPressure",
    ref_id("boundary-conditions-totalPressure") %></td>
    <td>Total pressure</td>
    <td><%= link_to "totalPressure",
    ref_id("boundary-conditions-totalPressure") %></td>
    <td>Very good</td>
</tr>
<tr>
    <td>Total pressure</td>
    <td><%= link_to "totalPressure",
    ref_id("boundary-conditions-totalPressure") %></td>
    <td>Static pressure</td>
    <td><%= link_to "fixedValue",
    ref_id("boundary-conditions-fixedValue") %></td>
    <td>Good</td>
</tr>
<tr>
    <td>Static pressure</td>
    <td><%= link_to "fixedValue",
    ref_id("boundary-conditions-fixedValue") %></td>
    <td>Static pressure</td>
    <td><%= link_to "fixedValue",
    ref_id("boundary-conditions-fixedValue") %></td>
    <td>Poor</td>
</tr>
</table>

### Compressible {#subsonic-compressible}

<table>
<tr>
  <th colspan="2">Inlet</th> <th colspan="2">Outlet</th> <th>Stability</th>
</tr>
<tr>
  <th>Physics</th><th>OpenFOAM</th><th>Physics</th><th>OpenFOAM</th><th></th>
</tr>
<tr>
    <td>Mass flow rate</td>
    <td><%= link_to "flowRateInletVelocity",
    ref_id("boundary-conditions-flowRateInletVelocity") %></td>
    <td>Static pressure</td>
    <td><%= link_to "fixedValue",
    ref_id("boundary-conditions-fixedValue") %></td>
    <td>Excellent</td>
</tr>
<tr>
    <td>Total pressure</td>
    <td><%= link_to "totalPressure",
    ref_id("boundary-conditions-totalPressure") %></td>
    <td>Total pressure</td>
    <td><%= link_to "totalPressure",
    ref_id("boundary-conditions-totalPressure") %></td>
    <td>Very good</td>
</tr>
<tr>
    <td>Total pressure</td>
    <td><%= link_to "totalPressure",
    ref_id("boundary-conditions-totalPressure") %></td>
    <td>Static pressure</td>
    <td><%= link_to "fixedValue",
    ref_id("boundary-conditions-fixedValue") %></td>
    <td>Good</td>
</tr>
<tr>
    <td>Static pressure</td>
    <td><%= link_to "fixedValue",
    ref_id("boundary-conditions-fixedValue") %></td>
    <td>Static pressure</td>
    <td><%= link_to "fixedValue",
    ref_id("boundary-conditions-fixedValue") %></td>
    <td>Poor</td>
</tr>
</table>

### Return flow options {#subsonic-return-flow}

<table>
<tr><th>Type</th> <th>Condition</th> <th>Stability</th></tr>
<tr>
    <td>Outflow</td>
    <td><%= link_to "zeroGradient",
    ref_id("boundary-conditions-zeroGradient") %></td>
    <td>Unstable if flow reverses</td>
</tr>
<tr>
    <td>Blocked</td>
    <td><%= link_to "inletOutlet",
    ref_id("boundary-conditions-inletOutlet") %></td>
    <td>Good, but unphysical</td>
</tr>
<tr>
    <td>Return flow I</td>
    <td><%= link_to "pressureInletOutlet",
    ref_id("boundary-conditions-pressure-inlet-outlet-velocity") %></td>
    <td>Good</td>
</tr>
<tr>
    <td>Return flow II</td>
    <td><%= link_to "totalPressure",
    ref_id("boundary-conditions-totalPressure") %></td>
    <td>Very good</td>
</tr>
</table>

## Heat transfer {#heat-transfer}

### Walls

<table>
<tr><th>Type</th> <th>Condition</th></tr>
<tr>
    <td>Adiabatic</td>
    <td><%= link_to "zeroGradient",
    ref_id("boundary-conditions-zeroGradient") %></td>
</tr>
<tr>
    <td>Fixed temperature</td>
    <td><%= link_to "fixedValue",
    ref_id("boundary-conditions-fixedValue") %></td>
</tr>
<tr>
    <td>Fixed heat flux</td>
    <td><%= link_to "fixedGradient",
    ref_id("boundary-conditions-fixedGradient") %></td>
</tr>
</table>

## Mesh motion {#mesh-motion}

### Walls

<table>
<tr><th>Type</th> <th>Condition</th></tr>
<tr>
    <td>Moving wall</td>
    <td><%= link_to "movingWallVelocity",
    ref_id("boundary-conditions-movingWallVelocity") %></td>
</tr>
</table>
