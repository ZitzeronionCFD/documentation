---
title: LambVector
copyright:
- Copyright (C) 2019-2021 OpenCFD Ltd.
tags:
- field
- function object
- Lamb
- post-processing
- vector
menu_id: function-objects-field-lambvector
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `LambVector` function object computes
'Lamb vector' \[m/s2\] <%= cite "truesdell_kinematics_2018" %> (p. 77) which is the
cross product of a velocity vector \[m/s\] and vorticity vector \[1/s\].

`LambVector` is defined as follows:

$$
\mathbf{l} = \vec{\omega} \times \u
$$

where $$\vec{\omega}(\mathbf{x},t) \equiv \vec{\omega}$$ is the
vorticity vector,
and $$\u (\mathbf{x}, t) \equiv \u$$ the velocity vector.

The motivation of `LambVector` function object is the literature-reported
quantitative connection between the Lamb vector (divergence) and the spatially
localised instantaneous fluid motions, e.g. high- and low-momentum fluid
parcels, which possess considerable level of capacity to affect the rate of
change of momentum, and to generate forces such as drag.

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `volVectorField`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `volScalarField`     | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `LambVector` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    LambVector1
    {
        // Mandatory entries (unmodifiable)
        type        LambVector;
        libs        (fieldFunctionObjects);

        // Optional (inherited) entries
        field           <inpField>;
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "LambVector/LambVector.H", "LambVector" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "LambVector",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: LambVector               | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Usage by the `postProcess` utility is not available.

An example of the usage involving the divergence computation of the `LambVector`
can be seen below:

In `controlDict.functions`:

    LambVector1
    {
        type        LambVector;
        libs        (fieldFunctionObjects);
        field       U;
    }

    div1
    {
        type        div;
        libs        (fieldFunctionObjects);
        field       LambVector;
    }

    fieldAverage1
    {
        type            fieldAverage;
        libs            (fieldFunctionObjects);
        writeControl    writeTime;
        fields
        (
            LambVector
            {
                mean        on;
                prime2Mean  off;
                base        time;
            }

           div(LambVector)
           {
               mean        on;
               prime2Mean  off;
               base        time;
           }
        );
    }

Please note that if `default divScheme` is `none`, in `fvSchemes.divSchemes`, a
numerical scheme should be chosen for related quantities, e.g. `div(LambVector)`
as follows:

    div(LambVector)  Gauss linear;

<% assert :string_exists, "LambVector.H", "LambVector" %>
<% assert :class_library_exists, "functionObjects/field/Make/files", "LambVector", "libfieldFunctionObjects" %>
<% assert :string_exists, "LambVector.C", "patches" %>

## Further information {#further-information}

Tutorial:

- [pisoFoam](ref_id://pisofoam)
  `cavity` tutorial: <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

\ofAssert functionObjectUseExists incompressible/pimpleFoam/LES/channel395DFSEM LambVector

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/LambVector" %>

API:

- [Foam::functionObjects::LambVector](doxy_id://Foam::functionObjects::LambVector)

Related:

- [Vorticity](ref_id://function-objects-field-vorticity)

<%= history "v1906", [{"v2006" => "LambVector"}, {"v1906" => "lambVector"}] %>

<!----------------------------------------------------------------------------->
