---
title: rotatingTotalPressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-rotatingTotalPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `rotatingTotalPressure` is a boundary condition that provides a total
pressure condition for patches in a rotating frame.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            rotatingTotalPressure;
        omega           <Function1<vector>>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "rotatingTotalPressureFvPatchScalarField.H", "rotatingTotalPressure" %>
<% assert :string_exists, "rotatingTotalPressureFvPatchScalarField.C", "omega" %>

where:

Property       | Description                             | Type    | Required | Default
---------------|-----------------------------------------|---------|----------|---------
`type`         | Type name: `rotatingTotalPressure`      | word    | yes      | -
`oemga`        | Angular velocity of the frame \[rad/s\] | Function1\<vector\> | yes  | -

The inherited entries are elaborated in:

- [Foam::totalPressureFvPatchScalarField](doxy_id://Foam::totalPressureFvPatchScalarField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/rotatingTotalPressure" %>

API:

- [Foam::rotatingTotalPressureFvPatchScalarField](doxy_id://Foam::rotatingTotalPressureFvPatchScalarField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
