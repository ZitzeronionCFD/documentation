---
title: sampledIsoSurface
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- post-processing
- sampling
- surface
menu_id: surfaces-isoSurface
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-sampling-surfaces
group: sampling-surfaces
---

<%= page_toc %>

Creates iso-surfaces according to constant field values on a mesh.

## Usage {#usage}

Basic operation of the `isoSurface` surface comprises:

    surfaces1
    {
        type            surfaces;
        libs            (sampling);

        surfaces
        {
            mySurface1
            {
                type            isoSurface;
                isoField        <field-name>;
                isoValues       (<field-values>);

                // Optional entries
                average         false;
                triangulate     false;
                simpleSubMesh   false;

                regularise      ;
                snap            true;
                mergeTol        ;
                bounds          ;

                isoMethod       <isoMethod-type>;
            }
        }
    }

<% assert :string_exists, "sampledSurfaces.H", "surfaces" %>
<% assert :class_library_exists, "sampling/Make/files", "sampledSurfaces", "libsampling" %>
<% assert :string_exists, "sampledIsoSurface.H", "sampledIsoSurface" %>
<% assert :string_exists, "sampledIsoSurface.C", "isoField" %>
<% assert :string_exists, "sampledIsoSurface.C", "isoValues" %>
<% assert :string_exists, "sampledIsoSurface.C", "average" %>
<% assert :string_exists, "sampledIsoSurface.C", "triangulate" %>
<% assert :string_exists, "sampledIsoSurface.C", "simpleSubMesh" %>
<% assert :string_exists, "sampledIsoSurface.C", "regularise" %>
<% assert :string_exists, "sampledIsoSurface.C", "snap" %>
<% assert :string_exists, "sampledIsoSurface.C", "mergeTol" %>
<% assert :string_exists, "sampledIsoSurface.C", "bounds" %>

isoMethod types:

- `default`:
- `cell`:
- `point`:
- `topo`:
<% assert :string_exists, "isoSurfaceParams.C", "default" %>
<% assert :string_exists, "isoSurfaceParams.C", "cell" %>
<% assert :string_exists, "isoSurfaceParams.C", "point" %>
<% assert :string_exists, "isoSurfaceParams.C", "topo" %>

filter types:

- `none`:
- `partial`:
- `full`:
- `clean`:
- `cell`:
- `diagCell`
<% assert :string_exists, "isoSurfaceParams.C", "none" %>
<% assert :string_exists, "isoSurfaceParams.C", "partial" %>
<% assert :string_exists, "isoSurfaceParams.C", "full" %>
<% assert :string_exists, "isoSurfaceParams.C", "clean" %>
<% assert :string_exists, "isoSurfaceParams.C", "cell" %>
<% assert :string_exists, "isoSurfaceParams.C", "diagCell" %>

## Sample output {#sample-output}

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/sampling/sampledSurface/isoSurface" %>

API:

- [Foam::isoSurface](doxy_id://Foam::sampledIsoSurface)

Example usage:

- <%= repo_link2("$FOAM_TUTORIALS/compressible/rhoSimpleFoam/squareBend") %>
- <%= repo_link2("$FOAM_TUTORIALS/incompressible/pimpleFoam/RAS/propeller") %>
