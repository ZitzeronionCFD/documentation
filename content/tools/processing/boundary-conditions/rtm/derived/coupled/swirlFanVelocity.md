---
title: swirlFanVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- coupled
menu_id: boundary-conditions-swirlFanVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-coupled
group: bcs-coupled
---

<%= page_toc %>

## Description

The `swirlFanVelocity` is a boundary condition that provides a jump condition
for velocity across a cyclic pressure jump condition and applies a
transformation to the velocity.

The velocity jump is specified with a swirl component as follows:

$$
  u_{tan} = \frac{\Delta_P}{r_{eff} f_{eff} \omega_{rpm} \frac{\pi}{30}}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$u_{tan}$$           | Tangential component of velocity
$$\Delta_P$$          | Pressure drop across the cyclic
$$r_{eff}$$           | Effective radius
$$f_{eff}$$           | Fan efficiency coefficient
$$\omega_{rpm}$$      | RPM of the fan

Alternatively an inner and outer radii can be used instead of $$r_{eff}$$.
The $$u_{tan}$$ is as follows for $$r > r_{inner}$$ and $$r < r_{outer}$$:

$$
  u_{tan} = \frac{\Delta_P}{r f_{eff} \frac{\omega_{rpm}}{\pi 30}}
$$

with $$r = x_{f} - x_{origin}$$, and $$f$$ is the face center.

The radial velocity is zero in the this model.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            swirlFanVelocity;
        rpm             <Function1<scalar>>;

        // Optional entries
        phi             <word>;
        p               <word>;
        rho             <word>;
        origin          <vector>;
        fanEff          <scalar>;
        rEff            <scalar>;
        rInner          <scalar>;
        rOuter          <scalar>;
        useRealRadius   <bool>;

        // Inherited entries
        patchType       cyclic;
        ...
    }

where:

Property              | Description                                | Type    | Required | Default
----------------------|--------------------------------------------|---------|----------|---------
`type`                | Type name: `swirlFanVelocity`              | word    | yes      | -
`rpm`                 | RPM of the fan                     | Function1\<scalar\> | yes  | -
`phi`                 | Name of flux field                         | word    | no       | phi
`p`                   | Name of pressure field                     | word    | no       | p
`rho`                 | Name of density field                      | word    | no       | rho
`origin`              | Fan centre                                 | vector  | no       | Zero
`fanEff`              | Fan efficiency                             | scalar  | no       | 1
`rEff`                | Effective radius                           | scalar  | no       | 0
`rInner`              | Inner radius                               | scalar  | no       | 0
`rOuter`              | Outer radius                               | scalar  | no       | 0
`useRealRadius`       | Flag to use inner/outer radii              | bool    | no       | false

<% assert :string_exists, "swirlFanVelocityFvPatchField.H", "swirlFanVelocity" %>
<% assert :string_exists, "swirlFanVelocityFvPatchField.C", "rpm" %>
<% assert :string_exists, "swirlFanVelocityFvPatchField.C", "phi" %>
<% assert :string_exists, "swirlFanVelocityFvPatchField.C", "p" %>
<% assert :string_exists, "swirlFanVelocityFvPatchField.C", "rho" %>
<% assert :string_exists, "swirlFanVelocityFvPatchField.C", "origin" %>
<% assert :string_exists, "swirlFanVelocityFvPatchField.C", "fanEff" %>
<% assert :string_exists, "swirlFanVelocityFvPatchField.C", "rEff" %>
<% assert :string_exists, "swirlFanVelocityFvPatchField.C", "rInner" %>
<% assert :string_exists, "swirlFanVelocityFvPatchField.C", "rOuter" %>
<% assert :string_exists, "swirlFanVelocityFvPatchField.C", "userRealRadius" %>

The inherited entries are elaborated in:

- [Foam::fixedJumpFvPatchField](doxy_id://Foam::fixedJumpFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

- Negative `rpm` will reverse the input tangential direction.
- This boundary condition needs to be used with a pressure-jump (e.g. fan)
  condition with a non-zero dp, otherwise no swirl will be applied (dp=0).
- Please ensure physical and complementary set-ups for the pressure-jump
  and `swirlFanVelocity` boundary conditions.
  {: .note}

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/swirlFanVelocity" %>

API:

- [Foam::swirlFanVelocityFvPatchField](doxy_id://Foam::swirlFanVelocityFvPatchField)

<%= history "v1806" %>

<!----------------------------------------------------------------------------->
