---
title: log
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- ln
- log
- post-processing
menu_id: function-objects-field-log
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `log` function object computes the
natural logarithm of an input `volScalarField`.

$$
    f = s \ln(\max(f_0, a)) + t
$$

where:

$$f$$
: Output `volScalarField`

$$f_0$$
: Input `volScalarField`

$$\ln$$
: Natural logarithm operator

$$a$$
: Clip scalar

$$s$$
: Scaling factor

$$t$$
: Offset factor

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `volScalarField`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `volScalarField`     | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `log` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    log1
    {
        // Mandatory entries (unmodifiable)
        type            log;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entry (runtime modifiable)
        field           <inpField>;

        // Optional entries (runtime modifiable)
        clip            1e-3;
        checkDimensions false;
        scale           1.0;
        offset          0.0;

        // Optional (inherited) entries
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "log.H", "log" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "log",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "log.C", "clip" %>
<% assert :string_exists, "log.C", "checkDimensions" %>
<% assert :string_exists, "log.C", "scale" %>
<% assert :string_exists, "log.C", "offset" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: log                      | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 field        | Name of the operand field           | word | yes      | -
 clip         | Value to clip the operand field values to prevent zero or negative input  | scalar | no  | SMALL
 checkDimensions | Flag to check dimensions of the operand field | bool   | no  | true
 scale        | Scaling factor - `s` above         | scalar | no     | 1.0
 offset       | Offset factor - `t` above          | scalar | no     | 0.0

The inherited entries are elaborated in:

- [functionObject.H](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func "log(<inpField>)" -scale 1.0 -offset 0.0

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/log" %>

API:

- [Foam::functionObjects::log](doxy_id://Foam::functionObjects::log)

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
