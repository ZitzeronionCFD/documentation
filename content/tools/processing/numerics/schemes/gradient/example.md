---
title: Gradient scheme example
short_title: Example
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- example
menu_id: schemes-gradientexample
license: CC-BY-NC-ND-4.0
menu_parent: schemes-gradient
group: schemes-examples
---

<%= page_toc %>

## Introduction {#introduction}

The test case comprises a 2-D section comprising extruded triangle cells.
An initial field is assigned the mesh cell centre values such that
the computed gradient should take the value of 1 in each co-ordinate direction.
By changing the choice of gradient scheme we can observe how each performs.

## Case set-up {#case-set-up}

Gradient schemes under test:

- Gauss linear
- Gauss pointLinear
- leastSquares

Interpolation schemes under test:

- Unlimited
- [cellLimited](ref_id://schemes-gradient-celllimited)
- [cellMDLimited](ref_id://schemes-gradient-cellmdlimited)
- [faceLimited](ref_id://schemes-gradient-facelimited)
- [faceMDLimited](ref_id://schemes-gradient-facemdlimited)

### Boundary conditions {#boundary-conditions}

Only the left and right domain boundaries are set to physical patch types; the
remainder are set to the [empty](ref_id://boundary-conditions-empty)
constraint to enforce the 2-D system.

### Mesh {#mesh}

The mesh comprises extruded triangle cells with low non-orthogonality with
average and maximum values of 7 and 26 deg, respectively.

### Scheme controls {#scheme-controls}

    grad(Cc)        <scheme>

## Results {#results}

The following images show the percentage error of the magnitude of the gradient
of the cell centres, defined as:
$$
    \textrm{error} = 100 \frac{\left|\grad(C_c)\right| - 2^{0.5}}{2^{0.5}}
$$
The range is set to +/- 10% for all images.

### Unlimited {#results-unlimited}

| ![Gauss linear](gradient-test-Gauss-linear.png) | ![Gauss point linear](gradient-test-Gauss-pointLinear.png)| ![Least squares](gradient-test-leastSquares.png) |
{: .noBorder}

### Cell limited {#results-cell-limited}

| ![Gauss linear](gradient-test-cellLimited-Gauss-linear-1.png) |  ![Gauss point linear](gradient-test-cellLimited-Gauss-pointLinear-1.png) | ![Least squares](gradient-test-cellLimited-leastSquares-1.png) |
{: .noBorder}

### Face limited {#results-face-limited}

| ![Gauss linear](gradient-test-faceLimited-Gauss-linear-1.png) | ![Gauss point linear](gradient-test-faceLimited-Gauss-pointLinear-1.png) | ![Least squares](gradient-test-faceLimited-leastSquares-1.png) |
{: .noBorder}

### Multi-directional cell limited {#results-cell-md-limited}

| ![Gauss linear](gradient-test-cellMDLimited-Gauss-linear-1.png) | ![Gauss point linear](gradient-test-cellMDLimited-Gauss-pointLinear-1.png) | ![Least squares](gradient-test-cellMDLimited-leastSquares-1.png) |
{: .noBorder}

### Multi-directional face limited {#results-face-md-limited}

| ![Gauss linear](gradient-test-faceMDLimited-Gauss-linear-1.png) | ![Gauss point linear](gradient-test-faceMDLimited-Gauss-pointLinear-1.png) | ![Least squares](gradient-test-faceMDLimited-leastSquares-1.png) |
{: .noBorder}