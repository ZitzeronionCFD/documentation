---
title: histogram
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- histogram
- post-processing
menu_id: function-objects-field-histogram
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `histogram` function object computes
the volume-weighted histogram of an input `volScalarField`.

### Operands

Operand       | Type            | Location
:---          |:---             |:---
 input        | volScalarField  | `$FOAM_CASE/<time>/<inpField>`
 output file  | dat             | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
 output field | -               | -

The set written contains two columns, the first the volume averaged values,
the second the raw bin count.

## Usage {#usage}

Example of the `histogram` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    histogram1
    {
        // Mandatory entries (unmodifiable)
        type            histogram;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entries (runtime modifiable)
        field       p;
        nBins       100;
        setFormat   gnuplot;

        // Optional entries (runtime modifiable)
        max         5;
        min        -5;

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "histogram.H", "histogram" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "histogram",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "histogram.C", "field" %>
<% assert :string_exists, "histogram.C", "nBins" %>
<% assert :string_exists, "histogram.C", "setFormat" %>
<% assert :string_exists, "histogram.C", "max" %>
<% assert :string_exists, "histogram.C", "min" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: histogram                | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 field        | Name of operand field               | word | yes      | -
 nBins        | Number of histogram bins            | label | yes     | -
 setFormat    | Output format                       | word | yes      | -
 max          | Maximum value sampled               | scalar | no     | fieldMax
 min          | minimum value sampled               | scalar | no     | 0.0

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Usage by the `postProcess` utility is not available.

### Notes on entries

If `max` is not provided it will use the field's min and max as the bin
extremes. If `max` is provided but not `min`, it will use 0.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/histogram" %>

API:

- [Foam::functionObjects::histogram](doxy_id://Foam::functionObjects::histogram)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
