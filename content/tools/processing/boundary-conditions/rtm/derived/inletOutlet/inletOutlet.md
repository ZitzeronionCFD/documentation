---
title: inletOutlet
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-inletOutlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
groups:
- bcs-inletoutlet
---

<%= page_toc %>

## Description

The `inletOutlet` is a generic boundary condition that provides an outflow
condition, with specified inflow for the case of return flow.

## Usage {#usage}

The condition requires entries in both theboundary` and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            inletOutlet;
        inletValue      <Field<Type>>;

        // Optional entries
        phi             <word>;

        // Inherited entries;
        ...
    }

<% assert :string_exists, "inletOutletFvPatchField.H", "inletOutlet" %>
<% assert :string_exists, "inletOutletFvPatchField.C", "inletValue" %>
<% assert :string_exists, "inletOutletFvPatchField.C", "phi" %>

where:

Property       | Description                    | Type    | Required | Default
---------------|--------------------------------|---------|----------|---------
`type`         | Type name: `inletOutlet`       | word    | yes      | -
`inletValue`   | Patch field for reverse flow   | Field\<Type\> | yes  | -
`phi`          | Name of flux field             | word    | no       | phi

The inherited entries are elaborated in:

- [Foam::mixedFvPatchField](doxy_id://Foam::mixedFvPatchField)
<!-- end of the list -->

- The mode of operation is determined by the sign of the flux across the
patch faces.
- Sign conventions:
  - Positive flux (out of domain): apply zero-gradient condition.
  - Negative flux (into domain): apply the `inletValue` fixed-value.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/LES/pitzDaily" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/inletOutlet" %>

API:

- [Foam::inletOutletFvPatchField](doxy_id://Foam::inletOutletFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
