---
title: Solvers
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- function object
- post-processing
- solvers
menu_id: function-objects-solvers
license: CC-BY-NC-ND-4.0
menu_parent: post-processing-function-objects
---

<%= page_toc %>

Options:

<%= insert_models "function-objects-solvers" %>

Related:

- [Source documentation](doxy_id://grpSolversFunctionObjects)
