---
title: atmBoundaryLayerInletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-atmBoundaryLayerInletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-atmospheric
group: bcs-atmospheric
---

<%= page_toc %>

## Description

The `atmBoundaryLayerInletVelocity` is a boundary condition that provides
a log-law type ground-normal inlet boundary condition for the streamwise
component of wind velocity (i.e. `u`) for homogeneous, two-dimensional,
dry-air, equilibrium and neutral atmospheric boundary layer modelling.

The ground-normal streamwise flow speed profile expression:

$$
    u = \frac{u^*}{\kappa} \ln \left( \frac{z - d + z_0}{z_0} \right)
$$

$$
    v = w = 0
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$u$$        | Ground-normal streamwise flow speed profile    \[m/s\]
$$v$$        | Spanwise flow speed                            \[m/s\]
$$w$$        | Ground-normal flow speed                       \[m/s\]
$$u^*$$      | Friction velocity                              \[m/s\]
$$\kappa$$   | von Kármán constant                            \[-\]
$$z$$        | Ground-normal coordinate component             \[m\]
$$d$$        | Ground-normal displacement height              \[m\]
$$z_0$$      | Aerodynamic roughness length                   \[m\]

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            atmBoundaryLayerInletVelocity;

        // Inherited entries
        ...
    }

<% assert :string_exists, "atmBoundaryLayerInletVelocityFvPatchVectorField.H", "atmBoundaryLayerInletVelocity" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `atmBoundaryLayerInletVelocity` | word    | yes      | -

The inherited entries are elaborated in:

- atmBoundaryLayer.H
- inletOutletFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/turbineSiting" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/derivedFvPatchFields/atmBoundaryLayerInletVelocity" %>

API:

- [Foam::atmBoundaryLayerInletVelocityFvPatchVectorField](doxy_id://Foam::atmBoundaryLayerInletVelocityFvPatchVectorField)

<%= history "v1806" %>

<!----------------------------------------------------------------------------->
