---
title: Curle
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- acoustics
- Curle
- field
- function object
- post-processing
- pressure
menu_id: function-objects-field-curle
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `Curle` function object computes the acoustic
pressure based on Curle's analogy.

Curle's analogy is implemented as:

$$
    p' = \frac{1}{4 \pi}
            \frac{\vec{r}}{| \vec{r} | ^2} \cdot
            \left(
            \frac{\vec{F}}{| \vec{r} |}
            + \frac{1}{c_0}\frac{d\vec{F}}{dt}
            \right)
$$

where:

$$p'$$
: Curle's acoustic pressure \[Pa\] or \[Pa (m3/kg)\]

$$c_0$$
: Reference speed of sound \[m/s\]

$$\vec{r}$$
: Distance vector to observer locations \[m\]

$$\vec{F}$$
: Force \[N\] or \[N (m3/kg)\]

$$t$$
: Time \[s\]

### Operands

Operand        | Type           | Location
:---           |:---            |:---
input          | -              | -
output file 1  | dat            | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
output file 2  | surface file   | `$FOAM_CASE/<outputDir>/<time>/<surfaceName>`
output field   | -              | -

## Usage {#usage}

Example of the `Curle` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    Curle1
    {
        // Mandatory entries (unmodifiable)
        type            Curle;
        libs            (fieldFunctionObjects);
        input           <point|surface>;
        output          <point|surface>;
        patches         (<patch1> <patch2> ... <patchN>);
        c0              343;

        // Conditional mandatory entries (runtime modifiable)
        // if input=point
        observerPositions    (<point1> <point2> ... <pointN>);

        // if input=surface
        surface              <surfaceFileName>;    // "inputSurface.obj";

        // if output=surface
        surfaceType          <surfaceType>;        // ensight;
        formatOptions
        {
            vtk
            {
                format  ascii;
            }
        }

        // Optional entries (runtime modifiable)
        p               p;

        // Optional (inherited) entries
        writePrecision  8;
        writeToFile     true;
        useUserTime     true;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "Curle/Curle.H", "Curle" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "Curle",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "Curle/Curle.C", "input" %>
<% assert :string_exists, "Curle/Curle.C", "output" %>
<% assert :string_exists, "Curle/Curle.C", "c0" %>
<% assert :string_exists, "Curle/Curle.C", "observerPositions" %>
<% assert :string_exists, "Curle/Curle.C", "surface" %>
<% assert :string_exists, "Curle/Curle.C", "surfaceType" %>
<% assert :string_exists, "Curle/Curle.C", "formatOptions" %>
<% assert :string_exists, "Curle/Curle.C", "patches" %>
<% assert :string_exists, "Curle/Curle.C", "p" %>
<% assert :string_exists, "writeFile.C", "writePrecision" %>
<% assert :string_exists, "writeFile.C", "writeToFile" %>
<% assert :string_exists, "writeFile.C", "useUserTime" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: Curle                    | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 patches      | Sound generation patch names        | wordList |  yes | -
 c0           | Reference speed of sound            | scalar | yes    | -
 input        | Input type                          | word | yes      | -
 output       | Output type                         | word | yes      | -
 p            | Pressure field name                 | word |  no      | p
 observerPositions | List of observer positions (x y z) | pointList | conditional | -
 surface      | Input surface file name             | word | conditional | -
 surfaceType  | Output surface type                 | word | conditional | -
 formatOptions | Dictionary of surface format options | dictionary | conditional | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [writeFile](ref_id://function-objects-field-writefile)

Options for the `input` and `output` entries:

    point   | Operand is a set of points
    surface | Operand is a surface

Usage by the `postProcess` utility is not available.

### Notes on entries

- Only the normal-pressure force is included in the force calculation.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/vortexShed" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/Curle/" %>

API:

- [Foam::functionObjects::Curle](doxy_id://Foam::functionObjects::Curle)

<%= history "v1706" %>

<!----------------------------------------------------------------------------->
