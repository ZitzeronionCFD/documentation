---
title: Smooth
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- solvers
- smooth
menu_id: smooth-smooth
license: CC-BY-NC-ND-4.0
menu_parent: linearequationsolvers-smooth
group: solvers-smooth
---

<%= page_toc %>

## Properties {#properties}

- symmetric or asymmetric matrices
- run-time selectable [smoother](<%= ref_id "linearequationsolvers-smooth", "smoothers" %>)

## Usage {#usage}

    solver          smooth;
    smoother        <smoother>;
    relTol          <relative tolerance>;
    tolerance       <absolute tolerance>;

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/OpenFOAM/matrices/LduMatrix/Solvers/SmoothSolver" %>

API:

- [Foam::smoothSolver](doxy_id://Foam::smoothSolver)
