---
title: Inlet or outlet conditions
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-inletOutlets
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

The boundary conditions in this group are applicable to both [inlet](../inlet)
and [outlet](../outlet) patches.

<%= insert_models "bcs-inletoutlet" %>

## Further information {#further-information}

See also

- [grpInletOutletBoundaryConditions](doxy_id://grpInletOutletBoundaryConditions)
