---
title: Multi-directional face-limited gradient scheme
short_title: MD face-limited
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- limited
- gradient
menu_id: schemes-gradient-facemdlimited
license: CC-BY-NC-ND-4.0
#menu_parent: schemes-gradient
group: schemes-gradient-limiter
---

<%= page_toc %>

## Properties {#properties}

## Usage {#usage}

The scheme is specified using:

    gradSchemes
    {
        default         none;
        grad(U)         faceMDLimited <scheme> <coefficient>;
    }

The coefficient should be specified in the range zero to one, where values of

## Further information {#further-information}

Source code

- [Foam::fv::faceMDLimitedGrad](doxy_id://Foam::fv::faceMDLimitedGrad)
