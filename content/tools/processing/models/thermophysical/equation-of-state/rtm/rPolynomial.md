---
title: Reciprocal polynomial
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- equation of state
- polynomial
menu_id: equationOfState-rPolynomial
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-equationOfState
group: equation-of-state-models
---

<%= page_toc %>

Density, $$\rho$$ \[kg/m$$^3$$\]

$$
\rho = \frac{1}{c_0 + c_1 T + c_2 T^2 + c_3 p + c_4 p T}
$$

Entropy, $$ S $$ \[J/kg/K \]

$$
S = 0
$$

Compressibility, $$ \psi $$ \[s$$^2$$/m$$^2$$\]

$$
\psi = \left( \rho (c_3 + c_4 T) \right)^2
$$

## Usage

The `rPolynomial` equation of state is specified as:

~~~
equationOfState
{
    C       (<c0> <c1> <c2> <c3> <c4>)
}
~~~

Where

- `C` : Polynomial coefficients (x5)

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/equationOfState/rPolynomial" %>
