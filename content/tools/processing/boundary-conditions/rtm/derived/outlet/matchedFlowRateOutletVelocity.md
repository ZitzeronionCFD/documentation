---
title: matchedFlowRateOutletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- outlet
menu_id: boundary-conditions-matchedFlowRateOutletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-outlet
group: bcs-outlet-velocity
---

<%= page_toc %>

## Description

The `matchedFlowRateOutletVelocity` is a velocity-outlet boundary condition that
corrects the extrapolated velocity to match the flow rate of the specified
corresponding inlet patch.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            matchedFlowRateOutletVelocity;

        // Optional entries
        inletPatch      <word>;
        rho             <word>;
        volumetric      <bool>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "matchedFlowRateOutletVelocityFvPatchVectorField.H", "matchedFlowRateOutletVelocity" %>
<% assert :string_exists, "matchedFlowRateOutletVelocityFvPatchVectorField.C", "inletPatch" %>
<% assert :string_exists, "matchedFlowRateOutletVelocityFvPatchVectorField.C", "rho" %>
<% assert :string_exists, "matchedFlowRateOutletVelocityFvPatchVectorField.C", "volumetric" %>

where:

Property             | Description                        | Type    | Required | Default
---------------------|------------------------------------|---------|----------|---------
`type`               | Type name: `matchedFlowRateOutletVelocity`   | word    | yes      | -
`inletPatch`         | Name of patch from which the corresponding flow rate is obtained  | word    | yes      | -
`rho`                | Name of the density field used to normalize the mass flux  | word    | no       | rho
`volumetric`         | Flag to enable volumetric- (true) of mass-based flow rate  | bool    | no       | true

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/matchedFlowRateOutletVelocity" %>

API:

- [Foam::matchedFlowRateOutletVelocityFvPatchVectorField](doxy_id://Foam::matchedFlowRateOutletVelocityFvPatchVectorField)

<%= history "v1712" %>

<!----------------------------------------------------------------------------->
