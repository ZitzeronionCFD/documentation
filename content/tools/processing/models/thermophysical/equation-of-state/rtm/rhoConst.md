---
title: Constant density
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- equation of state
- incompressible
menu_id: equationOfState-rhoConst
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-equationOfState
group: equation-of-state-models
---

<%= page_toc %>

Density, $$\rho$$ \[kg/m$$^3$$\]

$$
\rho = \rho_0
$$

Entropy, $$ S $$ \[J/kg/K \]

$$
S = 0
$$

Compressibility, $$ \psi $$ \[s$$^2$$/m$$^2$$\]

$$
\psi = 0
$$

## Usage

The `rhoConst` equation of state is specified as:

~~~
equationOfState
{
    rho         <scalar>;
}
~~~

Where

- `rho` : Constant density \[kg/m$$^3$$\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/equationOfState/rhoConst" %>
