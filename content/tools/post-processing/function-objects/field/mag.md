---
title: mag
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- mag
- post-processing
menu_id: function-objects-field-mag
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `mag` function object computes the magnitude of an input field.

### Operands

Operand      | Type                       | Location
:---         |:---                        |:---
input        | `{vol,surface}<Type>Field` | `$FOAM_CASE/<time>/<inpField>`
output file  | -                          | -
output field | `{vol,surface}ScalarField` | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `mag` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    mag1
    {
        // Mandatory entries (unmodifiable)
        type            mag;
        libs            (fieldFunctionObjects);

        // Mandatory (inherited) entries (runtime modifiable)
        field           <inpField>;

        // Optional (inherited) entries
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "mag/mag.H", "mag" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "mag",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: mag                      | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 field        | Name of the operand field           | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func "mag(<field>)"

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/mag" %>

API:

- [Foam::functionObjects::mag](doxy_id://Foam::functionObjects::mag)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
