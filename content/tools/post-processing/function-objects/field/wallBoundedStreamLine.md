---
title: wallBoundedStreamLine
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- streamline
- wall
menu_id: function-objects-field-wallboundedstreamline
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `wallBoundedStreamLine` function object
generates streamline data by sampling a set of user-specified fields along a
particle track, transported by a user-specified velocity field, constrained
to a patch.

### Operands

Operand       | Type        | Location
:---          |:---         |:---
input         | -           | -
output file   | -           | `$FOAM_CASE/postProcessing/<FO>/<time>/<file>`
output field  | -           | -

## Usage {#usage}

Example of the `wallBoundedStreamLine` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    wallBoundedStreamLine1
    {
        // Mandatory entries (unmodifiable)
        type            wallBoundedStreamLine;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        U               <fieldTrack>;
        fields          (<fieldTrack> <field1> ... <fieldN>);
        setFormat       vtk;
        direction       bidirectional;
        lifeTime        10000;
        cloud           particleTracks;
        seedSampleSet
        {
            type        patchSeed;
            patches     (wall);
            axis        x;
            maxPoints   20000;
        }

        // Optional entries (runtime modifiable)
        bounds          (0.2 -10 -10)(0.22 10 10);
        trackLength     1e-3;
        nSubCycle       1;
        interpolationScheme cellPoint;

        // Deprecated
        // trackForward true;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists,
   "wallBoundedStreamLine/wallBoundedStreamLine.H", "wallBoundedStreamLine" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "wallBoundedStreamLine",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "wallBoundedStreamLine.C", "U" %>
<% assert :string_exists, "wallBoundedStreamLine.C", "fields" %>
<% assert :string_exists, "wallBoundedStreamLine.C", "setFormat" %>
<% assert :string_exists, "wallBoundedStreamLine.C", "direction" %>
<% assert :string_exists, "wallBoundedStreamLine.C", "lifeTime" %>
<% assert :string_exists, "wallBoundedStreamLine.C", "cloud" %>
<% assert :string_exists, "wallBoundedStreamLine.C", "seedSampleSet" %>
<% assert :string_exists, "wallBoundedStreamLine.C", "bounds" %>
<% assert :string_exists, "wallBoundedStreamLine.C", "trackLength" %>
<% assert :string_exists, "wallBoundedStreamLine.C", "nSubCycle" %>
<% assert :string_exists, "wallBoundedStreamLine.C", "interpolationScheme" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: wallBoundedStreamLine    | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 U            | Name of tracking velocity field     | word |  yes  | -
 fields       | Names of operand fields to sample   | wordList | yes | -
 setFormat    | Type of output data                 | word |  yes  | -
 direction    | Direction to track                  | vector | yes | -
 lifetime     | Maximum number of particle tracking steps | label | yes | -
 cloud        | Name of cloud                       | word |  yes  | -
 seedSampleSet| Name of seeding method (see below)  | word |  yes  | -
 bounds       | Bounding box to trim tracks         | vector | no | invertedBox
 trackLength  | Tracking segment length             | scalar | no | VGREAT
 nSubCycle    | Number of tracking steps per cell   | label  | no | 1
 interpolationScheme | Interp. scheme for sample    | word | no | cellPoint

Options for the `seedSampleSet` entry:

    uniform   | uniform particle seeding
    cloud     | cloud of points
    patchSeed | seeding via patch faces
    triSurfaceMeshPointSet | points according to a tri-surface mesh

Options for the `setFormat` entry:

    csv
    ensight
    gnuplot
    jplot
    nastran
    raw
    vtk
    xmgr

Options for the `direction` entry:

    bidirectional
    forward
    backward

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

### Notes on entries

- When specifying the track resolution, the `trackLength` or `nSubCycle`
option should be used.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/motorBike" %>
- <%= repo_link2 "$FOAM_TUTORIALS/mesh/snappyHexMesh/motorBike_leakDetection" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/wallBoundedStreamLine" %>

API:

- [Foam::functionObjects::wallBoundedStreamLine](doxy_id://Foam::functionObjects::wallBoundedStreamLine)

<%= history "v1606+" %>

<%= page_toc %>

<!----------------------------------------------------------------------------->