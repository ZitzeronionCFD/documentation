---
title: Corrected surface-normal gradient scheme
short_title: Corrected
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- gradient
menu_id: schemes-sngrad-corrected
license: CC-BY-NC-ND-4.0
menu_parent: schemes-sngrad
group: schemes-sngrad
---

<%= page_toc %>

$$
    \snGrad Q
  =
    \underbrace{\alpha \frac{Q_P - Q_N}{| \vec{d} |}}_{\mathrm{implicit}}
  + \underbrace{\left( \hat{\vec{n}} - \alpha \hat{\vec{d}} \right) \dprod \left( \grad Q \right)_f}_{\mathrm{explicit\ correction}}
$$

where
$$
    \alpha = \frac{1}{cos(\theta)}
$$

## Properties {#properties}

- Central-difference snGrad scheme with non-orthogonal correction
- delta coefficients given by: nonOrthDeltaCoeffs
- explicit non-orthogonal correction

## Usage {#usage}

The scheme is specified using:

    snGradSchemes
    {
        default         none;
        grad(U)         corrected;
    }

## Further information {#further-information}

Source code

- [Foam::fv::correctedSnGrad](doxy_id://Foam::fv::correctedSnGrad)
