---
title: waveMaker
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-waveMaker
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-wave
group: bcs-wave
---

<%= page_toc %>

## Description

The `waveMaker` is a boundary condition that provides
a point motion boundary condition to generate waves based on either piston
or flap motions.

#### Reference

        Hughes, S.A. (1993).
        Physical Models And Laboratory Techniques In Coastal Engineering.
        Advanced Series On Ocean Engineering, volume 7.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            waveMaker;
        motionType      <word>;
        n               <vector>;
        initialDepth    <scalar>;
        wavePeriod      <scalar>;
        waveHeight      <scalar>;
        wavePhase       <scalar>;
        rampTime        <scalar>;

        // Optional entries
        waveAngle       <scalar>;
        startTime       <scalar>;
        secondOrder     <bool>;
        nPaddle         <label>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "waveMakerPointPatchVectorField.H", "waveMaker" %>
<% assert :string_exists, "waveMakerPointPatchVectorField.C", "motionType" %>
<% assert :string_exists, "waveMakerPointPatchVectorField.C", "n" %>
<% assert :string_exists, "waveMakerPointPatchVectorField.C", "initialDepth" %>
<% assert :string_exists, "waveMakerPointPatchVectorField.C", "wavePeriod" %>
<% assert :string_exists, "waveMakerPointPatchVectorField.C", "waveHeight" %>
<% assert :string_exists, "waveMakerPointPatchVectorField.C", "wavePhase" %>
<% assert :string_exists, "waveMakerPointPatchVectorField.C", "rampTime" %>
<% assert :string_exists, "waveMakerPointPatchVectorField.C", "waveAngle" %>
<% assert :string_exists, "waveMakerPointPatchVectorField.C", "startTime" %>
<% assert :string_exists, "waveMakerPointPatchVectorField.C", "secondOrder" %>
<% assert :string_exists, "waveMakerPointPatchVectorField.C", "nPaddle" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `waveMaker`                     | word    | yes      | -
`motionType`           | Motion type                                | word    | yes      | -
`n`                    | Direction of motion                        | vector  | yes      | -
`initialDepth`         | Initial depth                              | scalar  | yes      | -
`wavePeriod`           | Wave period                                | scalar  | yes      | -
`waveHeight`           | Wave height                                | scalar  | yes      | -
`wavePhase`            | Wave phase                                 | scalar  | yes      | -
`rampTime`             | Time to reach maximum motion               | scalar  | yes      | -
`waveAngle`            | Wave angle                                 | scalar  | no       | 0
`startTime`            | Start time                                 | scalar  | no       | case start time
`secondOrder`          | Second order calculation                   | bool    | no       | false
`nPaddle`              | Number of paddles                          | label   | no       | 1

Options for the `motionType` entry:

Property              | Description
----------------------|--------------------------------------------
`piston`              | Piston type
`flap`                | Flap type
`solitary`            | Solitary type

The inherited entries are elaborated in:

- fixedValuePointPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/multiphase/interFoam/laminar/waves/waveMakerFlap" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/waveModels/derivedPointPatchFields/waveMaker" %>

API:

- [Foam::waveMakerPointPatchVectorField](doxy_id://Foam::waveMakerPointPatchVectorField)

<%= history "v1812" %>

<!----------------------------------------------------------------------------->
