---
title: turbulentIntensityKineticEnergyInlet
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-turbulentIntensityKineticEnergyInlet
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inletOutlets
group: bcs-inletoutlet
---

<%= page_toc %>

## Description

The `turbulentIntensityKineticEnergyInlet` is a boundary condition that
provides a turbulent kinetic energy condition, based on user-supplied turbulence
intensity, defined as a fraction of the mean velocity:

$$
    k_p = 1.5 (I |\u|)^2
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$k_p$$               | Kinetic energy at the patch
$$I$$                 | Turbulence intensity
$$\u$$                | Velocity field

In the event of reverse flow, a zero-gradient condition is applied.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type             patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type             turbulentIntensityKineticEnergyInlet;
        intensity        <scalar>;

        // Optional entries
        U               <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "turbulentIntensityKineticEnergyInletFvPatchScalarField.H", "turbulentIntensityKineticEnergyInlet" %>
<% assert :string_exists, "turbulentIntensityKineticEnergyInletFvPatchScalarField.C", "intensity" %>
<% assert :string_exists, "turbulentIntensityKineticEnergyInletFvPatchScalarField.C", "U" %>

where:

Property              | Description                                | Type    | Required | Default
----------------------|--------------------------------------------|---------|----------|---------
`type`                | Type name: `turbulentIntensityKineticEnergyInlet`    | word     | yes      | -
`intensity`           | Fraction of mean field \[0-1\]             | scalar  | yes      | -
`U`                   | Name of velocity field                     | word    | no       | U

The inherited entries are elaborated in:

- [inletOutletFvPatchFields.H](ref_id://boundary-conditions-inletOutlet)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoSimpleFoam/squareBendLiq" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/turbulentIntensityKineticEnergyInlet" %>

API:

- [Foam::turbulentIntensityKineticEnergyInletFvPatchScalarField](doxy_id://Foam::turbulentIntensityKineticEnergyInletFvPatchScalarField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
