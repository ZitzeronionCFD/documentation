---
title: Power (enthalpy)
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- enthalpy
- power
- thermodynamics
menu_id: thermo-hPower
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-thermo
group: thermo-models
---

<%= page_toc %>

Heat capacity at constant pressure, $$ c_p $$ \[J/kg/K\]

$$
c_p = c_0 \left( \frac{T}{T_{\ref}} \right)^{n_0} + c_{p, eos}
$$

## Usage

The `hPower` thermodynamics model is defined using the entries:

~~~
thermodynamics
{
    C0              <scalar>;
    n0              <scalar>;
    Tref            <scalar>;
    Hf              <scalar>;
}
~~~

Where

- `C0` : reference heat capacity at constant pressure \[J/kg/K\]
- `n0` : model exponent \[-\];
- `Tref` : reference temperature \[K\]
- `Hf` : heat of formation \[J/kg\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/specie/thermo/hPower" %>
