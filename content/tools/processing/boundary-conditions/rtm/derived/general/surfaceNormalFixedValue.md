---
title: surfaceNormalFixedValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-surfaceNormalFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-general
group: boundary-conditions-general
---

<%= page_toc %>

## Description

The `surfaceNormalFixedValue` is a boundary condition that provides a
surface-normal vector boundary condition by its magnitude.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            surfaceNormalFixedValue;
        refValue        <scalarField>;

        // Optional entries
        ramp            <Function1<scalar>>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "surfaceNormalFixedValueFvPatchVectorField.H", "surfaceNormalFixedValue" %>
<% assert :string_exists, "surfaceNormalFixedValueFvPatchVectorField.C", "refValue" %>
<% assert :string_exists, "surfaceNormalFixedValueFvPatchVectorField.C", "ramp" %>

where:

Property           | Description               | Type    | Required | Default
-------------------|---------------------------|---------|----------|---------
`type`             | Type name: `surfaceNormalFixedValue` | word    | yes      | -
`refValue`         | Reference value fiueld    | scalarField | yes  | -
`ramp`             | Time-based ramping        | Function1\<scalar\> | no  | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/simpleCar" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/surfaceNormalFixedValue" %>

API:

- [Foam::surfaceNormalFixedValueFvPatchVectorField](doxy_id://Foam::surfaceNormalFixedValueFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
