---
title: Basic
copyright:
- Copyright (C) 2021 OpenCFD Ltd.
menu_id: solvers-basic
license: CC-BY-NC-ND-4.0
menu_parent: applications-solvers
---

<%= page_toc %>

## Solvers {#solvers}

Solver applications in the *basic* category typically comprise methods to
solve a system based on a single equation.  Options include:

<%= insert_models "solvers-basic" %>

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SOLVERS/basic" %>
