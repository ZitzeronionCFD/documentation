---
title: surfaceNoise
tags:
- acoustics
- fft
- noise
- surface
copyright:
- Copyright 2023 (C) OpenCFD Ltd.
menu_id: surfaceNoise
license: CC-BY-NC-ND-4.0
menu_parent: noise
---

<%= page_toc %>

## Surface-based noise

The `surfaceNoise` utility reads data in Ensight format, e.g.

~~~
noiseModel      surfaceNoise;

surfaceNoiseCoeffs
{
    // Common entries (see above)

    // Input file
    file       "postProcessing/faceSource1/surface/patch/patch.case";

    // Surface reader
    reader          ensight;

    // Surface writer
    writer          ensight;


    // Optional entries

    // Pressure field name; default = p
    p           p;

    // Write interval for FFT data, default = 1
    fftWriteInterval 100;

    // Area-weighted averaging switch, default = no (ensemble) for backwards
    // compatibility
    areaAverage     yes;

    // Collate times for ensight output - ensures geometry is only written once
    writeOptions
    {
        ensight
        {
            collateTimes    true;
        }
    }
}
~~~

The surface noise model is parallel aware.
{: .note}

Pressure data is read in Ensight format
{: .note}

Input surface data must be provided in Ensight Format, e.g. using the
[surfaceFieldValue](ref_id://function-objects-field-surfacefieldValue) function
object. When specifying the `formatOPtions` the `collatedTimes` option should
be employed, i.e.

~~~
surfaceFormat   ensight;
formatOptions
{
    ensight
    {
        format          binary;
        collateTimes    true;
    }
}
fields          (p);
~~~

## Output

Surface data:

- Ensight surface containing SPL, PSD, one-third octave data
- Instead of time, each snapshot represents a given frequency

![surface noise](../surfaceNoise-SPL.png)

Text files:

- Surface average frequency histories:
  - `Average_Prms_f.dat` : RMS pressure \[Pa\]
  - `Average_PSD_dB_Hz_f.dat` : Power Spectral Density in \[dB/Hz\]
  - `Average_PSD_f_f.dat` : Power Spectral Density in  \[Pa$$^2$$/Hz\]
  - `Average_SPL_dB_f.dat` : Sound Pressure Level  \[dB\]

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_UTILITIES/postProcessing/noise" %>

API:

- [Foam::noiseModels::surfaceNoise](doxy_id://Foam::noiseModels::surfaceNoise)

See also

- [noise](../../noise)
- [pointNoise](../pointNoise)
- [Curle](ref_id://function-objects-field-curle) function object

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/vortexShed" %>

<%= history "v1606+" %>
