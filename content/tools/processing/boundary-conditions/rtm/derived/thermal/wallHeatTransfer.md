---
title: wallHeatTransfer
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-wallHeatTransfer
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `wallHeatTransfer` is a boundary condition that provides
an enthalpy condition for wall heat transfer.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            wallHeatTransfer;
        Tinf            <scalarField>;
        alphaWall       <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "wallHeatTransferFvPatchScalarField.H", "wallHeatTransfer" %>
<% assert :string_exists, "wallHeatTransferFvPatchScalarField.C", "Tinf" %>
<% assert :string_exists, "wallHeatTransferFvPatchScalarField.C", "alphaWall" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `wallHeatTransfer`              | word    | yes      | -
`Tinf`                 | Temperature at the wall                    | scalarList | yes   | -
`alphaWall`            | Thermal diffusivity at the wall            | scalarList | yes   | -

The inherited entries are elaborated in:

- mixedFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/wallHeatTransfer" %>

API:

- [Foam::wallHeatTransferFvPatchScalarField](doxy_id://Foam::wallHeatTransferFvPatchScalarField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
