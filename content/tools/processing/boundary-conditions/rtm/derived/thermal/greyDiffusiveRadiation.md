---
title: greyDiffusiveRadiation
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-greyDiffusiveRadiation
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `greyDiffusiveRadiation` is a boundary condition that
provides a grey-diffuse condition for radiation intensity, `I`, for use with
the finite-volume discrete-ordinates model (fvDOM), in which the radiation
temperature is retrieved from the temperature field boundary condition.

An external radiative heat flux can be added using `qRadExt`. If
`qRadExtDir` is specified, this ray closest to this direction is used.
Otherwise, the face normal is used as direction to set `qRadExt`.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            greyDiffusiveRadiation;

        // Optional entries
        T               <word>;
        qRadExt         <scalar>;
        qRadExtDir      <vector>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "greyDiffusiveRadiationMixedFvPatchScalarField.H", "greyDiffusiveRadiation" %>
<% assert :string_exists, "greyDiffusiveRadiationMixedFvPatchScalarField.C", "T" %>
<% assert :string_exists, "greyDiffusiveRadiationMixedFvPatchScalarField.C", "qRadExt" %>
<% assert :string_exists, "greyDiffusiveRadiationMixedFvPatchScalarField.C", "qRadExtDir" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `greyDiffusiveRadiation`        | word    | yes      | -
`T`                    | Name of temperature field                  | word    | no       | T
`qRadExt`              | Radiative external flux                    | scalar  | no       | 0
`qRadExtDir`           | Radiative external flux direction          | vector  | no       | Zero

The inherited entries are elaborated in:

- mixedFvPatchFields.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/fireFoam/LES/smallPoolFire2D" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/radiation/derivedFvPatchFields/greyDiffusiveRadiation" %>

API:

- [Foam::radiation::greyDiffusiveRadiationMixedFvPatchScalarField](doxy_id://Foam::radiation::greyDiffusiveRadiationMixedFvPatchScalarField)

<%= history "1.6" %>

<!----------------------------------------------------------------------------->
