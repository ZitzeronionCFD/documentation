---
title: alphatWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-alphatWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `alphatWallFunction` is a boundary condition that provides
provides a turbulent thermal diffusivity condition when using wall functions.

The turbulent thermal diffusivity calculated using:

$$
    \alpha_t = \frac{\mu_t}{Pr_t}
$$

where:

Property              | Description
----------------------|--------------------------------------------
$$\alpha_t$$          | Turbulence thermal diffusivity
$$\mu_t$$             | Turbulence viscosity
$$Pr_t$$              | Turbulent Prandtl number

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            compressible::alphatWallFunction;

        // Optional entries
        Prt             <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "alphatWallFunctionFvPatchScalarField.H", "alphatWallFunction" %>
<% assert :string_exists, "alphatWallFunctionFvPatchScalarField.C", "Prt" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `compressible::alphatWallFunction` | word | yes | -
`Prt`                  | Turbulent Prandtl number                   | scalar  | no       | 0.85

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantSimpleFoam/roomWithThickCeiling" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/wallFunctions/alphatWallFunctions/alphatWallFunction" %>

API:

- [Foam::compressible::alphatWallFunctionFvPatchScalarField](doxy_id://Foam::compressible::alphatWallFunctionFvPatchScalarField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
