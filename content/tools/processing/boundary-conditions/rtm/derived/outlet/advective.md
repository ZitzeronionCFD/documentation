---
title: advective
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- outlet
menu_id: boundary-conditions-advective
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-outlet
group: bcs-outlet-general
---

<%= page_toc %>

## Description

The `advective` is a general boundary condition to simulate non-reflecting
advective outflow condition, based on solving the material derivative of
the given field on the boundary.

The material derivative is solved for a given field:

$$
\frac{D \phi}{D t}
\approx
\frac{\partial \phi}{\partial t} + U_n \cdot \frac{\partial \phi}{\partial \mathbf{n}}
=
0
$$

where:

Property             | Description
---------------------|-------
$$\phi$$             | Field
$$U_n$$              | Advection velocity component normal to boundary
$$\mathbf{n}$$       | Outward-pointing unit boundary-normal vector

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            advective;

        // Optional entries
        phi             <word>;
        rho             <word>;
        lInf            <scalar>;

        // Conditional entries

            // when lInf is given
            fieldInf    <Type>;

        // Inherited entries
        ...
    }

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`type`                 | Type name: `advective`                     | word    | yes      | -
`phi`                  | Name of the flux transporting the field    | word    | no       | phi
`rho`                  | Name of the density field used to normalise the mass flux       | word | no | rho
`lInf`                 | Relaxation length-scale                    | scalar  | no       | -GREAT
`fieldInf`             | Field value of the far-field               | Type    | no       | Zero

The inherited entries are elaborated in:

- mixedFvPatchFields.H

<!-- end of the list -->

- The standard time schemes (`backward`, `Euler`, `CrankNicolson`, `localEuler`)
are supported.
- The relaxation length-scale `lInf` and the far-field value `fieldInf` are
to optionally relax the value at the boundary to a specified far-field value.
- If `lInf` is specified, `fieldInf` will be required.
- `rho` is only required in the case of a mass-based flux.
- The advection velocity is assumed to be normal to the boundary.
{: .note}

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/LES/planeChannel" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/advective" %>

API:

- [Foam::advectiveFvPatchField](doxy_id://Foam::advectiveFvPatchField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
