---
title: Region boundary conditions
short_title: Region
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-region
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

Available region boundary conditions include:

<%= insert_models "bcs-region" %>
