---
title: derivedFields
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
menu_id: function-objects-field-derivedfields
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `derivedFields` function object computes computes two predefined
derived fields, i.e. `rhoU`, and `pTotal`, where the defined fields are
hard-coded as follows:

$$rhoU$$
: $$\rho \u $$

$$pTotal$$
: $$p + 0.5 \rho \mag{\u}^2$$

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `vol<Type>Field`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `vol<Type>Field`     | `$FOAM_CASE/<time>/<outField>`

where `<Type>=Scalar/Vector/SphericalTensor/SymmTensor/Tensor`.

## Usage {#usage}

Example of the `derivedFields` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    derivedFields1
    {
        // Mandatory entries (unmodifiable)
        type            derivedFields;
        libs            (fieldFunctionObjects);

        // Mandatory entries (runtime modifiable)
        derived         (rhoU pTotal);

        // Optional entries (runtime modifiable)
        rhoRef          1.0;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "derivedFields/derivedFields.H", "derivedFields" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "derivedFields",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "derivedFields/derivedFields.C", "derived" %>
<% assert :string_exists, "derivedFields/derivedFields.C", "rhoRef" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property   | Description                           | Type | Required | Default
------------|---------------------------------------|------|----------|--------
 type       | Type name: derivedFields              | word | yes      | -
 libs       | Library name: fieldFunctionObjects    | word | yes      | -
 derived    | Names of operand fields (rhoU\|pTotal) | word | yes      | -
 rhoRef     | Reference density for incompressible flows | scalar | no  | 1.0

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/rhoSimpleFoam/gasMixing/injectorPipe" %>
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/simpleFoam/squareBend" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/derivedFields" %>

API:

- [Foam::functionObjects::derivedFields](doxy_id://Foam::functionObjects::derivedFields)

<%= history "v1906" %>

<!----------------------------------------------------------------------------->
