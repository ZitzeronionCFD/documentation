---
title: Wave
copyright:
- Copyright (C) 2018-2021 OpenCFD Ltd.
tags:
- waves
menu_id: models-waves
license: CC-BY-NC-ND-4.0
menu_parent: models
---

<%= page_toc %>

## Solitary wave theories {#solitary-wave-theories}

<%= insert_models "wave-solitary" %>

## Regular wave theories {#regular-wave-theories}

<%= insert_models "wave-regular" %>

## Wave absorption theories {#wave-absorption-theories}

<%= insert_models "wave-absorption" %>

## Usage {#usage}

Wave cases are set-up by defining
[boundary conditions](ref_id://boundary-conditions)
on the velocity and phase fraction fields, in combination with common settings
in the `waveProperties` dictionary located in the `constant` directory.

The condition on the phase fraction field, typically named `alpha`, is set to
`waveAlpha`:

    <patch>
    {
        type            waveAlpha;
        values          <initial value>;
    }

and the velocity field, typically named `U`, to `waveVelocity`:

    <patch>
    {
        type            waveVelocity;
        values          <initial value>;
    }

## Further information {#further-information}

Source code:

- <%= repo_link2 "$FOAM_SRC/waveModels" %>
