---
title: Backward time scheme
short_title: Backward
copyright:
- Copyright (C) 2016-2021 OpenCFD Ltd.
tags:
- time
menu_id: schemes-time-backward
license: CC-BY-NC-ND-4.0
menu_parent: numerics-schemes-time
group: schemes-time
---

<%= page_toc %>

## Properties {#properties}

- Implicit
- Second order
- Transient
- Boundedness not guaranteed
- Conditionally stable

$$
    \ddt{\phi} = \frac{1}{\Delta t} \left( \frac{3}{2}\phi - 2 \old{\phi} + \frac{1}{2}\oldold{\phi}\right)
$$

## Usage {#usage}

The scheme is specified using:

    ddtSchemes
    {
        default         backward;
        ddt(phi)        backward;
    }

<% assert :string_exists, "backwardDdtScheme.H", "backward" %>

## Further information {#further-information}

Source code:

- [Foam::fv::backwardDdtScheme](doxy_id://Foam::fv::backwardDdtScheme)
