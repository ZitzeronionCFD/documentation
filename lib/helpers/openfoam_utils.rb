# frozen_string_literal: true

# Module to provide access to OpenFOAM project information
module OpenFOAM
  class << self
    attr_accessor :web_url, :repo_base_url, :repo_url, :repo_dir, :assertions
  end
  self.repo_dir = ENV['WM_PROJECT_DIR']
  repo_dir.freeze
  self.assertions = []

  # Utility functions
  module Utils
    def raise_warning(msg)
      puts "\nWARNING: #{msg}\n"
    end

    def raise_error(msg)
      puts "\nERROR: #{msg}\n"
      raise StandardError
    end

    def repo_url(path = nil)
      "#{@config[:openfoam][:master_url]}/#{path}"
    end

    def repo_base_url(path = nil)
      "#{@config[:openfoam][:dev_url]}/Development/openfoam/#{path}"
    end

    def breadcrumbs
      out = +'<ul class="breadcrumbs">'
      # out += "<li>#{link_to("Home", "/#")}</li>"
      breadcrumbs_trail.each do |crumb|
        next if crumb.nil? || crumb.path.nil?

        anchor = link_to(crumb[:short_title] || crumb[:title], crumb.path)
        out += "<li>#{anchor}</li>"
      end
      out += '</ul>'
      out
    end

    def page_toc
      "- TOC\n {:toc}\n"
    end

    def insert_models(*groups)
      out = ''
      groups.each do |group|
        if @config[:models][group].nil?
          out += "- none\n"
        else
          @config[:models][group].each do |id|
            item = @items[id]
            out += "- [#{item[:title]}](#{item.path})\n"
          end
        end
      end
      out
    end

    def repo_files(stem, one_file: false, raise_errors: true)
      files = h(`\\cd #{OpenFOAM.repo_dir} && git ls-files *#{stem}`).split("\n")

      return files unless one_file

      if raise_errors
        n = files.size
        raise_error("Unable to find file #{stem}") if n == 0
        raise_error("Multiple instances found (#{n}) for file *#{stem}:\n#{files}") if n > 1
      end

      return files[0] unless files.empty?
    end

    def link_to_external(body, url_options = {}, html_options = {})
      link_to(
        body,
        url_options,
        html_options.merge(target: '_blank', rel: 'noopener noreferrer')
      )
    end

    def vimeo(id, width, height)
      out = +'<div align="center">'
      out << "<iframe src=\"https://player.vimeo.com/video/#{id}\""
      out << " width=\"#{width}\" height=\"#{height}\" "
      out << 'frameborder="0" '
      out << 'webkitallowfullscreen mozallowfullscreen allowfullscreen>'
      out << '</iframe>'
      out << '</div>'
      out
    end

    def repo_file_link(stem)
      out = repo_files(stem, one_file: true)
      link_to_external out, "#{@config[:openfoam][:master_url]}/#{out}"
    end

    def repo_file_snippet(stem, tag = nil, tag2 = tag)
      out = repo_files(stem, one_file: true)

      text = File.read("#{OpenFOAM.repo_dir}/#{out}")
      text = text[/#{Regexp.escape(tag)}(.*?)#{Regexp.escape(tag2)}/m, 1] unless tag.nil?

      if text.nil?
        raise_error("No text found between tags (#{tag} -> #{tag2}) in file #{stem}")
      end

      "~~~\n#{text}\n~~~\n"
    end

    def openfoam_version_link(version)
      v = version.gsub(/\+/, '')
      v.gsub!(/00/, '0-0')
      link_to_external(
        version,
        "#{@config[:openfoam][:web_url]}/news/main-news/openfoam-#{v}"
      )
    end

    def history(introduced, previous = [])
      url = openfoam_version_link introduced
      out = "<p>History:<ul><li>Introduced in version #{url}</li>"
      unless previous.empty?
        out += '<li>Previously known as:<ul>'
        previous.each do |hash|
          hash.each do |version, name|
            url = openfoam_version_link version
            out += "<li><code>#{name}</code> : #{url}</li>"
          end
        end
        out += '</ul></li>'
      end
      out += '</ul></p>'
      out
    end

    def repo_file_path(stem, raise_errors: true)
      repo_files(stem, one_file: true, raise_errors: raise_errors)
    end

    def repo_link(envvar, stem)
      out = repo_file_path(stem)
      rel_path = out[0].gsub(/(.*#{stem}).*/, '\1')
      path = "#{OpenFOAM.repo_dir}/#{path}".gsub(ENV[envvar], "$#{envvar}")
      link_to_external path, "#{@config[:openfoam][:master_url]}/#{rel_path}"
    end

    def cat_repo_file(stem)
      out = repo_file_path(stem)
      contents = h(`cat #{OpenFOAM.repo_dir}/#{out[0]}`)
      "<pre><code>#{contents}</code></pre>"
    end

    def check_options_size(options, size)
      if options.size != size
        raise_error("options must be size #{size}, but found #{options.size}")
      end
      options
    end

    def file_exists(stem, options)
      check_options_size(options, 0)
      file = repo_file_path(stem, raise_errors: false)
      raise_warning("Failed assertion file exists '#{stem}'") if file.nil?
      !file.nil?
    end

    def text_exists(stem, options)
      lookup = check_options_size(options, 1)
      file = repo_file_path(stem, raise_errors: false)
      if file.nil?
        raise_warning("Failed assertion '#{lookup[0]}' - cannot find '#{stem}'")
        return false
      end
      found = h(`\\cd #{OpenFOAM.repo_dir} && git grep '#{lookup}' #{OpenFOAM.repo_dir}/#{file}`)
      raise_warning("Failed assertion '#{lookup}' in file '#{file}'") if found.empty?
      !found.empty?
    end

    def string_exists(stem, options)
      lookup = check_options_size(options, 1)
      file = repo_file_path(stem, raise_errors: false)
      if file.nil?
        raise_warning("Failed assertion '#{lookup[0]}' - cannot find '#{stem}'")
        return false
      end
      found = h(`\\cd #{OpenFOAM.repo_dir} && git grep '#{lookup}' #{OpenFOAM.repo_dir}/#{file}`)
      raise_warning("Failed assertion '#{lookup}' in file '#{file}'") if found.empty?
      !found.empty?
    end

    def class_library_exists(stem, options)
      class_name, lib_name = check_options_size(options, 2)
      file = repo_file_path(stem, raise_errors: false)
      if file.nil?
        raise_warning("Failed assertion '#{class_name}' - cannot find '#{stem}'")
        return false
      end
      found = h(`\\cd #{OpenFOAM.repo_dir} && git grep '#{class_name}' #{file} && git grep '#{lib_name}' #{file}`)
      raise_warning("Failed assertion '#{class_name}' in file '#{file}'") if found.empty?
      !found.empty?
    end

    def assert(fun, stem, *options)
      return if method(fun).call stem, options

      file = caller_locations.first.path.gsub(/ *item */, '').gsub(/ .*/, '')
      OpenFOAM.assertions.append(
        {
          test: fun.to_s,
          file: file,
          lineno: caller_locations.first.lineno,
          lookup: options.join(','),
          repo_file: stem
        }
      )
      # raise StandardError
    end

    def repo_link2(path)
      raise_error("Empty path #{path}") if path.empty?

      full_path = path.gsub(%r{\$(.*?)/}) { "#{ENV[::Regexp.last_match(1)]}/" }
      unless File.exist?(full_path)
        raise_error("Unable to find path #{full_path}\nInput: #{path}")
      end
      repo_path = full_path.gsub(%r{#{OpenFOAM.repo_dir}/}, '')
      link_to_external path, "#{@config[:openfoam][:master_url]}/#{repo_path}"
    end

    def array1d_to_str_table(arr, col_max = 80)
      maxlen = arr.map(&:length).max + 1
      cols = col_max / maxlen
      width = col_max / cols
      str = +''
      arr.each_with_index do |opt, index|
        str += opt.ljust(width, ' ')
        str += "\n" if ((index + 1) % cols).zero?
      end
      str
    end

    def help_util(app)
      text = md_protect(h(`#{app} -help`))
      "~~~ txt\n#{text}\n~~~\n"
    end

    def md_protect(str)
      s = str.gsub(/&lt;/, '<')
      s.gsub!(/&gt;/, '>')
      s.gsub!(/&#39;/, '\'')
      s.gsub!(/&quot;/, '\"')
      s
    end

    def help_util_usage(app)
      help = md_protect(h(`#{app} -help`))
      usage = ''
      help.split("\n").each do |line|
        usage = line.match(/^Usage(.*)$/m)
        break if !usage.nil?
      end

      return if usage.nil?

      str = usage[0].gsub(/Usage: /,'')
      "~~~ txt\n#{str}\n~~~\n"
    end

    def help_util_usage_table(app)
      help = h(`\\#{app} -help`)
      opts = []
      help.split("\n").each do |line|
        arg = line.scan(/^  -[A-z]+\.* /)
        opts.append(arg[0].strip) unless arg.empty?
      end

      str = array1d_to_str_table(opts)
      "~~~ txt\n#{str}\n~~~\n"
    end

    def help_util_arguments(app)
      text = md_protect(h(`#{app} -help`))

      start_index = text.index("Arguments:")
      if start_index.nil?
          no_input("argument")
      else
          start_index += "Arguments:".length + 1
          end_index = text.index("Options:")
          if end_index.nil?
              no_input("argument")
          else
              arguments = text[start_index...end_index].strip
              if arguments.empty?
                  no_input("argument")
              else
                  "~~~ txt\n#{arguments}\n~~~\n"
              end
          end
      end
    end

    def help_util_options(app)
      text = md_protect(h(`#{app} -help`))
      options_start = false
      options = []
      text.each_line do |line|
        if options_start
          options << line
          break if line.include?("Display full help and exit")
        end
        options_start = true if line.include?("Options:")
      end

      if options.empty?
        no_input("option")
      else
        "~~~ txt\n#{options.join}\n~~~\n"
      end
    end

    def help_options(app)
      help = h(`\\#{app} -help`)
      opts = []
      help.split("\n").each do |line|
        arg = line.scan(/^  -[A-z]+\.* /)
        opts.append(arg[0].strip) unless arg.empty?
      end

      str = array1d_to_str_table(opts)
      "~~~ txt\n#{str}\n~~~\n"
    end

    def no_input(msg)
      "No #{msg} needed."
    end

    def no_output(msg)
      "No #{msg} output."
    end

  end
end
