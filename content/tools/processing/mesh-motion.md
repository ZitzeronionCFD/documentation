---
title: Mesh motion
copyright:
- Copyright (C) 2018-2023 OpenCFD Ltd.
tags:
- mesh
- motion
menu_id: mesh-motion
license: CC-BY-NC-ND-4.0
menu_parent: processing
---

<%= page_toc %>

<!--
OpenFOAM offers a wide range of conditions
, grouped according to:
- <%= link_to_menuid "mesh-motion-dynamicmotionsolver" %>
- <%= link_to_menuid "mesh-motion-dynamicrefine" %>
-->

Dynamic mesh functionality is categorised by the terminology:

- motion: geometry change only
- topology change: e.g. cell splitting

## Usage {#usage}

Mesh motion is controlled via the `dynamicMeshDict` located in the `constant`
directory.  For a list of solver applications that include mesh motion
capabilities see [here](<%= ref_id "applications-solvers", "capability-matrix" %>).
The type of motion is set according to the
`dynamicFvMesh` entry, e.g.

```
dynamicFvMesh       <type>;
```

<% assert :string_exists, "dynamicFvMeshNew.C", "dynamicFvMesh" %>

Options include:

<%= insert_models "mesh-motion-dynamicFvMesh" %>

All the solvers tagged with *dynamic mesh* in the
[Capability matrix](<%= ref_id "applications-solvers", "capability-matrix" %>) support
mesh motion as well as topology changes

- for geometry change only specify in the `dynamicMeshDict`:

      dynamicFvMesh   dynamicMotionSolverFvMesh

<% assert :string_exists, "dynamicFvMeshNew.C", "dynamicFvMesh" %>

- this particular mesh type reads entries to select the actual motion solver to
  use, e.g.:

      motionSolverLibs ("libfvMotionSolvers.so");
      motionSolver displacementSBRStress;
      ...

<% assert :string_exists, "motionSolver.C", "motionSolverLibs" %>
<% assert :string_exists, "motionSolver.C", "motionSolver" %>
<% assert :string_exists, "displacementSBRStressFvMotionSolver.H", "displacementSBRStress" %>

- the actual motion solver might require a field with initial and boundary
  conditions, e.g. `0/pointDisplacement` and entries in the `fvSolution`,
  `fvSchemes` dictionaries.
