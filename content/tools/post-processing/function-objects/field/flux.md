---
title: flux
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- flux
- function object
- post-processing
menu_id: function-objects-field-flux
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `flux` function object computes
the flux of an input vector field.

### Operands

Operand      | Type                       | Location
:---         |:---                        |:---
input        | `{vol,surface}VectorField` | `$FOAM_CASE/<time>/<inpField>`
output file  | -                          | -
output field | `surfaceScalarField`       | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `flux` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    flux1
    {
        // Mandatory entries (unmodifiable)
        type            flux;
        libs            (fieldFunctionObjects);

        // Optional entries (runtime modifiable)
        rho             none;

        // Optional (inherited) entries
        field           <field>;
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "flux/flux.H", "flux" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "flux",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "flux/flux.H", "rho" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: flux                     | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 rho          | Name of density field               | word |  no      | none

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Usage by the `postProcess` utility is not available.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/flux" %>

API:

- [Foam::functionObjects::flux](doxy_id://Foam::functionObjects::flux)

<%= history "v1612+" %>

<!----------------------------------------------------------------------------->
