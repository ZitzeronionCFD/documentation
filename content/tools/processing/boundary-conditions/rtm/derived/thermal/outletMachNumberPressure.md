---
title: outletMachNumberPressure
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-outletMachNumberPressure
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-thermal
group: bcs-thermal
---

<%= page_toc %>

## Description

The `outletMachNumberPressure` is a boundary condition that
maintains a certain subsonic Mach number at an
outlet patch by dynamically adjusting the static outlet pressure. It makes
it possible, for example, to simulate the flow in a preturbine engine
exhaust manifold, without resolving details of the flow inside the turbine.
In general, the flow in a choked nozzle can be non-trivial and expensive
to simulate.

This formulation is derived from a simple model of the gas flow through
a nozzle with fixed geometry. The nozzle flow is assumed to be quasi-steady,
1D, isentropic and compressible.

This gives the following general relationship between pressure ratio and
Mach number in any cross section inside the nozzle:

$$
    \frac{p_{tot}}{p}=\left[ 1+ \frac{k-1}{2}\;M^2 \right]^{\frac{k}{k-1}}
$$

where the constant ratio of heat capacities is $$k=c_p/c_v$$.
The Mach number in the cross section is $$M=V/c$$, where $$c$$ is
the speed of sound and V is the uniform velocity in the streamwise
direction.

Overall pressure difference across the nozzle is
$$
    r =  pBack/p_{tot}
$$

When $$k=1.4$$, the flow in the nozzle throat becomes choked when
$$ r<0.5$$ and non-choked otherwise. This implementation is not applicable
when $$ r>=1 $$ where backflow would occur.

The nozzle model assumption locks the relationship between nozzle cross
sectional areas and Mach numbers. For a choked flow it is only the Mach
number on the outlet patch, $$M_{outlet}$$, that needs to be stated in the
boundary dictionary.

Care should be taken however to ensure that the entries in the input
dictionary and the CFD geometry satisfy the following equation
$$
    c1\frac{A_{outlet}}{A_1}=\frac{1}{M_{outlet}}\left[\frac{1+\frac{k-1}{2}
    M_{outlet}^2}{1+\frac{k-1}{2}}\right]^{\frac{k+1}{2(k-1)}}
$$
where $$c1$$ compensate for non-uniform outlet profiles, $$A_{outlet}$$
is geometrical outlet patch area and $$A_1$$ is assumed nozzle throat
area.

In the non-choked case the outlet patch Mach number is calculated as
$$
    M_{outlet} =
        \frac{A_1}
        {c1\;A_{outlet}}
        \sqrt{\frac{2}{k-1}\left[r^\frac{2}{k}-r^\frac{k+1}{k} \right]}
$$

The accompanying boundary conditions for velocity should be
`pressureInletOutletVelocity`.

#### References

    Fox, R. W., McDonald, A. T., & Mitchell, J. W. (2020).
    Fox and McDonald's introduction to fluid mechanics.
    John Wiley & Sons.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            outletMachNumberPressure;
        pBack           <scalar>;
        choked          <bool>;

        // Optional entries
        M               <scalar>;
        c1              <scalar>;
        A1              <scalar>;
        phi             <word>;
        rho             <word>;
        U               <word>;
        relax           <scalar>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "outletMachNumberPressureFvPatchScalarField.H", "outletMachNumberPressure" %>
<% assert :string_exists, "outletMachNumberPressureFvPatchScalarField.C", "pBack" %>
<% assert :string_exists, "outletMachNumberPressureFvPatchScalarField.C", "choked" %>
<% assert :string_exists, "outletMachNumberPressureFvPatchScalarField.C", "M" %>
<% assert :string_exists, "outletMachNumberPressureFvPatchScalarField.C", "c1" %>
<% assert :string_exists, "outletMachNumberPressureFvPatchScalarField.C", "A1" %>
<% assert :string_exists, "outletMachNumberPressureFvPatchScalarField.C", "phi" %>
<% assert :string_exists, "outletMachNumberPressureFvPatchScalarField.C", "rho" %>
<% assert :string_exists, "outletMachNumberPressureFvPatchScalarField.C", "U" %>
<% assert :string_exists, "outletMachNumberPressureFvPatchScalarField.C", "relax" %>

where:

Property | Description                                | Type    | Required | Default
---------|--------------------------------------------|---------|----------|---------
`type`   | Type name: `outletMachNumberPressure`      | word    | yes      | -
`pBack`  | Pressure downstream of nozzle              | bool    | yes      | -
`choked` | Defines nozzle conditions                  | bool    | yes      | -
`M`      | Outlet Mach number                         | scalar  | no       | 0
`c1`     | Correction factor for non-uniform profiles | scalar  | no       | 0
`A1`     | Nozzle throat area \[m^2\]                 | scalar  | no       | 0
`relax`  | Underrelaxation of static pressure         | scalar  | no       | 0
`phi`    | Name of flux field                         | word    | no       | phi
`rho`    | Name of density field                      | word    | no       | rho
`U`      | Name of velocity field                     | word    | no       | U

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/reactingFoam/RAS/chokedNozzle" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/thermoTools/derivedFvPatchFields/outletMachNumberPressure" %>

API:

- [Foam::outletMachNumberPressureFvPatchScalarField](doxy_id://Foam::outletMachNumberPressureFvPatchScalarField)

<%= history "v2206" %>

<!----------------------------------------------------------------------------->
