---
title: adjointInletNuaTilda
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-adjointInletNuaTilda
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-optimisation
group: bcs-optimisation
---

<%= page_toc %>

## Description

The `adjointInletNuaTilda` is a boundary condition that provides
an inlet boundary condition for adjoint `nuaTilda`.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            adjointInletNuaTilda;
        solverName      <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "adjointInletNuaTildaFvPatchScalarField.H", "adjointInletNuaTilda" %>
<% assert :string_exists, "adjointInletNuaTildaFvPatchScalarField.C", "solverName" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `adjointInletNuaTilda`          | word    | yes      | -
`solverName`   | Name of operand solver                     | word    | yes      | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- adjointBoundaryConditions.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/adjointOptimisationFoam/shapeOptimisation/motorBike" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/optimisation/adjointOptimisation/adjoint/turbulenceModels/incompressibleAdjoint/adjointRAS/derivedFvPatchFields/adjointInletNuaTilda" %>

API:

- [Foam::adjointInletNuaTildaFvPatchScalarField](doxy_id://Foam::adjointInletNuaTildaFvPatchScalarField)

<%= history "v1906" %>

<!----------------------------------------------------------------------------->
