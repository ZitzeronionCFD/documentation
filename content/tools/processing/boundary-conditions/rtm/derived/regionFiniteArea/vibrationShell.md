---
title: vibrationShell
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-vibrationShell
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-regionFa
group: bcs-regionFa
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            vibrationShell;

        // Inherited entries
        ...
    }

<% assert :string_exists, "vibrationShellFvPatchScalarField.H", "vibrationShell" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `vibrationShell`                | word    | yes      | -

The inherited entries are elaborated in:

- mixedFvPatchFields.H
- vibrationShellModel.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/compressible/acousticFoam/obliqueAirJet" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/regionFaModels/derivedFvPatchFields/vibrationShell" %>

API:

- [Foam::vibrationShellFvPatchScalarField](doxy_id://Foam::vibrationShellFvPatchScalarField)

<%= history "v1906" %>

<!----------------------------------------------------------------------------->
