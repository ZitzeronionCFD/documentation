---
title: cylindricalInletVelocity
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- inlet
menu_id: boundary-conditions-cylindricalInletVelocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-inlet
group: bcs-inlet-velocity
---

<%= page_toc %>

## Description

The `cylindricalInletVelocity` is a velocity inlet boundary condition to model
inlet velocity in cylindrical coordinates using a central axis, central point,
revolutions per minute (rpm), axial and radial velocity magnitudes.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            cylindricalInletVelocity;
        origin          <vector>;
        axis            <vector>;
        axialVelocity   <Function1<scalar>>;
        radialVelocity  <Function1<scalar>>;
        rpm             <Function1<scalar>>;

        // Inherited entries
        ...
    }

where:

Property          | Description                           | Type    | Required | Default
------------------|---------------------------------------|---------|----------|---------
`type`            | Type name: `cylindricalInletVelocity` | word    | yes      | -
`origin`          | Origin of the rotation                | vector  | yes      | -
`axis`            | Axis of the rotation                  | vector  | yes      | -
`axialVelocity`   | Axial speed                           | Function1\<scalar\>  | yes      | -
`radiaVelocity`   | Radial speed                          | Function1\<scalar\>  | yes      | -
`rpm`             | Revolutions per minute                | Function1\<scalar\>  | yes      | -

The inherited entries are elaborated in:

- [Foam::fixedValueFvPatchField](doxy_id://Foam::fixedValueFvPatchField)
- [Foam::Function1](doxy_id://Foam::Function1)
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/combustion/XiDyMFoam/annularCombustorTurbine" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/finiteVolume/fields/fvPatchFields/derived/cylindricalInletVelocity" %>

API:

- [Foam::cylindricalInletVelocityFvPatchVectorField](doxy_id://Foam::cylindricalInletVelocityFvPatchVectorField)

<%= history "1.5" %>

<!----------------------------------------------------------------------------->
