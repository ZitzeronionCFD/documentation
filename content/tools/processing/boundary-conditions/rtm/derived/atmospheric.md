---
title: Atmospheric boundary conditions
short_title: Atmospheric
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-atmospheric
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions
---

<%= page_toc %>

Available atmospheric boundary conditions include:

<%= insert_models "bcs-atmospheric" %>
