---
title: alphatPhaseChangeWallFunction
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-alphatPhaseChangeWallFunction
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-multiphase
group: bcs-multiphase
---

<%= page_toc %>

## Description

The `compressible::alphatPhaseChangeWallFunction` is a boundary condition that provides
an abstract base-class for all `alphatWallFunctions` supporting phase-change.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Optional entries
        dmdt        <scalarField>;
        mDotL       <scalarField>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "alphatPhaseChangeWallFunctionFvPatchScalarField.C", "dmdt" %>
<% assert :string_exists, "alphatPhaseChangeWallFunctionFvPatchScalarField.C", "mDotL" %>

where:

Property               | Description                                | Type    | Required | Default
-----------------------|--------------------------------------------|---------|----------|---------
`dmdt`                 | Rate of phase-change                       | scalarField | no   | 0.0
`mDotL`                | Latent heat of the phase-change            | scalarField | no   | 0.0

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/phaseSystemModels/reactingEuler/multiphaseSystem/derivedFvPatchFields/alphatPhaseChangeWallFunction" %>

API:

- [Foam::compressible::alphatPhaseChangeWallFunctionFvPatchScalarField](doxy_id://Foam::compressible::alphatPhaseChangeWallFunctionFvPatchScalarField)

<%= history "v2012" %>

<!----------------------------------------------------------------------------->
