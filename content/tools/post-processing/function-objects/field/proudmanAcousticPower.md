---
title: proudmanAcousticPower
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- acoustics
- field
- function object
- post-processing
- Proudman
menu_id: function-objects-field-proudmanacousticpower
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `proudmanAcousticPower` function object
computes the acoustic power due to the volume of isotropic turbulence
using Proudman's formula.

The acoustic power, i.e. $$P_A$$ \[W/m^3\] in terms of turbulent kinetic energy,
i.e. $$k$$, and turbulent kinetic energy dissipation rate, i.e. $$\epsilon$$
is given as:

$$
    P_A = \alpha_\epsilon \rho \epsilon M_t^5
$$

where $$\alpha_\epsilon = 0.1$$ is a constant and

$$
    M_t = \frac{\sqrt{2 k}}{a_0}
$$

with $$a_0$$ the speed of sound.

The acoustic power is also output in `dB` using:

$$
    L_P = 10 \log \frac{P_A}{P_{\ref}}
$$

where $$P_{\ref} = 1e^{-12}$$ \[W/m^3\] is a constant.

### Operands

Operand       | Type             | Location
:---          |:---              |:---
input         | `volScalarField` | `$FOAM_CASE/<time>/<inpField>`
output file   | -                | -
output field  | `volScalarField` | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `proudmanAcousticPower` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    proudmanAcousticPower1
    {
        // Mandatory entries (unmodifiable)
        type            proudmanAcousticPower;
        libs            (fieldFunctionObjects);

        // Optional entries (runtime modifiable)
        alphaEps    0.1;
        // For incompressible flow simulations
        rhoInf      1.225;
        aRef        340;

        // Optional (inherited) entries
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists,
   "proudmanAcousticPower/proudmanAcousticPower.H", "proudmanAcousticPower" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "proudmanAcousticPower",
   "libfieldFunctionObjects" %>
<% assert :string_exists,
   "proudmanAcousticPower/proudmanAcousticPower.H", "alphaEps" %>
<% assert :string_exists,
   "proudmanAcousticPower/proudmanAcousticPower.H", "rhoInf" %>
<% assert :string_exists,
   "proudmanAcousticPower/proudmanAcousticPower.H", "aRef" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: proudmanAcousticPower    | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -
 rhoInf     | Freestream density (for incompressible) | scalar | conditional | -
 aRef         | Speed of sound (incompressible)     | scalar | conditional | -
 alphaEps     | Empirical model coefficient         | scalar | no | 0.1

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)

Usage by the `postProcess` utility is not available.

### Notes on entries

The freestream density and reference speed of sound are only necessary
when a thermodynamics package is unavailable, typically for incompressible
cases.

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/proudmanAcousticPower" %>

API:

- [Foam::functionObjects::proudmanAcousticPower](doxy_id://Foam::functionObjects::proudmanAcousticPower)

<%= history "v1912" %>

<!----------------------------------------------------------------------------->
