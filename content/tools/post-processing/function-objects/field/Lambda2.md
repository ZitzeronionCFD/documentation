---
title: Lambda2
copyright:
- Copyright (C) 2019-2021 OpenCFD Ltd.
tags:
- eigenvalue
- field
- function object
- Lambda2
- post-processing
- velocity
menu_id: function-objects-field-lambda2
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `Lambda2` function object computes the second largest eigenvalue of
the sum of the square of the symmetrical and anti-symmetrical parts of
the velocity gradient tensor.

### Operands

Operand      | Type                 | Location
:---         |:---                  |:---
input        | `volVectorField`     | `$FOAM_CASE/<time>/<inpField>`
output file  | -                    | -
output field | `volScalarField`     | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `Lambda2` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    Lambda21
    {
        // Mandatory entries (unmodifiable)
        type            Lambda2;
        libs            (fieldFunctionObjects);

        // Optional (inherited) entries
        field           <inpField>;
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "Lambda2/Lambda2.H", "Lambda2" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "Lambda2",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: Lambda2                  | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func Lambda2

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/Lambda2" %>

API:

- [Foam::functionObjects::Lambda2](doxy_id://Foam::functionObjects::Lambda2)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->
