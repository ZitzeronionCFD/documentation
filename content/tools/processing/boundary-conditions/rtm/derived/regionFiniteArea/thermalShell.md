---
title: thermalShell
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
- wall
menu_id: boundary-conditions-thermalShell
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-regionFa
group: bcs-regionFa
---

<%= page_toc %>

## Description

The `compressible::thermalShell` is a boundary condition that provides
a coupled temperature condition between a primary region (3D mesh)
and a thermal shell model (2D mesh).

The primary region boundary creates the finite area region
and evolves its energy equation.

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            thermalShell;

        // Inherited entries
        ...
    }

<% assert :string_exists, "thermalShellFvPatchScalarField.H", "thermalShell" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `compressible::thermalShell`    | word    | yes      | -

The inherited entries are elaborated in:

- fixedValueFvPatchField.H
- thermalShellModel.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/heatTransfer/buoyantPimpleFoam/hotRoomWithThermalShell" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/regionFaModels/derivedFvPatchFields/thermalShell" %>

API:

- [Foam::compressible::thermalShellFvPatchScalarField](doxy_id://Foam::compressible::thermalShellFvPatchScalarField)

<%= history "v1906" %>

<!----------------------------------------------------------------------------->
