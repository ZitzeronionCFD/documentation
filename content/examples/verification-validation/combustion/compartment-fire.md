---
title: Compartment fire
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- combustion
- example
- fire
- reactions
- verification
- validation
menu_id: verification-validation-combustion-compartment-fire
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-combustion
group: verification-validation-combustion
---

<%= page_toc %>

## Overview {#sec-overview}

- Solver: [fireFoam](ref_id://firefoam)
- <%= repo_link2 "$FOAM_TUTORIALS/combustion/fireFoam/LES/compartmentFire" %>
