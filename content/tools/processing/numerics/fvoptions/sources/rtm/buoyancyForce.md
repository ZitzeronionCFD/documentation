---
title: Buoyancy force
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- buoyancy
- fvOption
- momentum
- source
menu_id: fvoption-buoyancyforce
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-momentum
---

<%= page_toc %>

## Properties {#properties}

Adds the buoyancy force

$$
    S = \rho \vec{g}
$$

## Usage {#usage}

The option is specified using:

    buoyancy1
    {
        type            buoyancyForce;
        selectionMode   all;
        fields          (U);
    }

<% assert :string_exists, "buoyancyForce.H", "buoyancyForce" %>

## Further information {#further-information}

Tutorials

- *none*

Source code

- <%= repo_link2 "$FOAM_SRC/fvOptions/sources/derived/buoyancyForce" %>
