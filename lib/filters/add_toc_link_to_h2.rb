# frozen_string_literal: true

require 'nokogiri'

class AddRefToTocFilter < Nanoc::Filter
  identifier :add_toc_link_to_h2

  def run(content, _params)
    doc = Nokogiri::HTML(content)
    h1 = doc.at_css('h1').add_child(
      Nokogiri::HTML.fragment("<a id='page-toc'></a>")
    );

    headers = doc.css('h2:not(#page-toc),h3:not(.no-top)')
    headers.each do |header|

      # Convert headers to permalinks chain: U+1F517
      header.inner_html = "#{"#{header.inner_html}<a class=\"permalink\" href=\"##{header['id']}\">&#x1F517;</a>"}"
      #header.add_next_sibling(
      #  Nokogiri::HTML.fragment("<a class=\"permalink\" href=\"##{header['id']}#\">&#x1F517;</a>")
      #)

      # Add link to 'top' under each h2..h5
      #header.add_next_sibling(
      #  Nokogiri::HTML.fragment("<a href='#page-toc'>top</a><br><br>")
      #)
    end

    # Move markdown toc and wrap in div
    div_wrapper = doc.at_xpath('//div[@class="wrapper"]')
    toc = doc.at_css('ul#markdown-toc')
    if !toc.nil? && !div_wrapper.nil?
      toc.wrap('<div class="markdown-toc"></div>')
      div_wrapper.add_child(toc)
    end

    doc.to_s
  end
end
