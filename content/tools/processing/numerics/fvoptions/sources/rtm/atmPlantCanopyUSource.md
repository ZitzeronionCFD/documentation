---
title: atmPlantCanopyUSource
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- atmospheric
- fvOption
- momentum
- source
menu_id: fvoption-atmplantcanopyusource
menu_parent: numerics-fvoptions-sources
license: CC-BY-NC-ND-4.0
group: fvoptions-sources-atm
---

<%= page_toc %>

## Properties {#properties}

- The `atmPlantCanopyUSource` applies sources on velocity, i.e. `U`,
to incorporate effects of plant canopy for atmospheric boundary layer modelling.
- The `atmPlantCanopyUSource` can be applied on any RAS turbulence model.
- The `atmPlantCanopyUSource` inherits the traits of the
[fvOption](ref_id://fvoptions), and `cellSetOption`.

Corrections applied to:

`U`
: Velocity \[m/s\]

Required fields:

`U`
: Velocity \[m/s\]

`plantCd`
: Plant canopy drag coefficient \[-\]

`leafAreaDensity`
: Leaf area density \[1/m\]

## Model equations {#model-equations}

The model expression (<%= cite "sogachev_atm_2006" %>, Eq. 42):

$$
    S_p -= \alpha \rho \left( C_d {LAD} |\u_o| \right) \u
$$

where

$$f_M$$
: fvMatrix of the velocity field

$$C_d$$
: Plant canopy drag coefficient \[-\]

$${LAD}$$
: Leaf area density \[1/m\]

$$\u_o$$
: Previous-iteration velocity field \[m/s\]

$$\alpha$$
: Phase fraction in multiphase computations, otherwise equals to 1

$$\rho$$
: Fluid density in compressible computations, otherwise equals to 1

## Usage {#usage}

Example of the fvOptions specification using `constant/fvOptions` file:

    atmPlantCanopyUSource1
    {
        // Mandatory entries (unmodifiable)
        type                  atmPlantCanopyUSource;

        atmPlantCanopyUSourceCoeffs
        {
            // Mandatory (inherited) entries (unmodifiable)
            selectionMode    all;

            // Optional entries (unmodifiable)
            rho             rho;
        }

        // Optional (inherited) entries
        ...
    }

<% assert :string_exists, "atmPlantCanopyUSource.H", "atmPlantCanopyUSource" %>
<% assert :string_exists, "atmPlantCanopyUSource.C", "rho" %>
<% assert :string_exists, "cellSetOption.C", "selectionMode" %>
<% assert :string_exists, "cellSetOption.C", "cellZone" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: atmPlantCanopyUSource | word | yes      | -
 rho          | Name of density field               | word | no       | rho

The inherited entries are elaborated in:

- [fvOption](ref_id://fvoptions)
  - `cellSetOption`

## Further information {#further-information}

Tutorials

- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmFlatTerrain" %>
- <%= repo_link2 "$FOAM_TUTORIALS/verificationAndValidation/atmosphericModels/atmForestStability" %>

Source code

- <%= repo_link2 "$FOAM_SRC/atmosphericModels/fvOptions/atmPlantCanopyUSource" %>

<%= history "v2006" %>

<!----------------------------------------------------------------------------->
