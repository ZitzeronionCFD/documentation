---
title: Planar Poiseuille non-Newtonian flow
short_title: Planar Poiseuille
copyright:
- Copyright (C) 2018-2022 OpenCFD Ltd.
tags:
- example
- laminar
- verification
- validation
menu_id: verification-validation-laminar-planar-poiseuille
license: CC-BY-NC-ND-4.0
menu_parent: verification-validation-laminar
group: verification-validation-laminar
---

<%= page_toc %>

## Overview {#sec-overview}

- Solver: [pimpleFoam](ref_id://pimplefoam)
- Experimental case described by Amoreira and Olivera <%= cite "amoreira_comparison_2010" %>
- Start-up planar Poiseuille flow of a non-Newtonian fluid
- Modelled using the Maxwell viscoelastic laminar stress model
- Initially at rest
- Constant pressure gradient applied from time zero
- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pimpleFoam/laminar/planarPoiseuille" %>

## Mesh {#sec-mesh}

- 1D structured mesh created using [blockMesh](ref_id://blockmesh)

## Results {#sec-results}

The predictions are compared against the analytical solution for velocity given
by Waters and King <%= cite "waters_unsteady_1970" %>

$$
    u(y,t) = \frac{3}{2}(1 - y^2) + \sum\limits_{k=1}^\infty A_k(t)B_k(t),
$$

$$
    A_k(t) = e^{-b_k t}
    \begin{cases}
        \frac{b_k-a_k^2}{c_k}\sinh c_k t
       + \cosh c_k t, & \text{if } b_k \geq a_k, \\
        \frac{b_k-a_k^2}{c_k}\sin c_k t
       + \cos c_k t, & \text{if } b_k \lt a_k,
    \end{cases}
$$

$$
    B_k(y) = \frac{48(-1)^k}{(2k -1)^3\pi^3} \cos \frac{2k - 1}{2}\pi y,
$$

where

$$
    a_k = \frac{2k - 1}{2}\pi \sqrt{E},\;
    b_k = \frac{1+\beta a_k^2}{2},\;
    c_k = \sqrt{|b_k^2 - a_k^2|}.
$$

![Velocity against time](planar-poiseuille-400x400.png)
