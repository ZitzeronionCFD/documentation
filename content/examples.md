---
title: Examples
copyright:
- Copyright (C) 2017-2022 OpenCFD Ltd.
tags:
- example
menu_id: examples
license: CC-BY-NC-ND-4.0
menu_parent: home
menu_weight: 500
---

<%= page_toc %>

## Introduction {#introduction}

OpenFOAM includes a large set of tutorial cases to showcase the code's
functionality, available under the directory:

- [Tutorials](repo_id://tutorials)

The following links provide entry points to a selection of documented cases:

- [Test cases](./test-cases)
- [Verification and validation](./verification-validation)
- [Tutorials](./tutorials)
