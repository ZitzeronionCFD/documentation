---
title: Command line
copyright:
- Copyright (C) 2021 OpenCFD Ltd.
tags:
- navigation
menu_id: command-line
license: CC-BY-NC-ND-4.0
menu_parent: fundamentals
menu_weight: 40
---

<%= page_toc %>

## Command line interface *user @openfoam:~\$...* {#command-line-interface}

OpenFOAM comprises many applications, primarily designed to be driven by the
*command line*.  Whilst sometimes unfamiliar to new users, this interface
provides the most flexible means by which to control the phases of case set-up,
execution and evaluation of results, and lends itself to automation and
batch-processing.

## Basic usage {#usage}

Applications are invoked using commands of the form:

~~~
<application> [OPTIONS] <arguments>
~~~

More detailed usage information is available using the *help* option; for
example, when applied to the blockMesh application as:

~~~
blockMesh -help
~~~

the following text is presented:

<%= help_util "blockMesh" %>

## Navigation {#navigation}

### Useful commands {#navigation-useful-commands}

Moving around the source code and cases is easily performed using standard
linux commands, e.g.

- `ls`: list directory contents
- `cd`: change directory

In addition, several *aliases* are available which provide short cuts to many
OpenFOAM directories. These include:

- `foam`: change to the main OpenFOAM project directory
- `src`: change to the top-level source directory
- `sol`: change to the top-level solver directory
- `util`: change to the top-level utilities directory
- `tut`: change to the top-level tutorial directory

### Tab completion {#navigation-tab-completion}

Command line tab completion of OpenFOAM applications presents users with
selections that are tailored for the current action.  For example, issuing

~~~
checkMesh <TAB> <TAB>
~~~

returns the list of available options for the `checkMesh` utility:

<%= help_options "checkMesh" %>

Many options require additional user inputs, identified by `-option <value>`
entries when requesting help via the `-help` option, as shown in the
previous [blockMesh](#usage) example.

If using the `-time` option, the completion will limit the options to the list
of available times, e.g. after running the tutorial:

- <%= repo_link2("$FOAM_TUTORIALS/incompressible/icoFoam/cavity/cavity") %>

Invoking the following:

~~~
checkMesh -time <TAB> <TAB>
~~~

returns:

~~~
0    0.1  0.2  0.3  0.4  0.5
~~~

Multi-region cases are notoriously complex to set-up and process.  The `-region`
option will limit the available options to the list of regions, e.g. applied to
the tutorial:

- <%= repo_link2("$FOAM_TUTORIALS/heatTransfer/chtMultiRegionSimpleFoam/heatExchanger") %>

Invoking the following:

~~~
checkMesh -region <TAB> <TAB>
~~~

returns:

~~~
air     porous
~~~

### Command line help {#navigation-command-line-help}

Manual pages provide a straightforward interface to access the options
available to all OpenFOAM applications.  See the full list of options here:

- [Man pages](../man)
