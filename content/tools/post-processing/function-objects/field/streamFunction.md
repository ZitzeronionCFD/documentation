---
title: streamFunction
copyright:
- Copyright (C) 2020-2021 OpenCFD Ltd.
tags:
- field
- function object
- post-processing
- stream function
menu_id: function-objects-field-streamfunction
license: CC-BY-NC-ND-4.0
menu_parent: function-objects-field
group: function-objects-field
---

<%= page_toc %>

## Description {#description}

The `streamFunction` function object computes the stream function, i.e.
https://w.wiki/Ncm.

### Operands

Operand      | Type                   | Location
:---         |:---                    |:---
input        | `surfaceScalarField`   | `$FOAM_CASE/<time>/<inpField>`
output file  | -                      | -
output field | `pointScalarField`     | `$FOAM_CASE/<time>/<outField>`

## Usage {#usage}

Example of the `streamFunction` function object
by using `functions` sub-dictionary in `system/controlDict` file:

    streamFunction1
    {
        // Mandatory entries (unmodifiable)
        type            streamFunction;
        libs            (fieldFunctionObjects);

        // Optional (inherited) entries
        field           <inpField>;
        result          <fieldResult>;
        region          region0;
        enabled         true;
        log             true;
        timeStart       0;
        timeEnd         1000;
        executeControl  timeStep;
        executeInterval 1;
        writeControl    timeStep;
        writeInterval   1;
    }

<% assert :string_exists, "streamFunction.H", "streamFunction" %>
<% assert :class_library_exists,
   "functionObjects/field/Make/files",
   "streamFunction",
   "libfieldFunctionObjects" %>
<% assert :string_exists, "fieldExpression.C", "field" %>
<% assert :string_exists, "fieldExpression.C", "result" %>
<% assert :string_exists, "regionFunctionObject.C", "region" %>
<% assert :string_exists, "functionObjectList.C", "enabled" %>
<% assert :string_exists, "functionObject.C", "log" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeStart" %>
<% assert :string_exists, "timeControlFunctionObject.C", "timeEnd" %>
<% assert :string_exists, "timeControlFunctionObject.C", "execute" %>
<% assert :string_exists, "timeControlFunctionObject.C", "write" %>

where the entries mean:

 Property     | Description                         | Type | Required | Default
--------------|-------------------------------------|------|----------|--------
 type         | Type name: streamFunction           | word | yes      | -
 libs         | Library name: fieldFunctionObjects  | word | yes      | -

The inherited entries are elaborated in:

- [functionObject](ref_id://post-processing-function-objects)
- [fieldExpression](ref_id://function-objects-field-fieldexpression)

Example by using the `postProcess` utility:

    postProcess -func streamFunction

## Further information {#further-information}

Tutorial:

- <%= repo_link2 "$FOAM_TUTORIALS/incompressible/pisoFoam/RAS/cavity" %>

Source code:

- <%= repo_link2 "$FOAM_SRC/functionObjects/field/streamFunction" %>

API:

- [Foam::functionObjects::streamFunction](doxy_id://Foam::functionObjects::streamFunction)

<%= history "v1606+" %>

<!----------------------------------------------------------------------------->