---
title: timeVaryingMappedFixedValue
copyright:
- Copyright (C) 2023 OpenCFD Ltd.
tags:
- boundary conditions
menu_id: boundary-conditions-timeVaryingMappedFixedValue
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-meshMotion
group: bcs-meshMotion
---

<%= page_toc %>

## Description

## Usage {#usage}

The condition requires entries in both the boundary and field files.

### Boundary file

    <patchName>
    {
        type            patch;
        ...
    }

### Field file

    <patchName>
    {
        // Mandatory entries
        type            timeVaryingMappedFixedValue;

        // Optional entries
        setAverage      <bool>;
        perturb         <scalar>;
        points          <word>;
        offset          <Function1<Type>>;
        fieldTable      <word>;
        mapMethod       <word>;

        // Inherited entries
        ...
    }

<% assert :string_exists, "timeVaryingMappedFixedValuePointPatchField.H", "timeVaryingMappedFixedValue" %>
<% assert :string_exists, "timeVaryingMappedFixedValuePointPatchField.C", "setAverage" %>
<% assert :string_exists, "timeVaryingMappedFixedValuePointPatchField.C", "perturb" %>
<% assert :string_exists, "timeVaryingMappedFixedValuePointPatchField.C", "points" %>
<% assert :string_exists, "timeVaryingMappedFixedValuePointPatchField.C", "offset" %>
<% assert :string_exists, "timeVaryingMappedFixedValuePointPatchField.C", "fieldTable" %>
<% assert :string_exists, "timeVaryingMappedFixedValuePointPatchField.C", "mapMethod" %>

where:

Property       | Description                                | Type    | Required | Default
---------------|--------------------------------------------|---------|----------|---------
`type`         | Type name: `timeVaryingMappedFixedValue`   | word    | yes      | -
`setAverage`   | Use average value                          | bool    | no       | false
`perturb`      | Perturb points for regular geometries      | scalar  | no       | 1e-5
`points`       | Name of points file                        | word    | no       | points
`fieldTable`   | Alternative field name to sample           | word    | no       | this field name
`mapMethod`    | Type of mapping                            | word    | no       | planar
`offset`       | Offset to mapped values                    | scalar  | no       | Zero

The inherited entries are elaborated in:

- fixedValuePointPatchField.H
- Function1.H
<!-- end of the list -->

## Further information {#further-information}

Tutorial:

- N/A

Source code:

- <%= repo_link2 "$FOAM_SRC/fvMotionSolver/pointPatchFields/derived/timeVaryingMappedFixedValue" %>

API:

- [Foam::timeVaryingMappedFixedValuePointPatchField](doxy_id://Foam::timeVaryingMappedFixedValuePointPatchField)

<%= history "2.1.1" %>

<!----------------------------------------------------------------------------->
