---
title: Pressure inlet outlet velocity
copyright:
- Copyright (C) 2017-2021 OpenCFD Ltd.
tags:
- boundary conditions
- outlet
menu_id: boundary-conditions-pressure-inlet-outlet-velocity
license: CC-BY-NC-ND-4.0
menu_parent: boundary-conditions-outlet
group: bcs-outlet-velocity
---

<%= page_toc %>

## Properties {#properties}

- Wrapper around the [mixed](ref_id://boundary-conditions-mixed) condition
- Applicable to the velocity vector field
- Flow out of the domain: assigns a
  [zeroGradient](ref_id://boundary-conditions-zeroGradient) condition
- Flow into the domain: assigns a velocity based on the flux in the patch-normal
  direction

## Usage {#usage}

The condition is specified in the field file using:

    <patchName>
    {
        type                pressureInletOutletVelocity;
        tangentialVelocity  <field value>;
        value               <field value>;

        // Optional entries
        phi                 phi;
    }

<% assert :string_exists,
   "pressureInletOutletVelocityFvPatchVectorField.H",
   "pressureInletOutletVelocity" %>
<% assert :string_exists,
   "pressureInletOutletVelocityFvPatchVectorField.C",
   "tangentialVelocity" %>
<% assert :string_exists,
   "pressureInletOutletVelocityFvPatchVectorField.C",
   "phi" %>

## Further information {#further-information}

Source code:

- [Foam::pressureInletOutletVelocityFvPatchVectorField](doxy_id://Foam::pressureInletOutletVelocityFvPatchVectorField)
