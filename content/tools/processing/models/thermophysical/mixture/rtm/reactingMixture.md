---
title: Reacting Mixture
short_title: Reacting
copyright:
- Copyright (C) 2022 OpenCFD Ltd.
tags:
- mixture
- multi-component
- reacting
- single phase
- thermophysical
menu_id: mixture-reactingMixture
license: CC-BY-NC-ND-4.0
menu_parent: thermophysical-mixture
group: mixture-models
---

<%= page_toc %>

The `reactingMixture` model describes a single phase mixture that is
defined using individual, possibly reacting species.

## Usage

The `reactingMixture` is set in the top-level `thermoType` dictionary:

~~~
thermoType
{
    mixture         reactingMixture;
    <...>
}
~~~

Specie and reaction properties are introduced by the `chemistryReader`, e.g.
to specify the `foamChemistryReader`:

~~~
chemistryReader foamChemistryReader;
foamChemistryFile <path-to-reactions-file>;
foamChemistryThermoFile "path-to-thermophysical-properties-file";
~~~

For example:

~~~
chemistryReader foamChemistryReader;
foamChemistryFile "<constant>/reactions";
foamChemistryThermoFile "<constant>/thermo";
~~~

The `foamChemistryFile` contains entries for:

~~~
elements        (<list-of-elements>);
species         (<list-of-species>)

reactions
{
    // List of reactions in dictionary format
}
~~~

The `foamChemistryThermoFile` contains descriptions for each specie whose
requirements depend on the top-level `thermoType` options
[detailed earlier](ref_id://models-thermophysical).

## Initial conditions {#initial-conditions}

See [multiComponentMixture](ref_id://mixture-multiComponentMixture#initial-conditions)
initial conditions.

## Further information {#further-information}

Source code

- <%= repo_link2 "$FOAM_SRC/thermophysicalModels/reactionThermo/mixtures/reactingMixture" %>

Example usage:

- reactingFoam `counterFlowFlame2D` tutorial:
  <%= repo_link2 "$FOAM_TUTORIALS/combustion/reactingFoam/laminar/counterFlowFlame2D" %>
